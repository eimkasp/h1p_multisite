﻿<?php
/*
Plugin Name: Host1Plus Sync Params
Plugin URI:  http://www.host1plus.com
Description: This looks for configured parameters in URL and Cookies and rewrites links to configured domains to include this data
Version:     1.0
Author:      Linas Žilinskas
Author URI:  mailto:linas@host1plus.com
*/

if (!defined('ABSPATH')) die();

function wp_h1p_sync_params_adminmenu()
{
    add_options_page('Host1Plus Sync Params', 'H1P Sync Params', 'manage_options', 'wp_h1p_sync_params_settings', 'wp_h1p_sync_params_settings');
}

function wp_h1p_sync_params_settings()
{
    // add admin side js
    $admin_js_url = plugins_url('wp-h1p-sync-params/admin.js');
    wp_register_script('wp_h1p_sync_params_admin_js', $admin_js_url);
    wp_enqueue_script('wp_h1p_sync_params_admin_js');

    echo '<div class="wrap">';
    echo '<form id="wp_h1p_sync_params_settings_form" method="post" action="options.php">';
    echo '<h2>Host1Plus Sync Params</h2>';

    settings_fields('wp_h1p_sync_params_settings');
    do_settings_sections('wp_h1p_sync_params_settings');
    submit_button();

    echo '</form>';
}

function wp_h1p_sync_params_options_init()
{
    add_settings_section('wp_h1p_sync_params_general_settings', 'General Settings', 'wp_h1p_sync_params_base_settings_callback', 'wp_h1p_sync_params_settings');

    add_settings_field('wp_h1p_sync_params_cookie_prefix', 'Cookie Prefix', 'wp_h1p_sync_params_cookie_prefix_callback', 'wp_h1p_sync_params_settings', 'wp_h1p_sync_params_general_settings');
    register_setting('wp_h1p_sync_params_settings', 'wp_h1p_sync_params_cookie_prefix');
    add_option('wp_h1p_sync_params_cookie_prefix', '');

    add_settings_field('wp_h1p_sync_params_thisdomain', 'Current Domain', 'wp_h1p_sync_params_thisdomain_callback', 'wp_h1p_sync_params_settings', 'wp_h1p_sync_params_general_settings');
    register_setting('wp_h1p_sync_params_settings', 'wp_h1p_sync_params_thisdomain');
    add_option('wp_h1p_sync_params_thisdomain', '');

    add_settings_field('wp_h1p_sync_params_sites', 'Domains to Synchronize', 'wp_h1p_sync_params_sites_callback', 'wp_h1p_sync_params_settings', 'wp_h1p_sync_params_general_settings');
    register_setting('wp_h1p_sync_params_settings', 'wp_h1p_sync_params_sites');
    add_option('wp_h1p_sync_params_sites', []);

    add_settings_field('wp_h1p_sync_params_cookies', 'Parameters to Synchronize', 'wp_h1p_sync_params_cookies_callback', 'wp_h1p_sync_params_settings', 'wp_h1p_sync_params_general_settings');
    register_setting('wp_h1p_sync_params_settings', 'wp_h1p_sync_params_cookies');
    add_option('wp_h1p_sync_params_cookies', []);
}

function wp_h1p_sync_params_base_settings_callback()
{
    return true;
}


function wp_h1p_sync_params_cookie_prefix_callback()
{
    echo '<input name="wp_h1p_sync_params_cookie_prefix" id="wp_h1p_sync_params_cookie_prefix" type="text" value="' . get_option('wp_h1p_sync_params_cookie_prefix') . '" class="code" />';
}

function wp_h1p_sync_params_thisdomain_callback()
{
    echo '<input name="wp_h1p_sync_params_thisdomain" id="wp_h1p_sync_params_thisdomain" type="text" value="' . get_option('wp_h1p_sync_params_thisdomain') . '" class="code" /> (Without any subdomain, e.g. www)';
}

function wp_h1p_sync_params_sites_callback()
{
    $values = get_option('wp_h1p_sync_params_sites');

    if (count($values) == 0)
        echo '<input name="wp_h1p_sync_params_sites[]" id="wp_h1p_sync_params_sites" type="text" class="code" value="" /><br />';

    for ($i = 0; $i < count($values); $i++)
        echo '<input name="wp_h1p_sync_params_sites[]" id="wp_h1p_sync_params_sites" type="text" class="code" value="' . $values[$i] . '" /><br />';

    echo '<br /><input type="button" class="button" value="Add more" onclick="wp_h1p_sync_params_sites_add_new_row(); return false;" />';
}

function wp_h1p_sync_params_cookies_callback()
{
    $values = get_option('wp_h1p_sync_params_cookies');

    if (count($values) == 0)
        echo '<input name="wp_h1p_sync_params_cookies[]" id="wp_h1p_sync_params_cookies" type="text" class="code" value="" /><br />';

    for ($i = 0; $i < count($values); $i++)
        echo '<input name="wp_h1p_sync_params_cookies[]" id="wp_h1p_sync_params_cookies" type="text" class="code" value="' . $values[$i] . '" /><br />';

    echo '<br /><input type="button" class="button" value="Add more" onclick="wp_h1p_sync_params_cookies_add_new_row(); return false;" />';
}

/**
 * Adds javascript to the non-admin pages to rewrite links to include additional data
 */
function wp_h1p_sync_params_replace_links()
{
    $replace_links = plugins_url('wp-h1p-sync-params/rewrite_links.php');
    wp_register_script('wp_h1p_sync_params_replace_links', $replace_links);
    wp_enqueue_script('wp_h1p_sync_params_replace_links');
}

/**
 * Parses the URL looking for configured parameters and sets cookies with the data
 */
function wp_h1p_sync_params_set_remote_cookies()
{
    if (!is_front_page() && !is_home() && !is_category() && !is_single() && !is_page())
        return;

    $thisdomain = get_option('wp_h1p_sync_params_thisdomain');
    $cookies    = get_option('wp_h1p_sync_params_cookies');
    $prefix     = get_option('wp_h1p_sync_params_cookie_prefix');

    if (!is_array($cookies))
        $cookies = [];

    $req_params = [];
    parse_str($_SERVER['QUERY_STRING'], $req_params);

    foreach ($req_params as $name => $value)
    {
        if (!empty($prefix) && strpos($name, $prefix) === 0) // url contains param with our prefix
        {
            $original_name = preg_replace('/' . $prefix . '/', '', $name, 1);

            if (in_array($original_name, $cookies))
            {
                if (!empty($_COOKIE[$name])) // unset cookie with prefix if one without prefix is found
                    setcookie($name, null, -1, '/', '.' . $thisdomain, false, false);

                setrawcookie($original_name, rawurlencode($value), time() + 60*60*24*90, '/', '.' . $thisdomain, false, false);
            }
        }
        elseif (in_array($name, $cookies)) // check original name, e.g. coming from Commision Junction link
        {
            setrawcookie($name, rawurlencode($value), time() + 60*60*24*90, '/', '.' . $thisdomain, false, false);
        }
    }
}

add_action('admin_init', 'wp_h1p_sync_params_options_init');
add_action('admin_menu', 'wp_h1p_sync_params_adminmenu');
add_action('wp_footer', 'wp_h1p_sync_params_replace_links', 10);
add_action('wp', 'wp_h1p_sync_params_set_remote_cookies');
