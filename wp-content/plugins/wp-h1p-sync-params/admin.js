function wp_h1p_sync_params_sites_add_new_row()
{
    var new_row_val = '<br /><input name="wp_h1p_sync_params_sites[]" id="wp_h1p_sync_params_sites" type="text" class="code" value="" />';
    
    jQuery('[name="wp_h1p_sync_params_sites[]"]').last().after(new_row_val);
}

function wp_h1p_sync_params_cookies_add_new_row()
{
    var new_row_val = '<br /><input name="wp_h1p_sync_params_cookies[]" id="wp_h1p_sync_params_cookies" type="text" class="code" value="" />';
    
    jQuery('[name="wp_h1p_sync_params_cookies[]"]').last().after(new_row_val);
}

jQuery("#wp_h1p_sync_params_settings_form").submit(function()
{
    jQuery(this).find(":input").filter(function(){ return !this.value; }).attr("disabled", "disabled");
    
    return true;
});