<?php

require __DIR__ . '/../../../wp-load.php';

$cookies = get_option('wp_h1p_sync_params_cookies');
$sites = get_option('wp_h1p_sync_params_sites');
$prefix = get_option('wp_h1p_sync_params_cookie_prefix');

$existing_cookies = $_COOKIE;

$prepared_cookies = [];

foreach ($_COOKIE as $cookie_name => $cookie_value)
{
    if (!empty($prefix) && strpos($cookie_name, $prefix) === 0)
    {
        $cookie_name = preg_replace('/' . $prefix . '/', '', $cookie_name, 1);
    }

    if (in_array($cookie_name, $cookies))
        $prepared_cookies[] = [
            'name' => $prefix . $cookie_name,
            'value' => rawurlencode(stripslashes($cookie_value))
        ];
}


$additional_data = json_encode($prepared_cookies);
$sites = json_encode($sites);

?>
var additional_data = <?php echo $additional_data; ?>;
var sites = <?php echo $sites; ?>;

function modify_form_action(form, sites, additional_data)
{
    var action = jQuery(form).attr('action');
    
    for (i = 0; i < sites.length; i++)
    {
        var site = sites[i].replace(/\./g, '\\.').replace(/\-/g, '\\-');

        var pattern = '^https?\:\/\/(.*\.)?' + site + '(?![.]).*';

        var regxp = new RegExp(pattern);

        if (regxp.test(action))
        {
            for (j = 0; j < additional_data.length; j++)
            {
                jQuery('<' + 'input' + '>').attr({
                    type: 'hidden',
                    id: additional_data[j].name,
                    name: additional_data[j].name,
                    value: additional_data[j].value
                }).appendTo(form);
            }

            break;
        }
    }
}

function modify_a_href(a, sites, additional_data)
{
    var link = jQuery(a).attr('href');
    
    for (i = 0; i < sites.length; i++)
    {
        var site = sites[i].replace(/\./g, '\\.').replace(/\-/g, '\\-');
      
        var regxp = new RegExp('^https?\:\/\/(.*\.)?' + site + '(?![.]).*');
        if (regxp.test(link))

        {
            var additional_req_params = '';

            for (j = 0; j < additional_data.length; j++)
                additional_req_params += '&' + additional_data[j].name + '=' + additional_data[j].value;
            
            if (link.indexOf('?') === -1)
                additional_req_params = additional_req_params.replace('&', '?');

            jQuery(a).attr('href', link += additional_req_params);

            break;
        }
    }
}

jQuery('form').each(function(){modify_form_action(jQuery(this), sites, additional_data);});
jQuery('a').each(function(){modify_a_href(jQuery(this), sites, additional_data);});
