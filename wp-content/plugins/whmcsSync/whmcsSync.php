<?php
/**
 *
 * Plugin Name: WHMCS Synchronizer
 *
 * Plugin URI: http://www.host1plus.com
 *
 * Description: Plugin coneccts to whmcs database and gets all products prices with all currencies.
 *
 * Version: 1.4
 *
 * Author: Darius Banionis
 *
 * License:
 */

//error_reporting(0);

class whmcsSync {


    private static $instance = null;

    public static $pluginName = 'WHMCS Synchronizer';
    public static $version = '1.3';
    public static $priceList = array();
    public static $confPriceList = array();
    public static $priceList_domains = [];
    public static $priceList_addons = [];
    public static $priceList_domains_sorted = [];
    public static $stepsconfig = [];
	public static $product_ids_to_get_conf_prices = array(296, 295, 297, 298, 294, 271, 306, 307, 308, 323, 319, 321, 320, 322, 342, 344, 345, 346, 347, 348, 349, 350, 351, 356, 357, 362, 363 );

    private static $vps_gid = 37;

    public static $gtm_diff = '+2 hours';
    public static $prefix = 'whmcsSynch_';
    public static $messages = array( 'top' => [''], 'currency_select' => [''], 'cron_settings' => [''], 'whmcsapi_settings' => [''], 'whmcsdb_settings' => [''] ); // array for error messages
    public static $settings;
    private static $script_dir = __DIR__;
    private static $pricestablename, $confpricestablename, $addonspricestablename, $settingstablename, $stepsconfigtablename, $options_page_uri_name, $settings_page_url, $domainspricestablename;


    public static function getInstance() {

        if ( is_null( self::$instance ) )
            self::$instance = new whmcsSync;

        return self::$instance;
    }

    public function __construct(){

        global $wpdb;

        if(!session_id()) session_start();

        self::$pricestablename = $wpdb->prefix.self::$prefix.'prices';
        self::$confpricestablename = $wpdb->prefix.self::$prefix.'confprices';
        self::$settingstablename = $wpdb->prefix.self::$prefix.'settings';
        self::$stepsconfigtablename = $wpdb->prefix.self::$prefix.'stepsconfig';
        self::$domainspricestablename = $wpdb->prefix.self::$prefix.'domainsprices';
        self::$addonspricestablename = $wpdb->prefix.self::$prefix.'addonsprices';
        self::$options_page_uri_name = self::$prefix.'settings';

        self::$settings_page_url = admin_url().'options-general.php?page='.self::$options_page_uri_name;

        self::$settings = self::get_all_setting();

        if( isset( $_SESSION[ self::$prefix.'msgs' ] ) ){
            foreach( $_SESSION[ self::$prefix.'msgs' ] as $place => $errors ){
                if( is_array( $errors ) && is_array( self::$messages[$place] ) ) self::$messages[$place] = array_merge( self::$messages[$place], $errors );
            }
            unset( $_SESSION[ self::$prefix.'msgs' ] );
        }

    }


    /**
     * WHEN FOR SOME REASON JSON_DECODE FAILS.
     * This will remove unwanted characters.
     * Check http://www.php.net/chr for details
     *
     * @param json $json
     *
     * @return json
     */
    public static function decodeJsonString($json){


        for ($i = 0; $i <= 31; ++$i) {
            $json = str_replace(chr($i), "", $json);
        }
        $json = str_replace(chr(127), "", $json);

        // This is the most common part
        // Some file begins with 'efbbbf' to mark the beginning of the file. (binary level)
        // here we detect it and we remove it, basically it's the first 3 characters
            if (0 === strpos(bin2hex($json), 'efbbbf')) {
            $json = substr($json, 3);
        }
        $json = json_decode($json, true);
        return $json;
    }


    /**
     * Returns setup price key by cycle name
     *
     * @param string $cycle - Cycle name
     *
     * @return string
     */
    public static function getSetupCycleName($cycle){

        $cycleNames = array(

            'monthly' => 'msetupfee',
            'quarterly' => 'qsetupfee',
            'semiannually' => 'ssetupfee',
            'annually' => 'asetupfee',
            'biennially' => 'bsetupfee',
            'triennially' => 'tsetupfee'

        );

        return $cycleNames[$cycle];

    }

    /**
     * Get billing cycle name
     *
     * @param int $months - Count of months
     *
     * @return string
     */
    public static function billingCycles($months = null){

        $cycles = array(

            '1' => 'monthly',
            '3' => 'quarterly',
            '6' => 'semiannually',
            '12' => 'annually',
            '24' => 'biennially',
            '36' => 'triennially'

        );

        if(!$months){

            return $cycles;

        } else {

            return  isset($cycles[$months]) ? $cycles[$months] : null;

        }



    }

    /**
     * Get billing cycle months count
     *
     * @param string $cycle - Billing cycle name
     *
     * @return int
     */
    public static function getMonthsByCycleName($cycle){

        $cycles = array(

            'monthly' => '1' ,
            'quarterly' => '3' ,
            'semiannually' => '6',
            'annually' => '12',
            'biennially' => '24' ,
            'triennially' => '36'

        );

        return $cycles[$cycle];

    }

    /**
     * Format price in selected currency
     *
     * @param float $price - Price
     *
     * @return string
     */
    public static function formatPrice($price){

        $prefix = self::$settings['price_construct_prefix'] == 'yes' ? self::$settings['currency_prefix'] : '';
        $suffix = self::$settings['price_construct_currency'] == 'yes' ? self::$settings['currency_suffix'] : '';

        return $prefix . number_format($price, 2) . $suffix;

    }


    /**
     * Get Product Price by Cycle
     *
     * @param float $pid - Product ID
     * @param string $cycle - Cycle name
     * @param string $param - Optional. Get only specyfied param
     * @param array $prices - Optional. Price to filter
     *
     * @return array|string
     */
    public static function getPrice($pid, $cycle, $param = null, $prices = null){

        $productPrices = $prices ? $prices : self::getPriceOptions($pid);
        $cycle_name = self::billingCycles($cycle);
        $price = array();

        foreach($productPrices as $p){

            if(isset($p['cycle']) && $p['cycle'] == $cycle_name) $price = $p;

        };

        if($param){

            return isset($price[$param]) ? $price[$param] : null;

        } else {

            return $price;

        }

    }

    /**
     * Get minimal products monthly price
     *
     * @param float $pid - Product ID
     *
     * @return string
     */
    public static function getMinPrice($pid){

        $productPrices = self::getPriceOptions($pid);

        $minPrice = null;

        if( self::$vps_gid == $productPrices[0]['gid'] ){ //if vps
            $minPrice = $productPrices[0]['price_monthly_value']; //montly price of first billing cycle

        }
        else{

            foreach($productPrices as $price){

                if(isset($price['price_monthly_value']) && (!$minPrice || (float)$price['price_monthly_value'] < $minPrice) ) {

                    $minPrice = $price['price_monthly_value'];

                }

            };
        }
        return self::formatPrice($minPrice);

    }

    /**
     * Get minimal products monthly price for cloud server
     *
     * @param float $pid - Product ID
     *
     * @return string
     */
    public static function getCloudMinPrice( $location ){

        $plan_lin = array_keys( self::get_local_stepsconfig('cloud_plans')['linux'][$location]['plans'] )[0];
        $plan_win = array_keys( self::get_local_stepsconfig('cloud_plans')['windows'][$location]['plans'] )[0];

        $price_lin = (float)self::getCloudConfigurablePrice( 'linux' , $location, $plan_lin )[0]['price_value'];
        $price_win = (float)self::getCloudConfigurablePrice( 'windows' , $location, $plan_win )[0]['price_value'];

        $minPrice = null;

        if( $price_lin < $price_win ){
            $minPrice = $price_lin;
        }
        else{
            $minPrice = $price_win;
        }

        return self::formatPrice($minPrice);

    }

    /**
     * Get aviailable product price options by billing cycles
     *
     * @param int $pid - Product ID
     * @param string $period - Period name. If param is provided method returns only specific period price options
     * @param bool $setup - Include setup price
     *
     * @return array
     */
    public static function getPriceOptions($pid, $period = null, $setup = true){

        $productPrices = self::get_local_pricing($pid);
        $priceOptions = array();

        if($productPrices['paytype'] == 'recurring'){

            foreach(self::billingCycles() as $months => $cycleName){

                if($productPrices[$cycleName] !== '-1.00'){

                    $setupPrice = $productPrices[self::getSetupCycleName($cycleName)];
                    $price = ($setup) ? $productPrices[$cycleName] + $setupPrice : $productPrices[$cycleName];

                    $options = array(
                        'cycle' => $cycleName,
                        'months' => $months,
                        'price_value' => number_format($price  , 2),
                        'price_monthly_value' => number_format($price/$months, 2),
                        'price_monthly' => self::formatPrice($price/$months),
                        'price' => self::formatPrice($price),
                        'setup_value' => number_format($setupPrice),
                        'setup' => self::formatPrice($setupPrice),
                        'pid' => $productPrices['pid'],
                        'gid' => $productPrices['gid']
                    );

                    if($period && $period == $cycleName){

                        return $options;

                    }

                    $priceOptions[] = $options;

                }

            }

        }

        return $priceOptions;

    }

    /**
     *  Get pricing from local db
     *
     *  @return array
     */
    public static function getAddonsPrices($addonid = null){

        if(!self::$priceList_addons || !count(self::$priceList_addons)){

            global $wpdb;

            $query = 'SELECT * FROM `'.self::$addonspricestablename.'` ORDER BY `id`';

            $db_data = $wpdb->get_results( $query, ARRAY_A );

            if( $wpdb->last_error != '' ){
                self::message( 'Can\'t get pricing list.<br/>mysqli said: '.$wpdb->last_error, 'top', 'error', true );
                return false;
            }

            if( empty( $db_data ) ){
                self::message( 'There\'s no addons pricing in local database.', 'top', 'update', true );
                return false;
            }

            foreach( $db_data as $row ){
                $prices[ $row['id'] ] = $row;
            }

            self::$priceList_addons = $prices;

        }

        if($addonid) {

            return self::$priceList_addons[$addonid];

        }

        return self::$priceList_addons;
    }

    /**
     * Get configurable price options by product (location) id
     *
     * @param float $pid - Optional. Returns specified product prices only
     *
     * @return array
     */
    public static function getConfigurablePrices($pid = null){

        if(!self::$confPriceList){

            global $wpdb;

            $query = 'SELECT * FROM `'.self::$confpricestablename.'`';

            $db_data = $wpdb->get_results($query, ARRAY_A);

            $prices = array();

            foreach($db_data as $location_prices){

                $prices[$location_prices['pid']] = $location_prices['value'];

            }

            if( $wpdb->last_error != '' ){
                self::message( 'Can\'t get configurable prices.<br/>mysqli said: '.$wpdb->last_error, 'top', 'error', true );
                return false;
            }

            if( empty($db_data) ){
                self::message( 'There\'s no configurable prices in local database.', 'top', 'update', true );
                return false;
            }

            foreach($prices as $product_id => $price_options){

                self::$confPriceList[$product_id] = self::decodeJsonString($price_options);

            }

        }

        if($pid && array_key_exists( $pid, self::$confPriceList ) && $configOptions = self::$confPriceList[$pid]['product']['configoptions']['configoption']){

            $prices = array();

            foreach($configOptions as $param){

                $prices[$param['name']] = array(
                    'id' => $param['id'],
                    'type' => $param['type'],
                    'options' => array()
                );

                foreach($param['options']['option'] as $option) {

                    $prices[$param['name']]['options'][(string)$option['name']] = array(

                        'id' => $option['id'],
                        'pricing' => $option['pricing'][self::$settings['currency_code']]

                    );

                }

            }

            return $prices;


        } else {

            return self::$confPriceList;

        }

    }

    /**
     * Calculates configurable product options to match specific value
     *
     * @param float $value - Value to be matched
     * @param array $options - Configurable options
     *
     * @return array
     */
    public static function getConfigurableOptionsKeys($value, $options){

        if(in_array($value, $options)) return [$value];

        $keys = [];

        $valueMin = (float)min($options);
        $valueLeft = (float)$value;

        while($valueLeft && $valueLeft >= $valueMin){

            $opt = 0;

            foreach($options as $option){

                if(!$opt || (float)$option <= $valueLeft && (float)$option > $opt){

                   $opt = (float)$option;

                }

            }

            $keys[] = $opt;
            $valueLeft -= $opt;

        }

        return $keys;

    }

    /**
     * Get configurable product price by configuration
     *
     * @params int $pid - product ID
     * @params array $config - product configuration options
     *
     * @return array
     */
    public static function getConfigurablePrice($pid, $config){

        $productPrices = self::getPriceOptions($pid);
        $configPrices = self::getConfigurablePrices($pid);

        foreach($productPrices as $priceIndex => $priceCycle){

            $productPrices[$priceIndex]['price_value'] = (float)$productPrices[$priceIndex]['price_value'];


            foreach($config as $params){

                $confPrice = 0;
                $confSetupPrice = 0;

                // if($priceCycle['cycle'] == 'monthly') var_dump($configPrices[$params['key']], '--------------');

                if($configPrices[$params['key']]['type'] == 4){

                    $config_step_val = key($configPrices[$params['key']]['options']);
                    $config_val = $config_step_val * ceil($params['value']/$config_step_val);

                    $confPrice = $configPrices[$params['key']]['options'][$config_step_val]['pricing'][$priceCycle['cycle']] * ceil($config_val/$config_step_val);
                    $confSetupPrice = $configPrices[$params['key']]['options'][$config_step_val]['pricing'][self::getSetupCycleName($priceCycle['cycle'])];

                } else {

                    $config_step_val = $params['value'];
                    $config_options_keys = self::getConfigurableOptionsKeys($config_step_val, array_keys($configPrices[$params['key']]['options']));

                    if($config_options_keys){

                        foreach($config_options_keys as $config_options_key){

                            if(isset($configPrices[$params['key']]['options'][$config_options_key])){

                                $confPrice += $configPrices[$params['key']]['options'][$config_options_key]['pricing'][$priceCycle['cycle']];
                                $confSetupPrice += $configPrices[$params['key']]['options'][$config_options_key]['pricing'][self::getSetupCycleName($priceCycle['cycle'])];

                            }

                        }


                    }

                }

                $productPrices[$priceIndex]['price_value'] +=  ((float)$confPrice + (float)$confSetupPrice);

            }

            $productPrices[$priceIndex]['price'] = self::formatPrice($productPrices[$priceIndex]['price_value']);
            $productPrices[$priceIndex]['price_monthly_value'] = number_format($productPrices[$priceIndex]['price_value']/self::getMonthsByCycleName($priceCycle['cycle']), 2);
            $productPrices[$priceIndex]['price_monthly'] = self::formatPrice($productPrices[$priceIndex]['price_value']/self::getMonthsByCycleName($priceCycle['cycle']));
            $productPrices[$priceIndex]['price_value'] = number_format($productPrices[$priceIndex]['price_value'], 2);

        }

        return $productPrices;

    }

    /**
     * Get cloud configurable product price
     *
     * @params int $os - os name linux|windows
     * @params array $location - location: germany, us_east .. etc..
     *
     * @return array
     */
    public static function getCloudConfigurablePrice($os, $location, $plan_name){

        $cloud_config_opts_ids_by_pid = self::get_local_stepsconfig('cloud_config_opts_ids_by_pid');
        $cloud_base_values = self::get_local_stepsconfig('cloud_base_values');
        $cloud_plans = self::get_local_stepsconfig('cloud_plans');

        $pid = $cloud_plans[ $os ][ $location ]['pid'];
        $copt_ids = $cloud_config_opts_ids_by_pid[ $pid ];
        $custom_plan = $cloud_plans[ $os ][ $location ]['plans'][strtoupper($plan_name)];

        $productPrices = self::getPriceOptions($pid);
        $configPrices = self::getConfigurablePrices($pid);

        //var_dump( $cloud_config_opts_ids_by_pid );
        //var_dump( $cloud_base_values );
        //var_dump( $copt_ids );

        foreach($productPrices as $priceIndex => $priceCycle){

            $productPrices[$priceIndex]['price_value'] = (float)$productPrices[$priceIndex]['price_value'];

            foreach($copt_ids as $param_key => $param_id){

                $confPrice = 0;
                $confSetupPrice = 0;

                $copt_type = 1;
                $copt_key = '';
                foreach( $configPrices as $key => $value ){
                    if( $value['id'] == $param_id ){
                        $copt_type = $value['type'];
                        $copt_key = $key;
                        break;
                    }
                }
                if($copt_type == 4){
                    $config_val = $custom_plan['options'][ $param_key ];
                    $options_pricing = reset( $configPrices[$copt_key]['options'] ) ['pricing'];
                    $confPrice = (float)$options_pricing[$priceCycle['cycle']] * $config_val;
                    $confSetupPrice = $options_pricing[self::getSetupCycleName($priceCycle['cycle'])];
                    //var_dump( $config_val );
                }

                $productPrices[$priceIndex]['price_value'] +=  ((float)$confPrice + (float)$confSetupPrice);

            }

            $productPrices[$priceIndex]['price'] = self::formatPrice($productPrices[$priceIndex]['price_value']);
            $productPrices[$priceIndex]['price_monthly_value'] = number_format($productPrices[$priceIndex]['price_value']/self::getMonthsByCycleName($priceCycle['cycle']), 2);
            $productPrices[$priceIndex]['price_monthly'] = self::formatPrice($productPrices[$priceIndex]['price_value']/self::getMonthsByCycleName($priceCycle['cycle']));
            $productPrices[$priceIndex]['price_value'] = number_format($productPrices[$priceIndex]['price_value'], 2);

        }

        //var_dump( $productPrices );

        return $productPrices;

    }


    /**
     * Get configurable product param & value ids
     *
     * @params int $pid - product ID
     * @params array $config - product configuration options
     *
     * @return array
     */
    public static function getProductConfigIds($pid, $config){

        $configPrices = self::getConfigurablePrices($pid);

        if($configPrices){

            foreach($config as $cfg_key => $cfg){

                if(isset($configPrices[$cfg['key']]) && isset($configPrices[$cfg['key']]['options'][$cfg['value']])){

                    $config[$cfg_key]['id'] = $configPrices[$cfg['key']]['id'];
                    $config[$cfg_key]['value_id'] = $configPrices[$cfg['key']]['options'][$cfg['value']]['id'];

                }

            }

        }

        return $config;

    }


    /**
     * Get WHMCS Clients count
     *
     * @return int
     */
    public static function getClientsCount(){

        return self::$settings['whmcs_clients_count'];

    }


    /**
     * Get domains pricing
     *
     * @params string $tld - Return specific tld prices only
     *
     * @return array
     */
    public static function getDomainsPrices( $tld = '' ) {
        self::$priceList_domains = self::get_local_pricing_domains();
        if( !empty( $tld ) ){
            if( array_key_exists( $tld, self::$priceList_domains ) )
                return self::$priceList_domains[ $tld ]['prices'];
            else return false;
        }

        if( !empty( self::$priceList_domains_sorted ) )
            return self::$priceList_domains_sorted;
        else{

            foreach( self::$priceList_domains as $tld => $tinfo ){
                $key = $tinfo['domain_order'];
                self::$priceList_domains_sorted[ $key ]['tld'] = $tld;
                self::$priceList_domains_sorted[ $key ]['prices'] = $tinfo['prices'];
            }
            ksort( self::$priceList_domains_sorted );
            return self::$priceList_domains_sorted;
        }
        return self::$priceList_domains;
    }


    /**
     * Trigger to activate wordpress plugin
     */
    public static function activatePlugin() {
        self::createTablesStructure();
    }


    /**
     * Creates initial db structure for plugin
     */
    private static function createTablesStructure() {
        global $wpdb;

        #settings table
        $wpdb->query( 'SHOW TABLES LIKE "'.self::$settingstablename.'"' );

        if( $wpdb->num_rows < 1 ){

            $settings_sql = 'CREATE TABLE '.self::$settingstablename.' (
                            `setting` varchar(100) NOT NULL,
                            `value` varchar(200),
                            UNIQUE KEY setting (setting)
                            ) CHARACTER SET=utf8 ;';

            $wpdb->query( $settings_sql );
            if( $wpdb->last_error != '' ){
                $dberror = $wpdb->last_error; //bcz query below overrides error
                self::error('Can\'t cant create table: '.self::$settingstablename."<br/> mysqli said: ".$dberror );
            }

            $wpdb->query( 'INSERT INTO `'.self::$settingstablename.'` (`setting`,`value`)
                    VALUES ( "currency", "1" ),
                    ( "currency_code", "" ),
                    ( "currency_prefix", "" ),
                    ( "currency_suffix", "" ),
                    ( "last_update", "never" ),
                    ( "last_update_confprices", "never" ),
                    ( "last_update_domains", "never" ),
                    ( "last_update_stepsconfig", "never" ),
                    ( "cron_schedule", "off" ),
                    ( "price_construct_prefix", "yes" ),
                    ( "price_construct_cycle", "monthly" ),
                    ( "price_construct_currency", "yes" ),
                    ( "whmcs_api_url", "" ),
                    ( "whmcs_api_url_custom", "" ),
                    ( "whmcs_clients_count", "0" ),
                    ( "whmcs_api_user", "" ),
                    ( "whmcs_api_pass", "" )
                    ' );
                    if( $wpdb->last_error != '' ){
                        $dberror = $wpdb->last_error; //bcz query below overrides error
                        $wpdb->query( 'DROP TABLE '.self::$settingstablename );
                        self::error('Can\'t insert data to: '.self::$settingstablename."<br/> mysqli said: ".$dberror );
                    }
        }

        #prices table
        $wpdb->query( 'SHOW TABLES LIKE "'.self::$pricestablename.'"' );
        if( $wpdb->num_rows < 1 ){

            $prices_sql = 'CREATE TABLE '.self::$pricestablename.' (
                            `pid` int NOT NULL,
                            `gid` int NOT NULL,
                            `product` varchar(200) NOT NULL,
                            `paytype` varchar(30) NOT NULL,
                            `currency` varchar(5) NOT NULL,
                            `prefix` varchar(5) NOT NULL,
                            `monthly` decimal(10,2) NOT NULL,
                            `quarterly` decimal(10,2) NOT NULL,
                            `semiannually` decimal(10,2) NOT NULL,
                            `annually` decimal(10,2) NOT NULL,
                            `biennially` decimal(10,2) NOT NULL,
                            `triennially` decimal(10,2) NOT NULL,
                            `msetupfee` decimal(10,2) NOT NULL,
                            `qsetupfee` decimal(10,2) NOT NULL,
                            `ssetupfee` decimal(10,2) NOT NULL,
                            `asetupfee` decimal(10,2) NOT NULL,
                            `bsetupfee` decimal(10,2) NOT NULL,
                            `tsetupfee` decimal(10,2) NOT NULL,
                            UNIQUE KEY pid (pid)
                            ) CHARACTER SET=utf8 ;';
            $wpdb->query( $prices_sql );

            if( $wpdb->last_error != '' ){
                $dberror = $wpdb->last_error; //bcz query below overrides error
                $wpdb->query( 'DROP TABLE '.self::$settingstablename );
                self::error( 'Can\'t create table: '.self::$pricestablename."<br/> mysqli said: ".$dberror );
            }
        }

        #configurable products prices table
        $wpdb->query( 'SHOW TABLES LIKE "'.self::$confpricestablename.'"' );
        if( $wpdb->num_rows < 1 ){

            $conf_prices_sql = 'CREATE TABLE '.self::$confpricestablename.' (
                            `pid` int NOT NULL,
                            `value` longtext NOT NULL,
                            UNIQUE KEY pid (pid)
                            ) CHARACTER SET=utf8 ;';

            $wpdb->query( $conf_prices_sql );

            if( $wpdb->last_error != '' ){
                $dberror = $wpdb->last_error; //bcz query below overrides error
                $wpdb->query( 'DROP TABLE '.self::$confpricestablename );
                self::error( 'Can\'t create table: '.self::$confpricestablename."<br/> mysqli said: ".$dberror );
            }
        }

        #steps config
        $wpdb->query( 'SHOW TABLES LIKE "'.self::$stepsconfigtablename.'"' );
        if( $wpdb->num_rows < 1 ){

            $steps_sql = 'CREATE TABLE '.self::$stepsconfigtablename.' (
                            `what` varchar(100) NOT NULL,
                            `data` text NOT NULL,
                            UNIQUE KEY what (what)
                            ) CHARACTER SET=utf8 ;';
            $wpdb->query( $steps_sql );

            if( $wpdb->last_error != '' ){
                $dberror = $wpdb->last_error; //bcz query below overrides error
                $wpdb->query( 'DROP TABLE '.self::$pricestablename );
                $wpdb->query( 'DROP TABLE '.self::$settingstablename );
                self::error( 'Can\'t create table: '.self::$stepsconfigtablename."<br/> mysqli said: ".$dberror );
            }
        }

        #addons prices
        $wpdb->query( 'SHOW TABLES LIKE "'.self::$addonspricestablename.'"' );
        if( $wpdb->num_rows < 1 ){

            $steps_sql = 'CREATE TABLE '.self::$addonspricestablename.' (
                            `id` int(11) NOT NULL,
                            `name` varchar(100) NOT NULL,
                            `price` decimal(10,2) NOT NULL,
                            `billingcycle` varchar(100) NOT NULL,
                            UNIQUE KEY id (id)
                            ) CHARACTER SET=utf8 ;';
            $wpdb->query( $steps_sql );

            if( $wpdb->last_error != '' ){
                $dberror = $wpdb->last_error; //bcz query below overrides error
                self::error( 'Can\'t create table: '.self::$addonspricestablename."<br/> mysqli said: ".$dberror );
            }
        }

        #prices table (domains)
        $wpdb->query( 'SHOW TABLES LIKE "'.self::$domainspricestablename.'"' );
        if( $wpdb->num_rows < 1 ){

            $prices_sql = 'CREATE TABLE '.self::$domainspricestablename.' (
                            `tld` varchar(20) NOT NULL,
                            `currency` varchar(5) NOT NULL,
                            `prefix` varchar(5) NOT NULL,
                            `y1` decimal(10,2) NOT NULL,
                            `y2` decimal(10,2) NOT NULL,
                            `y3` decimal(10,2) NOT NULL,
                            `y4` decimal(10,2) NOT NULL,
                            `y5` decimal(10,2) NOT NULL,
                            `y6` decimal(10,2) NOT NULL,
                            `y7` decimal(10,2) NOT NULL,
                            `y8` decimal(10,2) NOT NULL,
                            `y9` decimal(10,2) NOT NULL,
                            `y10` decimal(10,2) NOT NULL,
                            `domain_order` tinyint NOT NULL,
                            `t1` decimal(10,2) NOT NULL,
                            `t2` decimal(10,2) NOT NULL,
                            `t3` decimal(10,2) NOT NULL,
                            `t4` decimal(10,2) NOT NULL,
                            `t5` decimal(10,2) NOT NULL,
                            `t6` decimal(10,2) NOT NULL,
                            `t7` decimal(10,2) NOT NULL,
                            `t8` decimal(10,2) NOT NULL,
                            `t9` decimal(10,2) NOT NULL,
                            `t10` decimal(10,2) NOT NULL,
                            `r1` decimal(10,2) NOT NULL,
                            `r2` decimal(10,2) NOT NULL,
                            `r3` decimal(10,2) NOT NULL,
                            `r4` decimal(10,2) NOT NULL,
                            `r5` decimal(10,2) NOT NULL,
                            `r6` decimal(10,2) NOT NULL,
                            `r7` decimal(10,2) NOT NULL,
                            `r8` decimal(10,2) NOT NULL,
                            `r9` decimal(10,2) NOT NULL,
                            `r10` decimal(10,2) NOT NULL,
                            UNIQUE KEY tld (tld)
                            ) CHARACTER SET=utf8 ;';
            $wpdb->query( $prices_sql );

            if( $wpdb->last_error != '' ){
                $dberror = $wpdb->last_error; //bcz query below overrides error
                $wpdb->query( 'DROP TABLE '.self::$pricestablename );
                $wpdb->query( 'DROP TABLE '.self::$settingstablename );
                self::error( 'Can\'t create table: '.self::$domainspricestablename."<br/> mysqli said: ".$dberror );
            }
        }
    }


    /**
     * Plugin deactivation
     */
    public static function deactivatePlugin() {

        if( wp_next_scheduled( self::$prefix.'task_hook' ) ){ //if cron exist remove it
            wp_clear_scheduled_hook( self::$prefix.'task_hook' );
        }
    }


    /**
     * Uninstall plugin
     */
    public static function uninstallPlugin() {
        self::dropTables();
    }


    /**
     * Remove all plugins db tables
     */
    private static function dropTables() {
        global $wpdb;

        $wpdb->query( 'DROP TABLE '.self::$settingstablename.', '.self::$addonspricestablename.', '.self::$pricestablename.', '.self::$stepsconfigtablename.', '.self::$domainspricestablename.', '.self::$confpricestablename ) or die( $wpdb->last_error.'; query: '.$wpdb->last_query ); //delete tables

    }


    /**
     * Displays error message and exists script execution
     *
     * @params string $message - message to display
     * @params string $errno - error code
     */
    private static function error( $message, $errno = E_USER_ERROR ) { //trigger E_USER type error

        if(isset($_GET['action']) && $_GET['action'] == 'error_scrape'){
            echo '<strong>' . $message . '</strong>';
            exit;
        }
        else trigger_error($message, $errno);

    }


    /**
     * Displays custom message
     *
     * @params string $message - message to display
     * @params string $place - Optional. Default = 'top'. Message position
     * @params string $type - Optional. Default = 'update'. Message type. Available options: success, error, update
     * @params bool $direct - Optional. Default = false
     */
    public static function message( $message, $place = 'top', $type = 'update', $direct = false ) { // $places - place to put messages [top,currency_select,cron_settings,msg_whmcsapi_settings,msg_whmcsdb_settings], direct, if need to add directly, not in session

        switch( $type ):
            case 'success': $class = 'success'; break;
            case 'error': $class = 'error'; break;
            default: $class = 'updated'; break;
        endswitch;

        $message = '<div class="'.$class.'" style="margin:10px auto;padding:3px;">'.$message.'</div>';

        if( $direct ) self::$messages[ $place ][] = $message;
        else
            $_SESSION[ self::$prefix.'msgs' ][ $place ][] = $message;

    }


    /**
     * Creates wp-admin settings menu option
     */
    public static function create_menu() {
        add_options_page( self::$pluginName, self::$pluginName, 'manage_options', self::$options_page_uri_name, array( __CLASS__, 'settingsPage' ));
    }


    /**
     * Creates plugin setting page
     */
    public static function settingsPage() {

        self::$priceList = self::get_local_pricing();
        self::$priceList_domains = self::get_local_pricing_domains();
        self::$stepsconfig = self::get_local_stepsconfig();

        if( !empty( self::$settings['whmcs_api_user'] ) && !empty( self::$settings['whmcs_api_pass'] ) && !empty( self::$settings['whmcs_api_url'] ) ){ // whmcs api provided
            $currencies = self::get_whmcs_currencies_api();
            $currencies_count = count( $currencies );
            if( $currencies_count < 1 ) self::message( 'Cant get currencies list via WHMCS API', 'currency_select', 'error', true );
        }

        if( !empty( self::$priceList ) )
            foreach( self::$priceList as $pr )
                $prices_by_gid[ $pr['gid'] ][] = $pr;
        else
            $prices_by_gid = array( array() );

        $priceList_domains = !empty( self::$priceList_domains ) ? self::$priceList_domains : [];

        include_once( self::$script_dir.'/settings_tpl.php' );
    }

    /**
     * Get plugin settings
     */
    private static function get_all_setting(){
        global $wpdb;

        $settings_array = array();

        $wparray = $wpdb->get_results( 'SELECT * FROM `'.self::$settingstablename.'`', ARRAY_A );
        foreach( $wparray as $setting_row )
            $settings_array[ $setting_row['setting'] ] = (string)$setting_row['value'] ;

        return $settings_array;
    }


    /**
     * Update plugin settings
     *
     * @param string $setting - setting key
     * @param string $value - new value
     */
    public static function change_setting( $setting, $value ){
        global $wpdb;

        $wpdb->escape_by_ref( $setting ); //var by ref
        $wpdb->escape_by_ref( $value ); //var by ref

        $wpdb->update( self::$settingstablename, array( 'value' => $value ), array( 'setting' => $setting ));

    }

    /**
     *  Get currencies by the whmcs api call
     *
     *  @return object
     */
    public static function get_whmcs_currencies(){

        $currencies = array();

        $postfields['username'] = self::$settings['whmcs_api_user'];
        $postfields['password'] = md5(self::$settings['whmcs_api_pass']);
        $postfields["action"] = "getcurrencies";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::$settings['whmcs_api_url']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields) );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch);
        curl_close($ch);

        if( $result && !empty( $result ) ){

            if( strpos( $result, 'result=error;' ) !== false ){
                self::message( 'WHMCS API returns error:'.$result, 'currency_select', 'update', true );
                return false;
            }


            if( self::_isValidXML( $result ) ){
                $result = new SimpleXMLElement($result);

                if (!empty( $result->currencies->currency[0]->id)) {

                    foreach( $result->currencies->currency as $currency ){
                        $currencies[(string)$currency->id] = $currency;
                    }

                } else {

                    self::message( 'WHMCS API missing data.', 'currency_select', 'update', true );
                    return false;

                }
            }
            else{
                self::message( 'WHMCS API returns unexpected xml.', 'currency_select', 'update', true );
                return false;
            }
        }

        if(isset($currencies[whmcsSync::$settings['currency']])){
            whmcsSync::change_setting( 'currency_code', $currencies[whmcsSync::$settings['currency']]->code );
            whmcsSync::change_setting( 'currency_prefix', $currencies[whmcsSync::$settings['currency']]->prefix );
            whmcsSync::change_setting( 'currency_suffix', $currencies[whmcsSync::$settings['currency']]->suffix );
        }
        
        return $currencies;
    }


    /**
     *  Get currencies by the whmcs api call
     *
     *  @return array
     */
    public static function get_whmcs_currencies_api(){

        $options = array();

        if($currencies = self::get_whmcs_currencies()){

            foreach( $currencies as $id => $currency ){
                $options[(int)$id] = (string)$currency->code;
            }

        };

        return $options;

    }



    /**
     *  Get prices from whmcs database
     *
     *  @return array
     */
    public static function get_whmcs_pricing_api(){

        $postfields['username'] = self::$settings['whmcs_api_user'];
        $postfields['password'] = md5(self::$settings['whmcs_api_pass']);
        $postfields["action"] = "H1PgetPriceList";
        $postfields["currency"] = self::$settings['currency'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::$settings['whmcs_api_url_custom']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields) );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch);
        curl_close($ch);

        if( $result && !empty( $result ) ){

            if( strpos( $result, 'result=error;' ) !== false ){
                self::message( 'WHMCS API returns error:'.$result, 'H1PgetPriceList', 'update', true );
                return;
            }

            $result = simplexml_load_string($result);

            if ( !empty( $result->products ) ) {


                foreach( $result->products->product as $product ){
                    $pid = (int)$product->id;

                    $products[ $pid ]['gid'] = (int)$product->gid;
                    $products[ $pid ]['name'] = (string)$product->name;
                    $products[ $pid ]['paytype'] = (string)$product->paytype;
                    $products[ $pid ]['currency'] = (string)$product->currency;
                    $products[ $pid ]['prefix'] = (string)$product->prefix;

                    $products[ $pid ]['monthly'] = (float)$product->monthly;
                    $products[ $pid ]['quarterly'] = (float)$product->quarterly;
                    $products[ $pid ]['semiannually'] = (float)$product->semiannually;
                    $products[ $pid ]['annually'] = (float)$product->annually;
                    $products[ $pid ]['biennially'] = (float)$product->biennially;
                    $products[ $pid ]['triennially'] = (float)$product->triennially;

                    $products[ $pid ]['msetupfee'] = (float)$product->msetupfee;
                    $products[ $pid ]['qsetupfee'] = (float)$product->qsetupfee;
                    $products[ $pid ]['ssetupfee'] = (float)$product->ssetupfee;
                    $products[ $pid ]['asetupfee'] = (float)$product->asetupfee;
                    $products[ $pid ]['bsetupfee'] = (float)$product->bsetupfee;
                    $products[ $pid ]['tsetupfee'] = (float)$product->tsetupfee;
                }

            } else self::message( 'WHMCS API returns unexpected xml.', 'H1PgetPriceList', 'update', true );
        }

        return $products;
    }

/**
 *  Get prices from whmcs database
 *
 *  @return array
 */
public static function get_whmcs_addons_pricing_api(){
    global $wpdb;

    #addons prices
    $wpdb->query( 'SHOW TABLES LIKE "'.self::$addonspricestablename.'"' );
    if( $wpdb->num_rows < 1 ){

        $steps_sql = 'CREATE TABLE '.self::$addonspricestablename.' (
                        `id` int(11) NOT NULL,
                        `name` varchar(100) NOT NULL,
                        `price` decimal(10,2) NOT NULL,
                        `billingcycle` varchar(100) NOT NULL,
                        UNIQUE KEY id (id)
                        ) CHARACTER SET=utf8 ;';
        $wpdb->query( $steps_sql );

        if( $wpdb->last_error != '' ){
            $dberror = $wpdb->last_error; //bcz query below overrides error
            self::error( 'Can\'t create table: '.self::$addonspricestablename."<br/> mysqli said: ".$dberror );
        }
    }

    $postfields['username'] = self::$settings['whmcs_api_user'];
    $postfields['password'] = md5(self::$settings['whmcs_api_pass']);
    $postfields["action"] = "h1pgetaddonsprices";
    $postfields["currency"] = self::$settings['currency'];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, self::$settings['whmcs_api_url_custom']);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 300);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields) );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $result = curl_exec($ch);
    curl_close($ch);

    if( $result && !empty( $result ) ){

        if( strpos( $result, 'result=error;' ) !== false ){
            self::message( 'WHMCS API returns error:'.$result, 'H1PgetPriceList', 'update', true );
            return;
        }

        $result = json_decode($result, true);

        if ( !empty( $result['addons'] ) ) {

            $wpdb->query( 'TRUNCATE TABLE `'.self::$addonspricestablename.'`;' );

            foreach( $result['addons']['addon'] as $addon ){
                $values .= '('.$addon['id'].', "'.$addon['name'].'", "'.reset($addon['pricing'])['monthly'].'", "'.$addon['billingcycle'].'"
                ),';
            }

            //remove last symbol, in this case ','
            $values = substr($values, 0, -1);

            $query = '
                INSERT INTO `'.self::$addonspricestablename.'` (`id`, `name`, `price`, `billingcycle`)
                VALUES
                '.$values.'
                ';

            $wpdb->query( $query );

            if( $wpdb->last_error != '' ){
                self::message( 'Error updating prices mysqli said: '.$wpdb->last_error, 'top', 'error' );
            }
            else self::change_setting( 'last_update', current_time( 'mysql' ) );

        }

    }
    else self::message( 'WHMCS API returns unexpected response.', 'h1pgetaddonsprices', 'update', true );

    return $products;
}


    /**
     *  Get stepsconfig from whmcs
     *
     *  @return array
     */
    public static function get_whmcs_stepsconfig(){

        $postfields['username'] = self::$settings['whmcs_api_user'];
        $postfields['password'] = md5(self::$settings['whmcs_api_pass']);
        $postfields["action"] = "H1PgetStepsconfig";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::$settings['whmcs_api_url_custom']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields) );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch);
        curl_close($ch);

        if( $result && !empty( $result ) ){

            return json_decode( $result, 1 )['data'];

        }
    }


    /**
     *  Updates local stepconfig from whmcs
     */
    public static function update_stepsconfig_to_db(){
        global $wpdb;

        $sc = self::get_whmcs_stepsconfig();

        if( empty( $sc ) ) return;

        $values = '';

        foreach( $sc as $what => $data ){
            $values .= '("'.$what.'", \''.json_encode( $data ).'\'),';
        }

        //remove last symbol, in this case ','
        $values = substr($values, 0, -1);

        $query = '
            INSERT INTO `'.self::$stepsconfigtablename.'` (`what`, `data`)
            VALUES
            '.$values.'
            ON DUPLICATE KEY
            UPDATE
            `what`=VALUES(`what`), `data`=VALUES(`data`)
            ;
            ';

        $wpdb->query( $query );

        if( $wpdb->last_error != '' ){
            self::message( 'Error updating steps data mysqli said: '.$wpdb->last_error, 'top', 'error' );
        }
        else self::change_setting( 'last_update_stepsconfig', current_time( 'mysql' ) );

    }


    /**
     *  Updates local stepconfig from whmcs source
     */
    public static function customer_count_update(){
        global $wpdb;

        $postfields['username'] = self::$settings['whmcs_api_user'];
        $postfields['password'] = md5(self::$settings['whmcs_api_pass']);
        $postfields["action"] = "H1PgetClientsCount";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::$settings['whmcs_api_url_custom']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields) );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch);
        curl_close($ch);
    /*
        echo'<pre>';
        var_dump( $result );
        exit;*/

        if( $result && !empty( $result ) ){

            whmcsSync::change_setting( 'whmcs_clients_count', json_decode( $result, 1 )['clients_count'] );

        }


    }


    /**
     *  Get prices from whmcs database
     *
     *  @return array
     */
    public static function get_whmcs_pricing_api_domains(){

        $postfields['username'] = self::$settings['whmcs_api_user'];
        $postfields['password'] = md5(self::$settings['whmcs_api_pass']);
        $postfields["action"] = "H1PgetDomainsPrices";
        $postfields["currency"] = self::$settings['currency'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::$settings['whmcs_api_url_custom']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields) );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch);
        curl_close($ch);

        if( $result && !empty( $result ) ){

            if( strpos( $result, 'result=error;' ) !== false ){
                self::message( 'WHMCS API returns error:'.$result, 'H1PgetDomainsPrices', 'update', true );
                return;
            }

            $domainPricing = [];
            $result = json_decode($result, true);

            if ( !empty( $result['domainregister'] ) ) {
                foreach( $result['domainregister'] as $tld => $domainrow ){
                    $domainPricing_r[ $tld ]['price1'] = $domainrow['prices'][1];
                    $domainPricing_r[ $tld ]['price2'] = $domainrow['prices'][2];
                    $domainPricing_r[ $tld ]['price3'] = $domainrow['prices'][3];
                    $domainPricing_r[ $tld ]['price4'] = $domainrow['prices'][4];
                    $domainPricing_r[ $tld ]['price5'] = $domainrow['prices'][5];
                    $domainPricing_r[ $tld ]['price6'] = $domainrow['prices'][6];
                    $domainPricing_r[ $tld ]['price7'] = $domainrow['prices'][7];
                    $domainPricing_r[ $tld ]['price8'] = $domainrow['prices'][8];
                    $domainPricing_r[ $tld ]['price9'] = $domainrow['prices'][9];
                    $domainPricing_r[ $tld ]['price10'] = $domainrow['prices'][10];
                    $domainPricing_r[ $tld ]['currency'] = $domainrow['currency'];
                    $domainPricing_r[ $tld ]['prefix'] = $domainrow['prefix'];
                    $domainPricing_r[ $tld ]['domain_order'] = $domainrow['domain_order'];
                }
                $domainPricing['domainregister'] = $domainPricing_r;
            } else self::message( 'WHMCS API returns unexpected xml.', 'H1PgetPriceList', 'update', true );

            if ( !empty( $result['domaintransfer'] ) ) {
                foreach( $result['domaintransfer'] as $tld => $domainrow ){
                    $domainPricing_t[ $tld ]['price1'] = $domainrow['prices'][1];
                    $domainPricing_t[ $tld ]['price2'] = $domainrow['prices'][2];
                    $domainPricing_t[ $tld ]['price3'] = $domainrow['prices'][3];
                    $domainPricing_t[ $tld ]['price4'] = $domainrow['prices'][4];
                    $domainPricing_t[ $tld ]['price5'] = $domainrow['prices'][5];
                    $domainPricing_t[ $tld ]['price6'] = $domainrow['prices'][6];
                    $domainPricing_t[ $tld ]['price7'] = $domainrow['prices'][7];
                    $domainPricing_t[ $tld ]['price8'] = $domainrow['prices'][8];
                    $domainPricing_t[ $tld ]['price9'] = $domainrow['prices'][9];
                    $domainPricing_t[ $tld ]['price10'] = $domainrow['prices'][10];
                    $domainPricing_t[ $tld ]['currency'] = $domainrow['currency'];
                    $domainPricing_t[ $tld ]['prefix'] = $domainrow['prefix'];
                    $domainPricing_t[ $tld ]['domain_order'] = $domainrow['domain_order'];
                }
                $domainPricing['domaintransfer'] = $domainPricing_t;
            } else self::message( 'WHMCS API returns unexpected xml.', 'H1PgetPriceList', 'update', true );

            if ( !empty( $result['domainrenew'] ) ) {
                foreach( $result['domainrenew'] as $tld => $domainrow ){
                    $domainPricing_r[ $tld ]['price1'] = $domainrow['prices'][1];
                    $domainPricing_r[ $tld ]['price2'] = $domainrow['prices'][2];
                    $domainPricing_r[ $tld ]['price3'] = $domainrow['prices'][3];
                    $domainPricing_r[ $tld ]['price4'] = $domainrow['prices'][4];
                    $domainPricing_r[ $tld ]['price5'] = $domainrow['prices'][5];
                    $domainPricing_r[ $tld ]['price6'] = $domainrow['prices'][6];
                    $domainPricing_r[ $tld ]['price7'] = $domainrow['prices'][7];
                    $domainPricing_r[ $tld ]['price8'] = $domainrow['prices'][8];
                    $domainPricing_r[ $tld ]['price9'] = $domainrow['prices'][9];
                    $domainPricing_r[ $tld ]['price10'] = $domainrow['prices'][10];
                    $domainPricing_r[ $tld ]['currency'] = $domainrow['currency'];
                    $domainPricing_r[ $tld ]['prefix'] = $domainrow['prefix'];
                    $domainPricing_r[ $tld ]['domain_order'] = $domainrow['domain_order'];
                }
                $domainPricing['domainrenew'] = $domainPricing_r;
            } else self::message( 'WHMCS API returns unexpected xml.', 'H1PgetPriceList', 'update', true );
        }

        return $domainPricing;
    }


    /**
     *  Get pricing from local db
     *
     *  @return array
     */
    public static function get_local_stepsconfig($plan = null){

        if(!self::$stepsconfig){

            global $wpdb;

            $query = 'SELECT * FROM `'.self::$stepsconfigtablename.'`';

            $db_data = $wpdb->get_results( $query, ARRAY_A );

            if( $wpdb->last_error != '' ){
                self::message( 'Can\'t get steps config list.<br/>mysqli said: '.$wpdb->last_error, 'top', 'error', true );
                return false;
            }

            if( empty( $db_data ) ){
                self::message( 'There\'s no steps config in local database.', 'top', 'update', true );
                return false;
            }

            $sc = [];

            foreach( $db_data as $row ){

                $sc[ $row['what'] ] = json_decode( $row['data'], true );

            }

            self::$stepsconfig = $sc;

        }


        if(isset($plan)) {

            return isset(self::$stepsconfig[$plan]) ? self::$stepsconfig[$plan] : array();

        } else {

            return self::$stepsconfig;

        }

    }


    /**
     *  Get pricing from local db
     *
     *  @return array
     */
    public static function get_local_pricing($pid = null){

        if(!self::$priceList || !count(self::$priceList)){

            global $wpdb;

            $query = 'SELECT * FROM `'.self::$pricestablename.'` ORDER BY `gid`, `pid`';

            $db_data = $wpdb->get_results( $query, ARRAY_A );

            if( $wpdb->last_error != '' ){
                self::message( 'Can\'t get pricing list.<br/>mysqli said: '.$wpdb->last_error, 'top', 'error', true );
                return false;
            }

            if( empty( $db_data ) ){
                self::message( 'There\'s no pricing in local database.', 'top', 'update', true );
                return false;
            }

            foreach( $db_data as $row ){
                $prices[ $row['pid'] ] = $row;
                $prices[ $row['pid'] ]['price'] = '';

                $sep_price = explode( '.', $row[ self::$settings['price_construct_cycle'] ] );

                $prices[ $row['pid'] ]['main'] = $sep_price[0]; // before point
                $prices[ $row['pid'] ]['sub']  = $sep_price[1]; // after point

                if( self::$settings['price_construct_prefix'] == 'yes' ) $prices[ $row['pid'] ]['price'] .= $row['prefix'];

                if( $row['paytype'] == 'recurring' )
                    if( self::$settings['price_construct_cycle'] == 'monthly' ) $prices[ $row['pid'] ]['price'] .= $row['monthly'];
                    else $prices[ $row['pid'] ]['price'] .= $row['annually'];
                else
                    $prices[ $row['pid'] ]['price'] .= $row['monthly'];

                if( self::$settings['price_construct_currency'] == 'yes' ) $prices[ $row['pid'] ]['price'] .= $row['currency'];
            }

            self::$priceList = $prices;

        }

        if($pid) {

            return self::$priceList[$pid];

        }

        return self::$priceList;
    }


    /**
     *  Get pricing from local db
     *
     *  @return array
     */
    public static function get_local_pricing_domains(){ //

        global $wpdb;

        $prices = [];

        $query = 'SELECT * FROM `'.self::$domainspricestablename.'` ORDER BY `tld`';

        $db_data = $wpdb->get_results( $query, ARRAY_A );

        if( $wpdb->last_error != '' ){
            self::message( 'Can\'t get + pricing list.<br/>mysqli said: '.$wpdb->last_error, 'top', 'error', true );
            return false;
        }

        if( empty( $db_data ) ){
            self::message( 'There\'s no domains pricing in local database.', 'top', 'update', true );
            return false;
        }

        foreach( $db_data as $row ){

            $prices[ $row['tld'] ]['domain_order'] = $row[ 'domain_order' ];

            for( $i=1; $i<=10; $i++ ){
                $domain_price = $row[ 'y'.$i ];

                if( $domain_price <= 0 ) continue;

                $sep_price = explode( '.', $domain_price );

                $prices[ $row['tld'] ]['prices']['register'][ $i ]['main'] = $sep_price[0]; // before point
                $prices[ $row['tld'] ]['prices']['register'][ $i ]['sub']  = $sep_price[1]; // after point

                $prices[ $row['tld'] ]['prices']['register'][ $i ]['price'] = '';
                if( self::$settings['price_construct_prefix'] == 'yes' ) $prices[ $row['tld'] ]['prices']['register'][ $i ]['price'] .= $row['prefix'];

                $prices[ $row['tld'] ]['prices']['register'][ $i ]['price'] .= $domain_price;

                if( self::$settings['price_construct_currency'] == 'yes' ) $prices[ $row['tld'] ]['prices']['register'][ $i ]['price'] .= $row['currency'];
            }
            for( $i=1; $i<=10; $i++ ){
                $domain_price = $row[ 't'.$i ];

                if( $domain_price <= 0 ) continue;

                $sep_price = explode( '.', $domain_price );

                $prices[ $row['tld'] ]['prices']['transfer'][ $i ]['main'] = $sep_price[0]; // before point
                $prices[ $row['tld'] ]['prices']['transfer'][ $i ]['sub']  = $sep_price[1]; // after point

                $prices[ $row['tld'] ]['prices']['transfer'][ $i ]['price'] = '';
                if( self::$settings['price_construct_prefix'] == 'yes' ) $prices[ $row['tld'] ]['prices']['transfer'][ $i ]['price'] .= $row['prefix'];

                $prices[ $row['tld'] ]['prices']['transfer'][ $i ]['price'] .= $domain_price;

                if( self::$settings['price_construct_currency'] == 'yes' ) $prices[ $row['tld'] ]['prices']['transfer'][ $i ]['price'] .= $row['currency'];
            }
            for( $i=1; $i<=10; $i++ ){
                $domain_price = $row[ 'r'.$i ];

                if( $domain_price <= 0 ) continue;

                $sep_price = explode( '.', $domain_price );

                $prices[ $row['tld'] ]['prices']['renew'][ $i ]['main'] = $sep_price[0]; // before point
                $prices[ $row['tld'] ]['prices']['renew'][ $i ]['sub']  = $sep_price[1]; // after point

                $prices[ $row['tld'] ]['prices']['renew'][ $i ]['price'] = '';
                if( self::$settings['price_construct_prefix'] == 'yes' ) $prices[ $row['tld'] ]['prices']['renew'][ $i ]['price'] .= $row['prefix'];

                $prices[ $row['tld'] ]['prices']['renew'][ $i ]['price'] .= $domain_price;

                if( self::$settings['price_construct_currency'] == 'yes' ) $prices[ $row['tld'] ]['prices']['renew'][ $i ]['price'] .= $row['currency'];
            }

            if( !isset($prices[ $row['tld'] ]['prices']) || !is_array( $prices[ $row['tld'] ]['prices'] ) ){
                unset( $prices[ $row['tld'] ] );
            }
        }

        return $prices;
    }



    /**
     *  Update prices to local database
     *
     *  @return array
     */
    public static function update_whmcs_prices(){ //
        global $wpdb;

        $products = self::get_whmcs_pricing_api();

        if( empty( $products ) ) return;

        $values = '';

        foreach( $products as $pid => $product ){
            $values .= '('.$pid.', '.$product['gid'].', "'.$product['name'].'", "'.$product['paytype'].'", "'.$product['currency'].'", "'.$product['prefix'].'",
            '.$product['monthly'].', '.$product['quarterly'].','.$product['semiannually'].', '.$product['annually'].','.$product['biennially'].', '.$product['triennially'].',
            '.$product['msetupfee'].', '.$product['qsetupfee'].','.$product['ssetupfee'].', '.$product['asetupfee'].','.$product['bsetupfee'].', '.$product['tsetupfee'].'
            ),';
        }

        //remove last symbol, in this case ','
        $values = substr($values, 0, -1);

        $query = '
            INSERT INTO `'.self::$pricestablename.'` (`pid`, `gid`, `product`, `paytype`, `currency`, `prefix`,
            `monthly`, `quarterly`, `semiannually`, `annually`, `biennially`, `triennially`,
            `msetupfee`, `qsetupfee`, `ssetupfee`, `asetupfee`, `bsetupfee`, `tsetupfee`
            )
            VALUES
            '.$values.'
            ON DUPLICATE KEY
            UPDATE
            `pid`=VALUES(`pid`), `gid`=VALUES(`gid`), `product`=VALUES(`product`), `paytype`=VALUES(`paytype`), `currency`=VALUES(`currency`), `prefix`=VALUES(`prefix`),
            `monthly`=VALUES(`monthly`),
            `quarterly`=VALUES(`quarterly`),
            `semiannually`=VALUES(`semiannually`),
            `annually`=VALUES(`annually`),
            `biennially`=VALUES(`biennially`),
            `triennially`=VALUES(`triennially`),

            `msetupfee`=VALUES(`msetupfee`),
            `qsetupfee`=VALUES(`qsetupfee`),
            `ssetupfee`=VALUES(`ssetupfee`),
            `asetupfee`=VALUES(`asetupfee`),
            `bsetupfee`=VALUES(`bsetupfee`),
            `tsetupfee`=VALUES(`tsetupfee`)
            ;
            ';

        $wpdb->query( $query );

        if( $wpdb->last_error != '' ){
            self::message( 'Error updating prices mysqli said: '.$wpdb->last_error, 'top', 'error' );
        }
        else self::change_setting( 'last_update', current_time( 'mysql' ) );

        self::get_whmcs_addons_pricing_api();

    }


    /**
     *  Update configurable products prices
     */
    public static function update_whmcs_confprices(){

        global $wpdb;

        $product_ids = self::$product_ids_to_get_conf_prices;
        $postfields = [];
        $postfields['username'] = self::$settings['whmcs_api_user'];
        $postfields['password'] = md5(self::$settings['whmcs_api_pass']);
        $postfields["action"] = "h1pgetproducts";

        foreach ($product_ids as $pid) {

            $postfields["pid"] = $pid;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, self::$settings['whmcs_api_url_custom']);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 300);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields) );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            $result = curl_exec($ch);
            curl_close($ch);


            if($result && !empty($result)){

                if(strpos( $result, 'result=error;' ) !== false){
                    self::message( 'WHMCS API returns error:'.$result);
                }

                $result = new SimpleXMLElement($result);
                $productData = json_encode($result->products);

                if (sizeof($productData) > 0) {

                    $query = "INSERT INTO `".self::$confpricestablename."` (`pid`, `value`)
                        VALUES ('$pid','$productData')
                        ON DUPLICATE KEY
                        UPDATE `value`='$productData';";

                    $wpdb->query($query);

                    if( $wpdb->last_error != '' ){

                        $dberror = $wpdb->last_error; //bcz query below overrides error
                        self::error("Can\'t update configurable products prices data. Mysqli said: ".$dberror );

                    } else {

                        self::change_setting( 'last_update_confprices', current_time('mysql'));

                    }

                }

            } else {

                self::message('Failed to update configurable prices.');

            }

        }

    }


    /**
     *  Update domains pricing
     */
    public static function update_whmcs_prices_domains(){
        global $wpdb;

        $domains = self::get_whmcs_pricing_api_domains();

        if( empty( $domains['domainregister'] ) ) return;

        $values = '';
        foreach( $domains['domainregister'] as $tld => $domain ){
            $values .= '("'.$tld.'", "'.$domain['currency'].'", "'.$domain['prefix'].'",
            '.$domain['price1'].', '.$domain['price2'].', '.$domain['price3'].', '.$domain['price4'].', '.$domain['price5'].',
            '.$domain['price6'].', '.$domain['price7'].', '.$domain['price8'].', '.$domain['price9'].', '.$domain['price10'].',
            '.(int)$domain['domain_order'].',
            '.$domains['domaintransfer'][$tld]['price1'].', '.$domains['domaintransfer'][$tld]['price2'].', '.$domains['domaintransfer'][$tld]['price3'].', '.$domains['domaintransfer'][$tld]['price4'].', '.$domains['domaintransfer'][$tld]['price5'].',
            '.$domains['domaintransfer'][$tld]['price6'].', '.$domains['domaintransfer'][$tld]['price7'].', '.$domains['domaintransfer'][$tld]['price8'].', '.$domains['domaintransfer'][$tld]['price9'].', '.$domains['domaintransfer'][$tld]['price10'].',
            '.$domains['domainrenew'][$tld]['price1'].', '.$domains['domainrenew'][$tld]['price2'].', '.$domains['domainrenew'][$tld]['price3'].', '.$domains['domainrenew'][$tld]['price4'].', '.$domains['domainrenew'][$tld]['price5'].',
            '.$domains['domainrenew'][$tld]['price6'].', '.$domains['domainrenew'][$tld]['price7'].', '.$domains['domainrenew'][$tld]['price8'].', '.$domains['domainrenew'][$tld]['price9'].', '.$domains['domainrenew'][$tld]['price10'].'),';

        }

        //remove last symbol, in this case ','
        $values = substr($values, 0, -1);

        $query = '
            INSERT INTO `'.self::$domainspricestablename.'` (`tld`, `currency`, `prefix`, `y1`, `y2`, `y3`, `y4`, `y5`, `y6`, `y7`, `y8`, `y9`, `y10`, `domain_order`, `t1`, `t2`, `t3`, `t4`, `t5`, `t6`, `t7`, `t8`, `t9`, `t10`, `r1`, `r2`, `r3`, `r4`, `r5`, `r6`, `r7`, `r8`, `r9`, `r10` )
            VALUES
            '.$values.'
            ON DUPLICATE KEY
            UPDATE
            `currency`=VALUES(`currency`), `prefix`=VALUES(`prefix`),
            `y1`=VALUES(`y1`), `y2`=VALUES(`y2`),
            `y3`=VALUES(`y3`), `y4`=VALUES(`y4`),
            `y5`=VALUES(`y5`), `y6`=VALUES(`y6`),
            `y7`=VALUES(`y7`), `y8`=VALUES(`y8`),
            `y9`=VALUES(`y9`), `y10`=VALUES(`y10`),
            `domain_order`=VALUES(`domain_order`),
            `t1`=VALUES(`t1`), `t2`=VALUES(`t2`),
            `t3`=VALUES(`t3`), `t4`=VALUES(`t4`),
            `t5`=VALUES(`t5`), `t6`=VALUES(`t6`),
            `t7`=VALUES(`t7`), `t8`=VALUES(`t8`),
            `t9`=VALUES(`t9`), `t10`=VALUES(`t10`),
            `r1`=VALUES(`r1`), `r2`=VALUES(`r2`),
            `r3`=VALUES(`r3`), `r4`=VALUES(`r4`),
            `r5`=VALUES(`r5`), `r6`=VALUES(`r6`),
            `r7`=VALUES(`r7`), `r8`=VALUES(`r8`),
            `r9`=VALUES(`r9`), `r10`=VALUES(`r10`);
            ';

        $wpdb->query( $query );

        if( $wpdb->last_error != '' ){
            self::message( 'Error updating prices mysqli said: '.$wpdb->last_error, 'top', 'error' );
        }
        else self::change_setting( 'last_update_domains', current_time( 'mysql' ) );

    }


    /**
     * Set WP cron shedule
     *
     * @param string  $recurrence - cron recurrence
     */
    public static function set_cron( $recurrence ){

        $start_time = time();
        switch( $recurrence ):
            case 'daily':
                $start_time = strtotime( '+1 day', $start_time );
            break;
            case 'twicedaily':
                $start_time = strtotime( '+12 hours', $start_time );
            break;
            case 'hourly':
                $start_time = strtotime( '+1 hour', $start_time );
            break;
        endswitch;

        if( $recurrence != 'off' ){
            if ( !wp_next_scheduled( self::$prefix.'task_hook' ) ){ //set new cron
                wp_schedule_event( $start_time, $recurrence, self::$prefix.'task_hook' );
                self::message( 'Cron turned on.' );
            }
            else{ //update cron
                wp_clear_scheduled_hook( self::$prefix.'task_hook' );
                wp_schedule_event( $start_time, $recurrence, self::$prefix.'task_hook' );
                self::message( 'Cron rescheduled.' );
            }
        }
        elseif( wp_next_scheduled( self::$prefix.'task_hook' ) ){ //if cron exist remove it
            wp_clear_scheduled_hook( self::$prefix.'task_hook' );
            self::message( 'Cron turned off.' );
        }

    }

    /**
     * Here comes all cron tasks
     */
    public static function doCron(){

        whmcsSync::update_whmcs_prices();
        whmcsSync::update_whmcs_confprices();
        whmcsSync::update_whmcs_prices_domains();
        whmcsSync::update_stepsconfig_to_db();
        whmcsSync::customer_count_update();

    }

    /**
     * Redirect to page
     *
     * @param $where - page url
     */
    public static function redirect( $where = 'self' ){
        switch( $where ):
        case 'self':
            $where = self::$settings_page_url;
            header( 'Location: '.$where ); exit;
        break;
        case filter_var( $where, FILTER_VALIDATE_URL):
            header( 'Location: '.$where ); exit;
        break;
        endswitch;

        exit;
    }

    public function _isValidXML($xml) {
        $doc = @simplexml_load_string($xml);
        if ($doc) {
            return true; //this is valid
        } else {
            return false; //this is not valid
        }
}

} // class end

whmcsSync::getInstance();

if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] == 'whmcsSynch_settings' ){

        if( isset( $_POST[ 'change_settings' ] ) && $_POST[ 'change_settings' ] == 1 ){ // changing some settings

            if( isset( $_POST[ 'new_currency' ] ) && $_POST[ 'new_currency' ] != whmcsSync::$settings['currency'] ){

                $currencies = whmcsSync::get_whmcs_currencies();

                if(isset($currencies[$_POST['new_currency']])){
                    whmcsSync::change_setting( 'currency', $_POST['new_currency'] );
                    whmcsSync::change_setting( 'currency_code', $currencies[$_POST['new_currency']]->code );
                    whmcsSync::change_setting( 'currency_prefix', $currencies[$_POST['new_currency']]->prefix );
                    whmcsSync::change_setting( 'currency_suffix', $currencies[$_POST['new_currency']]->suffix );
                }

            }
            if( isset( $_POST[ 'new_cron_schedule' ] ) && $_POST[ 'new_cron_schedule' ] != whmcsSync::$settings['cron_schedule'] ){
                whmcsSync::change_setting( 'cron_schedule', $_POST[ 'new_cron_schedule' ] );
                whmcsSync::set_cron( $_POST[ 'new_cron_schedule' ] );
            }
            if( isset( $_POST[ 'price_construct_prefix' ] ) && $_POST[ 'price_construct_prefix' ] != whmcsSync::$settings['price_construct_prefix'] ){
                whmcsSync::change_setting( 'price_construct_prefix', $_POST[ 'price_construct_prefix' ] );
            }
            if( isset( $_POST[ 'price_construct_cycle' ] ) && $_POST[ 'price_construct_cycle' ] != whmcsSync::$settings['price_construct_cycle'] ){
                whmcsSync::change_setting( 'price_construct_cycle', $_POST[ 'price_construct_cycle' ] );
            }
            if( isset( $_POST[ 'price_construct_currency' ] ) && $_POST[ 'price_construct_currency' ] != whmcsSync::$settings['price_construct_currency'] ){
                whmcsSync::change_setting( 'price_construct_currency', $_POST[ 'price_construct_currency' ] );
            }
            if( isset( $_POST[ 'new_whmcs_api_url' ] ) && $_POST[ 'new_whmcs_api_url' ] != whmcsSync::$settings['whmcs_api_url'] ){
                whmcsSync::change_setting( 'whmcs_api_url', $_POST[ 'new_whmcs_api_url' ] );
            }
            if( isset( $_POST[ 'new_whmcs_api_url_custom' ] ) && $_POST[ 'new_whmcs_api_url_custom' ] != whmcsSync::$settings['whmcs_api_url_custom'] ){
                whmcsSync::change_setting( 'whmcs_api_url_custom', $_POST[ 'new_whmcs_api_url_custom' ] );
            }
            if( isset( $_POST[ 'new_whmcs_api_user' ] ) && $_POST[ 'new_whmcs_api_user' ] != whmcsSync::$settings['whmcs_api_user'] ){
                whmcsSync::change_setting( 'whmcs_api_user', $_POST[ 'new_whmcs_api_user' ] );
            }
            if( isset( $_POST[ 'new_whmcs_api_pass' ] ) && $_POST[ 'new_whmcs_api_pass' ] != whmcsSync::$settings['whmcs_api_pass'] ){
                whmcsSync::change_setting( 'whmcs_api_pass', $_POST[ 'new_whmcs_api_pass' ] );
            }
            if( isset( $_POST[ 'new_whmcs_db_host' ] ) && $_POST[ 'new_whmcs_db_host' ] != whmcsSync::$settings['whmcs_db_host'] ){
                whmcsSync::change_setting( 'whmcs_db_host', $_POST[ 'new_whmcs_db_host' ] );
            }
            if( isset( $_POST[ 'new_whmcs_db_user' ] ) && $_POST[ 'new_whmcs_db_user' ] != whmcsSync::$settings['whmcs_db_user'] ){
                whmcsSync::change_setting( 'whmcs_db_user', $_POST[ 'new_whmcs_db_user' ] );
            }
            if( isset( $_POST[ 'new_whmcs_db_pass' ] ) && $_POST[ 'new_whmcs_db_pass' ] != whmcsSync::$settings['whmcs_db_pass'] ){
                whmcsSync::change_setting( 'whmcs_db_pass', $_POST[ 'new_whmcs_db_pass' ] );
            }
            if( isset( $_POST[ 'new_whmcs_db_db' ] ) && $_POST[ 'new_whmcs_db_db' ] != whmcsSync::$settings['whmcs_db_db'] ){
                whmcsSync::change_setting( 'whmcs_db_db', $_POST[ 'new_whmcs_db_db' ] );
            }

            whmcsSync::message( 'Settings updated.', 'top' );
            whmcsSync::redirect(); //redirect to settings page after settings changes
        }

        if( isset( $_POST[ 'force_price_update' ] ) ){
            whmcsSync::update_whmcs_prices();
            whmcsSync::redirect(); //redirect to settings page after settings changes
        }
        if( isset( $_POST[ 'force_confprices_update' ] ) ){

            whmcsSync::update_whmcs_confprices();
            whmcsSync::redirect(); //redirect to settings page after settings changes

        }
        if( isset( $_POST[ 'force_domains_price_update' ] ) ){
            whmcsSync::update_whmcs_prices_domains();
            whmcsSync::redirect(); //redirect to settings page after settings changes
        }
        if( isset( $_POST[ 'force_stepsconfig_update' ] ) ){
            whmcsSync::update_stepsconfig_to_db();
            whmcsSync::redirect(); //redirect to settings page after settings changes
        }

        if( isset( $_POST[ 'force_customer_count_update' ] ) ){
            whmcsSync::customer_count_update();
            whmcsSync::redirect(); //redirect to settings page after settings changes
        }


}

register_activation_hook( __FILE__, array( 'whmcsSync', 'activatePlugin' ) ); // on plugin activate
register_deactivation_hook( __FILE__, array( 'whmcsSync', 'deactivatePlugin' ) ); // on plugin deactivate

register_uninstall_hook( __FILE__, array( 'whmcsSync', 'uninstallPlugin' ) );

add_action('admin_menu', array(  'whmcsSync', 'create_menu' ));

add_action( whmcsSync::$prefix.'task_hook', array( 'whmcsSync', 'doCron' ) );

?>
