<?php

?>
<style>
table.pricing th{
    background: #ddd;
}
</style>
<div style="padding: 20px;">
    <h1><?php echo self::$pluginName ?> settings</h1>
    <?php foreach( self::$messages[ 'top' ] as $message ) echo $message; ?>
    <br/>
    <table>
    <tr>
    <td style="width: 600px; vertical-align: top;">

        <div style="text-align: left;">
            <h3>Currency</h3>
            <?php foreach( self::$messages[ 'currency_select' ] as $message ) echo $message; ?>
            <form action="<?php echo self::$settings_page_url ?>" method="post" style="">
                <select name="new_currency" style="width: 70px;margin-right:5px;">
                    <?php foreach( $currencies as $cid => $currency ): ?>
                        <option value="<?php echo $cid; ?>"<?php echo ( self::$settings['currency'] == strtolower( $cid ) ) ? ' selected' : '' ?>><?php echo $currency; ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="hidden" name="change_settings" value="1"/>
                <input type="submit" name="update_currency" class="button button-primary" value="Change" />
            </form>
        </div>
        <div style="text-align: left;">
            <h3>Cron [<?php echo self::$settings['cron_schedule'] ?>]</h3>
            <?php foreach( self::$messages[ 'cron_settings' ] as $message ) echo $message; ?>
            <form action="<?php echo self::$settings_page_url ?>" method="post" style="">
                Recurrence:
                <select name="new_cron_schedule" style="width: 70px;margin-right:5px;">
                    <option value="off"<?php echo ( self::$settings['cron_schedule'] == 'off' ) ? ' selected' : '' ?>>Turned off</option>
                    <option value="daily"<?php echo ( self::$settings['cron_schedule'] == 'daily' ) ? ' selected' : '' ?>>Daily</option>
                    <option value="twicedaily"<?php echo ( self::$settings['cron_schedule'] == 'twicedaily' ) ? ' selected' : '' ?>>Twice a day</option>
                    <option value="hourly"<?php echo ( self::$settings['cron_schedule'] == 'hourly' ) ? ' selected' : '' ?>>Hourly</option>
                </select>
                <input type="hidden" name="change_settings" value="1"/>
                <input type="submit" name="update_currency" class="button button-primary" value="Update" />
            </form>
            <span style="font-size: 11px;">Next cron: <?php echo ( wp_next_scheduled( self::$prefix.'task_hook' ) ) ? date( "m-d H:i:s", strtotime( self::$gtm_diff, wp_next_scheduled( self::$prefix.'task_hook' ) ) ) : 'Not scheduled' ?></span>
        </div>
        <div style="text-align: left;">
            <h3>WHMCS API</h3>
            <?php foreach( self::$messages[ 'whmcsapi_settings' ] as $message ) echo $message; ?>
            <form action="<?php echo self::$settings_page_url ?>" method="post" style="">
                <table>
                <tr>
                    <td>API Link:</td><td><input type="text" name="new_whmcs_api_url" value="<?php echo self::$settings['whmcs_api_url'] ?>" style="width: 203px;" /></td>
                </tr>
                <tr>
                    <td>API Link (custom H1P):</td><td><input type="text" name="new_whmcs_api_url_custom" value="<?php echo self::$settings['whmcs_api_url_custom'] ?>" style="width: 203px;" /></td>
                </tr>
                <tr>
                    <td>User: </td><td><input type="text" name="new_whmcs_api_user" value="<?php echo self::$settings['whmcs_api_user'] ?>" style="width: 203px;" /></td>
                </tr>
                <tr>
                    <td>Password: </td><td><input type="password" name="new_whmcs_api_pass" value="<?php echo self::$settings['whmcs_api_pass'] ?>" style="width: 203px;" /></td>
                </tr>
                <tr>
                    <td></td><td align="right"><input type="submit" name="update_whmcs_api_data" class="button button-primary" value="Update"/></td>
                </tr>
            </table>
            <input type="hidden" name="change_settings" value="1"/>
            </form>
        </div>
        <div style="text-align: left;">
            <h3>Price structure</h3> <span style="font-size:11px;"></span>
            <?php foreach( self::$messages[ 'whmcsapi_settings' ] as $message ) echo $message; ?>
            <form action="<?php echo self::$settings_page_url ?>" method="post" style="">
                <table>
                <tr>
                    <td>Prefix</td>
                    <td>Price</td>
                    <td>Currency</td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <select name="price_construct_prefix" style="margin-right:5px;">
                            <option value="yes"<?php echo ( self::$settings['price_construct_prefix'] == strtolower( 'yes' ) ) ? ' selected' : '' ?>>Yes</option>
                            <option value="no"<?php echo ( self::$settings['price_construct_prefix'] == strtolower( 'no' ) ) ? ' selected' : '' ?>>No</option>
                        </select>
                    </td>
                    <td>
                        <select name="price_construct_cycle" style="margin-right:5px;">
                            <option value="monthly"<?php echo ( self::$settings['price_construct_cycle'] == strtolower( 'monthly' ) ) ? ' selected' : '' ?>>Monthly</option>
                            <option value="annually"<?php echo ( self::$settings['price_construct_cycle'] == strtolower( 'annually' ) ) ? ' selected' : '' ?>>Annually</option>
                        </select>
                    </td>
                    <td>
                        <select name="price_construct_currency" style="margin-right:5px;">
                            <option value="yes"<?php echo ( self::$settings['price_construct_currency'] == strtolower( 'yes' ) ) ? ' selected' : '' ?>>Yes</option>
                            <option value="no"<?php echo ( self::$settings['price_construct_currency'] == strtolower( 'no' ) ) ? ' selected' : '' ?>>No</option>
                        </select>
                    </td>
                    <td><input type="submit" name="update_whmcs_api_data" class="button button-primary" value="Update"/></td>
                </tr>
                <tr>
                    <td colspan=4>
                    <?php if( !empty( self::$priceList[1] ) ): ?>
                        Example:
                        <span style="font-weight: bold;">
                            <?php echo self::$priceList[1]['price']; ?>
                        </span>
                    <?php endif; ?>
                    </td>
                </tr>
            </table>
            <input type="hidden" name="change_settings" value="1"/>
            </form>
        </div>
        <div style="text-align: left;">
            <h3>How to use</h3>
            <p>
                To access prices in templates, you need to put this line:<br/>
                "<code style="font-weight:bold;">$whmcsPrices = whmcsSync::getInstance()->priceList;</code>", to your template or header file.<br/>
                Usage example:  <code style="font-weight:bold;">$whmcsPrices[11]['monthly'] | $whmcsPrices[pid]['annually']</code><br/>
                                <code style="font-weight:bold;">$whmcsPrices[11]['price'] | $whmcsPrices[pid]['price']['main' OR 'sub']</code>
            </p>
            <p>
                To access prices for domains:<br/>
                "<code style="font-weight:bold;">$domainPrice = whmcsSync::getInstance()->getDomainsPrices( TLD );</code>", to your template or header file.<br/>
                Usage example to get full price:  <code style="font-weight:bold;">$domainPrice[2]['price'] | $domainPrice[period]['price']</code><br/>
                Use 'main, 'sub' instead of 'price' to get part price
                <br><br>
                <code>$domainPrices = whmcsSync::getInstance()->getDomainsPrices();</code> - to get all domains prices sorted by order.
                <code>$domainPrices['tld']</code>
                <code>$domainPrices['prices']['register|transfer']</code>
            </p>
            <p>
                To access steps config:<br/>
                "<code style="font-weight:bold;">$steps_config = whmcsSync::getInstance()->get_local_stepsconfig();</code>"
            </p>
        </div>
    </td>
    <td style="vertical-align:top;text-align:right;">

        <table width=620>
        <tr>
            <td align="left">
                <h2 style="margin: 0;">Whmcs clients count</h2>
                <span> [Current number: <?php echo self::$settings['whmcs_clients_count'] ?>] </span>
            </td>
            <td></td>
            <td></td>
            <td align="right">
                <form action="<?php echo self::$settings_page_url ?>" method="post">
                    <input type="submit" name="update_stepsconfig" class="button button-primary" value="Force customers count update"/>
                    <input type="hidden" name="force_customer_count_update" value="1"/>
                </form>
            </td>
        </tr>
        </table>

        <table width=620>
        <tr>
            <td align="left">
                <h2 style="margin: 0;">Steps config</h2>
                <span> [Last price update: <?php echo self::$settings['last_update_stepsconfig'] ?>] </span>
            </td>
            <td></td>
            <td></td>
            <td align="right">
                <form action="<?php echo self::$settings_page_url ?>" method="post">
                    <input type="submit" name="update_stepsconfig" class="button button-primary" value="Force steps config update"/>
                    <input type="hidden" name="force_stepsconfig_update" value="1"/>
                </form>
            </td>
        </tr>
        </table>


        <span id="update_products_info_progress"></span>

        <table width=620 style="margin-top: 20px;">
        <tr>
            <td align="left">
                <h2 style="margin: 0;">VPS/Cloud Prices</h2>
                <span> [Last price update: <?php echo self::$settings['last_update_confprices'] ?>] </span>
            </td>
            <td></td>
            <td>
            </td>
            <td align="right">
                <form action="<?php echo self::$settings_page_url ?>" method="post">
                    <input type="submit" name="update_confprices" class="button button-primary" value="Force Configurable prices update"/>
                    <input type="hidden" name="force_confprices_update" value="1"/>
                </form>
            </td>
        </tr>
        </table>


        <table width=620 style="margin-top: 20px;">
        <tr>
            <td align="left">
                <h2 style="margin: 0;">Prices</h2>
                <span> [Last price update: <?php echo self::$settings['last_update'] ?>] </span>
            </td>
            <td></td>
            <td></td>
            <td align="right">
                <form action="<?php echo self::$settings_page_url ?>" method="post">
                    <input type="submit" name="update_prices" class="button button-primary" value="Force prices update"/>
                    <input type="hidden" name="force_price_update" value="1"/>
                </form>
            </td>
        </tr>
        </table>

        <table class="pricing" cellpadding=5 cellspacing=0 border=1 width=620>
            <?php foreach( $prices_by_gid as $group ):
                $temp_gid_var = reset( $group );
            ?>
                <tr>
                    <th align="center" width=20>PID</th>
                    <th align="center" width=400>Products<?php echo !empty( $temp_gid_var['gid'] ) ? ' ( GID:  '.$temp_gid_var['gid'].' ) ' : ''; ?></th>
                    <th align="center" width=100>Monthly<?php echo !empty( $temp_gid_var['currency'] ) ? ' ( '.$temp_gid_var['currency'].' ) ' : ''; ?></th>
                    <th align="center" width=100>Annually<?php echo !empty( $temp_gid_var['currency'] ) ? ' ( '.$temp_gid_var['currency'].' ) ' : ''; ?></th>
                    <th align="center" width=100>Pay Type</th>
                </tr>
                <?php if( !empty( $group ) ): ?>
                    <?php foreach( $group as $product ): ?>
                        <tr>
                            <td align="center" width=20><?php echo $product['pid'] ?></td>
                            <td align="center" width=400><?php echo $product['product'] ?></td>
                            <td align="center" width=100><?php echo $product['prefix'].$product['monthly'] ?></td>
                            <td align="center" width=100><?php echo $product['prefix'].$product['annually'] ?></td>
                            <td align="center" width=100><?php echo $product['paytype'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan=4 align="center" style="font-weight:bold;">No entries found.</td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
        </table>
    <br>
    <div id="domains_pricing">
        <span> [Last price update: <?php echo self::$settings['last_update_domains'] ?>] </span>

        <table width=620>
        <tr>
            <td align="left"><h2>Domains Prices</h2></td>
            <td></td>
            <td></td>
            <td align="right">
                <form action="<?php echo self::$settings_page_url ?>" method="post">
                    <input type="submit" name="update_prices" class="button button-primary" value="Force prices update"/>
                    <input type="hidden" name="force_domains_price_update" value="1"/>
                </form>
            </td>
        </tr>
        </table>

        <table class="pricing" cellpadding=5 cellspacing=0 border=1 width=620>
            <tr>
                <th align="center" width=40>TLD</th>
                <th align="center" width=200>Price [register]</th>
                <th align="center" width=200>Price [transfer]</th>
                <th align="center" width=200>Price [renew]</th>
                <th align="center" width=20>Order</th>
            </tr>
            <?php foreach( $priceList_domains as $tld => $domain_pricing ): ?>
                <tr>
                    <td align="center" width=20><?= $tld ?></td>
                    <td align="center" width=200>
                        <?php foreach( $domain_pricing['prices']['register'] as $year => $price ): ?>
                            <div>Period <?= $year ?> - <?= $price['price'] ?></div>
                        <?php endforeach; ?>
                    </td>
                    <td align="center" width=200>
                        <?php foreach( $domain_pricing['prices']['transfer'] as $year => $price ): ?>
                            <div>Period <?= $year ?> - <?= $price['price'] ?></div>
                        <?php endforeach; ?>
                    </td>
                    <td align="center" width=200>
                        <?php foreach( $domain_pricing['prices']['renew'] as $year => $price ): ?>
                            <div>Period <?= $year ?> - <?= $price['price'] ?></div>
                        <?php endforeach; ?>
                    </td>
                    <td align="center" width=20><?= $domain_pricing['domain_order'] ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
    </td>
    </tr>
    </table>
</div>
<script>
    function loadContent() {
        jQuery.ajax({
            type: "get",
            url: "/wp-content/themes/h1p_v3/crons/update_whmcsp_info.php",
            beforeSend: function () {
                var x=document.getElementById("update_products_info_progress");
                document.getElementById("update_products_info").style.display = 'none';
                x.innerHTML = "Updating <img src=\"/wp-admin/images/loading.gif\" />";

            },
            success: function (data) {
                var x=document.getElementById("update_products_info_progress");
                document.getElementById("update_products_info").style.display = 'initial';
                x.innerHTML = "<span style=\"color:green;\">Successfully updated.</span>";

            },
            error: function () {
                var x=document.getElementById("update_products_info_progress");
                document.getElementById("update_products_info").style.display = 'initial';
                x.innerHTML = "<span style=\"color:red;\">Failed. Please check if cronjob file exits in template directory and try again.<span>";
            }
        });
    }

</script>
