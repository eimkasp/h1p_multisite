jQuery(document).ready(function($){
    // Select or deselect all checkboxes
    var select_all_th = $('#cb-select-all-1');
    var select_all_tf = $('#cb-select-all-2');
    $( select_all_th ).click(function($) {
        if(select_all_th.is(":checked"))
        {
            jQuery('.cbs-handler').each(function(){
                jQuery(this).attr('checked', true);
            });
            jQuery( select_all_tf ).attr('checked', true);
        }
        else
        {
            jQuery('.cbs-handler').each(function(){
                jQuery(this).attr('checked', false);
            });
            jQuery( select_all_tf ).attr('checked', false);
        }
    });
    $( select_all_tf ).click(function($) {
        if(select_all_tf.is(":checked"))
        {
            jQuery('.cbs-handler').each(function(){
                jQuery(this).attr('checked', true);
            });
            jQuery( select_all_th ).attr('checked', true);
        }
        else
        {
            jQuery('.cbs-handler').each(function(){
                jQuery(this).attr('checked', false);
            });
            jQuery( select_all_th ).attr('checked', false);
        }
    });
});