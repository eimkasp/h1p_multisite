<?php
wp_enqueue_style('style', plugin_dir_url(__FILE__ ).'h1p_reviews.css');
wp_register_script( 'h1p_reviews', plugins_url('/h1p_reviews.js', __FILE__), array('jquery'));
wp_enqueue_script( 'h1p_reviews' );
?>

<div style="padding: 20px;">
    <h1><?php echo self::$pluginName ?> settings</h1>
    <?php foreach( self::$messages[ 'top' ] as $message ) echo $message; ?>
    <br/>
<?php

if( self::count_reviews() == 0 )
{
?>
    <div style="text-align: left;">
        <h3>Initiate Reviews</h3>
        <form action="<?php echo self::$settings_page_url ?>" method="post">
            <input type="submit" name="initiate_reviews" class="button button-primary" value="Initiate Reviews" />
        </form>
    </div>
<?php
}
else
{
   
    // Pagination
    $pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
    $limit = 20; // number of rows in page
    $offset = ( $pagenum - 1 ) * $limit;
    $source = (isset( $_GET['source'] ) ? htmlspecialchars($_GET['source']) : "any");
    $status = (isset( $_GET['status'] ) ? htmlspecialchars($_GET['status']) : "any");
    $order_by = (isset( $_GET['order_by'] ) ? htmlspecialchars($_GET['order_by']) : "timestamp");
    $sort_order = (isset( $_GET['sort_order'] ) ? strtoupper(htmlspecialchars($_GET['sort_order'])) : "DESC");
    $total_reviews = self::count_reviews( $source, $status );
    $num_of_pages = ceil( $total_reviews / $limit );
    // Get reviews
    $entries = self::get_local_reviews( $offset, $limit, $source, $status, $order_by, $sort_order );
    // page links
    $page_links = paginate_links( array(
        'base' => add_query_arg( 'pagenum', '%#%' ),
        'format' => '',
        'prev_text' => __( '&laquo;', 'text-domain' ),
        'next_text' => __( '&raquo;', 'text-domain' ),
        'total' => $num_of_pages,
        'current' => $pagenum
    ) );
?>
    <div style="text-align: left;">
        <h3>All Reviews</h3>
        <div class="alignleft" style="padding: 2px 8px 0 0;">
            <form action="<?php echo self::$settings_page_url ?>" method="get">
                <select id="source" name="source">
                    <option value="any" <?php echo ($source== 'any' ? 'selected="selected"' : "" ); ?>>Source</option>
                    <option value="facebook" <?php echo ($source== 'facebook' ? 'selected="selected"' : "" ); ?>>Facebook</option>
                    <option value="twitter" <?php echo ($source== 'twitter' ? 'selected="selected"' : "" ); ?>>Twitter</option>
                    <option value="shopper" <?php echo ($source== 'shopper' ? 'selected="selected"' : "" ); ?>>ShopperApproved</option>
                </select>
                <select id="status" name="status">
                    <option value="any" <?php echo ($status== 'any' ? 'selected="selected"' : "" ); ?>>Status</option>
                    <option value="new" <?php echo ($status== 'new' ? 'selected="selected"' : "" ); ?>>New</option>
                    <option value="accepted" <?php echo ($status== 'accepted' ? 'selected="selected"' : "" ); ?>>Accepted</option>
                    <option value="rejected" <?php echo ($status== 'rejected' ? 'selected="selected"' : "" ); ?>>Rejected</option>
                </select>
                <input type="hidden" name="page" value="h1p_reviews_settings">
                <input type="submit" class="button" value="Filter">
            </form>
        </div>
        <form action="<?php echo self::$settings_page_url.'&source='.$source.'&status='.$status; ?>" method="post">
            <div class="alignleft" style="padding: 2px 8px 0 0;">
                <select name="action">
                    <option value="-1" selected="selected">Bulk Actions</option>
                    <option value="accept">Accept</option>
                    <option value="reject">Reject</option>
                </select>
                <input type="hidden" name="edit_reviews" value="1"/>
                <input type="submit" class="button action" value="Apply">
            </div> 
<?php
            // Page numbers
            if ( $page_links )
            {
                echo '<div class="tablenav"><div class="tablenav-pages">' . $page_links . '</div></div>';
            }

    if( empty($entries) )
    {
    ?>
            <table class="wp-list-table widefat fixed">
                <thead>
                <tr>
                    <th style="text-align: center;">
                        No entries found.
                    </th>
                </tr>
                </thead>
            </table>
    <?php
    }
    else
    {
    ?>
            <table class="wp-list-table widefat fixed">
                <thead>
                    <tr>
                        <th><input id="cb-select-all-1" type="checkbox" style="margin: -1px 0 0 0px;"></th>
                        <th>Name</th>
                        <th class="manage-column sortable <?php echo ($order_by=='rating' ? 'sorted' : ''); ?> <?php echo ($sort_order=='ASC' ? 'asc' : 'desc'); ?>">
                            <a href="<?php echo self::$settings_page_url.'&source='.$source.'&status='.$status.'&order_by=rating&sort_order='.($sort_order=='DESC' ? 'asc' : 'desc'); ?>"><span>Rating</span><span class="sorting-indicator"></span></a>
                        </th>
                        <th>Review Text</th>
                        <th class="manage-column sortable <?php echo ($order_by=='timestamp' ? 'sorted' : ''); ?> <?php echo ($sort_order=='ASC' ? 'asc' : 'desc'); ?>">
                            <a href="<?php echo self::$settings_page_url.'&source='.$source.'&status='.$status.'&order_by=timestamp&sort_order='.($sort_order=='DESC' ? 'asc' : 'desc'); ?>"><span>Date</span><span class="sorting-indicator"></span></a>
                        </th>
                        <th>Status</th>
                        <th>Source</th>
                        <th>Review URL</th>
                        <th>Photo URL</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th><input id="cb-select-all-2" type="checkbox" style="margin: -1px 0 0 0px;"></th>
                        <th>Name</th>
                        <th class="manage-column sortable <?php echo ($order_by=='rating' ? 'sorted' : ''); ?> <?php echo ($sort_order=='ASC' ? 'asc' : 'desc'); ?>">
                            <a href="<?php echo self::$settings_page_url.'&source='.$source.'&status='.$status.'&order_by=rating&sort_order='.($sort_order=='DESC' ? 'asc' : 'desc'); ?>"><span>Rating</span><span class="sorting-indicator"></span></a>
                        </th>
                        <th>Review Text</th>
                        <th class="manage-column sortable <?php echo ($order_by=='timestamp' ? 'sorted' : ''); ?> <?php echo ($sort_order=='ASC' ? 'asc' : 'desc'); ?>">
                            <a href="<?php echo self::$settings_page_url.'&source='.$source.'&status='.$status.'&order_by=timestamp&sort_order='.($sort_order=='DESC' ? 'asc' : 'desc'); ?>"><span>Date</span><span class="sorting-indicator"></span></a>
                        </th>
                        <th>Status</th>
                        <th>Source</th>
                        <th>Review URL</th>
                        <th>Photo URL</th>
                    </tr>
                </tfoot>
                <tbody>
    <?php
        foreach ($entries as $value)
        {
                echo "<tr>";
                    echo "<td><input id='checkbox-".$value['id']."' class='cbs-handler' type='checkbox' name='cbs[]'' value='".$value['id']."'></td>";
                    echo "<td>".$value['name']."</td>";
                    echo "<td>".$value['rating']."</td>";
                    echo "<td>".$value['review_text']."</td>";
                    echo "<td>".date("Y-m-d H:i:s", $value['timestamp'])."</td>";
                    echo "<td>".$value['status']."</td>";
                    echo "<td>".$value['source']."</td>";
                    echo "<td><a href='".$value['url']."'>Link</a></td>";
                    echo "<td><a href='".$value['photo_url']."'>Photo</a></td>";
                echo "</tr>";
        }
    ?>
                </tbody>
            </table>
        </form>
    <?php
    }
    // Page numbers
    if ( $page_links )
    {
        echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
    }
}

?>
</div>