<?php
/*
Plugin Name: HOST1PLUS Reviews
Plugin URI: http://www.host1plus.com
Description: Plugin gets HOST1PLUS reviews from Facebook, Twitter and ShopperApproved.
Version: 0.1
Author: Edgaras Jovaiša
Author URI: http://www.host1plus.com
License:
*/

//error_reporting( E_ALL );

require_once 'twitter_api/twitter.php';
require_once 'facebook_sdk/src/facebook.php';
require_once 'shopper_approved_parser/parse.php';

class h1p_reviews{

private static $instance = null;
public static $pluginName = 'HOST1PLUS Reviews';
public static $version = '0.1';
public static $messages = array( 'cron_settings' => array(''), 'top' => array('') ); // array for error messages
public static $prefix = 'h1p_reviews_';
public static $settings;
private static $script_dir = __DIR__;
private static $reviews_table, $settingstablename, $options_page_uri_name, $settings_page_url;


public static function getInstance() {

    if ( is_null( self::$instance ) )
        self::$instance = new h1p_reviews;

    return self::$instance;
}

public function __construct(){
    global $wpdb;

    if(session_status() == PHP_SESSION_NONE) session_start();

    self::$reviews_table = $wpdb->prefix.self::$prefix.'data';
    self::$settingstablename = $wpdb->prefix.self::$prefix.'settings';
    self::$options_page_uri_name = self::$prefix.'settings';
    self::$settings_page_url = admin_url().'options-general.php?page='.self::$options_page_uri_name;

    self::$settings = self::get_all_setting();

    // Errors
    if( isset( $_SESSION[ self::$prefix.'msgs' ] ) ){
        foreach( $_SESSION[ self::$prefix.'msgs' ] as $place => $errors ){
            if( is_array( $errors ) && is_array( self::$messages[$place] ) ) self::$messages[$place] = array_merge( self::$messages[$place], $errors );
        }
        unset( $_SESSION[ self::$prefix.'msgs' ] );
    }
}

// Gets all plugin settings
private static function get_all_setting(){ //get plugin settings
    global $wpdb;

    $settings_array = array();
    
    $wparray = $wpdb->get_results( 'SELECT * FROM `'.self::$settingstablename.'`', ARRAY_A );
    foreach( $wparray as $setting_row )
        $settings_array[ $setting_row['setting'] ] = (string)$setting_row['value'] ;
    
    return $settings_array;
}

// Activate plugin
public static function activatePlugin()
{
    self::createTablesStructure();
    self::set_cron();
}

// Deactivate plugin - turn off cron
public static function deactivatePlugin()
{
    if( wp_next_scheduled( self::$prefix.'task_hook' ) ){ //if cron exist remove it
        wp_clear_scheduled_hook( self::$prefix.'task_hook' );
    }
}

// Uninstall plugin
public static function uninstallPlugin()
{
    self::dropTables();
}

// Create plugin tables
private static function createTablesStructure()
{
    global $wpdb;
    
    #settings table
    $wpdb->query( 'SHOW TABLES LIKE "'.self::$settingstablename.'"' );
    if( $wpdb->num_rows < 1 ){
    
        $settings_sql = 'CREATE TABLE '.self::$settingstablename.' (
                        `setting` varchar(100) NOT NULL,
                        `value` varchar(200),
                        UNIQUE KEY setting (setting)
                        ) CHARACTER SET=utf8 ;';
    
        $wpdb->query( $settings_sql );
        if( $wpdb->last_error != '' ){ 
            $dberror = $wpdb->last_error; //bcz query below overrides error
            self::error('Can\'t cant create table: '.self::$settingstablename."<br/> mysqli said: ".$dberror );
        }
        // Initiate settings
        $wpdb->query( 'INSERT INTO `'.self::$settingstablename.'` (`setting`,`value`)
                VALUES 
                    ( "facebook_page_id", "103404964373" ),
                    ( "facebook_app_id", "490078851094553" ),
                    ( "facebook_app_secret", "7d28c6dc0c8d59189eb8ad685bb52f28" ),
                    ( "facebook_page_access_token", "CAAG9uVZCU2BkBAIJ5zyzDf2qF8zCX4HZBkPDKicpa2cQM5jxtE7RjvrgPoE9CGWZBRojQLRY3jKREwuSflA0x2gjYXdT3HCTnvBbZB6ZA7ysgtGiLsSE0PCTGIvroSqr6CqsqS7wSotSZBdnPEjNynAeTjeT6WayjUzgjoojaeqPbzhxJ7pfWl2McJtwctya4ZD" ),
                    ( "twitter_consumer_key", "rx64FQnEPY8aUkQ1sIYzOv3sg" ),
                    ( "twitter_consumer_secret", "cEjwgf5WRiJEyaSbnwqxSIQ844BMMG3RwlhiNM3XBRbhTx9JIJ" ),
                    ( "twitter_oauth_token", "21645561-2UTg4L329c9q4lzuxXt3XYDJk7WNW5hqt7Hx1nAg7" ),
                    ( "twitter_oauth_token_secret", "Ps4bg0fasMcY80miBAJvOawLolcOXCrSNBHEv7LrwzGFg" ),
                    ( "twitter_keywords", "@host1plus, #host1plus" )
                ' );

                if( $wpdb->last_error != '' ){
                    $dberror = $wpdb->last_error; //bcz query below overrides error
                    $wpdb->query( 'DROP TABLE '.self::$settingstablename );
                    self::error('Can\'t insert data to: '.self::$settingstablename."<br/> mysqli said: ".$dberror );
                }
    }
        
    #prices table
    $wpdb->query( 'SHOW TABLES LIKE "'.self::$reviews_table.'"' );
    if( $wpdb->num_rows < 1 ){
    
        $reviews_sql = 'CREATE TABLE '.self::$reviews_table.' (
                        `id` bigint(20) NOT NULL AUTO_INCREMENT,
                        `name` varchar(255) NOT NULL,
                        `review_text` longtext,
                        `rating` int(5),
                        `timestamp` varchar(15) NOT NULL,
                        `status` varchar(20) NOT NULL,
                        `url` varchar(256) NOT NULL,
                        `photo_url` varchar(256) NOT NULL,
                        `source` varchar(20) NOT NULL,
                        UNIQUE KEY id (id)
                        ) CHARACTER SET=utf8 ;';
        $wpdb->query( $reviews_sql );
        
        if( $wpdb->last_error != '' ){ 
            $dberror = $wpdb->last_error; //bcz query below overrides error
            $wpdb->query( 'DROP TABLE '.self::$reviews_table );
            self::error( 'Can\'t create table: '.self::$reviews_table."<br/> mysqli said: ".$dberror );
        }
    }
}

// Delete all plugin table
private static function dropTables()
{
    global $wpdb;
      
    $wpdb->query( 'DROP TABLE '.self::$settingstablename.', '.self::$reviews_table ) or die( $wpdb->last_error.'; query: '.$wpdb->last_query ); //delete tables
    
}

// cron function
public static function cron_action()
{
    // yesterday 00:00:00
    $from_time_stamp = mktime(0, 0, 0, date("m"), date("d")-1, date("Y"));
    // today 00:00:00
    $to_time_stamp = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
    self::getReviews( $from_time_stamp, $to_time_stamp );
}

// get reviews
public static function getReviews( $from_time_stamp, $to_time_stamp )
{

    // get all reviews from to timestamps
    $facebook_reviews = get_facebook_reviews(
                                              self::$settings['facebook_page_id'],
                                              self::$settings['facebook_app_id'], 
                                              self::$settings['facebook_app_secret'], 
                                              self::$settings['facebook_page_access_token'], 
                                              $from_time_stamp, 
                                              $to_time_stamp 
                                            );

    if( !is_string($facebook_reviews) )
    {
        self::reviews_to_db( $facebook_reviews, 'facebook' );
    }
    else
    {
        // error
        self::message( 'Facebook error: '.$facebook_reviews, 'top', 'error');
    }

    // get most 6-9 days reviews, because of Twitter API limits
    $twitter_reviews  = get_twitter_reviews(
                                              explode(',', self::$settings['twitter_keywords']),
                                              self::$settings['twitter_consumer_key'], 
                                              self::$settings['twitter_consumer_secret'],
                                              array( 'oauth_token' => self::$settings['twitter_oauth_token'], 'oauth_token_secret' => self::$settings['twitter_oauth_token_secret'] ),
                                              $from_time_stamp, 
                                              $to_time_stamp 
                                            );

    if( !is_string($twitter_reviews) )
    {
        self::reviews_to_db( $twitter_reviews, 'twitter' );
    }
    else
    {
        // error
        self::message( 'Twitter error: '.$twitter_reviews, 'top', 'error');
    }

    $shopper_reviews  = get_shopper_reviews(
                                              $from_time_stamp, 
                                              $to_time_stamp 
                                            );
    if( !is_string($shopper_reviews) )
    {
        self::reviews_to_db( $shopper_reviews, 'shopper' );
    }
    else
    {
        // error
        self::message( 'ShopperApproved parser error: '.$shopper_reviews, 'top', 'error');
    }

}

// Put reviews to DB
private static function reviews_to_db( $reviews, $source )
{
    global $wpdb;
    $values = array();

    foreach ($reviews as $value)
    {
        $values[] = '( "'.$wpdb->escape((isset($value['name']) ? $value['name']: "")).'", "'.$wpdb->escape((isset($value['review_text']) ? $value['review_text']: "")).'", "'.$wpdb->escape((isset($value['rating']) ? $value['rating']: "")).'", "'.$wpdb->escape((isset($value['created_time']) ? $value['created_time']: "")).'", "'.$wpdb->escape((isset($value['url']) ? $value['url']: "")).'", "'.$wpdb->escape((isset($value['picture']) ? $value['picture']: "")).'", "new", "'.$wpdb->escape($source).'" )';
    }
    $wpdb->query( 'INSERT INTO `'.self::$reviews_table.'` (`name`,`review_text`,`rating`,`timestamp`,`url`, `photo_url`,`status`,`source`) VALUES '.implode(",", $values) );

    if( $wpdb->last_error != '' ){
        $dberror = $wpdb->last_error;
        self::error('Can\'t insert data to: '.self::$settingstablename."<br/> mysqli said: ".$dberror );
    }
}

// Create wp-admin settings menu option
public static function create_menu()
{ 
    add_options_page( self::$pluginName, self::$pluginName, 'manage_options', self::$options_page_uri_name, array( __CLASS__, 'settingsPage' ));
}

// Create plugin setting page
public static function settingsPage()
{
    include_once( self::$script_dir.'/settings_tpl.php' );
}

// Get reviews from local DB by given query
public static function get_local_reviews( $offset, $limit, $source = 'any', $status = 'any', $order_by = 'timestamp', $sort_order = 'DESC' )
{
    global $wpdb;
    $where = array();

    if( $status != 'any' || $source != 'any' )
    {
        if($status != 'any')
        {
            $where[] = "`status` = '$status'";
        }
        if($source != 'any')
        {
            $where[] = "`source` = '$source'";
        }
    }
    if(empty($order_by) || empty($sort_order))
    {
        $order_by = 'timestamp';
        $sort_order = 'DESC';
    }
    $query = "SELECT * FROM `".self::$reviews_table."`".( !empty( $where ) ? " WHERE ".implode( " AND ", $where ) : "" )." ORDER BY `".$order_by."` ".$sort_order." LIMIT $offset, $limit";

    $db_data = $wpdb->get_results( $query, ARRAY_A );

    return $db_data;
}

// change reviews status
public static function change_status( $review_ids, $status )
{
    global $wpdb;

    $wpdb->query( "UPDATE `".self::$reviews_table."` SET `status` = '".$status."' WHERE `id` IN ( ".implode(", ", $review_ids)." )" );
}

// Get reviews count
public static function count_reviews( $source = 'any', $status = 'any' )
{
    global $wpdb;
    $where = array();

    if( $status != 'any' || $source != 'any' )
    {
        if($status != 'any')
        {
            $where[] = "`status` = '$status'";
        }
        if($source != 'any')
        {
            $where[] = "`source` = '$source'";
        }
    }
    return $wpdb->get_var( "SELECT COUNT(`id`) FROM `".self::$reviews_table."` ".( !empty( $where ) ? "WHERE ".implode( " AND ", $where ) : "" ) );
}

// redirect user to page
public static function redirect( $where = 'self' ){
    switch( $where ):
    case 'self':
        $where = self::$settings_page_url;
        header( 'Location: '.$where ); exit;
    break;
    case filter_var( $where, FILTER_VALIDATE_URL):
        header( 'Location: '.$where ); exit;
    break;
    endswitch;
    
    exit;
}

// if error
private static function error( $message, $errno = E_USER_ERROR ) { //trigger E_USER type error
 
    if(isset($_GET['action']) && $_GET['action'] == 'error_scrape'){
        echo '<strong>' . $message . '</strong>';
        exit;
    }
    else trigger_error($message, $errno);
 
}
// puts message into $place
public static function message( $message, $place = 'top', $type = 'update', $direct = false ) {
// $places - place to put messages [top,cron_settings], direct, if need to add directly, not in session
 
    switch( $type ):
        case 'success': $class = 'success'; break;
        case 'error': $class = 'error'; break;
        default: $class = 'updated'; break;
    endswitch;
    
    $message = '<div class="'.$class.'" style="margin:10px auto;padding:3px;">'.$message.'</div>';
    
    if( $direct ) self::$messages[ $place ][] = $message;
    else
        $_SESSION[ self::$prefix.'msgs' ][ $place ][] = $message;
 
}

// set cron for everyday to update reviews
public static function set_cron(){

    // Tomorrow 01:00:00
    $start_time = mktime(1, 0, 0, date("m"), date("d")+1, date("Y"));

    //set new cron
    if ( !wp_next_scheduled( self::$prefix.'task_hook' ) )
    { 
        wp_schedule_event( $start_time, 'daily', self::$prefix.'task_hook' );
    }
}

/*-----------------------------------------------------*/

} // end of class

h1p_reviews::getInstance();

// initiate reviews
if( isset( $_POST[ 'initiate_reviews' ] ) )
{
    // Sat, 01 Jan 2000 00:00:00 GMT
    $from_time_stamp = 946684800;
    // today 00:00
    $to_time_stamp = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
    h1p_reviews::getReviews( $from_time_stamp, $to_time_stamp );
    h1p_reviews::message( 'Reviews initiated.', 'top' );
}

// edit reviews status
if( isset( $_POST[ 'edit_reviews' ] ) )
{
    if( isset( $_POST[ 'action' ] ) )
    {
        if( $_POST[ 'action' ] == 'accept' )
        {
            if( isset( $_POST[ 'cbs' ] ) && !empty( $_POST[ 'cbs' ] ) )
            {
                h1p_reviews::change_status( $_POST[ 'cbs' ], 'accepted' );
            }
        }
        if( $_POST[ 'action' ] == 'reject' )
        {
            if( isset( $_POST[ 'cbs' ] ) && !empty( $_POST[ 'cbs' ] ) )
            {
                h1p_reviews::change_status( $_POST[ 'cbs' ], 'rejected' );
            }
        }
    }
}

register_activation_hook( __FILE__, array( 'h1p_reviews', 'activatePlugin' ) );     // on plugin activate
register_deactivation_hook( __FILE__, array( 'h1p_reviews', 'deactivatePlugin' ) ); // on plugin deactivate
register_uninstall_hook( __FILE__, array( 'h1p_reviews', 'uninstallPlugin' ) );     // on plugin uninstall
// Create settings menu
add_action( 'admin_menu', array( 'h1p_reviews', 'create_menu' ) );
// Hook Cron action
add_action( h1p_reviews::$prefix.'task_hook', array( 'h1p_reviews', 'cron_action' ) );

?>