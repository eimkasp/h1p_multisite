<?php

// Twitter OAuth
require_once ('twitteroauth.php');

// Search for Tweets by given query
function get_twitter_reviews( $queries, $twitter_consumer_key, $twitter_consumer_secret, $tokens, $from_time_stamp, $to_time_stamp )
{
    $all_tweets = array();
    foreach ($queries as $query)
    {
        $tweets = array();
        // Arguments 1 and 2 - application static tokens, 2 and 3 - user tokens, received from Twitter during authentification
        $connection = new TwitterOAuth($twitter_consumer_key, $twitter_consumer_secret, $tokens['oauth_token'], $tokens['oauth_token_secret']);  
        $connection->host = 'https://api.twitter.com/1.1/';
        $tweetsJson = $connection->get( "search/tweets.json?q=".urlencode($query)."&result_type=recent&count=100" );

        if(!empty($tweetsJson->statuses))
        {
            foreach ($tweetsJson->statuses as $value)
            {
                // if review time in interval
                $timestamp = convert_twitter_date($value->created_at);
                if( $from_time_stamp <= $timestamp && $to_time_stamp > $timestamp )
                {
                    $tweets[] = array( 'name' => $value->user->name, 'rating' => '', 'review_text' => $value->text, 'created_time' => $timestamp, 'url' => "https://twitter.com/".$value->user->id_str."/status/".$value->id_str, 'picture' => $value->user->profile_image_url_https );
                }
            }
        }

        $all_tweets = array_merge( $all_tweets, $tweets );
    }

    if( !empty($all_tweets) )
    {
        return $all_tweets;
    }
    else
    {
        return "No tweets found.";
    }
}

// converts Twitter time to UNIX timestamp
function convert_twitter_date($date)
{
    list($D, $M, $d, $h, $m, $s, $z, $y) = sscanf($date, "%3s %3s %2d %2d:%2d:%2d %5s %4d");
    date_default_timezone_set('UTC');
    return (int)strtotime("$d $M $y $h:$m:$s $z");
}

?>