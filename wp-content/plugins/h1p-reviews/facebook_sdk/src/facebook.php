<?php

// include required files form Facebook SDK
// added in v4.0.5
require_once( 'Facebook/HttpClients/FacebookHttpable.php' );
require_once( 'Facebook/HttpClients/FacebookCurl.php' );
require_once( 'Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once( 'Facebook/Entities/AccessToken.php' );
require_once( 'Facebook/Entities/SignedRequest.php' );

// added in v4.0.0
require_once( 'Facebook/FacebookSession.php' );
require_once( 'Facebook/FacebookRedirectLoginHelper.php' );
require_once( 'Facebook/FacebookRequest.php' );
require_once( 'Facebook/FacebookResponse.php' );
require_once( 'Facebook/FacebookSDKException.php' );
require_once( 'Facebook/FacebookRequestException.php' );
require_once( 'Facebook/FacebookPermissionException.php' );
require_once( 'Facebook/FacebookOtherException.php' );
require_once( 'Facebook/FacebookAuthorizationException.php' );
require_once( 'Facebook/GraphObject.php' );
require_once( 'Facebook/GraphSessionInfo.php' );

// added in v4.0.5
use Facebook\FacebookHttpable;
use Facebook\FacebookCurl;
use Facebook\FacebookCurlHttpClient;
// added in v4.0.0
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookOtherException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;


function get_facebook_reviews( $page_id, $app_id, $app_secret, $page_access_token, $from_time_stamp, $to_time_stamp )
{
    $session = new FacebookSession($page_access_token);
    FacebookSession::setDefaultApplication($app_id, $app_secret);

    if (isset($session)) {
        $request = new FacebookRequest( $session, 'GET', '/'.$page_id.'/ratings?access_token='.$page_access_token.'&limit=100000&date_format=U&fields=open_graph_story' );
        $response = $request->execute();
        $graphObject = $response->getGraphObject();
        $data = $graphObject->asArray();
        $ratings = array();

        if(!empty($data['data']))
        {
            foreach ($data['data'] as $value)
            {
                $story = $value->open_graph_story;
                if( $from_time_stamp <= $story->publish_time && $to_time_stamp > $story->publish_time )
                {
                    if($story->from->id != "")
                    {
                        $request_picture = new FacebookRequest( $session, 'GET', '/'.$story->from->id.'/picture?redirect=false&fields=url&type=normal&height=100&width=100' );
                        $response_picture = $request_picture->execute();
                        $picture_graphObject = $response_picture->getGraphObject();
                        $picture_data = $picture_graphObject->asArray();
                    }
                    $ratings[] = array( 
                                        'name' => $story->from->name, 
                                        'rating' => $story->data->rating->value, 
                                        'review_text' => (property_exists($story->data, 'review_text') ? $story->data->review_text : ""),
                                        'created_time' => $story->publish_time,
                                        'url' => "https://www.facebook.com/".$story->from->id."/activity/".$story->id,
                                        'picture' => ($picture_data['url'] != "" ? "https://graph.facebook.com/".$story->from->id."/picture?type=normal&width=100&height=100" : "")
                                    );
                }
                
            }
        }

        if( !empty($ratings) )
        {
            return $ratings;
        }
        else
        {
            return "No ratings found.";
        }
    }
    else
    {
        return "No session.";
    }
}

?>