<?php

function get_shopper_reviews( $from_time_stamp, $to_time_stamp )
{
    $loop = true;
    $i = 0;
    $reviews = array();
    $url_pattern = 'http://www.shopperapproved.com/reviews/host1plus.com/date-%d.html';

    while($loop)
    {
        $parsed_reviews = parse_shopper_page( sprintf($url_pattern, $i), $from_time_stamp, $to_time_stamp );
        if(!$parsed_reviews['continue'])
        {
            // Save last reviews and end loop
            $loop = false;
            $reviews = array_merge($reviews, $parsed_reviews['reviews']);
        }
        else
        {
            $reviews = array_merge($reviews, $parsed_reviews['reviews']);
        }
        $i++;
    }

    if( !empty($reviews) )
    {
        return $reviews;
    }
    else
    {
        return "No reviews found.";
    }
}

function parse_shopper_page( $url, $from_time_stamp, $to_time_stamp )
{
    $dom = new DOMDocument();
    @$dom->loadHTMLFile($url);
    $reviews = array( 'reviews' => array(), 'continue' => true );

    foreach($dom->getElementsByTagName('div') as $node)
    {
        if($node->getAttribute('class') == 'review')
        {
            $i = 0;
            $is = false;
            $temp = array();
            foreach($node->getElementsByTagName('div') as $node2)
            {
                switch ($i)
                {
                    case 1:
                        // name
                        $temp['name'] = htmlspecialchars( trim( $node2->nodeValue ) );
                        $node_list = $node2->getElementsByTagName('a');
                        if($node_list->length > 0)
                        {
                            $temp['url'] = $node_list->item(0)->getAttribute('href');
                        }
                        break;
                    case 2:
                        // time created
                        $temp['created_time'] = strtotime( $node2->nodeValue );
                        break;
                    case 3:
                        // rating
                        $temp['rating'] = intval( $node2->getElementsByTagName('span')->item(0)->getElementsByTagName('img')->item(0)->getAttribute('alt') );
                        break;
                    case 4:
                        // review text
                        $temp['review_text'] = htmlspecialchars( trim( $node2->nodeValue ) );
                        break;
                }

                // Delete childs
                $nodesToDelete = array(); 
                foreach($node2->childNodes as $child) 
                { 
                    $nodesToDelete[] = $child; 
                }
                foreach($nodesToDelete as $nodeDel) 
                { 
                    $nodeDel->parentNode->removeChild($nodeDel); 
                }

                $i++;
            }

            if(isset($temp['name']) && $temp['name'] != "" && isset($temp['created_time']) && $temp['created_time'] != "" && isset($temp['rating']) && $temp['rating'] != "" && isset($temp['review_text']) && $temp['review_text'] != "")
            {
                $is = true;
            }

            if($is)
            {
                if( $from_time_stamp <= $temp['created_time'] && $to_time_stamp > $temp['created_time'] )
                {
                    $reviews['reviews'][] = $temp;
                }
            }
            else
            {
                $reviews['continue'] = false;
                return $reviews;
            }
        }
    }

    return $reviews;
}

?>