/**
 * Plugin: kk Star Ratings
 *
 * Description: js for the wordpress plugin kk Star Ratings.
 *
 * @package kk Star Ratings
 * @subpackage WordPress Plugin
 * @author Kamal Khan
 * @plugin_uri http://wakeusup.com/2011/05/kk-star-ratings/
 */

(function($, window, document, undefined){

	$.fn.kkstarratings_h1p = function(options)
	{
		$.fn.kkstarratings_h1p.options = $.extend({
			ajaxurl   : null,
			nonce     : null,
			func      : null,
			grs       : false,
			msg       : 'Rate this post',
			fuelspeed : 400,
			thankyou  : 'Thank you for rating.',
			error_msg : 'An error occured.',
			tooltip   : true,
			tooltips  : {
				0 : {
					tip   : "Poor",
					color : "red"
				},
				1 : {
					tip   : "Fair",
					color : "brown"
				},
				2 : {
					tip   : "Average",
					color : "orange"
				},
				3 : {
					tip   : "Good",
					color : "blue"
				},
				4 : {
					tip   : "Excellent",
					color : "green"
				}
			}
		}, $.fn.kkstarratings_h1p.options, options ? options : {});

		var Objs = [];
		this.each(function(){
			Objs.push($(this));
		});

		$.fn.kkstarratings_h1p.fetch(Objs, 0, '0%', $.fn.kkstarratings_h1p.options.msg, true);

		return this.each(function(){});

    };

	$.fn.kkstarratings_h1p.animate = function(obj)
	{
		if(!obj.hasClass('disabled'))
		{
			var legend = $('.kksr-h1p-legend', obj).html(),
				fuel = $('.kksr-h1p-fuel', obj).css('width');
			$('.kksr-h1p-stars a', obj).hover( function(){
				var stars = $(this).attr('href').split('#')[1];
				if($.fn.kkstarratings_h1p.options.tooltip!=0)
				{
					if($.fn.kkstarratings_h1p.options.tooltips[stars-1]!=null)
					{
						$('.kksr-h1p-legend', obj).html('<span style="color:'+$.fn.kkstarratings_h1p.options.tooltips[stars-1].color+'">'+$.fn.kkstarratings_h1p.options.tooltips[stars-1].tip+'</span>');
					}
					else
					{
						$('.kksr-h1p-legend', obj).html(legend);
					}
				}
				$('.kksr-h1p-fuel', obj).stop(true,true).css('width', '0%');
				$('.kksr-h1p-stars a', obj).each(function(index, element) {
					var a = $(this),
						s = a.attr('href').split('#')[1];
					if(parseInt(s)<=parseInt(stars))
					{
						$('.kksr-h1p-stars a', obj).stop(true, true);
						a.hide().addClass('kksr-h1p-star').addClass('orange').fadeIn('fast');
					}
				});
			}, function(){
				$('.kksr-h1p-stars a', obj).removeClass('kksr-h1p-star').removeClass('orange');
				if($.fn.kkstarratings_h1p.options.tooltip!=0) $('.kksr-h1p-legend', obj).html(legend);
				$('.kksr-h1p-fuel', obj).stop(true,true).animate({'width':fuel}, $.fn.kkstarratings_h1p.options.fuelspeed);
			}).unbind('click').click( function(){
				return $.fn.kkstarratings_h1p.click(obj, $(this).attr('href').split('#')[1]);
			});
		}
		else
		{
			$('.kksr-h1p-stars a', obj).unbind('click').click( function(){ return false; });
		}
	};

	$.fn.kkstarratings_h1p.update = function(obj, per, legend, disable, is_fetch)
	{
		if(disable=='true')
		{
			$('.kksr-h1p-fuel', obj).removeClass('yellow').addClass('orange');
		}
		$('.kksr-h1p-fuel', obj).stop(true, true).animate({'width':per}, $.fn.kkstarratings_h1p.options.fuelspeed, 'linear', function(){
			if(disable=='true')
			{
				obj.addClass('disabled');
				$('.kksr-h1p-stars a', obj).unbind('hover');
			}
			if(!$.fn.kkstarratings_h1p.options.grs || !is_fetch)
			{
				$('.kksr-h1p-legend', obj).stop(true,true).hide().html(legend?legend:$.fn.kkstarratings_h1p.options.msg).fadeIn('slow', function(){
					$.fn.kkstarratings_h1p.animate(obj);
				});
			}
			else
			{
				$.fn.kkstarratings_h1p.animate(obj);
			}
		});
	};

	$.fn.kkstarratings_h1p.click = function(obj, stars)
	{
		$('.kksr-h1p-stars a', obj).unbind('hover').unbind('click').removeClass('kksr-h1p-star').removeClass('orange').click( function(){ return false; });
		
		var legend = $('.kksr-h1p-legend', obj).html(),
			fuel = $('.kksr-h1p-fuel', obj).css('width');
		
		$.fn.kkstarratings_h1p.fetch(obj, stars, fuel, legend, false);
		
		return false;
	};

	$.fn.kkstarratings_h1p.fetch = function(obj, stars, fallback_fuel, fallback_legend, is_fetch)
	{
		var postids = [];
		$.each(obj, function(){
			postids.push($(this).attr('data-id'));
		});
		$.ajax({
			url: $.fn.kkstarratings_h1p.options.ajaxurl,
			data: 'action='+$.fn.kkstarratings_h1p.options.func+'&id='+postids+'&stars='+stars+'&_wpnonce='+$.fn.kkstarratings_h1p.options.nonce,
			type: "post",
			dataType: "json",
			beforeSend: function(){
				$('.kksr-h1p-fuel', obj).animate({'width':'0%'}, $.fn.kkstarratings_h1p.options.fuelspeed);
				if(stars)
				{
					$('.kksr-h1p-legend', obj).fadeOut('fast', function(){
						$('.kksr-h1p-legend', obj).html('<span style="color: green">'+$.fn.kkstarratings_h1p.options.thankyou+'</span>');
					}).fadeIn('slow');
				}
			},
			success: function(response){
				$.each(obj, function(){
					var current = $(this),
						current_id = current.attr('data-id');
					if(response[current_id].success)
					{
						$.fn.kkstarratings_h1p.update(current, response[current_id].fuel+'%', response[current_id].legend, response[current_id].disable, is_fetch);
					}
					else
					{
						$.fn.kkstarratings_h1p.update(current, fallback_fuel, fallback_legend, false, is_fetch);
					}
				});
			},
			complete: function(){
				
			},
			error: function(e){
				$('.kksr-h1p-legend', obj).fadeOut('fast', function(){
					$('.kksr-h1p-legend', obj).html('<span style="color: red">'+$.fn.kkstarratings_h1p.options.error_msg+'</span>');
				}).fadeIn('slow', function(){
					$.fn.kkstarratings_h1p.update(obj, fallback_fuel, fallback_legend, false, is_fetch);
				});
			}
		});
	};

	$.fn.kkstarratings_h1p.options = {
		ajaxurl   : bhittani_plugin_kksr_h1p_js.ajaxurl,
		func      : bhittani_plugin_kksr_h1p_js.func,
		nonce     : bhittani_plugin_kksr_h1p_js.nonce,
		grs       : bhittani_plugin_kksr_h1p_js.grs,
		tooltip   : bhittani_plugin_kksr_h1p_js.tooltip,
		tooltips  : bhittani_plugin_kksr_h1p_js.tooltips,
		msg       : bhittani_plugin_kksr_h1p_js.msg,
		fuelspeed : bhittani_plugin_kksr_h1p_js.fuelspeed,
		thankyou  : bhittani_plugin_kksr_h1p_js.thankyou,
		error_msg : bhittani_plugin_kksr_h1p_js.error_msg
	};
   
})(jQuery, window, document);

jQuery(document).ready( function($){
	$('.kk-star-ratings-h1p').kkstarratings_h1p();
});