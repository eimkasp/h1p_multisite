<?php

/*
Plugin Name: kk Star Ratings - DON'T UPDATE!
Plugin URI: http://wakeusup.com/2011/05/kk-star-ratings/
Description: Renewed from the ground up(as of v2.0), clean, animated and light weight ratings feature for your blog. With kk Star Ratings, you can <strong>allow your blog posts to be rated by your blog visitors</strong>. It also includes a <strong>widget</strong> which you can add to your sidebar to show the top rated post. Wait! There is more to it. Enjoy the extensive options you can set to customize this plugin.
Version: 2.4
Author: Kamal Khan
Author URI: http://bhittani.com
License: GPLv2 or later
*/

require_once 'bhittani-framework/plugin.php';

if(!class_exists('BhittaniPlugin_kkStarRatingsH1P')) :

	class BhittaniPlugin_kkStarRatingsH1P extends BhittaniPlugin
	{
		private $_Menus;

		public function __construct($id, $nick, $ver)
		{
			parent::__construct($id, $nick, $ver);
			$this->_Menus = array();
		}
		/**
		  * File uri
		  *
		  * @since 1.0 Initially defined
		  *
		  * @param string $path Path to file.
		  *
		  * @return string full uri.
		  */
		public static function file_uri($path)
		{
			return plugins_url($path, __FILE__);
		}
		/**
		  * File path
		  *
		  * @since 1.0 Initially defined
		  *
		  * @param string $path Path to file.
		  *
		  * @return string full path.
		  */
		public static function file_path($path)
		{
			return dirname(__FILE__).'/'.$path;
		}
		/** function/method
		* Usage: hook js frontend
		* Arg(0): null
		* Return: void
		*/
		public function js()
		{
			$nonce = wp_create_nonce($this->id);
			$Params = array();
			$Params['nonce'] = $nonce; //for security
			$Params['grs'] = parent::get_options('kksr_h1p_grs') ? true : false;
			$Params['ajaxurl'] = admin_url('admin-ajax.php');
			$Params['func'] = 'kksr_h1p_ajax';
			$Params['msg'] = parent::get_options('kksr_h1p_init_msg');
			$Params['fuelspeed'] = (int) parent::get_options('kksr_h1p_js_fuelspeed');
			$Params['thankyou'] = parent::get_options('kksr_h1p_js_thankyou');
			$Params['error_msg'] = parent::get_options('kksr_h1p_js_error');
			$Params['tooltip'] = parent::get_options('kksr_h1p_tooltip');
			$Params['tooltips'] = parent::get_options('kksr_h1p_tooltips');
			$this->enqueue_js('js', self::file_uri('js.js'), $this->ver, array('jquery'), $Params, false, true);
		}
		/** function/method
		* Usage: hook js admin - helper
		* Arg(0): null
		* Return: void
		*/
		public function js_admin()
		{
			$nonce = wp_create_nonce($this->id);
			$Params = array();
			$Params['nonce'] = $nonce;
			$Params['func_reset'] = 'kksr_h1p_admin_reset_ajax';
			$this->enqueue_js('js_admin', self::file_uri('js_admin.js'), $this->ver, array('jquery', 'bhittaniplugin_admin_script'), $Params);
		}
		/** function/method
		* Usage: hook admin scripts
		* Arg(0): null
		* Return: void
		*/
		public function admin_scripts()
		{
			foreach($this->_Menus as $menu)
			{
				add_action('admin_print_scripts-'.$menu, array(&$this, 'js_admin'));
			}
		}
		/** function/method
		* Usage: hook css
		* Arg(0): null
		* Return: void
		*/
		public function css()
		{
			$this->enqueue_css('', self::file_uri('css.css'));
		}
		/** function/method
		* Usage: hook custom css
		* Arg(0): null
		* Return: void
		*/
		public function css_custom()
		{
			$stars = parent::get_options('kksr_h1p_stars') ? parent::get_options('kksr_h1p_stars') : 5;

			$star_w = parent::get_options('kksr_h1p_stars_w') ? parent::get_options('kksr_h1p_stars_w') : 24;
			$star_h = parent::get_options('kksr_h1p_stars_h') ? parent::get_options('kksr_h1p_stars_h') : 24;

			$star_gray = parent::get_options('kksr_h1p_stars_gray');
			$star_yellow = parent::get_options('kksr_h1p_stars_yellow');
			$star_orange = parent::get_options('kksr_h1p_stars_orange');

			/* Change in order to serve assets as SSL
			 * Strip url to the beginning of '/wp-content' and add appropriate protocol
			 */

			$star_gray_url = site_url() . substr($star_gray, strpos($star_gray, '/wp-content'));
			$star_yellow_url = site_url() . substr($star_yellow, strpos($star_yellow, '/wp-content'));
			$star_orange_url = site_url() . substr($star_orange, strpos($star_orange, '/wp-content'));

			echo '<style>';

			//echo '/* '. site_url() .'*/';

			echo '.kk-star-ratings-h1p { width:'.($star_w*$stars).'px; }';
			echo '.kk-star-ratings-h1p .kksr-h1p-stars a { width:'.($star_w).'px; }';
			echo '.kk-star-ratings-h1p .kksr-h1p-stars span.inact { width:'.($star_w).'px; }';
			echo '.kk-star-ratings-h1p .kksr-h1p-stars, .kk-star-ratings-h1p .kksr-h1p-stars .kksr-h1p-fuel, .kk-star-ratings-h1p .kksr-h1p-stars a { height:'.($star_h).'px; }';
			echo '.kk-star-ratings-h1p .kksr-h1p-stars, .kk-star-ratings-h1p .kksr-h1p-stars .kksr-h1p-fuel, .kk-star-ratings-h1p .kksr-h1p-stars span.inact { height:'.($star_h).'px; }';

			echo $star_gray ? '.kk-star-ratings-h1p .kksr-h1p-star.gray { background-image: url('.$star_gray_url.'); }' : '';
			echo $star_yellow ? '.kk-star-ratings-h1p .kksr-h1p-star.yellow { background-image: url('.$star_yellow_url.'); }' : '';
			echo $star_orange ? '.kk-star-ratings-h1p .kksr-h1p-star.orange { background-image: url('.$star_orange_url.'); }' : '';

			echo '</style>';
		}
		/** function/method
		* Usage: Setting defaults and backwards compatibility
		* Arg(0): null
		* Return: void
		*/
		public function activate()
		{
			$ver_current = $this->ver;

			$Options = array();
			$Options['kksr_h1p_enable'] = 1; // 1|0
			$Options['kksr_h1p_clear'] = 0; // 1|0
			$Options['kksr_h1p_show_in_home'] = 0; // 1|0
			$Options['kksr_h1p_show_in_archives'] = 0; // 1|0
			$Options['kksr_h1p_show_in_posts'] = 1; // 1|0
			$Options['kksr_h1p_show_in_pages'] = 0; // 1|0
			$Options['kksr_h1p_unique'] = 0; // 1|0
			$Options['kksr_h1p_position'] = 'top-left'; // 'top-left', 'top-right', 'bottom-left', 'bottom-right'
			$Options['kksr_h1p_legend'] = '[avg] ([per]) [total] vote[s]'; // [total]=total ratings, [avg]=average, [per]=percentage [s]=singular/plural
			$Options['kksr_h1p_init_msg'] = 'Rate this post'; // string
			$Options['kksr_h1p_column'] = 1; // 1|0

			if(!parent::get_options('kksr_ver'))
			{
				$Options['kksr_h1p_ver'] = $ver_current;
				$Options['kksr_h1p_stars'] = 5;
				$Options['kksr_h1p_stars_w'] = 24;
				$Options['kksr_h1p_stars_h'] = 24;
				$Options['kksr_h1p_stars_gray'] = 0;
				$Options['kksr_h1p_stars_yellow'] = 0;
				$Options['kksr_h1p_stars_orange'] = 0;
				$Options['kksr_h1p_js_fuelspeed'] = 400;
				$Options['kksr_h1p_js_thankyou'] = 'Thank you for your vote';
				$Options['kksr_h1p_js_error'] = 'An error occurred';
				$Options['kksr_h1p_tooltip'] = 1;
				$Opt_tooltips = array();
				$Opt_tooltips[0]['color'] = 'red';
				$Opt_tooltips[0]['tip'] = 'Poor';
				$Opt_tooltips[1]['color'] = 'brown';
				$Opt_tooltips[1]['tip'] = 'Fair';
				$Opt_tooltips[2]['color'] = 'orange';
				$Opt_tooltips[2]['tip'] = 'Average';
				$Opt_tooltips[3]['color'] = 'blue';
				$Opt_tooltips[3]['tip'] = 'Good';
				$Opt_tooltips[4]['color'] = 'green';
				$Opt_tooltips[4]['tip'] = 'Excellent';
				$Options['kksr_h1p_tooltips'] = base64_encode(serialize($Opt_tooltips));
				parent::update_options($Options);
			}

			parent::update_options(array('kksr_h1p_ver'=>$ver_current));
		}
		/** function/method
		* Usage: helper for hooking (registering) the menu
		* Arg(0): null
		* Return: void
		*/
		public function menu()
		{
			// Create main menu tab
			$this->_Menus[] = add_menu_page(
				$this->nick.' - Settings',
				$this->nick,
	            'manage_options',
				$this->id.'_settings',
				array(&$this, 'options_general'),
				self::file_uri('icon.png')
			);
			// Create images menu tab
			$this->_Menus[] = add_submenu_page(
				$this->id.'_settings',
				$this->nick.' - Settings',
				'Settings',
				'manage_options',
				$this->id.'_settings',
				array(&$this, 'options_general')
			);
			// Create images menu tab
			$this->_Menus[] = add_submenu_page(
				$this->id.'_settings',
				$this->nick.' - Stars',
				'Stars',
				'manage_options',
				$this->id.'_settings_stars',
				array(&$this, 'options_stars')
			);
			// Create tooltips menu tab
			$this->_Menus[] = add_submenu_page(
				$this->id.'_settings',
				$this->nick.' - Tooltips',
				'Tooltips',
				'manage_options',
				$this->id.'_settings_tooltips',
				array(&$this, 'options_tooltips')
			);
			// Create reset menu tab
			$this->_Menus[] = add_submenu_page(
				$this->id.'_settings',
				$this->nick.' - Reset',
				'Reset',
				'manage_options',
				$this->id.'_settings_reset',
				array(&$this, 'options_reset')
			);
			// Create info menu tab
			$this->_Menus[] = add_submenu_page(
				$this->id.'_settings',
				$this->nick.' - Help',
				'Help',
				'manage_options',
				$this->id.'_settings_info',
				array(&$this, 'options_info')
			);
		}
		/** function/method
		* Usage: show options/settings form page
		* Arg(0): null
		* Return: void
		*/
		public function options_page($opt)
		{
			if (!current_user_can('manage_options'))
			{
				wp_die( __('You do not have sufficient permissions to access this page.') );
			}
			$sidebar = true;
			$h3 = 'kk Star Ratings (h1p)';
			$Url = array(
			    array(
				    'title' => 'Home',
				    'link' => 'http://wpapis.com/kk-star-ratings/'
				),
				array(
					'title' => 'Changelog',
				    'link' => 'http://wpapis.com/kk-star-ratings/#tab-1369077219827-3-8'
				)
			);
			include self::file_path('admin.php');
		}
		/** function/method
		* Usage: show general options
		* Arg(0): null
		* Return: void
		*/
		public function options_general()
		{
			$this->options_page('general');
		}
		/** function/method
		* Usage: show images options
		* Arg(0): null
		* Return: void
		*/
		public function options_stars()
		{
			$this->options_page('stars');
		}
		/** function/method
		* Usage: show tooltips options
		* Arg(0): null
		* Return: void
		*/
		public function options_tooltips()
		{
			$this->options_page('tooltips');
		}
		/** function/method
		* Usage: show reset options
		* Arg(0): null
		* Return: void
		*/
		public function options_reset()
		{
			$this->options_page('reset');
		}
		/** function/method
		* Usage: show info options
		* Arg(0): null
		* Return: void
		*/
		public function options_info()
		{
			$this->options_page('info');
		}
		public function kksr_h1p_admin_reset_ajax()
		{
			header('content-type: application/json; charset=utf-8');
			check_ajax_referer($this->id);

			$Reset = $_POST['kksr_h1p_reset'];
			if(is_array($Reset))
			{
				foreach($Reset as $id => $val)
				{
					if($val=='1')
					{
						delete_post_meta($id, '_kksr_h1p_ratings');
						delete_post_meta($id, '_kksr_h1p_casts');
						delete_post_meta($id, '_kksr_h1p_ips');
						delete_post_meta($id, '_kksr_h1p_avg');
					}
				}
			}

			$Response = array();
			$Response['success'] = 'true';
			echo json_encode($Response);
			die();
		}
		public function kksr_h1p_ajax()
		{
			header('Content-type: application/json; charset=utf-8');
			check_ajax_referer($this->id);

			$Response = array();

			$total_stars = is_numeric(parent::get_options('kksr_h1p_stars')) ? parent::get_options('kksr_h1p_stars') : 5;

			$stars = is_numeric($_POST['stars']) && ((int)$_POST['stars']>0) && ((int)$_POST['stars']<=$total_stars)
					? $_POST['stars']:
					0;
			$ip = $_SERVER['REMOTE_ADDR'];

			$Ids = explode(',', $_POST['id']);

			foreach($Ids as $pid) :

			$ratings = get_post_meta($pid, '_kksr_h1p_ratings', true) ? get_post_meta($pid, '_kksr_h1p_ratings', true) : 0;
			$casts = get_post_meta($pid, '_kksr_h1p_casts', true) ? get_post_meta($pid, '_kksr_h1p_casts', true) : 0;

			if($stars==0 && $ratings==0)
			{
				$Response[$pid]['legend'] = "0 / 0";//parent::get_options('kksr_h1p_init_msg')
				$Response[$pid]['disable'] = 'false';
				$Response[$pid]['fuel'] = '0';
				do_action('kksr_h1p_init', $pid, false, false);
			}
			else
			{
				$nratings = $ratings + ($stars/($total_stars/5));
				$ncasts = $casts + ($stars>0);
				$avg = $nratings ? number_format((float)($nratings/$ncasts), 1, '.', '') : 0;
				$per = $nratings ? number_format((float)((($nratings/$ncasts)/5)*100), 2, '.', '') : 0;
				$Response[$pid]['disable'] = 'false';
				if($stars)
				{
					$Ips = get_post_meta($pid, '_kksr_h1p_ips', true) ? unserialize(base64_decode(get_post_meta($pid, '_kksr_h1p_ips', true))) : array();
					if(!in_array($ip, $Ips))
					{
						$Ips[] = $ip;
					}
					$ips = base64_encode(serialize($Ips));
					update_post_meta($pid, '_kksr_h1p_ratings', $nratings);
					update_post_meta($pid, '_kksr_h1p_casts', $ncasts);
					update_post_meta($pid, '_kksr_h1p_ips', $ips);
					update_post_meta($pid, '_kksr_h1p_avg', $avg);
					$Response[$pid]['disable'] = parent::get_options('kksr_h1p_unique') ? 'true' : 'false';
					do_action('kksr_h1p_rate', $pid, $stars, $ip);
				}
				else
				{
					do_action('kksr_h1p_init', $pid, number_format((float)($avg*($total_stars/5)), 1, '.', '').'/'.$total_stars, $ncasts);
				}
				$legend = parent::get_options('kksr_h1p_legend');
				$legend = str_replace('[total]', $ncasts, $legend);
				$legend = str_replace('[avg]', number_format((float)($avg*($total_stars/5)), 1, '.', '').' / '.$total_stars, $legend);
				$legend = str_replace('[s]', $ncasts==1?'':'s', $legend);
				$Response[$pid]['legend'] = str_replace('[per]',$per.'%', $legend);
				$Response[$pid]['fuel'] = $per;
			}

			$Response[$pid]['success'] = true;

			endforeach;

			echo json_encode($Response);
			die();
		}
		protected function trim_csv_cb($value)
		{
			if(trim($value)!="")
			    return true;
			return false;
		}
		protected function exclude_cat($id)
		{
			$excl_categories = parent::get_options('kksr_h1p_exclude_categories');
			$Cat_ids = $excl_categories ? array_filter(array_map('trim', explode(",", $excl_categories)), array(&$this, 'trim_csv_cb')) : array();
			$Post_cat_ids = wp_get_post_categories($id);
			$Intersection = array_intersect($Cat_ids, $Post_cat_ids);
			return count($Intersection);
		}
		public function markup($id=false)
		{
			$id = !$id ? get_the_ID() : $id;
			if($this->exclude_cat($id))
			{
				return '';
			}

			$disabled = false;
			if(get_post_meta($id, '_kksr_h1p_ips', true))
			{
				$Ips = unserialize(base64_decode(get_post_meta($id, '_kksr_h1p_ips', true)));
				$ip = $_SERVER['REMOTE_ADDR'];
				if(in_array($ip, $Ips))
				{
					$disabled = parent::get_options('kksr_h1p_unique') ? true : false;
				}
			}
			$pos = parent::get_options('kksr_h1p_position');

			$markup = '
			<div class="kk-star-ratings-h1p '.($disabled || (is_archive() && parent::get_options('kksr_h1p_disable_in_archives')) ? 'disabled ' : ' ').$pos.($pos=='top-right'||$pos=='bottom-right' ? ' rgt' : ' lft').'" data-id="'.$id.'">
			    <div class="kksr-h1p-stars kksr-h1p-star gray">
			        <div class="kksr-h1p-fuel kksr-h1p-star '.($disabled ? 'orange' : 'yellow').'" style="width:0%;"></div>
			        <!-- kksr-h1p-fuel -->';
			$total_stars = parent::get_options('kksr_h1p_stars');
			for($ts = 1; $ts <= $total_stars; $ts++)
			{
				$markup .= '<a href="#'.$ts.'"></a>';
			}
			$markup .='
			    </div>
			    <!-- kksr-h1p-stars -->
			    <div class="kksr-h1p-legend">';
			if(parent::get_options('kksr_h1p_grs'))
			{
				$markup .= apply_filters('kksr_h1p_legend', parent::get_options('kksr_h1p_legend'), $id);
			}
			$markup .=
			    '</div>
			    <!-- kksr-h1p-legend -->
			</div>
			<!-- kk-star-ratings-h1p -->
			';
			$markup .= parent::get_options('kksr_h1p_clear') ? '<br clear="both" />' : '';
			return $markup;
		}
		public function markup_inact($id=false)
		{
			$id = !$id ? get_the_ID() : $id;
			if($this->exclude_cat($id))
			{
				return '';
			}

			$disabled = false;
			if(get_post_meta($id, '_kksr_h1p_ips', true))
			{
				$Ips = unserialize(base64_decode(get_post_meta($id, '_kksr_h1p_ips', true)));
				$ip = $_SERVER['REMOTE_ADDR'];
				if(in_array($ip, $Ips))
				{
					$disabled = parent::get_options('kksr_h1p_unique') ? true : false;
				}
			}
			$pos = parent::get_options('kksr_h1p_position');

			$markup = '
			<div class="inact kk-star-ratings-h1p '.($disabled || (is_archive() && parent::get_options('kksr_h1p_disable_in_archives')) ? 'disabled ' : ' ').$pos.($pos=='top-right'||$pos=='bottom-right' ? ' rgt' : ' lft').'" data-id="'.$id.'">
			    <div class="kksr-h1p-stars kksr-h1p-star gray">
			        <div class="kksr-h1p-fuel kksr-h1p-star '.($disabled ? 'orange' : 'yellow').'" style="width:0%;"></div>
			        <!-- kksr-h1p-fuel -->';
			$total_stars = parent::get_options('kksr_h1p_stars');
			for($ts = 1; $ts <= $total_stars; $ts++)
			{
				$markup .= '<span class="inact"></span>';
			}
			$markup .='
			    </div>
			    <!-- kksr-h1p-stars -->
			    <div class="kksr-h1p-legend">';
			if(parent::get_options('kksr_h1p_grs'))
			{
				$markup .= apply_filters('kksr_h1p_legend', parent::get_options('kksr_h1p_legend'), $id);
			}
			$markup .=
			    '</div>
			    <!-- kksr-h1p-legend -->
			</div>
			<!-- kk-star-ratings-h1p -->
			';
			$markup .= parent::get_options('kksr_h1p_clear') ? '<br clear="both" />' : '';
			return $markup;
		}
		public function manual($atts)
		{
			extract(shortcode_atts(array('id' => false), $atts));
		    if(!is_admin() && parent::get_options('kksr_h1p_enable'))
			{
			    if(
					((parent::get_options('kksr_h1p_show_in_home')) && (is_front_page() || is_home()))
					|| ((parent::get_options('kksr_h1p_show_in_archives')) && (is_archive()))
				  )
				    return $this->markup($id);
				else if(is_single() || is_page())
				    return $this->markup($id);
			}
			else
			{
				remove_shortcode('kkratings_h1p');
				remove_shortcode('kkstarratings_h1p');
			}
			return '';
		}
		public function filter($content)
		{
			if(parent::get_options('kksr_h1p_enable')) :
			if(
			    ((parent::get_options('kksr_h1p_show_in_home')) && (is_front_page() || is_home()))
				|| ((parent::get_options('kksr_h1p_show_in_archives')) && (is_archive()))
				|| ((parent::get_options('kksr_h1p_show_in_posts')) && (is_single()))
				|| ((parent::get_options('kksr_h1p_show_in_pages')) && (is_page()))
			  ) :
			    remove_shortcode('kkratings_h1p');
				remove_shortcode('kkstarratings_h1p');
				$content = str_replace('[kkratings_h1p]', '', $content);
				$content = str_replace('[kkstarratings_h1p]', '', $content);
				$markup = $this->markup();
				switch(parent::get_options('kksr_h1p_position'))
				{
					case 'bottom-left' :
					case 'bottom-right' : return $content . $markup;
					default : return $markup . $content;
				}
			endif;
			endif;
			return $content;
		}
		public function kk_star_rating_h1p($pid=false)
		{
		    if(parent::get_options('kksr_h1p_enable'))
				return $this->markup($pid);
			return '';
		}
		public function kk_star_rating_h1p_inact($pid=false)
		{
		    if(parent::get_options('kksr_h1p_enable'))
				return $this->markup_inact($pid);
			return '';
		}
		public function kk_star_ratings_h1p_get($total=5, $cat=false)
		{
			global $wpdb;
			$table = $wpdb->prefix . 'postmeta';
			if(!$cat)
			    $rated_posts = $wpdb->get_results("SELECT a.ID, a.post_title, b.meta_value AS 'ratings' FROM " . $wpdb->posts . " a, $table b, $table c WHERE a.post_status='publish' AND a.ID=b.post_id AND a.ID=c.post_id AND b.meta_key='_kksr_h1p_avg' AND c.meta_key='_kksr_h1p_casts' ORDER BY b.meta_value DESC, c.meta_value DESC LIMIT $total");
			else
			{
			    $table2 = $wpdb->prefix . 'term_taxonomy';
			    $table3 = $wpdb->prefix . 'term_relationships';
			    $rated_posts = $wpdb->get_results("SELECT a.ID, a.post_title, b.meta_value AS 'ratings' FROM " . $wpdb->posts . " a, $table b, $table2 c, $table3 d, $table e WHERE c.term_taxonomy_id=d.term_taxonomy_id AND c.term_id=$cat AND d.object_id=a.ID AND a.post_status='publish' AND a.ID=b.post_id AND a.ID=e.post_id AND b.meta_key='_kksr_h1p_avg' AND e.meta_key='_kksr_h1p_casts' ORDER BY b.meta_value DESC, e.meta_value DESC LIMIT $total");
			}

			return $rated_posts;
		}
		public function add_column($Columns)
		{
			if(parent::get_options('kksr_h1p_column'))
			    $Columns['kk_star_ratings_h1p'] = 'Ratings';
			return $Columns;
		}
		public function add_row($Columns, $id)
		{
			if(parent::get_options('kksr_h1p_column'))
			{
				$total_stars = parent::get_options('kksr_h1p_stars');
				$row = 'No ratings';
				$raw = (get_post_meta($id, '_kksr_h1p_ratings', true)?get_post_meta($id, '_kksr_h1p_ratings', true):0);
				if($raw)
				{
					$_avg = get_post_meta($id, '_kksr_h1p_avg', true);
					$avg = '<strong>'.($_avg?((number_format((float)($_avg*($total_stars/5)), 1, '.', '')).'/'.$total_stars):'0').'</strong>';
					$cast = (get_post_meta($id, '_kksr_h1p_casts', true)?get_post_meta($id, '_kksr_h1p_casts', true):'0').' votes';
					$per = ($raw>0?ceil((($raw/$cast)/5)*100):0).'%';
					$row = $avg . ' (' . $per . ') ' . $cast;
				}
				switch($Columns)
				{
					case 'kk_star_ratings_h1p' : echo $row; break;
				}
			}
		}
		/** function/method
		* Usage: Allow sorting of columns
		* Arg(1): $Args (array)
		* Return: (array)
		*/
		public function sort_columns($Args)
		{
			$Args = array_merge($Args,
				array('kk_star_ratings_h1p' => 'kk_star_ratings_h1p')
			);
			return wp_parse_args($Args);
		}
		/** function/method
		* Usage: Allow sorting of columns - helper
		* Arg(1): $Query (array)
		* Return: null
		*/
		public function sort_columns_helper($Query)
		{
			if(!is_admin())
			{
				return;
			}
		    $orderby = $Query->get( 'orderby');
		    if($orderby=='kk_star_ratings_h1p')
		    {
		        $Query->set('meta_key','_kksr_h1p_avg');
		        $Query->set('orderby','meta_value_num');
		    }
		}
		public function grs_legend($legend, $id)
		{
			
			// Update 2017-05-31. Looks like outdated non working schema
			
			if(parent::get_options('kksr_h1p_grs'))
			{
				$title = get_the_title($id);

			    $best = parent::get_options('kksr_h1p_stars');
				$score = get_post_meta($id, '_kksr_h1p_ratings', true) ? get_post_meta($id, '_kksr_h1p_ratings', true) : 0;

				if($score)
				{
					$votes = get_post_meta($id, '_kksr_h1p_casts', true) ? get_post_meta($id, '_kksr_h1p_casts', true) : 0;
					$avg = $score ? number_format((float)($score/$votes), 1, '.', '') : 0;
					$per = $score ? number_format((float)((($score/$votes)/5)*100), 2, '.', '') : 0;

					$leg = str_replace('[total]', '<span property="v:reviewCount">'.$votes.'</span>', $legend);
					$leg = str_replace('[avg]', '<span property="v:average">'.$avg.'</span> / <span property="v:best">'.$best.'</span>', $leg);
					$leg = str_replace('[per]',$per.'%', $leg);
					$leg = str_replace('[s]', $votes==1?'':'s', $leg);

					$snippet = '<div xmlns:v="http://rdf.data-vocabulary.org/#" typeof="v:Review-aggregate">';
					$snippet .= '<span property="v:itemreviewed" class="kksr-h1p-title">' . $title . '</span>';
					$snippet .= '<span rel="v:rating">';
					$snippet .= '    <span typeof="v:Rating">';
					$snippet .=          $leg;
					$snippet .= '    </span>';
					$snippet .= '</span>';
					$snippet .= '</div>';
				}
				else
				{
					$snippet = parent::get_options('kksr_h1p_init_msg');
				}

				return $snippet;
			}
			return $legend;
		}

		public function google_rich_snippets_h1p($pid=false) {

			// ignoring options: kksr_h1p_grs, showing all the time

			// Need to get the main article's ratings
			$id = get_the_ID();

			$title = get_the_title($id);
			
			$best = parent::get_options('kksr_h1p_stars');
			$score = get_post_meta($id, '_kksr_h1p_ratings', true) ? get_post_meta($id, '_kksr_h1p_ratings', true) : 0;

			if ($score) : 	// --- if have score

				$votes = get_post_meta($id, '_kksr_h1p_casts', true) ? get_post_meta($id, '_kksr_h1p_casts', true) : 0;
				$avg = $score ? number_format((float)($score/$votes), 1, '.', '') : 0;

			?>


<script type="application/ld+json">
	{ 
		"@context": "http://schema.org",
		"@type": "Product",
		"name": "<?=$title?>",
		"aggregateRating":
			{
				"@type": "AggregateRating",
				"ratingValue": "<?=$avg?>",
				"reviewCount": "<?=$votes?>"
			}
	}
</script>

			<?php

			endif;		// --- end if have score
						// --- show nothing if score nonexistent (on index or search pages or post was not rated yet)

			return true;

		}
		
	}

	$kkStarRatingsH1P_obj = new BhittaniPlugin_kkStarRatingsH1P('bhittani_plugin_kksr_h1p', 'kk Star Ratings (h1p)', '2.4');

	// Setup
    register_activation_hook(__FILE__, array($kkStarRatingsH1P_obj, 'activate'));

	// Scripts
	add_action('wp_enqueue_scripts', array($kkStarRatingsH1P_obj, 'js'));
	add_action('wp_enqueue_scripts', array($kkStarRatingsH1P_obj, 'css'));
	add_action('wp_head', array($kkStarRatingsH1P_obj, 'css_custom'));
	add_action('admin_init', array($kkStarRatingsH1P_obj, 'admin_scripts'));
	add_action('wp_head', array($kkStarRatingsH1P_obj, 'google_rich_snippets_h1p'));

	// Menu
	add_action('admin_menu', array($kkStarRatingsH1P_obj, 'menu'));

    // AJAX
	add_action('wp_ajax_kksr_h1p_admin_reset_ajax', array($kkStarRatingsH1P_obj, 'kksr_h1p_admin_reset_ajax'));
	add_action('wp_ajax_kksr_h1p_ajax', array($kkStarRatingsH1P_obj, 'kksr_h1p_ajax'));
	add_action('wp_ajax_nopriv_kksr_h1p_ajax', array($kkStarRatingsH1P_obj, 'kksr_h1p_ajax'));

    // Main Hooks
	add_filter('the_content', array($kkStarRatingsH1P_obj, 'filter'));
	add_shortcode('kkratings_h1p', array($kkStarRatingsH1P_obj, 'manual'));
	add_shortcode('kkstarratings_h1p', array($kkStarRatingsH1P_obj, 'manual'));

	// Google Rich Snippets
	add_filter('kksr_h1p_legend', array($kkStarRatingsH1P_obj, 'grs_legend'), 1, 2);

    // Posts/Pages Column
	add_filter( 'manage_posts_columns', array($kkStarRatingsH1P_obj, 'add_column') );
	add_filter( 'manage_pages_columns', array($kkStarRatingsH1P_obj, 'add_column') );
	add_filter( 'manage_posts_custom_column', array($kkStarRatingsH1P_obj, 'add_row'), 10, 2 );
	add_filter( 'manage_pages_custom_column', array($kkStarRatingsH1P_obj, 'add_row'), 10, 2 );
	add_filter( 'manage_edit-post_sortable_columns', array($kkStarRatingsH1P_obj, 'sort_columns') );
	add_filter( 'pre_get_posts', array($kkStarRatingsH1P_obj, 'sort_columns_helper') );

    // For use in themes
	if(!function_exists('kk_star_ratings_h1p'))
	{
		function kk_star_ratings_h1p($pid=false)
		{
			global $kkStarRatingsH1P_obj;
			return $kkStarRatingsH1P_obj->kk_star_rating_h1p($pid);
		}
	}

	if(!function_exists('kk_star_ratings_h1p_get'))
	{
		function kk_star_ratings_h1p_get($lim=5, $cat=false)
		{
			global $kkStarRatingsH1P_obj;
			return $kkStarRatingsH1P_obj->kk_star_ratings_h1p_get($lim, $cat);
		}
	}

    // For use in themes inactive
	if(!function_exists('kk_star_ratings_h1p_inact'))
	{
		function kk_star_ratings_h1p_inact($pid=false)
		{
			global $kkStarRatingsH1P_obj;
			return $kkStarRatingsH1P_obj->kk_star_rating_h1p_inact($pid);
		}
	}

	// For use in themes 
	if(!function_exists('google_rich_snippets_h1p'))
	{
		function google_rich_snippets_h1p($pid=false)
		{
			global $kkStarRatingsH1P_obj;
			return $kkStarRatingsH1P_obj->google_rich_snippets_h1p($pid);
		}
	}


    require_once 'shortcode/shortcode.php';
	require_once 'widget.php';

endif;

?>
