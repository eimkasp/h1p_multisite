<?php

if(!class_exists('BhittaniPlugin_kkStarRatingsH1P_Shortcode')) : 
    // Declare and define the class.
	class BhittaniPlugin_kkStarRatingsH1P_Shortcode
	{	
		
		static public function tinymce_add_button()
		{
			if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
				return;

			if ( get_user_option('rich_editing') == 'true') 
			{
				add_filter("mce_external_plugins", array("BhittaniPlugin_kkStarRatingsH1P_Shortcode","tinymce_custom_plugin"));
				add_filter('mce_buttons', array("BhittaniPlugin_kkStarRatingsH1P_Shortcode",'tinymce_register_button'));
			}
		}
			 
		static public function tinymce_register_button($buttons) 
		{
			array_push($buttons, "|", "kkstarratings_h1p");
			return $buttons;
		}
			 
		static public function tinymce_custom_plugin($plugin_array) 
		{
			//echo WP_PLUGIN_URL.'/kk-star-ratings/shortcode/mce/kkstarratings_h1p/editor_plugin.js';
			//$plugin_array['kkstarratings_h1p'] = WP_PLUGIN_URL.'/kk-star-ratings-h1p/shortcode/mce/kkstarratings_h1p/editor_plugin.js';
			$plugin_array['kkstarratings_h1p'] = BhittaniPlugin_kkStarRatingsH1P::file_uri('shortcode/mce/kkstarratings_h1p/editor_plugin.js');
			return $plugin_array;
		}
	}
	
	add_action('init', array('BhittaniPlugin_kkStarRatingsH1P_Shortcode','tinymce_add_button'));

endif;
?>