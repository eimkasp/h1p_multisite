// JavaScript Document
(function() {
	tinymce.create('tinymce.plugins.kkStarRatingsH1P', {
		init : function(ed, url) {

			// Register button and click event
			ed.addButton('kkstarratings_h1p', {
				title : 'Insert Star Ratings', 
				cmd : 'mceKKStarRatingsH1P', 
				image: url + '/icon.png', 
				onclick : function(){
					ed.execCommand('mceReplaceContent', false, "[kkstarratings_h1p]");
				}});
		},

		getInfo : function() {
			return {
				longname : 'kk Star Ratings (h1p)',
				author : 'Kamal Khan',
				authorurl : 'http://bhittani.com',
				infourl : 'http://wakeusup.com/2011/05/kk-star-ratings',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('kkstarratings_h1p', tinymce.plugins.kkStarRatingsH1P);
	
})();