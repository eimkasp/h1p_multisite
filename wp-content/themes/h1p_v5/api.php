<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Api
*/


    // ATTENTION
    // There must be a content in this WP page



wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

get_header();

?>

<article class="page api">

    <header class="headline api">
        <div class="container adjust-vertical-center">
            <h1 class="page-title">
                <img class="center display-block p-b-20" width="110" src="<?php echo $images ?>img/features/cloud/cloud_api_icon.svg" alt="API">
                <?php _e('Powerful API')?>
            </h1>
            <div class="title-descr"><?php _e('Learn the curve of managing your Cloud Server with our in-house developed API.')?></div>
            <div class="row center p-v-30">
                <a href="<?php echo $config['links']['developers_api']; ?>" class="button highlight large"><?php _e('Documentation');?></a>
            </div>


        </div>
    </header> <!-- end of .headline.api -->

    <section class="main page-content">


        <section class="positions extra-pad-top p-b-60">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('What do we offer?'); ?></h2>
                    <p class="title-follow-text center"><?php _e('Everything you need in different programming languages in one place.'); ?></p>
                </div>

                <div class="row three-col-row">

                    <div class="block-col program-lang active">
                        <span class="program-lang-ico lang-curl"></span>
                        <div class="lang-block-overlay">
                        </div>
                    </div>
                    <div class="block-col program-lang active">
                        <span class="program-lang-ico lang-go"></span>
                        <div class="lang-block-overlay">
                        </div>
                    </div>
                    <div class="block-col program-lang active">
                        <span class="program-lang-ico lang-java"></span>
                        <div class="lang-block-overlay">
                        </div>
                    </div>

                </div>
                <div class="row three-col-row">

                    <div class="block-col program-lang active">
                        <span class="program-lang-ico lang-js" ></span>
                        <div class="lang-block-overlay">
                        </div>
                    </div>
                    <div class="block-col program-lang active">
                        <span class="program-lang-ico lang-node"></span>
                        <div class="lang-block-overlay">
                        </div>
                    </div>
                    <div class="block-col program-lang active">
                        <span class="program-lang-ico lang-objc"></span>
                        <div class="lang-block-overlay">
                        </div>
                    </div>

                </div> <!-- end of .three-col-row -->

                <div class="row three-col-row">

                    <div class="block-col program-lang active">
                        <span class="program-lang-ico lang-php" ></span>
                        <div class="lang-block-overlay">
                        </div>
                    </div>
                    <div class="block-col program-lang active">
                        <span class="program-lang-ico lang-py"></span>
                        <div class="lang-block-overlay">
                        </div>
                    </div>
                    <div class="block-col program-lang active">
                        <span class="program-lang-ico lang-rb"></span>
                        <div class="lang-block-overlay">
                        </div>
                    </div>

                </div> <!-- end of .three-col-row -->

                <div class="row three-col-row">

                    <div class="block-col program-lang active">
                        <span class="program-lang-ico lang-swift" ></span>
                        <div class="lang-block-overlay">
                        </div>
                    </div>
                    <div class="block-col program-lang active">
                        <span class="program-lang-ico lang-csharp"></span>
                        <div class="lang-block-overlay">
                        </div>
                    </div>
                    <div class="block-col program-lang active">
                        <span class="program-lang-ico lang-c"></span>
                        <div class="lang-block-overlay">
                        </div>
                    </div>

                </div> <!-- end of .three-col-row -->


                <div class="row p-t-40">
                    <div class="block-col program-lang">
                        <div class="row two-col-row">
                            <div class="block-col p-40 center-sm">
                                <h3 class="block-title-lower"><?php _e('Don\'t know where to start?'); ?></h3>
                                <p class="title-follow-text-lower "><?php _e('Take a look at our practical guide to API.'); ?></p>
                                <a class="inline regular-link m-t-20" href="https://www.host1plus.com/blog/articles/practical-guide-api/"><?php _e('See how it works'); ?><?php if (get_locale() == 'pt_BR') echo ' (EN)' ?></a> <i class="fa fa-angle-right"></i>
                            </div>
                            <div class="block-col p-30 center">
                                <img width="66%" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/cloud_servers/how_to_use_api.svg" alt="How to start using API">
                            </div>
                        </div>
                    </div>
                </div>

            </div> <!-- end of .container -->

        </section>

        <section class="ask-questions extra-pad-top p-b-60 color-bg-style1">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Don’t have a Cloud Server yet?')?></h2>
                    <span class="p-t-20 display-block">
                        <?php _e('Unlock extra functionality for your environment built on high-end hardware and high-speed network.'); ?>
                    </span>
                </div>

                <div class="container-flex">
                    <div class="block-fluid">
                        <ul class="features-list checklist brightblue">
                            <li><?php _e('KVM virtualization')?></li>
                            <li><?php _e('Windows & Linux')?></li>
                        </ul>
                    </div>
                    <div class="block-fluid">
                        <ul class="features-list checklist brightblue">
                            <li><?php _e('Powerful API')?></li>
                            <li><?php _e('IPv4 and IPv6 support')?></li>
                        </ul>
                    </div>
                    <div class="block-fluid">
                        <ul class="features-list checklist brightblue">
                            <li><?php _e('1 free backup')?></li>
                            <li><?php _e('10G network')?></li>
                        </ul>
                    </div>
                </div>

                <div class="row center m-t-40">
                    <a href="<?php echo $config['links']['cloud-servers'];?>" class="button primary orange large"><?php _e('Get Yours!');?></a>
                </div>

            </div>

        </section>

        <section class="service-faq extra-pad-top" data-section-title="FAQ">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php

                    /*ob_start();
                    the_content();
                    $page_content = ob_get_contents();
                    ob_clean();
                    include_block("htmlblocks/faq/cs-hosting-api.php", array('changelog' => $page_content) );*/

                    include_block("htmlblocks/faq/cs-hosting-api.php");

                ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->

    </section>

</article>

<?php

universal_redirect_footer([
    'en' => $site_en_url.'/api/',
    'br' => $site_br_url.'/api/'
]);

get_footer(); 

?>
