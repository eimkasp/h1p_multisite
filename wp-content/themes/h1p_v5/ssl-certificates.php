<?php
/**
 * @package WordPress
 * @subpackage h1p_v4
 *
 * Template Name: SSL Certificates
 */


// prepare ssl prices

$sslPrices = array(

    'ComodoSSL' => $whmcsPrices[91],
    'PositiveSSL' => $whmcsPrices[104],

    'RapidSSL' => $whmcsPrices[23],
    'QuickSSLPremium' => $whmcsPrices[12],
    'TrueBizID' => $whmcsPrices[15]

);

$sslMinPrice = $sslPrices['PositiveSSL']['price'];

$sslComodoMinPrice = $sslPrices['PositiveSSL']['price'];
$sslGeoTrustMinPrice = $sslPrices['RapidSSL']['price'];


// prepare tutorials posts

$tutorials_cat = get_category_by_slug("tutorials");
$tutorials_id = $tutorials_cat->term_id;

$tutorials = new WP_Query(array(
    'cat' => $tutorials_id,
    'posts_per_page' => 6,
    'orderby' => 'meta_value_num',
    'meta_key' => '_post_views_count',
    'tag' => 'ssl-certificate',
    'order' => 'DESC'
));


get_header();

?>

<div class="page ssl-certificates">

    <section class="image-header">
        <div class="container">
            <div class="title"><?php _e('SSL Certificates')?></div>
            <div class="title-descr"><?php _e('Protect your online business!')?></div>
            <div class="descr-text"><?php _e('Security has its price, but it doesn’t have to be a high one. Preserve sensitive data – passwords, credit card numbers – and gain the trust you deserve.')?></div>
            <div class="menu">
                <a class="item" href="#comodo-ssl-certificate" data-scrollto="comodo-ssl-certificate"><?php printf(__('COMODO SSL from %s / yr'), $sslComodoMinPrice)?></a>
                <a class="item" href="#geotrust-ssl-certificate" data-scrollto="geotrust-ssl-certificate"><?php printf(__('GeoTrust SSL from %s / yr'), $sslGeoTrustMinPrice)?></a>
            </div>
        </div>
    </section>

    <section class="page-content">
        <div class="container">

            <div class="page-path">
                <span class="text-item"><?php _e('Home')?></span>
                <i class="fa fa-angle-right text-item"></i>
                <span class="text-item active"><?php _e('SSL Certificates')?></span>
            </div>

            <div class="page-title-block">
                <h1 class="page-title"><?php printf(__('Choose an SSL certificate from %s / year!'), '<span class="price">'.$sslMinPrice.'</span>')?></h1>
                <span class="title-text"><?php _e('If you are looking for a quick and cost-effective solution to increase the security of your online presence, select from a range of Host1Plus SSL certificates.')?></span>
            </div>

            <div class="certificate">

                <div class="scroll-id-target" id="comodo-ssl-certificate"></div>

                <h2 class="product-logo"><i class="icon ssl-certificates comodo"></i></h2>

                <?php include_block("htmlblocks/pricing-tables/ssl-certificates/comodo.php"); ?>

            </div>

            <div class="certificate">

                <div class="scroll-id-target" id="geotrust-ssl-certificate"></div>

                <h2 class="product-logo"><i class="icon ssl-certificates geotrust"></i></h2>

                <?php include_block("htmlblocks/pricing-tables/ssl-certificates/geotrust.php"); ?>

            </div>

            <div class="block-about">

                <h4 class="block-title"><?php _e('All Plans Include')?></h4>

                <p class="block-description"><?php _e('All SSL certificates are designed to fulfill your needs. Stay safe and run your business without worries.')?></p>

                <div class="features">
                    <div class="badges">
                        <div class="badge"><i class="icon ssl-badge comodo-authentic-secure"></i></div>
                        <div class="badge"><i class="icon ssl-badge comodo-secure"></i></div>
                        <div class="badge"><i class="icon ssl-badge comodo-ev-ssl-site"></i></div>
                    </div>
                    <ul class="features-list checklist green">
                        <li><?php _e('Responsive expert support')?></li>
                        <li><?php _e('Extra-quick setup')?></li>
                        <li><?php _e('Compatible with 99&#37; browsers')?></li>
                        <li><?php _e('Strong encryption')?></li>
                        <li><?php _e('2048-bit root')?></li>
                        <li><?php _e('Mobile device compatibility')?></li>
                        <li><?php _e('Trust mark')?></li>
                    </ul>
                </div>

            </div>

        </div>
    </section>

    <?php include_block("htmlblocks/slides/why-h1p-ssl.php"); ?>

    <section class="integration">

        <div class="container">

            <h2 class="block-title"><?php _e('SSL Certificate Integration')?></h2>

            <div class="steps">

                <div class="step step-1">

                    <span class="step-title"><span class="step-nmb">1</span> <?php _e('Choose an SSL certificate')?></span>

                    <div class="step-content">

                        <div class="options">

                            <div class="option option-1">

                                <div class="row">

                                    <div class="col option-title">

                                        <div class="circle">
                                            <span class="inner">
                                                <span class="txt"><?php _e('Standard SSL’s')?></span>
                                            </span>
                                        </div>

                                    </div>

                                    <div class="col option-content">
                                        <ul class="plans-list fa-ul">
                                            <li><i class="fa-li fa fa-chevron-circle-right"></i> <?php _e('Comodo SSL')?> <span class="price"><?php echo $sslPrices['ComodoSSL']['price']?> <?php _e('/ year')?></span></li>
                                            <li><i class="fa-li fa fa-chevron-circle-right"></i> <?php _e('Positive SSL')?> <span class="price"><?php echo $sslPrices['PositiveSSL']['price']?> <?php _e('/ year')?></span></li>
                                            <li><i class="fa-li fa fa-chevron-circle-right"></i> <?php _e('RapidSSL')?> <span class="price"><?php echo $sslPrices['RapidSSL']['price']?> <?php _e('/ year')?></span></li>
                                            <li><i class="fa-li fa fa-chevron-circle-right"></i> <?php _e('QuickSSL Premium')?> <span class="price"><?php echo $sslPrices['QuickSSLPremium']['price']?> <?php _e('/ year')?></span></li>
                                        </ul>
                                    </div>

                                </div>



                                <div class="option-footer">
                                    <?php _e('Get standard SSL certificates at the best price for a good kickoff: start your online business without any security risks and ensure the satisfaction of your clients from day one.')?>
                                </div>

                            </div>

                            <div class="options-separator"><?php _e('OR')?></div>

                            <div class="option option-2">

                                <div class="row">

                                    <div class="col option-title">

                                        <div class="circle">
                                            <span class="inner">
                                                <span class="txt"><?php _e('Business SSL’s')?></span>
                                            </span>
                                        </div>

                                    </div>

                                    <div class="col option-content">

                                        <ul class="plans-list fa-ul">
                                            <li><i class="fa-li fa fa-chevron-circle-right"></i> <?php _e('True BusinessID')?> <span class="price">$127.00 <?php _e('/ year')?></span></li>
                                       </ul>

                                    </div>

                                </div>

                                <div class="option-footer">
                                    <?php _e('Corporate level SSL certificates are dedicated for online businesses and e-commerce sites. They ensure the best user experience and steadfast security for your clients.')?>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="step step-2">

                    <span class="step-title"><span class="step-nmb">2</span> <?php _e('Install')?></span>

                    <div class="step-content">

                        <span class="step-subtitle"><?php _e('Watch Host1Plus video tutorials and install an SSL certificates with ease.')?></span>

                        <div class="options">

                            <div class="option option-1">

                                <div class="option-title">

                                    <span class="title"><?php _e('Use a control panel:')?></span>

                                </div>

                                <div class="option-content">

                                    <a href="javascript:" class="video-btn" data-popup="https://www.youtube.com/watch?v=rEu5_HTIW3I" data-popup-type="iframe">

                                        <div class="play" ><i class="fa fa-play"></i></div>

                                        <span class="label"><i class="icon ssl-integration cpanel"></i></span>

                                    </a>

                                    <a href="javascript:" class="video-btn" data-popup="https://www.youtube.com/watch?v=uW9WAGUAsD8" data-popup-type="iframe">

                                        <div class="play"><i class="fa fa-play"></i></div>

                                        <span class="label"><i class="icon ssl-integration directadmin"></i></span>

                                    </a>

                                </div>

                            </div>

                            <div class="options-separator"><?php _e('OR')?></div>

                            <div class="option option-2">

                                <div class="option-title">

                                    <span class="title"><?php _e('Install SSL manually:')?></span>

                                </div>

                                <div class="option-content">

                                    <a href="https://www.youtube.com/user/Host1Plus/search?query=SSL" class="video-btn" target="_blank">

                                        <div class="play"><i class="fa fa-play"></i></div>

                                        <span class="label"><?php _e('View All Tutorials')?></span>

                                    </a>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="step step-3">

                    <span class="step-title"><span class="step-nmb">3</span> <?php _e('Prepare Your CMS')?></span>

                    <div class="step-content">

                        <div class="options">

                            <div class="option">

                                <div class="option-title">

                                    <span class="title"><?php _e('Install SSL for your CMS with the help of Host1Plus tutorials!')?></span>

                                </div>

                                <div class="col option-content">

                                    <ul class="tutorials-list">

                                        <?php $i = 0; foreach ($tutorials->posts as $post): ?>

                                            <li class="tutorial tutorials-item-<?php echo $i+1?>">
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="javascript:" class="title"><?php echo $post->post_title;?></a>
                                                <span class="tutorial-data">
                                                    <span class="rating">

                                                        <span class="stars">
                                                           <?php if(function_exists("kk_star_ratings_inact")) : echo kk_star_ratings_inact($post->ID); endif; ?>
                                                        </span>

                                                    </span>
                                                    <a href="javascript:" class="comments">0 <?php _e('comments')?> <i class="fa fa-angle-right"></i></a>
                                                </span>
                                            </li>

                                        <?php $i++; endforeach; ?>

                                    </ul>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="step step-4">

                    <span class="step-title"><span class="step-nmb">4</span> <?php _e('Done!')?></span>

                </div>


            </div>

        </div>

    </section>

</div>
<?php get_footer(); ?>
