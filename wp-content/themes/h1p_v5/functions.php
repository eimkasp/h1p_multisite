<?php


define('H1P_THEME_DIR', get_template_directory());


// get whmcs prices & config params

$whmcsPrices = array();
$stepsConfig = array();

if(class_exists('whmcsSync')){

    $whmcs = whmcsSync::getInstance();

    $whmcsPrices = whmcsSync::getInstance()->get_local_pricing(); //to get price list, usage example: $whmcsPrices[11]['monthly'] | $whmcsPrices[pid][recurring]
    $stepsConfig = whmcsSync::getInstance()->get_local_stepsconfig();

}


// include host1plus configuration

include(__DIR__ . '/config.php');

include(__DIR__ . "/framework/vc-functions.php");
include(__DIR__ . "/framework/admin/admin-functions.php");
include(__DIR__ . "/framework/cpt.php");
include(__DIR__ . "/framework/nav.php");
include(__DIR__ . "/framework/security.php");
include(__DIR__ . "/framework/enques.php");
include(__DIR__ . "/framework/custom-fields.php");





/**
 * Get all accepted reviews from DB
 *
 * @return array
 */
function get_reviews()
{

    global $wpdb;

    $reviews = array();

    $reviews = $wpdb->get_results("SELECT * FROM `wp_h1p_reviews_data` WHERE `status` = 'accepted' ORDER BY RAND() LIMIT 10", ARRAY_A);

    return $reviews;

};



/**
 * Include content with binded variables
 *
 * @param string $path
 * @param array $params
 */
function include_block($path, $params = array()){

    $mypage = PageData::getInstance();  // checking if this object is already created and get access to it

    include($path);

}

/**
 * Set theme translations directory
 */
add_action('after_setup_theme', 'my_theme_setup');

function my_theme_setup(){

    $domain = 'default';
    $path = get_stylesheet_directory() . '/languages/'; 
    $locale = apply_filters( 'theme_locale', get_locale(), $domain );
    load_textdomain( $domain, $path . $locale . '.mo' );

}


/**
 * Add current locale to body class
 */
add_filter('body_class','browser_body_class');
function browser_body_class($classes = []) {
    $classes[] = get_locale();
    return $classes;
}


function get_numeric_case($number, $check_float = false){

    $nmb = intval($number);
    $last = intval(substr($nmb, -1));

    if($check_float && $nmb != floatval($number)){

        return 0.1;

    }

    if($last > 0){

        $second_last = strlen($nmb) > 1 ? intval(substr($nmb, -2, 1)) : 0;

        if($second_last == 1){

            return 0;

        } else {

            if($last == 1) {

                return 1;

            } else {

                return 2;

            }

        }

    } else {

        return 0;

    }


}

function _x_lc($text, $context_lt = null){

    $locale = get_locale();

    if($locale == 'lt_LT' && $context_lt){

        return translate_with_gettext_context($text, $context_lt);

    } else {

        return translate($text);

    }

}


/**
 * Header UX representation type property for use in element class attribute
 * initiated from Template, used in header includes
 *
 * @param boolean $header_not_fixed_overide
 * @return string $header_class
 */
function get_main_header_type ($header_not_fixed_overide = null) {

    global $header_not_fixed;   // can be set as exception in Template file
                                // by default all pages should be fixed
    
    if (isset($header_not_fixed_overide)) {
        $localscope_header_not_fixed = $header_not_fixed_overide;   // use overide
    } else {
        $localscope_header_not_fixed = $header_not_fixed;   // use from template
    }

    if ($localscope_header_not_fixed) {
        /*not_fixed is true, so => regular*/
        $header_class = 'header-regular';
    } else {
        $header_class = 'header-fixed';
    }

    return $header_class;
}


################## PROMO !! #########

if( isset( $_GET['promo'] ) && !empty( $_GET['promo'] ) ) $_SESSION['promo'] = $_GET['promo'];
$whmcs_promo = isset( $_SESSION['promo'] ) && !empty( $_SESSION['promo'] ) ? $_SESSION['promo'] : '';
global $whmcs_promo;

####################################

#tag support for pages
function tags_support_all() {
    register_taxonomy_for_object_type('post_tag', 'page');
}
// ensure all tags are included in queries
function tags_support_query($wp_query) {
    if ($wp_query->get('tag')) $wp_query->set('post_type', 'any');
}
// tag hooks
add_action('init', 'tags_support_all');
add_action('pre_get_posts', 'tags_support_query');


/* Deregister emoji scripts and styling */

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


/* Language selector universal redirects */

function universal_redirect_footer( $links_array ) {

    global $site_current_url;   // (from config.php) declaring for var access
    
    // array structure: 'en'->'/some-permalink/'
    // 
    //var_dump($links_array);
    ?>
		<script>
			(function($) {
                var $replace_link_element = $('.js-lang-switch');
    <?php
        foreach ($links_array as $key => $value) :
    ?>            
                if ($replace_link_element.hasClass('<?php echo $key ?>')) {
                    $replace_link_element.attr('href','<?php echo $value ?>');
                }
    <?php
        endforeach; 
    ?>
                
			})(jQuery);
		</script>
	<?php
}



class PageData {

    /* -- General configuration -- */

    protected static $instance = NULL;
    
    protected static $service_locations_translate = array (
        // translation of locations to whmcs pricing functions
        'vps' => [
            'frankfurt' => 'frankfurt',
            'chicago' => 'chicago',
            'los_angeles' => 'los_angeles',
            'sao_paulo' => 'sao_paulo',
            'johannesburg' => 'johannesburg'
        ],
        'cs_lin' => [
            'frankfurt' => 'germany',   
            'chicago' => 'us_east',
            'sao_paulo' => 'brazil'
            // allowed values: germany, us_east
        ],
        'cs_win' => [
            'frankfurt' => 'germany',   
            'chicago' => 'us_east',
            'sao_paulo' => 'sao_paulo'
            // allowed values: germany, us_east
        ]
    );

    protected static $locations_oposite_translate = array (
        // this is translation from normalized naming to opposite
        'frankfurt' => 'germany',
        'chicago' => 'us_east',
        'los_angeles' => 'us_west',
        'sao_paulo' => 'brazil',
        'johannesburg' => 'africa'
    );

    protected static $service_activity_by_location = array(
        // values: active, pending, unavailable
        // if location is temporary closed - SET 'unavailable' VALUE HERE
        'frankfurt' => [
            'vps' => 'active',
            'cs_lin' => 'active',
            'cs_win' => 'active',
            'web' => 'active',
            'res' => 'active'
        ],
        'chicago' => [
            'vps' => 'active',
            'cs_lin' => 'active',
            'cs_win' => 'active'
            /*'web' => 'unavailable',*/
            /*'res' => 'unavailable'*/
        ],
        'los_angeles' => [
            'vps' => 'active',
            'web' => 'active'
        ],
        'sao_paulo' => [
            'vps' => 'active',
            'cs_lin' => 'active',
            'cs_win' => 'active',
            'web' => 'active',
            'res' => 'active'
        ],
        'johannesburg' => [
            'vps' => 'active'
            /*'web' => 'unavailable',*/
            /*'res' => 'unavailable'*/
        ],
        'amsterdam' => [
            /*'web' => 'active',*/
            /*'res' => 'active'*/
        ],
        'london' => [
            /*'web' => 'active',*/
        ],
        'siauliai' => [
            /*'web' => 'active',*/
            /*'res' => 'active'*/
        ],
        'hongkong' => [
            'vps' => 'pending',
            'cs_lin' => 'pending',
            'cs_win' => 'pending'
        ]
    );

    // this is to used with $service_locations_translate applied
    protected static $service_config = array(
        'vps' => [
            'public_promo' => NULL,
            'cheapest_location' => 'chicago',
            'default_location' => 'chicago'
        ],
        'cs_lin' => [
            'public_promo' => [
                /*'code' => 'CS50OFF',
                'price_ratio' => 0.5*/
            ],
            'cheapest_location' => 'frankfurt',   
            'default_location' => 'frankfurt'
        ],
        'cs_win' => [
            'public_promo' => [
                /*'code' => 'CS50OFF',
                'price_ratio' => 0.5*/
            ],
            'cheapest_location' => 'frankfurt',
            'default_location' => 'frankfurt'
        ], 
        'web' => [
            'public_promo' => [
                /*'code' => '40OFFWEB',
                'price_ratio' => 0.6*/
            ],
            // all prices the same
            'cheapest_location' => 'los_angeles',
            'default_location' => 'los_angeles'
        ],
        'res' => [
            'public_promo' => NULL,
            // all prices the same
            'cheapest_location' => 'frankfurt',
            'default_location' => 'frankfurt'
        ]
    );

    protected static $service_vps_cheapest_plan = 'amber';
    protected static $service_cs_lin_cheapest_plan = 'lin1';
    protected static $service_cs_win_cheapest_plan = 'win4';


    
    public $service;    // is the KEY of PageData object. All functions align returned values according $service dimensions
    public $promo;
    public $default_location;
    public $cheapest_location;
    public $min_price;

    private $whmcs;
    private $whmcs_settings;
    protected static $config; // have all $config available for usage with WHMCS pricing functions



    /**
    * Returns self object or creates new instance.
    * To be called instead of constructor !
    *
    * @access public
    * @static
    *
    * @param mixed $inst points what type of instance
    *
    * @return object
    */
    public static function getInstance($config = NULL, $service = NULL) {

        if ((!isset($config)) AND (self::$instance == NULL)) {
            return;     // silently fail
        }

        if( self::$instance == NULL ) {
            self::$instance = new self($config, $service); 
        }
        return self::$instance;
    }

    /**
     * Object constructor for setting all defaults
     * 
     * @param obj $config - the very global $config variable
     * @param string $service - short code for service: vps, cs
     */
    public function __construct() {

        $args = func_get_args(); 
        //echo 'There are '.count($args).' arguments supplied';
        $conf = func_get_arg(0);
        if (count($args) > 0) {
            $service = func_get_arg(1);
        }
        
        $this->whmcs = whmcsSync::getInstance();
        $this->whmcs_settings = whmcsSync::$settings;

        self::$config = $conf;
        if (isset($service)) {
            // this scenario is for defined service page
            $this->service = $service;
            $this->promo = $this->getPromo($service);   // this influences min_price calculation, must be first to set
            $this->default_location = $this->getDefaultLocation($service);
            $this->cheapest_location = $this->getCheapestLocation($service);
            $this->min_price = $this->getMinPrice();
        } else {
            // this scenario is for homepage or alike, to get overview of all services
        }
    }

    public function getPromo($service) {
        if (array_key_exists('code', self::$service_config)) {
            return self::$service_config[$service]['public_promo']['code'];
        } else {
            return NULL;
        }
    }

    /**
     * Minimum price for service (GENERAL). If no location specified, returns cheapest location min price.
     * 
     * @param string unified $location - frankfurt, chicago, los_angeles, sao_paulo, johannesburg, ... all by city name
     * @return string Currency and Price value in decimal
     */

    public function getMinPrice($location = NULL) {

        if (!isset($location)) {
            $location = $this->getCheapestLocation();
        }

        // now we have some location already defined
        if(!$this->isVisibleLocation($location)) {
            return NULL;
        }

        $currency = $this->whmcs_settings['currency_prefix'];

        switch ($this->service) {
            case 'vps': 
                $_location = $this->translateLocation($location);
                $id = self::$config['products']['vps_hosting']['locations'][$_location]['id'];
                $price_list_vps = $this->whmcs->getPriceOptions($id);
                // IMPORTANT CHANGE! - starting from semiannual billing cycle [0] -> [2]
                $price_value = $price_list_vps[2]['price_monthly_value'];
                break;
            case 'cs_lin':
                $_location = $this->translateLocation($location);
                $price_value = $this->whmcs->getCloudConfigurablePrice('linux' , $_location, 'LIN1' )[0]['price_value'];   
                break;
            case 'cs_win':
                $_location = $this->translateLocation($location);
                $price_value = $this->whmcs->getCloudConfigurablePrice('windows' , $_location, 'WIN4' )[0]['price_value']; 
                break;
            case 'web':
                $id = self::$config['products']['web_hosting']['plans'][1]['id'];
                $price_list_web = $this->whmcs->getPriceOptions($id);
                $price_value = $price_list_web[0]['price_monthly_value'];
                break;
            case 'res':
                $id = self::$config['products']['reseller_hosting']['plans'][1]['id'];
                $price_list_res = $this->whmcs->getPriceOptions($id);
                $price_value = $price_list_res[0]['price_value'];
        }

        if ($price_value == NULL) {
            return NULL;    // location parameter was wrong
        }
        
        if ($this->promo) {
            $price_with_promo = $price_value * self::$service_config[$this->service]['public_promo']['price_ratio'];
            $price = $currency . number_format((float)$price_with_promo, 2, '.', '');
        } else {
            $price = $currency . number_format((float)$price_value, 2, '.', '');
        }

            /*echo '<pre>';
            var_dump($price);
            echo '</pre>';*/

        return $price;
    }

    /**
     * Minimum price for service (WHEN NO SERVICE DEFINED). 
     * 
     * @param string $service - frankfurt, chicago, los_angeles, sao_paulo, johannesburg, ... all by city name
     * @param string unified $location - frankfurt, chicago, los_angeles, sao_paulo, johannesburg, ... all by city name
     * @return string Currency and Price value in decimal
     */

    public function getServiceMinPrice($service, $location = NULL) {

        if (!isset($location)) {
            $location = $this->getCheapestLocation($service);
        }

        // now we have some location already defined
        if(!$this->isVisibleLocation($location, $service)) {
            return NULL;
        }

        $currency = $this->whmcs_settings['currency_prefix'];

        switch ($service) {
            case 'vps': 
                $_location = $this->translateLocation($location, $service);
                $id = self::$config['products']['vps_hosting']['locations'][$_location]['id'];
                $price_list_vps = $this->whmcs->getPriceOptions($id);
                // IMPORTANT CHANGE! - starting from semiannual billing cycle [0] -> [2]
                $price_value = $price_list_vps[2]['price_monthly_value'];
                break;
            case 'cs_lin':
                $_location = $this->translateLocation($location, $service);
                $price_value = $this->whmcs->getCloudConfigurablePrice('linux' , $_location, 'LIN1' )[0]['price_value'];
                break;
            case 'cs_win':
                $_location = $this->translateLocation($location, $service);
                $price_value = $this->whmcs->getCloudConfigurablePrice('windows' , $_location, 'WIN4' )[0]['price_value'];
                break;
            case 'web':
                $id = self::$config['products']['web_hosting']['plans'][1]['id'];
                $price_list_web = $this->whmcs->getPriceOptions($id);
                $price_value = $price_list_web[0]['price_monthly_value'];
                break;
            case 'res':
                $id = self::$config['products']['reseller_hosting']['plans'][1]['id'];
                $price_list_res = $this->whmcs->getPriceOptions($id);
                $price_value = $price_list_res[0]['price_value'];
        }

        if ($price_value == NULL) {
            return NULL;    // location parameter was wrong
        }
        
        if ($this->getPromo($service)) {
            $price_with_promo = $price_value * self::$service_config[$service]['public_promo']['price_ratio'];
            $price = $currency . number_format((float)$price_with_promo, 2, '.', '');
        } else {
            $price = $currency . number_format((float)$price_value, 2, '.', '');
        }

        return $price;
    }

    /**
     * Helper for checking if location should exist on the map (active and pending)
     * 
     * @param string unified $location - frankfurt, chicago, los_angeles, sao_paulo, johannesburg, ... all by city name
     * @return boolean
     */
    public function isVisibleLocation ($location, $service = NULL) {
        if (!isset($service)) {
            $service = $this->service;
        }
        $activity_status = self::$service_activity_by_location[$location][$service];
        if (in_array($activity_status, array('active','pending','unavailable'))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Helper for checking if location is active for sales and can have a pricing
     * 
     * @param string unified $location - frankfurt, chicago, los_angeles, sao_paulo, johannesburg, ... all by city name
     * @return boolean
     */
    public function isValidLocation ($location) {
        if (!isset($this->service)) {
            return false;
        }

        $activity_status = self::$service_activity_by_location[$location][$this->service];
        if ($activity_status == 'active') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Helper for checking if location is unavailable for sales - to have ability showing "Temporary unavailable"
     * 
     * @param string unified $location - frankfurt, chicago, los_angeles, sao_paulo, johannesburg, ... all by city name
     * @return boolean
     */
    public function isUnavailableLocation ($location) {
        if (!isset($this->service)) {
            return false;
        }

        $activity_status = self::$service_activity_by_location[$location][$this->service];
        if ($activity_status == 'unavailable') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Helper for checking if location is pending for installation
     * 
     * @param string unified $location - frankfurt, chicago, los_angeles, sao_paulo, johannesburg, ... all by city name
     * @return boolean
     */
    public function isPendingLocation ($location) {
        if (!isset($this->service)) {
            return false;
        }

        $activity_status = self::$service_activity_by_location[$location][$this->service];
        if ($activity_status == 'pending') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Helper for translating different locations naming to unified name
     * 
     * @param string unified $location - frankfurt, chicago, los_angeles, sao_paulo, johannesburg, ... all by city name
     * @return string The exact location naming needed for different services
     */
    public function translateLocation($location, $service = NULL) {
        if (!isset($service)) {
            $service = $this->service;
        }
        return self::$service_locations_translate[$service][$location];
    }

    public function translateLocationOposite($location) {
        return self::$locations_oposite_translate[$location];
    }

    public function getDefaultLocation($service = NULL) {
        if (!isset($service)) {
            $service = $this->service;
        }
        return self::$service_config[$service]['default_location'];
        // no need for location translation
    }

    public function getCheapestLocation($service = NULL) {
        if (!isset($service)) {
            $service = $this->service;
        }
        return self::$service_config[$service]['cheapest_location'];
        // no need for location translation
    }

    public function getLocationMinPrice ($location) {
        return $this->getMinPrice($location);
        // no need for location translation
    }

    private function getCloudServicePid ($location) {
        
        $location_translation = array(
            'sao_paulo' => 'brazil',
            'frankfurt' => 'germany',
            'chicago' => 'us_east',
            'los_angeles' => 'us_west',
            'johannesburg' => 'africa'
        );
        $_location = $location_translation[$location];
        
        $cloud_data = $this->whmcs->get_local_stepsconfig('cloud_plans');

        switch ($this->service) {
            case 'cs_lin':
                $os = 'linux';
                break;
            case 'cs_win':
                $os = 'windows';
        }
        $cloud_location_pid = array(
            'chicago' => $cloud_data[$os][$location_translation['chicago']]['pid'],
            'frankfurt' => $cloud_data[$os][$location_translation['frankfurt']]['pid'],
            'sao_paulo' => $cloud_data[$os][$location_translation['sao_paulo']]['pid']
        );

        return $cloud_location_pid[$location];
    }

    /**
     * Checkout button link in <a href..> element
     * 
     * @param string unified $location - frankfurt, chicago, los_angeles, sao_paulo, johannesburg, ... all by city name
     * @return string link to checkout
     */

    public function getMinPriceCheckoutLink ($location = NULL) {
        // links are made using GET method

        if (!isset($location)) {
            $location = $this->getCheapestLocation();
        }

        // if we cannot give valid pricing, redirect to main service page
        if (!$this->isValidLocation($location)) {
            return NULL;
        }
            
            /* Simplify! We'll give no link if not valid
               ----------------------------------------- 
            
            switch ($this->service) {
                case 'vps':
                    $link = self::$config['links']['vps-hosting'];
                    break;
                case 'cs_lin':
                    // intentional break; skipping
                case 'cs_win':
                    $link = self::$config['links']['cloud-servers'];
                    break;
                case 'web':
                    $link = self::$config['links']['web-hosting'];
                    break;
                case 'res':
                    $link = self::$config['links']['reseller-hosting'];
                default:
                    // wrong service, redirect to homepage
                    $link = self::$config['links']['website'];
            }
            return $link;
        } else {
            // location invalid, redirect to homepage
            $link = self::$config['links']['website'];
        }*/
        
        switch ($this->service) {
            case 'vps': 
                $_location = $this->translateLocationOposite($location);
                $vps_locations = self::$config['products']['vps_hosting']['locations'];
                $link = self::$config['whmcs_links']['checkout_vps'].'?force_clean=1&plan=amber&location='.$_location.'&pid='.$vps_locations[$location]['id'];
                break;
            case 'cs_lin':
                $pid_key = $this->getCloudServicePid($location);
                $link = self::$config['whmcs_links']['checkout_cloud'].'?force_clean=1&plan=LIN1&pid='.$pid_key;
                break;
            case 'cs_win':
                $pid_key = $this->getCloudServicePid($location);
                $link = self::$config['whmcs_links']['checkout_cloud'].'?force_clean=1&plan=WIN4&pid='.$pid_key;
                break;
            case 'web':
                // Generate no checkout links for Web hosting and Reseller hosting
                // intentional break; skipping
            case 'res':
                $link = '#';    // means as NULL
        }

        if (($this->promo) AND ($this->service != 'web') AND ($this->service != 'res')) {
            $link .= '&promocode='.$this->promo;
        };

        return $link;
    }


    /**
     * Outputs class for Checkout button depending on location activeness
     * 
     * @param string unified $location - frankfurt, chicago, los_angeles, sao_paulo, johannesburg, ... all by city name
     * @return string Class name
     */
    public function checkoutActivityClass ($location) {

        // now we have some location already defined
        if($this->isValidLocation($location)) {
            return "active";
        } else return "disabled";

    }

    /**
     * Checkout button with via <form> generation
     * 
     * @param string unified $location - frankfurt, chicago, los_angeles, sao_paulo, johannesburg, ... all by city name
     * @return string The full HTML for <form> element depending on service type and location
     */
    public function checkoutFormLocations ($location) {

        // now we have some location already defined
        if(!$this->isValidLocation($location)) {
            return NULL;
        }

        // NOTICE: mind the default billing cycle, should correlate with pricing table

        $web_locations_id = array (
            'los_angeles' => '2055',    // cP
            'chicago' => '4146',        // cP
            'london' => '2052',         // DirectAdmin
            'amsterdam' => '3999',      // cP
            'johannesburg' => '4000',   // cP
            'siauliai'  => '3486',      // cP
            'frankfurt' => '2190',      // cP
            'sao_paulo' => '5446'       // cP
        );
        $vps_locations = self::$config['products']['vps_hosting']['locations'];

        switch ($this->service) {
            case 'vps':
                $checkout_link = self::$config['whmcs_links']['checkout_vps'];
                $pid_key = $vps_locations[$location]['id'];
                $plan_key = self::$service_vps_cheapest_plan;
                break;
            case 'cs_lin':
                $checkout_link = self::$config['whmcs_links']['checkout_cloud'];
                $pid_key = $this->getCloudServicePid($location);
                $plan_key = strtoupper (self::$service_cs_lin_cheapest_plan);   // exception with strtoupper for cloud plans
                break;
            case 'cs_win':
                $checkout_link = self::$config['whmcs_links']['checkout_cloud'];
                $pid_key = $this->getCloudServicePid($location);
                $plan_key = strtoupper (self::$service_cs_win_cheapest_plan);   // exception with strtoupper for cloud plans
                break;
            case 'web':
                $checkout_link = self::$config['whmcs_links']['checkout_web'];
                $pid_key = self::$config['products']['web_hosting']['plans'][1]['id'];
                break;
            case 'res':
                $checkout_link = self::$config['whmcs_links']['checkout_web'];
                $pid_key = self::$config['products']['reseller_hosting']['plans'][1]['id'];
        }

        ob_start();
        ?>
        
        <form action="<?php echo $checkout_link ?>" method="get">
            <input type="hidden" name="a" value="add"/>
            <input type="hidden" name="pid" value="<?php echo $pid_key; ?>"/>
            <?php if (($this->service == 'web') OR ($this->service == 'res')) : ?>
            <input type="hidden" name="configoption[301]" value="<?php echo $web_locations_id[$location];?>"/>
            <input type="hidden" name="billingcycle" value="annually">
            <?php endif; ?>
            <?php if (($this->service == 'vps') OR ($this->service == 'cs_lin') OR ($this->service == 'cs_win')) : ?>
            <input type="hidden" name="plan" value="<?php echo $plan_key; ?>"/>
            <?php endif; ?>
            <input type="hidden" name="language" value="<?php echo self::$config['whmcs_lang']; ?>"/>
            <input type="hidden" name="currency" value="<?php echo self::$config['whmcs_curr']; ?>"/>
            <?php if ($this->service == 'vps') : ?>
            <input type="hidden" name="location" value="<?php echo $vps_locations[$location]['key']; ?>"/>
            <input type="hidden" name="billingcycle" value="semiannually">
            <?php endif; ?>
            <?php if (($this->service == 'cs_lin') OR ($this->service == 'cs_win')) : ?>
            <input type="hidden" name="billingcycle" value="quarterly">
            <?php endif; ?>
            <?php if ($this->promo) : ?>
            <input type="hidden" name="promocode" value="<?php echo $this->promo;?>"/>
            <?php endif; ?>
            <button type="submit" class="button small ghost checkout-link"><?php _e('Get Started')?></button>
        </form>

        <?php 
        $output = ob_get_clean();

        // ATTENTION Resellers hosting does not form this block;
        if ($this->service != 'res') {
            return $output;
        } else {
            return NULL;
        }
    }

}