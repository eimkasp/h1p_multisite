<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: cheap VPS
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);


wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

get_header(); ?>

<div id="cheap-vps">

    <div class="landing-header tablet-pad">
        <h1 class="page-title">Cheap VPS Hosting</h1>
        <h2 class="page-subtitle">Affordable VPS hosting in 5 worldwide locations</h2>

        <div class="choose-location">

            <div class="title">Choose your server location</div>
            <div><span class="arrow-down"></span></div>
            <div class="locations-list">

                <?php
                    $plan = 'amber';
                    $planConfig = $whmcs->get_local_stepsconfig('vps_plans')[ $plan ];
                    $locations = $config['products']['vps_hosting']['locations'];
                    $first_loc_override_key = 'chicago';
                    if( array_key_exists( $first_loc_override_key, $locations) ){
                        $first_loc_override = $locations[$first_loc_override_key]; //which location should be first
                        unset( $locations[$first_loc_override_key] );
                        $locations = [$first_loc_override_key => $first_loc_override] + $locations;
                    }
                    foreach($locations as $location_key => $location):

                        $prices = $whmcs->getConfigurablePrice($location['id'], $planConfig['configs']);
                        /*echo'<pre>';
                        var_dump( $prices );
                        echo'<pre>';*/
                 ?>

                    <div class="location" onclick="$(this).find('form').submit();">
                        <form class="" action="<?php echo $config['whmcs_links']['checkout_vps']?>" method="get">

                            <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                            <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                            <input type="hidden" name="location" value="<?php echo $location['key'] ?>"/>
                            <input type="hidden" name="plan" value="amber"/>

                            <div class="label">
                                <?php
                                switch ($location_key){
                                    case 'los_angeles':
                                        echo __('Los Angeles');
                                        break;
                                    case 'chicago':
                                        echo __('Chicago');
                                        break;
                                    case 'sao_paulo':
                                        echo __('São Paulo');
                                        break;
                                    case 'frankfurt':
                                        echo __('Frankfurt');
                                        break;
                                    case 'johannesburg':
                                        echo __('Johannesburg');
                                        break;
                                }
                                ?>
                            </div>
                            <div><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/<?php echo strtoupper( $location['country_code'] ) ?>.png" alt="" width="80px"/></div>
                            <div class="price-notice">from <span class="price"><?php echo $prices[0]['price']; ?></span> / month</div>
                        </form>
                    </div>

                <?php endforeach; ?>
            </div>
            <div class="notice compare" onclick="$('html, body').animate({scrollTop: $('#vps-compare').offset().top}, 2000);">
                Compare our offer with other VPS hosting providers
            </div>
        </div>

    </div>

    <div class="landing-notice">
        <div class="container">
            <div class="row">
                <div class="side left">
                    <div class="ln1">VPS HOSTING 10% OFF</div>
                    <div class="ln2">Get a 10% discount on your first order for all VPS plans, except Amber.</div>
                </div>
                <div class="side right">
                    <div class="ln1">PROMO CODE</div>
                    <div class="ln2">VPS10OFF</div>
                </div>
            </div>
        </div>
    </div>

    <div class="features-boxes container">
        <h2 class="title">
            Our VPS hosting features
        </h2>
        <div class="feature-box">
            <div class="fico">
                <i class="ico scalable"></i>
            </div>
            <div class="ftitle">Scalable Resources</div>
            <div class="fcontent">
                Pay for what you use! Customize the resources according your specific needs and stay on a budget!
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico locations"></i>
            </div>
            <div class="ftitle">Multiple Locations</div>
            <div class="fcontent">
                5 VPS hosting locations over 4 continents guarantee lower latency and better network speed.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico dns"></i>
            </div>
            <div class="ftitle">DNS Management</div>
            <div class="fcontent">
                Manage your server without struggle! Now you can easily create your domain zones and edit its records with no extra support through our Client Area.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico protection"></i>
            </div>
            <div class="ftitle">DDoS Protection</div>
            <div class="fcontent">
                Enjoy safe online environment and avoid interruptions with fully DDoS protected VPS services.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico linux"></i>
            </div>
            <div class="ftitle">Linux OS Templates</div>
            <div class="fcontent">
                Power up your cheap VPS server with a range of Linux distributions such as Ubuntu, Fedora, Debian, CentOS or Suse.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico console"></i>
            </div>
            <div class="ftitle">Full Root Access</div>
            <div class="fcontent">
                Enjoy full authority over your server and edit or install various programs according to your needs.
            </div>
        </div>
    </div>

    <?php include __DIR__ . '/../htmlblocks/reviews_2.php'; ?>

    <div class="what-else">
        <h2 class="title">Why choose Host1Plus?</h2>

        <div class="block-tabs">
            <div class="tabs-wrapper">
                <div class="app-tabs">
                    <div class="table">
                    <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active">MONEY-BACK GUARANTEE</span></div>
                        <div class="cell"><span data-tab-link="feature.hidden-fees" class="tab desktop">NO HIDDEN FEES</span></div>
                        <div class="cell"><span data-tab-link="feature.responsive" class="tab desktop">RESPONSIVE CLIENT AREA</span></div>
                        <div class="cell"><span data-tab-link="feature.multilingual" class="tab desktop">MULTILINGUAL TECH SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.savings" class="tab desktop">MAJOR SAVINGS</span></div>
                    </div>
                </div>
                <div class="tabs" data-tabs="feature">

                    <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">MONEY-BACK GUARANTEE</span>
                    <div class="tab-content active" data-tab-id="money-back">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.jpg">
                        <span class="title">MONEY-BACK GUARANTEE</span>
                        <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
                    </div>

                    <span data-tab-link="feature.hidden-fees" data-tab-toggle="self" class="tab mob">NO HIDDEN FEES</span>
                    <div class="tab-content" data-tab-id="hidden-fees">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/hidden-fees.jpg">
                        <span class="title">NO HIDDEN FEES</span>
                        <p>Fair prices guaranteed! Enjoy your hosting services with no extra costs.</p>
                    </div>

                    <span data-tab-link="feature.responsive" data-tab-toggle="self" class="tab mob">RESPONSIVE CLIENT AREA</span>
                    <div class="tab-content" data-tab-id="responsive">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/responsive.jpg">
                        <span class="title">RESPONSIVE CLIENT AREA</span>
                        <p>Always on the go? Easily manage your services using any mobile device.</p>
                    </div>

                    <span data-tab-link="feature.multilingual" data-tab-toggle="self" class="tab mob">MULTILINGUAL TECH SUPPORT</span>
                    <div class="tab-content" data-tab-id="multilingual">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/multilingual.jpg">
                        <span class="title">MULTILINGUAL TECH SUPPORT</span>
                        <p>No more language barriers! We speak English, Lithuanian, Spanish and Portuguese.</p>
                    </div>

                    <span data-tab-link="feature.savings" data-tab-toggle="self" class="tab mob">MAJOR SAVINGS</span>
                    <div class="tab-content" data-tab-id="savings">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/savings.jpg">
                        <span class="title">MAJOR SAVINGS</span>
                        <p>Sign up for an extended billing cycle and save up to 11% for your purchase!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include __DIR__ . '/block-vps-locations.php'; ?>


    <div id="vps-compare" class="compare-block">
        <div class="container">
            <h2 class="title">Compare our budget VPS offer with other hosting providers</h2>

            <div class="table-wrap">

                <table class="compare-6">
                    <tbody>
                        <tr>
                            <td class="left w220 date">
                                Data taken on 2015/09/02
                            </td>
                            <td class="h1p logo-2">
                                <div class="pic darker host1plus"></div>
                                <div class="title">Host1Plus</div>
                            </td>
                            <td class="logo-2">
                                <div class="pic darker digital-ocean"></div>
                                <div class="title">Digital Ocean</div>
                            </td>
                            <td class="logo-2">
                                <div class="pic darker linode"></div>
                                <div class="title">Linode</div>
                            </td>
                            <td class="logo-2">
                                <div class="pic darker godaddy"></div>
                                <div class="title">GoDaddy</div>
                            </td>
                            <td class="logo-2">
                                <div class="pic darker and1"></div>
                                <div class="title">1and1</div>
                            </td>
                            <td class="logo-2">
                                <div class="pic darker hostgator"></div>
                                <div class="title">Hostgator</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature tsmaller-14 left w220">
                                CPU
                            </td>
                            <td class="value tsmaller-16 h1p">
                                2
                            </td>
                            <td class="value tsmaller-16">
                                2
                            </td>
                            <td class="value tsmaller-16">
                                2
                            </td>
                            <td class="value tsmaller-16">
                                2
                            </td>
                            <td class="value tsmaller-16">
                                2
                            </td>
                            <td class="value tsmaller-16">
                                2
                            </td>
                        </tr>
                        <tr>
                            <td class="feature tsmaller-14 left w220">
                                RAM
                            </td>
                            <td class="value h1p tsmaller-16">
                                2 GB
                            </td>
                            <td class="value tsmaller-16">
                                2 GB
                            </td>
                            <td class="value tsmaller-16">
                                2 GB
                            </td>
                            <td class="value tsmaller-16">
                                2 GB
                            </td>
                            <td class="value tsmaller-16">
                                2 GB
                            </td>
                            <td class="value tsmaller-16">
                                2 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left tsmaller-14 w220">
                                Disk Space
                            </td>
                            <td class="value h1p tsmaller-16">
                                80 GB
                            </td>
                            <td class="value tsmaller-16">
                                40 GB
                            </td>
                            <td class="value tsmaller-16">
                                48 GB
                            </td>
                            <td class="value tsmaller-16">
                                60 GB
                            </td>
                            <td class="value tsmaller-16">
                                150 GB
                            </td>
                            <td class="value tsmaller-16">
                                120 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left tsmaller-14 w220">
                                Bandwidth
                            </td>
                            <td class="value h1p tsmaller-16">
                                2000 GB
                            </td>
                            <td class="value tsmaller-16">
                                3000 GB
                            </td>
                            <td class="value tsmaller-16">
                                3000 GB
                            </td>
                            <td class="value tsmaller-16">
                                2000 GB
                            </td>
                            <td class="value tsmaller-16">
                                Unlimited
                            </td>
                            <td class="value tsmaller-16">
                                1500 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left tsmaller-14 w220">
                                DDoS Protection
                            </td>
                            <td class="value h1p tsmaller-16">
                                <i class="haveit yes white"></i>
                            </td>
                            <td class="value tsmaller-16">
                                <i class="haveit no"></i>
                            </td>
                            <td class="value tsmaller-16">
                                <i class="haveit no"></i>
                            </td>
                            <td class="value">
                                <i class="haveit yes"></i>
                            </td>
                            <td class="value">
                                <i class="haveit no"></i>
                            </td>
                            <td class="value">
                                <i class="haveit yes"></i>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left tsmaller-14 w220">
                                Root Access
                            </td>
                            <td class="value h1p">
                                <i class="haveit yes white"></i>
                            </td>
                            <td class="value">
                                <i class="haveit yes"></i>
                            </td>
                            <td class="value">
                                <i class="haveit yes"></i>
                            </td>
                            <td class="value">
                                <i class="haveit yes"></i>
                            </td>
                            <td class="value">
                                <i class="haveit yes"></i>
                            </td>
                            <td class="value">
                                <i class="haveit yes"></i>
                            </td>
                        </tr>
                            <td class="feature left tsmaller-14 w220">
                                Available locations
                            </td>
                            <td class="value h1p info-tooltip tsmaller-16">
                                <div style="position:relative;">
                                    5
                                    <div class="tcontent">
                                        <div class="triangle"></div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/US.png" alt="" /> Chicago, USA
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/US.png" alt="" /> Los Angeles, USA
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/DE.png" alt="" /> Frankfurt, Germany
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/BR.png" alt="" /> Sao Paulo, Brazil
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/ZA.png" alt="" /> Johannesburg, South Africa
                                        </div>
                                    </div>
                                </div>
                                <div class="handle"></div>
                            </td>
                            <td class="value info-tooltip tsmaller-16">
                                <div style="position:relative;">
                                    6
                                    <div class="tcontent">
                                        <div class="triangle"></div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/US.png" alt="" /> San Francisko, USA
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/US.png" alt="" /> New York, USA
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/GB.png" alt="" /> London, UK
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/NL.png" alt="" /> Amsterdam, The Netherlands
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/DE.png" alt="" /> Frankfurt, Germany
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/SG.png" alt="" /> Singapore
                                        </div>
                                    </div>
                                </div>
                                <div class="handle"></div>
                            </td>
                            <td class="value info-tooltip tsmaller-16">
                                <div style="position:relative;">
                                    8
                                    <div class="tcontent">
                                        <div class="triangle"></div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/US.png" alt="" /> Newark, USA
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/US.png" alt="" /> Atlanta, USA
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/US.png" alt="" /> Dallas, USA
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/US.png" alt="" /> Fremont, USA
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/DE.png" alt="" /> Frankfurt, Germany
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/GB.png" alt="" /> London, UK
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/SG.png" alt="" /> Singapore
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/JP.png" alt="" /> Tokyo, Japan
                                        </div>
                                    </div>
                                </div>
                                <div class="handle"></div>
                            </td>
                            <td class="value info-tooltip tsmaller-16">
                                <div style="position:relative;">
                                    1
                                    <div class="tcontent">
                                        <div class="triangle"></div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/US.png" alt="" /> Arizona, USA
                                        </div>
                                    </div>
                                </div>
                                <div class="handle"></div>
                            </td>
                            <td class="value info-tooltip tsmaller-16">
                                <div style="position:relative;">
                                    4
                                    <div class="tcontent">
                                        <div class="triangle"></div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/US.png" alt="" /> Kansas, USA
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/US.png" alt="" /> Pennsylvania, USA
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/DE.png" alt="" /> Germany
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/GB.png" alt="" /> United Kingdom
                                        </div>
                                    </div>
                                </div>
                                <div class="handle"></div>
                            </td>
                            <td class="value info-tooltip tsmaller-16">
                                <div style="position:relative;">
                                    2
                                    <div class="tcontent">
                                        <div class="triangle"></div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/US.png" alt="" /> Provo, USA
                                        </div>
                                        <div class="loc-row">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/24/US.png" alt="" /> Houston, USA
                                        </div>
                                    </div>
                                </div>
                                <div class="handle"></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left tsmaller-14 w220">
                                Price / mo
                            </td>
                            <td class="value h1p price orange">
                                <span class="prefix">$</span>14.70
                            </td>
                            <td class="value price orange">
                                <span class="prefix">$</span>20.00
                            </td>
                            <td class="value price orange">
                                <span class="prefix">$</span>20.00
                            </td>
                            <td class="value price orange">
                                <span class="prefix">$</span>39.99
                            </td>
                            <td class="value price orange">
                                <span class="prefix">$</span>19.99
                            </td>
                            <td class="value price orange">
                                <span class="prefix">$</span>79.95
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>

            <div class="compare-notice">
                *Third-party logos and marks are registered trademarks of their respective owners. All rights reserved.
            </div>

        </div>

    </div>

    <div class="faq2">
        <h2 class="title">Ask questions, get answers!</h2>
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq">FAQ</h3>
            <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    What is VPS hosting?
                    <div class="answer">
                        VPS is an abbreviation of Virtual Private Server. A VPS server contains both features of shared and dedicated server hosting. VPS is located on a physical machine that contains other VPS servers as well, but it functions independently of any other servers on the machine as a dedicated server.
                    </div>
                </div>
                <div class="question">
                    How VPS hosting differs from shared hosting?
                    <div class="answer">
                        While in most of the cases VPS and shared hosting has its similarities, the differences are more significant. <br>
                        Shared and VPS hosting are both located on a physical machine that contains other servers at the same time. While using shared hosting – other servers can access the resources you have on your server and use them accordingly, while VPS works more as a separate independent server not accessible for other servers on the same physical machine. Thus VPS provides greater resource availability, security and control than shared hosting.
                    </div>
                </div>
                <div class="question">
                    Do you provide VPS hosting based not on the Linux OS?
                    <div class="answer">
                        Our low cost VPS hosting servers are only based on Linux operating system. During the ordering process you can choose from various offered instances of Linux OS templates - Ubuntu, Fedora, Debian, CentOS and Suse. However, if you are looking for Windows OS, you can try our <a href="<?php echo $config['links']['dedicated-servers']?>">dedicated hosting</a>.
                    </div>
                </div>
                <div class="question">
                    What are the possible locations for Linux VPS hosting?
                    <div class="answer">
                        Currently we provide cheap Linux VPS hosting in 5 different locations – USA (Los Angeles, Chicago), Germany, Brazil and South Africa.
                    </div>
                </div>
                <div class="question">
                    What happens when VPS bandwidth limit is reached?
                    <div class="answer">
                        If your server uses a lot of network resources, there is a chance that you will reach your monthly VPS bandwidth limit. After the limit is exceeded, the network speed of your server will become limited to 64 Kbits/s. You will also receive a notice about this limitation via email.
                        There are a few options if you want to solve this problem:<br>
                        <br>
                        1)	Wait for the end of the month (or the end of your billing cycle) when the bandwidth count starts over.<br>
                        2)	Upgrade your bandwidth resources at your Client Area as your VPS is fully customizable. However, in this case, the price of your VPS would increase.
                    </div>
                </div>
                <div class="question">
                    What is the network speed of your Linux VPS hosting?
                    <div class="answer">
                        We limit the network speed provided depending on the location of your VPS hosting service.<br>
                        <br>
                        <strong>Outgoing</strong> (data that is being sent from your server to another one) network speed is limited up to 500 Mbps in all VPS hosting locations except São Paulo, Brazil, where network speed limit is higher - up to 200 Mbps.<br>
                        <br>
                        <strong>Incoming</strong> (data that is being transferred to your server from another one) network speed does not have any limitations.
                    </div>
                </div>
            </div>
            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/coding-scripting/ssh-bash/beginner-ssh-bash/common-ssh-commands"><i class="fa fa-file-text-o file-ico"></i> Common SSH Commands</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/common-log-files-on-a-linux-system"><i class="fa fa-file-text-o file-ico"></i> Common log files on a Linux system</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/cloud-hosting-services/all-cloud-hosting-tutorials/how-to-install-ssl-2"><i class="fa fa-file-text-o file-ico"></i> How to Install SSL</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-change-a-time-zone-on-a-vps-server"><i class="fa fa-file-text-o file-ico"></i> How to change a time zone on a VPS server? </a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/logs-on-linux-vps/"><i class="fa fa-file-text-o file-ico"></i> Where can I find logs on my Linux VPS</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/how-to-set-up-openvpn-on-linux-vps"><i class="fa fa-file-text-o file-ico"></i> How to set up OpenVPN on Linux VPS </a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/how-to-install-csf-on-linux-vps"><i class="fa fa-file-text-o file-ico"></i> How to install CSF on Linux VPS </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="back-to-top">
        <div class="back-box">
            <table>
            <tbody>
            <tr>
                <td><div class="main-text">Choose the right VPS plan for you!</div></td>
                <td><a href="<?php echo $config['whmcs_links']['checkout_vps']?>?promocode=VPS10OFF&plan=amber" class="button">Explore Now</a></td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>

    <?php /*
    <div class="prefooter">
        <h3 class="sub-title">Cheap VPS Hosting at Host1Plus</h3>
        <p>
            <span style="font-weight:300;font-size:28px;">Get your online projects running for only</span><br>
            <span style="font-weight:600;font-size:30px;line-height: 30px;">$2.50 per month.</span>
        </p>
        <a href="#" class="button orange">GET STARTED</a>
    </div>
    */ ?>

</div>

<?php get_footer(); ?>
