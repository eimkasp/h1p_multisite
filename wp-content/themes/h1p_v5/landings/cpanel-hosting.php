<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: cPanel Hosting
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);


wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

get_header(); ?>

<div id="cpanel-hosting" class="landing-hosting">

    <div class="landing-header tablet-pad">
        <h1 class="page-title">cPanel web hosting</h1>
        <h2 class="page-subtitle">Easier hands-on management with the most popular control panel.</h2>

        <div class="plansnpricing">
            <div class="title">Plans & prices</div>
            <div class="arrow_down"></div>
        </div>
        <div class="webhosting-info container">
            <?php include( __DIR__ . "/../htmlblocks/pricing-tables/web-hosting.php"); ?>
            <div class="title">cPanel Hosting Locations</div>
            <div class="locations">
                <div class="loc">
                    <div class="ico flag us"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/US.png" alt="" width="48px"/></div>
                    <div class="cityname">Los Angeles</div>
                    <div class="countryname">United States</div>
                </div>
                    <div class="loc">
                        <div class="ico flag us"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/US.png" alt="" width="48px"/></div>
                        <div class="cityname">Chicago</div>
                        <div class="countryname">United States</div>
                    </div>
                <div class="loc">
                    <div class="ico flag nl"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/NL.png" alt="" width="48px"/></div>
                    <div class="cityname">Amsterdam</div>
                    <div class="countryname">The Netherlands</div>
                </div>
                <div class="loc">
                    <div class="ico flag lt"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/LT.png" alt="" width="48px"/></div>
                    <div class="cityname">Siauliai</div>
                    <div class="countryname">Lithuania</div>
                </div>
                <div class="loc">
                    <div class="ico flag za"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/ZA.png" alt="" width="48px"/></div>
                    <div class="cityname">Johannesburg</div>
                    <div class="countryname">South Africa</div>
                </div>
                <div class="loc">
                    <div class="ico flag de"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/DE.png" alt="" width="48px"/></div>
                    <div class="cityname">Frankfurt</div>
                    <div class="countryname">Germany</div>
                </div>
                <div class="loc">
                    <div class="ico flag br"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/BR.png" alt="" width="48px"/></div>
                    <div class="cityname">Sao Paulo</div>
                    <div class="countryname">Brazil</div>
                </div>
            </div>
        </div>
    </div>

    <div class="features-boxes container">
        <h2 class="title">
            Why choose cPanel hosting?
        </h2>
        <div class="feature-box">
            <div class="fico">
                <i class="ico cpanel"></i>
            </div>
            <div class="ftitle">Most popular</div>
            <div class="fcontent">
                cPanel is the most popular and well-trusted control panel in the market which ensures easy management for your web hosting services.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico setup"></i>
            </div>
            <div class="ftitle">Instant setup</div>
            <div class="fcontent">
                Get your web hosting account up and running in just a few minutes! Sign up, log in, lay your hands on cPanel and power up your website in no time.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico optimized"></i>
            </div>
            <div class="ftitle">Optimized servers</div>
            <div class="fcontent">
                Both your control panel and your website work even better when it’s run on our cPanel optimized servers. Give it a shot!
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico speed"></i>
            </div>
            <div class="ftitle">Speedy management</div>
            <div class="fcontent">
                Access all your web hosting features without struggle – manage your website settings, adjust your email settings and check your logs.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico regular"></i>
            </div>
            <div class="ftitle">Free regular backups</div>
            <div class="fcontent">
                Let us take care of everything! We take regular backups to increase your data protection and make sure your work isn’t lost in any circumstances.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico statistics"></i>
            </div>
            <div class="ftitle">Full statistics</div>
            <div class="fcontent">
                Track your website, traffic and bandwidth usage statistics to increase your overall performance.
            </div>
        </div>
    </div>

        <?php include __DIR__ . '/../htmlblocks/reviews_2.php'; ?>

    <div class="what-else">
        <h2 class="title">Why choose Host1Plus?</h2>

        <div class="block-tabs">
            <div class="tabs-wrapper">
                <div class="app-tabs">
                    <div class="table">
                        <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active">14-DAY MONEY-BACK GUARANTEE</span></div>
                        <div class="cell"><span data-tab-link="feature.support" class="tab desktop">24/7 CUSTOMER SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.uptime" class="tab desktop">99.9% UPTIME</span></div>
                        <div class="cell"><span data-tab-link="feature.flexible" class="tab desktop">FLEXIBLE PAYMENT OPTIONS</span></div>
                        <div class="cell"><span data-tab-link="feature.resources" class="tab desktop">ON-DEMAND RESOURCES</span></div>
                        <div class="cell"><span data-tab-link="feature.migration" class="tab desktop">EASY MIGRATION</span></div>
                    </div>
                </div>
                <div class="tabs" data-tabs="feature">

                    <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">14-DAY MONEY-BACK GUARANTEE</span>
                    <div class="tab-content active" data-tab-id="money-back">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.jpg">
                        <span class="title">14-DAY MONEY-BACK GUARANTEE</span>
                        <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
                    </div>

                    <span data-tab-link="feature.support" data-tab-toggle="self" class="tab mob">24/7 CUSTOMER SUPPORT</span>
                    <div class="tab-content" data-tab-id="support">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/support.jpg">
                        <span class="title">24/7 CUSTOMER SUPPORT</span>
                        <p>Our dedicated customer support team is always ready to give you a hand no matter how big or small your problem is.</p>
                    </div>

                    <span data-tab-link="feature.uptime" data-tab-toggle="self" class="tab mob">99.9% UPTIME</span>
                    <div class="tab-content" data-tab-id="uptime">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/uptime.jpg">
                        <span class="title">99.9% UPTIME</span>
                        <p>We ensure immaculate availability of your cPanel hosting service – while following our strict policies, we generously compensate our customers if downtime occurs.</p>
                    </div>

                    <span data-tab-link="feature.flexible" data-tab-toggle="self" class="tab mob">FLEXIBLE PAYMENT OPTIONS</span>
                    <div class="tab-content" data-tab-id="flexible">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/payment.jpg">
                        <span class="title">FLEXIBLE PAYMENT OPTIONS</span>
                        <p>Choose from a variety of different payment options, including Credit Card, Paypal, Skrill, Paysera, CashU, Ebanx, Braintree, Alipay transactions and Bitcoin payments.</p>
                    </div>

                    <span data-tab-link="feature.resources" data-tab-toggle="self" class="tab mob">ON-DEMAND RESOURCES</span>
                    <div class="tab-content" data-tab-id="resources">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/resources.jpg">
                        <span class="title">ON-DEMAND RESOURCES</span>
                        <p>Grab more resources to support your growing needs! We offer flexible web hosting plans from beginners to advanced website builders.</p>
                    </div>

                    <span data-tab-link="feature.migration" data-tab-toggle="self" class="tab mob">EASY MIGRATION</span>
                    <div class="tab-content" data-tab-id="migration">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/migration.jpg">
                        <span class="title">EASY MIGRATION</span>
                        <p>Migrate your website to Host1Plus with no chance of data loss. We provide quick and simple website migration whenever you’re ready.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="faq2">
        <h2 class="title">Ask questions, get answers!</h2>
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq">FAQ</h3>
            <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    What is cPanel?
                    <div class="answer">
                        cPanel is one of the most popular standard control panels on the market. This control panel is well-known and widely used for Linux-based hosting services, as it ensures easy web administration, contains a simplified layout and multiple management options.
                    </div>
                </div>
                <div class="question">
                    How does cPanel hosting differ from regular web hosting?
                    <div class="answer">
                        cPanel hosting is similar to regular web hosting, but it also includes a handy control panel for easier management. Along with cPanel, here at Host1Plus you can also benefit from a selection of worldwide web hosting locations, including Frankfurt, Germany; Amsterdam, Netherlands; Šiauliai, Lithuania; Johannesburg, South Africa; Los Angeles, US; and Sao Paulo, Brazil.
                    </div>
                </div>
                <div class="question">
                    Do I have to pay for cPanel separately?
                    <div class="answer">
                        No, it will be already included in your web hosting package.
                    </div>
                </div>
                <div class="question">
                    Can you help me migrate my website from another hosting provider, if I have been using their cPanel web hosting services?
                    <div class="answer">
                        Yes, we provide simple and quick website migration from other hosting providers if you have already been using web hosting services with cPanel. Please contact our technical support at <a href="mailto:support@host1plus.com">support@host1plus.com</a> for more information.
                    </div>
                </div>
                <div class="question">
                    How do I start using cPanel?
                    <div class="answer">
                        To get started with cPanel hosting, choose your plan, select your data center location, your billing period and complete your order. Continue by entering your personal details and choose a payment option. Finally, validate your details and wait for a confirmation email with your login details.
                    </div>
                </div>
                <div class="question">
                    What operating systems are compatible with cPanel?
                    <div class="answer">
                        This control panel is based on Linux OS, but it has a simple graphic interface.
                    </div>
                </div>
                <div class="question">
                    How do I log in to my cPanel hosting account?
                    <div class="answer">
                        Once you complete your web hosting order, you will receive your full login details by email. You can also log in to the control panel through your Host1Plus Client Area.
                    </div>
                </div>
            </div>
            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services-tutorials/web-hosting-services/all-web-hosting-tutorials/setting-dns-in-cpanel"><i class="fa fa-file-text-o file-ico"></i> Setting DNS in cPanel </a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/control-panels/cpanel/how-to-add-domain-in-cpanel"><i class="fa fa-file-text-o file-ico"></i> How to Add Addon Domain in cPanel</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/control-panels/cpanel/how-to-create-email-in-cpanel"><i class="fa fa-file-text-o file-ico"></i> How to Create Email in cPanel </a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/control-panels/cpanel/all-cpanel-tutorials/how-to-create-ftp-account-in-cpanel"><i class="fa fa-file-text-o file-ico"></i> How to Create FTP Account in cPanel</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/control-panels/cpanel/all-cpanel-tutorials/how-to-set-password-for-directory-in-cpanel"><i class="fa fa-file-text-o file-ico"></i> How to Set Password for Directory in cPanel</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services-tutorials/web-hosting-services/all-web-hosting-tutorials/how-to-add-database"><i class="fa fa-file-text-o file-ico"></i> How to Add a Database</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/web-hosting-services/all-web-hosting-tutorials/how-to-create-a-subdomain-cpanel"><i class="fa fa-file-text-o file-ico"></i> How to Create a Subdomain in cPanel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="back-to-top">
        <div class="back-box">
            <table>
            <tbody>
            <tr>
                <td><div class="main-text">Choose the right cPanel Web hosting plan for you!</div></td>
                <td><a href="#" class="button" onclick="$('html, body').animate({scrollTop: $('.landing-header').offset().top}, 2000);">Back to top <i class="fa fa-angle-up"></i></a></td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>

    <div class="prefooter">
        <h2 class="title">Looking for other cPanel hosting solutions?</h2>
        <div class="subtitle">cPanel VPS Hosting</div>
        <p>
            Looking for more power? Try cPanel VPS hosting and create your own easily-managed virtual machine.
        </p>
        <a href="<?php echo $config['links']['website'] . '/cpanel-vps/'; ?>" class="button">LEARN MORE</a>
    </div>


</div>

<?php get_footer(); ?>
