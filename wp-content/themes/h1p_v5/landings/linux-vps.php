<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: Linux VPS old
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);


wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

$vps_hosting_min_price = $whmcs->getMinPrice($config['products']['vps_hosting']['locations']['chicago']['id']);

get_header(); ?>

<div id="linux-vps">

    <div class="landing-header tablet-pad">
        <h1 class="page-title">Linux VPS hosting</h1>
        <h2 class="page-subtitle">Affordable Linux VPS hosting services</h2>

        <div class="choose-location">

            <div class="title">Choose your server location</div>
            <div><span class="arrow-down"></span></div>
            <div class="locations-list">

                <?php
                    $plan = 'amber';
                    $planConfig = $whmcs->get_local_stepsconfig('vps_plans')[ $plan ];
                    $locations = $config['products']['vps_hosting']['locations'];

                    $first_loc_override_key = 'chicago';
                    if( array_key_exists( $first_loc_override_key, $locations) ){
                        $first_loc_override = $locations[$first_loc_override_key]; //which location should be first
                        unset( $locations[$first_loc_override_key] );
                        $locations = [$first_loc_override_key => $first_loc_override] + $locations;
                    }

                    foreach($locations as $location_key => $location):

                        $prices = $whmcs->getConfigurablePrice($location['id'], $planConfig['configs']);
                 ?>

                    <div class="location" onclick="$(this).find('form').submit();">
                        <form class="" action="<?php echo $config['whmcs_links']['checkout_vps']?>" method="get">

                            <input type="hidden" name="plan" value="<?php echo $plan;?>"/>
                            <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                            <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                            <input type="hidden" name="promocode" value="<?php echo $whmcs_promo;?>"/>
                            <input type="hidden" name="location" value="<?php echo $location['key'] ?>"/>

                            <div class="label">
                                <?php
                                switch ($location_key){
                                    case 'los_angeles':
                                        echo __('Los Angeles');
                                        break;
                                    case 'chicago':
                                        echo __('Chicago');
                                        break;
                                    case 'sao_paulo':
                                        echo __('São Paulo');
                                        break;
                                    case 'frankfurt':
                                        echo __('Frankfurt');
                                        break;
                                    case 'johannesburg':
                                        echo __('Johannesburg');
                                        break;
                                }
                                ?>
                            </div>
                            <div><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/<?php echo strtoupper( $location['country_code'] ) ?>.png" alt="" width="80px"/></div>
                            <div class="price-notice">from <span class="price"><?php echo $prices[0]['price']; ?></span> / month</div>
                        </form>
                    </div>

                <?php endforeach; ?>
            </div>
            <div class="os-container">
                <div class="title">Available Linux VPS Hosting Operating Systems</div>
                <div class="os-list">
                    <div class="os">
                        <div class="ico ubuntu"></div>
                        <div class="os-title">Ubuntu</div>
                    </div>
                    <div class="os">
                        <div class="ico fedora"></div>
                        <div class="os-title">Fedora</div>
                    </div>
                    <div class="os">
                        <div class="ico debian"></div>
                        <div class="os-title">Debian</div>
                    </div>
                    <div class="os">
                        <div class="ico centos"></div>
                        <div class="os-title">CentOS</div>
                    </div>
                    <div class="os">
                        <div class="ico suse"></div>
                        <div class="os-title">Suse</div>
                    </div>
                </div>
                <div class="bottom-notice">
                    Power up your cheap VPS server with a range of Linux distributions – all available at the checkout.
                </div>
            </div>
        </div>

    </div>

    <div class="features-boxes container">
        <h2 class="title">
            Why choose Host1Plus Linux VPS hosting?
        </h2>
        <div class="feature-box">
            <div class="fico">
                <i class="ico console"></i>
            </div>
            <div class="ftitle">Full Root Access</div>
            <div class="fcontent">
                Enjoy full authority over your server and edit or install various programs according to your needs.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico cp"></i>
            </div>
            <div class="ftitle">cPanel Control Panel</div>
            <div class="fcontent">
                Enjoy easier hands-on server management with the most popular control panel.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico linux"></i>
            </div>
            <div class="ftitle">Diversity of Linux OS Distros</div>
            <div class="fcontent">
                Run multiple OS instances (Ubuntu, Fedora, Debian, CentOS, Suse) with unbeatable performance.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico scalable"></i>
            </div>
            <div class="ftitle">Fully Scalable Resource Allocation</div>
            <div class="fcontent">
                Manage your resources and pay only for what you use! You can grab additional resources at our Client Area.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico dns"></i>
            </div>
            <div class="ftitle">DNS Management</div>
            <div class="fcontent">
                Manage your server without struggle! Now you can easily create your domain zones and edit records with no extra support.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico protection"></i>
            </div>
            <div class="ftitle">DDoS Protection</div>
            <div class="fcontent">
                A hassle free solution for secure online environment. Our DDoS protection covers up to 40 Gbit/s of a single attack and is completely free.
            </div>
        </div>
    </div>

        <?php include __DIR__ . '/../htmlblocks/reviews_2.php'; ?>

    <div class="what-else">
        <h2 class="title">Why choose Host1Plus?</h2>

        <div class="block-tabs">
            <div class="tabs-wrapper">
                <div class="app-tabs">
                    <div class="table">
                    <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active">MONEY-BACK GUARANTEE</span></div>
                        <div class="cell"><span data-tab-link="feature.responsive" class="tab desktop">RESPONSIVE CLIENT AREA</span></div>
                            <div class="cell"><span data-tab-link="feature.hidden-fees" class="tab desktop">NO HIDDEN FEES</span></div>
                        <div class="cell"><span data-tab-link="feature.multilingual" class="tab desktop">MULTILINGUAL TECH SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.savings" class="tab desktop">MAJOR SAVINGS</span></div>
                    </div>
                </div>
                <div class="tabs" data-tabs="feature">

                    <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">MONEY-BACK GUARANTEE</span>
                    <div class="tab-content active" data-tab-id="money-back">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.jpg">
                        <span class="title">MONEY-BACK GUARANTEE</span>
                        <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
                    </div>
                    <span data-tab-link="feature.responsive" data-tab-toggle="self" class="tab mob">RESPONSIVE CLIENT AREA</span>
                    <div class="tab-content" data-tab-id="responsive">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/responsive.jpg">
                        <span class="title">RESPONSIVE CLIENT AREA</span>
                        <p>Always on the go? Easily manage your services using any mobile device.</p>
                    </div>

                        <span data-tab-link="feature.hidden-fees" data-tab-toggle="self" class="tab mob">NO HIDDEN FEES</span>
                        <div class="tab-content" data-tab-id="hidden-fees">
                            <img src="<?php bloginfo('template_directory'); ?>/landings/images/hidden-fees.jpg">
                            <span class="title">NO HIDDEN FEES</span>
                            <p>Fair prices guaranteed! Enjoy your hosting services with no extra costs.</p>
                        </div>

                    <span data-tab-link="feature.multilingual" data-tab-toggle="self" class="tab mob">MULTILINGUAL TECH SUPPORT</span>
                    <div class="tab-content" data-tab-id="multilingual">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/multilingual.jpg">
                        <span class="title">MULTILINGUAL TECH SUPPORT</span>
                        <p>No more language barriers! We speak English, Lithuanian, Spanish and Portuguese.</p>
                    </div>

                    <span data-tab-link="feature.savings" data-tab-toggle="self" class="tab mob">MAJOR SAVINGS</span>
                    <div class="tab-content" data-tab-id="savings">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/savings.jpg">
                        <span class="title">MAJOR SAVINGS</span>
                        <p>Sign up for an extended billing cycle and save up to 11% for your purchase!</p>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php include __DIR__ . '/block-vps-locations.php'; ?>

    <div class="faq2">
        <h2 class="title">Ask questions, get answers!</h2>
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq">FAQ</h3>
            <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    What is Linux VPS hosting?
                    <div class="answer">
                        Linux VPS hosting is a usual VPS hosting just with a Linux operating system installed. Linux VPS has its own operating system, applications and data that are separated from other people on the same server and performs just like a dedicated one.
                    </div>
                </div>
                <div class="question">
                    Do you provide VPS hosting based not on the Linux OS?
                    <div class="answer">
                        Our VPS hosting servers are only based on Linux operating system. However, during the ordering process you can choose from various offered instances of Linux OS templates - Ubuntu, Fedora, Debian, CentOS and Suse. However, if you are looking for Windows OS, you can try our <a href="<?php echo $config['links']['dedicated-servers']?>">dedicated hosting</a>.
                    </div>
                </div>
                <div class="question">
                    What are the possible locations for Linux VPS hosting?
                    <div class="answer">
                        Currently we provide cheap Linux VPS hosting in 5 different locations – USA (Los Angeles, Chicago), Germany, Brazil and South Africa.
                    </div>
                </div>
                <div class="question">
                    Do I have to install Linux OS by myself?
                    <div class="answer">
                        Our Linux VPS hosting comes with Linux OS already installed and is free of charge.
                    </div>
                </div>
                <div class="question">
                    What happens when VPS bandwidth limit is reached?
                    <div class="answer">
                        If your server uses a lot of network resources, there is a chance that you will reach your monthly VPS bandwidth limit. After the limit is exceeded, the network speed of your server will become limited to 64 Kbits/s. You will also receive a notice about this limitation via email.
                        There are a few options if you want to solve this problem:<br>
                        <br>
                        1)	Wait for the end of the month (or the end of your billing cycle) when the bandwidth count starts over.<br>
                        2)	Upgrade your bandwidth resources at your Client Area as your VPS is fully customizable. However, in this case, the price of your VPS would increase.
                    </div>
                </div>
                <div class="question">
                    What is the network speed of your Linux VPS hosting?
                    <div class="answer">
                        We limit the network speed provided depending on the location of your VPS hosting service.<br>
                        <br>
                        <strong>Outgoing</strong> (data that is being sent from your server to another one) network speed is limited up to 500 Mbps in all VPS hosting locations except São Paulo, Brazil, where network speed limit is higher - up to 200 Mbps.<br>
                        <br>
                        <strong>Incoming</strong> (data that is being transferred to your server from another one) network speed does not have any limitations.
                    </div>
                </div>
            </div>
            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/how-to-set-up-openvpn-on-linux-vps"><i class="fa fa-file-text-o file-ico"></i> How To Set Up OpenVPN on Linux VPS </a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/how-to-install-csf-on-linux-vps"><i class="fa fa-file-text-o file-ico"></i> How to Install CSF on Linux VPS</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/where-can-i-find-linux-error-logs"><i class="fa fa-file-text-o file-ico"></i> Logs On Linux VPS </a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/how-to-install-cpanel-and-whm-in-your-vps"><i class="fa fa-file-text-o file-ico"></i> How to install cPanel & WHM in your VPS </a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services-tutorials/vps-hosting-services/all-vps-hosting-tutorials/common-log-files-on-a-linux-system/"><i class="fa fa-file-text-o file-ico"></i> Common log files on a Linux system </a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services-tutorials/vps-hosting-services/all-vps-hosting-tutorials/how-to-use-for-loop-in-linux-shell-console/"><i class="fa fa-file-text-o file-ico"></i> How to use (for) loop in Linux shell console  </a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services-tutorials/vps-hosting-services/all-vps-hosting-tutorials/change-time-vps/"><i class="fa fa-file-text-o file-ico"></i> How to change time in your VPS </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="back-to-top">
        <div class="back-box">
            <table>
            <tbody>
            <tr>
                <td><div class="main-text">Linux VPS hosting - plans starting at <?php echo $vps_hosting_min_price ?> per month</div></td>
                <td><a href="<?php echo $config['whmcs_links']['checkout_vps']?>?plan=amber&location=us_east" class="button">Explore Now</a></td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>
<?php /*
    <div class="prefooter">
        <div class="container">
            <h2 class="title">Looking for a specific Linux operating system?</h2>
            <p class="simple-text">
                Check out our <a href="<?php echo $config['links']['website'] . '/ubuntu-vps-hosting/'; ?>">Ubuntu</a>, <a href="<?php echo $config['links']['website'] . '/centos-vps-hosting/'; ?>">CentOS</a> and <a href="<?php echo $config['links']['website'] . '/debian-vps-hosting/'; ?>">Debian</a> VPS hosting
            </p>
        </div>
    </div>
*/ ?>
</div>

<?php get_footer(); ?>
