<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: Linux Hosting
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);


wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

get_header();

    global $whmcs;
    global $config;

    $web_hosting_min_price = $whmcs->getMinPrice($config['products']['web_hosting']['plans'][1]['id']);
    $vps_hosting_min_price = $whmcs->getMinPrice($config['products']['vps_hosting']['locations']['chicago']['id']);
    $dedicated_servers_min_price = $whmcs->getPriceOptions($config['products']['dedicated_servers']['plans'][1]['id'], 'quarterly')['price_monthly'];
    $reseller_hosting_min_price = $whmcs->getMinPrice($config['products']['reseller_hosting']['plans'][1]['id']);
?>

<div id="linux-hosting">

    <div class="landing-header tablet-pad">
        <h1 class="page-title">Linux Hosting</h1>
        <h2 class="page-subtitle">Best Linux hosting solutions crafted for your demanding needs</h2>

        <div class="services-container">

            <?php
                include_block( __DIR__ . "/../htmlblocks/services_locations_colls.php",
                    [
                        'web_hosting' => ['los_angeles','chicago','london','amsterdam','siauliai','johannesburg','frankfurt','san_paulo'],
                        'vps_hosting' => ['los_angeles','chicago','johannesburg','frankfurt','san_paulo'],
                        'reseller_hosting' => ['chicago','frankfurt','amsterdam','san_paulo','johannesburg','siauliai'],
                    ]
                );
            ?>

        </div>

    </div>

    <div class="features-boxes container">
        <h2 class="title">
            Our Linux hosting overview
        </h2>
        <div class="feature-box">
            <div class="fico">
                <i class="ico linux"></i>
            </div>
            <div class="ftitle">Simple Linux Optimization</div>
            <div class="fcontent">
                Various software packages can be simply downloaded from net and adapted for your service depending on optimization level you require.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico console"></i>
            </div>
            <div class="ftitle">Open Source Software</div>
            <div class="fcontent">
                Enjoy community based software with publicly available source code, constant updates and free products.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico scalable"></i>
            </div>
            <div class="ftitle">Highly Affordable Prices</div>
            <div class="fcontent">
                Run low or high traffic websites all with easy administration and multiple configuration options for a reasonable cost.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico security"></i>
            </div>
            <div class="ftitle">Increased Security</div>
            <div class="fcontent">
                Preserve your data on Linux OS! Authentication, authorization and encryption security levels ensure that no one can access your data without permission.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico speed"></i>
            </div>
            <div class="ftitle">High Network Speed</div>
            <div class="fcontent">
                Benefit from high network speed, low latency connection to any location, ensured by our professional network administrators.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico support"></i>
            </div>
            <div class="ftitle">Community Support</div>
            <div class="fcontent">
                Double support efficiency! Get your questions answered from our in-house support professionals or join a huge community of Linux developers and system administrators.
            </div>
        </div>
    </div>

    <div class="services-compare container">
        <h2 class="title">
            Find your best match by comparing our different<br>Linux hosting services!
        </h2>
        <div class="table-wrap smaller">
            <table>
                <tbody>
                    <tr>
                        <th class="darkbg">
                            FEATURES
                        </th>
                        <th class="bg1">
                            <div class="fs30">WEB</div>
                            <div class="fs18">HOSTING</div>
                            <div class="price-block smaller">
                                <div class="wrapper">
                                    <span class="main-price">
                                        <div class="from">From</div><?php echo $web_hosting_min_price ?></span> / mo
                                </div>
                            </div>
                            <div class="button-wrap">
                                <a href="<?php echo $config['links']['web-hosting']?>" class="button">VIEW PLANS</a>
                            </div>
                        </th>
                        <th class="bg2">
                            <div class="fs30">VPS</div>
                            <div class="fs18">HOSTING</div>
                            <div class="price-block smaller">
                                <div class="wrapper">
                                    <span class="main-price">
                                        <div class="from">From</div><?php echo $vps_hosting_min_price ?></span> / mo
                                </div>
                            </div>
                            <div class="button-wrap">
                                <a href="<?php echo $config['whmcs_links']['checkout_vps']?>?plan=amber&location=us_east" class="button">VIEW PLANS</a>
                            </div>
                        </th>
                        <th class="bg3">
                            <div class="fs30">RESELLER</div>
                            <div class="fs18">HOSTING</div>
                            <div class="price-block smaller">
                                <div class="wrapper">
                                    <span class="main-price">
                                        <div class="from">From</div><?php echo $reseller_hosting_min_price ?></span> / mo
                                </div>
                            </div>
                            <div class="button-wrap">
                                <a href="<?php echo $config['links']['reseller-hosting']?>" class="button">VIEW PLANS</a>
                            </div>
                        </th>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Server resource scalability</div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-check"></i></div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-check"></i></div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-check"></i></div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Full root access</div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-times"></i></div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-check"></i></div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-times"></i></div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">DDoS protection</div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-times"></i></div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-check"></i></div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-times"></i></div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Available operating systems</div>
                        </td>
                        <td class="center">
                            <div class="box">CloudLinux</div>
                        </td>
                        <td class="center">
                            <div class="box">Ubuntu, Fedora, Debian, CentOS, Suse</div>
                        </td>
                        <td class="center">
                            <div class="box">CloudLinux</div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Available control panels </div>
                        </td>
                        <td class="center">
                            <div class="box">cPanel or DirectAdmin (free)</div>
                        </td>
                        <td class="center">
                            <div class="box">cPanel ($12 / mo)</div>
                        </td>
                        <td class="center">
                            <div class="box">cPanel, DirectAdmin, WHMCS</div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Locations</div>
                        </td>
                        <td class="">
                            <div class="box">
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/US.png" alt="US" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Chicago,<br>USA
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/US.png" alt="US" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Los Angeles, USA
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/BR.png" alt="BR" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        São Paulo, Brazil
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/DE.png" alt="DE" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Frankfurt, Germany
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/LT.png" alt="LT" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Šiauliai, Lithuania
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/ZA.png" alt="ZA" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Johannesburg, South Africa
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/NL.png" alt="NL" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Amsterdam, Netherlands
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/GB.png" alt="GB" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        London, United Kingdom
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="">
                            <div class="box">
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/US.png" alt="US" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Chicago,<br>USA
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/US.png" alt="US" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Los Angeles, USA
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/BR.png" alt="BR" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        São Paulo, Brazil
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/DE.png" alt="DE" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Frankfurt, Germany
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/ZA.png" alt="ZA" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Johannesburg, South Africa
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="">
                            <div class="box">
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/US.png" alt="US" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Chicago,<br>USA
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/DE.png" alt="DE" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Frankfurt, Germany
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/NL.png" alt="NL" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Amsterdam, Netherlands
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/BR.png" alt="BR" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        São Paulo, Brazil
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/ZA.png" alt="ZA" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Johannesburg, South Africa
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/LT.png" alt="LT" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Šiauliai, Lithuania
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linetopbnone">
                        <td class="feature-name darkbg">
                            <div class="box">Price per month</div>
                        </td>
                        <td class="center">
                            <div class="box">
                                <div class="price-block smaller no-pad lighter-color">
                                    <div class="wrapper">
                                        <span class="main-price">
                                            <div class="from">From</div><?php echo $web_hosting_min_price ?></span> / mo
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="center">
                            <div class="box">
                                <div class="price-block smaller no-pad lighter-color">
                                    <div class="wrapper">
                                        <span class="main-price">
                                            <div class="from">From</div><?php echo $vps_hosting_min_price ?></span> / mo
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="center">
                            <div class="box">
                                <div class="price-block smaller no-pad lighter-color">
                                    <div class="wrapper">
                                        <span class="main-price">
                                            <div class="from">From</div><?php echo $reseller_hosting_min_price ?></span> / mo
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

        <?php include __DIR__ . '/../htmlblocks/reviews_2.php'; ?>

        <?php include __DIR__ . '/../htmlblocks/location-services.php'; ?>

    <div class="what-else">
        <h2 class="title">Why choose Host1Plus?</h2>

        <div class="block-tabs">
            <div class="tabs-wrapper">
                <div class="app-tabs">
                    <div class="table">
                        <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active">14-DAY MONEY-BACK GUARANTEE</span></div>
                        <div class="cell"><span data-tab-link="feature.multilingual" class="tab desktop">MULTILINGUAL TECH SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.savings" class="tab desktop">MAJOR SAVINGS</span></div>
                        <div class="cell"><span data-tab-link="feature.support" class="tab desktop">EXTRA-CARE SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.responsive" class="tab desktop">RESPONSIVE CLIENT AREA</span></div>
                    </div>
                </div>
                <div class="tabs" data-tabs="feature">

                    <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">14-DAY MONEY-BACK GUARANTEE</span>
                    <div class="tab-content active" data-tab-id="money-back">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.jpg">
                        <span class="title">14-Day Money-Back Guarantee</span>
                        <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
                    </div>

                    <span data-tab-link="feature.multilingual" data-tab-toggle="self" class="tab mob">MULTILINGUAL TECH SUPPORT</span>
                    <div class="tab-content" data-tab-id="multilingual">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/multilingual.jpg">
                        <span class="title">Multilingual Tech Support</span>
                        <p>No more language barriers! We speak English, Spanish, Portuguese and Lithuanian.</p>
                    </div>

                    <span data-tab-link="feature.savings" data-tab-toggle="self" class="tab mob">MAJOR SAVINGS</span>
                    <div class="tab-content" data-tab-id="savings">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/savings.jpg">
                        <span class="title">Major Savings</span>
                        <p>Sign up for an extended billing cycle and save up to 11% for your purchase!</p>
                    </div>

                    <span data-tab-link="feature.support" data-tab-toggle="self" class="tab mob">EXTRA-CARE SUPPORT</span>
                    <div class="tab-content" data-tab-id="support">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/support.jpg">
                        <span class="title">Extra-Care Support</span>
                        <p>Order extra-care support and cover it all – server monitoring and management, network issue resolution, basic security check and much more.</p>
                    </div>

                    <span data-tab-link="feature.responsive" data-tab-toggle="self" class="tab mob">RESPONSIVE CLIENT AREA</span>
                    <div class="tab-content" data-tab-id="responsive">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/responsive.jpg">
                        <span class="title">Responsive Client Area</span>
                        <p>Always on the go? Easily manage your services using any mobile device.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php include __DIR__ . '/../htmlblocks/payment_methods.php'; ?>

    <div class="faq2">
        <h2 class="title">Ask questions, get answers!</h2>
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq">FAQ</h3>
            <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    How long will my VPS setup take?
                    <div class="answer">
                        After you order a VPS, the service is usually set up instantly. However, if you pay using a credit card, it might take up to a few hours.
                    </div>
                </div>
                <div class="question">
                    What is the network speed of your Linux VPS hosting?
                    <div class="answer">
                        We limit the network speed provided depending on the location of your VPS hosting service.<br>
                        <br>
                        <strong>Outgoing</strong> (data that is being sent from your server to another one) network speed is limited up to 500 Mbps in all VPS hosting locations except São Paulo, Brazil, where network speed limit is higher - up to 200 Mbps.<br>
                        <br>
                        <strong>Incoming</strong> (data that is being transferred to your server from another one) network speed does not have any limitations.
                    </div>
                </div>
                <div class="question">
                    Will I be able to upgrade my service after the purchase?
                    <div class="answer">
                        You can always upgrade your hosting plan at your Client Area. <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1232/0/how-to-upgrade-my-web-hosting-plan">Click here</a> to see a full guide how to upgrade/downgrade your services.
                    </div>
                </div>
                <div class="question">
                    Do you provide DDoS protection?
                    <div class="answer">
                        Host1Plus provides DDoS protection for VPS in all hosting locations, except Frankfurt, Germany.<br>
                        <br>
                        However, please note that DDoS protection ensures that your server is running and is available online during DDoS attack, but it doesn’t ensure SLA (service-level-agreement). During the attack, your server latency might increase, depending on your server location.
                    </div>
                </div>
                <div class="question">
                    What virtualization do you use for VPS hosting?
                    <div class="answer">
                        <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1243/108/what-virtualization-do-you-use-for-vps-hosting">Learn more</a>
                    </div>
                </div>
            </div>
            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/how-to-set-up-openvpn-on-linux-vps"><i class="fa fa-file-text-o file-ico"></i> How to set up OpenVPN on Linux VPS?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-csf-on-linux"><i class="fa fa-file-text-o file-ico"></i> How to install CSF on Linux?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/logs-on-linux-vps"><i class="fa fa-file-text-o file-ico"></i> Logs on Linux VPS</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/how-to-open-ports-on-linux-operating-systems"><i class="fa fa-file-text-o file-ico"></i> How to open ports on Linux operating system?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/how-to-setup-ip-rotations-for-emails-on-linux"><i class="fa fa-file-text-o file-ico"></i> How to setup IP rotations for emails on Linux</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/web-hosting-services/all-web-hosting-tutorials/installing-ssl-to-directadmin"><i class="fa fa-file-text-o file-ico"></i> Installing SSL to DirectAdmin</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/control-panels/cpanel/all-cpanel-tutorials/how-to-create-ftp-account-in-cpanel"><i class="fa fa-file-text-o file-ico"></i> How to create FTP account in cPanel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="back-to-top">
        <div class="back-box">
            <table>
            <tbody>
            <tr>
                <td><div class="main-text">Get started with our reliable and powerful Linux hosting solutions!</div></td>
                <td><a href="#" class="button" onclick="$('html, body').animate({scrollTop: $('.landing-header').offset().top}, 2000);">Explore your options</a></td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>

</div>

<?php get_footer(); ?>
