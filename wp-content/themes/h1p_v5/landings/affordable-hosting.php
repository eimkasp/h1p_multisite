<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: Affordable Hosting
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);


wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

$whmcs_promo = 'GETSTARTED';
global $whmcs_promo;

get_header(); ?>

<div id="affordable-hosting" class="landing-hosting">

    <div class="landing-header tablet-pad">
        <h1 class="page-title">Affordable Hosting</h1>
        <h2 class="page-subtitle">Affordable yet reliable web hosting solutions in 7 different countries</h2>

        <div class="plansnpricing">
            <br>
            <br>
        </div>
        <div class="webhosting-info container">
            <?php include( __DIR__ . "/../htmlblocks/pricing-tables/web-hosting.php"); ?>

            <div class="notice compare" onclick="$('html, body').animate({scrollTop: $('#vps-compare').offset().top}, 2000);">
                <span class="underline">Compare Host1Plus web hosting service with other hosting providers</span>&nbsp;&nbsp;&nbsp;<i class="fa fa-sort-desc"></i>
            </div>

            <div class="hosting-promo-block">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <div class="block-title">
                                    WEB HOSTING 20% OFF
                                </div>
                                <div class="description">
                                    Promo code is valid for new clients, for all plans except Starter. Can be used with 1-12 month cycles.
                                </div>
                            </td>
                            <td>
                                <div class="promo-title">
                                    Promo code
                                </div>
                                <div class="promo-code">
                                    GETSTARTED
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <?php include __DIR__ . '/../htmlblocks/payment_methods.php'; ?>

    <div class="features-boxes">
        <div class="container">
            <h2 class="title">
                Host1Plus affordable hosting features
            </h2>
            <div class="feature-box">
                <div class="fico">
                    <i class="ico statistics"></i>
                </div>
                <div class="ftitle">High Connection Speed</div>
                <div class="fcontent">
                    Skip all the annoying lags! Our 1 Gbit network uplink speeds up your server connection so that you would enjoy quick data downloads, uploads and website loading.
                </div>
            </div>
            <div class="feature-box">
                <div class="fico">
                    <i class="ico setup"></i>
                </div>
                <div class="ftitle">One-Click Apps Installation</div>
                <div class="fcontent">
                    Perfection has no limits! Boost your hosting service by installing the most popular apps, such as, WordPress, Drupal, Joomla, or choose from more than 100 other available apps.
                </div>
            </div>
            <div class="feature-box">
                <div class="fico">
                    <i class="ico domain"></i>
                </div>
                <div class="ftitle">Free Domain Name</div>
                <div class="fcontent">
                    Get a free domain name with Business or Business Pro plans when signing up with us for 6 months and above billing cycles!
                </div>
            </div>
            <div class="feature-box">
                <div class="fico">
                    <i class="ico security"></i>
                </div>
                <div class="ftitle">Ensured Data Security</div>
                <div class="fcontent">
                    Easily backup your data and protect it from any unforeseen interference.
                </div>
            </div>
            <div class="feature-box">
                <div class="fico">
                    <i class="ico management"></i>
                </div>
                <div class="ftitle">Hands-On Service Management</div>
                <div class="fcontent">
                    Enjoy quick and easy server management with cPanel or DirectAdmin control panels without any additional cost.
                </div>
            </div>
            <div class="feature-box">
                <div class="fico">
                    <i class="ico email-acc"></i>
                </div>
                <div class="ftitle">Unlimited Email Accounts</div>
                <div class="fcontent">
                    Organize your communication with multiple emails on the same domain name! Create different email accounts for your different needs and purposes.
                </div>
            </div>
        </div>
    </div>

        <?php include __DIR__ . '/../htmlblocks/reviews_2.php'; ?>

    <div class="what-else">
        <h2 class="title">Why choose Host1Plus?</h2>

        <div class="block-tabs">
            <div class="tabs-wrapper">
                <div class="app-tabs">
                    <div class="table">
                        <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active">14-DAY MONEY-BACK GUARANTEE</span></div>
                        <div class="cell"><span data-tab-link="feature.multilingual" class="tab desktop">MULTILINGUAL TECH SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.savings" class="tab desktop">MAJOR SAVINGS</span></div>
                        <div class="cell"><span data-tab-link="feature.database" class="tab desktop">EFFORTLESS DATABASE ADMINISTRATION</span></div>
                            <div class="cell"><span data-tab-link="feature.hidden-fees" class="tab desktop">NO HIDDEN FEES</span></div>
                    </div>
                </div>
                <div class="tabs" data-tabs="feature">

                    <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">14-DAY MONEY-BACK GUARANTEE</span>
                    <div class="tab-content active" data-tab-id="money-back">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.png">
                        <span class="title">14-DAY MONEY-BACK GUARANTEE</span>
                        <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
                    </div>

                    <span data-tab-link="feature.multilingual" data-tab-toggle="self" class="tab mob">MULTILINGUAL TECH SUPPORT</span>
                    <div class="tab-content" data-tab-id="multilingual">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/multilingual.png">
                        <span class="title">MULTILINGUAL TECH SUPPORT</span>
                        <p>No more language barriers! We speak English, Spanish, Portuguese and Lithuanian.</p>
                    </div>

                    <span data-tab-link="feature.savings" data-tab-toggle="self" class="tab mob">MAJOR SAVINGS</span>
                    <div class="tab-content" data-tab-id="savings">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/savings.png">
                        <span class="title">MAJOR SAVINGS</span>
                        <p>Sign up for an extended billing cycle and save up to 11% for your purchase!</p>
                    </div>

                    <span data-tab-link="feature.responsive" data-tab-toggle="self" class="tab mob">EFFORTLESS DATABASE ADMINISTRATION</span>
                    <div class="tab-content" data-tab-id="database">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/responsive.png">
                        <span class="title">EFFORTLESS DATABASE ADMINISTRATION</span>
                        <p>Store large amounts of data - create your databases and manage control user permissions using MySQL and PostgreSQL.</p>
                    </div>

                    <span data-tab-link="feature.hidden-fees" data-tab-toggle="self" class="tab mob">NO HIDDEN FEES</span>
                    <div class="tab-content" data-tab-id="hidden-fees">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/hidden-fees.png">
                        <span class="title">NO HIDDEN FEES</span>
                        <p>Fair prices guaranteed! Enjoy your cheap web hosting services with no extra costs.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="vps-compare" class="compare-block">
        <div class="container">
            <h2 class="title">Compare Host1Plus affordable web hosting offer against other hosting providers</h2>

            <div class="table-wrap">

                <table>
                    <tbody>
                        <tr>
                            <td class="left date">
                                Data taken at 14/09/2015
                            </td>
                            <td class="h1p logo">
                                <div class="pic host1plus"></div>
                                <div class="title">Host1Plus</div>
                            </td>
                            <td class="value logo">
                                <div class="pic hostgator"></div>
                                <div class="title">Hostgator</div>
                            </td>
                            <td class="value logo">
                                <div class="pic and1"></div>
                                <div class="title">1and1</div>
                            </td>
                            <td class="value logo">
                                <div class="pic bluehost"></div>
                                <div class="title">Bluehost</div>
                            </td>
                            <td class="value logo">
                                <div class="pic justhost"></div>
                                <div class="title">JustHost</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Plan
                            </td>
                            <td class="value h1p">
                                Personal
                            </td>
                            <td class="value">
                                Hatchling
                            </td>
                            <td class="value">
                                Starter
                            </td>
                            <td class="value">
                                Starter
                            </td>
                            <td class="value">
                                Starter
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Domains
                            </td>
                            <td class="value h1p">
                                3
                            </td>
                            <td class="value">
                                1
                            </td>
                            <td class="value">
                                1
                            </td>
                            <td class="value">
                                1
                            </td>
                            <td class="value">
                                1
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Disk space
                            </td>
                            <td class="value h1p">
                                5 GB
                            </td>
                            <td class="value">
                                Unlimited
                            </td>
                            <td class="value">
                                100 GB
                            </td>
                            <td class="value">
                                100 GB
                            </td>
                            <td class="value">
                                100 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Bandwidth
                            </td>
                            <td class="value h1p">
                                100 GB
                            </td>
                            <td class="value">
                                Unlimited
                            </td>
                            <td class="value">
                                Unlimited
                            </td>
                            <td class="value">
                                Unlimited
                            </td>
                            <td class="value">
                                Unlimited
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Available control panels
                            </td>
                            <td class="value h1p tsmaller">
                                cPanel or DirectAdmin
                            </td>
                            <td class="value tsmaller">
                                cPanel
                            </td>
                            <td class="value tsmaller">
                                cPanel
                            </td>
                            <td class="value tsmaller">
                                cPanel
                            </td>
                            <td class="value tsmaller">
                                cPanel
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Email accounts
                            </td>
                            <td class="value h1p">
                                Unlimited
                            </td>
                            <td class="value">
                                Unlimited
                            </td>
                            <td class="value">
                                10
                            </td>
                            <td class="value">
                                100
                            </td>
                            <td class="value">
                                100
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Price per month
                            </td>
                            <td class="value h1p">
                                $2.49
                            </td>
                            <td class="value">
                                $10.95
                            </td>
                            <td class="value">
                                $5.99
                            </td>
                            <td class="value">
                                $5.99
                            </td>
                            <td class="value">
                                $5.99
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Available locations
                            </td>
                            <td class="value h1p tsmaller">
                                Chicago, USA<br>
                                Los Angeles, USA<br>
                                Brazil, BR<br>
                                Frankfurt, DE<br>
                                Šiauliai, LT<br>
                                Johannesburg, ZA<br>
                                Amsterdam, NL<br>
                                London, UK
                            </td>
                            <td class="value tsmaller">
                                Provo, USA<br>
                                Houston, USA
                            </td>
                            <td class="value tsmaller">
                                Kansas, USA<br>
                                Germany<br>
                                United Kingdom

                            </td>
                            <td class="value tsmaller">
                                Dallas, USA<br>
                                Amsterdam, NL<br>
                                Hong Kong
                            </td>
                            <td class="value tsmaller">
                                Chicago, USA
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="compare-notice">
                    *Third-party logos and marks are registered trademarks of their respective owners. All rights reserved.
                </div>

            </div>
        </div>

    </div>


    <div class="faq2">
        <h2 class="title">Ask questions, get answers!</h2>
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq">FAQ</h3>
            <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    How long it will take to set up my web hosting account?
                    <div class="answer">
                        After you order and make a payment for you web hosting service, your account will be set up immediately. However, if you pay using a credit card, it might take up to a few hours.
                    </div>
                </div>
                <div class="question">
                    Can I upgrade my web hosting plan later?
                    <div class="answer">
                        You can upgrade your service to another web hosting plan with more resources at our Client Area.
                    </div>
                </div>
                <div class="question">
                    Do I need a domain name to host my website?
                    <div class="answer">
                        If you want to host a website, you must have a domain. You may purchase a domain from any domain registrar, use the one you already own or register a new one at Host1Plus.
                    </div>
                </div>
                <div class="question">
                    What versions of PHP do you use?
                    <div class="answer">
                        We currently offer 5 versions of PHP – 4.4, 5.2, 5.3, 5.4 and 5.5.
                    </div>
                </div>
                <div class="question">
                    How to buy a dedicated IP for my web hosting service?
                    <div class="answer">
                        You can add it through our Client Area ($4/mo). To do so, first, log in to your Client Area and click Services > Web Hosting. <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1231/103/how-to-buy-a-dedicated-ip-for-my-web-hosting-service">Read more</a>.<br>
                        However, you could get a free dedicated IP together with our Business Pro web hosting plan.
                    </div>
                </div>
            </div>
            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/web-hosting-services/differences-between-shared-hosting-and-dedicated-server"><i class="fa fa-file-text-o file-ico"></i> Differences between shared hosting and dedicated server</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/cms-tutorials/wordpress/wordpress-transfer/how-to-move-site-on-same-server-but-different-domain"><i class="fa fa-file-text-o file-ico"></i> How to move site on same server but different domain</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/cms-tutorials/wordpress/wordpress-installation/how-to-run-wordpress-installation-script"><i class="fa fa-file-text-o file-ico"></i> How to run WordPress installation script</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/cms-tutorials/joomla/joomla-administration/how-to-change-url-of-joomla-site"><i class="fa fa-file-text-o file-ico"></i> How to change URL of Joomla site</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/web-hosting-services/all-web-hosting-tutorials/how-to-create-a-subdomain-cpanel"><i class="fa fa-file-text-o file-ico"></i> How to create a subdomain in cPanel</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/web-hosting-services/all-web-hosting-tutorials/setting-dns-in-cpanel"><i class="fa fa-file-text-o file-ico"></i> Setting DNS in cPanel</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/web-hosting-services/all-web-hosting-tutorials/how-to-add-domain-in-directadmin"><i class="fa fa-file-text-o file-ico"></i> How to add domain in DirectAdmin</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="back-to-top">
        <div class="back-box">
            <table>
            <tbody>
            <tr>
                <td><div class="main-text">Sign up with Host1Plus and enjoy hassle-free and highly<br>low cost web hosting solutions!</div></td>
                <td><a href="#" class="button" onclick="$('html, body').animate({scrollTop: $('.landing-header').offset().top}, 2000);">View Plans</a></td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>

    <div class="prefooter">
        <h3 class="sub-title">Looking for more powerful hosting services?</h3>
        <h3 class="sub-sub-title">Check out our VPS hosting offers!</h3>
        <a href="<?php echo $config['links']['website'] . '/cheap-vps-hosting/'; ?>" class="button">VPS hosting</a>
    </div>


</div>

<?php get_footer(); ?>
