<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: Chicago VPS Hosting
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-v2.style', get_template_directory_uri() . '/landings/css/landing-v2.css', ['landings.style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);


wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

$vps_hosting_min_price = $whmcs->getMinPrice($config['products']['vps_hosting']['locations']['chicago']['id']);

get_header(); ?>

<div id="chicago-vps">

    <div class="landing-header tablet-pad">
        <h1 class="page-title">Chicago VPS hosting</h1>
        <h2 class="page-subtitle">Super-fast Chicago VPS hosting services from only <?php echo $vps_hosting_min_price ?> per month</h2>

        <div class="choose-location">

            <div class="row">
                <div class="cell">
                    <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/us-flag-77.png" alt="" width="76px"/>
                </div>
                <div class="cell">
                    <div class="loc-name">
                        Chicago, USA
                    </div>
                    <div class="price">
                        from <strong><?php echo $vps_hosting_min_price ?></strong> / month
                    </div>
                </div>
                <div class="cell" style="text-align:right;">
                    <a href="<?php echo $config['whmcs_links']['checkout_vps']?>?location=us_east&plan=amber" class="button big orange">View Plans</a>
                </div>
            </div>
        </div>
        <div class="notice compare" onclick="$('html, body').animate({scrollTop: $('#vps-compare').offset().top}, 2000);">
            Compare our offer with other popular USA VPS providers
        </div>

    </div>


    <div  class="features-boxes container">
        <h2 class="title">Chicago VPS overview</h2>

        <div class="feature-box">
            <div class="fico">
                <i class="ico hardware"></i>
            </div>
            <div class="ftitle">Next-Level Hardware</div>
            <div class="fcontent">
                Such features as ECC memory and hot-swap hard drives ensure non-interruptible server performance.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico high-speed"></i>
            </div>
            <div class="ftitle">10 Gbps Network</div>
            <div class="fcontent">
                The bigger, the better! Enjoy our thoroughly developed, resources-rich network and get rid of irritatingly low network speeds.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico ram"></i>
            </div>
            <div class="ftitle">DDR4 Random Access Memory</div>
            <div class="fcontent">
                Double your server memory! Higher density modules allow a greater RAM capacity providing more space for your data.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico response"></i>
            </div>
            <div class="ftitle">NVMe-Backed Storage</div>
            <div class="fcontent">
                Speed is of utmost importance. Non-Volatile Memory caching enables high performance by significantly improving your server speed.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico cpu"></i>
            </div>
            <div class="ftitle">Xeon E5 2620 V3 Processors</div>
            <div class="fcontent">
                6 server-grade cores, higher trafficking and increased security of the latest Xeon technology is all about creating faultless server performance.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico ddos"></i>
            </div>
            <div class="ftitle">Full DDoS Protection</div>
            <div class="fcontent">
                Each month, we protect over 400 clients from DDoS attacks. If you’re affected, we’ll make sure you are in this list as well!
            </div>
        </div>
    </div>

    <div class="view-plans">
        <div class="container">
            <div class="row">
                <div class="cell title">
                    <div class="ico">
                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/us-flag-77.png" alt="" width="61px"/>
                    </div>
                    Chicago, USA
                </div>
                <div class="cell description">
                    A 40,000 square feet data center is placed under six layers of security, including visitor screening, authentication scanners, developed UPS systems and high-tech cooling equipment. As it provides continuous power, high network connectivity and extremely low latency, it is appreciated as one of the most reliable collocation facilities in USA.
                </div>
                <div class="cell description">
                    <ul>
                        <li>Rotary UPS systems</li>
                        <li>Diesel-powered generators</li>
                        <li>Minimum redundancy</li>
                        <li>Low latency peering with top traffic destinations</li>
                    </ul>
                </div>
                <div class="cell button-wrap">
                    <a href="<?php echo $config['whmcs_links']['checkout_vps']?>?location=us_east&plan=amber" class="button orange-border">VIEW PLANS</a>
                </div>
            </div>
        </div>
    </div>

        <?php include __DIR__ . '/../htmlblocks/reviews_2.php'; ?>

    <div class="locations-wrapper tablet-pad">
        <div class="title">Looking for something else?</div>
        <div class="subtitle">Point your server to any other destination. The closer you get – the lower latency and higher connection speed you receive.</div>

        <div class="choose-location">
            <div class="locations-list">

                <?php
                    $plan = 'amber';
                    $planConfig = $whmcs->get_local_stepsconfig('vps_plans')[ $plan ];
                    $locations = $config['products']['vps_hosting']['locations'];


                    foreach($locations as $location_key => $location):
                        if( in_array( $location_key, [ 'chicago' ] ) ) continue;

                        $prices = $whmcs->getConfigurablePrice($location['id'], $planConfig['configs']);
                 ?>

                    <div class="location" onclick="$(this).find('form').submit();">
                        <form class="" action="<?php echo $config['whmcs_links']['checkout_vps']?>" method="get">

                            <input type="hidden" name="plan" value="<?php echo $plan;?>"/>
                            <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                            <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                            <input type="hidden" name="location" value="<?php echo $location['key'] ?>"/>
                            <input type="hidden" name="promocode" value="<?php echo $whmcs_promo;?>"/>

                            <div class="label">
                                <?php
                                switch ($location_key){
                                    case 'los_angeles':
                                        echo __('Los Angeles');
                                        break;
                                    case 'chicago':
                                        echo __('Chicago');
                                        break;
                                    case 'sao_paulo':
                                        echo __('São Paulo');
                                        break;
                                    case 'frankfurt':
                                        echo __('Frankfurt');
                                        break;
                                    case 'johannesburg':
                                        echo __('Johannesburg');
                                        break;
                                }
                                ?>
                            </div>
                            <div><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/<?php echo strtoupper( $location['country_code'] ) ?>.png" alt="" width="80px"/></div>
                            <div class="price-notice">from <span class="price"><?php echo $prices[0]['price']; ?></span> / month</div>
                        </form>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>

    </div>


    <div class="what-else">
        <h2 class="title">What else is there for you?</h2>

        <div class="block-tabs">
            <div class="tabs-wrapper">
                <div class="app-tabs">
                    <div class="table">
                        <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active">14-DAY MONEY-BACK GUARANTEE</span></div>
                        <div class="cell"><span data-tab-link="feature.savings" class="tab desktop ">MAJOR SAVINGS</span></div>
                        <div class="cell"><span data-tab-link="feature.support" class="tab desktop">EXTRA-CARE SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.responsive" class="tab desktop">RESPONSIVE CLIENT AREA</span></div>
                        <div class="cell"><span data-tab-link="feature.database" class="tab desktop">EFFORTLESS DATABASE ADMINISTRATION</span></div>
                    </div>
                </div>
                <div class="tabs" data-tabs="feature">

                    <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">14-DAY MONEY-BACK GUARANTEE</span>
                    <div class="tab-content active" data-tab-id="money-back">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.jpg">
                        <span class="title">14-Day Money-Back Guarantee</span>
                        <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
                    </div>

                    <span data-tab-link="feature.savings" data-tab-toggle="self" class="tab mob">MAJOR SAVINGS</span>
                    <div class="tab-content" data-tab-id="savings">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/savings.jpg">
                        <span class="title">Major Savings</span>
                        <p>Sign up for an extended billing cycle and save up to 11% for your purchase!</p>
                    </div>

                    <span data-tab-link="feature.support" data-tab-toggle="self" class="tab mob">EXTRA-CARE SUPPORT</span>
                    <div class="tab-content" data-tab-id="support">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/support.jpg">
                        <span class="title">Extra-Care Support</span>
                        <p>Order extra-care support and cover it all – server monitoring and management, network issue resolution, basic security check and much more.</p>
                    </div>

                    <span data-tab-link="feature.responsive" data-tab-toggle="self" class="tab mob">RESPONSIVE CLIENT AREA</span>
                    <div class="tab-content" data-tab-id="responsive">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/responsive.jpg">
                        <span class="title">Responsive Client Area</span>
                        <p>Always on the go? Easily manage your services using any mobile device.</p>
                    </div>

                    <span data-tab-link="feature.responsive" data-tab-toggle="self" class="tab mob">EFFORTLESS DATABASE ADMINISTRATION</span>
                    <div class="tab-content" data-tab-id="database">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/responsive.jpg">
                        <span class="title">Effortless Database Administration</span>
                        <p>Store large amounts of data - create your databases and manage control user permissions using MySQL and PostgreSQL.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div id="vps-compare" class="compare-block">
        <div class="container">
            <h2 class="title">Compare Host1Plus offer against other Chicago VPS providers</h2>

            <div class="table-wrap">

                <table>
                    <tbody>
                        <tr>
                            <td class="left date">
                                Data taken on 21/8/2015
                            </td>
                            <td class="h1p logo">
                                <div class="pic host1plus"></div>
                                <div class="title">Host1Plus</div>
                            </td>
                            <td class="value logo">
                                <div class="pic vpsnet"></div>
                                <div class="title">Vps.net</div>
                            </td>
                            <td class="value logo">
                                <div class="pic greengeeks"></div>
                                <div class="title">Greengeeks</div>
                            </td>
                            <td class="value logo">
                                <div class="pic vultr"></div>
                                <div class="title">Vultr</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                CPU
                            </td>
                            <td class="value h1p">
                                1 Core
                            </td>
                            <td class="value">
                                2 Cores
                            </td>
                            <td class="value">
                                4 Cores
                            </td>
                            <td class="value">
                                1 Core
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                RAM
                            </td>
                            <td class="value h1p">
                                768 MB
                            </td>
                            <td class="value">
                                1 GB
                            </td>
                            <td class="value">
                                1 GB
                            </td>
                            <td class="value">
                                1 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Disk Space
                            </td>
                            <td class="value h1p">
                                60 GB
                            </td>
                            <td class="value">
                                20 GB
                            </td>
                            <td class="value">
                                25 GB
                            </td>
                            <td class="value">
                                20 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Bandwidth
                            </td>
                            <td class="value h1p">
                                1000 GB
                            </td>
                            <td class="value">
                                6000 GB
                            </td>
                            <td class="value">
                                1000 GB
                            </td>
                            <td class="value">
                                2000 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Monthly price
                            </td>
                            <td class="value h1p">
                                $4.75
                            </td>
                            <td class="value">
                                $30.00
                            </td>
                            <td class="value">
                                $39.95
                            </td>
                            <td class="value">
                                $10.00
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                OS
                            </td>
                            <td class="value h1p tsmaller">
                                Ubuntu, Fedora, Debian, CentOS, Suse
                            </td>
                            <td class="value tsmaller">
                                CentOS, Ubuntu, Windows Std Edn
                            </td>
                            <td class="value tsmaller">
                                CentOS
                            </td>
                            <td class="value tsmaller">
                                CentOS, Ubuntu, Debian, FreeBSD, CoreOS, Windows 2012 R2
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                DDoS Protection
                            </td>
                            <td class="value h1p">
                                40 Gbps
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                            <td class="value">
                                10 Gbps
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                DNS Management
                            </td>
                            <td class="value h1p">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="compare-notice">
                    *Third-party logos and marks are registered trademarks of their respective owners. All rights reserved.
                </div>

            </div>
        </div>

    </div>

    <div class="faq2">
        <h2 class="title">Ask questions, get answers!</h2>
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq">FAQ</h3>
            <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    How long will my VPS setup take?
                    <div class="answer">
                        After you order a VPS, the service is usually set up instantly. However, if you pay using a credit card, it might take up to a few hours.
                    </div>
                </div>
                <div class="question">
                    What virtualization do you use for VPS hosting?
                    <div class="answer">
                        We are currently using OpenVZ virtualization.
                    </div>
                </div>
                <div class="question">
                    What payment methods do you provide?
                    <div class="answer">
                        Our accepted payment methods include credit cards, Paypal, WebMoney, Alipay, Skrill, Ebanx, PaySera, CashU and Bitcoin. However, depending on the country you come from, some methods might be restricted.<br>
                        If you have trouble accessing your chosen billing method from your account, please open a ticket at <a href="mailto:support@host1plus.com">support@host1plus.com</a> - we're always happy to help!
                    </div>
                </div>
                <div class="question">
                    Which control panels can I install on my VPS?
                    <div class="answer">
                        You can install open source control panels or any other web server management panel by yourself such as cPanel, ISPconfig, Plesk, Ajenti, Kloxo, OpenPanel, ZPanel, EHCP, ispCP, VHCS, RavenCore, Virtualmin, Webmin, DTC, DirectAdmin, InterWorx, SysCP, BlueOnyx and more.
                    </div>
                </div>
                <div class="question">
                    Is it possible to install a custom OS to OpenVZ VPS?
                    <div class="answer">
                        We are using only official OpenVZ templates so it is not possible to install any custom OS.
                    </div>
                </div>
                <div class="question">
                    What happens when my CPU limit is reached?
                    <div class="answer">
                        If you are overusing your CPU you will receive a notification that the power of your processor is temporarily limited.<br>
                        You can review your CPU power usage on the statistics page at your Client Area (for more information <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1244/108/where-can-i-find-my-vps-statistics-page">click here</a>).<br>
                        If you want to avoid reaching the limit, you can upgrade your CPU at your Client Area (for more information <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1241/108/how-to-upgradedowngrade-my-vps">click here</a>).
                    </div>
                </div>
            </div>
            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/how-to-create-virtualhost-on-apache"><i class="fa fa-file-text-o file-ico"></i> How to create virtualhost on Apache?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-csf-on-linux"><i class="fa fa-file-text-o file-ico"></i> How to install CSF on Linux?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/introduction-to-ssh-keys"><i class="fa fa-file-text-o file-ico"></i> Introduction to SSH keys</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-gnome-desktop"><i class="fa fa-file-text-o file-ico"></i> How to install GNOME desktop?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-change-a-time-zone-on-a-vps-server-2"><i class="fa fa-file-text-o file-ico"></i> How to change a time zone on a VPS server?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-add-ip-address-on-linux-operating-systems"><i class="fa fa-file-text-o file-ico"></i> How to add IP address on CentOS operating system?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="back-to-top">
        <div class="back-box">
            <table>
            <tbody>
            <tr>
                <td><div class="main-text">Choose the right Chicago VPS plan for you!</div></td>
                <td><a href="<?php echo $config['whmcs_links']['checkout_vps']?>?location=us_east&plan=amber" class="button">Explore Now</a></td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>

    <div class="prefooter">
        <h2 class="sub-title">Check out other Chicago hosting solutions</h2>
        <a href="<?php echo $config['links']['web-hosting']?>" class="button" style="">Web hosting in Chicago</a>
        <a href="<?php echo $config['links']['reseller-hosting']?>" class="button" style="margin-left:0px;">Reseller hosting in Chicago</a>
    </div>

</div>

<?php get_footer(); ?>