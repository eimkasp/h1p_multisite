<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: WordPress VPS Hosting
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);


wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

get_header();

    global $whmcs;
    global $config;

    $web_hosting_min_price = $whmcs->getPriceOptions($config['products']['web_hosting']['plans'][2]['id'] )[0]['price_monthly'];
    $vps_hosting_min_price = $whmcs->getMinPrice($config['products']['vps_hosting']['locations']['chicago']['id']);

?>

<div id="wp-vps">

    <div class="landing-header tablet-pad">
        <h1 class="page-title">WordPress VPS Hosting</h1>
        <h2 class="page-subtitle">Scalable WordPress VPS hosting solutions with an easy installation,<br>available in 5 worldwide locations</h2>

        <div class="choose-location">

            <div class="title">Select your server location</div>
            <div><span class="arrow-down"></span></div>
            <div class="locations-list">

                <?php
                    $plan = 'amber';
                    $planConfig = $whmcs->get_local_stepsconfig('vps_plans')[ $plan ];
                    $locations = $config['products']['vps_hosting']['locations'];

                    $first_loc_override_key = 'chicago';
                    if( array_key_exists( $first_loc_override_key, $locations) ){
                        $first_loc_override = $locations[$first_loc_override_key]; //which location should be first
                        unset( $locations[$first_loc_override_key] );
                        $locations = [$first_loc_override_key => $first_loc_override] + $locations;
                    }

                    foreach($locations as $location_key => $location):

                        $prices = $whmcs->getConfigurablePrice($location['id'], $planConfig['configs']);
                 ?>

                    <div class="location" onclick="$(this).find('form').submit();">
                        <form class="" action="<?php echo $config['whmcs_links']['checkout_vps']?>" method="get">

                            <input type="hidden" name="plan" value="<?php echo $plan;?>"/>
                            <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                            <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                            <input type="hidden" name="promocode" value="<?php echo $whmcs_promo;?>"/>
                            <input type="hidden" name="cstep" value="vps-package"/>
                            <input type="hidden" name="location" value="<?php echo $location['key'] ?>"/>

                            <div class="label">
                                <?php
                                switch ($location_key){
                                    case 'los_angeles':
                                        echo __('Los Angeles');
                                        break;
                                    case 'chicago':
                                        echo __('Chicago');
                                        break;
                                    case 'sao_paulo':
                                        echo __('São Paulo');
                                        break;
                                    case 'frankfurt':
                                        echo __('Frankfurt');
                                        break;
                                    case 'johannesburg':
                                        echo __('Johannesburg');
                                        break;
                                }
                                ?>
                            </div>
                            <div><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/<?php echo strtoupper( $location['country_code'] ) ?>.png" alt="" width="80px"/></div>
                            <div class="price-notice">from <span class="price"><?php echo $prices[0]['price']; ?></span> / month</div>
                        </form>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>

    </div>

    <div class="features-boxes container">
        <h2 class="title">
            WordPress VPS hosting overview
        </h2>
        <div class="feature-box">
            <div class="fico">
                <i class="ico cpanel"></i>
            </div>
            <div class="ftitle">Hands-On Server Management</div>
            <div class="fcontent">
                Manage your WordPress VPS without difficulty. Get cPanel license for $12/month and enjoy its easy to use interface.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico shield"></i>
            </div>
            <div class="ftitle">Full DDoS Protection</div>
            <div class="fcontent">
                Secure your server with Host1Plus. Our VPS hosting services are fully protected from DDoS attacks.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico puzzle"></i>
            </div>
            <div class="ftitle">Huge Selection of Themes and Plugins</div>
            <div class="fcontent">
                SEO optimization, security enhancements, social media integration, theme selection and many more! All of these things can be managed by using various WordPress plugins and themes.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico www"></i>
            </div>
            <div class="ftitle">Easy DNS Management</div>
            <div class="fcontent">
                No requests for tech support anymore! You get the control of your domain records – edit, create its zones or point them to any destination you wish.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico tools"></i>
            </div>
            <div class="ftitle">Handy Publishing Tools</div>
            <div class="fcontent">
                Easily organize your content flow! Create drafts, schedule your publications, secure them with passwords or make them available only for certain people.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico scalable"></i>
            </div>
            <div class="ftitle">Scalable Resources</div>
            <div class="fcontent">
                Grab more resources to support your growing needs! Customize the resources through your Client Area according to your preferences and stay on a budget!
            </div>
        </div>
    </div>

    <div class="services-compare container">
        <h2 class="title">
            Why choose WordPress VPS instead of a web hosting service?
        </h2>
        <div class="table-wrap">
            <table>
                <tbody>
                    <tr>
                        <th class="darkbg">
                            FEATURES
                        </th>
                        <th class="darkerbg">
                            WORDPRESS<br>WEB HOSTING
                            <div class="price-block">
                                <div class="wrapper">
                                    <span class="main-price">
                                        <div class="from">From</div><?php echo $web_hosting_min_price ?></span> / mo
                                </div>
                            </div>
                            <div class="button-wrap">
                                <a href="<?php echo $config['links']['web-hosting']?>" class="button semirounded wide">VIEW PLANS</a>
                            </div>
                        </th>
                        <th class="darkestbg">
                            WORDPRESS<br>VPS HOSTING
                            <div class="price-block">
                                <div class="wrapper">
                                    <span class="main-price">
                                        <div class="from">From</div><?php echo $vps_hosting_min_price ?></span> / mo
                                </div>
                            </div>
                            <div class="button-wrap">
                                <a href="<?php echo $config['whmcs_links']['checkout_vps']?>?cstep=vps-package&location=us_east&plan=amber" class="button semirounded wide">VIEW PLANS</a>
                            </div>
                        </th>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Scalability</div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-times"></i></div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-check"></i></div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Full root access</div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-times"></i></div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-check"></i></div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Disk space</div>
                        </td>
                        <td class="center">
                            <div class="box">From 1 GB</div>
                        </td>
                        <td class="center">
                            <div class="box">From 20 GB</div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Bandwidth</div>
                        </td>
                        <td class="center">
                            <div class="box">From 50 GB</div>
                        </td>
                        <td class="center">
                            <div class="box">From 500 GB</div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Entry processes</div>
                        </td>
                        <td class="center">
                            <div class="box">From 15</div>
                        </td>
                        <td class="center">
                            <div class="box">Unlimited</div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">DNS management</div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-check"></i></div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-check"></i></div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">DDoS protection</div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-times"></i></div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-check"></i></div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Backups</div>
                        </td>
                        <td class="center">
                            <div class="box">Manual backups</div>
                        </td>
                        <td class="center">
                            <div class="box">Automated backups<br>(for additional price)</div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Price</div>
                        </td>
                        <td class="center">
                            <div class="box">From <?php echo $web_hosting_min_price ?> / month</div>
                        </td>
                        <td class="center">
                            <div class="box">From <?php echo $vps_hosting_min_price ?> / month</div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Locations</div>
                        </td>
                        <td class="">
                            <div class="box">
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/BR.png" alt="BR" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        São Paulo, Brazil
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/DE.png" alt="DE" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Frankfurt, Germany
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/LT.png" alt="LT" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Šiauliai, Lithuania
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/ZA.png" alt="ZA" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Johannesburg, South Africa
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/NL.png" alt="NL" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Amsterdam, The Netherlands
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/GB.png" alt="GB" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        London, United Kingdom
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/US.png" alt="US" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Chicago, USA
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/US.png" alt="US" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Los Angeles, USA
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="">
                            <div class="box">
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/BR.png" alt="BR" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        São Paulo, Brazil
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/DE.png" alt="DE" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Frankfurt, Germany
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/ZA.png" alt="ZA" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Johannesburg, South Africa
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/US.png" alt="US" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Chicago, USA
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/US.png" alt="US" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Los Angeles, USA
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Control panel</div>
                        </td>
                        <td class="center">
                            <div class="box">DirectAdmin or cPanel (free)</div>
                        </td>
                        <td class="center">
                            <div class="box">cPanel license ($12 / mo)</div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linetopbnone">
                        <td class="feature-name darkbg">
                            <div class="box"></div>
                        </td>
                        <td class="center bg4">
                            <div class="box">
                                <div class="button-wrap">
                                    <a href="<?php echo $config['links']['web-hosting']?>" class="button semirounded wide">VIEW PLANS</a>
                                </div>
                            </div>
                        </td>
                        <td class="center darkestbg">
                            <div class="box">
                                <div class="button-wrap">
                                    <a href="<?php echo $config['whmcs_links']['checkout_vps']?>?cstep=vps-package&location=us_east&plan=amber" class="button semirounded wide">VIEW PLANS</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <?php include __DIR__ . '/../htmlblocks/reviews_2.php'; ?>

    <div class="what-else">
        <h2 class="title">What else do we offer?</h2>

        <div class="block-tabs">
            <div class="tabs-wrapper">
                <div class="app-tabs">
                    <div class="table">
                        <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active">14-DAY MONEY-BACK GUARANTEE</span></div>
                        <div class="cell"><span data-tab-link="feature.flexible" class="tab desktop">FLEXIBLE PAYMENT OPTIONS</span></div>
                        <div class="cell"><span data-tab-link="feature.multilingual" class="tab desktop">MULTILINGUAL TECH SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.savings" class="tab desktop">MAJOR SAVINGS</span></div>
                        <div class="cell"><span data-tab-link="feature.support" class="tab desktop">EXTRA-CARE SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.responsive" class="tab desktop">RESPONSIVE CLIENT AREA</span></div>
                    </div>
                </div>
                <div class="tabs" data-tabs="feature">

                    <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">14-DAY MONEY-BACK GUARANTEE</span>
                    <div class="tab-content active" data-tab-id="money-back">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.jpg">
                        <span class="title">14-Day Money-Back Guarantee</span>
                        <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
                    </div>

                    <span data-tab-link="feature.flexible" data-tab-toggle="self" class="tab mob">FLEXIBLE PAYMENT OPTIONS</span>
                    <div class="tab-content" data-tab-id="flexible">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/payment.jpg">
                        <span class="title">Flexible Payment Options</span>
                        <p>Select your preferred payment method from a variety of options, including Credit Card, Paypal, Skrill, Paysera, CashU, Ebanx, Braintree, Alipay transactions and Bitcoin payments.</p>
                    </div>

                    <span data-tab-link="feature.multilingual" data-tab-toggle="self" class="tab mob">MULTILINGUAL TECH SUPPORT</span>
                    <div class="tab-content" data-tab-id="multilingual">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/multilingual.jpg">
                        <span class="title">Multilingual Tech support</span>
                        <p>No more language barriers! We speak English, Lithuanian, Spanish and Portuguese.</p>
                    </div>

                    <span data-tab-link="feature.savings" data-tab-toggle="self" class="tab mob">MAJOR SAVINGS</span>
                    <div class="tab-content" data-tab-id="savings">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/savings.jpg">
                        <span class="title">Major Savings</span>
                        <p>Sign up for an extended billing cycle and save up to 11% for your purchase!</p>
                    </div>

                    <span data-tab-link="feature.support" data-tab-toggle="self" class="tab mob">EXTRA-CARE SUPPORT</span>
                    <div class="tab-content" data-tab-id="support">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/support.jpg">
                        <span class="title">Extra-Care Support</span>
                        <p>Worried about managing your service? You can order our extra-care support that will cover it all - server monitoring, management, network issue and performance resolution and much more.</p>
                    </div>

                    <span data-tab-link="feature.responsive" data-tab-toggle="self" class="tab mob">RESPONSIVE CLIENT AREA</span>
                    <div class="tab-content" data-tab-id="responsive">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/responsive.jpg">
                        <span class="title">Responsive Client Area</span>
                        <p>Always on the go? Easily manage your services using any mobile device.</p>
                    </div>

                </div>
            </div>
        </div>

    <div class="faq2">
        <h2 class="title">Ask questions, get answers!</h2>
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq">FAQ</h3>
            <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    How long will my VPS setup take?
                    <div class="answer">
                        After you order a VPS, the service is usually set up instantly. However, if you pay using a credit card, it might take up to a few hours.
                    </div>
                </div>
                <div class="question">
                    What virtualization do you use for VPS hosting?
                    <div class="answer">
                        We are currently using OpenVZ virtualization.
                    </div>
                </div>
                <div class="question">
                    What is the network speed of your Linux VPS hosting?
                    <div class="answer">
                        We limit the network speed provided depending on the location of your VPS hosting service.<br>
                        <br>
                        <strong>Outgoing</strong> (data that is being sent from your server to another one) network speed is limited up to 500 Mbps in all VPS hosting locations except São Paulo, Brazil, where network speed limit is higher - up to 200 Mbps.<br>
                        <br>
                        <strong>Incoming</strong> (data that is being transferred to your server from another one) network speed does not have any limitations.
                    </div>
                </div>
                <div class="question">
                    Is it possible to install a custom OS to OpenVZ VPS?
                    <div class="answer">
                        We are using only official OpenVZ templates only so it is not possible to install any custom OS.
                    </div>
                </div>
                <div class="question">
                    Which control panels can I install on my VPS?
                    <div class="answer">
                        You can manually install any free open source control panel that supports Ubuntu operating system, such as Sentora, Ajenti, Vesta and many more.
                    </div>
                </div>
            </div>
            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/cms-tutorials/wordpress/wordpress-installation/how-to-run-wordpress-installation-script"><i class="fa fa-file-text-o file-ico"></i> How to run WordPress installation script</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/cms-tutorials/wordpress/wordpress-administration/how-to-setup-multisite-using-wordpress"><i class="fa fa-file-text-o file-ico"></i> How to setup multisite using WordPress</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/cms-tutorials/wordpress/other-wordpress/how-to-use-cookies-in-wordpress"><i class="fa fa-file-text-o file-ico"></i> How to use cookies within WordPress</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/cms-tutorials/wordpress/other-wordpress/how-to-pass-value-in-wordpress-url"><i class="fa fa-file-text-o file-ico"></i> How to pass value in WordPress URL</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/cms-tutorials/wordpress/wordpress-transfer/how-to-move-wordpress-to-a-new-server"><i class="fa fa-file-text-o file-ico"></i> How to move WordPress to a new server</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/cms-tutorials/wordpress/wordpress-themes/how-to-install-wordpress-theme"><i class="fa fa-file-text-o file-ico"></i> How to install WordPress theme</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/cms-tutorials/wordpress/wordpress-administration/how-to-add-pages-to-wordpress"><i class="fa fa-file-text-o file-ico"></i> How to add pages to WordPress</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="back-to-top">
        <div class="back-box">
            <table>
            <tbody>
            <tr>
                <td><div class="main-text">Great performance and effortless management - get started<br>with our WordPress VPS hosting!</div></td>
                <td><a href="<?php echo $config['whmcs_links']['checkout_vps']?>?cstep=vps-package&location=us_east&plan=amber" class="button">Sign up</a></td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>

    <div class="prefooter">
        <h3 class="sub-title">Still not sure that VPS would be the best option for you? </h3>
        <h3 class="sub-sub-title">Check out our other hosting services</h3>
        <a href="<?php echo $config['links']['web-hosting']?>" class="button">WEB HOSTING</a> <a href="<?php echo $config['links']['dedicated-servers']?>" class="button" style="margin-right:0px;">DEDICATED SERVERS</a>
    </div>

</div>

<?php get_footer(); ?>