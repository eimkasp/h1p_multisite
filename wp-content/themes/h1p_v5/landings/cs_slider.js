$(document).ready(function(){

    $(window).load(function(){
        $('[data-cs]').each(function( index ) {
            var first_slide_height = parseInt( $(this).find('[data-cs-slide="1"]').height() );
            $(this).find('[data-cs-wrapper]').height( first_slide_height );
        });
    });

    /* content sliders init */

        $('[data-cs]').each(function( index ) {
            var slides_num = $(this).find('[data-cs-slide]').length;

            $(this).find('[data-cs-slide]').each(function( index ) {
                var wrapper_width = parseInt( $(this).closest('[data-cs-wrapper]').width() );
                var leftpx = wrapper_width * parseInt( index );
                $(this).css('left', leftpx+'px');
            });

            //paging
            if( $(this).find('[data-cs-paging]').length ){ //if paging wrapper exists
                $(this).find('[data-cs-paging]').append('<ul></ul>');
                $(this).find('[data-cs-paging] ul').append('<li><a class="active" data-cs-slindex="1"></a></li>');
                for (var i = 2; i <= slides_num; i++) {
                    $(this).find('[data-cs-paging] ul').append('<li><a data-cs-slindex="'+i+'"></a></li>');
                }
            }
        });
    /*-------------------------*/

    $( window ).resize(function() {

        $('[data-cs]').each(function( index ) {

            var old_wrapper_width = Math.abs( parseInt( $(this).find('[data-cs-slide].active').next().css('left') ) );
            if( old_wrapper_width <= 0 ) old_wrapper_width = Math.abs( parseInt( $(this).find('[data-cs-slide].active').prev().css('left') ) );

            $(this).find('[data-cs-slide]').each(function( index ) {
                var wrapper_width = parseInt( $(this).closest('[data-cs-wrapper]').width() );
                var current_left = parseInt( $(this).css('left') );
                var leftpx = ( current_left / old_wrapper_width) * wrapper_width;

                $(this).css('left', leftpx+'px');
            });

            var slide_height = parseInt( $(this).find('[data-cs-slide].active').height() );
            $(this).find('[data-cs-wrapper]').height( slide_height );

        });
    });

    $('[data-cs] [data-cs-slindex]').click(function(){
        csGoToSlide( $(this).closest('[data-cs]'), $(this).attr('data-cs-slindex') );
    });


    $('[data-cs] [data-cs-prev]').click(function(){
        var $slider = $(this).closest('[data-cs]');
        var current_slide_num = parseInt( $slider.find('[data-cs-slide].active').attr('data-cs-slide') );
        csGoToSlide( $slider, current_slide_num - 1 );
    });
    $('[data-cs] [data-cs-next]').click(function(){
        var $slider = $(this).closest('[data-cs]');
        var current_slide_num = parseInt( $slider.find('[data-cs-slide].active').attr('data-cs-slide') );
        csGoToSlide( $slider, current_slide_num + 1 );
    });

    function csGoToSlide( $slider, slide_index ){

        if( $slider.find('[data-cs-slide]').is(':animated') ) { //prevent clicks while sliding
            return;
        }
        
        var wrapper_width = $slider.find('[data-cs-wrapper]').width();
        var slides_num = $slider.find('[data-cs-slide]').length;
        var current_slide_index = parseInt( $slider.find('[data-cs-slide].active').attr('data-cs-slide') );

        var full_width_px = slides_num * wrapper_width;
        var min_px = 0;
        var max_px = full_width_px - wrapper_width;
        var current_pos = $slider.find('[data-cs-slide="1"]').position().left; //first slide pos
        var current_pos_abs = Math.abs( current_pos );

        var new_slide_height = $slider.find('[data-cs-slide="'+slide_index+'"]').height();

        if( current_slide_index < slide_index ){
            var direction = 'left';
            var steps = slide_index - current_slide_index;
        }
        else if( current_slide_index > slide_index ){
            var direction = 'right';
            var steps = current_slide_index - slide_index;
        }
        else{
            //return;
        }

        if(
            ( current_pos_abs <= 0 && direction == 'right' )
            ||
            ( max_px - current_pos_abs <= 0 && direction == 'left' )
        ){
            alert( 'no more slides thatway' );
        }
        else{
            $slider.find('[data-cs-slide]').animate({
                left: (direction=='right'?'+':'-') + '=' + ( steps * wrapper_width )
            }, 1000, function() {
                // Animation complete.
            });
            $slider.find('[data-cs-wrapper]').animate({
                height: new_slide_height
            }, 1000, function() {
                // Animation complete.
            });

            //change active slide
            $slider.find('[data-cs-slide]').removeClass('active');
            $slider.find('[data-cs-slide="'+slide_index+'"]').addClass('active');

            $slider.find('[data-cs-paging] [data-cs-slindex]').removeClass('active');
            $slider.find('[data-cs-paging] [data-cs-slindex="'+slide_index+'"]').addClass('active');

            $('[data-cs-next], [data-cs-prev]').removeClass('disabled');
            if( slide_index == slides_num ){//if last slide
                $slider.find('[data-cs-next]').addClass('disabled');
            }
            if( slide_index == 1 ){//if last slide
                $slider.find('[data-cs-prev]').addClass('disabled');
            }
        }

    }

});