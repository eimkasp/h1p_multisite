<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: DDos protected VPS
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-ddos-vps.style', get_template_directory_uri() . '/landings/css/landing-ddos-vps.css', ['landings.style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);


wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

$vps_hosting_min_price = $whmcs->getMinPrice($config['products']['vps_hosting']['locations']['chicago']['id']);

get_header(); ?>

<div id="ddos-vps">

    <div class="landing-header tablet-pad">
        <h1 class="page-title">DDOS Protected VPS Hosting</h1>
        <h2 class="page-subtitle">Fully DDoS protected VPS hosting solutions in 3 continents</h2>

        <div class="choose-location">

            <div class="title">Choose your server location</div>
            <div class="locations-list">

                <?php
                    $plan = 'amber';
                    $planConfig = $whmcs->get_local_stepsconfig('vps_plans')[ $plan ];
                    $locations = $config['products']['vps_hosting']['locations'];

                    $first_loc_override_key = 'chicago';
                    if( array_key_exists( $first_loc_override_key, $locations) ){
                        $first_loc_override = $locations[$first_loc_override_key]; //which location should be first
                        unset( $locations[$first_loc_override_key] );
                        $locations = [$first_loc_override_key => $first_loc_override] + $locations;
                    }

                    foreach($locations as $location_key => $location):

                        if( $location_key == "frankfurt" ) continue;

                        $prices = $whmcs->getConfigurablePrice($location['id'], $planConfig['configs']);

                 ?>

                    <div class="location" onclick="$(this).find('form').submit();">
                        <form class="" action="<?php echo $config['whmcs_links']['checkout_vps']?>" method="get">

                            <input type="hidden" name="plan" value="<?php echo $plan;?>"/>
                            <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                            <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                            <input type="hidden" name="promocode" value="<?php echo $whmcs_promo;?>"/>
                            <input type="hidden" name="location" value="<?php echo $location['key'] ?>"/>

                            <div class="label">
                                <?php
                                switch ($location_key){
                                    case 'los_angeles':
                                        echo __('Los Angeles');
                                        break;
                                    case 'chicago':
                                        echo __('Chicago');
                                        break;
                                    case 'sao_paulo':
                                        echo __('São Paulo');
                                        break;
                                    case 'johannesburg':
                                        echo __('Johannesburg');
                                        break;
                                }
                                ?>
                            </div>
                            <div><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/<?php echo strtoupper( $location['country_code'] ) ?>.png" alt="" width="80px"/></div>
                            <div class="price-notice">from <span class="price"><?php echo $prices[0]['price']; ?></span> / month</div>
                        </form>
                    </div>

                <?php endforeach; ?>
            </div>

        </div>

    </div>
    <div class="container">
        <div class="what-is-ddos">
            <div class="text-block" style="top:30px;">
                <div class="title">What is DDoS protected VPS?</div>
                <div class="text">DDoS protection provides you with more security than regular virtual private server hosting. Like any VPS hosting service, Host1Plus DDoS protected VPS equips you with more power & more resources. At the same time it provides full protection from any kind of DDoS attacks and preserves immaculate performance of your virtual system.</div>
            </div>
            <div class="text-block" style="top:430px;left:616px;">
                <div class="title">How DDoS protection works?</div>
                <div class="text">During an attack, its perpetrator attempts to disrupt a virtual machine or intercept network resources by sending immense amounts of DDoS flood. When a DDoS attack is detected, all the traffic is redirected to the scrubbing center of our partner <a href="https://www.staminus.net/" target="_blank">Staminus</a>, an advanced hybrid DDoS protection solutions provider, where it is carefully filtered and only genuine requests are sent back to the primary destination.</div>
            </div>
        </div>
    </div>

    <div class="why-choose-antidoss">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="title">Why choose anti-DDoS VPS at Host1Plus?</div>
                    <ul>
                        <li>Protection from all types of DDoS attacks</li>
                        <li>Unlimited duration of attacks per month</li>
                        <li>Up to 40 Gbit/s limit of an attack</li>
                        <li>Wide selection of operating systems - Ubuntu, CentOS, Debian, Fedora, Suse.</li>
                        <li>Integrated DNS manager</li>
                        <li>DDoS protection is Absolutely free</li>
                    </ul>
                </div>
                <div class="col">
                    <div class="title">DDoS protected VPS locations</div>
                    <p>
                        Our premium data centers provide reliable environment with multiple power-lines, powerful diesel generators and advanced cooling systems.
                    </p>
                    <div class="locations">
                        <div class="location">
                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/US.png" alt="" width="40px"/>
                            <strong>Chicago, US</strong>
                        </div>
                        <div class="location">
                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/US.png" alt="" width="40px"/>
                            <strong>Los Angeles, US</strong>
                        </div>
                        <div class="location">
                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/ZA.png" alt="" width="40px"/>
                            <strong>Johannesburg, ZA</strong>
                        </div>
                        <div class="location">
                            <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/BR.png" alt="" width="40px"/>
                            <strong>Sao Paulo, BR</strong>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="title">How strong our DDoS protection is?</div>
                    <ul>
                        <li>Average attack strength is up to 38 Gbit/s </li>
                        <li>99.3% attacks are mitigated successfully</li>
                        <li>Each month, over 400 of our clients are successfully protected from DDoS attacks</li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

        <?php include __DIR__ . '/../htmlblocks/reviews_2.php'; ?>


    <div class="what-else">
        <h2 class="title">What else do we offer?</h2>

        <div class="block-tabs">
            <div class="tabs-wrapper">
                <div class="app-tabs">
                    <div class="table">
                        <div class="cell"><span data-tab-link="feature.uptime" class="tab desktop active">99.9% UPTIME</span></div>
                        <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop">14-DAY MONEY-BACK GUARANTEE</span></div>
                        <div class="cell"><span data-tab-link="feature.reliable" class="tab desktop">RELIABLE HARDWARE</span></div>
                        <div class="cell"><span data-tab-link="feature.responsive" class="tab desktop">RESPONSIVE CLIENT AREA</span></div>
                        <div class="cell"><span data-tab-link="feature.multilingual" class="tab desktop">MULTILINGUAL TECH SUPPORT</span></div>
                    </div>
                </div>
                <div class="tabs" data-tabs="feature">

                    <span data-tab-link="feature.uptime" data-tab-toggle="self" class="tab mob">99.9% UPTIME</span>
                    <div class="tab-content active" data-tab-id="uptime">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/uptime.jpg">
                        <span class="title">99.9% UPTIME</span>
                        <p>Choose us and preserve your online performance – we guarantee 99.9% uptime. And if we’re wrong - we always compensate our customers if any unexpected downtime occurs.</p>
                    </div>

                    <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">14-Day Money-Back Guarantee</span>
                    <div class="tab-content" data-tab-id="money-back">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.jpg">
                        <span class="title">14-Day Money-Back Guarantee</span>
                        <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
                    </div>

                    <span data-tab-link="feature.reliable" data-tab-toggle="self" class="tab mob">RELIABLE HARDWARE</span>
                    <div class="tab-content" data-tab-id="reliable">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/raid.jpg">
                        <span class="title">RELIABLE HARDWARE</span>
                        <p>Benefit from enterprise hardware with dual PSU and RAID data storage to prevent data loss due to unexpected damage.</p>
                    </div>

                    <span data-tab-link="feature.responsive" data-tab-toggle="self" class="tab mob">RESPONSIVE CLIENT AREA</span>
                    <div class="tab-content" data-tab-id="responsive">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/responsive.jpg">
                        <span class="title">RESPONSIVE CLIENT AREA</span>
                        <p>Always on the go? Easily manage your services using any mobile device.</p>
                    </div>

                    <span data-tab-link="feature.multilingual" data-tab-toggle="self" class="tab mob">MULTILINGUAL TECH SUPPORT</span>
                    <div class="tab-content" data-tab-id="multilingual">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/multilingual.jpg">
                        <span class="title">MULTILINGUAL TECH SUPPORT</span>
                        <p>No more language barriers! We speak English, Lithuanian, Spanish and Portuguese.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="our-partners">
        <h2 class="title">Our partners</h2>
        <div class="row partners">
            <div class="col partner">
                <img src="<?php echo get_template_directory_uri() ?>/landings/images/partners/staminus.jpg" alt=""/>
            </div>
            <div class="col partner">
                <img src="<?php echo get_template_directory_uri() ?>/landings/images/partners/equinix.jpg" alt=""/>
            </div>
            <div class="col partner">
                <img src="<?php echo get_template_directory_uri() ?>/landings/images/partners/sourcebridge.jpg" alt=""/>
            </div>
            <div class="col partner">
                <img src="<?php echo get_template_directory_uri() ?>/landings/images/partners/centos.jpg" alt=""/>
            </div>
            <div class="col partner">
                <img src="<?php echo get_template_directory_uri() ?>/landings/images/partners/microsoft.jpg" alt=""/>
            </div>
            <div class="col partner">
                <img src="<?php echo get_template_directory_uri() ?>/landings/images/partners/suse.jpg" alt=""/>
            </div>
        </div>
    </div>

    <div class="faq2">
        <h2 class="title">Ask questions, get answers!</h2>
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq">FAQ</h3>
            <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    In which locations do you provide DDoS protection?
                    <div class="answer">
                        Host1Plus provides DDoS protection for VPS in all hosting locations, except Frankfurt, Germany.<br>
                        However, please note that DDoS protection ensures that your server is running and is available online during a DDoS attack, but it doesn’t ensure SLA (service-level-agreement). During the attack, your server latency might increase, depending on your location.
                    </div>
                </div>
                <div class="question">
                    What payment methods do you provide?
                    <div class="answer">
                        Our accepted payment methods include credit cards, Paypal, WebMoney, Alipay, Skrill, Ebanx, PaySera, CashU and Bitcoin. However, depending on the country you come from, some methods might be restricted.<br>
                        If you have trouble accessing your chosen billing method from your account, please open a ticket at <a href="mailto:support@host1plus.com">support@host1plus.com</a> - we're always happy to help!
                    </div>
                </div>
                <div class="question">
                    What virtualization do you use for VPS hosting?
                    <div class="answer">
                        We are currently using OpenVZ virtualization.
                    </div>
                </div>
                <div class="question">
                    Is it possible to install a custom OS to OpenVZ VPS?
                    <div class="answer">
                        We are using only official OpenVZ templates so it is not possible to install any custom OS.
                    </div>
                </div>
                <div class="question">
                    Which control panels can I install on my VPS?
                    <div class="answer">
                        You can install open source control panels or any other web server management panel by yourself such as cPanel, ISPconfig, Plesk, Ajenti, Kloxo, OpenPanel, ZPanel, EHCP, ispCP, VHCS, RavenCore, Virtualmin, Webmin, DTC, DirectAdmin, InterWorx, SysCP, BlueOnyx and more.
                    </div>
                </div>
                <div class="question">
                    How long will my VPS setup take?
                    <div class="answer">
                        After you order a VPS, the service is usually set up instantly. However, if you pay using a credit card, it might take up to a few hours.
                    </div>
                </div>
            </div>
            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/how-to-create-virtualhost-on-apache"><i class="fa fa-file-text-o file-ico"></i> How to create virtualhost on Apache?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-csf-on-linux"><i class="fa fa-file-text-o file-ico"></i> How to install CSF on Linux?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/introduction-to-ssh-keys"><i class="fa fa-file-text-o file-ico"></i> Introduction to SSH keys</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-gnome-desktop"><i class="fa fa-file-text-o file-ico"></i> How to install GNOME desktop?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-change-a-time-zone-on-a-vps-server-2"><i class="fa fa-file-text-o file-ico"></i> How to change a time zone on a VPS server?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-add-ip-address-on-linux-operating-systems"><i class="fa fa-file-text-o file-ico"></i> How to add IP address on CentOS operating system?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="back-to-top">
        <div class="back-box">
            <table>
            <tbody>
            <tr>
                <td><div class="main-text">DDoS Protected VPS hosting starting from <?php echo $vps_hosting_min_price ?>/mo</div></td>
                <td><a href="<?php echo $config['whmcs_links']['checkout_vps']?>?plan=amber" class="button">Explore Now</a></td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>

</div>

<?php get_footer(); ?>
