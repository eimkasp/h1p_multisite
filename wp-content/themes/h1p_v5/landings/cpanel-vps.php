<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: cPanel VPS (old)
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);

wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

get_header();

global $config;
global $whmcs_promo;
global $whmcs;

?>


<div class="landing-header tablet-pad">
    <h1 class="page-title">cPanel VPS hosting</h1>
    <h2 class="page-subtitle">Flawless performance and advanced tools for demanding clients</h2>

    <div class="choose-location">

        <div class="title">Choose your server location</div>
        <div><span class="arrow-down"></span></div>
        <div class="locations-list">

            <?php
                $plan = 'bronze';
                $planConfig = $whmcs->get_local_stepsconfig('vps_plans')[ $plan ];
                $locations = $config['products']['vps_hosting']['locations'];

                $first_loc_override_key = 'chicago';
                if( array_key_exists( $first_loc_override_key, $locations) ){
                    $first_loc_override = $locations[$first_loc_override_key]; //which location should be first
                    unset( $locations[$first_loc_override_key] );
                    $locations = [$first_loc_override_key => $first_loc_override] + $locations;
                }

                foreach($locations as $location_key => $location):

                    $prices = $whmcs->getConfigurablePrice($location['id'], $planConfig['configs']);
             ?>

                <div class="location" onclick="$(this).find('form').submit();">
                    <form class="" action="<?php echo $config['whmcs_links']['checkout_vps']?>" method="get">

                        <input type="hidden" name="plan" value="<?php echo $plan;?>"/>
                        <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                        <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                        <input type="hidden" name="promocode" value="<?php echo $whmcs_promo;?>"/>
                        <input type="hidden" name="location" value="<?php echo $location['key'] ?>"/>

                        <div class="label">
                            <?php
                            switch ($location_key){
                                case 'los_angeles':
                                    echo __('Los Angeles');
                                    break;
                                case 'chicago':
                                    echo __('Chicago');
                                    break;
                                case 'sao_paulo':
                                    echo __('São Paulo');
                                    break;
                                case 'frankfurt':
                                    echo __('Frankfurt');
                                    break;
                                case 'johannesburg':
                                    echo __('Johannesburg');
                                    break;
                            }
                            ?>
                        </div>
                        <div><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/<?php echo strtoupper( $location['country_code'] ) ?>.png" alt="" width="80px"/></div>
                        <div class="price-notice">from <span class="price"><?php echo $prices[0]['price']; ?></span> / month</div>
                    </form>
                </div>

            <?php endforeach; ?>
        </div>
    </div>

</div>
<div class="landing-notice">
    Enjoy hands-on server management with the most popular control panel available only for $12 per month.
</div>
<div id="features-slider" class="features-slider content-slider container" data-cs>
    <h2 class="title">Why choose cPanel VPS hosting?</h2>

    <div class="features-slider-wrapper" data-cs-wrapper>
        <div class="features-slide active" data-cs-slide="1">
            <div class="slider-image" style="max-width:530px;">
                <img src="<?php bloginfo('template_directory'); ?>/landings/images/cpanel_logo.jpg" alt="fi">
            </div>
            <h3 class="slide-title">Complete reliability</h3>
            <span class="slide-text">cPanel is the industry standard control panel that has gained popularity for easy server administration. While used for Linux-based VPS hosting services, it provides strong control and immaculate stability.</span>
        </div>
        <div class="features-slide" data-cs-slide="2" style="">
            <div class="slider-image">
                <img src="<?php bloginfo('template_directory'); ?>/landings/images/productivity.jpg">
            </div>
            <h3 class="slide-title">Superior productivity</h3>
            <span class="slide-text">With full statistics you can track your website performance, bandwidth and disk space usage to increase your overall performance and discover your audience by analyzing your online traffic.</span>
        </div>
        <div class="features-slide" data-cs-slide="3" style="">
            <div class="slider-image">
                <img src="<?php bloginfo('template_directory'); ?>/landings/images/rocket.jpg">
            </div>
            <h3 class="slide-title">Top performance</h3>
            <span class="slide-text">Increase your VPS server efficiency! Host1Plus cPanel optimized servers boost your virtual machine’s performance and provide immaculate stability and worldwide connectivity.</span>
        </div>
        <div class="features-slide" data-cs-slide="4" style="">
            <div class="slider-image">
                <img src="<?php bloginfo('template_directory'); ?>/landings/images/management.jpg">
            </div>
            <h3 class="slide-title">Effortless management</h3>
            <span class="slide-text">Operate your server with ease with cPanel file management tools. Easy access, quick edit and reliable backup options, improved database and domain administration strengthen your control of your virtual machine.</span>
        </div>
        <div class="features-slide" data-cs-slide="5" style="">
            <div class="slider-image">
                <img src="<?php bloginfo('template_directory'); ?>/landings/images/security.jpg">
            </div>
            <h3 class="slide-title">Solid security</h3>
            <span class="slide-text">cPanel safeguards your access to the server and provides handy tools for reducing the chance of external attacks.</span>
        </div>
        <div class="features-slide" data-cs-slide="6" style="">
            <div class="slider-image">
                <img src="<?php bloginfo('template_directory'); ?>/landings/images/speed.jpg">
            </div>
            <h3 class="slide-title">Extreme speed</h3>
            <span class="slide-text">Get your work done in no time, as cPanel simplifies your virtual machine’s management thus ensuring speedy response times. Your websites, apps, online projects will run without any interruptions.</span>
        </div>
    </div>
    <div class="slide-selection" data-cs-paging></div>

    <div class="slider-navigation">
        <a data-cs-prev class="link previous disabled" data-cs-prev>
            <i class="fa fa-angle-left prev-handle nav-handle"></i>
        </a>
        <a data-cs-next class="link next" data-cs-next>
            <i class="fa fa-angle-right next-handle nav-handle"></i>
        </a>
    </div>
</div>

    <?php include __DIR__ . '/../htmlblocks/reviews_2.php'; ?>

<div class="what-else">
    <h2 class="title">Why choose Host1Plus?</h2>

    <div class="block-tabs">
        <div class="tabs-wrapper">
            <div class="app-tabs">
                <div class="table">
                <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active">14-DAY MONEY-BACK GUARANTEE</span></div>
                    <div class="cell"><span data-tab-link="feature.support" class="tab desktop">CUSTOMER SUPPORT</span></div>
                    <div class="cell"><span data-tab-link="feature.uptime" class="tab desktop">99.9% UPTIME</span></div>
                    <div class="cell"><span data-tab-link="feature.payment" class="tab desktop">FLEXIBLE PAYMENT OPTIONS</span></div>
                    <div class="cell"><span data-tab-link="feature.security" class="tab desktop">DDOS PROTECTION</span></div>
                    <div class="cell"><span data-tab-link="feature.resources" class="tab desktop">FLEXIBLE PLANS</span></div>
                </div>
            </div>
            <div class="tabs" data-tabs="feature">

            <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">14-Day Money-Back Guarantee</span>
            <div class="tab-content active" data-tab-id="money-back">
              <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.jpg">
              <span class="title">14-Day Money-Back Guarantee</span>
              <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
            </div>

              <span data-tab-link="feature.support" data-tab-toggle="self" class="tab mob">Customer Support</span>
              <div class="tab-content" data-tab-id="support">
                <img src="<?php bloginfo('template_directory'); ?>/landings/images/support.jpg">
                <span class="title">Customer Support</span>
                <p>Problems with your cPanel VPS hosting? We’re always ready to give you a hand. Contact our support department and get the help you need.</p>
              </div>
              <span data-tab-link="feature.uptime" data-tab-toggle="self" class="tab mob">99.9% Uptime</span>
              <div class="tab-content" data-tab-id="uptime">
                <img src="<?php bloginfo('template_directory'); ?>/landings/images/uptime.jpg">
                <span class="title">99.9% Uptime</span>
                <p>Choose us and preserve your online performance – we guarantee 99.9% uptime. And if we’re wrong - we always compensate our customers if any unexpected downtime occurs.</p>
              </div>
              <span data-tab-link="feature.payment" data-tab-toggle="self" class="tab mob">Flexible Payment Options</span>
              <div class="tab-content" data-tab-id="payment">
                <img src="<?php bloginfo('template_directory'); ?>/landings/images/payment.jpg">
                <span class="title">Flexible Payment Options</span>
                <p>Select your preferred payment method from a variety of options, including Credit Card, Paypal, Skrill, Paysera, CashU, Ebanx, Braintree, Alipay transactions and Bitcoin payments.</p>
              </div>
              <span data-tab-link="feature.security" data-tab-toggle="self" class="tab mob">DDOS Protection</span>
              <div class="tab-content" data-tab-id="security">
                <img src="<?php bloginfo('template_directory'); ?>/landings/images/security.jpg">
                <span class="title">DDoS Protection</span>
                <p>We provide free DDoS protection for all VPS users - stay safe from DDoS perpetrators and avoid critical attacks that may harm your virtual machine.</p>
              </div>
              <span data-tab-link="feature.resources" data-tab-toggle="self" class="tab mob">Flexible plans</span>
              <div class="tab-content" data-tab-id="resources">
                <img src="<?php bloginfo('template_directory'); ?>/landings/images/resources.jpg">
                <span class="title">Flexible Plans</span>
                <p>Grab more resources to support your growing needs! You can add resources whenever you want.</p>
              </div>
            </div>
        </div>
    </div>
</div>

<?php include __DIR__ . '/block-vps-locations.php'; ?>

<div class="faq2">
    <h2 class="title">Ask questions, get answers!</h2>
    <div class="tabs container">
        <h3 class="tab active" data-tab-index="faq">FAQ</h3>
        <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
        <span class="tab-spacing"></span>
    </div>
    <div class="tabs-content container">
        <div class="tab-content active" data-tab-content="faq">
            <div class="question">
                How does cPanel VPS hosting differ from regular VPS hosting?
                <div class="answer">
                    cPanel VPS hosting resembles to regular VPS hosting, but it also includes a convenient control panel for simple virtual machine management and is especially suited for website management. Powerful Host1Plus cPanel VPS servers are available in a wide range of locations worldwide, Frankfurt, Germany; Johannesburg, South Africa; Los Angeles and Chicago, US; and Sao Paulo, Brazil.
                </div>
            </div>
            <div class="question">
                Do I have to pay for cPanel separately?
                <div class="answer">
                    When buying our affordable VPS with cPanel , you have to add a cPanel license to your hosting package at the checkout. If you encounter any difficulties installing cPanel, please contact our technical support.
                </div>
            </div>
            <div class="question">
                How can I add more resources to my cPanel VPS?
                <div class="answer">
                    As we offer flexible VPS hosting plans, you can upgrade or downgrade your resources anytime by logging in to your Client Area and adjusting your preferences.
                </div>
            </div>
            <div class="question">
                How cPanel VPS hosting is different from cPanel web hosting services?
                <div class="answer">
                    cPanel VPS hosting is the best option for people with higher demands. In comparison, cPanel VPS provides more resources and more control than regular cPanel web hosting plans. The user is also given the ability to increase or decrease the amount of server resources when needed. cPanel VPS is equipped with a full paid cPanel license which can be used separately – even if you choose to switch to another hosting service or change your plan.
                </div>
            </div>
        </div>
        <div class="tab-content" data-tab-content="tutorials">
            <div class="tutorial-list">
                <div class="tutorial-row">
                    <div class="tutorial-item">
                        <a target="_blank" href="http://www.host1plus.com/tutorials/control-panels/cpanel/all-cpanel-tutorials/how-to-install-cpanel-in-host1plus"><i class="fa fa-file-text-o file-ico"></i> How to install cPanel at Host1Plus</a>
                    </div>
                    <div class="tutorial-item">
                        <a target="_blank" href="http://www.host1plus.com/tutorials/control-panels/cpanel/all-cpanel-tutorials/cpanel-requirements"><i class="fa fa-file-text-o file-ico"></i> cPanel Requirements </a>
                    </div>
                </div>
                <div class="tutorial-row">
                    <div class="tutorial-item">
                        <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services-tutorials/web-hosting-services/all-web-hosting-tutorials/setting-dns-in-cpanel"><i class="fa fa-file-text-o file-ico"></i> Setting DNS in cPanel </a>
                    </div>
                    <div class="tutorial-item">
                        <a target="_blank" href="http://www.host1plus.com/tutorials/control-panels/cpanel/how-to-add-domain-in-cpanel"><i class="fa fa-file-text-o file-ico"></i> How to Add Addon Domain in cPanel</a>
                    </div>
                </div>
                <div class="tutorial-row">
                    <div class="tutorial-item">
                        <a target="_blank" href="http://www.host1plus.com/tutorials/control-panels/cpanel/all-cpanel-tutorials/how-to-create-email-in-cpanel"><i class="fa fa-file-text-o file-ico"></i> How to Create Email Account in cPanel </a>
                    </div>
                    <div class="tutorial-item">
                        <a target="_blank" href="http://www.host1plus.com/tutorials/control-panels/cpanel/all-cpanel-tutorials/how-to-create-ftp-account-in-cpanel"><i class="fa fa-file-text-o file-ico"></i> How to Create FTP Account in cPanel</a>
                    </div>
                </div>
                <div class="tutorial-row">
                    <div class="tutorial-item">
                        <a target="_blank" href="http://www.host1plus.com/tutorials/control-panels/cpanel/all-cpanel-tutorials/how-to-set-password-for-directory-in-cpanel"><i class="fa fa-file-text-o file-ico"></i> How to Set Password for Directory in cPanel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="back-to-top">
    <div class="back-box">
        <table>
        <tbody>
        <tr>
            <td><div class="main-text">Choose the right cPanel VPS hosting plan for you!</div></td>
            <td><a href="<?php echo $config['whmcs_links']['checkout_vps']?>?location=us_east&plan=bronze" class="button">Explore Now</a></td>
        </tr>
        </tbody>
        </table>
    </div>
</div>

<div class="prefooter">
    <h2 class="title">Looking for other cPanel hosting solutions?</h2>
    <h3 class="sub-sub-title">cPanel Web Hosting</h3>
    <p>Try cPanel web hosting and enjoy effortless website management for an affordable price.</p>
    <a href="<?php echo $config['links']['website'] . '/cpanel-hosting/'; ?>" class="button">Learn more</a>
</div>

<?php get_footer(); ?>
