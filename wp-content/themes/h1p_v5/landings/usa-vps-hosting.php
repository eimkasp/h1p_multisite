<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: USA VPS Hosting (old)
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-v2.style', get_template_directory_uri() . '/landings/css/landing-v2.css', ['landings.style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);


wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

$vps_hosting_min_price = $whmcs->getMinPrice($config['products']['vps_hosting']['locations']['chicago']['id']);
$vps_la_hosting_min_price = $whmcs->getMinPrice($config['products']['vps_hosting']['locations']['los_angeles']['id']);

get_header(); ?>

<div id="usa-vps">

    <div class="landing-header tablet-pad">
        <h1 class="page-title">USA VPS Hosting</h1>
        <h2 class="page-subtitle">Powerful USA VPS hosting from <?php echo $vps_hosting_min_price ?> per month</h2>

        <div class="choose-location">

            <div class="title">Choose your preferred server location</div>
            <div class="location-buttons">
                <table style="margin:auto;">
                    <tr class="buttons">
                        <td>
                            <a href="<?php echo $config['whmcs_links']['checkout_vps']?>?location=us_east&plan=amber" class="button big orange">Chicago</a>
                        </td>
                        <td style="padding-left:20px;">
                            <a href="<?php echo $config['whmcs_links']['checkout_vps']?>?location=us_west&plan=amber" class="button button big orange">Los Angeles</a>
                        </td>
                    </tr>
                    <tr class="prices">
                        <td>
                            from <?php echo $vps_hosting_min_price ?> / mo
                        </td>
                        <td>
                            from <?php echo $vps_la_hosting_min_price ?> / mo
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="notice compare" onclick="$('html, body').animate({scrollTop: $('#vps-compare').offset().top}, 2000);">
            Compare our offer with other popular USA VPS providers
        </div>

    </div>

    <div class="usa-locations">
        <div class="container">
            <div class="loc-row">

                <div class="col">
                    <div class="title"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/US.png" alt="" />Los Angeles, CA</div>
                    <div class="content">
                        <p>
                            Located in a dynamic junction of technology innovation with incomparable access to the American and Asia-Pacific regions, this data center will ensure you continuous power with no cut-offs, extremely low latency and unmatched connectivity.
                        </p>
                        <ul>
                            <li>Caterpillar and Katolight diesel generators</li>
                            <li>Powerware UPS</li>
                            <li>20GB – level 3 communications</li>
                            <li>10GE – private peering (Comcast)</li>
                        </ul>
                    </div>
                </div>
                <div class="col">
                    <div class="title"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/US.png" alt="" />Chicago, IL</div>
                    <div class="content">
                        <p>
                            This 40,000 square feet data center is a powerful infrastructure with diesel generators and constant air conditioning systems. It ensures rock-solid reliability and high uptime standards.
                        </p>
                        <ul>
                            <li>Rotary UPS systems</li>
                            <li>Diesel-powered generators</li>
                            <li>Minimum redundancy</li>
                            <li>Low latency peering with top traffic destinations</li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div  class="features-boxes container">
        <h2 class="title">Why choose VPS hosting USA?</h2>

        <div class="feature-box">
            <div class="fico">
                <i class="ico high-speed"></i>
            </div>
            <div class="ftitle">High Connection Speed</div>
            <div class="fcontent">
                Careful, attentive and constant server monitoring by our professional network administrators team makes sure you get the best connection speed possible.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico ddos"></i>
            </div>
            <div class="ftitle">DDoS Protection</div>
            <div class="fcontent">
                Looking for more security? Our USA VPS hosting is fully protected from any kind of DDoS attacks and is absolutely free.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico cpu"></i>
            </div>
            <div class="ftitle">Xeon Processors</div>
            <div class="fcontent">
                Enjoy improved service quality with server-grade CPUs! Create a faultless server with higher core counts and less vulnerability.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico hdd"></i>
            </div>
            <div class="ftitle">Hot Swappable Hard Drives</div>
            <div class="fcontent">
                Avoid disturbance! If a failure occurs – you won’t even notice as hard drives are replaced without shutting down the whole server.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico dns"></i>
            </div>
            <div class="ftitle">DNS Management</div>
            <div class="fcontent">
                A hassle-free solution for your domain management – create and edit your domain zones with no extra support.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico console"></i>
            </div>
            <div class="ftitle">Full Root Access</div>
            <div class="fcontent">
                100% flexibility and control! Enjoy full server control - install applications, manage your data, edit user permissions and set limits independently.
            </div>
        </div>
    </div>

    <div class="locations-wrapper tablet-pad">
        <div class="title">Looking for something else?</div>
        <div class="subtitle">Increase your connection speed and reduce latency by picking up another nearby VPS location</div>

        <div class="choose-location">
            <div class="locations-list">

                <?php
                    $plan = 'amber';
                    $planConfig = $whmcs->get_local_stepsconfig('vps_plans')[ $plan ];
                    $locations = $config['products']['vps_hosting']['locations'];


                    foreach($locations as $location_key => $location):
                        if( in_array( $location_key, [ 'los_angeles', 'chicago' ] ) ) continue;

                        $prices = $whmcs->getConfigurablePrice($location['id'], $planConfig['configs']);
                 ?>

                    <div class="location" onclick="$(this).find('form').submit();">
                        <form class="" action="<?php echo $config['whmcs_links']['checkout_vps']?>" method="get">

                            <input type="hidden" name="plan" value="<?php echo $plan;?>"/>
                            <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                            <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                            <input type="hidden" name="promocode" value="<?php echo $whmcs_promo;?>"/>
                            <input type="hidden" name="location" value="<?php echo $location['key'] ?>"/>
                            <input type="hidden" name="plan" value="amber"/>

                            <div class="label">
                                <?php
                                switch ($location_key){
                                    case 'los_angeles':
                                        echo __('Los Angeles');
                                        break;
                                    case 'chicago':
                                        echo __('Chicago');
                                        break;
                                    case 'sao_paulo':
                                        echo __('São Paulo');
                                        break;
                                    case 'frankfurt':
                                        echo __('Frankfurt');
                                        break;
                                    case 'johannesburg':
                                        echo __('Johannesburg');
                                        break;
                                }
                                ?>
                            </div>
                            <div><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/<?php echo strtoupper( $location['country_code'] ) ?>.png" alt="" width="80px"/></div>
                            <div class="price-notice">from <span class="price"><?php echo $prices[0]['price']; ?></span> / month</div>
                        </form>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>

    </div>

    <div class="what-else">
        <h2 class="title">Why choose Host1Plus?</h2>

        <div class="block-tabs">
            <div class="tabs-wrapper">
                <div class="app-tabs">
                    <div class="table">
                        <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active">MONEY-BACK GUARANTEE</span></div>
                        <div class="cell"><span data-tab-link="feature.hidden-fees" class="tab desktop">NO HIDDEN FEES</span></div>
                        <div class="cell"><span data-tab-link="feature.responsive" class="tab desktop">RESPONSIVE CLIENT AREA</span></div>
                        <div class="cell"><span data-tab-link="feature.multilingual" class="tab desktop">MULTILINGUAL TECH SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.savings" class="tab desktop">MAJOR SAVINGS</span></div>
                    </div>
                </div>
                <div class="tabs" data-tabs="feature">

                    <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">MONEY-BACK GUARANTEE</span>
                    <div class="tab-content active" data-tab-id="money-back">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.jpg">
                        <span class="title">MONEY-BACK GUARANTEE</span>
                        <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
                    </div>

                    <span data-tab-link="feature.hidden-fees" data-tab-toggle="self" class="tab mob">NO HIDDEN FEES</span>
                    <div class="tab-content" data-tab-id="hidden-fees">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/hidden-fees.jpg">
                        <span class="title">NO HIDDEN FEES</span>
                        <p>Fair prices guaranteed! Enjoy your hosting services with no extra costs.</p>
                    </div>

                    <span data-tab-link="feature.responsive" data-tab-toggle="self" class="tab mob">RESPONSIVE CLIENT AREA</span>
                    <div class="tab-content" data-tab-id="responsive">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/responsive.jpg">
                        <span class="title">RESPONSIVE CLIENT AREA</span>
                        <p>Always on the go? Easily manage your services using any mobile device.</p>
                    </div>

                    <span data-tab-link="feature.multilingual" data-tab-toggle="self" class="tab mob">MULTILINGUAL TECH SUPPORT</span>
                    <div class="tab-content" data-tab-id="multilingual">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/multilingual.jpg">
                        <span class="title">MULTILINGUAL TECH SUPPORT</span>
                        <p>No more language barriers! We speak English, Lithuanian, Spanish and Portuguese.</p>
                    </div>

                    <span data-tab-link="feature.savings" data-tab-toggle="self" class="tab mob">MAJOR SAVINGS</span>
                    <div class="tab-content" data-tab-id="savings">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/savings.jpg">
                        <span class="title">MAJOR SAVINGS</span>
                        <p>Sign up for an extended billing cycle and save up to 11% for your purchase!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <?php include __DIR__ . '/../htmlblocks/reviews_2.php'; ?>

    <div id="vps-compare" class="compare-block">
        <div class="container">
            <h2 class="title">Join Host1Plus and get an affordable VPS USA hosting solution with highly-competitive server resources</h2>

            <div class="table-wrap">

                <table>
                    <tbody>
                        <tr>
                            <td class="left date">
                                Data taken at 21/08/2015
                            </td>
                            <td class="h1p logo">
                                <div class="pic host1plus"></div>
                                <div class="title">Host1Plus</div>
                            </td>
                            <td class="value logo">
                                <div class="pic linode"></div>
                                <div class="title">Linode</div>
                            </td>
                            <td class="value logo">
                                <div class="pic hostgator"></div>
                                <div class="title">Hostgator</div>
                            </td>
                            <td class="value logo">
                                <div class="pic and1"></div>
                                <div class="title">1and1</div>
                            </td>
                            <td class="value logo">
                                <div class="pic bluehost"></div>
                                <div class="title">Bluehost</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                CPU
                            </td>
                            <td class="value h1p">
                                2 Cores
                            </td>
                            <td class="value">
                                2 Cores
                            </td>
                            <td class="value">
                                2 Cores
                            </td>
                            <td class="value">
                                2 Cores
                            </td>
                            <td class="value">
                                2 Cores
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                RAM
                            </td>
                            <td class="value h1p">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Storage
                            </td>
                            <td class="value h1p">
                                80 GB
                            </td>
                            <td class="value">
                                60 GB
                            </td>
                            <td class="value">
                                120 GB
                            </td>
                            <td class="value">
                                150 GB
                            </td>
                            <td class="value">
                                30 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Bandwidth
                            </td>
                            <td class="value h1p">
                                2000 GB
                            </td>
                            <td class="value">
                                3000 GB
                            </td>
                            <td class="value">
                                1500 GB
                            </td>
                            <td class="value">
                                Unlimited
                            </td>
                            <td class="value">
                                1000 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Price
                            </td>
                            <td class="value h1p">
                                $14.70
                            </td>
                            <td class="value">
                                $20.00
                            </td>
                            <td class="value">
                                $79.95
                            </td>
                            <td class="value">
                                $19.99
                            </td>
                            <td class="value">
                                $29.99
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                OS
                            </td>
                            <td class="value h1p tsmaller">
                                Ubuntu, Fedora, Debian, CentOS, Suse
                            </td>
                            <td class="value tsmaller">
                                Arch Linux, CentOS, Debian, Fedora, Gentoo, openSUSE, Slackware, Ubuntu
                            </td>
                            <td class="value tsmaller">
                                CentOS, Ubuntu
                            </td>
                            <td class="value tsmaller">
                                CentOS, openSUSE, Ubuntu, Debian, Windows Server 2008 R2 SP1, Windows Server 2008 R2 SP1 with Parallels Plesk 12
                            </td>
                            <td class="value tsmaller">
                                CentOS
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                DDoS Protection
                            </td>
                            <td class="value h1p">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                DNS Management
                            </td>
                            <td class="value h1p">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Tech support
                            </td>
                            <td class="value h1p tsmaller">
                                Tickets
                            </td>
                            <td class="value tsmaller">
                                Phone, email
                            </td>
                            <td class="value tsmaller">
                                Live chat, phone, tickets
                            </td>
                            <td class="value tsmaller">
                                Phone, email
                            </td>
                            <td class="value tsmaller">
                                Live chat, tickets
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Data center locations
                            </td>
                            <td class="value h1p tsmaller">
                                Chicago, Los Angeles
                            </td>
                            <td class="value tsmaller">
                                Newark, Atlanta, Dallas, Fremont
                            </td>
                            <td class="value tsmaller">
                                Provo, Houston
                            </td>
                            <td class="value tsmaller">
                                Kansas
                            </td>
                            <td class="value tsmaller">
                                Utah
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="compare-notice">
                    *Third-party logos and marks are registered trademarks of their respective owners. All rights reserved.
                </div>

            </div>
        </div>

    </div>

    <div class="faq2">
        <h2 class="title">Ask questions, get answers!</h2>
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq">FAQ</h3>
            <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    How long will my VPS setup take?
                    <div class="answer">
                        After you order a VPS, the service is usually set up instantly. However, if you pay using a credit card, it might take up to a few hours.
                    </div>
                </div>
                <div class="question">
                    Which control panels can I install on my VPS?
                    <div class="answer">
                        You can install open source control panels or any other web server management panel by yourself such as cPanel, ISPconfig, Plesk, Ajenti, Kloxo, OpenPanel, ZPanel, EHCP, ispCP, VHCS, RavenCore, Virtualmin, Webmin, DTC, DirectAdmin, InterWorx, SysCP, BlueOnyx and more.
                    </div>
                </div>
                <div class="question">
                    What virtualization do you use for VPS hosting?
                    <div class="answer">
                        We are currently using OpenVZ virtualization.
                    </div>
                </div>
                <div class="question">
                    How to upgrade my VPS?
                    <div class="answer">
                        Log in to your Client Area and click Services > VPS Hosting. <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1241/108/how-to-upgradedowngrade-my-vps">Read more</a>.
                    </div>
                </div>
                <div class="question">
                    How to purchase extra-care service for my VPS?
                    <div class="answer">
                        Log in to your Client Area and click on Services > VPS Hosting. <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1236/108/how-to-purchase-extra-care-service-for-my-vps">Read more</a>.
                    </div>
                </div>
                <div class="question">
                    How do I create a virtual console on my VPS?
                    <div class="answer">
                        First, log in to your Client Area and click Services > VPS Hosting.
                    </div>
                </div>
            </div>
            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/how-to-create-virtualhost-on-apache"><i class="fa fa-file-text-o file-ico"></i> How to create virtualhost on Apache?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-csf-on-linux"><i class="fa fa-file-text-o file-ico"></i> How to install CSF on Linux?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/introduction-to-ssh-keys"><i class="fa fa-file-text-o file-ico"></i> Introduction to SSH keys</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-gnome-desktop"><i class="fa fa-file-text-o file-ico"></i> How to install GNOME desktop?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-change-a-time-zone-on-a-vps-server-2"><i class="fa fa-file-text-o file-ico"></i> How to change a time zone on a VPS server?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-add-ip-address-on-linux-operating-systems"><i class="fa fa-file-text-o file-ico"></i> How to add IP address on CentOS operating system?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="back-to-top">
        <div class="back-box">
            <table>
            <tbody>
            <tr>
                <td><div class="main-text">Kick off and get started with our USA VPS hosting!</div></td>
                <td><a href="<?php echo $config['whmcs_links']['checkout_vps']?>?location=us_east&plan=amber" class="button">Explore Now</a></td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>

    <div class="prefooter">
        <h3 class="sub-title">Looking for other hosting solutions in USA?</h3>
        <a href="<?php echo $config['links']['web-hosting']?>" class="button">Web hosting in USA</a> <a href="<?php echo $config['links']['reseller-hosting']?>" class="button" style="margin-left:0px;">Reseller hosting in USA</a>
    </div>

</div>

<?php get_footer(); ?>