<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: Ubuntu VPS Hosting (old)
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);


wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

get_header();

    global $whmcs;
    global $config;

    $web_hosting_min_price = $whmcs->getMinPrice($config['products']['web_hosting']['plans'][1]['id']);
    $vps_hosting_min_price = $whmcs->getMinPrice($config['products']['vps_hosting']['locations']['chicago']['id']);
    $dedicated_servers_min_price = $whmcs->getPriceOptions($config['products']['dedicated_servers']['plans'][1]['id'], 'quarterly')['price_monthly'];
    $reseller_hosting_min_price = $whmcs->getMinPrice($config['products']['reseller_hosting']['plans'][1]['id']);
?>

<div id="ubuntu-hosting">

    <div class="landing-header tablet-pad">
        <div class="container">
            <h1 class="page-title"><span class="oslogo ubuntu"></span>Ubuntu VPS Hosting</h1>
            <h2 class="page-subtitle">Root access Ubuntu VPS hosting operating across 4 continents</h2>
        </div>

            <div class="choose-location">

                <div class="title">Choose your server location</div>
                <div><span class="arrow-down"></span></div>
                <div class="locations-list">

                    <?php
                        $plan = 'amber';
                        $planConfig = $whmcs->get_local_stepsconfig('vps_plans')[ $plan ];
                        $locations = $config['products']['vps_hosting']['locations'];

                        $first_loc_override_key = 'chicago';
                        if( array_key_exists( $first_loc_override_key, $locations) ){
                            $first_loc_override = $locations[$first_loc_override_key]; //which location should be first
                            unset( $locations[$first_loc_override_key] );
                            $locations = [$first_loc_override_key => $first_loc_override] + $locations;
                        }

                        foreach($locations as $location_key => $location):

                            $prices = $whmcs->getConfigurablePrice($location['id'], $planConfig['configs']);
                     ?>

                        <div class="location" onclick="$(this).find('form').submit();">
                            <form class="" action="<?php echo $config['whmcs_links']['checkout_vps']?>" method="get">

                                <input type="hidden" name="plan" value="<?php echo $plan;?>"/>
                                <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                                <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                                <input type="hidden" name="promocode" value="<?php echo $whmcs_promo;?>"/>
                                <input type="hidden" name="cstep" value="vps-package"/>
                                <input type="hidden" name="location" value="<?php echo $location['key'] ?>"/>

                                <div class="label">
                                    <?php
                                    switch ($location_key){
                                        case 'los_angeles':
                                            echo __('Los Angeles');
                                            break;
                                        case 'chicago':
                                            echo __('Chicago');
                                            break;
                                        case 'sao_paulo':
                                            echo __('São Paulo');
                                            break;
                                        case 'frankfurt':
                                            echo __('Frankfurt');
                                            break;
                                        case 'johannesburg':
                                            echo __('Johannesburg');
                                            break;
                                    }
                                    ?>
                                </div>
                                <div><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/<?php echo strtoupper( $location['country_code'] ) ?>.png" alt="" width="80px"/></div>
                                <div class="price-notice">from <span class="price"><?php echo $prices[0]['price']; ?></span> / month</div>
                            </form>
                        </div>

                    <?php endforeach; ?>
                </div>
                <div class="distributions">
                    <strong>Available CentOS distributions:</strong> 14.04 x86_64&nbsp;&nbsp;|&nbsp;&nbsp;14.04 x86&nbsp;&nbsp;|&nbsp;&nbsp;12.04 x86_64&nbsp;&nbsp;|&nbsp;&nbsp;12.04 x86
                    <div class="compare" onclick="$('html, body').animate({scrollTop: $('#vps-compare').offset().top}, 2000);">
                        <span class="underline">Compare our offer with other popular Ubuntu VPS providers</span>&nbsp;&nbsp;&nbsp;<i class="fa fa-sort-desc"></i>
                    </div>
                </div>
            </div>

    </div>

    <div class="features-boxes container">
        <h2 class="title">
            Our Ubuntu VPS overview
        </h2>
        <div class="feature-box">
            <div class="fico">
                <i class="ico settings"></i>
            </div>
            <div class="ftitle">Easy Software Management</div>
            <div class="fcontent">
                Choose an application to install at our checkout or find the needed software among thousands of available options at Ubuntu software center.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico protection"></i>
            </div>
            <div class="ftitle">DDoS Protection</div>
            <div class="fcontent">
                100% flexibility and control! Enjoy adjustable software options and administer your whole service to achieve the best levels of optimization.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico console"></i>
            </div>
            <div class="ftitle">Full Root Access</div>
            <div class="fcontent">
                100% flexibility and control! Enjoy adjustable software options and administer your whole service to achieve the best levels of optimization.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico scalable"></i>
            </div>
            <div class="ftitle">Scalable Resources</div>
            <div class="fcontent">
                Pay only for what you use – change server resources at our Client Area according to your needs.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico speed2"></i>
            </div>
            <div class="ftitle">High Network Speed</div>
            <div class="fcontent">
                Benefit from high network speed and low latency connections to North or South America, Europe and Africa ensured by our professional network administrators.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico dns"></i>
            </div>
            <div class="ftitle">DNS Management</div>
            <div class="fcontent">
                Get full control over your domains! Create or edit its zones, as well as point them to other destination you need.
            </div>
        </div>
    </div>

        <?php include __DIR__ . '/../htmlblocks/reviews_2.php'; ?>

    <div class="what-else">
        <h2 class="title">Why choose Host1Plus?</h2>

        <div class="block-tabs">
            <div class="tabs-wrapper">
                <div class="app-tabs">
                    <div class="table">
                        <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active">14-DAY MONEY-BACK GUARANTEE</span></div>
                        <div class="cell"><span data-tab-link="feature.multilingual" class="tab desktop">MULTILINGUAL TECH SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.savings" class="tab desktop">MAJOR SAVINGS</span></div>
                        <div class="cell"><span data-tab-link="feature.support" class="tab desktop">EXTRA-CARE SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.responsive" class="tab desktop">RESPONSIVE CLIENT AREA</span></div>
                    </div>
                </div>
                <div class="tabs" data-tabs="feature">

                    <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">14-DAY MONEY-BACK GUARANTEE</span>
                    <div class="tab-content active" data-tab-id="money-back">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.jpg">
                        <span class="title">14-Day Money-Back Guarantee</span>
                        <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
                    </div>

                    <span data-tab-link="feature.multilingual" data-tab-toggle="self" class="tab mob">MULTILINGUAL TECH SUPPORT</span>
                    <div class="tab-content" data-tab-id="multilingual">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/multilingual.jpg">
                        <span class="title">Multilingual Tech Support</span>
                        <p>No more language barriers! We speak English, Spanish, Portuguese and Lithuanian.</p>
                    </div>

                    <span data-tab-link="feature.savings" data-tab-toggle="self" class="tab mob">MAJOR SAVINGS</span>
                    <div class="tab-content" data-tab-id="savings">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/savings.jpg">
                        <span class="title">Major Savings</span>
                        <p>Sign up for an extended billing cycle and save up to 11% for your purchase!</p>
                    </div>

                    <span data-tab-link="feature.support" data-tab-toggle="self" class="tab mob">EXTRA-CARE SUPPORT</span>
                    <div class="tab-content" data-tab-id="support">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/support.jpg">
                        <span class="title">Extra-Care Support</span>
                        <p>Order extra-care support and cover it all – server monitoring and management, network issue resolution, basic security check and much more.</p>
                    </div>

                    <span data-tab-link="feature.responsive" data-tab-toggle="self" class="tab mob">RESPONSIVE CLIENT AREA</span>
                    <div class="tab-content" data-tab-id="responsive">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/responsive.jpg">
                        <span class="title">Responsive Client Area</span>
                        <p>Always on the go? Easily manage your services using any mobile device.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div id="vps-compare" class="compare-block">
        <div class="container">
            <h2 class="title">Take a look at Host1Plus Ubuntu VPS hosting<br>offer compared to other well-known hosting providers</h2>

            <div class="table-wrap">

                <table class="white-cols">
                    <tbody>
                        <tr>
                            <td class="left date">
                                Data taken at 02/09/2015
                            </td>
                            <td class="h1p logo">
                                <div class="pic host1plus"></div>
                                <div class="title">Host1Plus</div>
                            </td>
                            <td class="value logo">
                                <div class="pic linode"></div>
                                <div class="title">Linode</div>
                            </td>
                            <td class="value logo">
                                <div class="pic hostgator"></div>
                                <div class="title">Hostgator</div>
                            </td>
                            <td class="value logo">
                                <div class="pic and1"></div>
                                <div class="title">1and1</div>
                            </td>
                            <td class="value logo">
                                <div class="pic digital-ocean"></div>
                                <div class="title">DigitalOcean</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                CPU
                            </td>
                            <td class="value h1p">
                                2 Cores
                            </td>
                            <td class="value">
                                2 Cores
                            </td>
                            <td class="value">
                                2 Cores
                            </td>
                            <td class="value">
                                2 Cores
                            </td>
                            <td class="value">
                                2 Cores
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                RAM
                            </td>
                            <td class="value h1p">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Storage
                            </td>
                            <td class="value h1p">
                                80 GB
                            </td>
                            <td class="value">
                                60 GB
                            </td>
                            <td class="value">
                                120 GB
                            </td>
                            <td class="value">
                                150 GB
                            </td>
                            <td class="value">
                                40 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Bandwidth
                            </td>
                            <td class="value h1p">
                                2000 GB
                            </td>
                            <td class="value">
                                3000 GB
                            </td>
                            <td class="value">
                                1500 GB
                            </td>
                            <td class="value">
                                Unlimited
                            </td>
                            <td class="value">
                                3000 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Price
                            </td>
                            <td class="value h1p">
                                $14.70
                            </td>
                            <td class="value">
                                $20.00
                            </td>
                            <td class="value">
                                $79.95
                            </td>
                            <td class="value">
                                $19.99
                            </td>
                            <td class="value">
                                $20.00
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Available Ubuntu OS distributions
                            </td>
                            <td class="value h1p tsmaller">
                                Ubuntu 14.04<br>Ubuntu 12.04
                            </td>
                            <td class="value tsmaller">
                                Ubuntu 15.04<br>Ubuntu 14.10<br>Ubuntu 14.04<br>Ubuntu 12.04
                            </td>
                            <td class="value tsmaller">
                                Ubuntu 14.04<br>Ubuntu 12.04
                            </td>
                            <td class="value tsmaller">
                                Ubuntu 14.04
                            </td>
                            <td class="value tsmaller">
                                Ubuntu 14.04<br>Ubuntu 12.04.5<br>Ubuntu 10.04
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                DDoS Protection
                            </td>
                            <td class="value h1p">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                DNS Management
                            </td>
                            <td class="value h1p">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Data Center Locations
                            </td>
                            <td class="value h1p tsmaller">
                                Chicago, USA<br>Los Angeles, USA<br>São Paulo, BR<br>Frankfurt, DE<br>Johannesburg, ZA
                            </td>
                            <td class="value tsmaller">
                                Singapore<br>North America<br>Frankfurt, DE<br>London, UK
                            </td>
                            <td class="value tsmaller">
                                Houston, USA<br>Provo, USA
                            </td>
                            <td class="value tsmaller">
                                Kansas, USA<br>Germany<br>United Kingdom
                            </td>
                            <td class="value tsmaller">
                                San Francisco, USA<br>New York, USA<br>London, UK<br>Amsterdam, NL<br>Frankfurt, DE<br>Singapore
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="compare-notice">
                    *Third-party logos and marks are registered trademarks of their respective owners. All rights reserved.
                </div>

            </div>
        </div>

    </div>

    <?php include __DIR__ . '/../htmlblocks/payment_methods.php'; ?>

    <div class="faq2">
        <h2 class="title">Ask questions, get answers!</h2>
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq">FAQ</h3>
            <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    How long will my VPS setup take?
                    <div class="answer">
                        After you order a VPS, the service is usually set up instantly. However, if you pay using a credit card, it might take up to a few hours.
                    </div>
                </div>
                    <div class="question">
                        What virtualization do you use for VPS hosting?
                        <div class="answer">
                            We are currently using OpenVZ virtualization.
                        </div>
                    </div>
                <div class="question">
                    What is the network speed of your Linux VPS hosting?
                    <div class="answer">
                        We limit the network speed provided depending on the location of your VPS hosting service.<br>
                        <br>
                        <strong>Outgoing</strong> (data that is being sent from your server to another one) network speed is limited up to 500 Mbps in all VPS hosting locations except São Paulo, Brazil, where network speed limit is higher - up to 200 Mbps.<br>
                        <br>
                        <strong>Incoming</strong> (data that is being transferred to your server from another one) network speed does not have any limitations.
                    </div>
                </div>
                <div class="question">
                    Is it possible to install a custom OS to OpenVZ VPS?
                    <div class="answer">
                        We are using only official OpenVZ templates only so it is not possible to install any custom OS.
                    </div>
                </div>
                <div class="question">
                    Which control panels can I install on my VPS?
                    <div class="answer">
                        You can manually install any free open source control panel that supports Ubuntu operating system, such as Sentora, Ajenti, Vesta and many more.
                    </div>
                </div>
            </div>
            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/how-to-install-lamp"><i class="fa fa-file-text-o file-ico"></i> How to install Apache, MySQL, PHP on Ubuntu?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/how-to-set-up-openvpn-on-linux-vps"><i class="fa fa-file-text-o file-ico"></i> How to set up OpenVPN on Linux?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-gnome-desktop"><i class="fa fa-file-text-o file-ico"></i> How to install desktop on Linux?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-change-a-time-zone-on-a-vps-server-2"><i class="fa fa-file-text-o file-ico"></i> How to change a time zone on VPS?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/how-to-setup-ip-rotations-for-emails-on-linux"><i class="fa fa-file-text-o file-ico"></i> How to setup IP rotations for emails on Linux</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="back-to-top">
        <div class="back-box">
            <table>
            <tbody>
            <tr>
                <td><div class="main-text">Join our worldwide community and benefit from powerful<br>Host1Plus Ubuntu VPS hosting</div></td>
                <td><a href="<?php echo $config['whmcs_links']['checkout_vps']?>?cstep=vps-package&location=us_east&plan=amber" class="button">Get started</a></td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>

    <div class="prefooter">
        <h3 class="sub-title">Looking for other OS options?</h3>
        <h3 class="sub-sub-title">Check out available Host1Plus Linux VPS solutions</h3>
        <a href="<?php echo $config['links']['website'] . '/linux-vps-hosting/'; ?>" class="button" style="">Linux VPS hosting</a>
    </div>

</div>

<?php get_footer(); ?>
