<div class="vps-locations tablet-pad" data-selected-loc="de">
    <h2 class="title">
        VPS Hosting Locations
    </h2>
    <div class="vps-locations-container">
        <div class="flags">
            <div class="flag de active" data-loc="de">
                <div class="ico"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/DE.png" alt="" width="68px"/></div>
                <div class="label">Frankfurt</div>
                <div class="content mob">
                    Being one of the most secure and reliable data centers in the world, our server location in Frankfurt sustains uptime and boosts performance.
                </div>

            </div><div class="flag br" data-loc="br">

                <div class="ico"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/BR.png" alt="" width="68px"/></div>
                <div class="label">São Paulo</div>
                <div class="content mob">
                    With a wide range of security equipment, the facility in São Paulo upholds a secure and reliable environment and preserves server performance.
                </div>

            </div><div class="flag za" data-loc="za">

                <div class="ico"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/ZA.png" alt="" width="68px"/></div>
                <div class="label">Johannesburg</div>
                <div class="content mob">
                    A reliable server environment in Johannesburg guarantees stability and lower latency for online projects, targeted to audiences in the whole African region.
                </div>

            </div><div class="flag us-la" data-loc="us-la">

                <div class="ico"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/US.png" alt="" width="68px"/></div>
                <div class="label">Los Angeles</div>
                <div class="content mob">
                    Premium data centers in LA, California, guarantee immaculate connectivity and ultra-fast response times.
                </div>

            </div><div class="flag us-ch" data-loc="us-ch">

                <div class="ico"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/US.png" alt="" width="68px"/></div>
                <div class="label">Chicago</div>
                <div class="content mob">
                    A modern data center in Chicago, Illinois, is distinguished by high security standards and thorough server upkeep.
                </div>
            </div>
        </div>
        <div class="about-location">
            <div class="location-description de active">
                <div class="title">
                    Frankfurt, Germany
                </div>
                <div class="content">
                    Being one of the most secure and reliable data centers in the world, our server location in Frankfurt sustains uptime and boosts performance.
                </div>
            </div>
            <div class="location-description za">
                <div class="title">
                    Johannesburg, South Africa
                </div>
                <div class="content">
                    A reliable server environment in Johannesburg guarantees stability and lower latency for online projects, targeted to audiences in the whole African region.
                </div>
            </div>
            <div class="location-description br">
                <div class="title">
                    São Paulo, Brazil
                </div>
                <div class="content">
                    With a wide range of security equipment, the facility in São Paulo upholds a secure and reliable environment and preserves server performance.
                </div>
            </div>
            <div class="location-description us-la">
                <div class="title">
                    Los Angeles, United States
                </div>
                <div class="content">
                    Premium data center in LA, California, guarantees immaculate connectivity and ultra-fast response times.
                </div>
            </div>
            <div class="location-description us-ch">
                <div class="title">
                    Chicago, United States
                </div>
                <div class="content">
                    A modern data center in Chicago, Illinois, is distinguished by high security standards and thorough server upkeep.
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hidden">
    <script type="text/javascript">
        <!--//--><![CDATA[//><!-- Preload images

            if (document.images) {
                img1 = new Image();
                img2 = new Image();
                img3 = new Image();
                img4 = new Image();
                img5 = new Image();

                img1.src = "<?php echo get_template_directory_uri() ?>/landings/images/chickago_bg.jpg";
                img2.src = "<?php echo get_template_directory_uri() ?>/landings/images/frankfurt_bg.jpg";
                img3.src = "<?php echo get_template_directory_uri() ?>/landings/images/losangeles_bg.jpg";
                img4.src = "<?php echo get_template_directory_uri() ?>/landings/images/johanesburg_bg.jpg";
                img5.src = "<?php echo get_template_directory_uri() ?>/landings/images/sanpaulo_bg.jpg";
            }

        //--><!]]>
    </script>
</div>