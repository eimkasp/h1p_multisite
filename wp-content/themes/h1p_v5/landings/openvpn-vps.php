<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: OpenVPN  VPS
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);


wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

get_header(); ?>

<div id="openvpn-vps">

    <div class="landing-header tablet-pad">
        <h1 class="page-title">OpenVPN VPS</h1>
        <h2 class="page-subtitle">Spin up your server with our affordable OpenVPN VPS hosting</h2>

        <div class="choose-location">

            <div class="title">Choose your server location</div>
            <div><span class="arrow-down"></span></div>
            <div class="locations-list">

                <?php
                    $plan = 'amber';
                    $planConfig = $whmcs->get_local_stepsconfig('vps_plans')[ $plan ];
                    $locations = $config['products']['vps_hosting']['locations'];

                    $first_loc_override_key = 'chicago';
                    if( array_key_exists( $first_loc_override_key, $locations) ){
                        $first_loc_override = $locations[$first_loc_override_key]; //which location should be first
                        unset( $locations[$first_loc_override_key] );
                        $locations = [$first_loc_override_key => $first_loc_override] + $locations;
                    }

                    foreach($locations as $location_key => $location):

                        $prices = $whmcs->getConfigurablePrice($location['id'], $planConfig['configs']);
                 ?>

                    <div class="location" onclick="$(this).find('form').submit();">
                        <form class="" action="<?php echo $config['whmcs_links']['checkout_vps']?>" method="get">

                            <input type="hidden" name="plan" value="<?php echo $plan;?>"/>
                            <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                            <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                            <input type="hidden" name="promocode" value="<?php echo $whmcs_promo;?>"/>
                            <input type="hidden" name="cstep" value="vps-package"/>
                            <input type="hidden" name="location" value="<?php echo $location['key'] ?>"/>

                            <div class="label">
                                <?php
                                switch ($location_key){
                                    case 'los_angeles':
                                        echo __('Los Angeles');
                                        break;
                                    case 'chicago':
                                        echo __('Chicago');
                                        break;
                                    case 'sao_paulo':
                                        echo __('São Paulo');
                                        break;
                                    case 'frankfurt':
                                        echo __('Frankfurt');
                                        break;
                                    case 'johannesburg':
                                        echo __('Johannesburg');
                                        break;
                                }
                                ?>
                            </div>
                            <div><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/<?php echo strtoupper( $location['country_code'] ) ?>.png" alt="" width="80px"/></div>
                            <div class="price-notice">from <span class="price"><?php echo $prices[0]['price']; ?></span> / month</div>
                        </form>
                    </div>

                <?php endforeach; ?>
            </div>
            <div class="os-container">
                <div class="title">Available Linux VPS Hosting Operating Systems</div>
                <div class="os-list">
                    <div class="os">
                        <div class="ico ubuntu"></div>
                        <div class="os-title">Ubuntu</div>
                    </div>
                    <div class="os">
                        <div class="ico fedora"></div>
                        <div class="os-title">Fedora</div>
                    </div>
                    <div class="os">
                        <div class="ico debian"></div>
                        <div class="os-title">Debian</div>
                    </div>
                    <div class="os">
                        <div class="ico centos"></div>
                        <div class="os-title">CentOS</div>
                    </div>
                    <div class="os">
                        <div class="ico suse"></div>
                        <div class="os-title">Suse</div>
                    </div>
                </div>
                <div class="bottom-notice"></div>
            </div>
        </div>

    </div>

    <div class="landing-notice what-is-vps">
        <div class="container">
        <table>
        <tbody>
            <tr>
                <td class="title">
                    What is OpenVPN?
                </td>
                <td class="description">
                    OpenVPN is an open source software creating remote connection technology. After installing OpenVPN to the server, you can create your own Virtual Private Network, where the IP address is given from a virtual subnet. This means that despite your actual location, your IP address will be visible as from the country where your server is located.
                </td>
            </tr>
        </tbody>
        </table>
        </div>
    </div>

    <div class="features-boxes container">
        <h2 class="title">
            OpenVPN VPS hosting overview
        </h2>
        <div class="feature-box">
            <div class="fico">
                <i class="ico privacy"></i>
            </div>
            <div class="ftitle">Enhanced Privacy</div>
            <div class="fcontent">
                Hide your IP with a VPN! Connect to your server and while browsing, protect your privacy by using the server’s IP address.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico portspeed"></i>
            </div>
            <div class="ftitle">High Port Speed</div>
            <div class="fcontent">
                Enjoy quick access and fast downloads/uploads! Our network speed is limited up to 500 Mbps in all locations, except São Paulo (200 Mbps).
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico remote"></i>
            </div>
            <div class="ftitle">Remote Network Access</div>
            <div class="fcontent">
                Install OpenVPN app to your mobile device and access your server anywhere, anytime and with anything!
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico multi-location"></i>
            </div>
            <div class="ftitle">Multi-Locational IP Addresses</div>
            <div class="fcontent">
                Get multiple IP addresses for an affordable cost! Pay only $2.00 / month for additional one.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico click"></i>
            </div>
            <div class="ftitle">Access Blocked Websites</div>
            <div class="fcontent">
                Browse with no limits! With an OpenVPN server you can access the content which is blocked by various internet censorship filters.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico console"></i>
            </div>
            <div class="ftitle">Open Source</div>
            <div class="fcontent">
                Download OpenVPN app with no expenses, easily <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-setup-openvpn-access-server-on-linux">install</a> it to your VPS and get started! And if you have questions – enjoy surfing through the public detailed app documentations.
            </div>
        </div>
    </div>

        <?php include __DIR__ . '/../htmlblocks/reviews_2.php'; ?>

    <div class="what-else">
        <h2 class="title">Why choose Host1Plus?</h2>

        <div class="block-tabs">
            <div class="tabs-wrapper">
                <div class="app-tabs">
                    <div class="table">
                        <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active">14-DAY MONEY-BACK GUARANTEE</span></div>
                        <div class="cell"><span data-tab-link="feature.flexible" class="tab desktop">FLEXIBLE PAYMENT OPTIONS</span></div>
                        <div class="cell"><span data-tab-link="feature.multilingual" class="tab desktop">MULTILINGUAL TECH SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.responsive" class="tab desktop">RESPONSIVE CLIENT AREA</span></div>
                        <div class="cell"><span data-tab-link="feature.support" class="tab desktop">EXTRA-CARE SUPPORT</span></div>
                    </div>
                </div>
                <div class="tabs" data-tabs="feature">

                    <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">14-DAY MONEY-BACK GUARANTEE</span>
                    <div class="tab-content active" data-tab-id="money-back">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.jpg">
                        <span class="title">14-Day Money-Back Guarantee</span>
                        <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
                    </div>

                    <span data-tab-link="feature.flexible" data-tab-toggle="self" class="tab mob">FLEXIBLE PAYMENT OPTIONS</span>
                    <div class="tab-content" data-tab-id="flexible">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/payment.jpg">
                        <span class="title">Flexible Payment Options</span>
                        <p>Choose from a variety of different payment options, including Credit Card, Paypal, Skrill, Paysera, CashU, Ebanx, Braintree, Alipay transactions and Bitcoin payments.</p>
                    </div>

                    <span data-tab-link="feature.multilingual" data-tab-toggle="self" class="tab mob">MULTILINGUAL TECH SUPPORT</span>
                    <div class="tab-content" data-tab-id="multilingual">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/multilingual.jpg">
                        <span class="title">Multilingual Tech Support</span>
                        <p>No more language barriers! We speak English, Lithuanian, Spanish and Portuguese.</p>
                    </div>

                    <span data-tab-link="feature.responsive" data-tab-toggle="self" class="tab mob">RESPONSIVE CLIENT AREA</span>
                    <div class="tab-content" data-tab-id="responsive">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/responsive.jpg">
                        <span class="title">Responsive Client Area</span>
                        <p>Always on the go? Easily manage your services using any mobile device.</p>
                    </div>

                    <span data-tab-link="feature.support" data-tab-toggle="self" class="tab mob">EXTRA-CARE SUPPORT</span>
                    <div class="tab-content" data-tab-id="support">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/support.png">
                        <span class="title">Extra-Care Support</span>
                        <p>Worried about managing your service? Extra-Care support will cover it all - server monitoring, management, network issue and performance resolution and much more.</p>
                    </div>

                </div>
            </div>
        </div>

    </div>

    <?php include __DIR__ . '/block-vps-locations.php'; ?>

    <div class="faq2">
        <h2 class="title">Ask questions, get answers!</h2>
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq">FAQ</h3>
            <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    How long will my VPS setup take?
                    <div class="answer">
                        After you order a VPS, the service is usually set up instantly. However, if you pay using a credit card, it might take up to a few hours.
                    </div>
                </div>
                <div class="question">
                    What virtualization do you use for VPS hosting?
                    <div class="answer">
                        We are currently using OpenVZ virtualization.
                    </div>
                </div>
                <div class="question">
                    Do I get a dedicated IP address with my VPS account?
                    <div class="answer">
                        Yes, you will get a dedicated IP address already included with any of our VPS plans.
                        However, if you need more, you can purchase them at the checkout or through your <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1241/0/how-to-upgradedowngrade-my-vps"></a>Client Area</a>.
                    </div>
                </div>
                <div class="question">
                    Is it possible to install a custom OS to OpenVZ VPS?
                    <div class="answer">
                        We are using only official OpenVZ templates only so it is not possible to install any custom OS.
                    </div>
                </div>
                <div class="question">
                    What if I reach my VPS bandwidth limit?
                    <div class="answer">
                        In case your server uses a lot of network resources, there is a chance that you might reach your monthly VPS bandwidth limit. If this happens, the network speed of your server will be limited to 64 Kbits/s and you will instantly receive an email with a notification about the limit. If you wish to solve this problem you may either wait for the month to end (or end of your billing cycle) when the bandwidth count starts over; or upgrade your bandwidth resources within your Client Area. Please keep in mind that in this case, the price of your VPS would increase.
                    </div>
                </div>
            </div>
            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/how-to-set-up-openvpn-on-linux-vps"><i class="fa fa-file-text-o file-ico"></i> How to set up OpenVPN on Linux VPS? </a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/common-log-files-on-a-linux-system"><i class="fa fa-file-text-o file-ico"></i> Common log files on a Linux system </a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/all-vps-hosting-tutorials/change-time-vps"><i class="fa fa-file-text-o file-ico"></i> How to change time in your VPS </a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/coding-scripting/security-basics/beginner-security-basics/secure-server-with-ssh-publicprivate-key-authentification"><i class="fa fa-file-text-o file-ico"></i> Secure server with SSH Public/Private key authentication </a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/how-to-setup-ip-rotations-for-emails-on-linux"><i class="fa fa-file-text-o file-ico"></i> How to setup IP rotations for emails on Linux </a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/introduction-to-ssh-keys"><i class="fa fa-file-text-o file-ico"></i> Introduction to SSH keys </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="back-to-top">
        <div class="back-box">
            <table>
            <tbody>
            <tr>
                <td><div class="main-text">Interested? Get our affordable OpenVPN VPS hosting<br>and benefit from fully scalable hosting solutions</div></td>
                <td><a href="<?php echo $config['whmcs_links']['checkout_vps']?>?cstep=vps-package&location=us_east&plan=amber" class="button">Sign up</a></td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>

    <div class="prefooter">
        <div class="container">
            <h3 class="sub-title">Looking for something else?</h3>
            <h3 class="sub-sub-title">Check our other hosting solutions</h3>
            <a href="<?php echo $config['links']['web-hosting'] ?>" class="button">Web hosting</a>
            <a href="<?php echo $config['links']['reseller-hosting'] ?>" class="button" style="margin-right:0px;">Reseller hosting</a>
        </div>
    </div>

</div>

<?php get_footer(); ?>
