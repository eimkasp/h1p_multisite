<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: South Africa VPS Hosting
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-v2.style', get_template_directory_uri() . '/landings/css/landing-v2.css', ['landings.style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);


wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

$vps_hosting_min_price = $whmcs->getMinPrice($config['products']['vps_hosting']['locations']['johannesburg']['id']);

get_header(); ?>

<div id="south-africa-vps">

    <div class="landing-header tablet-pad">
        <h1 class="page-title">VPS South Africa</h1>
        <h2 class="page-subtitle">Lightning-fast and highly customizable VPS hosting in South Africa</h2>

        <div class="choose-location">

            <div class="row">
                <div class="cell">
                    <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/za-flag-77.png" alt="" width="76px"/>
                </div>
                <div class="cell">
                    <div class="loc-name">
                        Johannesburg, South Africa
                    </div>
                    <div class="price">
                        from <strong><?php echo $vps_hosting_min_price ?></strong> / month
                    </div>
                </div>
                <div class="cell" style="text-align:right;">
                    <a href="<?php echo $config['whmcs_links']['checkout_vps']?>?location=africa&plan=amber" class="button big orange">View Plans</a>
                </div>
            </div>
        </div>
        <div class="notice compare" onclick="$('html, body').animate({scrollTop: $('#vps-compare').offset().top}, 2000);">
            Compare our offer with other popular VPS South Africa providers
        </div>

    </div>


    <div  class="features-boxes container">
        <h2 class="title">VPS South Africa overview</h2>

        <div class="feature-box">
            <div class="fico">
                <i class="ico high-speed"></i>
            </div>
            <div class="ftitle">High Network Speed</div>
            <div class="fcontent">
                Enjoy fast connection speed and low latency in African region. Our professional network administrators make sure your connection speed is always as high as possible by thoroughly detecting any network anomaly.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico hardware"></i>
            </div>
            <div class="ftitle">Enterprise Hardware</div>
            <div class="fcontent">
                Place your South Africa VPS on the next-level hardware with hot-swappable disks to preserve stability, multiple Internet lines and blistering connectivity.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico dns"></i>
            </div>
            <div class="ftitle">DNS Management</div>
            <div class="fcontent">
                A hassle-free solution for your domain management – create and edit your domain zones with no extra support.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico cpu"></i>
            </div>
            <div class="ftitle">Intel Xeon Processors</div>
            <div class="fcontent">
                Speedy and sustainable processors with higher core counts can handle any workload – there is no speed deceleration.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico ddos"></i>
            </div>
            <div class="ftitle">Full DDoS Protection</div>
            <div class="fcontent">
                Get rid of unexpected interruptions! Our DDoS protection covers up to 40 Gbit/s of a single attack free of charge.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico response"></i>
            </div>
            <div class="ftitle">Smart Response Technology</div>
            <div class="fcontent">
                Intel® SRT recognizes and collects your mostly used data, stores it into built-in SSD drives, which accelerates the whole data usage process.
            </div>
        </div>
    </div>

    <div class="view-plans">
        <div class="container">
            <div class="row">
                <div class="cell title">
                    Johannesburg, South Africa
                </div>
                <div class="cell description">
                    Over 15,500 square feet data center is characterized as the largest provider of hosting services in Africa. A high-speed fibre network, bulk power supply and sustainable IT solutions ensure that your server is running with unmatched connectivity, high uptime standards and continuous power supply.
                </div>
                <div class="cell button-wrap">
                    <a href="<?php echo $config['whmcs_links']['checkout_vps']?>?location=africa&plan=amber" class="button orange-border">VIEW PLANS</a>
                </div>
            </div>
        </div>
    </div>

        <?php include __DIR__ . '/../htmlblocks/reviews_2.php'; ?>

    <div class="locations-wrapper tablet-pad">
        <div class="title">Looking for something else?</div>
        <div class="subtitle">Point your server to any other destination. The closer you get – the lower latency and higher network speed you receive</div>

        <div class="choose-location">
            <div class="locations-list">

                <?php
                    $plan = 'amber';
                    $planConfig = $whmcs->get_local_stepsconfig('vps_plans')[ $plan ];
                    $locations = $config['products']['vps_hosting']['locations'];

                    $first_loc_override_key = 'chicago';
                    if( array_key_exists( $first_loc_override_key, $locations) ){
                        $first_loc_override = $locations[$first_loc_override_key]; //which location should be first
                        unset( $locations[$first_loc_override_key] );
                        $locations = [$first_loc_override_key => $first_loc_override] + $locations;
                    }

                    foreach($locations as $location_key => $location):
                        if( in_array( $location_key, [ 'johannesburg' ] ) ) continue;

                        $prices = $whmcs->getConfigurablePrice($location['id'], $planConfig['configs']);
                 ?>

                    <div class="location" onclick="$(this).find('form').submit();">
                        <form class="" action="<?php echo $config['whmcs_links']['checkout_vps']?>" method="get">

                            <input type="hidden" name="plan" value="<?php echo $plan;?>"/>
                            <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                            <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                            <input type="hidden" name="promocode" value="<?php echo $whmcs_promo;?>"/>

                            <div class="label">
                                <?php
                                switch ($location_key){
                                    case 'los_angeles':
                                        echo __('Los Angeles');
                                        break;
                                    case 'chicago':
                                        echo __('Chicago');
                                        break;
                                    case 'sao_paulo':
                                        echo __('São Paulo');
                                        break;
                                    case 'frankfurt':
                                        echo __('Frankfurt');
                                        break;
                                    case 'johannesburg':
                                        echo __('Johannesburg');
                                        break;
                                }
                                ?>
                            </div>
                            <div><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/<?php echo strtoupper( $location['country_code'] ) ?>.png" alt="" width="80px"/></div>
                            <div class="price-notice">from <span class="price"><?php echo $prices[0]['price']; ?></span> / month</div>
                        </form>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>

    </div>


    <div class="what-else">
        <h2 class="title">What else is there for you?</h2>

        <div class="block-tabs">
            <div class="tabs-wrapper">
                <div class="app-tabs">
                    <div class="table">
                        <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active">MONEY-BACK GUARANTEE</span></div>
                        <div class="cell"><span data-tab-link="feature.multilingual" class="tab desktop">MULTILINGUAL TECH SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.hidden-fees" class="tab desktop">NO HIDDEN FEES</span></div>
                        <div class="cell"><span data-tab-link="feature.responsive" class="tab desktop">RESPONSIVE CLIENT AREA</span></div>
                        <div class="cell"><span data-tab-link="feature.management" class="tab desktop">EASY SERVER MANAGEMENT</span></div>
                    </div>
                </div>
                <div class="tabs" data-tabs="feature">

                    <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">MONEY-BACK GUARANTEE</span>
                    <div class="tab-content active" data-tab-id="money-back">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.jpg">
                        <span class="title">MONEY-BACK GUARANTEE</span>
                        <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
                    </div>

                    <span data-tab-link="feature.multilingual" data-tab-toggle="self" class="tab mob">MULTILINGUAL TECH SUPPORT</span>
                    <div class="tab-content" data-tab-id="multilingual">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/multilingual.jpg">
                        <span class="title">MULTILINGUAL TECH SUPPORT</span>
                        <p>Our experienced in-house tech support is capable of solving your issues in seconds! Just give us a short notice through our ticket system in your Client Area or directly at <a href="mailto:support@host1plus.com">support@host1plus.com</a>.</p>
                    </div>

                    <span data-tab-link="feature.hidden-fees" data-tab-toggle="self" class="tab mob">NO HIDDEN FEES</span>
                    <div class="tab-content" data-tab-id="hidden-fees">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/hidden-fees.jpg">
                        <span class="title">NO HIDDEN FEES</span>
                        <p>Fair prices guaranteed! Enjoy your hosting services with no extra costs.</p>
                    </div>

                    <span data-tab-link="feature.responsive" data-tab-toggle="self" class="tab mob">RESPONSIVE CLIENT AREA</span>
                    <div class="tab-content" data-tab-id="responsive">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/responsive.jpg">
                        <span class="title">RESPONSIVE CLIENT AREA</span>
                        <p>Always on the go? Easily manage your services using any mobile device.</p>
                    </div>

                    <span data-tab-link="feature.management" data-tab-toggle="self" class="tab mob">EASY SERVER MANAGEMENT</span>
                    <div class="tab-content" data-tab-id="management">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/management-cpanel.jpg">
                        <span class="title">EASY SERVER MANAGEMENT</span>
                        <p>Enjoy one of the most popular and well-trusted control panels on the market! Sign up and get a full license for only $12.00 per month.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div id="vps-compare" class="compare-block">
        <div class="container">
            <h2 class="title">Compare our offer to other hosting market players in Africa</h2>

            <div class="table-wrap">

                <table>
                    <tbody>
                        <tr>
                            <td class="left date">
                                Data taken at 21/08/2015
                            </td>
                            <td class="h1p logo">
                                <div class="pic host1plus"></div>
                                <div class="title">Host1Plus</div>
                            </td>
                            <td class="value logo">
                                <div class="pic webafrica"></div>
                                <div class="title">Webafrica</div>
                            </td>
                            <td class="value logo">
                                <div class="pic elitehost"></div>
                                <div class="title">Elitehost</div>
                            </td>
                            <td class="value logo">
                                <div class="pic afrihost"></div>
                                <div class="title">Afrihost</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                CPU
                            </td>
                            <td class="value h1p">
                                2 Core
                            </td>
                            <td class="value">
                                1 Core
                            </td>
                            <td class="value">
                                1 Core
                            </td>
                            <td class="value">
                                1 Core
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                RAM
                            </td>
                            <td class="value h1p">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Disk Space
                            </td>
                            <td class="value h1p">
                                80 GB
                            </td>
                            <td class="value">
                                50 GB
                            </td>
                            <td class="value">
                                80 GB
                            </td>
                            <td class="value">
                                100 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Bandwidth
                            </td>
                            <td class="value h1p">
                                2000 GB
                            </td>
                            <td class="value">
                                400 GB
                            </td>
                            <td class="value">
                                Unlimited
                            </td>
                            <td class="value">
                                100 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Price
                            </td>
                            <td class="value h1p">
                                $19.00
                            </td>
                            <td class="value">
                                $23.02
                            </td>
                            <td class="value">
                                $61.60
                            </td>
                            <td class="value">
                                $22.33
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                OS
                            </td>
                            <td class="value h1p tsmaller">
                                Ubuntu, Fedora, Debian, CentOS, Suse
                            </td>
                            <td class="value tsmaller">
                                CentOS, Ubuntu
                            </td>
                            <td class="value tsmaller">
                                Ubuntu, Fedora, Debian, CentOS, Suse, Slackware
                            </td>
                            <td class="value tsmaller">
                                CentOS, Ubuntu, Microsoft Server 2008, Microsoft Server 2012, Microsoft Server Enterprise, Microsoft Server DataCenter
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                DDoS Protection
                            </td>
                            <td class="value h1p">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                DNS Management
                            </td>
                            <td class="value h1p">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                South Africa Data Center Locations
                            </td>
                            <td class="value h1p tsmaller">
                                Johannesburg
                            </td>
                            <td class="value tsmaller">
                                Cape Town
                            </td>
                            <td class="value tsmaller">
                                Johannesburg
                            </td>
                            <td class="value tsmaller">
                                Johannesburg
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="compare-notice">
                    *Third-party logos and marks are registered trademarks of their respective owners. All rights reserved.
                </div>

            </div>
        </div>

    </div>

    <div class="faq2">
        <h2 class="title">Ask questions, get answers!</h2>
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq">FAQ</h3>
            <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    Which control panels can I install on my VPS?
                    <div class="answer">
                        You can install open source control panels or any other web server management panel by yourself such as cPanel, ISPconfig, Plesk, Ajenti, Kloxo, OpenPanel, ZPanel, EHCP, ispCP, VHCS, RavenCore, Virtualmin, Webmin, DTC, DirectAdmin, InterWorx, SysCP, BlueOnyx and more.
                    </div>
                </div>
                <div class="question">
                    What virtualization do you use for VPS hosting?
                    <div class="answer">
                        We are currently using OpenVZ virtualization.
                    </div>
                </div>
                <div class="question">
                    What payment methods do you provide?
                    <div class="answer">
                        Our accepted payment methods include credit cards, Paypal, WebMoney, Alipay, Skrill, Ebanx, PaySera, CashU and Bitcoin. However, depending on the country you come from, some methods might be restricted.<br>
                        If you have trouble accessing your chosen billing method from your account, please open a ticket at <a target="_blank" href="mailto:support@host1plus.com">support@host1plus.com</a> - we're always happy to help!
                    </div>
                </div>
                <div class="question">
                    What happens when my CPU limit is reached?
                    <div class="answer">
                        If you are overusing your CPU you will receive a notification that the power of your processor is temporarily limited.<br>
                        You can review your CPU power usage on the statistics page in your Client Area (for more information <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1244/108/where-can-i-find-my-vps-statistics-page">click here</a>).<br>
                        If you want to avoid reaching the limit, you can upgrade your CPU in your Client Area (for more information visit <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1241/108/how-to-upgradedowngrade-my-vps"></a>click here</a>).
                    </div>
                </div>
                <div class="question">
                    How to reinstall an operating system (OS) on my VPS?
                    <div class="answer">
                        To reinstall an OS on your VPS, log in to your Client Area and click Services > VPS Hosting. <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1240/108/how-to-reinstall-an-operating-system-os-on-my-vps">Read more</a>.
                    </div>
                </div>
                <div class="question">
                    How to purchase extra-care service for my VPS?
                    <div class="answer">
                        Log in to your Client Area and click on Services > VPS Hosting. <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1236/108/how-to-purchase-extra-care-service-for-my-vps"></a>Read more</a>.
                    </div>
                </div>
            </div>
            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/how-to-create-virtualhost-on-apache"><i class="fa fa-file-text-o file-ico"></i> How to create virtualhost on Apache?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-csf-on-linux"><i class="fa fa-file-text-o file-ico"></i> How to install CSF on Linux?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/introduction-to-ssh-keys"><i class="fa fa-file-text-o file-ico"></i> Introduction to SSH keys</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-gnome-desktop"><i class="fa fa-file-text-o file-ico"></i> How to install GNOME desktop?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-change-a-time-zone-on-a-vps-server-2"><i class="fa fa-file-text-o file-ico"></i> How to change a time zone on a VPS server?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-add-ip-address-on-linux-operating-systems"><i class="fa fa-file-text-o file-ico"></i> How to add IP address on CentOS operating system?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="back-to-top">
        <div class="back-box">
            <table>
            <tbody>
            <tr>
                <td><div class="main-text">Take a look at South Africa VPS hosting plans</div></td>
                <td><a href="<?php echo $config['whmcs_links']['checkout_vps']?>?location=africa&plan=amber" class="button">View Plans</a></td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>

    <div class="prefooter">
        <h3 class="sub-title" style="font-size:36px;line-height: 40px;">If you need less resources for your website or you wish to start  reseller business, check out other South Africa hosting solutions</h3>
        <a href="<?php echo $config['links']['web-hosting']?>" class="button" style="">Web hosting in South Africa</a>
        <a href="<?php echo $config['links']['reseller-hosting']?>" class="button" style="margin-left:0px;">Reseller hosting in South Africa</a>
    </div>

</div>

<?php get_footer(); ?>