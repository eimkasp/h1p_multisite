(function($) {

    /* vps loc changing */
    $(document).ready(function(){
        $('.vps-locations .flags .flag').click(function(){
            var main_container = $(this).closest('.vps-locations');
            var new_loc = $(this).attr('data-loc');

            main_container.attr('data-selected-loc', new_loc);

            main_container.find('.flags .flag').removeClass('active');
            main_container.find('.flags .flag.'+ new_loc +'').addClass('active');

            main_container.find('.location-description').removeClass('active');
            main_container.find('.location-description.'+ new_loc +'').addClass('active');
        });
    });

    /* tabs handling */
    $(document).ready(function(){
        $('[data-tab-index]').click(function(){
            var tab_index = $(this).attr('data-tab-index');

            $('[data-tab-index]').removeClass('active');
            $(this).addClass('active');

            $('[data-tab-content]').removeClass('active');
            $('[data-tab-content="'+ tab_index +'"]').addClass('active');
        });

        $('.question:not(.active)').click(function(){
            $('.question').removeClass('active');
            $(this).addClass('active');
        });
    });

})(jQuery);