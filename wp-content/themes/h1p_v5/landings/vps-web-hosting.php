<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: VPS/WEB hosting
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);


wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

get_header();

    global $whmcs;
    global $config;

    $web_hosting_min_price = $whmcs->getMinPrice($config['products']['web_hosting']['plans'][1]['id']);
    $vps_hosting_min_price = $whmcs->getMinPrice($config['products']['vps_hosting']['locations']['chicago']['id']);
    $cpanel_min_price = $whmcs->getMinPrice( 129 );
?>

<div id="vps-web-hosting">

    <div class="landing-header tablet-pad">
        <h1 class="page-title">Virtual Private Server Hosting</h1>
        <h2 class="page-subtitle">Virtual server hosting plans starting from <?php echo $vps_hosting_min_price ?></h2>

        <div class="choose-location">

            <div class="title">Select your server location</div>
            <div><span class="arrow-down"></span></div>
            <div class="locations-list">

                <?php
                    $plan = 'amber';
                    $planConfig = $whmcs->get_local_stepsconfig('vps_plans')[ $plan ];
                    $locations = $config['products']['vps_hosting']['locations'];

                    $first_loc_override_key = 'chicago';
                    if( array_key_exists( $first_loc_override_key, $locations) ){
                        $first_loc_override = $locations[$first_loc_override_key]; //which location should be first
                        unset( $locations[$first_loc_override_key] );
                        $locations = [$first_loc_override_key => $first_loc_override] + $locations;
                    }

                    foreach($locations as $location_key => $location):

                        $prices = $whmcs->getConfigurablePrice($location['id'], $planConfig['configs']);
                 ?>

                    <div class="location" onclick="$(this).find('form').submit();">
                        <form class="" action="<?php echo $config['whmcs_links']['checkout_vps']?>" method="get">

                            <input type="hidden" name="plan" value="<?php echo $plan;?>"/>
                            <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                            <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                            <input type="hidden" name="promocode" value="<?php echo $whmcs_promo;?>"/>
                            <input type="hidden" name="cstep" value="vps-package"/>
                            <input type="hidden" name="location" value="<?php echo $location['key'] ?>"/>

                            <div class="label">
                                <?php
                                switch ($location_key){
                                    case 'los_angeles':
                                        echo __('Los Angeles');
                                        break;
                                    case 'chicago':
                                        echo __('Chicago');
                                        break;
                                    case 'sao_paulo':
                                        echo __('São Paulo');
                                        break;
                                    case 'frankfurt':
                                        echo __('Frankfurt');
                                        break;
                                    case 'johannesburg':
                                        echo __('Johannesburg');
                                        break;
                                }
                                ?>
                            </div>
                            <div><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/<?php echo strtoupper( $location['country_code'] ) ?>.png" alt="" width="80px"/></div>
                            <div class="price-notice">from <span class="price"><?php echo $prices[0]['price']; ?></span> / month</div>
                        </form>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>

    </div>

    <div class="landing-notice what-is-vps">
        <div class="container">
        <table>
        <tbody>
            <tr>
                <td rowspan=2 class="excl-mob">
                    <div class="ico vps-server"></div>
                </td>
                <td class="title">
                    What is a virtual private server?
                </td>
            </tr>
            <tr>
                <td class="description">
                    Virtual private server (VPS) is a virtual environment created on a physical computer, providing control and privacy measures of a dedicated server. VPS is created by dividing a physical machine into multiple virtual environments with separate operating systems and can be rebooted and controlled by different users independently.
                </td>
            </tr>
        </tbody>
        </table>
        </div>
    </div>

    <div class="features-boxes container">
        <h2 class="title">
            What can you use it for?
        </h2>
        <div class="feature-box">
            <div class="fico">
                <i class="ico reliable"></i>
            </div>
            <div class="ftitle">A reliable medium for applications</div>
            <div class="fcontent">
                Develop, test and run your apps on our virtual private server with no concerns! Ease your service management with cPanel or any other user-friendly interface.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico blogging"></i>
            </div>
            <div class="ftitle">Blogging, ecommerce and more</div>
            <div class="fcontent">
                Worried about your traffic? Virtual server hosting supports dynamic websites, high-traffic blogs and demanding online businesses.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico running"></i>
            </div>
            <div class="ftitle">Running resource intensive scripts</div>
            <div class="fcontent">
                Create your scripts in a reliable virtual environment with scalable resources and less limits. Enjoy multiple domains, databases and endless entry processes!
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico ftp"></i>
            </div>
            <div class="ftitle">Create an FTP server</div>
            <div class="fcontent">
                Accelerate your file sharing processes. As a virtual host we ensure you receive excellent connection and superior speed.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico gaming"></i>
            </div>
            <div class="ftitle">Don’t stop gaming</div>
            <div class="fcontent">
                Choose our virtual server location closest to you and enjoy low latency, instant activation and DDoS protection for a ridiculously low price.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico backup-data"></i>
            </div>
            <div class="ftitle">Backup your data</div>
            <div class="fcontent">
                Restore your files, databases and hard drives on your network with no stress! Our automated backups will increase your data protection to the maximum!
            </div>
        </div>
    </div>

    <div class="services-compare container">
        <h2 class="title">
            Web Hosting vs. VPS
        </h2>
        <div class="table-wrap">
            <table>
                <tbody>
                    <tr>
                        <th class="darkbg">
                            FEATURES
                        </th>
                        <th class="darkerbg">
                            WEB HOSTING
                            <div class="price-block">
                                <div class="wrapper">
                                    <span class="main-price">
                                        <div class="from">From</div><?php echo $web_hosting_min_price ?></span> / mo
                                </div>
                            </div>
                            <div class="button-wrap">
                                <a href="<?php echo $config['links']['web-hosting']?>" class="button semirounded">VIEW PLANS</a>
                            </div>
                        </th>
                        <th class="darkestbg">
                            VPS HOSTING
                            <div class="price-block">
                                <div class="wrapper">
                                    <span class="main-price">
                                        <div class="from">From</div><?php echo $vps_hosting_min_price ?></span> / mo
                                </div>
                            </div>
                            <div class="button-wrap">
                                <a href="<?php echo $config['whmcs_links']['checkout_vps']?>?cstep=vps-package&location=us_east&plan=amber" class="button semirounded">VIEW PLANS</a>
                            </div>
                        </th>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Full root access</div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-times"></i></div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-check"></i></div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">DNS management</div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-check"></i></div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-check"></i></div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">DDoS protection</div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-times"></i></div>
                        </td>
                        <td class="center">
                            <div class="box"><i class="fa fa-check"></i></div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Disk space</div>
                        </td>
                        <td class="center">
                            <div class="box">From 1 GB</div>
                        </td>
                        <td class="center">
                            <div class="box">From 20 GB</div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Bandwidth</div>
                        </td>
                        <td class="center">
                            <div class="box">From 50 GB</div>
                        </td>
                        <td class="center">
                            <div class="box">From 500 GB</div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Entry processes</div>
                        </td>
                        <td class="center">
                            <div class="box">From 15</div>
                        </td>
                        <td class="center">
                            <div class="box">Unlimited</div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Backups</div>
                        </td>
                        <td class="center">
                            <div class="box">Manual backups</div>
                        </td>
                        <td class="center">
                            <div class="box">Automated backups (for additional price)</div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Locations</div>
                        </td>
                        <td class="">
                            <div class="box">
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/BR.png" alt="BR" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        São Paulo, Brazil
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/DE.png" alt="DE" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Frankfurt, Germany
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/LT.png" alt="LT" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Šiauliai, Lithuania
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/ZA.png" alt="ZA" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Johannesburg, South Africa
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/NL.png" alt="NL" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Amsterdam, The Netherlands
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/GB.png" alt="GB" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        London, United Kingdom
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/US.png" alt="US" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Chicago, USA
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/US.png" alt="US" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Los Angeles, USA
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="">
                            <div class="box">
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/BR.png" alt="BR" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        São Paulo, Brazil
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/DE.png" alt="DE" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Frankfurt, Germany
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/ZA.png" alt="ZA" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Johannesburg, South Africa
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/US.png" alt="US" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Chicago, USA
                                    </div>
                                </div>
                                <div class="flag-row">
                                    <div class="flag-col">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/32/US.png" alt="US" width="30" />
                                    </div>
                                    <div class="flag-col">
                                        Los Angeles, USA
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linebnone">
                        <td class="feature-name darkbg">
                            <div class="box">Available operating systems</div>
                        </td>
                        <td class="center">
                            <div class="box">CloudLinux</div>
                        </td>
                        <td class="center">
                            <div class="box">Ubuntu, Fedora, Debian, CentOS, Suse</div>
                        </td>
                    </tr>
                    <tr class="linebnone separator-line">
                        <td class="darkbg">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                        <td class="center">
                            <div class="separator underline"></div>
                        </td>
                    </tr>
                    <tr class="linetopbnone">
                        <td class="feature-name darkbg">
                            <div class="box">Control panel</div>
                        </td>
                        <td class="center">
                            <div class="box">DirectAdmin or cPanel (free)</div>
                        </td>
                        <td class="center">
                            <div class="box">cPanel license ($12 / mo)</div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <?php include __DIR__ . '/block-reviews.php'; ?>

    <div class="save-time">
        <div class="container">
        <table>
        <tbody>
            <tr>
                <td rowspan=2 class="excl-mob">
                    <div class="ico save-time-ico"></div>
                </td>
                <td class="title">
                    Want to save your precious time?
                </td>
            </tr>
            <tr>
                <td class="description">
                    Enjoy easier server management with one of the most popular and well-trusted control panels on the market! Sign up and get a full license for only <?php echo $cpanel_min_price; ?> per month.
                </td>
            </tr>
        </tbody>
        </table>
        </div>
    </div>

    <div class="what-else">
        <h2 class="title">What else do we offer?</h2>

        <div class="block-tabs">
            <div class="tabs-wrapper">
                <div class="app-tabs">
                    <div class="table">
                    <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active">14-DAY MONEY-BACK GUARANTEE</span></div>
                        <div class="cell"><span data-tab-link="feature.support" class="tab desktop">EXTRA-CARE SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.responsive" class="tab desktop">RESPONSIVE CLIENT AREA</span></div>
                        <div class="cell"><span data-tab-link="feature.productivity" class="tab desktop">SUPERIOR PRODUCTIVITY</span></div>
                        <div class="cell"><span data-tab-link="feature.multilingual" class="tab desktop">MULTILINGUAL TECH SUPPORT</span></div>
                    </div>
                </div>
                <div class="tabs" data-tabs="feature">

                    <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">14-DAY MONEY-BACK GUARANTEE</span>
                    <div class="tab-content active" data-tab-id="money-back">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.jpg">
                        <span class="title">14-DAY MONEY-BACK GUARANTEE</span>
                        <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
                    </div>

                    <span data-tab-link="feature.support" data-tab-toggle="self" class="tab mob">EXTRA-CARE SUPPORT</span>
                    <div class="tab-content" data-tab-id="support">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/support.jpg">
                        <span class="title">Extra-Care Support</span>
                        <p>Worried about managing your service? You can order our extra-care support that will cover it all - server monitoring, management, network issue and performance resolution and much more.</p>
                    </div>

                    <span data-tab-link="feature.responsive" data-tab-toggle="self" class="tab mob">RESPONSIVE CLIENT AREA</span>
                    <div class="tab-content" data-tab-id="responsive">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/responsive.jpg">
                        <span class="title">Responsive Client Area</span>
                        <p>Always on the go? Easily manage your services using any mobile device.</p>
                    </div>

                    <span data-tab-link="feature.productivity" data-tab-toggle="self" class="tab mob">SUPERIOR PRODUCTIVITY</span>
                    <div class="tab-content" data-tab-id="productivity">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/pruductiivity-2.jpg">
                        <span class="title">Superior Productivity</span>
                        <p>Track your website, traffic and bandwidth usage statistics to increase your overall performance.</p>
                    </div>

                    <span data-tab-link="feature.multilingual" data-tab-toggle="self" class="tab mob">MULTILINGUAL TECH SUPPORT</span>
                    <div class="tab-content" data-tab-id="multilingual">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/multilingual.jpg">
                        <span class="title">MULTILINGUAL TECH SUPPORT</span>
                        <p>No more language barriers! We speak English, Lithuanian, Spanish and Portuguese.</p>
                    </div>

                </div>
            </div>
        </div>

    <div class="faq2">
        <h2 class="title">Ask questions, get answers!</h2>
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq">FAQ</h3>
            <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    How long will my VPS setup take?
                    <div class="answer">
                        After you order a VPS, the service is usually set up instantly. However, if you pay using a credit card, it might take up to a few hours.
                    </div>
                </div>
                <div class="question">
                    What virtualization do you use for VPS hosting?
                    <div class="answer">
                        We are currently using OpenVZ virtualization.
                    </div>
                </div>
                <div class="question">
                    Which control panels can I install on my VPS?
                    <div class="answer">
                        You can install open source control panels or any other web server management panel by yourself such as cPanel, ISPconfig, Plesk, Ajenti, Kloxo, OpenPanel, ZPanel, EHCP, ispCP, VHCS, RavenCore, Virtualmin, Webmin, DTC, DirectAdmin, InterWorx, SysCP, BlueOnyx and more.
                    </div>
                </div>
                <div class="question">
                    How to upgrade my VPS?
                    <div class="answer">
                        Log in to your Client Area and click Services > VPS Hosting. <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1241/108/how-to-upgradedowngrade-my-vps">Read more</a>.
                    </div>
                </div>
                <div class="question">
                    How to purchase extra-care service for my VPS?
                    <div class="answer">
                        Log in to your Client Area and click on Services > VPS Hosting. <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1236/108/how-to-purchase-extra-care-service-for-my-vps">Read more</a>.
                    </div>
                </div>
                <div class="question">
                    How do I create a virtual console on my VPS?
                    <div class="answer">
                        First, log in to your Client Area and click Services > VPS Hosting.
                    </div>
                </div>
            </div>
            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/all-vps-hosting-tutorials/change-time-vps"><i class="fa fa-file-text-o file-ico"></i> How to change time in your vps</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/common-log-files-on-a-linux-system"><i class="fa fa-file-text-o file-ico"></i> Common log files on a Linux system</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/shells-linux/shell-variables"><i class="fa fa-file-text-o file-ico"></i> Shell Variables</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/coding-scripting/ssh-bash/beginner-ssh-bash/monitoring-server-over-ssh"><i class="fa fa-file-text-o file-ico"></i> Monitoring Server Over SSH</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/shells-linux/what-is-shell-scripting"><i class="fa fa-file-text-o file-ico"></i> What is Shell Scripting?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/coding-scripting/apache/beginner-apache/top-10-apache-modules"><i class="fa fa-file-text-o file-ico"></i> Top 10 Apache Modules</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/htaccess-basics"><i class="fa fa-file-text-o file-ico"></i> .htaccess basics</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/security-linux/htaccess-configuration"><i class="fa fa-file-text-o file-ico"></i> .htaccess Configuration</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="back-to-top">
        <div class="back-box">
            <table>
            <tbody>
            <tr>
                <td><div class="main-text">Take a look at our virtual private hosting plans</div></td>
                <td><a href="<?php echo $config['whmcs_links']['checkout_vps']?>?cstep=vps-package&plan=amber" class="button">Learn More</a></td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>

    <div class="prefooter">
        <h3 class="title">Need something else?</h3>
        <p>
            Check out our other hosting solutions
        </p>
        <a href="<?php echo $config['links']['web-hosting']?>" class="button orange">VIEW WEB HOSTING PLANS</a> <a href="<?php echo $config['links']['reseller-hosting'] ?>" class="button orange">VIEW RESELLER HOSTING PLANS</a>
    </div>

</div>

<?php get_footer(); ?>