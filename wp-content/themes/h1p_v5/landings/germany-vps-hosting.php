<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: Germany VPS Hosting (old)
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-v2.style', get_template_directory_uri() . '/landings/css/landing-v2.css', ['landings.style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);


wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

$vps_hosting_min_price = $whmcs->getMinPrice($config['products']['vps_hosting']['locations']['frankfurt']['id']);

get_header(); ?>

<div id="germany-vps">

    <div class="landing-header tablet-pad">
        <h1 class="page-title">VPS hosting in Germany</h1>
        <h2 class="page-subtitle">High performance VPS Germany services with unbeatable uptime and flexibility</h2>

        <div class="choose-location">

            <div class="row">
                <div class="cell">
                    <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/DE.png" alt="" width="76px"/>
                </div>
                <div class="cell">
                    <div class="loc-name">
                        Frankfurt, Germany
                    </div>
                    <div class="price">
                        from <strong><?php echo $vps_hosting_min_price ?></strong> / month
                    </div>
                </div>
                <div class="cell" style="text-align:right;">
                    <a href="<?php echo $config['whmcs_links']['checkout_vps']?>?plan=amber&location=germany" class="button big orange">View Plans</a>
                </div>
            </div>
        </div>
        <div class="notice compare" onclick="$('html, body').animate({scrollTop: $('#vps-compare').offset().top}, 2000);">
            Compare our offer with other popular Germany VPS providers
        </div>

    </div>


    <div  class="features-boxes container">
        <h2 class="title">Our Germany VPS hosting features</h2>

        <div class="feature-box">
            <div class="fico">
                <i class="ico hardware"></i>
            </div>
            <div class="ftitle">Enterprise Hardware</div>
            <div class="fcontent">
                Place your German VPS on the next-level hardware with hot-swappable disks to preserve stability, multiple Internet lines and blistering connectivity.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico high-speed"></i>
            </div>
            <div class="ftitle">High Network Speed</div>
            <div class="fcontent">
                Benefit from high network speed, low latency connection to Germany and Europe, ensured by our professional network administrators.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico power"></i>
            </div>
            <div class="ftitle">Two Power lines</div>
            <div class="fcontent">
                High level redundancy is ensured as power lines are interchanged when one is cut off. Power cuts or other electrical failures are not a big deal anymore.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico cpu"></i>
            </div>
            <div class="ftitle">Intel Xeon Processors</div>
            <div class="fcontent">
                Enhance your server performance with enterprise Xeon processors. Improved service quality with server-grade CPUs provided.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico ssd"></i>
            </div>
            <div class="ftitle">SSD Caching</div>
            <div class="fcontent">
                When caching and pre-loading your commonly used data into the SSD drive, SSD caching speeds up the access to your data and ensures rapid loading and boot times.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico ram"></i>
            </div>
            <div class="ftitle">Error-Correcting Code Memory</div>
            <div class="fcontent">
                Say goodbye to unexpected interruptions! If there’s an error within your data – it will be corrected on the fly. Automated correcting system solves the issues without any server disturbance.
            </div>
        </div>
    </div>

    <div class="view-plans">
        <div class="container">
            <div class="row">
                <div class="cell title">
                    Frankfurt, Germany
                </div>
                <div class="cell description">
                    Built on a powerful infrastructure platform in Frankfurt, Germany, this data center brings top security by using powerful diesel generators and advanced cooling systems. The latest equipment and technology ensures exceptional service with unmatched connectivity and high uptime standards.
                </div>
                <div class="cell button-wrap">
                    <a href="<?php echo $config['whmcs_links']['checkout_vps']?>?plan=amber&location=germany" class="button orange-border">VIEW PLANS</a>
                </div>
            </div>
        </div>
    </div>

        <?php include __DIR__ . '/../htmlblocks/reviews_2.php'; ?>

    <div class="locations-wrapper tablet-pad">
        <div class="title">Looking for something else?</div>
        <div class="subtitle">Point your server to any other destination. The closer you get – the lower latency and higher network speed you receive</div>

        <div class="choose-location">
            <div class="locations-list">

                <?php
                    $plan = 'amber';
                    $planConfig = $whmcs->get_local_stepsconfig('vps_plans')[ $plan ];
                    $locations = $config['products']['vps_hosting']['locations'];

                    $first_loc_override_key = 'chicago';
                    if( array_key_exists( $first_loc_override_key, $locations) ){
                        $first_loc_override = $locations[$first_loc_override_key]; //which location should be first
                        unset( $locations[$first_loc_override_key] );
                        $locations = [$first_loc_override_key => $first_loc_override] + $locations;
                    }

                    foreach($locations as $location_key => $location):
                        if( in_array( $location_key, [ 'frankfurt' ] ) ) continue;

                        $prices = $whmcs->getConfigurablePrice($location['id'], $planConfig['configs']);
                 ?>

                    <div class="location" onclick="$(this).find('form').submit();">
                        <form class="" action="<?php echo $config['whmcs_links']['checkout_vps']?>" method="get">

                            <input type="hidden" name="plan" value="<?php echo $plan;?>"/>
                            <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                            <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                            <input type="hidden" name="location" value="<?php echo $location['key'] ?>"/>
                            <input type="hidden" name="promocode" value="<?php echo $whmcs_promo;?>"/>

                            <div class="label">
                                <?php
                                switch ($location_key){
                                    case 'los_angeles':
                                        echo __('Los Angeles');
                                        break;
                                    case 'chicago':
                                        echo __('Chicago');
                                        break;
                                    case 'sao_paulo':
                                        echo __('São Paulo');
                                        break;
                                    case 'frankfurt':
                                        echo __('Frankfurt');
                                        break;
                                    case 'johannesburg':
                                        echo __('Johannesburg');
                                        break;
                                }
                                ?>
                            </div>
                            <div><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/<?php echo strtoupper( $location['country_code'] ) ?>.png" alt="" width="80px"/></div>
                            <div class="price-notice">from <span class="price"><?php echo $prices[0]['price']; ?></span> / month</div>
                        </form>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>

    </div>


    <div class="what-else">
        <h2 class="title">What else is there for you?</h2>

        <div class="block-tabs">
            <div class="tabs-wrapper">
                <div class="app-tabs">
                    <div class="table">
                        <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active">MONEY-BACK GUARANTEE</span></div>
                        <div class="cell"><span data-tab-link="feature.multilingual" class="tab desktop">MULTILINGUAL TECH SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.hidden-fees" class="tab desktop">NO HIDDEN FEES</span></div>
                        <div class="cell"><span data-tab-link="feature.responsive" class="tab desktop">RESPONSIVE CLIENT AREA</span></div>
                        <div class="cell"><span data-tab-link="feature.savings" class="tab desktop">MAJOR SAVINGS</span></div>
                        <div class="cell"><span data-tab-link="feature.support" class="tab desktop">EXTRA-CARE SUPPORT</span></div>
                    </div>
                </div>
                <div class="tabs" data-tabs="feature">

                    <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">MONEY-BACK GUARANTEE</span>
                    <div class="tab-content active" data-tab-id="money-back">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.jpg">
                        <span class="title">Money-Back Guarantee</span>
                        <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
                    </div>

                    <span data-tab-link="feature.multilingual" data-tab-toggle="self" class="tab mob">MULTILINGUAL TECH SUPPORT</span>
                    <div class="tab-content" data-tab-id="multilingual">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/multilingual.jpg">
                        <span class="title">Multilingual Tech Support</span>
                        <p>No more language barriers! We speak English, Lithuanian, Spanish and Portuguese.</p>
                    </div>

                    <span data-tab-link="feature.hidden-fees" data-tab-toggle="self" class="tab mob">NO HIDDEN FEES</span>
                    <div class="tab-content" data-tab-id="hidden-fees">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/hidden-fees.jpg">
                        <span class="title">No Hidden Fees</span>
                        <p>Fair prices guaranteed! Enjoy your hosting services with no extra costs.</p>
                    </div>

                    <span data-tab-link="feature.responsive" data-tab-toggle="self" class="tab mob">RESPONSIVE CLIENT AREA</span>
                    <div class="tab-content" data-tab-id="responsive">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/responsive.jpg">
                        <span class="title">Responsive Client Area</span>
                        <p>Always on the go? Easily manage your services using any mobile device.</p>
                    </div>

                    <span data-tab-link="feature.savings" data-tab-toggle="self" class="tab mob">MAJOR SAVINGS</span>
                    <div class="tab-content" data-tab-id="savings">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/savings.jpg">
                        <span class="title">Major Savings</span>
                        <p>Sign up for an extended billing cycle and save up to 11% for your purchase!</p>
                    </div>

                    <span data-tab-link="feature.support" data-tab-toggle="self" class="tab mob">EXTRA-CARE SUPPORT</span>
                    <div class="tab-content" data-tab-id="support">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/support.jpg">
                        <span class="title">Extra-Care Support</span>
                        <p>Order extra-care support and cover it all – server monitoring and management, network issue resolution, basic security check and much more.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div id="vps-compare" class="compare-block">
        <div class="container">
            <h2 class="title">Still searching elsewhere? Compare Host1Plus resources and features with other VPS hosing providers in Germany</h2>

            <div class="table-wrap">

                <table>
                    <tbody>
                        <tr>
                            <td class="left date bg-trans">
                                Data taken at 21/08/2015
                            </td>
                            <td class="h1p logo">
                                <div class="pic host1plus"></div>
                                <div class="title">Host1Plus</div>
                            </td>
                            <td class="value logo">
                                <div class="pic and1"></div>
                                <div class="title">1and1</div>
                            </td>
                            <td class="value logo">
                                <div class="pic hosteurope"></div>
                                <div class="title">HostEurope</div>
                            </td>
                            <td class="value logo">
                                <div class="pic vpsnet"></div>
                                <div class="title">VPS.net</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                CPU
                            </td>
                            <td class="value h1p">
                                2 Cores
                            </td>
                            <td class="value">
                                2 Cores
                            </td>
                            <td class="value">
                                1 Core
                            </td>
                            <td class="value">
                                1 Core
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                RAM
                            </td>
                            <td class="value h1p">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Storage
                            </td>
                            <td class="value h1p">
                                80 GB
                            </td>
                            <td class="value">
                                150 GB
                            </td>
                            <td class="value">
                                150 GB
                            </td>
                            <td class="value">
                                50 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Bandwidth
                            </td>
                            <td class="value h1p">
                                2000 GB
                            </td>
                            <td class="value">
                                Unlimited
                            </td>
                            <td class="value">
                                Unlimited
                            </td>
                            <td class="value">
                                4000 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Price
                            </td>
                            <td class="value h1p">
                                $14.70
                            </td>
                            <td class="value">
                                $19.99
                            </td>
                            <td class="value">
                                $14.64
                            </td>
                            <td class="value">
                                $24.00
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                OS
                            </td>
                            <td class="value h1p tsmaller">
                                Ubuntu, Fedora, Debian, CentOS, Suse
                            </td>
                            <td class="value tsmaller">
                                CentOS, Debian, openSUSE, Ubuntu, Windows Server 2008 R2 SP1
                            </td>
                            <td class="value tsmaller">
                                CentOS, Ubuntu, Debian, Windows Server 2008 R2
                            </td>
                            <td class="value tsmaller">
                                CentOS, Ubuntu, Windows STD EDN
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                DDoS Protection
                            </td>
                            <td class="value h1p">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Root Access
                            </td>
                            <td class="value h1p">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                DNS Management
                            </td>
                            <td class="value h1p">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Tech Support
                            </td>
                            <td class="value h1p tsmaller">
                                Tickets, Email
                            </td>
                            <td class="value tsmaller">
                                Phone, Email
                            </td>
                            <td class="value tsmaller">
                                Phone, Email
                            </td>
                            <td class="value tsmaller">
                                Phone, Live chat, Tickets
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Data Center Locations in Germany
                            </td>
                            <td class="value h1p tsmaller">
                                Frankfurt
                            </td>
                            <td class="value tsmaller">
                                Berlin, Montabaur, Karlsruhe
                            </td>
                            <td class="value tsmaller">
                                Cologne
                            </td>
                            <td class="value tsmaller">
                                Frankfurt
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="compare-notice">
                    *Third-party logos and marks are registered trademarks of their respective owners. All rights reserved.
                </div>

            </div>
        </div>

    </div>

    <div class="faq2">
        <h2 class="title">Ask questions, get answers!</h2>
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq">FAQ</h3>
            <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    How long will my VPS setup take?
                    <div class="answer">
                        After you order a VPS, the service is usually set up instantly. However, if you pay using a credit card, it might take up to a few hours.
                    </div>
                </div>
                <div class="question">
                    What virtualization do you use for VPS hosting?
                    <div class="answer">
                        We are currently using OpenVZ virtualization.
                    </div>
                </div>
                <div class="question">
                    Is it possible to install a custom OS to OpenVZ VPS?
                    <div class="answer">
                        We are using only official OpenVZ templates so it is not possible to install any custom OS.
                    </div>
                </div>
                <div class="question">
                    What if I reach my VPS bandwidth limit?
                    <div class="answer">
                        In case your server uses a lot of network resources, there is a chance that you might reach your monthly VPS bandwidth limit. If this happens, the network speed of your server will be limited to 64 Kbits/s and you will instantly receive an email with a notification about the limit. If you wish to solve this problem you may either wait for the month to end (or end of your billing cycle) when the bandwidth count starts over; or upgrade your bandwidth resources within your Client Area. Please keep in mind that in this case, the price of your VPS would increase.
                    </div>
                </div>
                <div class="question">
                    What is the network speed of your Linux VPS hosting?
                    <div class="answer">
                        We limit the network speed provided depending on the location of your VPS hosting service.<br>
                        <br>
                        <strong>Outgoing</strong> (data that is being sent from your server to another one) network speed is limited up to 500 Mbps in all VPS hosting locations except São Paulo, Brazil, where network speed limit is higher - up to 200 Mbps.<br>
                        <br>
                        <strong>Incoming</strong> (data that is being transferred to your server from another one) network speed does not have any limitations.
                    </div>
                </div>
                <div class="question">
                    What happens when my CPU limit is reached?
                    <div class="answer">
                        If you are overusing your CPU you will receive a notification that the power of your processor is temporarily limited.<br>
                        You can review your CPU power usage on the statistics page in your Client Area (for more information <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1244/108/where-can-i-find-my-vps-statistics-page">click here</a>).<br>
                        If you want to avoid reaching the limit, you can upgrade your CPU in your Client Area (for more information visit <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1241/108/how-to-upgradedowngrade-my-vps">click here</a>).
                    </div>
                </div>
            </div>
            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/how-to-create-virtualhost-on-apache"><i class="fa fa-file-text-o file-ico"></i> How to create virtualhost on Apache?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-csf-on-linux"><i class="fa fa-file-text-o file-ico"></i> How to install CSF on Linux?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/introduction-to-ssh-keys"><i class="fa fa-file-text-o file-ico"></i> Introduction to SSH keys</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-gnome-desktop"><i class="fa fa-file-text-o file-ico"></i> How to install GNOME desktop?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-change-a-time-zone-on-a-vps-server-2"><i class="fa fa-file-text-o file-ico"></i> How to change a time zone on a VPS server?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-add-ip-address-on-linux-operating-systems"><i class="fa fa-file-text-o file-ico"></i> How to add IP address on CentOS operating system?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="back-to-top">
        <div class="back-box">
            <table>
            <tbody>
            <tr>
                <td><div class="main-text">Take a look at Germany VPS plans</div></td>
                <td><a href="<?php echo $config['whmcs_links']['checkout_vps']?>?plan=amber&location=germany" class="button">Get started</a></td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>

    <div class="prefooter">
        <h3 class="sub-title">Learn more about other hosting solutions in Germany</h3>
        <a href="<?php echo $config['links']['web-hosting']?>" class="button" style="">Web hosting in Germany</a>
        <a href="<?php echo $config['links']['reseller-hosting']?>" class="button" style="margin-left:0px;">Reseller hosting in Germany</a>
    </div>

</div>

<?php get_footer(); ?>