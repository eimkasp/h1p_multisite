<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: European Hosting
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);


wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

get_header(); ?>

<div id="european-hosting">

    <div class="landing-header tablet-pad">
        <div class="container">
            <h1 class="page-title">European hosting</h1>
            <h2 class="page-subtitle">Our available Europe hosting options stretching across 4 different countries!</h2>

            <div class="services-container">

                <?php
                    include_block( __DIR__ . "/../htmlblocks/services_locations_colls.php",
                        [
                            'web_hosting' => ['london','frankfurt','amsterdam','siauliai'],
                            'vps_hosting' => ['frankfurt'],
                            'reseller_hosting' => ['frankfurt','amsterdam','siauliai'],
                            'dedicated_servers' => ['frankfurt'],
                        ]
                    );
                ?>

            </div>

        </div>
    </div>

    <div class="services-overview">
        <div class="container">
            <h2 class="title">Our European hosting services overview</h2>
            <?php
                include_block( __DIR__ . "/../htmlblocks/services-overview.php");
            ?>
        </div>
    </div>

    <?php include __DIR__ . '/../htmlblocks/reviews_2.php'; ?>

    <div class="else-locations">
        <div class="container">
            <h2 class="title">Looking for hosting solutions elsewhere?<br>Check out other available hosting locations at Host1Plus</h2>

            <div class="services-container">

                <?php
                    include_block( __DIR__ . "/../htmlblocks/services_locations_colls.php",
                        [
                            'web_hosting' => ['los_angeles','chicago','san_paulo','johannesburg'],
                            'vps_hosting' => ['los_angeles','chicago','san_paulo','johannesburg'],
                            'reseller_hosting' => ['chicago','san_paulo','johannesburg'],
                        ]
                    );
                ?>

            </div>

        </div>
    </div>

    <div class="what-else">
        <h2 class="title">Why choose Host1Plus?</h2>

        <div class="block-tabs">
            <div class="tabs-wrapper">
                <div class="app-tabs">
                    <div class="table">
                        <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active">14-DAY MONEY-BACK GUARANTEE</span></div>
                        <div class="cell"><span data-tab-link="feature.multilingual" class="tab desktop">MULTILINGUAL TECH SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.flexible" class="tab desktop">FLEXIBLE PAYMENT OPTIONS</span></div>
                        <div class="cell"><span data-tab-link="feature.reliable" class="tab desktop">RELIABLE HARDWARE</span></div>
                        <div class="cell"><span data-tab-link="feature.savings" class="tab desktop">MAJOR SAVINGS</span></div>
                        <div class="cell"><span data-tab-link="feature.responsive" class="tab desktop">RESPONSIVE CLIENT AREA</span></div>
                    </div>
                </div>
                <div class="tabs" data-tabs="feature">

                    <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">14-DAY MONEY-BACK GUARANTEE</span>
                    <div class="tab-content active" data-tab-id="money-back">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.jpg">
                        <span class="title">14-Day Money-Back Guarantee</span>
                        <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
                    </div>

                    <span data-tab-link="feature.multilingual" data-tab-toggle="self" class="tab mob">MULTILINGUAL TECH SUPPORT</span>
                    <div class="tab-content" data-tab-id="multilingual">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/multilingual.jpg">
                        <span class="title">Multilingual Tech Support</span>
                        <p>No more language barriers! We speak English, Lithuanian, Spanish and Portuguese.</p>
                    </div>

                    <span data-tab-link="feature.flexible" data-tab-toggle="self" class="tab mob">FLEXIBLE PAYMENT OPTIONS</span>
                    <div class="tab-content" data-tab-id="flexible">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/payment.jpg">
                        <span class="title">Flexible Payment Options</span>
                        <p>Choose from a variety of different payment options, including Credit Card, Paypal, Skrill, Paysera, CashU, Ebanx, Braintree, Alipay transactions and Bitcoin payments.</p>
                    </div>

                    <span data-tab-link="feature.reliable" data-tab-toggle="self" class="tab mob">RELIABLE HARDWARE</span>
                    <div class="tab-content" data-tab-id="reliable">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/raid.jpg">
                        <span class="title">Reliable Hardware</span>
                        <p>Benefit from enterprise hardware with dual PSU and RAID data storage to prevent data loss due to unexpected damage.</p>
                    </div>

                    <span data-tab-link="feature.savings" data-tab-toggle="self" class="tab mob">MAJOR SAVINGS</span>
                    <div class="tab-content" data-tab-id="savings">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/savings.jpg">
                        <span class="title">Major Savings</span>
                        <p>Sign up for an extended billing cycle and save up to 11% for your purchase!</p>
                    </div>

                    <span data-tab-link="feature.responsive" data-tab-toggle="self" class="tab mob">RESPONSIVE CLIENT AREA</span>
                    <div class="tab-content" data-tab-id="responsive">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/responsive.jpg">
                        <span class="title">Responsive Client Area</span>
                        <p>Always on the go? Easily manage your services using any mobile device.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="faq2">
        <h2 class="title">Ask questions, get answers!</h2>
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq">FAQ</h3>
            <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    How long will my VPS setup take?
                    <div class="answer">
                        After you order a VPS, the service is usually set up instantly. However, if you pay using a credit card, it might take up to a few hours.
                    </div>
                </div>
                <div class="question">
                    What virtualization do you use for VPS hosting?
                    <div class="answer">
                        We are currently using OpenVZ virtualization.
                    </div>
                </div>
                <div class="question">
                    What is the network speed of your Linux VPS hosting?
                    <div class="answer">
                        We limit the network speed provided depending on the location of your VPS hosting service.<br>
                        <br>
                        <strong>Outgoing</strong> (data that is being sent from your server to another one) network speed is limited up to 500 Mbps in all VPS hosting locations except São Paulo, Brazil, where network speed limit is higher - up to 200 Mbps.<br>
                        <br>
                        <strong>Incoming</strong> (data that is being transferred to your server from another one) network speed does not have any limitations.
                    </div>
                </div>
                <div class="question">
                    How to log in to my web hosting panel?
                    <div class="answer">
                        First, log in to your Client Area and click Services > Web Hosting. <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1230/103/how-to-log-in-to-my-web-hosting-panel">Learn more</a>
                    </div>
                </div>
                <div class="question">
                    What IP addresses will my reseller hosting clients get?
                    <div class="answer">
                        When you purchase a reseller hosting plan you will get an IP address that will remain the same for your clients as well.
                    </div>
                </div>
                <div class="question">
                        What control panels do you provide for web hosting?
                    <div class="answer">
                        <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1233/103/what-control-panels-do-you-provide-for-web-hostng">Learn more</a>
                    </div>
                </div>
            </div>
            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/how-to-set-up-openvpn-on-linux-vps"><i class="fa fa-file-text-o file-ico"></i> How to set up OpenVPN on Linux VPS?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/logs-on-linux-vps"><i class="fa fa-file-text-o file-ico"></i> Logs on Linux VPS</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/control-panels/cpanel/how-to-add-domain-in-cpanel"><i class="fa fa-file-text-o file-ico"></i> How to add addon domain in cPanel?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/dedicated-hosting-services/how-to-install-cpanel-in-dedicated-server"><i class="fa fa-file-text-o file-ico"></i> How to install cPanel in dedicated server</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/dedicated-hosting-services/how-to-check-if-all-the-ports-are-open-on-the-dedicated-server"><i class="fa fa-file-text-o file-ico"></i> How to check if all the ports are open on the dedicated server</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/control-panels/cpanel/how-to-add-domain-in-cpanel"><i class="fa fa-file-text-o file-ico"></i> How to add addon domain in cPanel?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/control-panels/directadmin/all-directadmin-tutorials/importing-cpanel-user-to-directadmin"><i class="fa fa-file-text-o file-ico"></i> Importing cPanel user to DirectAdmin</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-modify-php-settings-on-a-web-hosting-server"><i class="fa fa-file-text-o file-ico"></i> How to modify PHP settings on a web hosting server?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="back-to-top">
        <div class="back-box">
            <table>
            <tbody>
            <tr>
                <td><div class="main-text">Join Host1Plus and benefit from powerful European hosting solutions ensuring blistering connectivity</div></td>
                <td><a href="#" class="button" onclick="$('html, body').animate({scrollTop: $('.landing-header').offset().top}, 2000);">Explore your options</a></td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>


</div>

<?php get_footer(); ?>
