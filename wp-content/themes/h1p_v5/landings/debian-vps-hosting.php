<?php
/**
 * @package WordPress
 * @subpackage h1pv4
 */
/*
Template Name: Landing: Debian VPS Hosting
*/

wp_enqueue_style('landings-csslider.style', get_template_directory_uri() . '/landings/css/cs_slider.css', ['style']);
wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings/css/landing.css', ['style']);
wp_enqueue_style('landings-responsive.style', get_template_directory_uri() . '/landings/css/responsive.css', ['style']);


wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('landing.js', get_template_directory_uri() . '/landings/landing.js', ['jquery'], false, true);

get_header();

    global $whmcs;
    global $config;

    $web_hosting_min_price = $whmcs->getMinPrice($config['products']['web_hosting']['plans'][1]['id']);
    $vps_hosting_min_price = $whmcs->getMinPrice($config['products']['vps_hosting']['locations']['chicago']['id']);
    $dedicated_servers_min_price = $whmcs->getPriceOptions($config['products']['dedicated_servers']['plans'][1]['id'], 'quarterly')['price_monthly'];
    $reseller_hosting_min_price = $whmcs->getMinPrice($config['products']['reseller_hosting']['plans'][1]['id']);
?>

<div id="centos-hosting">

    <div class="landing-header tablet-pad">
        <div class="container">
            <h1 class="page-title"><span class="oslogo debian "></span>Debian VPS Hosting</h1>
            <h2 class="page-subtitle">Carefully crafted Debian VPS hosting with full server control</h2>
        </div>

            <div class="choose-location">

                <div class="title">Choose your preferred server location</div>
                <div><span class="arrow-down"></span></div>
                <div class="locations-list">

                    <?php
                        $plan = 'amber';
                        $planConfig = $whmcs->get_local_stepsconfig('vps_plans')[ $plan ];
                        $locations = $config['products']['vps_hosting']['locations'];

                        $first_loc_override_key = 'chicago';
                        if( array_key_exists( $first_loc_override_key, $locations) ){
                            $first_loc_override = $locations[$first_loc_override_key]; //which location should be first
                            unset( $locations[$first_loc_override_key] );
                            $locations = [$first_loc_override_key => $first_loc_override] + $locations;
                        }

                        foreach($locations as $location_key => $location):

                            $prices = $whmcs->getConfigurablePrice($location['id'], $planConfig['configs']);
                     ?>

                        <div class="location" onclick="$(this).find('form').submit();">
                            <form class="" action="<?php echo $config['whmcs_links']['checkout_vps']?>" method="get">

                                <input type="hidden" name="plan" value="<?php echo $plan;?>"/>
                                <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                                <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                                <input type="hidden" name="promocode" value="<?php echo $whmcs_promo;?>"/>
                                <input type="hidden" name="location" value="<?php echo $location['key'] ?>"/>

                                <div class="label">
                                    <?php
                                    switch ($location_key){
                                        case 'los_angeles':
                                            echo __('Los Angeles');
                                            break;
                                        case 'chicago':
                                            echo __('Chicago');
                                            break;
                                        case 'sao_paulo':
                                            echo __('São Paulo');
                                            break;
                                        case 'frankfurt':
                                            echo __('Frankfurt');
                                            break;
                                        case 'johannesburg':
                                            echo __('Johannesburg');
                                            break;
                                    }
                                    ?>
                                </div>
                                <div><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/<?php echo strtoupper( $location['country_code'] ) ?>.png" alt="" width="80px"/></div>
                                <div class="price-notice">from <span class="price"><?php echo $prices[0]['price']; ?></span> / month</div>
                            </form>
                        </div>

                    <?php endforeach; ?>
                </div>
                <div class="distributions">
                    <strong>Available Debian distributions:</strong> 7.0 x86_64&nbsp;&nbsp;|&nbsp;&nbsp;7.0 x86&nbsp;&nbsp;|&nbsp;&nbsp;6.0 x86_64&nbsp;&nbsp;|&nbsp;&nbsp;6.0 x86
                    <div class="compare" onclick="$('html, body').animate({scrollTop: $('#vps-compare').offset().top}, 2000);">
                        <span class="underline">Compare our offer against other Debian VPS hosting providers</span>&nbsp;&nbsp;&nbsp;<i class="fa fa-sort-desc"></i>
                    </div>
                </div>
            </div>

    </div>

    <div class="features-boxes container">
        <h2 class="title">
            Debian VPS hosting overview
        </h2>
        <div class="feature-box">
            <div class="fico">
                <i class="ico fingerprint"></i>
            </div>
            <div class="ftitle">Ensured Security</div>
            <div class="fcontent">
                No security puzzles! After the installation of Debian OS, it automatically gets all the security patches before the very first boot.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico console"></i>
            </div>
            <div class="ftitle">Full Root Access</div>
            <div class="fcontent">
                Enjoy full authority over your server and edit or install various programs according to your needs.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico dns"></i>
            </div>
            <div class="ftitle">DNS Management</div>
            <div class="fcontent">
                Manage your server without struggle! Create your domain zones, edit them or set its destinations all by yourself through our Client Area.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico settings"></i>
            </div>
            <div class="ftitle">Stability Branch Upkeep</div>
            <div class="fcontent">
                When it comes to high stability measures – Debian is an excellent option. Its Stable branch is always updated if too many bugs appear on the system.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico protection"></i>
            </div>
            <div class="ftitle">DDoS Protection</div>
            <div class="fcontent">
                A hassle free solution for secure online environment. Our VPS DDoS protection covers up to 40 Gbit/s of a single attack and is completely free.
            </div>
        </div>
        <div class="feature-box">
            <div class="fico">
                <i class="ico releases"></i>
            </div>
            <div class="ftitle">Constant Debian Releases</div>
            <div class="fcontent">
                Perfection has no borders and Debian gets it! Major OS releases are provided every two years to take it up a notch.
            </div>
        </div>
    </div>

        <?php include __DIR__ . '/../htmlblocks/reviews_2.php'; ?>

    <div id="vps-compare" class="compare-block">
        <div class="container">
            <h2 class="title">Still not sure? Compare our Debian VPS hosting offer with other hosting providers</h2>

            <div class="table-wrap w995">

                <table>
                    <tbody>
                        <tr>
                            <td class="left date">
                                Data taken at 07/09/2015
                            </td>
                            <td class="h1p logo">
                                <div class="pic host1plus"></div>
                                <div class="title">Host1Plus</div>
                            </td>
                            <td class="value logo">
                                <div class="pic mediatemple"></div>
                                <div class="title">MediaTemple</div>
                            </td>
                            <td class="value logo">
                                <div class="pic asmallorange"></div>
                                <div class="title">Asmallorange</div>
                            </td>
                            <td class="value logo">
                                <div class="pic and1"></div>
                                <div class="title">1and1</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                CPU
                            </td>
                            <td class="value h1p">
                                2 Cores
                            </td>
                            <td class="value">
                                2 Cores
                            </td>
                            <td class="value">
                                2 Cores
                            </td>
                            <td class="value">
                                2 Cores
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                RAM
                            </td>
                            <td class="value h1p">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                            <td class="value">
                                2 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Storage
                            </td>
                            <td class="value h1p">
                                80 GB
                            </td>
                            <td class="value">
                                20 GB
                            </td>
                            <td class="value">
                                30 GB
                            </td>
                            <td class="value">
                                150 GB
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Bandwidth
                            </td>
                            <td class="value h1p">
                                2000 GB
                            </td>
                            <td class="value">
                                2000 GB
                            </td>
                            <td class="value">
                                1000 GB
                            </td>
                            <td class="value">
                                Unlimited
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Price
                            </td>
                            <td class="value h1p">
                                $14.70
                            </td>
                            <td class="value">
                                $30.00
                            </td>
                            <td class="value">
                                $30.00
                            </td>
                            <td class="value">
                                $19.99
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Available Ubuntu OS Distributions
                            </td>
                            <td class="value h1p tsmaller">
                                 Debian 7<br>Debian 6
                            </td>
                            <td class="value tsmaller">
                                 Debian 8<br>Debian 7<br>Debian 6
                            </td>
                            <td class="value tsmaller">
                                 Debian 7
                            </td>
                            <td class="value tsmaller">
                                 Debian 7
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                DDoS Protection
                            </td>
                            <td class="value h1p">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-check"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                            <td class="value">
                                <i class="fa fa-minus"></i>
                            </td>
                        </tr>
                        <tr>
                            <td class="feature left">
                                Data Center Locations
                            </td>
                            <td class="value h1p tsmaller">
                                Chicago, USA<br>Los Angeles, USA<br>São Paulo, BR<br>Frankfurt, DE<br>Johannesburg, ZA
                            </td>
                            <td class="value tsmaller">
                                El Segundo, USA<br>Ashburn, USA
                            </td>
                            <td class="value tsmaller">
                                Dallas, USA<br>Dearbon, USA
                            </td>
                            <td class="value tsmaller">
                                Utah, USA<br>Germany<br>United Kingdom
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="compare-notice">
                    *Third-party logos and marks are registered trademarks of their respective owners. All rights reserved.
                </div>

            </div>
        </div>

    </div>

    <div class="what-else">
        <h2 class="title">Why choose Host1Plus?</h2>

        <div class="block-tabs">
            <div class="tabs-wrapper">
                <div class="app-tabs">
                    <div class="table">
                        <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active">14-DAY MONEY-BACK GUARANTEE</span></div>
                        <div class="cell"><span data-tab-link="feature.multilingual" class="tab desktop">MULTILINGUAL TECH SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.support" class="tab desktop">EXTRA-CARE SUPPORT</span></div>
                        <div class="cell"><span data-tab-link="feature.management" class="tab desktop">EASY SERVER MANAGEMENT</span></div>
                        <div class="cell"><span data-tab-link="feature.productivity" class="tab desktop">SUPERIOR PRODUCTIVITY</span></div>
                        <div class="cell"><span data-tab-link="feature.uptime" class="tab desktop">99.9% UPTIME</span></div>
                    </div>
                </div>
                <div class="tabs" data-tabs="feature">

                    <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob">14-DAY MONEY-BACK GUARANTEE</span>
                    <div class="tab-content active" data-tab-id="money-back">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/guarantee.png">
                        <span class="title">14-Day Money-Back Guarantee</span>
                        <p>Put us to the test! Try our services without any risk and get your money back if we don’t meet your expectations.</p>
                    </div>

                    <span data-tab-link="feature.multilingual" data-tab-toggle="self" class="tab mob">MULTILINGUAL TECH SUPPORT</span>
                    <div class="tab-content" data-tab-id="multilingual">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/multilingual.png">
                        <span class="title">Multilingual Tech Support</span>
                        <p>No more language barriers! We speak English, Spanish, Portuguese and Lithuanian.</p>
                    </div>

                    <span data-tab-link="feature.support" data-tab-toggle="self" class="tab mob">EXTRA-CARE SUPPORT</span>
                    <div class="tab-content" data-tab-id="support">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/support.png">
                        <span class="title">Extra-Care Support</span>
                        <p>Order extra-care support and cover it all – server monitoring and management, network issue resolution, basic security check and much more.</p>
                    </div>

                    <span data-tab-link="feature.management" data-tab-toggle="self" class="tab mob">EASY SERVER MANAGEMENT</span>
                    <div class="tab-content" data-tab-id="management">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/management-cpanel.png">
                        <span class="title">Easy Server Management</span>
                        <p>Enjoy one of the most popular and well-trusted control panels on the market! Sign up and get a full license for only $12.00 per month.</p>
                    </div>

                    <span data-tab-link="feature.productivity" data-tab-toggle="self" class="tab mob">SUPERIOR PRODUCTIVITY</span>
                    <div class="tab-content" data-tab-id="productivity">
                        <img src="<?php bloginfo('template_directory'); ?>/landings/images/pruductiivity-2.png">
                        <span class="title">Superior Productivity</span>
                        <p>Track your website, traffic and bandwidth usage statistics to increase your overall performance.</p>
                    </div>

                    <span data-tab-link="feature.uptime" data-tab-toggle="self" class="tab mob">99.9% UPTIME</span>
                    <div class="tab-content" data-tab-id="uptime">
                      <img src="<?php bloginfo('template_directory'); ?>/landings/images/uptime.png">
                      <span class="title">99.9% Uptime</span>
                      <p>Choose us and preserve your online performance – we guarantee 99.9% uptime. And if we’re wrong - we always compensate our customers if any unexpected downtime occurs.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php include __DIR__ . '/../htmlblocks/payment_methods.php'; ?>

    <div class="faq2">
        <h2 class="title">Ask questions, get answers!</h2>
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq">FAQ</h3>
            <h3 class="tab" data-tab-index="tutorials">Tutorials</h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    How long will my VPS setup take?
                    <div class="answer">
                        After you order a VPS, the service is usually set up instantly. However, if you pay using a credit card, it might take up to a few hours.
                    </div>
                </div>
                <div class="question">
                    What virtualization do you use for VPS hosting?
                    <div class="answer">
                        We are currently using OpenVZ virtualization.
                    </div>
                </div>
                <div class="question">
                    Is it possible to install a custom OS to OpenVZ VPS?
                    <div class="answer">
                        We are using only official OpenVZ templates only so it is not possible to install any custom OS.
                    </div>
                </div>
                <div class="question">
                    What is the network speed of your Linux VPS hosting?
                    <div class="answer">
                        We limit the network speed provided depending on the location of your VPS hosting service.<br>
                        <br>
                        <strong>Outgoing</strong> (data that is being sent from your server to another one) network speed is limited up to 500 Mbps in all VPS hosting locations except São Paulo, Brazil, where network speed limit is higher - up to 200 Mbps.<br>
                        <br>
                        <strong>Incoming</strong> (data that is being transferred to your server from another one) network speed does not have any limitations.
                    </div>
                </div>
                <div class="question">
                    Which control panels can I install on my VPS?
                    <div class="answer">
                        You can manually install any free open source control panel that supports Ubuntu operating system, such as Sentora, Ajenti, Vesta and many more.
                    </div>
                </div>
            </div>
            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/how-to-create-virtualhost-on-apache"><i class="fa fa-file-text-o file-ico"></i> How to create virtualhost on Apache?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-csf-on-linux"><i class="fa fa-file-text-o file-ico"></i> How to install CSF on Linux?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/introduction-to-ssh-keys"><i class="fa fa-file-text-o file-ico"></i> Introduction to SSH keys</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-gnome-desktop"><i class="fa fa-file-text-o file-ico"></i> How to install GNOME desktop?</a>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-change-a-time-zone-on-a-vps-server-2"><i class="fa fa-file-text-o file-ico"></i> How to change a time zone on a VPS server?</a>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/how-to-setup-ip-rotations-for-emails-on-linux"><i class="fa fa-file-text-o file-ico"></i> How to setup IP rotations for emails on Linux</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="back-to-top">
        <div class="back-box">
            <table>
            <tbody>
            <tr>
                <td><div class="main-text" style="font-size:30px;">Sign up and enjoy powerful Debian VPS hosting with root access,<br>high security standards and blistering connection speed!</div></td>
                <td><a href="<?php echo $config['whmcs_links']['checkout_vps']?>?location=us_east&plan=amber" class="button">Sign up</a></td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>

    <div class="prefooter">
        <h3 class="sub-title">Looking for other Linux operating system</h3>
        <h3 class="sub-sub-title">Explore other Host1Plus Linux VPS hosting solutions</h3>
        <a href="<?php echo $config['links']['website'] . '/linux-vps-hosting/'; ?>" class="button" style="">Linux VPS hosting</a>
    </div>

</div>

<?php get_footer(); ?>
