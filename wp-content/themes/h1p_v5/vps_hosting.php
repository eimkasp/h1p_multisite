<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: VPS Hosting facelift
*/

wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);
wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);

//wp_enqueue_script('wow', get_template_directory_uri() . '/js/vendor/wow.js', ['jquery'], '1.1.2', true);

global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'vps');

// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing vps-hosting">

    <header class="headline vps-hosting">
        <div class="container adjust-vertical-center">

<?php /*
            <div class="hot-news-wrapper">
                <div class="hot-news-overflow-container">
                    <div class="hot-news-group">
                        <div class="hot-news-handle notice"><?php _e('Upgrades'); ?> <i class="fa fa-angle-right"></i></div>
                        <?php 
                            if (get_locale() == 'en_US') {
                                $news_link = 'https://www.host1plus.com/press/host1plus-hardware-upgrades-sao-paulo-brazil';
                            } elseif (get_locale() == 'pt_BR') {
                                $news_link = 'https://www.host1plus.com/press/host1plus-anuncia-upgrade-de-hardware-em-sao-paulo-brasil/';
                            }
                        ?>
                        <div class="hot-news-message"><a href="<?php echo $news_link ?>" target="_blank" title="<?php _e('Hardware upgrades in São Paulo, Brazil'); ?>!"><?php _e('Hardware upgrades in São Paulo, Brazil'); ?>!</a></div>
                    </div>
                </div> <!-- end of .hot-news-overflow-container -->
            </div>
*/ 
?>
            <h1 class="page-title"><?php _e('VPS Hosting')?> <span class="highlight"><?php _e('from')?> <?php echo $mypage->getMinPrice(); ?></span></h1>
            <div class="title-descr"><?php _e('Ultimate control and server stability in 5 locations worldwide.')?></div>

        </div>
    </header> <!-- end of .headline.vps-hosting -->

    <div class="vps-promo-withheader"></div>

    <section class="main page-content">


        <section class="service-pricing extra-pad-top" data-section-button="<?php _e('View Plans');?>">

            <div class="scroll-id-target" id="plans-table"></div>

            <div class="vps-promo-section-wide"></div>

            <div class="container">

                <div class="vps-promo-container-wide"></div>

                <div>

                    <?php include_block("htmlblocks/pricing-tables/vps-hosting-v2.php"); ?>

                </div>

                <?php include_block("htmlblocks/pricing-tables/vps-all-plans-include.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-pricing -->

    <?php 
        if (get_locale() == 'en_US'):
    ?>

        <section class="service-cta p-v-60 color-bg-style1">

            <div class="container">
                
                <div class="container-flex cta">
                    <div class="block-col width-md-3-4 cta-left" style="justify-content: center;">
                        <span class="block-title-large-thin t-ff-header t-mspaced"><?php printf(__('What is the difference between %sweb hosting%s and %sVPS%s?'), '<span class="highlight">', '</span>', '<span class="highlight">', '</span>');?></span>
                    </div>
                    <div class="block-col width-md-1-4 cta-right">
                        <a href="/vps-server/" class="button xlarge primary"><?php _e('Compare');?></a>
                    </div> 
                </div> 

            </div>

        </section> <!--end of .extra-offer-cta-->

    <?php
        endif;
    ?>


        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>


        <section class="service-locations extra-pad-top color-bg-style2 no-zoom" data-section-title="<?php _e('Locations');?>">

            <div id="locations" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Virtual Private Servers'); echo ' '; _e('Locations');?></h2>
                        <p><?php _e('Multiple premium data centers ensure high connectivity and better reach.'); ?></p>
                </div>

                <?php include_block("htmlblocks/locations-services.php", array('type' => 'vps', 'speedtest' => true )); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-locations -->


        <section class="service-features extra-pad" data-section-title="<?php _e('Features');?>">

            <div id="features" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('VPS Hosting Features')?></h2>
                </div>

                <div class="container-flex features-list">

                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-fullroot.svg" alt="Full Root Access" width="130" height="130">
                        <h3><?php _e('Full Root Access')?></h3>
                        <p><?php _e('Get full flexibility and control on various Linux OS instances - Ubuntu, Fedora, Debian, CentOS, Suse.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-dns.svg" alt="Integrated DNS Management" width="130" height="130">
                        <h3><?php _e('DNS Management')?></h3>
                        <p><?php _e('Easily create your domain zones and edit records directly at your Client Area.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-livestats.svg" alt="Live Stats" width="130" height="130">
                        <h3><?php _e('Live Stats')?></h3>
                        <p><?php _e('Track your resource usage live! Enable alerts to avoid CPU, memory or network overuse.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-scale.svg"  alt="Scalable Resources" width="130" height="130">
                        <h3><?php _e('Scalable resources')?></h3>
                        <p><?php _e('Grab more resources to support your growing needs! Customize your VPS at the Client Area and stay on a budget!')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-instant.svg" alt="Instant Deployment" width="130" height="130">
                        <h3><?php _e('Instant Deployment')?></h3>
                        <p><?php _e('Get your VPS in 3 simple steps! Configure your server, fill in billing information and start using your VPS without delays.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-console.svg" alt="Virtual Console Access" width="130" height="130">
                        <h3><?php _e('Virtual Console Access')?></h3>
                        <p><?php _e('For instant recovery, retrieve access to your server by connecting via virtual console.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-popularos.svg"  alt="Popular Linux OS" width="130" height="130">
                        <h3><?php _e('Popular Linux OS')?></h3>
                        <p><?php printf(__('Create a healthy environment with a wide range of popular Linux distros: CentOS, %sUbuntu%s, Debian, Fedora, and Suse.'), '<a href="/vps-ubuntu/">', '</a>')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-rdns.svg" alt="rDNS Configuration" width="130" height="130">
                        <h3><?php _e('rDNS Configuration')?></h3>
                        <p><?php _e('Simplified rDNS self-management means less time spent getting support and more time working on your own projects.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-geolocatedips.svg" alt="Geolocated IPs" width="130" height="130">
                        <h3><?php _e('Geolocated IPs')?></h3>
                        <p><?php _e('Benefit from geolocated IPs to reduce latency, boost your SEO capability and localize your online business.')?></p>
                    </div>

                </div>

                <div class="row center">
                    <a href="#plans-table" class="button primary orange large" data-scrollto="plans-table"><?php _e('Compare Plans');?></a>
                </div>


                <div class="extra-pad color-bg-style3 p-30 m-t-40">

                    <div class="section-header">
                        <h2 class="block-title"><?php _e('Redundant Storage')?></h2>
                    </div>

                    <p class="p-t-20"><?php _e('Our infrastructure is designed to bring maximum reliability while Ceph storage eliminates any consequences of potential hardware failure that might arise. If any part of the hardware fails, the system automatically restores data and reduces downtime.')?></p>

                    <div class="scheme-wrapper">

                        <?php include_block("htmlblocks/svg-schemes/ceph-scheme.php"); ?>


                    </div>

                </div>


                <div class="container-flex features-list p-t-40">

                    <div class="block-col w-1_3 feature-small separator-line">
                        <img class="push-left" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-highavailability.svg" alt="High Availability" width="90" height="90">
                        <h3 class="adjust-left"><?php _e('High Availability')?></h3>
                        <p class="adjust-left p-b-0"><?php _e('Fault-tolerant distributed storage guarantees 99.9% availability.')?></p>
                    </div>
                    <div class="block-col w-1_3 feature-small separator-line">
                        <img class="push-left" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-solidsecurity.svg" alt="Solid Security" width="90" height="90">
                        <h3 class="adjust-left"><?php _e('Solid Security')?></h3>
                        <p class="adjust-left p-b-0"><?php _e('Multiple data copies ensure that no data is lost.')?></p>
                    </div>
                    <div class="block-col w-1_3 feature-small separator-line">
                        <img class="push-left" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-ultimatespeed.svg" alt="Ultimate Speed" width="90" height="90">
                        <h3 class="adjust-left"><?php _e('Ultimate Speed')?></h3>
                        <p class="adjust-left p-b-0"><?php _e('Ceph is distributed among multiple SSD and rotational disks to deliver maximum performance.')?></p>
                    </div>

                </div>


            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->


        <section class="flexibility-options extra-pad color-bg-style-grad2a">
            
            <div class="container">

                <div class="section-header p-b-60">
                    <h2 class="block-title white"><?php _e('Need more flexibility?');?></h2>
                    <span class="title-follow-text white"><?php _e('Try our scalable KVM VPS servers with API, custom ISO and more.');?></span>
                </div>

                <div class="container-flex block-offer1-container">
                    <div class="block-col block-offer1">
                        <a class="wrapper-content" href="/windows-vps/">
                            <img class="push-left m-r-20" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/header_icon_win_blue.svg" alt="Windows KVM VPS icon" width="90">
                            <div class="push-left text-wrap">
                                    <h3 class="title-paragraph-lower bold"><?php _e('Windows KVM VPS');?></h3>
                                    <span class="t-mshrinked"><?php _e('from');?> <?php echo $mypage->getServiceMinPrice('cs_win') ?><?php _e('/mo');?></span>
                            </div>
                        </a>
                    </div>
                    <div class="block-col block-offer1">
                        <a class="wrapper-content" href="/linux-vps/">
                            <img class="push-left m-r-20" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/header_icon_linux_blue.svg" alt="Linux KVM VPS icon" width="90">
                            <div class="push-left text-wrap">
                                <h3 class="title-paragraph-lower bold"><?php _e('Linux KVM VPS');?></h3>
                                <span class="t-mshrinked"><?php _e('from');?> <?php echo $mypage->getServiceMinPrice('cs_lin') ?><?php _e('/mo');?></span>
                            </div>
                        </a>
                    </div>
                </div> 

            </div>

        </section>


        <section class="service-faq extra-pad-top color-bg-style1" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/vps-hosting.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->

    </section> <!-- end of .main -->

</article>

<?php

include_block("htmlblocks/hosting/speedtest_enabling_js.php");

universal_redirect_footer([
    'en' => $site_en_url.'/vps-hosting/',
    'br' => $site_br_url.'/hospedagem-vps/'
]);

get_footer();

?>
