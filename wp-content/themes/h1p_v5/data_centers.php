<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Data centers
*/

wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, NULL);

get_header();

?>
<article class="page data-centers">

    <section class="main page-content">


        <section class="service-locations extra-pad-top color-bg-style2 service-locations-bg1 no-zoom">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Worldwide Server Locations')?></h2>
                        <p><?php _e('Multiple premium data centers ensure high connectivity and better reach.'); ?><br>
                        <?php _e('Pick a location to perform a speed test.');?></p>
                </div>

                <?php include_block("htmlblocks/locations-services.php", array('type' => 'vps', 'speedtest' => true )); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-locations -->

        <section class="data-centers-details extra-pad-top">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Data Centers')?></h2>
                </div>

                <div class="locations-container-flex">

                    <div class="flex-spacer location-frankfurt"></div>

                    <div class="scroll-id-target location-frankfurt" id="location-frankfurt"></div>

                    <div class="block-col block-description location-frankfurt">

                        <h3 class="location-title"><?php _e('Frankfurt, Germany')?></h3>

                        <p class="location-description"><?php _e('Being one of the best connected data centers in the world, our facility in Frankfurt enhances server security and reliability.'); ?></p>

                        <img class="location-landmark" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/locations/landmark-frankfurt.svg" alt="Frankfurt">

                    </div>

                    <div class="block-col block-features location-frankfurt">

                        <ul class="features-list checklist orange">
                            <li><?php _e('Outstanding connectivity');?></li>
                            <li><?php _e('High security');?></li>
                            <li><?php _e('2N ups redundancy');?></li>
                            <li><?php _e('Robust power distribution');?></li>
                            <li><?php _e('N+1 Generators');?></li>
                            <li><?php _e('Direct access to the DE-CIX internet exchange');?></li>
                            <li><?php _e('Operational excellence');?></li>
                            <li><?php _e('100% sustainable energy');?></li>
                        </ul>

                        <hr size="1">

                        <div class="location-speedtest">
                            <a data-popup="<?php echo $config['links']['speedtest']?>?location=frankfurt" data-popup-type="ajax" data-popup-id="location-frankfurt-speedtest" href="#location-frankfurt-speedtest">
                                <img class="pull-left" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/locations/speedtest.svg" alt="Speed test">
                                <span><?php _e('Speed test'); ?></span>
                            </a>
                        </div>

                    </div> <!-- end of .location-frankfurt -->



                    <div class="flex-spacer location-johannesburg"></div>

                    <div class="scroll-id-target location-johannesburg" id="location-johannesburg"></div>

                    <div class="block-col block-description location-johannesburg">

                        <h3 class="location-title"><?php _e('Johannesburg, South Africa')?></h3>

                        <p class="location-description"><?php _e('Advanced server technology and more sustainable IT solutions in Johannesburg’s data center allow a reliable server environment, guarantee stability and uninterruptible server performance.'); ?></p>

                        <img class="location-landmark" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/locations/landmark-johannesburg.svg" alt="Johannesburg">

                    </div>

                    <div class="block-col block-features location-johannesburg">

                        <ul class="features-list checklist orange">
                            <li><?php _e('A high-speed fibre network offering 400 GB of bandwidth');?></li>
                            <li><?php _e('Advanced server technology and more sustainable IT solutions');?></li>
                            <li><?php _e('Fully redundant network that is quick to deploy');?></li>
                            <li><?php _e('Fully converged and flexible IP network');?></li>
                        </ul>

                        <hr size="1">

                        <div class="location-speedtest">
                            <a data-popup="<?php echo $config['links']['speedtest']?>?location=johannesburg" data-popup-type="ajax" data-popup-id="location-johannesburg-speedtest" href="#location-johannesburg-speedtest">
                                <img class="pull-left" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/locations/speedtest.svg" alt="Speed test">
                                <span><?php _e('Speed test'); ?></span>
                            </a>
                        </div>

                    </div> <!-- end of .location-johannesburg -->



                    <div class="flex-spacer location-chicago"></div>

                    <div class="scroll-id-target location-losangeles" id="location-chicago"></div>

                    <div class="block-col block-description location-chicago">

                        <h3 class="location-title"><?php _e('Chicago, USA')?></h3>

                        <p class="location-description"><?php _e('A modern data center in Chicago, Illinois, is distinguished by high security standards and thorough server upkeep. This location is known for its key power configurations - in an unlikely event of an outage (or an environmental event) there should be no discernible impact on your applications.'); ?></p>

                        <img class="location-landmark" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/locations/landmark-chicago.svg" alt="Chicago">

                    </div>

                    <div class="block-col block-features location-chicago">

                        <ul class="features-list checklist orange">
                            <li><?php _e('Minimum N+1 redundancy');?></li>
                            <li><?php _e('Diverse, 34.5-kV power feeds');?></li>
                            <li><?php _e('33 1300-kW rotary UPS systems');?></li>
                            <li><?php _e('36.4 MW critical load');?></li>
                            <li><?php _e('Native IPv6 private global IP network');?></li>
                            <li><?php _e('Low-latency peering with top traffic destinations');?></li>
                            <li><?php _e('Multiple diverse fiber paths');?></li>
                            <li><?php _e('Metro connectivity');?></li>
                        </ul>

                        <hr size="1">

                        <div class="location-speedtest">
                            <a data-popup="<?php echo $config['links']['speedtest']?>?location=chicago" data-popup-type="ajax" data-popup-id="location-chicago-speedtest" href="#location-chicago-speedtest">
                                <img class="pull-left" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/locations/speedtest.svg" alt="Speed test">
                                <span><?php _e('Speed test'); ?></span>
                            </a>
                        </div>

                    </div> <!-- end of .location-chicago -->



                    <div class="flex-spacer location-losangeles"></div>

                    <div class="scroll-id-target location-losangeles" id="location-losangeles"></div>

                    <div class="block-col block-description location-losangeles">

                        <h3 class="location-title"><?php _e('Los Angeles, USA')?></h3>

                        <p class="location-description"><?php _e('Powerful data center in LA, California, is a global leader in data center infrastructure, guarantees immaculate connectivity and ultra-fast response times.'); ?></p>

                        <img class="location-landmark" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/locations/landmark-losangeles.svg" alt="Los Angeles">

                    </div>

                    <div class="block-col block-features location-losangeles">

                        <ul class="features-list checklist orange">
                            <li><?php _e('5 hurricane-rated facility');?></li>
                            <li><?php _e('Tier 1 bandwidth');?></li>
                            <li><?php _e('Minimum N+1 redundancy');?></li>
                            <li><?php _e('Three-tier electrical power design');?></li>
                            <li><?php _e('Support 8MW of fully redundant power');?></li>
                        </ul>

                        <hr size="1">

                        <div class="location-speedtest">
                            <a data-popup="<?php echo $config['links']['speedtest']?>?location=losangeles" data-popup-type="ajax" data-popup-id="location-losangeles-speedtest" href="#location-losangeles-speedtest">
                                <img class="pull-left" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/locations/speedtest.svg" alt="Speed test">
                                <span><?php _e('Speed test'); ?></span>
                            </a>
                        </div>

                    </div> <!-- end of .location-losangeles -->



                    <div class="flex-spacer location-saopaulo"></div>

                    <div class="scroll-id-target location-saopaulo" id="location-saopaulo"></div>

                    <div class="block-col block-description location-saopaulo">

                        <h3 class="location-title"><?php _e('São Paulo, Brazil')?></h3>

                        <p class="location-description"><?php _e('With a wide range of security equipment and high-performance infrastructure, our facility in São Paulo upholds a secure and reliable environment.'); ?></p>

                        <img class="location-landmark" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/locations/landmark-saopaulo.svg" alt="São Paulo">

                    </div>

                    <div class="block-col block-features location-saopaulo">

                        <ul class="features-list checklist orange">
                            <li><?php _e('N+1 redundancy');?></li>
                            <li><?php _e('2N ups redundancy & configuration');?></li>
                            <li><?php _e('Dual-powered equipments & multiple uplinks');?></li>
                        </ul>

                        <hr size="1">

                        <div class="location-speedtest">
                            <a data-popup="<?php echo $config['links']['speedtest']?>?location=saopaulo" data-popup-type="ajax" data-popup-id="location-saopaulo-speedtest" href="#location-saopaulo-speedtest">
                                <img class="pull-left" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/locations/speedtest.svg" alt="Speed test">
                                <span><?php _e('Speed test'); ?></span>
                            </a>
                        </div>

                    </div> <!-- end of .location-saopaulo -->



                </div> <!-- end of .locations-container-flex -->


            </div> <!-- end of .container -->

        </section>

    </section> <!-- end of .main -->

</article>

<?php

include_block("htmlblocks/hosting/speedtest_enabling_js.php");

universal_redirect_footer([
    'en' => $site_en_url.'/data-centers/',
    'br' => $site_br_url.'/data-centers/'
]);

get_footer(); 

?>

