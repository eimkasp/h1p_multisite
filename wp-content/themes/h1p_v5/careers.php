<?php
/**
* @package WordPress
* @subpackage h1p_v5
*
* Template Name: Careers
*/

if(get_locale() == 'pt_BR') {
    $lang_code = 'pt-BR';
} else {
    $lang_code = 'en';
}
wp_enqueue_script('recaptcha', 'https://www.google.com/recaptcha/api.js?&hl=' . $lang_code, [], false, true);
//wp_enqueue_script('pikaday', get_template_directory_uri() . '/js/vendor/pikaday.js', ['jquery'], false, true);
wp_enqueue_script('pikaday', 'https://cdnjs.cloudflare.com/ajax/libs/pikaday/1.4.0/pikaday.min.js', ['jquery'], false, true);
wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);




get_header();

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$custom_fields = get_post_custom();
$internship_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

?>

<article class="page jobs">

    <header class="headline careers">
        <div class="container adjust-vertical-center">
            <h1 class="page-title"><?php _e('Careers at Host1Plus')?></h1>
            <div class="title-descr"><?php _e('Are you an experienced specialist? A recent graduate? An ambitious student? If you’re excited by innovations and technology, we have a lot in common. Let’s work together!')?></div>
            <a href="#positions" class="button ghost adjust-vertical-tm20" data-scrollto="positions"><?php _e('Open Positions');?></a>
            <span class="space-10"></span>
            <a href="#internships" class="button ghost adjust-vertical-tm20" data-scrollto="internships"><?php _e('Internships');?></a>

        </div>
    </header> <!-- end of .headline.careers -->

    <section class="main page-content">


        <section class="positions extra-pad-top">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Why work here?')?></h2>
                </div>

                <span class="title-text m-b-20"><?php printf (__('We are a %s growing %s company that has already gathered a friendly team of highly-skilled and motivated specialists who adore their work. Always fully loaded with innovative ideas, we strive to push our skills further and beyond. As we are trying to fetch new talents, everyone is invited!'), '<em>','</em>'); ?></span>
                <br>

                <div class="row three-col-row benefits">
                    <div class="block-col">
                        <span class="pic-title"><?php _e('Free Snacks'); ?></span>
                        <img class="benefits-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/jobs/company-freesnacks.jpg" alt="<?php _e('Free Snacks'); ?>" >
                    </div>
                    <div class="block-col">
                        <span class="pic-title"><?php _e('Wellness Activities'); ?></span>
                        <img class="benefits-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/jobs/company-wellness.jpg" alt="<?php _e('Wellness Activities'); ?>" >
                    </div>
                    <div class="block-col">
                        <span class="pic-title"><?php _e('Annual Retreats'); ?></span>
                        <img class="benefits-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/jobs/company-retreats.jpg" alt="<?php _e('Annual Retreats'); ?>" >
                    </div>
                </div>
                <div class="row two-col-row benefits">
                    <div class="block-col">
                        <span class="pic-title"><?php _e('Training'); ?></span>
                        <img class="benefits-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/jobs/company-training.jpg" alt="<?php _e('Training'); ?>" >
                    </div>
                    <div class="block-col">
                        <span class="pic-title"><?php _e('Team Building Days'); ?></span>
                        <img class="benefits-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/jobs/company-teambuilding.jpg" alt="<?php _e('Team Building Days'); ?>" >
                    </div>
                </div>
                <div class="row three-col-row benefits">
                    <div class="block-col">
                        <span class="pic-title"><?php _e('Friendly Environment'); ?></span>
                        <img class="benefits-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/jobs/company-climate.jpg" alt="<?php _e('Friendly Environment'); ?>" >
                    </div>
                    <div class="block-col">
                        <span class="pic-title"><?php _e('Flexible Hours'); ?></span>
                        <img class="benefits-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/jobs/company-flexiblehours.jpg" alt="<?php _e('Flexible Hours'); ?>" >
                    </div>
                    <div class="block-col">
                        <span class="pic-title"><?php _e('Game Room'); ?></span>
                        <img class="benefits-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/jobs/company-games.jpg" alt="<?php _e('Game Room'); ?>" >
                    </div>
                </div>

            </div>

        </section>

        <section class="positions extra-pad-top">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Open Positions')?></h2>
                </div>

                <span class="title-text"><?php _e('We are looking for inspiring individuals! Roll up your sleeves and explore career opportunities at Host1Plus. Join us and enjoy new challenges, employ your creativity, develop your skills and create awesome online experiences.')?></span>
                <br>
                <span class="title-text"><?php printf(__('Please send your resume at %sjobs@host1plus.com%s and don\'t forget to enter the position you are applying for in the subject line.'), '<a href="mailto:jobs@host1plus.com">', '</a>');?></span>

                <div class="scroll-id-target" id="positions"></div>

                <div class="block-separator"></div>

                <div class="content-block offers">

                    <div class="layout-row two-col-row">

                        <div class="block-col accordion">

                        <?php if(get_locale() == 'en_US'): ?>
                            <div class="item offer">
                                <a class="item-title" href="#" data-prevent data-toggle-active><i class="fa fa-plus"></i> <h4><?php _e('Office Administrator')?></h4></a>
                                <div class="item-content">

                                    <p>
                                        <strong>Location:</strong> Kaunas, Lithuania
                                        <br/>
                                        <strong>Type:</strong> Full Time
                                        <br/>
                                        <strong>Language:</strong> English
                                    </p>

                                    <p>We are looking for a responsible and sociable Office Administrator to join our team in Kaunas, Lithuania.</p>

                                    <p class="list-title">Responsibilities</p>
                                    <ul>
                                        <li>Personnel document administration, upkeep and archiving (employment contracts, commandments, work schedule accounting, financial reports to Sodra);</li>
                                        <li>Correspondence, internal document management and data submission to internal systems;</li>
                                        <li>Travel and accommodation arrangement for colleagues;</li>
                                        <li>Track stocks of office supplies and place orders when necessary;</li>
                                        <li>Manage phone calls and correspondence (e-mail, letters, packages, etc.);</li>
                                        <li>Manage and schedule appointments, meetings;</li>
                                        <li>Office and company event management;</li>
                                        <li>Manage other requests from top management.</li>
                                    </ul>


                                    <p class="list-title">Requirements</p>
                                    <ul>
                                        <li>Knowledge and understanding of Lithuanian Labor Code;</li>
                                        <li>Excellent MS Office, MS Excel skills;</li>
                                        <li>Excellent written and spoken English skills;</li>
                                        <li>Excellent communication skills;</li>
                                        <li>Attentive to details, responsible;</li>
                                        <li>Proficiency in German and/or Russian language (advantage).</li>
                                    </ul>

                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if(get_locale() == 'en_US'): ?>
                            <div class="item offer">
                                <a class="item-title" href="#" data-prevent data-toggle-active><i class="fa fa-plus"></i> <h4><?php _e('HR Manager')?></h4></a>
                                <div class="item-content">

                                    <p>
                                        <strong>Location:</strong> Kaunas, Lithuania
                                        <br/>
                                        <strong>Type:</strong> Full Time
                                        <br/>
                                        <strong>Language:</strong> English
                                    </p>

                                    <p>We are looking for a talented and highly motivated HR Manager to join our team in Kaunas, Lithuania.</p>

                                    <p class="list-title">Responsibilities</p>
                                    <ul>
                                        <li>To implement and develop company’s HR procedures and policies;</li>
                                        <li>To coordinate and manage recruitment process;</li>
                                        <li>To perform onboarding procedures, coordinate trainings;</li>
                                        <li>To drive and manage employee’s performance processes, development plans, remuneration and reward procedures;</li>
                                        <li>To support departmental managers in administrative and HR related affairs;</li>
                                        <li>To participate in various HR projects;</li>
                                        <li>To provide support for office and company’s events management.</li>
                                    </ul>


                                    <p class="list-title">Requirements</p>
                                    <ul>
                                        <li>Bachelor's degree or equivalent in Human Resources/Law/Business Management;</li>
                                        <li>At least 3 years of experience in a similar position in an international environment;</li>
                                        <li>Knowledge and understanding of Lithuanian Labor Code;</li>
                                        <li>Excellent written and spoken English skills;</li>
                                    </ul>

                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if(get_locale() == 'en_US'): ?>

                            <div class="item offer">
                                <a class="item-title" href="#" data-prevent data-toggle-active><i class="fa fa-plus"></i> <h4><?php _e('Customer Support Specialist')?></h4></a>
                                <div class="item-content">

                                    <p>
                                        <strong>Location:</strong> Kaunas, Lithuania
                                        <br/>
                                        <strong>Type:</strong> Full Time
                                        <br/>
                                        <strong>Language:</strong> English
                                    </p>

                                    <p>We are seeking a motivated and energetic Customer Support Specialist to join our team in Kaunas, Lithuania. You will join our dynamic customer care team and take responsibility for providing professional assistance for incoming customer inquiries.</p>

                                    <p class="list-title">Responsibilities</p>
                                    <ul>
                                        <li>Working directly with our global customer base to answer questions and resolve technical issues;</li>
                                        <li>Windows OS and Linux OS configuration and maintenance;</li>
                                        <li>Web, Mail and DNS service debugging and problem resolution;</li>
                                        <li>Incident registration, client account management;</li>
                                        <li>Customer support concerning server maintenance;</li>
                                        <li>cPanel, DirectAdmin setup and administration;</li>
                                        <li>Internet network and interface (DNS, IP routing) configuration and customer assistance.</li>
                                    </ul>


                                    <p class="list-title">Requirements</p>
                                    <ul>
                                        <li>Scripting experience (Perl, Bash, PHP script writing);</li>
                                        <li>Virtualization technology (OpenVZ, KVM, Xen) knowledge;</li>
                                        <li>Understanding about IP network technology;</li>
                                        <li>Customer support experience;</li>
                                        <li>Passion for Linux OS and Internet technologies;</li>
                                        <li>Ability to work independently as well as in a team and learn new procedures quickly and efficiently;</li>
                                        <li>Keen attention to detail;</li>
                                        <li>Excellent written and spoken English skills;</li>
                                        <li>Other languages, especially Portuguese, would be an advantage.</li>
                                    </ul>

                                </div>
                            </div>
                        
                        <?php endif; ?>

                        <?php if(get_locale() == 'en_US'): ?>
                            <div class="item offer">
                                <a class="item-title" href="#" data-prevent data-toggle-active=""><i class="fa fa-plus"></i> <h4><?php _e('Account Manager')?></h4></a>
                                <div class="item-content">

                                    <p>
                                        <strong>Location:</strong> Kaunas, Lithuania
                                        <br/>
                                        <strong>Type:</strong> Full Time
                                        <br/>
                                        <strong>Language:</strong> English
                                    </p>

                                    <p>We are looking for a talented and highly motivated Account Manager with a good track record in consumer market and a keen interest in IT industry to join our team in Kaunas, Lithuania.</p>

                                    <p class="list-title">Responsibilities</p>
                                    <ul>
                                        <li>Analyze the needs of new and existing customers and develop selling strategies, track sales performance at all type of accounts, identify the needs with each account and exceed those needs to increase profitably;</li>
                                        <li>Increase client retention;</li>
                                        <li>Generating sales and gross profit for the company by prospecting new customers;</li>
                                        <li>Direct communication with clients and partners, long-term business relationship building and upselling;</li>
                                        <li>Working collaboratively with the Marketing department and other members of the executive team to provide detailed market intelligence;</li>
                                        <li>Creating custom offers and solutions.</li>
                                    </ul>


                                    <p class="list-title">Requirements</p>
                                    <ul>
                                        <li>Communication and interpersonal skills - comfortable and capable of communicating with all levels within and outside the business both written and verbal with a high level of professionalism;</li>
                                        <li>Organizational skills - self-motivated and able to demonstrate strong organization and prioritization skills in a sales and relationship management environment.</li>
                                        <li>Must be able to successfully manage a very diverse and demanding workload;</li>
                                        <li>Excellent written and spoken English skills (C1/C2);</li>
                                        <li>At least 3 years of experience in sales (IT background - advantage);</li>
                                        <li>Good knowledge of IT industry;</li>
                                        <li>Ability to work independently as well as in a team;</li>
                                        <li>Fast learning and multitasking skills;</li>
                                        <li>Proficiency in German and/or Russian language (advantage);</li>
                                        <li>Sociable and enthusiastic.</li>
                                    </ul>

                                </div>
                            </div>
                        <?php endif; ?>

                        </div>

                        <div class="block-col accordion">
                        
                        <?php if(get_locale() == 'en_US'): ?>

                            <div class="item offer">
                                <a class="item-title" href="#" data-prevent data-toggle-active=""><i class="fa fa-plus"></i> <h4><?php _e('Inbound Sales Manager')?></h4></a>
                                <div class="item-content">

                                    <p>
                                        <strong>Location:</strong> Kaunas, Lithuania
                                        <br/>
                                        <strong>Type:</strong> Full Time
                                        <br/>
                                        <strong>Language:</strong> English
                                    </p>

                                    <p>We are looking for a talented and highly motivated Inbound Sales Manager with a good track record in consumer market and a keen interest in IT industry to join our team in Kaunas, Lithuania.</p>

                                    <p class="list-title">Responsibilities</p>
                                    <ul>
                                        <li>Promptly and proactively address incoming inquiries generated through web advertising and marketing campaigns;</li>
                                        <li>Direct communication with clients and partners, long-term business relationship building and upselling;</li>
                                        <li>Analyzing the needs of new and current customers and develop selling strategies;</li>
                                        <li>Working collaboratively with the Marketing department and other members of the executive team to provide detailed market intelligence;</li>
                                        <li>Creating custom offers and solutions.</li>
                                    </ul>


                                    <p class="list-title">Requirements</p>
                                    <ul>
                                        <li>Excellent written and spoken English skills (C1/C2);</li>
                                        <li>At least 2 years of experience in sales (IT background - advantage);</li>
                                        <li>Good knowledge of IT industry;</li>
                                        <li>Ability to work independently as well as in a team;</li>
                                        <li>Fast learning and multitasking skills;</li>
                                        <li>Proficiency in German and/or Russian language (advantage);</li>
                                        <li>Sociable and enthusiastic.</li>
                                    </ul>

                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if(get_locale() == 'en_US'): ?>

                            <div class="item offer">
                                <a class="item-title" href="#" data-prevent data-toggle-active=""><i class="fa fa-plus"></i> <h4><?php _e('Outbound Sales Manager')?></h4></a>
                                <div class="item-content">

                                    <p>
                                        <strong>Location:</strong> Kaunas, Lithuania
                                        <br/>
                                        <strong>Type:</strong> Full Time
                                        <br/>
                                        <strong>Language:</strong> English
                                    </p>

                                    <p>We are looking for a talented and highly motivated Outbound Sales Manager with a good track record in consumer market and a keen interest in IT industry to join our team in Kaunas, Lithuania.</p>

                                    <p class="list-title">Responsibilities</p>
                                    <ul>
                                        <li>Analyzing the needs of new customers and develop selling strategies;</li>
                                        <li>Generating sales and gross profit for the company by prospecting new customers;</li>
                                        <li>Direct communication with clients and partners, long-term business relationship building and upselling;</li>
                                        <li>Working collaboratively with the Marketing department and other members of the executive team to provide detailed market intelligence;</li>
                                        <li>Creating custom offers and solutions.</li>
                                    </ul>


                                    <p class="list-title">Requirements</p>
                                    <ul>
                                        <li>Excellent written and spoken English skills (C1/C2);</li>
                                        <li>At least 2 years of experience in sales (IT background - advantage);</li>
                                        <li>Good knowledge of IT industry;</li>
                                        <li>Ability to work independently as well as in a team;</li>
                                        <li>Fast learning and multitasking skills;</li>
                                        <li>Proficiency in German and/or Russian language (advantage);</li>
                                        <li>Sociable and enthusiastic.</li>
                                    </ul>

                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if(get_locale() == 'en_US'): ?>

                            <div class="item offer">
                                <a class="item-title" href="#" data-prevent data-toggle-active=""><i class="fa fa-plus"></i> <h4><?php _e('Senior Linux System Administrator')?></h4></a>
                                <div class="item-content">

                                    <p>
                                        <strong>Location:</strong> Kaunas, Lithuania
                                        <br/>
                                        <strong>Type:</strong> Full Time
                                        <br/>
                                        <strong>Language:</strong> English
                                    </p>

                                    <p>We are looking for a Senior Linux System Administrator to join our team in Kaunas, Lithuania.</p>

                                    <p class="list-title">Responsibilities</p>
                                    <ul>
                                        <li>Maintain and expand the global infrastructure;</li>
                                        <li>Work on deployment and monitoring improvements;</li>
                                        <li>Analyze kernel and software problems;</li>
                                        <li>Communicate with data center staff;</li>
                                        <li>Advice on technology, work on automation and strategic projects;</li>
                                        <li>Join a rotation based on-call duty (sms/email) to resolve technical problems during off-hours.</li>
                                    </ul>


                                    <p class="list-title">Requirements</p>
                                    <ul>
                                        <li>Expert / Advanced level in Linux and/or BSD management;</li>
                                        <li>Good understanding of Linux kernel internals;</li>
                                        <li>Able to compile and deploy kernels, loadable modules, etc.;</li>
                                        <li>Good understanding of network protocols and topologies;</li>
                                        <li>Good systemd understanding;</li>
                                        <li>Proficient in LAMP stack;</li>
                                        <li>Deep understanding of virtualization technologies (hypervisors, containers);</li>
                                        <li>Experience building scalable, highly available or mission critical virtual infrastructures;</li>
                                        <li>Ability to write automation scripts;</li>
                                        <li>Strong analytical thinking and problem-solving skills;</li>
                                        <li>Excellent written and spoken English skills.</li>
                                    </ul>

                                </div>
                            </div>

                        <?php endif; ?>

                        </div>
                    </div>

                </div>

                <div class="content-block get-in-touch">
                    <span class="heading"><?php _e('Don\'t see your position here?')?></span>
                    <span class="content"><?php printf(__('We are always looking for talented people! %sGet in touch%s and we\'ll invite you for a cup of coffee!'), '<a href="mailto:jobs@host1plus.com">', '</a>')?></span>
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .positions -->


        <?php if($internship_form_id):?>

        <section class="internships">

            <div class="container">

                <div class="scroll-id-target" id="internships"></div>

                    <div class="section-header">
                        <h2 class="block-title"><?php _e('Internships')?></h2>
                    </div>

                    <p class="title-text">
                        <?php _e( 'Fulfill your talent and potential! We are looking for young professionals who desire real-life experience. Kick off an amazing career before you graduate! Work on real projects with an experienced team and gain new skills.')?>
                    </p>

                <div class="form-block">

                    <h4 class="form-title"><?php _e('Apply now!')?></h4>
                    <span class="note"><?php _e( '* fields are required')?></span>

                    <?php echo do_shortcode('[contact-form-7 id="'.$internship_form_id.'" title="Internships" html_class="wpcf7-form captcha"]');?>

                    <script type="text/javascript">

                        jQuery('select[name="location"]').find('option[value="London, UK"], option[value="Frankfurt, Germany"]').prop('disabled', true);

                    </script>

                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .internships -->

    </section> <!-- end of .main -->

    <?php endif;?>

</article>

<?php

universal_redirect_footer([
    'en' => $site_en_url.'/jobs/',
    'br' => $site_br_url.'/jobs/'
]);

get_footer(); 

?>

