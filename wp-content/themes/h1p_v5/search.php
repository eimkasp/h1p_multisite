<?php
/**
 * @package WordPress
 * @subpackage h1p_v4
 *
 *  Template Name: google search
 *
*/


get_header();

// no sanitization needed. Google search plugin takes over the query

?>

<div class="page search">

    <section class="headline search">

        <div class="container">

            <h1 class="page-title"><?php _e('Site Search')?></h1>
            <p class="title-descr"><?php _e('Insert keywords and find all related information you need.')?></p>

        </div>

    </section>

    <section>

        <div class="container">

            <div class="section-content">

                <div class="block-search">

                    <gcse:search>
                        <div style="text-align:center;"><i class="fa fa-spinner fa-spin" style="font-size:40px;"></i></div>
                    </gcse:search>

                </div>

            </div>

        </div>

    </section>


<script>
    (function() {
        var cx = '000874991297156403490:s9qurf8tggc';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//www.google.com/cse/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
    })();

    $(window).bind('load', function() {
        $('.gsc-search-button').click( function(){
            $('.supplementary-links').hide();
        });
        $('.gsc-input').keyup( function(e){
            if(e.which === 13){
                $('.supplementary-links').hide();
             }
        });
    });
</script>

<?php

if( !empty( $_GET['q'] ) ){
    echo '<script>$(window).bind("load", function() { $(".gsc-search-button").click(); });</script>';
}

    // If search page is accessed directly without search string, let's show suggestions
?>

    <section class="supplementary-links">

        <div class="container">

            <div class="block-title-strikethrough-close"></div>

            <p class="description center p-b-30"><?php _e('Search our online resources and find the information you need'); ?></p>

            <div class="row">
                <div class="info-block">
                    <h3><?php _e('Tutorials');?></h3>
                    <p><?php _e('Check out more than 700 professional tutorials.'); ?></p>
                    <p><a class="m-t-20 info-block-link" href="<?php echo $config['links']['tutorials']?>"><?php _e('Explore')?> <i class="fa fa-angle-right"></i></a></p>
                </div>
                <div class="info-block">
                    <h3><?php _e('Knowledge base');?></h3>
                    <p><?php _e('Quickly find detailed information about our service.'); ?></p>
                    <p><a class="m-t-20 info-block-link" href="<?php echo $config['links']['knowledge_base']?>"><?php _e('Explore')?> <i class="fa fa-angle-right"></i></a></p>
                </div>
                <div class="info-block">
                    <h3><?php _e('Blog');?></h3>
                    <p><?php _e('Read latest company news and discover industry trends.'); ?></p>
                    <p><a class="m-t-20 info-block-link" href="<?php echo $config['links']['blog']?>"><?php _e('Explore')?> <i class="fa fa-angle-right"></i></a></p>
                </div>
            </div>

        </div> <!-- end of .page-content-supplementary -->

    </section>

</div>

<?php

get_footer();

?>
