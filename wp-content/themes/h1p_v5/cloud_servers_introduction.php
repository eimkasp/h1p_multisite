<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Cloud Servers
*/

wp_enqueue_style('oswald-font', 'https://fonts.googleapis.com/css?family=Oswald:700', false);

if(get_locale() == 'pt_BR') {
    $lang_code = 'pt-BR';
} else {
    $lang_code = 'en';
}
wp_enqueue_script('recaptcha', 'https://www.google.com/recaptcha/api.js?&hl=' . $lang_code, [], false, true);
wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);
wp_enqueue_script('soon', get_template_directory_uri() . '/js/vendor/soon.min.js', ['jquery'], false, true);

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

get_header();

?>

<article class="page landing cloud-servers">

    <header class="headline cloud-servers">
        <div class="container adjust-vertical-center">
            <h1 class="page-title">
                <img class="center display-block p-b-5" width="120" src="<?php echo $images ?>img/features/cloud/cloud_computing_icon.svg" alt="Cloud Computing">
                <?php _e('Discover Cloud Servers')?>
            </h1>
            <div class="title-descr"><?php _e('Cloud computing made simple.')?></div>
            <div class="row center">

                <div style="display:none;" id="soon-message">
                    <p class="white p-t-30"><?php _e('Time has run out. Please refresh the page.'); ?></p>
                </div>
                <div class="soon" id="cloud-servers-counter"
                     data-due="2016-09-05T12:27:00"
                     data-layout="group tight label-uppercase label-small"
                     data-format="d,h,m,s"
                     data-separator="."
                     data-face="flip color-light corners-sharp"
                     data-labels-days="<?php _e('Days'); ?>"
                     data-labels-hours="<?php _e('Hours'); ?>"
                     data-labels-minutes="<?php _e('Minutes'); ?>"
                     data-labels-seconds="<?php _e('Seconds'); ?>"
                     data-event-complete="showMessage">
                </div>

            </div>


        </div>
    </header> <!-- end of .headline.cloud-servers -->


    <section class="main page-content">


        <section class="service-features extra-pad">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Core Features')?></h2>
                    <span class="title-follow-text"><?php _e('Essential functionality covered by a single service.')?></span>
                </div>

                <div class="features-list">
                    <table class="cloud-core-features">
                        <tbody>
                            <tr>
                                <td colspan="2">

                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="title"><?php _e('Powerful API') ?></div>
                                                    <div class="content">
                                                        <?php _e('Our simple API allows you to automate and customize your server setups. Access web control panel functionality directly and unlock advanced service features.') ?>
                                                    </div>
                                                </td>
                                                <td style="padding-left:30px;">
                                                    <img class="push-right" width="99" src="<?php echo $images ?>img/features/cloud/powerful_api.svg" alt="Powerful API">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td rowspan="2" class="flist">
                                    <div class="title"><?php _e('Simplicity. Power. Control.') ?></div>
                                    <div class="content">
                                        <?php _e('Unlock extra functionality for your environment built on high-end hardware and high-speed network.') ?>
                                    </div>
                                    <div class="content">
                                        <ul>
                                            <li><?php _e('IPv6 support') ?></li>
                                            <li><?php _e('DNS management') ?></li>
                                            <li><?php _e('Intel® Xeon® processors E5 v3') ?></li>
                                            <li><?php _e('DDR4 error correcting-code RAM') ?></li>
                                            <li><?php _e('Geolocated IPs') ?></li>
                                            <li><?php _e('rDNS control') ?></li>
                                            <li><?php _e('Live stats') ?></li>
                                            <li><?php _e('10G network') ?></li>
                                        </ul>
                                        <div class="">
                                            <strong><?php _e('More coming soon!') ?></strong>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img class="center display-block p-b-20" width="90" src="<?php echo $images ?>img/features/cloud/os.svg" alt="OS">
                                    <div class="title center"><?php _e('Windows and Linux') ?></div>
                                    <div class="content center">
                                        <?php _e('Pick the OS that you love. With pre-installed Windows and Linux servers we are ready to deploy your virtual machine in seconds.') ?>
                                    </div>
                                </td>
                                <td>
                                    <img class="center display-block p-b-20" width="85" src="<?php echo $images ?>img/features/cloud/kvm.svg" alt="KVM">
                                    <div class="title center"><?php _e('KVM virtualization') ?></div>
                                    <div class="content center">
                                        <?php _e('The most desired Linux hypervisor with Windows support, high stability, immaculate performance and solid security.') ?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div> <!-- end of .container -->

        </section>

        <div class="scroll-id-target" id="early-access"></div>
        <section class="service-early-acccess extra-pad-top color-bg-style4">


            <div class="container">

                <div class="content-block p-v-20">

                    <h2 class="block-title white center p-b-10"><?php _e('Registration for early access is over.'); ?></h2>
                    <p class="title-follow-text white center"><?php _e('While we\'re brushing up before the launch, sign up to get special offers for Cloud Servers!'); ?></p>

                    <form class="p-t-30 p-b-40" method="post" action="/subscribe" id="frmSS6" onsubmit="emailSubscr(event,$(this))">

                        <div class="block-subscription-email">
                            <input type="text" name="email" value="" placeholder="<?php _e('Email address');?>">
                            <input type="hidden" name="format" value="h">
                        </div>
                        <div class="block-subscription-submit">
                            <input class="button highlight" type="submit" value="<?php _e('Sign Up'); ?>">
                        </div>

                    </form>

                    <div id="subscription-response" class="block-subscription-response" data-block-subscription-response></div>

                </div> <!-- end of .content-block -->

            </div> <!-- end of .container -->

        </section>

        <section class="service-features extra-pad">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Clever Service Delivery'); ?></h2>
                </div>

                <div class="features-list2">
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/features/mono/blue_quick_deployment.svg" alt="Clock">
                                    </td>
                                    <td class="title">
                                        <?php _e('Quick Deployment')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('Create and deploy your virtual machine in seconds.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/features/mono/blue_multiple_payment.svg" alt="Payments">
                                    </td>
                                    <td class="title">
                                        <?php _e('Multiple Payment Methods')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('We offer the most popular payment options - PayPal, Credit Card, Alipay, Paysera, Bitcoin, Skrill and Ebanx.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/features/mono/blue_unlimited_scalability.svg" alt="Scalability">
                                    </td>
                                    <td class="title">
                                        <?php _e('Unlimited Scalability')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('No longer bound by the physical size of your server, quickly scale your virtual machine by adding or reducing your resources.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/features/mono/blue_flexible_customer_support.svg" alt="Support">
                                    </td>
                                    <td class="title">
                                        <?php _e('Flexible Customer Support')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('Benefit from multi-level technical assistance and quick issue resolution.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/features/mono/blue_monthly_billing.svg" alt="Billing">
                                    </td>
                                    <td class="title">
                                        <?php _e('Monthly Billing')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('Pay month-to-month without long-term commitments.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/features/mono/blue_easy_management.svg" alt="Management">
                                    </td>
                                    <td class="title">
                                        <?php _e('Easy Management')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('Our internal UI is built for convenient service, payment, ticket and feedback management.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </section>

        <section class="ask-questions extra-pad color-bg-style1">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask a question!')?></h2>
                    <p><?php _e('We\'d love to hear how we can help you. Tell us about your project and we\'ll get back to you in minutes.'); ?></p>
                </div>

                <div id="get_in_touch_form" class="contact-form row center" style="display:none;">
                    <?php echo do_shortcode('[contact-form-7 id="18740" title="Contact Sales" html_class="wpcf7-form captcha"]');?>
                </div>

                <div class="row center">
                    <button id="get_in_touch" class="button primary orange large"><?php _e('Get in Touch');?></button>
                </div>

            </div>

        </section>


        <section class="service-features extra-pad">

            <div class="container">

                <div class="section-header p-b-40">
                    <h2 class="block-title"><?php _e('Looking for something else?')?></h2>
                    <span class="title-follow-text"><?php _e('Deploy your VPS straight away')?>.</span>
                </div>

                <div class="container-flex">
                    <div class="block-fluid">
                        <ul class="features-list checklist orange">
                            <li><?php _e('OpenVZ virtualization')?></li>
                            <li><?php _e('5 locations worldwide')?></li>
                        </ul>
                    </div>
                    <div class="block-fluid">
                        <ul class="features-list checklist orange">
                            <li><?php _e('Intel® Xeon® processors')?></li>
                            <li><?php _e('500 Mbps uplink')?></li>
                        </ul>
                    </div>
                    <div class="block-fluid">
                        <ul class="features-list checklist orange">
                            <li><?php _e('IPv4 and IPv6 support')?></li>
                            <li><?php _e('2 free backups')?></li>
                        </ul>
                    </div>
                </div>

                <div class="row center p-t-40">
                    <a href="<?php echo $config['links']['vps-hosting']?>" class="button primary orange large m-t-40"><?php _e('View Plans');?></a>
                </div>

            </div> <!-- end of .container -->

        </section>



    </section>

</article>

<script>
    $(document).ready(function(){
        setInterval(function(){
            $('#cloud-servers-counter').soon().redraw();
        },50);
    })
    function showMessage() {
        document.getElementById('soon-message').style.display = '';
        //document.getElementById('soon-counter').style.display = 'none';
    }
</script>

<?php get_footer(); ?>
