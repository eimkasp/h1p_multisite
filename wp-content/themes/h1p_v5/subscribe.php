<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Subscription proxy
*/

/*Including MailChimp API helper class*/

    require_once ( __DIR__ . '/libs/MailChimp.php' );
    use \DrewM\MailChimp\MailChimp;

/*
============================================
Tasks: 
+ Form validation
+ Work out the response
+ Link to MailChimp API, with settings.
+ Create return values
+ HTTP_REFERER solution: $_SESSION ???
+ Language param passing
+ Subscribe origin registration to MailChimp
+ Manage API responses and mandatory logic
- Clear error messages when user got the info
- Styling: make RWD blocks
- Error handling for promise.catch

*/




$referer = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
// ($_SERVER['HTTP_REFERER'] works only if this page is called via Ajax

$result = 'error';

if ( ( strpos($referer, 'host1plus') !== false ) &&
    isset($_POST) &&
    isset($_POST['email'])
) {

    /*echo 'Success:: ';
    var_dump($_POST);*/

    $email = $_POST['email'];
    $signup_source = $_POST['signup_source'];
    $first_name = $_POST['first-name'];
    $last_name = $_POST['last-name'];

    /* Settings for MailChimp API 
       --------------------------
    */

    define("MAILCHIMPAPIKEY", "c9741169927eba8b3a7a8a577cfcb6ad-us14"); //MailChimp apikey
    $list_id = "5c10c3445e";    //MailChimp list id

    $mcapi = new MailChimp( MAILCHIMPAPIKEY );

    //$result = $mcapi->get('lists');


    /* Set language info
       -----------------
    */
    $page_locale = get_locale();
    if ($locale == 'en_US') {
        $member_language = 'english';
    } elseif ($locale == 'pt_BR') {
        $member_language = 'portuguese-br';
    } else {
        $member_language = 'unspecified';
    }
    

    /* Sanitize collected data
       -----------------------
    */

    $sanitized_first_name = sanitize_text_field($first_name);
    $sanitized_last_name = sanitize_text_field($last_name);
    $sanitized_email = sanitize_email($email);

    /* Call API
       --------
    */

    $result = $mcapi->post("lists/$list_id/members", [
                    'email_address' => $sanitized_email,
                    'status'        => 'pending',
                    'merge_fields' => [
                        'FNAME' => $sanitized_first_name,
                        'LNAME' => $sanitized_last_name,
                        'CL_GROUP' => 'none',
                        'CL_DIRECT' => 0,
                        'CL_ACTIVE' => 0,
                        'SPEAK_LANG' => $member_language,
                        'S_SOURCE' => $signup_source
                    ]
                ]);

    /* Construct JSON response
       -----------------------
    */

    $response = '';

    if ($mcapi->success()) {
        // user was subscribed to the list
        $response = array ('success' => 1, 'details' => __('Thank you!') . ' ' . __('Please check your email to confirm your subscription.') ); 

    } else {
        // API returned an error
        $error_result = $mcapi->getLastError();

        // Producing human-friendly error messages
        switch ($result['title']) {
            case 'Member Exists':
                $response_details = __('You are already subscribed to our mailing list.') . ' ' . __('Please check your email to confirm your subscription.');
                break;
            default:
                $response_details = __('Uknown error occured.');

        }

        $response = array ('success' => 0, 'details' => $response_details );
    }

    //var_dump ($result);

    header('Content-Type: application/json');
    echo json_encode($response);

    // NOTE: the Ajax call will reject if json format will be invalid (if used vardump, etc.)

}

?>
