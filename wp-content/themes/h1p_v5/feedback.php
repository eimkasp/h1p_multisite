<?php

/**
 * @package WordPress
 * @subpackage h1p_v4
 */
/*
  Template Name: Feedback
 */

get_header();
?>


<section class="block feedback">
    <div class="container">
        <div class="row">
            <form action="" method="POST">
                <h2 class="block-title">Leave your feedback here!</h2>
                <h4 class="block-subtitle">Lorem Ipsum is simply dummy text of the printing  and typesetting industry.</h4>
                <div class="block-content">

                    <div class="feedback-service">
                        <div class="feedback-block-title">Select the service you were using:</div>

                        <div class="feedback-content">

                            <div class="bigcheckboxes left">
                                <ul>
                                    <li>
                                        <label class="bigcheck">
                                            <input type="checkbox" class="bigcheck" name="sharedhosting" value="yes"/>
                                            <span class="bigcheck-target"></span>
                                            Shared Hosting
                                        </label>
                                    </li>
                                    <li>
                                        <label class="bigcheck">
                                            <input type="checkbox" class="bigcheck" name="vpshosting" value="yes"/>
                                            <span class="bigcheck-target"></span>
                                            VPS Hosting
                                        </label>
                                    </li>
                                    <li>
                                        <label class="bigcheck">
                                            <input type="checkbox" class="bigcheck" name="cloudhosting" value="yes"/>
                                            <span class="bigcheck-target"></span>
                                            Cloud Hosting
                                        </label>
                                    </li>
                                </ul>
                            </div>

                            <div class="bigcheckboxes">
                                <ul>
                                    <li>
                                        <label class="bigcheck ">
                                            <input type="checkbox" class="bigcheck" name="resellerhosting" value="yes"/>
                                            <span class="bigcheck-target"></span>
                                            Reseller Hosting
                                        </label>
                                    </li>
                                    <li>
                                        <label class="bigcheck">
                                            <input type="checkbox" class="bigcheck" name="dedicatedservers" value="yes"/>
                                            <span class="bigcheck-target"></span>
                                            Dedicated Servers
                                        </label>
                                    </li>
                                    <li>
                                        <label class="bigcheck">
                                            <input type="checkbox" class="bigcheck" name="other" value="yes"/>
                                            <span class="bigcheck-target"></span>
                                            Other
                                        </label>
                                    </li>
                                </ul>
                            </div>

                        </div>

                    </div>

                    <div class="feedback-service">
                        <div class="feedback-block-title">Rate your overall experience:</div>
                        <div class="feedback-content">

                            <script>
                                $(document).ready(function () {
                                    //  Check Radio-box
                                    $(".rating input:radio").attr("checked", false);
                                    $('.rating input').click(function () {

                                        var group = $(this).attr('name');
                                        var selectedVal = $(this).attr('value');

                                        $(".rating span." + group).removeClass('checked');

                                        $("input[name$='" + group + "']").each(function (i) {
                                            if (selectedVal >= $(this).attr('value')) {
                                                $(this).parent().addClass('checked');
                                            }
                                        });
                                    });
                                });
                            </script>


                            <div class="ratings-block">
                                <span class="rating-label">Features:</span>
                                <span class="rating">
                                    <span class="rating1"><input type="radio" name="rating1" id="str1-5" value="5"><label for="str1-5"></label></span>
                                    <span class="rating1"><input type="radio" name="rating1" id="str1-4" value="4"><label for="str1-4"></label></span>
                                    <span class="rating1"><input type="radio" name="rating1" id="str1-3" value="3"><label for="str1-3"></label></span>
                                    <span class="rating1"><input type="radio" name="rating1" id="str1-2" value="2"><label for="str1-2"></label></span>
                                    <span class="rating1"><input type="radio" name="rating1" id="str1-1" value="1"><label for="str1-1"></label></span>
                                </span>

                            </div>

                            <div class="ratings-block">
                                <span class="rating-label">Performance:</span>
                                <span class="rating">
                                    <span class="rating2"><input type="radio" name="rating2" id="str2-5" value="5"><label for="str2-5"></label></span>
                                    <span class="rating2"><input type="radio" name="rating2" id="str2-4" value="4"><label for="str2-4"></label></span>
                                    <span class="rating2"><input type="radio" name="rating2" id="str2-3" value="3"><label for="str2-3"></label></span>
                                    <span class="rating2"><input type="radio" name="rating2" id="str2-2" value="2"><label for="str2-2"></label></span>
                                    <span class="rating2"><input type="radio" name="rating2" id="str2-1" value="1"><label for="str2-1"></label></span>
                                </span>
                            </div>

                            <div class="ratings-block">
                                <span class="rating-label">Network Speed:</span>
                                <span class="rating">
                                    <span class="rating3"><input type="radio" name="rating3" id="str3-5" value="5"><label for="str3-5"></label></span>
                                    <span class="rating3"><input type="radio" name="rating3" id="str3-4" value="4"><label for="str3-4"></label></span>
                                    <span class="rating3"><input type="radio" name="rating3" id="str3-3" value="3"><label for="str3-3"></label></span>
                                    <span class="rating3"><input type="radio" name="rating3" id="str3-2" value="2"><label for="str3-2"></label></span>
                                    <span class="rating3"><input type="radio" name="rating3" id="str3-1" value="1"><label for="str3-1"></label></span>
                                </span>
                            </div>

                            <div class="ratings-block">
                                <span class="rating-label">Reliability:</span>
                                <span class="rating">
                                    <span class="rating4"><input type="radio" name="rating4" id="str4-5" value="5"><label for="str4-5"></label></span>
                                    <span class="rating4"><input type="radio" name="rating4" id="str4-4" value="4"><label for="str4-4"></label></span>
                                    <span class="rating4"><input type="radio" name="rating4" id="str4-3" value="3"><label for="str4-3"></label></span>
                                    <span class="rating4"><input type="radio" name="rating4" id="str4-2" value="2"><label for="str4-2"></label></span>
                                    <span class="rating4"><input type="radio" name="rating4" id="str4-1" value="1"><label for="str4-1"></label></span>
                                </span>
                            </div>

                            <div class="ratings-block">
                                <span class="rating-label">Support:</span>
                                <span class="rating">
                                    <span class="rating5"><input type="radio" name="rating5" id="str5-5" value="5"><label for="str5-5"></label></span>
                                    <span class="rating5"><input type="radio" name="rating5" id="str5-4" value="4"><label for="str5-4"></label></span>
                                    <span class="rating5"><input type="radio" name="rating5" id="str5-3" value="3"><label for="str5-3"></label></span>
                                    <span class="rating5"><input type="radio" name="rating5" id="str5-2" value="2"><label for="str5-2"></label></span>
                                    <span class="rating5"><input type="radio" name="rating5" id="str5-1" value="1"><label for="str5-1"></label></span>
                                </span>
                            </div>

                            <div class="ratings-block">
                                <span class="rating-label">Customer Care:</span>
                                <span class="rating">
                                    <span class="rating6"><input type="radio" name="rating6" id="str6-5" value="5"><label for="str6-5"></label></span>
                                    <span class="rating6"><input type="radio" name="rating6" id="str6-4" value="4"><label for="str6-4"></label></span>
                                    <span class="rating6"><input type="radio" name="rating6" id="str6-3" value="3"><label for="str6-3"></label></span>
                                    <span class="rating6"><input type="radio" name="rating6" id="str6-2" value="2"><label for="str6-2"></label></span>
                                    <span class="rating6"><input type="radio" name="rating6" id="str6-1" value="1"><label for="str6-1"></label></span>
                                </span>
                            </div>

                            <div class="ratings-block">
                                <span class="rating-label">Price:</span>
                                <span class="rating">
                                    <span class="rating7"><input type="radio" name="rating7" id="str7-5" value="5"><label for="str7-5"></label></span>
                                    <span class="rating7"><input type="radio" name="rating7" id="str7-4" value="4"><label for="str7-4"></label></span>
                                    <span class="rating7"><input type="radio" name="rating7" id="str7-3" value="3"><label for="str7-3"></label></span>
                                    <span class="rating7"><input type="radio" name="rating7" id="str7-2" value="2"><label for="str7-2"></label></span>
                                    <span class="rating7"><input type="radio" name="rating7" id="str7-1" value="1"><label for="str7-1"></label></span>
                                </span>
                            </div>

                        </div>
                    </div>

                    <div class="feedback-service">
                        <div class="feedback-block-title">How did we do ?</div>
                        <div class="feedback-content">
                            <textarea name="comment" rows="10" style="height: 200px;" placeholder="Enter your comment"></textarea>
                        </div>
                    </div>

                </div>
                <div class="block-footer">
                    <button type="submit" class="button primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

<?php

get_footer();
?>