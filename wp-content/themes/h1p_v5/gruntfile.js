module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        sass: {
            dist: {
                options: {
                    sourceMap: true,
                    outputStyle: 'nested'
                },
                files: {
                    'style.css': 'style.scss'
                }
            },
            landings: {
                options: {
                    sourceMap: false,
                    outputStyle: 'nested'
                },
                files: {
                    'landings2/landing.css': 'landings2/sass/landing2.scss'
                }
            }
        },

        cmq: {
            options: {
                log: true,
                ext: false
            },
            dist: {
                files: {
                    'style.css': ['style.css']
                }
            },
            landings: {
                files: {
                    'landings2/landing.css': ['landings2/landing.css']
                }
            }
        },


        cssmin: {
            options: {
                banner: '/* \nTheme Name: HOST1PLUS v5 \nAuthor: HOST1PLUS \nVersion: 0.0.1 \n*/'
            },
            dist: {
                files: {
                    'style.css': ['style.css']
                }
            },
            landings: {
                files: {
                    'landings2/landing.css': ['landings2/landing.css']
                }
            }
        },

        watch: {
            options: {
                interval: 3007
            },
            scss: {
                options: {
                    livereload: true,
                    spawn: false
                },
                files: '**/*.scss',
                tasks: ['sass']
            },
            css: {
                options: {
                    livereload: true,
                    spawn: true
                },
                files: ['style.css']
            },
            php: {
                options: {
                    livereload: true,
                    spawn: true
                },
                files: '**/*.php'

            },
            js: {
                options: {
                    livereload: true,
                    spawn: true
                },
                files: '**/*.js'
            }
        },

        concat: {
            js: {
                src: ['js/build/*.js', '!js/build/h1p.js', '!js/build/h1p.min.js'],
                dest: 'js/build/h1p.js'
            }
        },

        uglify: {
            js: {
                src: ['js/build/h1p.js'],
                dest: 'js/build/h1p.min.js'
            }
        },

        svgstore: {
            options: {
                inheritviewbox: true,
                prefix : 'icon-', // This will prefix each ID
                includedemo: true
            },
            default: {
                files: {
                    'svgdone/svg-icons.svg': ['svgprep/*.svg'],
                },
            },
        },

        perfbudget: {
            default: {
                options: {
                    url: 'http://www.host1plus.com',
                    key: 'A.dd6497b76131da3b5722f4ebdfcdd2e4',
                    runs: 3,
                    budget: {
                        render: '3000',
                        visualComplete: '5000',
                        SpeedIndex: '4500'
                    }
                }
            }
        },

        // Remove unused CSS across multiple files and ignore specific selectors
        uncss: {
            dist: {
                options: {
                    ignore: ['#added_at_runtime', '.created_by_jQuery']
                },
                files: {
                    'dist/css/tidy.css': ['**/*.php', 'htmlblocks/*.php']
                }
            }
        },

        potomo: {
            build: {
                options: {
                    poDel: false
                },
                files: [{
                    expand: true,
                    cwd: 'languages',
                    src: ['*.po'],
                    dest: 'languages',
                    ext: '.mo',
                    nonull: true
                }]
            }
        }

    });

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-combine-media-queries');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-perfbudget');
    grunt.loadNpmTasks('grunt-uncss');
    grunt.loadNpmTasks('grunt-svgstore');
    grunt.loadNpmTasks('grunt-potomo');

    grunt.registerTask('default', ['watch']);
    grunt.registerTask('test', ['perfbudget']);
    grunt.registerTask('buildcss', ['sass:dist', 'cmq:dist', 'cssmin:dist']);
    grunt.registerTask('buildlandings', ['sass:landings', 'cmq:landings', 'cssmin:landings']);
    grunt.registerTask('buildjs', ['concat', 'uglify']);
    grunt.registerTask('buildlang', ['potomo']);
    grunt.registerTask('build', ['sass', 'cmq', 'cssmin', 'concat', 'uglify', 'potomo']);
    grunt.registerTask('uncss', ['uncss']); // neveikia normaliai
    grunt.registerTask('buildlang', ['potomo']);

}
