<?php
/**
 * @package WordPress
 * @subpackage h1p_v4
 *
 *  Template Name: Tools
 */

get_header();

?>

<div class="page tools">

    <section class="image-header">
        <div class="container">
            <div class="title"><?php _e('Tools')?></div>
            <div class="title-descr"><?php _e('Flawless performance without struggle!')?></div>
            <div class="descr-text"><?php _e('Enjoy the most popular software for easier online project management! Stay safe, ease your work and get things done faster!')?></div>
            <div class="menu">
                <a class="item" href="#stop-the-hacker" data-scrollto="stop-the-hacker"><?php _e('StopTheHacker')?></a>
                <a class="item" href="#spam-experts" data-scrollto="spam-experts"><?php _e('SpamExperts')?></a>
                <a class="item" href="#softaculous" data-scrollto="softaculous"><?php _e('Softaculous')?></a>
            </div>
        </div>
    </section>



    <section class="page-content">
        <div class="container">

            <div class="page-path">
                <span class="text-item"><?php _e('Home')?></span>
                <i class="fa fa-angle-right text-item"></i>
                <span class="text-item active"><?php _e('Tools')?></span>
            </div>



            <?php $plans = $config['products']['tools']['stopthehacker']; ?>
            <section class="tool stop-the-hacker">

                <div class="scroll-id-target" id="stop-the-hacker"></div>

                <div class="section-header">
                    <div class="section-title">
                        <h2 class="title"><?php _e('StopTheHacker')?></h2>
                        <p class="description"><?php _e('Detect malware in time and protect your reputation with a set of modern security technologies.')?></p>
                    </div>
                    <div class="pricing">
                        <span class="price"><span data-select-value="selectedprice-stopthehacker"><?php echo $whmcsPrices[$plans['basic']['id']]['price']?></span><span class="sub full"><?php _e('per month')?></span></span>
                        <form action="<?php echo $config['whmcs_links']['checkout']?>" method="GET" class="order-form">

                            <input type="hidden" name="a" value="add"/>
                            <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                            <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                            <input type="hidden" name="promocode" value="none"/>

                            <select name="pid" class="product-options" data-select="selectedprice-stopthehacker">
                                <?php foreach($plans as $key => $plan):?>
                                <option value="<?php echo $plan['id']?>" data-value="<?php echo $whmcsPrices[$plan['id']]['price']?>" <?php if($key == 'basic'):?>selected="selected"<?php endif;?>><?php echo $plan['title']?> - <?php echo $whmcsPrices[$plan['id']]['price']?><?php _e('/month')?></option>
                                <?php endforeach;?>
                            </select>

                            <span class="append">
                                <button type="submit" class="button primary"><?php _e('Order Now')?></button>
                            </span>
                        </form>
                    </div>
                    <i class="icon tool-logo stop-the-hacker"></i>
                </div>

                <div class="plans">

                    <span class="plans-title"><?php _e('Malware Protection')?></span>

                    <div class="plans-table">

                        <div class="plan">
                            <span class="plan-title">Basic</span>
                            <div class="plan-description">
                                <ul class="inside-out">
                                    <li><?php _e('Reputation and Blacklist Monitoring')?></li>
                                    <li><?php _e('Standard Malware Detection')?></li>
                                    <li><?php _e('Scan Frequency')?>: <span class="highlight"><?php _e('Weekly')?></span></li>
                                </ul>
                            </div>
                        </div>

                        <div class="plan">
                            <span class="plan-title">Professional</span>
                            <div class="plan-description">
                                <ul class="inside-out">
                                    <li><?php _e('Basic Features + Advanced Malware Detection')?></li>
                                    <li><?php _e('Server-Side Malware Detection')?></li>
                                    <li><?php _e('Trust Seal')?></li>
                                    <li><?php _e('Speed Monitoring')?></li>
                                    <li><?php _e('Uptime Monitoring')?></li>
                                    <li><?php _e('Scan Frequency')?>: <span class="highlight"><?php _e('Daily')?></span></li>
                                </ul>
                            </div>
                        </div>

                        <div class="plan">
                            <span class="plan-title">Business</span>
                            <div class="plan-description">
                                <ul class="inside-out">
                                    <li><?php _e('Professional Package Features')?></li>
                                    <li><?php _e('Application Vulnerability Assessment')?></li>
                                    <li><?php _e('Server Vulnerability Assessment')?></li>
                                    <li><?php _e('Scan Frequency')?>: <span class="highlight"><?php _e('Hourly')?></span></li>
                                </ul>
                            </div>
                        </div>

                        <div class="plan">
                            <span class="plan-title">Business Plus</span>
                            <div class="plan-description">
                                <ul class="inside-out">
                                    <li><?php _e('Business Package Features')?></li>
                                    <li><?php _e('Annual Security Audit')?></li>
                                    <li><?php _e('Scan Frequency')?>: <span class="highlight"><?php _e('Continuously')?></span></li>
                                </ul>
                            </div>


                        </div>

                    </div>

                    <div class="tool-image"><i class="icon tool-box stop-the-hacker"></i></div>

                </div>

                <div class="features">
                    <div class="accordion col-left">
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('Automatic Malware Cleanup')?></a>
                            <div class="item-content"><?php _e('Get rid of malware before you suffer disruptions and data loss. If your website has malware on it, StopTheHacker automated system will remove it for you. Select the cleanup scale yourself – check and detect malware, decide whether to apply the fixes automatically or manually. The automatic malware cleanup comes with a backup feature, creating a backup of the original file, before cleaning it. If something goes wrong, you can always roll back.')?></div>
                        </div>
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('External Link Scan')?></a>
                            <div class="item-content"><?php _e('StopTheHacker scans both - the content on your website and other interlinked websites. External link scan ensures the security of your visitors and prevents them from being attacked while following your links. Get full information and effectively control your content to maintain your reputation and clients’ trust.')?></div>
                        </div>
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('Blacklist and Reputation Monitoring')?></a>
                            <div class="item-content"><?php _e('Blacklist Monitoring is a comprehensive daily check on the status of your website on the Google Safe Browsing List and other search engines including Yahoo, and Bing; malware blacklists like Malware Patrol and Malware URL; DNS Blacklists; phishing blacklists, spam blacklists and many more. If your website ends up on a blacklist, you get immediate notifications and professional help on the spot.')?></div>
                        </div>
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('.htaccess Hack Detection')?></a>
                            <div class="item-content"><?php _e('StopTheHacker’s scanner detects the alterations of your .htaccess file and is aware if the visitors of your site are redirected to other malicious domains. The auto cleanup system quickly fixes these issues without any manual intervention.')?></div>
                        </div>
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('StopTheHacker Trust Seal')?></a>
                            <div class="item-content"><?php _e('Secure your website or online store with StopTheHacker and show off! StopTheHacker’s Trust Seal increases the confidence in your site. Show your visitors and clients that you care about their safety and protection.')?></div>
                        </div>
                    </div>

                    <div class="accordion col-right">
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('Fully Automated Scans + Email Alerts')?></a>
                            <div class="item-content"><?php _e('Select the frequency of automated scans according to your preferences – get it done weekly, daily, hourly or even continuously and get rid of thousands of threats in time. Timely email alerts will always keep you up with the latest status of your website!')?></div>
                        </div>
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('Vulnerability Assessment')?></a>
                            <div class="item-content"><?php _e('Vulnerability scans prevent web-based intrusions and identify server and application security vulnerabilities. Check over 35,000 vulnerabilities on your servers, website and infrastructure. Stay confident as the vulnerabilities are detected anywhere - including custom installations, as well as the most popular CMS, like WordPress, Drupal, Joomla, and other apps.')?></div>
                        </div>
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('Speed Monitoring')?></a>
                            <div class="item-content"><?php _e('Receive real-time information about the page speed and response time of your website and ease your analysis with numerous detailed graphs.')?></div>
                        </div>
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('Uptime Monitoring')?></a>
                            <div class="item-content"><?php _e('Uptime Monitoring generates an overview of the availability of your website including details on down time. Never miss a thing – automated email alerts are sent every time the site goes down.')?></div>
                        </div>
                    </div>
                </div>

                <a class="toggle-features-link" href="javascript:" data-toggle-active="parent">
                    <span class="open"><?php _e('Collapse Features')?> <i class="fa fa-caret-up"></i></span>
                    <span class="closed"><?php _e('View More Features')?> <i class="fa fa-caret-down"></i></span>
                </a>

            </section>

            <?php
            $plan = $config['products']['tools']['spamexperts'];
            $planPrices = $whmcs->getPriceOptions($plan['id']);
            $selectedCycle = 3;
            $selectedPrice = $whmcs->getPrice($plan['id'], $selectedCycle, 'price_monthly');
            ?>
            <section class="tool spam-experts">

                <div class="scroll-id-target" id="spam-experts"></div>

                <div class="section-header">
                    <div class="section-title">
                        <h2 class="title"><?php _e('Spam Experts')?></h2>
                        <p class="description"><?php _e('Professional anti-spam software efficiently preserves your network from spam, viruses and malware.')?></p>
                    </div>
                    <div class="pricing">
                        <span class="price"><span data-select-value="selectedprice-spamexperts"><?php echo $selectedPrice;?></span><span class="sub full"><?php _e('per month')?></span></span>

                        <form action="<?php echo $config['whmcs_links']['checkout']?>" method="GET" class="order-form">

                            <input type="hidden" name="a" value="add"/>
                            <input type="hidden" name="pid" value="<?php echo $plan['id']?>"/>
                            <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                            <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                            <input type="hidden" name="promocode" value="none"/>

                            <select name="billingcycle" class="product-options" data-select="selectedprice-spamexperts">
                                <?php foreach($planPrices as $price):?>
                                    <option value="<?php echo $price['cycle']?>" data-value="<?php echo $price['price_monthly'];?>"<?php if($price['months'] == $selectedCycle):?> selected="selected"<?php endif;?>>
                                        <?php
                                        switch(get_numeric_case($price['months'])){
                                            case 0:
                                                echo $price['months'] . ' ' . _x_lc('months', '0 months') . ' - ' . $price['price_monthly'] . ' ' . __('/month');
                                                break;
                                            case 1:
                                                echo $price['months'] . ' ' . _x_lc('month', '1 month') . ' - ' . $price['price_monthly'] . ' ' . __('/month');
                                                break;
                                            case 2:
                                                echo $price['months'] . ' ' . _x_lc('months', '2 months') . ' - ' . $price['price_monthly'] . ' ' . __('/month');
                                                break;
                                        }
                                        ?>
                                    </option>
                                <?php endforeach;?>
                            </select>

                            <span class="append">
                                <button type="submit" class="button primary"><?php _e('Order Now')?></button>
                            </span>
                        </form>
                    </div>
                    <i class="icon tool-logo spam-experts"></i>
                </div>

                <div class="highlights">

                    <div class="highlights-lists">

                        <ul class="highlights-list checklist green left">
                            <li><?php _e('Automated Protection')?></li>
                            <li><?php _e('Improved Business Productivity')?></li>
                            <li><?php _e('Lower Bandwidth Consumption')?></li>
                        </ul>

                        <ul class="highlights-list checklist green right">
                            <li><?php _e('Full-Time Monitoring')?></li>
                            <li><?php _e('Reduced In-House IT Resource')?></li>
                            <li><?php _e('And Much More!')?></li>
                        </ul>

                    </div>

                    <div class="tool-image"><i class="icon tool-box spam-experts"></i></div>

                </div>

                <div class="features">
                    <div class="accordion col-left">
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('All-in-One')?></a>
                            <div class="item-content"><?php _e('We are happy to assist our clients and protect them against spam with the help of SpamExperts’ professional email security solutions and superb functionality. A full-pack of helpful and easy-to-use tools ensures a spam-free environment in just a few steps.')?></div>
                        </div>
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('Full Integration')?></a>
                            <div class="item-content"><?php _e('Benefit from free integration and automation plug-ins for cPanel, Plesk, Parallels Automation, DirectAdmin and more. Spam Experts API supports integration into any hosting infrastructure with a single click!')?></div>
                        </div>
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('Domain-Based Prices')?></a>
                            <div class="item-content"><?php _e('You pay for what you use only, meaning that your expenses depend on the number of domains you own. Regardless of how much you pay, you always get premium quality spam protection and full customer support, as well as every other client of ours!')?></div>
                        </div>
                    </div>
                    <div class="accordion col-right">
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('Optimized Email Security')?></a>
                            <div class="item-content"><?php _e('Block spam, stop viruses and prevent from being blacklisted, with no extra adjustments and no distress! SpamExperts’ filtering accuracy tops 99.9% with extremely unlikely false positives. Fully automated updates ensure that your virus base keeps pace with the latest malware trends.')?></div>
                        </div>
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('Full Control')?></a>
                            <div class="item-content"><?php _e('SpamExperts offer a user-friendly control panel with extensive functionality for different user groups. It allows you to access quarantine, manage blacklists and whitelists, search logs and fully configure the spam filter according to your needs – exploit control over your emails and be confident that important messages won’t ever get lost.')?></div>
                        </div>
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('Increased Productivity')?></a>
                            <div class="item-content"><?php _e('Save your precious time – don’t deal with spam any longer and give your business a significant boost in efficiency. Forget spam forever - all SpamExperts’ products are under active development, featuring free upgrades, regular updates and new product releases pop up constantly.')?></div>
                        </div>
                    </div>
                </div>

                <a class="toggle-features-link" href="javascript:" data-toggle-active="parent">
                    <span class="open"><?php _e('Collapse Features')?> <i class="fa fa-caret-up"></i></span>
                    <span class="closed"><?php _e('View More Features')?> <i class="fa fa-caret-down"></i></span>
                </a>

            </section>


            <?php $plans = $config['products']['tools']['softaculous']; ?>
            <section class="tool softaculous">

                <div class="scroll-id-target" id="softaculous"></div>

                <div class="section-header">
                    <div class="section-title">
                        <h2 class="title"><?php _e('Softaculous')?></h2>
                        <p class="description"><?php _e('The leading auto-installer will ease every step while working with your online projects. Get your apps running in seconds!')?></p>
                    </div>
                    <div class="pricing">
                        <span class="price"><span data-select-value="selectedprice-softaculous"><?php echo $whmcsPrices[$plans['dedicated']['id']]['price']?></span><span class="sub full"><?php _e('/month')?></span></span>

                        <form action="<?php echo $config['whmcs_links']['checkout']?>" method="GET" class="order-form">

                            <input type="hidden" name="a" value="add"/>
                            <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                            <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                            <input type="hidden" name="promocode" value="none"/>

                            <select name="pid" class="product-options" data-select="selectedprice-softaculous">
                                <?php foreach($plans as $key => $plan):?>
                                <option value="<?php echo $plan['id']?>" data-value="<?php echo $whmcsPrices[$plan['id']]['price']?>" <?php if($key == 'dedicated'):?>selected="selected"<?php endif;?>><?php echo $plan['title']?> - <?php echo $whmcsPrices[$plan['id']]['price']?><?php _e('/month')?></option>
                                <?php endforeach;?>
                            </select>

                            <span class="append">
                                <button type="submit" class="button primary"><?php _e('Order Now')?></button>
                            </span>
                        </form>
                    </div>
                    <i class="icon tool-logo softaculous"></i>
                </div>

                <div class="highlights">

                    <div class="highlights-lists">

                        <ul class="highlights-list checklist green">
                            <li><?php _e('Over 100 Popular Apps')?></li>
                            <li><?php _e('Installation on Sub-Domains and Add-On Domains')?></li>
                        </ul>

                        <ul class="highlights-list checklist green">
                            <li><?php _e('Prompt App Removal')?></li>
                            <li><?php _e('One-Click Updates')?></li>
                        </ul>

                    </div>

                    <div class="tool-image"><i class="icon tool-box softaculous"></i></div>

                </div>

                <div class="features">
                    <div class="accordion col-left">
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('Easy Management')?></a>
                            <div class="item-content">
                                <?php _e('Softaculous is a superb auto-installer with more than 320 scripts and 1115 PHP classes. The scripts cover the most of web applications any customer may ever need. With this ultra-fast auto-installer, everyone can find the app to power their website and enjoy it in seconds.')?>
                            </div>
                        </div>
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('Quick Installation')?></a>
                            <div class="item-content">
                                <?php _e('Being ridiculously fast, Softaculous performs script installation in just one simple step! Don’t ever struggle with individual script installers and waste your time – Softaculous makes it happen in the blink of an eye.')?>
                            </div>
                        </div>
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('Direct Script Import')?></a>
                            <div class="item-content">
                                <?php _e('With Softaculous you can always import other scripts, even if they weren’t installed by the tool itself. Choose which data you need from other auto installers and afterwards Softaculous will maintain the script for future updates.')?>
                            </div>
                        </div>
                    </div>
                    <div class="accordion col-right">
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('Excellent Reputation')?></a>
                            <div class="item-content">
                                <?php _e('Softaculous is well-known for its immaculate reputation. Want to leave feedback about the script you have tried? The tool itself contains ratings and reviews of various scripts. Save your time and choose the script you need with less effort.')?>
                            </div>
                        </div>
                        <div class="feature item">
                            <a class="item-title" href="javascript:" data-toggle-active><i class="fa fa-plus"></i> <?php _e('High Security')?></a>
                            <div class="item-content">
                                <?php _e('Get quick script updates and stay confident! Your scripts will always stay safe and run perfectly. New updates are available right after they are released by developers, so every user benefits from new features the very moment.')?>
                            </div>
                        </div>
                    </div>
                </div>

                <a class="toggle-features-link" href="javascript:" data-toggle-active="parent">
                    <span class="open"><?php _e('Collapse Features')?> <i class="fa fa-caret-up"></i></span>
                    <span class="closed"><?php _e('View More Features')?> <i class="fa fa-caret-down"></i></span>
                </a>

            </section>


        </div>
    </section>

    <?php include_block("htmlblocks/faq/tools.php"); ?>
    <?php include_block("htmlblocks/tutorials.php" , array('tags' => 'tools')); ?>
    <?php include_block("htmlblocks/blogposts.php", array('set' => 'tools')); ?>

</div>

<?php get_footer(); ?>
