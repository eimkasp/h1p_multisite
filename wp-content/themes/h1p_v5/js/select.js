jQuery(document).ready(function(){

    jQuery(document).on('change', 'select[data-select]', function(e){

        var $el = jQuery(this);

        var key = $el.data('select');
        var val = $el.find('option').filter(':selected').data('value');

        jQuery('[data-select-value="'+key+'"]').html(val);

    });

});
