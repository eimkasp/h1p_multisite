var Tabs = function (container) {

    var tabs = this;

    tabs.id = jQuery(container).data('tabs');
    tabs.$container = jQuery(container);
    tabs.$tabs = tabs.$container.children('*[data-tab-id]');
    tabs.$relativeContent = jQuery('*[data-tab-when^="' + tabs.id + '."]');
    tabs.$links = jQuery('*[data-tab-link^="' + tabs.id + '."]');
    tabs.$links_next = jQuery('*[data-tab-action="' + tabs.id + '.next"]');
    tabs.$links_prev = jQuery('*[data-tab-action="' + tabs.id + '.previous"]');

    tabs.$tabsTitleContainer = jQuery('*[data-tabs-title="' + tabs.id + '"]');

    tabs.useHash = tabs.$container.data('hash-tabs') ? true : false;

    tabs.rotateinterval = false;
    tabs.rotating = jQuery(container).data('rotate');

    if (tabs.rotating) {

        tabs.rotateinterval = setInterval(function () {
            tabs.rotate();
        }, tabs.rotating * 1000);

        jQuery(tabs.$container, tabs.$links, tabs.$links_next, tabs.$links_prev).hover(
            function () {
                clearInterval(tabs.rotateinterval);
            },
            function () {
                tabs.rotateinterval = setInterval(function () {
                    tabs.rotate();
                }, tabs.rotating * 1000);
            }
        );


    }


    tabs.rotate = function () {
        tabs.showNext();
    };


    tabs.updateTabsTitle = function () {

        var tabTitle = tabs.$tabs.filter('.active').data('tab-title');

        if (tabs.$tabsTitleContainer.length && tabTitle) {

            tabs.$tabsTitleContainer.html(tabTitle);

        }


    };

    // show tab by id
    tabs.showTab = function (id) {

        var $t = tabs.$tabs.filter('[data-tab-id="' + id + '"]');

        if (!id || !$t.length) return false;


        tabs.$tabs.filter('.active').removeClass('active');
        $t.addClass('active');

        tabs.$links.filter('.active').removeClass('active');
        tabs.$links.filter('[data-tab-link="' + tabs.id + '.' + id + '"]').addClass('active');

        tabs.$relativeContent.filter('.active').removeClass('active');
        tabs.$relativeContent.filter('[data-tab-when="' + tabs.id + '.' + id + '"]').addClass('active');

        tabs.updateTabsTitle();

        if (tabs.useHash) {

            var hash = window.location.hash;
            var regexp = new RegExp("(#|&)" + tabs.id + "=[^&]+&*", "i");

            if (!hash.length) hash = '#';

            if (regexp.test(hash)) {

                var h = regexp.exec(hash)[0];
                var newH = h.substr(0, 1) + tabs.id + '=' + id;
                if (h.substr(-1, 1) == '&') newH += '&';

                hash = hash.replace(h, newH);

            } else {

                if (hash != '#') hash += '&';

                hash += tabs.id + '=' + id;

            }

            window.location.hash = hash;

        }

        tabs.$container.trigger('tabChanged');

    };

    // show next tab
    tabs.showNext = function () {

        var $currentTab = tabs.$tabs.filter('.active');
        var $nextTab = $currentTab.next('[data-tab-id]');

        if (!$nextTab.length)
            $nextTab = tabs.$tabs.first();

        tabs.showTab($nextTab.data('tab-id'));

    };


    // show previous tab
    tabs.showPrevious = function () {

        var $currentTab = tabs.$tabs.filter('.active');
        var $prevTab = $currentTab.prev('[data-tab-id]');

        if (!$prevTab.length)
            $prevTab = tabs.$tabs.last();

        tabs.showTab($prevTab.data('tab-id'));

    };


    // bind actions for tabs links
    tabs.$links.each(function (i, link) {

        jQuery(link).on('click', function () {

            if (jQuery(this).data('tab-toggle') && jQuery(this).data('tab-toggle') == 'self' && jQuery(this).is('.active')) {

                jQuery(this).removeClass('active');

            } else {

                var tabSelector = jQuery(this).data('tab-link').split('.', 2);
                var tabId = (tabSelector && tabSelector.length > 1) ? tabSelector[1] : null;

                tabs.showTab(tabId);

            }

        });

    });

    tabs.$links_next.on('click', function () {

        tabs.showNext();

    });

    tabs.$links_prev.on('click', function () {

        tabs.showPrevious();

    });


    if (tabs.useHash && window.location.hash.length) {

        jQuery.each(tabs.$tabs, function (i, tab) {

            var tabId = jQuery(tab).data('tab-id');

            var regexp = new RegExp("(#|&)" + tabs.id + "=" + tabId + "+&*", "i");

            if (regexp.test(window.location.hash)) {

                tabs.showTab(tabId);

            }

        });

    }


    return tabs;

};

var ResponsiveTabsMenu = function (container) {

    var tabs = this;

    tabs.tabsId = jQuery(container).data('tabsmenu-responsive');
    tabs.$tabsContent = jQuery('*[data-tabs="' + tabs.tabsId + '"]');

    tabs.$container = jQuery(container);
    tabs.$tabsContainer = tabs.$container.children('*[data-tabsmenu-tabs]');
    tabs.$tabs = tabs.$tabsContainer.children();

    tabs.scroll = function () {

        // var posCenter = containerWidth/2

        var $activeTab = tabs.$tabs.children().filter('.active').parent();

        var tabsLength = tabs.$tabs.length;
        var activeIndex = tabs.$tabs.index($activeTab);
        var containerWidth = tabs.$tabsContainer.outerWidth();
        var tabsWidth = tabs.$tabsContainer.get(0).scrollWidth;


        var scrollDiff = tabsWidth - containerWidth;
        var scrollPos = activeIndex * 100 / (tabsLength - 1);
        var scrollLeft = scrollDiff * scrollPos / 100;


        tabs.$tabsContainer.stop(true).animate({
            'scrollLeft': scrollLeft
        }, 600, 'easeOutQuart');

    }


    tabs.$tabsContent.on('tabChanged', tabs.scroll);

    jQuery(window).on('load resize', tabs.scroll);

    tabs.scroll();

};


jQuery(document).ready(function (event) {

    // initialize tabs

    if (typeof window.data == typeof undefined) window.data = {};
    window.data.tabs = {};

    jQuery('*[data-tabs]').each(function (i, tabs) {

        window.data.tabs[jQuery(tabs).data('tabs').toLowerCase()] = new Tabs(tabs);

    });


    // initialize responsive tabs menu

    jQuery('*[data-tabsmenu-responsive]').each(function (i, menu) {

        new ResponsiveTabsMenu(menu);

    });


});
