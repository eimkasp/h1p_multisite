/*
 * This is to prevent problems from localized websites that does not create session data for GA this way it is not passed to WHMCS and visitor session is lost in Checkout process
 * ATTENTION: Please use <form class="pricing_table"..> in each pricing table !
 */

var tempInterval = setInterval(function(){

    if (typeof ga !== "undefined") {
        /*
         This is not working!

         ga(function(tracker) {
            clientId = tracker.get('clientId');
         });*/
        if (typeof ga.getAll !== "undefined") {
            var ga_code = ga.getAll()[0].get('clientId');
            clearInterval(tempInterval);
            $('form.pricing-table-js').prepend('<input type="hidden" name="_ga" value="' + ga_code + '"/>');
        }
    }
},10);

