// https://github.com/positionly/Typist

if (typeof Typist == 'function') {

    // proceed only if main script is embeded

    var typist;

    typist = document.querySelector("#typist-element");

    new Typist(typist, {
      letterInterval: 30,
      textInterval: 3000,
      onSlide: function(text, options) {
        var suffix;
        suffix = options.typist.getAttribute("data-typist-suffix");
        //return log(text + " " + suffix);
      }
    });
}
