/* 
 * Script for hot news handle animation in the header area
 * Saulius Vikerta
 * 
 * Usage: (in the header element) 
 * 
 *  <div class="hot-news-wrapper">
 *      <div class="hot-news-overflow-container">
 *          <div class="hot-news-group">
 *              <div class="hot-news-handle"><?php _e('New features'); ?> <i class="fa fa-angle-right"></i></div>
 *                  <div class="hot-news-message"><a href="#link" title="<?php _e('Some long title'); ?>"><?php _e('Some long title'); ?></a></div>
 *              </div>
 *          </div> <!-- end of .hot-news-overflow-container -->
 *      </div>
 */

(function($) {

    var msgTimer = null;

    $(document).ready(function(){
        //prepare visual change
        var $msgElement = $('.hot-news-message');
        var $handleElement = $('.hot-news-handle');
        var $msgContainer = $('.hot-news-wrapper');
        var $msgMovingBlock = $('.hot-news-group');
        var maxWidthToGo, amountToHide;

        function msgPrepare() {
            $msgMovingBlock.show();
            amountToHide = $msgContainer.width() - $handleElement.width() - 24;
            $msgMovingBlock.css('left', - amountToHide);    // hide and clip

            // Mobile views: if title too long, truncate text

            var msgWidth = $msgElement.width();
            var $msgTextEl = $msgElement.find('a');
            var msgText = $msgTextEl.text();

            var elementDistortion = 83;
            var delta = $msgContainer.width() - ($handleElement.width() + $msgElement.width() + elementDistortion);
            // delta - how much is difference
            if (delta < 0 ) {
                // need to truncate text
                var shorterText = msgText.substring(0,msgText.length + Math.floor(delta/5));
                $msgTextEl.text(shorterText + '..');
                msgWidth = $msgElement.innerWidth(); // the width has changed
            } else {
                // good to go
            }
            // after text measuring
            //console.log($msgElement.find('a').text());

            // move out as much, as msg text width 
            if ($msgContainer.innerWidth() > ($handleElement.outerWidth() + $msgElement.outerWidth())) {
                maxWidthToGo = msgWidth - amountToHide + 30;
                if (maxWidthToGo > 0) {
                    maxWidthToGo = 0;
                }
            } else {
                maxWidthToGo = 0;   // max is 0
            }
            //console.log('maxWidthToGo :',maxWidthToGo);
        }

        function msgAnimateShow() {
            
            $msgMovingBlock.animate(
                {left: maxWidthToGo }, 
                {
                    duration: 200,
                    specialEasing: {
                        left: "easeInOutExpo"
                    },
                    complete: function() {}
                }
            );
            //console.log('doing animateShow');
            $msgMovingBlock.addClass('active');
        }
        function msgAnimateHide() {
            $msgMovingBlock.animate(
                {left: - amountToHide}, 
                {
                    duration: 500,
                    specialEasing: {
                        left: "easeOutElastic"
                    },
                    complete: function() {}
                }
            );
            //console.log('doing animateHide');
            $msgMovingBlock.removeClass('active');
        }


        msgPrepare();   // do the prep work (rendering and calculations). Initial.

        $(window).resize(function(){
            msgPrepare();   // on each resize
        });

        $(document).on('click mouseenter touchstart', '.hot-news-handle', function(e){
            if ($msgMovingBlock.hasClass('active')) {
                //console.log ('got click or hover: but blocked');
                return;
            }
            e.stopPropagation();
            msgAnimateShow();
        });

        $(document).on('mouseenter', '.hot-news-group', function(e){
            if (msgTimer != null) {
                clearTimeout(msgTimer);
            }
        });
        
        $(document).on('mouseleave', '.hot-news-group', function(e){
            msgTimer = setTimeout (function(){
                if ($msgMovingBlock.hasClass('active')) {
                    msgAnimateHide();
                }
            }, 2000);
        });

        // click parent element and force hide
        $('.hot-news-group').parent().parent().parent().on( "click", function(e) {
            if ((e.target) == $('.hot-news-handle').get(0)) {
                // initial event has bubbled and doubling action
                e.stopPropagation();
                return;     // exit this event handler function
            }
            if ($msgMovingBlock.hasClass('active')) {
                msgAnimateHide();
            }
        });

    });

})(jQuery);