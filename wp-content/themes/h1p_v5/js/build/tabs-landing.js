/* tabs handling (landing pages) */

(function($) {

    $(document).ready(function(){
        $('[data-tab-index]').click(function(){
            var tab_index = $(this).attr('data-tab-index');

            $('[data-tab-index]').removeClass('active');
            $(this).addClass('active');

            $('[data-tab-content]').removeClass('active');
            $('[data-tab-content="'+ tab_index +'"]').addClass('active');
        });

        $('.question, .expand-onclick').click(function( e ){
            if ((e.target !== this) && ($(e.target).parent()[0] !== this)) return;

            if( $(this).hasClass( 'active' ) ){
                $(this).removeClass('active');
            }
            else{
                $('.question, .expand-onclick').removeClass('active');
                $(this).addClass('active');
            }
        });
    });

})(jQuery);