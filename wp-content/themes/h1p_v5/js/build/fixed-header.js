jQuery(document).ready(function(){

    var scrollMax = 56;
    var scrollSpeed = 6;
    var $header = jQuery('#page-header');

    if ($header.hasClass('header-regular')) {
        //Header is not fixed (flat)
        // exiting and no animations should be done
        return;
    } 

    var $firstRow = $header.find('.row.first');

    var headerHeightMax = 96;
    var headerHeightMin = 74;
    var headerHeight;

    var $menuContainer = $firstRow.find('.container');
    var menuHeight = $menuContainer.outerHeight();
    var menuPadding;

    var $logo = $menuContainer.find('.logo');
    var logoHeightMax = 64;
    var logoHeightMin = 44;
    var logoHeight;
    var logoPadding;

    jQuery(window).on('load scroll', function(){

        var scrollTop = (jQuery(window).scrollTop()/scrollSpeed > scrollMax) ? scrollMax : jQuery(window).scrollTop()/scrollSpeed;
        var scrollPercents = scrollTop*100/scrollMax;

        headerHeight = Math.round(headerHeightMax - scrollPercents*(headerHeightMax-headerHeightMin)/100);
        menuPadding = Math.round((headerHeight - menuHeight)/2);

        logoHeight = Math.round(logoHeightMax - scrollPercents*(logoHeightMax-logoHeightMin)/100);
        logoPadding = Math.round((menuHeight - logoHeight)/2);

        $firstRow.css({
            'height': headerHeight,
            'padding-top': menuPadding
        });

        $logo.css({
            'height': logoHeight,
            'padding-top': logoPadding
        });

        $header.toggleClass('scroll-max', scrollPercents >= 100);
        $header.toggleClass('scroll-min', scrollPercents <= 0);

    });


});
