var Dropdown = function(container){

    var dd = this;

    dd.$container = jQuery(container);
    dd.$toggle = dd.$container.children('[data-dropdown-toggle]').length ? dd.$container.children('[data-dropdown-toggle]') : dd.$container;
    dd.group = dd.$container.data('dropdown-group');
    dd.openCallback = dd.$container.data('dropdown-open-callback');
    dd.keepPosition = typeof dd.$container.data('keep-position') !== typeof undefined ? true : false;

    dd.open = function(event){

        if(dd.keepPosition) var top = dd.$container.offset().top - jQuery(window).scrollTop();
        // close all other dropdown in the same group
        if(dd.group) jQuery('[data-dropdown][data-dropdown-group="'+dd.group+'"].open').removeClass('open');

        dd.$container.toggleClass('open', true);

        if(dd.keepPosition) jQuery(window).scrollTop(dd.$container.offset().top - top);

        if(dd.openCallback)  eval(dd.openCallback);

    };

    dd.close = function(event){

        dd.$container.toggleClass('open', false);

    };

    dd.toggle = function(event){

        if(dd.$container.is('.open')){

            dd.close();

        } else {

            dd.open();


        }

    };


    dd.$toggle.on('click', dd.toggle);

};


jQuery(document).ready(function(event){

    // initialize dropdowns

    jQuery('*[data-dropdown]').each(function(i, dropdown){

       new Dropdown(dropdown);

    });

});
