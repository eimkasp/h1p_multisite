jQuery(document).ready(function(event){

    jQuery('*[data-toggle-active]').each(function(i, el){

      jQuery(el).on('click', function(){

          if (jQuery(this).attr('data-open-all-blocks') == 1){
              // This is used in Customer support blocks for toggling all blocks at once

              jQuery('[data-open-all-blocks]').parent().toggleClass('active');
              jQuery('[data-open-all-blocks]').toggleClass('active');
              /*console.log (jQuery('[data-open-all-blocks]'));*/

          }
          else{

            if(jQuery(this).data('toggle-active') == 'parent'){
              // This is regular toggling element for accordions

                jQuery(this).parent().toggleClass('active');


            } else {

                jQuery(this).toggleClass('active');

            }

          }

          if(jQuery(this).data('toggle-callback')){

              eval(jQuery(this).data('toggle-callback'));

          }

      });

    });

    jQuery('*[data-toggle-open]').each(function(i, el){

      jQuery(el).on('click', function(){

          if(jQuery(this).data('toggle-open') == 'parent'){

              jQuery(this).parent().toggleClass('open');


          } else {

              jQuery(this).toggleClass('open');

          }

          if(jQuery(this).data('toggle-callback')){

              eval(jQuery(this).data('toggle-callback'));

          }

      });

    });

    jQuery('*[data-toggle-class]').each(function(i, el){

      jQuery(el).on('click', function(){

          var data = jQuery(this).data('toggle-class');
          var params = data.split(',');

          var addclass = params[0];
          var $target = (params.length > 1) ? jQuery(params[1].trim()) : jQuery(this);

          $target.toggleClass(tclass);

      });

    });

    jQuery('*[data-expand-plan-features]').each(function(i, el){

      jQuery(el).on('click', function(){

          var $this = jQuery(this);
          var $container = $this.closest('.plans-pricing-table');

          var col = $this.data('expand-plan-features');


          jQuery.each($container.attr('class').split(/\s/), function(i, iclass){

             if(iclass.match(/expanded-[0-9]+/)) $container.removeClass(iclass);

          });

          $container.addClass('expanded-' + col);

      });

    });

    jQuery('*[data-collapse-plan-features]').each(function(i, el){

      jQuery(el).on('click', function(){

          var $this = jQuery(this);
          var $container = $this.closest('.plans-pricing-table');

          jQuery.each($container.attr('class').split(/\s/), function(i, iclass){

             if(iclass.match(/expanded-[0-9]+/)) $container.removeClass(iclass);

          });

      });

    });


});
