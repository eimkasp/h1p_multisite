// Clean solution for preventing default behavior for <a> links

jQuery(document).on('click','a[data-prevent]', function(e){
    // General Prevent default solution for a href markup.
    e.preventDefault();
});


// Solving problems for iOS Javascript events

var clickHandler = ('ontouchstart' in document.documentElement ? "touchstart" : "click");

$("a").bind(clickHandler, function(e) {
    handleTouch(e);
});

var handleTouch = function(event) {

    if (event.type !== 'click') {   // skip everything if it is not touch event

        var touches = event.changedTouches,
                first = touches[0],
                type = '';

        switch(event.type)
        {
          case 'touchstart':
            type = 'mousedown';
            break;

          case 'touchmove':
            type = 'mousemove';
            event.preventDefault();
            break;

          case 'touchend':
            type = 'mouseup';
            break;

          default:
            return;
        }


        var simulatedEvent = document.createEvent('MouseEvent');
        simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left*/, null);
        first.target.dispatchEvent(simulatedEvent);
    }
};
