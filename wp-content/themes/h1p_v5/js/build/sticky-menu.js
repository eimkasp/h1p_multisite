/* 
 * Script for populating contextual sticky menu.
 * Saulius Vikerta
 * 
 * has dependency on jquery.sticky.js plugin
 * Please enqueue library in the certain page and use designated attributes in your page HTML elements:
 * - data-section-button (together with data-link-target, data-button-styling)
 * - data-section-title
 * 
 * Please find that it can generate links and button.
 */


(function($){

        function activateContextItem(elem) {
            //console.log(elem);
        }

        /*function isScrolledIntoView(elem) {
            var docViewTop = $(window).scrollTop();
            var docViewBottom = docViewTop + $(window).height();

            var elemTop = $(elem).offset().top;
            var elemBottom = elemTop + $(elem).height();

            return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
        }*/

        $(document).ready(function(){
            
             /*If jquery.sticky.js is not enqueued in the page the code will not be executed*/

             if(jQuery().sticky) {
                 //console.log ('Yes - have Sticky lib');
             

                function stickyMenuGenerateLinks($container) {

                    /*prevent double execution*/
                    if ($('ul.sticky-menu-block').length > 0) {
                        return;     // means the function has been already executed once
                    }

                    /*elements with "[data-section-title]" are discovered, then their scroll targets are found. Links are then generated*/ 

                    var $menuUlElement = $('<ul class="sticky-menu-block sticky-nav nav"></ul>');
                    $container.append($menuUlElement);

                    $('[data-section-title]').each(function(){
                        var anchorLink = $(this).find('.scroll-id-target').attr('id');
                        $menuUlElement.append('<li><a class="context-item" href="#'+ anchorLink + '" data-scrollto="' + anchorLink + '">' + $(this).attr('data-section-title') + '</a></li>');
                    });

                }

                function stickyMenuGenerateButton($container) {

                    /*prevent double execution*/
                    if ($('div.sticky-menu-block').length > 0) {
                        return;     // means the function has been already executed once
                    }

                    /*elements with "[data-section-button]" are discovered, then their scroll or ext.link targets are found. Links are then generated*/ 
                    
                    var $menuButtonElem = $('[data-section-button]');

                    if ($menuButtonElem.length == 0) {
                        return; // no buttons in the page, then quit this func
                    }

                    var menuButtonVal = $menuButtonElem.attr('data-section-button'),
                    menuButtonExtLink = $menuButtonElem.attr('data-link-target'),
                    menuButtonStyling = $menuButtonElem.attr('data-button-styling'),
                    menuButtonStylingAttr,
                    anchorButtonHrefAttr,
                    anchorButtonScrolltoAttr = ''; 

                    if (menuButtonExtLink == '') {
                        // nothing was passed, same as NULL
                        anchorButtonHrefAttr = '';  // to prevent printing 'undefined'
                        
                    } else if (typeof menuButtonExtLink === 'undefined') {
                        // this is scroll target
                        anchorButton = $menuButtonElem.find('.scroll-id-target').attr('id');
                        anchorButtonHrefAttr = 'href="#' + anchorButton + '"';
                        anchorButtonScrolltoAttr = 'data-scrollto="' + anchorButton + '"';
                    } else {
                        // this is external link
                        anchorButtonHrefAttr = 'href="' + menuButtonExtLink + '"';
                    }

                    if (typeof menuButtonStyling === 'undefined') {
                        menuButtonStylingAttr = 'class="button ghost"';
                    } else {
                        menuButtonStylingAttr = 'class="' + menuButtonStyling + '"';
                    }

                    // adding up to a button
                    $container.append('<div class="sticky-menu-block"><a ' + menuButtonStylingAttr + ' ' + anchorButtonHrefAttr + ' ' + anchorButtonScrolltoAttr + '>' + menuButtonVal + '</a></div>');

                }

                function stickyEnhanceBlogMenu($element, $header) {
                    var scrolledVal = $(document).scrollTop().valueOf(),
                        headerSizing;

                    if (typeof $header == undefined) {
                        headerSizing = 0;
                    } else {
                        headerSizing = $header.height();
                    }
                    
                    if (scrolledVal > headerSizing) {
                        $element.addClass('fixed');
                    } else $element.removeClass('fixed');

                }


                

                /*Initiate sticky-menu*/
                $('.sticky-menu').sticky();
                
                /*Populate sticky-menu*/
                var $menu = $('.sticky-menu .container');

                $stickyMenuMode = $('.sticky-menu').data('mode');

                switch ($stickyMenuMode) {
                    case 'blog':

                        $(document).ready(function(){
                            stickyEnhanceBlogMenu($('.blog-header'), $('header'));
                            $(window).on('scroll', function(){
                                stickyEnhanceBlogMenu($('.blog-header'), $('header'));
                            });
                        });

                        break;

                    default:    // ='landing'

                        stickyMenuGenerateLinks($menu); 
                        stickyMenuGenerateButton($menu);

                        
                        /*Marking active links*/

                        var $stickyNavigationItem = $('.sticky-nav a');

                        var headerHeight = $('.sticky-menu').height();
                        $('.scroll-id-target').css('top', -headerHeight - 25);

                        $stickyNavigationItem.click(function(){
                            $stickyNavigationItem.removeClass('active');
                            $(this).addClass('active');
                        });

                        $(window).on('scroll', function(){

                            setTimeout(function(){
                                $stickyNavigationItem.removeClass('active');
                            }, 1000);

                            /*$('[data-section-title]:visible').each(function(){
                                //activateContextItem(this);
                            })*/
                        });

                        // end of default switch case

                } // end of switch


            } else { 
                 //console.log ('Sticky lib not found');
            }

        });

    })(jQuery);

