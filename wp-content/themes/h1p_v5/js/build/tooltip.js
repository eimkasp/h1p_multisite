var Tooltips = new function(){

    function showTooltip(trigger){

        var $trigger= jQuery(trigger);
        var id = $trigger.data('tooltip');
        var $container = jQuery('[data-tooltip-content="' + id + '"]');
        var $arrow = $container.children('.triangle');

        var mapDelta = 12;
        var cssTopPosition = $trigger.height();

        if ( $trigger.hasClass ("loc-cont") ) {
            cssTopPosition = $trigger.height() - mapDelta;
        }


        $container.css({
            'position': 'fixed',
            /*'position': 'absolute',*/
            'top': $trigger.offset().top - jQuery(window).scrollTop() + cssTopPosition,
            /*'top': cssTopPosition,*/
            'left': $trigger.offset().left,
            /*'left': 0,*/
            'max-width': ''
        });

        /*console.log ('Tooltip trigger height - ' + $trigger.height() );*/

        $arrow.css({
            'width': $trigger.width(),
            'left': 0
        });

        $trigger.addClass('active');
        $container.addClass('active');

        var offset = $container.outerWidth() + $container.offset().left - jQuery(document).width() + 10

        if($container.outerWidth() > (jQuery(document).width() - 20)) {

            $container.css({
                'max-width': (jQuery(document).width() - 20),
                'left': 10
            });

            $arrow.css('left', $trigger.offset().left - 10);

        } else if(offset > 0) {

            $container.css('left', '-=' + offset);
            $arrow.css('left', offset);

        }


    };

    function hideTooltip(trigger){

        var $trigger= jQuery(trigger);
        var id = $trigger.data('tooltip');
        var $container = jQuery('[data-tooltip-content="' + id + '"]');

        $trigger.removeClass('active');
        $container.removeClass('active');

    };

    function hideAllTooltips(){

        jQuery('[data-tooltip]').each(function(i, trigger){

            hideTooltip(trigger);

        });

    };


    jQuery(document).on('mouseover', '[data-tooltip]', function(){
        if (screen.width >= 780 )
            showTooltip(this);
    });
    jQuery(document).on('mouseout', '[data-tooltip]', function(){hideTooltip(this)});
    jQuery(document).on('click', '[data-tooltip]', function(){
        /*console.log (screen.width);*/
        if (screen.width < 780 )
            showTooltip(this);
    });


    jQuery(window).on('scroll resize', hideAllTooltips);

    jQuery(document).on("scrollstart", hideAllTooltips);

};
