function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

(function(){

    var cookiesPolicyVersion = 1;

    function accepCookiesPolicy(){

        var d = new Date(); d.setTime(d.getTime() + (365*24*60*60*1000));

        document.cookie = "h1pcp="+cookiesPolicyVersion+"; expires=" + d.toUTCString() + "; path=/";

        jQuery('#cookies-policy-accept').hide();
        jQuery('body').removeClass('cookies-alert-visible');

    }

    jQuery(document).ready(function(){

        var cookies = document.cookie.split(';');
        var accepted = false;

        jQuery.each(cookies, function(i, cookie){

            if(cookie.trim() == 'h1pcp=' + cookiesPolicyVersion) accepted = true;

        });

        if(!accepted){

            jQuery('body').addClass('cookies-alert-visible');
            jQuery('#cookies-policy-accept').show();

        }

    });

    window.accepCookiesPolicy = accepCookiesPolicy;

})();

jQuery.each(jQuery('input.pikaday-input'), function(i, input){

    var $pikaday = new Pikaday({
        field: input,
        yearRange: [1946,1998]
    });

});

var Dropdown = function(container){

    var dd = this;

    dd.$container = jQuery(container);
    dd.$toggle = dd.$container.children('[data-dropdown-toggle]').length ? dd.$container.children('[data-dropdown-toggle]') : dd.$container;
    dd.group = dd.$container.data('dropdown-group');
    dd.openCallback = dd.$container.data('dropdown-open-callback');
    dd.keepPosition = typeof dd.$container.data('keep-position') !== typeof undefined ? true : false;

    dd.open = function(event){

        if(dd.keepPosition) var top = dd.$container.offset().top - jQuery(window).scrollTop();
        // close all other dropdown in the same group
        if(dd.group) jQuery('[data-dropdown][data-dropdown-group="'+dd.group+'"].open').removeClass('open');

        dd.$container.toggleClass('open', true);

        if(dd.keepPosition) jQuery(window).scrollTop(dd.$container.offset().top - top);

        if(dd.openCallback)  eval(dd.openCallback);

    };

    dd.close = function(event){

        dd.$container.toggleClass('open', false);

    };

    dd.toggle = function(event){

        if(dd.$container.is('.open')){

            dd.close();

        } else {

            dd.open();


        }

    };


    dd.$toggle.on('click', dd.toggle);

};


jQuery(document).ready(function(event){

    // initialize dropdowns

    jQuery('*[data-dropdown]').each(function(i, dropdown){

       new Dropdown(dropdown);

    });

});

(function(){

    function getSizeString(bytes){

        var units = {
            'B': 1,
            'KB':1024,
            'MB':1048576,
            'GB':1073741824
        };

        var val = bytes;
        var unit = 'B';

        jQuery.each(units, function(key, mult){

            if((bytes/mult) > 1) {

                val = bytes/mult;
                unit = key;

            }

        });

        return Math.round(val*100)/100 + ' ' + unit;

    }

    function removeValidationMessages($container){

        $container.find('.wpcf7-not-valid-tip').remove();
        $container.removeClass('wpcf7-not-valid');

    }


    jQuery('[data-file-upload]').each(function(i, el){

        var $container = jQuery(el);

        var $input = $container.find('input[type="file"]');
        var $filename = $container.find('[data-file-upload-filename]');
        var $filename_nofile = $filename.text();
        var $filesize = $container.find('[data-file-upload-filesize]');
        var $filesize_nofile = $filesize.text();
        var $btn_remove = $container.find('[data-file-upload-removefile]');

        $input.on('change', function(){

            if($input[0]['files'] && $input[0]['files'].length){

                var filedata = $input[0]['files'][0];

                $filename.text(filedata['name'] ? filedata['name'] : 'Undefined');
                $filesize.text(filedata['size'] ? getSizeString(filedata['size']) : 'Undefined');

                $container.addClass('file-selected');

            } else {

                $filename.text($filename_nofile);
                $filesize.text($filesize_nofile);

                $container.removeClass('file-selected');
                removeValidationMessages($container);

            }

        });

        $btn_remove.click(function(){
            $input.val('');

            $filename.text($filename_nofile);
            $filesize.text($filesize_nofile);

            $container.removeClass('file-selected');
            removeValidationMessages($container);

            $input.trigger('change');

        });

        setInterval(function(){

            if($input.parent().find('.wpcf7-not-valid-tip').length) {

                $container.children('.wpcf7-not-valid-tip').remove();
                $container.addClass('wpcf7-not-valid');

                jQuery.each($input.parent().find('.wpcf7-not-valid-tip'), function(i, el){

                    $container.append(jQuery(el)[0].outerHTML);

                });

            } else {

                removeValidationMessages($container);

            }

            $input.trigger('change');

        }, 1000)

    });

})();

jQuery(document).ready(function(){

    var scrollMax = 56;
    var scrollSpeed = 6;
    var $header = jQuery('#page-header');

    if ($header.hasClass('header-regular')) {
        //Header is not fixed (flat)
        // exiting and no animations should be done
        return;
    } 

    var $firstRow = $header.find('.row.first');

    var headerHeightMax = 96;
    var headerHeightMin = 74;
    var headerHeight;

    var $menuContainer = $firstRow.find('.container');
    var menuHeight = $menuContainer.outerHeight();
    var menuPadding;

    var $logo = $menuContainer.find('.logo');
    var logoHeightMax = 64;
    var logoHeightMin = 44;
    var logoHeight;
    var logoPadding;

    jQuery(window).on('load scroll', function(){

        var scrollTop = (jQuery(window).scrollTop()/scrollSpeed > scrollMax) ? scrollMax : jQuery(window).scrollTop()/scrollSpeed;
        var scrollPercents = scrollTop*100/scrollMax;

        headerHeight = Math.round(headerHeightMax - scrollPercents*(headerHeightMax-headerHeightMin)/100);
        menuPadding = Math.round((headerHeight - menuHeight)/2);

        logoHeight = Math.round(logoHeightMax - scrollPercents*(logoHeightMax-logoHeightMin)/100);
        logoPadding = Math.round((menuHeight - logoHeight)/2);

        $firstRow.css({
            'height': headerHeight,
            'padding-top': menuPadding
        });

        $logo.css({
            'height': logoHeight,
            'padding-top': logoPadding
        });

        $header.toggleClass('scroll-max', scrollPercents >= 100);
        $header.toggleClass('scroll-min', scrollPercents <= 0);

    });


});

//there was yotpo
/*
 * This is to prevent problems from localized websites that does not create session data for GA this way it is not passed to WHMCS and visitor session is lost in Checkout process
 * ATTENTION: Please use <form class="pricing_table"..> in each pricing table !
 */

var tempInterval = setInterval(function(){

    if (typeof ga !== "undefined") {
        /*
         This is not working!

         ga(function(tracker) {
            clientId = tracker.get('clientId');
         });*/
        if (typeof ga.getAll !== "undefined") {
            var ga_code = ga.getAll()[0].get('clientId');
            clearInterval(tempInterval);
            $('form.pricing-table-js').prepend('<input type="hidden" name="_ga" value="' + ga_code + '"/>');
        }
    }
},10);


/* 
 * Script for hot news handle animation in the header area
 * Saulius Vikerta
 * 
 * Usage: (in the header element) 
 * 
 *  <div class="hot-news-wrapper">
 *      <div class="hot-news-overflow-container">
 *          <div class="hot-news-group">
 *              <div class="hot-news-handle"><?php _e('New features'); ?> <i class="fa fa-angle-right"></i></div>
 *                  <div class="hot-news-message"><a href="#link" title="<?php _e('Some long title'); ?>"><?php _e('Some long title'); ?></a></div>
 *              </div>
 *          </div> <!-- end of .hot-news-overflow-container -->
 *      </div>
 */

(function($) {

    var msgTimer = null;

    $(document).ready(function(){
        //prepare visual change
        var $msgElement = $('.hot-news-message');
        var $handleElement = $('.hot-news-handle');
        var $msgContainer = $('.hot-news-wrapper');
        var $msgMovingBlock = $('.hot-news-group');
        var maxWidthToGo, amountToHide;

        function msgPrepare() {
            $msgMovingBlock.show();
            amountToHide = $msgContainer.width() - $handleElement.width() - 24;
            $msgMovingBlock.css('left', - amountToHide);    // hide and clip

            // Mobile views: if title too long, truncate text

            var msgWidth = $msgElement.width();
            var $msgTextEl = $msgElement.find('a');
            var msgText = $msgTextEl.text();

            var elementDistortion = 83;
            var delta = $msgContainer.width() - ($handleElement.width() + $msgElement.width() + elementDistortion);
            // delta - how much is difference
            if (delta < 0 ) {
                // need to truncate text
                var shorterText = msgText.substring(0,msgText.length + Math.floor(delta/5));
                $msgTextEl.text(shorterText + '..');
                msgWidth = $msgElement.innerWidth(); // the width has changed
            } else {
                // good to go
            }
            // after text measuring
            //console.log($msgElement.find('a').text());

            // move out as much, as msg text width 
            if ($msgContainer.innerWidth() > ($handleElement.outerWidth() + $msgElement.outerWidth())) {
                maxWidthToGo = msgWidth - amountToHide + 30;
                if (maxWidthToGo > 0) {
                    maxWidthToGo = 0;
                }
            } else {
                maxWidthToGo = 0;   // max is 0
            }
            //console.log('maxWidthToGo :',maxWidthToGo);
        }

        function msgAnimateShow() {
            
            $msgMovingBlock.animate(
                {left: maxWidthToGo }, 
                {
                    duration: 200,
                    specialEasing: {
                        left: "easeInOutExpo"
                    },
                    complete: function() {}
                }
            );
            //console.log('doing animateShow');
            $msgMovingBlock.addClass('active');
        }
        function msgAnimateHide() {
            $msgMovingBlock.animate(
                {left: - amountToHide}, 
                {
                    duration: 500,
                    specialEasing: {
                        left: "easeOutElastic"
                    },
                    complete: function() {}
                }
            );
            //console.log('doing animateHide');
            $msgMovingBlock.removeClass('active');
        }


        msgPrepare();   // do the prep work (rendering and calculations). Initial.

        $(window).resize(function(){
            msgPrepare();   // on each resize
        });

        $(document).on('click mouseenter touchstart', '.hot-news-handle', function(e){
            if ($msgMovingBlock.hasClass('active')) {
                //console.log ('got click or hover: but blocked');
                return;
            }
            e.stopPropagation();
            msgAnimateShow();
        });

        $(document).on('mouseenter', '.hot-news-group', function(e){
            if (msgTimer != null) {
                clearTimeout(msgTimer);
            }
        });
        
        $(document).on('mouseleave', '.hot-news-group', function(e){
            msgTimer = setTimeout (function(){
                if ($msgMovingBlock.hasClass('active')) {
                    msgAnimateHide();
                }
            }, 2000);
        });

        // click parent element and force hide
        $('.hot-news-group').parent().parent().parent().on( "click", function(e) {
            if ((e.target) == $('.hot-news-handle').get(0)) {
                // initial event has bubbled and doubling action
                e.stopPropagation();
                return;     // exit this event handler function
            }
            if ($msgMovingBlock.hasClass('active')) {
                msgAnimateHide();
            }
        });

    });

})(jQuery);
var JsonForm = function(data, url, method){

    var form = this;

    form.fields = [];
    form.$el = jQuery('<form/>');

    form.$el.attr('method', method || 'post');
    form.$el.attr('action', url);

    form.submit = function(){

        form.$el.appendTo(jQuery('body'));
        form.$el.submit();

    };


    function validateJson(json){

        if(typeof json == 'number' || !isNaN(json)){

            return false;

        }

        try {

            JSON.parse(json);
            return true;

        } catch (e) {

            return false;

        }

    };

    function getFields(params, name){

        var fields = [];

        for (var key in params)
        {
            if (params.hasOwnProperty(key))
            {
                var fieldName = (typeof name != typeof undefined) ? name + '[' + key + ']' : key;

                if((typeof params[key] == 'string' && validateJson(params[key])) || typeof params[key] == 'object'){

                    fields = fields.concat(getFields(params[key], fieldName));

                } else {

                    var hiddenField = document.createElement('input');

                        hiddenField.setAttribute('type',  'hidden');
                        hiddenField.setAttribute('name',  fieldName);
                        hiddenField.setAttribute('value', params[key]);

                    fields.push(hiddenField);

                }
            }
        }

        return fields;

    };


    var fields = getFields(data);

    for (var index in fields)
    {
        form.$el.append(fields[index]);
    }

    return form;

}











// Clean solution for preventing default behavior for <a> links

jQuery(document).on('click','a[data-prevent]', function(e){
    // General Prevent default solution for a href markup.
    e.preventDefault();
});


// Solving problems for iOS Javascript events

var clickHandler = ('ontouchstart' in document.documentElement ? "touchstart" : "click");

$("a").bind(clickHandler, function(e) {
    handleTouch(e);
});

var handleTouch = function(event) {

    if (event.type !== 'click') {   // skip everything if it is not touch event

        var touches = event.changedTouches,
                first = touches[0],
                type = '';

        switch(event.type)
        {
          case 'touchstart':
            type = 'mousedown';
            break;

          case 'touchmove':
            type = 'mousemove';
            event.preventDefault();
            break;

          case 'touchend':
            type = 'mouseup';
            break;

          default:
            return;
        }


        var simulatedEvent = document.createEvent('MouseEvent');
        simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left*/, null);
        first.target.dispatchEvent(simulatedEvent);
    }
};

/* https://github.com/VincentGarreau/particles.js/ */

function particlesConfig() {

    return {
    "particles": {
      "number": {
        "value": 100,
        "density": {
          "enable": true,
          "value_area": 800
        }
      },
      "color": {
        "value": "#ffffff"
      },
      "shape": {
        "type": "polygon",
        "stroke": {
          "width": 0,
          "color": "#000000"
        },
        "polygon": {
          "nb_sides": 5
        },
        "image": {
          "src": "img/github.svg",
          "width": 100,
          "height": 100
        }
      },
      "opacity": {
        "value": 0.1,
        "random": false,
        "anim": {
          "enable": false,
          "speed": 1,
          "opacity_min": 0.02,
          "sync": false
        }
      },
      "size": {
        "value": 5,
        "random": true,
        "anim": {
          "enable": false,
          "speed": 40,
          "size_min": 0.1,
          "sync": false
        }
      },
      "line_linked": {
        "enable": true,
        "distance": 150,
        "color": "#ffffff",
        "opacity": 0.2,
        "width": 1
      },
      "move": {
        "enable": true,
        "speed": 0.5,
        "direction": "none",
        "random": false,
        "straight": false,
        "out_mode": "out",
        "attract": {
          "enable": false,
          "rotateX": 600,
          "rotateY": 1200
        }
      }
    },
    "interactivity": {
      "detect_on": "canvas",
      "events": {
        "onhover": {
          "enable": false,
          "mode": "repulse"
        },
        "onclick": {
          "enable": true,
          "mode": ["push","repulse"]
        },
        "resize": true
      },
      "modes": {
        "grab": {
          "distance": 400,
          "line_linked": {
            "opacity": 1
          }
        },
        "bubble": {
          "distance": 400,
          "size": 40,
          "duration": 2,
          "opacity": 8,
          "speed": 3
        },
        "repulse": {
          "distance": 200
        },
        "push": {
          "particles_nb": 4
        },
        "remove": {
          "particles_nb": 2
        }
      }
    },
    "retina_detect": true,
    "config_demo": {
      "hide_card": false,
      "background_color": "#b61924",
      "background_image": "",
      "background_position": "50% 50%",
      "background_repeat": "no-repeat",
      "background_size": "cover"
    }
  }

}

// initialize Magnific Popups
jQuery(document).ready(function(){

    var visiblePopups = [];

    jQuery('*[data-popup]').each(function(i, popup){

        var $popup = jQuery(popup);

        var id = $popup.data('popup-id') || null;
        var type = $popup.data('popup-type') || 'image';
        var src = $popup.data('popup');

        if(src){

            $popup.magnificPopup({
                //closeOnBgClick: true,     // this may be
                items: {
                    src: src,
                    type: type
                },
                iframe: {
                    patterns: {
                        youtube: {
                            index: 'youtube.com',
                            id: 'v=',
                            src: '//www.youtube.com/embed/%id%?autoplay=1&rel=0&wmode=opaque'
                        }
                    }
                },
                callbacks: {
                    // Generic callback option for magnific popups
                    open: function(){
                        var mp = $.magnificPopup.instance,
                            t = $(mp.st.el[0]),
                            cbFunction = t.data('callback');

                        //console.log( cbFunction );
                        if (cbFunction) {
                            setTimeout(function () {eval(cbFunction);}, 1000);
                        }
                    }
                }
            });

            if(id && window.location.hash && window.location.hash.replace(/^#/, '') == id && visiblePopups.indexOf(id) == -1){

                visiblePopups.push(id);

            }

        }


    });

    jQuery.each(visiblePopups, function(i, id){

        jQuery('[data-popup-id="' + id + '"]').first().click();

    });


});

var scrollTo = function(target, stopOnAction, container){

    var $container = container ? jQuery(container) : jQuery("html, body");
    var $target = jQuery(target);

    if($target.length){

        var to = $target.offset().top;

        $container.animate({

            scrollTop: to + 'px'

        },{

            duration: 800,
            easing: 'easeInOutQuart',
            start: function () {

                if(stopOnAction){

                    setTimeout(function () {

                        jQuery(window).on('mousedown.scrolling DOMMouseScroll.scrolling mousewheel.scrolling keyup.scrolling', function () {

                            $container.stop(true);

                        });

                    }, 100);

                }

            },

            always: function () {

                jQuery(window).off('mousedown.scrolling DOMMouseScroll.scrolling mousewheel.scrolling keyup.scrolling');

            }

        });

    }


};

/*Changes: now binding was extracted from document ready moment.*/

jQuery(document).on('click', '[data-scrollto]', function(e){

    var target = jQuery(this).data('scrollto');
    var $target = target == 'self' ? jQuery(this) : jQuery('#' + target);

    if(target == 'self' && $target.is('.active') || target != 'self' && $target.length){

        e.preventDefault();

        scrollTo($target, true);

    }

});


jQuery(document).ready(function(){

    jQuery(document).on('change', 'select[data-select]', function(e){

        var $el = jQuery(this);

        var key = $el.data('select');
        var val = $el.find('option').filter(':selected').data('value');

        jQuery('[data-select-value="'+key+'"]').html(val);

    });

});

var ContentSlider = function(container){

    var slider = this;

    slider.id = jQuery(container).data('slider');
    slider.$container = jQuery(container);
    slider.$slidesContainer = slider.$container.find('[data-slides]');
    slider.$slides = slider.$slidesContainer.children('.slide');
    slider.$navLinks = jQuery('*[data-slider-nav="'+slider.id+'"]');

    slider.updateNav = function(index){

        slider.$navLinks.filter('.active').removeClass('active');
        slider.$navLinks.filter('[data-slider-slide="'+index+'"]').addClass('active');

    };

    slider.showSlide = function(index){

        var $activeSlide = slider.$slides.eq(index);

        slider.$slides.filter('.active').removeClass('active');
        $activeSlide.addClass('active');

        slider.slideTo($activeSlide.position().left + slider.$container.scrollLeft());

        slider.updateNav(index);
    };

    slider.getClosestSlide = function(scrollLeft){

        var containerScrollLeft = slider.$container.scrollLeft();
        var $closestSlide = slider.$slides.first();
        var closestLeft = $closestSlide.position().left + containerScrollLeft;
        var closestDif = Math.abs(closestLeft - scrollLeft);

        slider.$slides.each(function(i, slide){

            var $slide = jQuery(slide);
            var slideLeft = $slide.position().left + containerScrollLeft;
            var slideDif = Math.abs(slideLeft - scrollLeft);

            if(slideDif < closestDif){

                $closestSlide = $slide;
                closestLeft = slideLeft;
                closestDif = slideDif;

            }

        });

        return $closestSlide;


    };

    // animate sliding
    slider.slideTo = function(scrollLeft){

        slider.$container.stop(true, true).animate({
            'scrollLeft': scrollLeft
        }, 800);


    };

    // stops easing on touchstart/mousdown
    slider.onMoveStart = function(){

        slider.$container.stop(true, false);

    };


    // handles drag events
    slider.onMoveTo = function(x,y){

        var containerScrollLeft = slider.$container.scrollLeft();
        var direction = x.split('=');
        var scrollLeft = containerScrollLeft - (direction[0]+direction[1]);
        var final = slider.$slidesContainer.is('.pep-start') ? false : true;
        var duration = final ? 800 : 0;
        var maxScroll = slider.$slidesContainer.width() - slider.$container.width();


        if(scrollLeft < 0) scrollLeft = 0;
        if(scrollLeft > maxScroll) scrollLeft = maxScroll;

        if(final) {

            var $closestSlide = slider.getClosestSlide(scrollLeft);

            scrollLeft = $closestSlide.position().left + containerScrollLeft;

            slider.$slides.filter('.active').removeClass('active');
            $closestSlide.addClass('active');


            slider.updateNav(slider.$slides.index($closestSlide));

            slider.slideTo(scrollLeft);


        } else {

            slider.$container.stop(true, true);

            slider.$container.scrollLeft(scrollLeft);

        }



    };


    slider.onMoveStop = function(event, pep){

        // disable css animations
        pep.cssAnimationsSupport = false;

    };

    slider.init = function(){

        if(slider.$slides.filter(':visible').first().width() == slider.$container.width() && slider.$slides.length > 1){

            var slideWidth = slider.$container.width();

            // fix slides and container widths
            slider.$slides.css('width', slideWidth);
            slider.$slidesContainer.css('width', slider.$slides.length*slideWidth);

            // add slider class
            slider.$container.addClass('slider-active');

            // bind draggable
            slider.$slidesContainer.pep(slider.pepOptions);

            slider.$container.on('mousedown.slider', slider.onMoveStart);

            slider.$container.scrollLeft(slider.$slides.filter('.active').position().left + slider.$container.scrollLeft());

        }

    };

    slider.destroy = function(){

        // remove fixed widths
        slider.$slides.css('width', '');
        slider.$slidesContainer.css('width', '');

        // remove slider class
        slider.$container.removeClass('slider-active');

        // unbind draggable
        jQuery.pep.unbind(slider.$slidesContainer);

        slider.$container.off('mousedown.slider', slider.onMoveStart);

    };

    // initialize slider navigation
    slider.initNav = function(){

        slider.$navLinks.each(function(i, link){

            jQuery(link).on('click', function(){

                slider.showSlide(jQuery(this).data('slider-slide'));

            });

        });

    };

    slider.pepOptions = {
        // initiate:                       function(){},
        start:                          slider.onMoveStart,
        // drag:                           function(){},
        stop:                           slider.onMoveStop,
        // easing:                         function(){},
        // rest:                           function(){},
        moveTo:                         slider.onMoveTo,
        callIfNotStarted:               ['stop', 'rest'],
        startThreshold:                 [0,0],
        grid:                           [1,1],
        debug:                          false,
        activeClass:                    'pep-active',
        multiplier:                     1,
        velocityMultiplier:             2.5,
        shouldPreventDefault:           true,
        allowDragEventPropagation:      true,
        stopEvents:                     '',
        hardwareAccelerate:             false,
        useCSSTranslation:              false,
        disableSelect:                  false,
        cssEaseString:                  "",
        cssEaseDuration:                0,
        shouldEase:                     true,
        droppable:                      false,
        droppableActiveClass:           'pep-dpa',
        overlapFunction:                false,
        constrainTo:                    false,
        removeMargins:                  false,
        place:                          false,
        deferPlacement:                 false,
        axis:                           'x',
        elementsWithInteraction:        'input',
        revert:                         false,
        revertAfter:                    'stop',
        // revertIf:                       function(){ return true; },
        ignoreRightClick:               true
    };


    jQuery(window).on('resize', jQuery.debounce(500, true, slider.destroy));
    jQuery(window).on('resize', jQuery.debounce(500, false, slider.init));

    slider.init();
    slider.initNav();

};

jQuery(document).ready(function(event){

    // initialize slider

    if(typeof window.data == typeof undefined) window.data = {};
    window.data.contentslider = {};

    jQuery('*[data-slider]').each(function(i, slider){

       window.data.contentslider[jQuery(slider).data('slider').toLowerCase()] =  new ContentSlider(slider);

    });

});

/* 
 * Script for populating contextual sticky menu.
 * Saulius Vikerta
 * 
 * has dependency on jquery.sticky.js plugin
 * Please enqueue library in the certain page and use designated attributes in your page HTML elements:
 * - data-section-button (together with data-link-target, data-button-styling)
 * - data-section-title
 * 
 * Please find that it can generate links and button.
 */


(function($){

        function activateContextItem(elem) {
            //console.log(elem);
        }

        /*function isScrolledIntoView(elem) {
            var docViewTop = $(window).scrollTop();
            var docViewBottom = docViewTop + $(window).height();

            var elemTop = $(elem).offset().top;
            var elemBottom = elemTop + $(elem).height();

            return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
        }*/

        $(document).ready(function(){
            
             /*If jquery.sticky.js is not enqueued in the page the code will not be executed*/

             if(jQuery().sticky) {
                 //console.log ('Yes - have Sticky lib');
             

                function stickyMenuGenerateLinks($container) {

                    /*prevent double execution*/
                    if ($('ul.sticky-menu-block').length > 0) {
                        return;     // means the function has been already executed once
                    }

                    /*elements with "[data-section-title]" are discovered, then their scroll targets are found. Links are then generated*/ 

                    var $menuUlElement = $('<ul class="sticky-menu-block sticky-nav nav"></ul>');
                    $container.append($menuUlElement);

                    $('[data-section-title]').each(function(){
                        var anchorLink = $(this).find('.scroll-id-target').attr('id');
                        $menuUlElement.append('<li><a class="context-item" href="#'+ anchorLink + '" data-scrollto="' + anchorLink + '">' + $(this).attr('data-section-title') + '</a></li>');
                    });

                }

                function stickyMenuGenerateButton($container) {

                    /*prevent double execution*/
                    if ($('div.sticky-menu-block').length > 0) {
                        return;     // means the function has been already executed once
                    }

                    /*elements with "[data-section-button]" are discovered, then their scroll or ext.link targets are found. Links are then generated*/ 
                    
                    var $menuButtonElem = $('[data-section-button]');

                    if ($menuButtonElem.length == 0) {
                        return; // no buttons in the page, then quit this func
                    }

                    var menuButtonVal = $menuButtonElem.attr('data-section-button'),
                    menuButtonExtLink = $menuButtonElem.attr('data-link-target'),
                    menuButtonStyling = $menuButtonElem.attr('data-button-styling'),
                    menuButtonStylingAttr,
                    anchorButtonHrefAttr,
                    anchorButtonScrolltoAttr = ''; 

                    if (menuButtonExtLink == '') {
                        // nothing was passed, same as NULL
                        anchorButtonHrefAttr = '';  // to prevent printing 'undefined'
                        
                    } else if (typeof menuButtonExtLink === 'undefined') {
                        // this is scroll target
                        anchorButton = $menuButtonElem.find('.scroll-id-target').attr('id');
                        anchorButtonHrefAttr = 'href="#' + anchorButton + '"';
                        anchorButtonScrolltoAttr = 'data-scrollto="' + anchorButton + '"';
                    } else {
                        // this is external link
                        anchorButtonHrefAttr = 'href="' + menuButtonExtLink + '"';
                    }

                    if (typeof menuButtonStyling === 'undefined') {
                        menuButtonStylingAttr = 'class="button ghost"';
                    } else {
                        menuButtonStylingAttr = 'class="' + menuButtonStyling + '"';
                    }

                    // adding up to a button
                    $container.append('<div class="sticky-menu-block"><a ' + menuButtonStylingAttr + ' ' + anchorButtonHrefAttr + ' ' + anchorButtonScrolltoAttr + '>' + menuButtonVal + '</a></div>');

                }

                function stickyEnhanceBlogMenu($element, $header) {
                    var scrolledVal = $(document).scrollTop().valueOf(),
                        headerSizing;

                    if (typeof $header == undefined) {
                        headerSizing = 0;
                    } else {
                        headerSizing = $header.height();
                    }
                    
                    if (scrolledVal > headerSizing) {
                        $element.addClass('fixed');
                    } else $element.removeClass('fixed');

                }


                

                /*Initiate sticky-menu*/
                $('.sticky-menu').sticky();
                
                /*Populate sticky-menu*/
                var $menu = $('.sticky-menu .container');

                $stickyMenuMode = $('.sticky-menu').data('mode');

                switch ($stickyMenuMode) {
                    case 'blog':

                        $(document).ready(function(){
                            stickyEnhanceBlogMenu($('.blog-header'), $('header'));
                            $(window).on('scroll', function(){
                                stickyEnhanceBlogMenu($('.blog-header'), $('header'));
                            });
                        });

                        break;

                    default:    // ='landing'

                        stickyMenuGenerateLinks($menu); 
                        stickyMenuGenerateButton($menu);

                        
                        /*Marking active links*/

                        var $stickyNavigationItem = $('.sticky-nav a');

                        var headerHeight = $('.sticky-menu').height();
                        $('.scroll-id-target').css('top', -headerHeight - 25);

                        $stickyNavigationItem.click(function(){
                            $stickyNavigationItem.removeClass('active');
                            $(this).addClass('active');
                        });

                        $(window).on('scroll', function(){

                            setTimeout(function(){
                                $stickyNavigationItem.removeClass('active');
                            }, 1000);

                            /*$('[data-section-title]:visible').each(function(){
                                //activateContextItem(this);
                            })*/
                        });

                        // end of default switch case

                } // end of switch


            } else { 
                 //console.log ('Sticky lib not found');
            }

        });

    })(jQuery);


/* tabs handling (landing pages) */

(function($) {

    $(document).ready(function(){
        $('[data-tab-index]').click(function(){
            var tab_index = $(this).attr('data-tab-index');

            $('[data-tab-index]').removeClass('active');
            $(this).addClass('active');

            $('[data-tab-content]').removeClass('active');
            $('[data-tab-content="'+ tab_index +'"]').addClass('active');
        });

        $('.question, .expand-onclick').click(function( e ){
            if ((e.target !== this) && ($(e.target).parent()[0] !== this)) return;

            if( $(this).hasClass( 'active' ) ){
                $(this).removeClass('active');
            }
            else{
                $('.question, .expand-onclick').removeClass('active');
                $(this).addClass('active');
            }
        });
    });

})(jQuery);
var Tabs = function (container) {

    var tabs = this;

    tabs.id = jQuery(container).data('tabs');
    tabs.$container = jQuery(container);
    tabs.$tabs = tabs.$container.children('*[data-tab-id]');
    tabs.$relativeContent = jQuery('*[data-tab-when^="' + tabs.id + '."]');
    tabs.$links = jQuery('*[data-tab-link^="' + tabs.id + '."]');
    tabs.$links_next = jQuery('*[data-tab-action="' + tabs.id + '.next"]');
    tabs.$links_prev = jQuery('*[data-tab-action="' + tabs.id + '.previous"]');

    tabs.$tabsTitleContainer = jQuery('*[data-tabs-title="' + tabs.id + '"]');

    tabs.useHash = tabs.$container.data('hash-tabs') ? true : false;

    tabs.rotateinterval = false;
    tabs.rotating = jQuery(container).data('rotate');

    if (tabs.rotating) {

        tabs.rotateinterval = setInterval(function () {
            tabs.rotate();
        }, tabs.rotating * 1000);

        jQuery(tabs.$container, tabs.$links, tabs.$links_next, tabs.$links_prev).hover(
            function () {
                clearInterval(tabs.rotateinterval);
            },
            function () {
                tabs.rotateinterval = setInterval(function () {
                    tabs.rotate();
                }, tabs.rotating * 1000);
            }
        );


    }


    tabs.rotate = function () {
        tabs.showNext();
    };


    tabs.updateTabsTitle = function () {

        var tabTitle = tabs.$tabs.filter('.active').data('tab-title');

        if (tabs.$tabsTitleContainer.length && tabTitle) {

            tabs.$tabsTitleContainer.html(tabTitle);

        }


    };

    // show tab by id
    tabs.showTab = function (id) {

        var $t = tabs.$tabs.filter('[data-tab-id="' + id + '"]');

        if (!id || !$t.length) return false;


        tabs.$tabs.filter('.active').removeClass('active');
        $t.addClass('active');

        tabs.$links.filter('.active').removeClass('active');
        tabs.$links.filter('[data-tab-link="' + tabs.id + '.' + id + '"]').addClass('active');

        tabs.$relativeContent.filter('.active').removeClass('active');
        tabs.$relativeContent.filter('[data-tab-when="' + tabs.id + '.' + id + '"]').addClass('active');

        tabs.updateTabsTitle();

        if (tabs.useHash) {

            var hash = window.location.hash;
            var regexp = new RegExp("(#|&)" + tabs.id + "=[^&]+&*", "i");

            if (!hash.length) hash = '#';

            if (regexp.test(hash)) {

                var h = regexp.exec(hash)[0];
                var newH = h.substr(0, 1) + tabs.id + '=' + id;
                if (h.substr(-1, 1) == '&') newH += '&';

                hash = hash.replace(h, newH);

            } else {

                if (hash != '#') hash += '&';

                hash += tabs.id + '=' + id;

            }

            window.location.hash = hash;

        }

        tabs.$container.trigger('tabChanged');

    };

    // show next tab
    tabs.showNext = function () {

        var $currentTab = tabs.$tabs.filter('.active');
        var $nextTab = $currentTab.next('[data-tab-id]');

        if (!$nextTab.length)
            $nextTab = tabs.$tabs.first();

        tabs.showTab($nextTab.data('tab-id'));

    };


    // show previous tab
    tabs.showPrevious = function () {

        var $currentTab = tabs.$tabs.filter('.active');
        var $prevTab = $currentTab.prev('[data-tab-id]');

        if (!$prevTab.length)
            $prevTab = tabs.$tabs.last();

        tabs.showTab($prevTab.data('tab-id'));

    };


    // bind actions for tabs links
    tabs.$links.each(function (i, link) {

        jQuery(link).on('click', function () {

            if (jQuery(this).data('tab-toggle') && jQuery(this).data('tab-toggle') == 'self' && jQuery(this).is('.active')) {

                jQuery(this).removeClass('active');

            } else {

                var tabSelector = jQuery(this).data('tab-link').split('.', 2);
                var tabId = (tabSelector && tabSelector.length > 1) ? tabSelector[1] : null;

                tabs.showTab(tabId);

            }

        });

    });

    tabs.$links_next.on('click', function () {

        tabs.showNext();

    });

    tabs.$links_prev.on('click', function () {

        tabs.showPrevious();

    });


    if (tabs.useHash && window.location.hash.length) {

        jQuery.each(tabs.$tabs, function (i, tab) {

            var tabId = jQuery(tab).data('tab-id');

            var regexp = new RegExp("(#|&)" + tabs.id + "=" + tabId + "+&*", "i");

            if (regexp.test(window.location.hash)) {

                tabs.showTab(tabId);

            }

        });

    }


    return tabs;

};

var ResponsiveTabsMenu = function (container) {

    var tabs = this;

    tabs.tabsId = jQuery(container).data('tabsmenu-responsive');
    tabs.$tabsContent = jQuery('*[data-tabs="' + tabs.tabsId + '"]');

    tabs.$container = jQuery(container);
    tabs.$tabsContainer = tabs.$container.children('*[data-tabsmenu-tabs]');
    tabs.$tabs = tabs.$tabsContainer.children();

    tabs.scroll = function () {

        // var posCenter = containerWidth/2

        var $activeTab = tabs.$tabs.children().filter('.active').parent();

        var tabsLength = tabs.$tabs.length;
        var activeIndex = tabs.$tabs.index($activeTab);
        var containerWidth = tabs.$tabsContainer.outerWidth();
        var tabsWidth = tabs.$tabsContainer.get(0).scrollWidth;


        var scrollDiff = tabsWidth - containerWidth;
        var scrollPos = activeIndex * 100 / (tabsLength - 1);
        var scrollLeft = scrollDiff * scrollPos / 100;


        tabs.$tabsContainer.stop(true).animate({
            'scrollLeft': scrollLeft
        }, 600, 'easeOutQuart');

    }


    tabs.$tabsContent.on('tabChanged', tabs.scroll);

    jQuery(window).on('load resize', tabs.scroll);

    tabs.scroll();

};


jQuery(document).ready(function (event) {

    // initialize tabs

    if (typeof window.data == typeof undefined) window.data = {};
    window.data.tabs = {};

    jQuery('*[data-tabs]').each(function (i, tabs) {

        window.data.tabs[jQuery(tabs).data('tabs').toLowerCase()] = new Tabs(tabs);

    });


    // initialize responsive tabs menu

    jQuery('*[data-tabsmenu-responsive]').each(function (i, menu) {

        new ResponsiveTabsMenu(menu);

    });


});

jQuery(document).ready(function(event){

    jQuery('*[data-toggle-active]').each(function(i, el){

      jQuery(el).on('click', function(){

          if (jQuery(this).attr('data-open-all-blocks') == 1){
              // This is used in Customer support blocks for toggling all blocks at once

              jQuery('[data-open-all-blocks]').parent().toggleClass('active');
              jQuery('[data-open-all-blocks]').toggleClass('active');
              /*console.log (jQuery('[data-open-all-blocks]'));*/

          }
          else{

            if(jQuery(this).data('toggle-active') == 'parent'){
              // This is regular toggling element for accordions

                jQuery(this).parent().toggleClass('active');


            } else {

                jQuery(this).toggleClass('active');

            }

          }

          if(jQuery(this).data('toggle-callback')){

              eval(jQuery(this).data('toggle-callback'));

          }

      });

    });

    jQuery('*[data-toggle-open]').each(function(i, el){

      jQuery(el).on('click', function(){

          if(jQuery(this).data('toggle-open') == 'parent'){

              jQuery(this).parent().toggleClass('open');


          } else {

              jQuery(this).toggleClass('open');

          }

          if(jQuery(this).data('toggle-callback')){

              eval(jQuery(this).data('toggle-callback'));

          }

      });

    });

    jQuery('*[data-toggle-class]').each(function(i, el){

      jQuery(el).on('click', function(){

          var data = jQuery(this).data('toggle-class');
          var params = data.split(',');

          var addclass = params[0];
          var $target = (params.length > 1) ? jQuery(params[1].trim()) : jQuery(this);

          $target.toggleClass(tclass);

      });

    });

    jQuery('*[data-expand-plan-features]').each(function(i, el){

      jQuery(el).on('click', function(){

          var $this = jQuery(this);
          var $container = $this.closest('.plans-pricing-table');

          var col = $this.data('expand-plan-features');


          jQuery.each($container.attr('class').split(/\s/), function(i, iclass){

             if(iclass.match(/expanded-[0-9]+/)) $container.removeClass(iclass);

          });

          $container.addClass('expanded-' + col);

      });

    });

    jQuery('*[data-collapse-plan-features]').each(function(i, el){

      jQuery(el).on('click', function(){

          var $this = jQuery(this);
          var $container = $this.closest('.plans-pricing-table');

          jQuery.each($container.attr('class').split(/\s/), function(i, iclass){

             if(iclass.match(/expanded-[0-9]+/)) $container.removeClass(iclass);

          });

      });

    });


});

var Tooltips = new function(){

    function showTooltip(trigger){

        var $trigger= jQuery(trigger);
        var id = $trigger.data('tooltip');
        var $container = jQuery('[data-tooltip-content="' + id + '"]');
        var $arrow = $container.children('.triangle');

        var mapDelta = 12;
        var cssTopPosition = $trigger.height();

        if ( $trigger.hasClass ("loc-cont") ) {
            cssTopPosition = $trigger.height() - mapDelta;
        }


        $container.css({
            'position': 'fixed',
            /*'position': 'absolute',*/
            'top': $trigger.offset().top - jQuery(window).scrollTop() + cssTopPosition,
            /*'top': cssTopPosition,*/
            'left': $trigger.offset().left,
            /*'left': 0,*/
            'max-width': ''
        });

        /*console.log ('Tooltip trigger height - ' + $trigger.height() );*/

        $arrow.css({
            'width': $trigger.width(),
            'left': 0
        });

        $trigger.addClass('active');
        $container.addClass('active');

        var offset = $container.outerWidth() + $container.offset().left - jQuery(document).width() + 10

        if($container.outerWidth() > (jQuery(document).width() - 20)) {

            $container.css({
                'max-width': (jQuery(document).width() - 20),
                'left': 10
            });

            $arrow.css('left', $trigger.offset().left - 10);

        } else if(offset > 0) {

            $container.css('left', '-=' + offset);
            $arrow.css('left', offset);

        }


    };

    function hideTooltip(trigger){

        var $trigger= jQuery(trigger);
        var id = $trigger.data('tooltip');
        var $container = jQuery('[data-tooltip-content="' + id + '"]');

        $trigger.removeClass('active');
        $container.removeClass('active');

    };

    function hideAllTooltips(){

        jQuery('[data-tooltip]').each(function(i, trigger){

            hideTooltip(trigger);

        });

    };


    jQuery(document).on('mouseover', '[data-tooltip]', function(){
        if (screen.width >= 780 )
            showTooltip(this);
    });
    jQuery(document).on('mouseout', '[data-tooltip]', function(){hideTooltip(this)});
    jQuery(document).on('click', '[data-tooltip]', function(){
        /*console.log (screen.width);*/
        if (screen.width < 780 )
            showTooltip(this);
    });


    jQuery(window).on('scroll resize', hideAllTooltips);

    jQuery(document).on("scrollstart", hideAllTooltips);

};

var TrialPopup = new function(){

    this.toogleplans = function(block) {


        jQuery(".trial .plan-col").not('.'+block).hide();
        jQuery(".trial .plan-col.tac").show();

    };


    this.acceptTac = function(){

        jQuery('#acceptTac').prop('checked') ? jQuery('#addWhenAccepted').html('<a href="#" data-prevent onclick="" class="button primary">Order Now</a>') : jQuery('#addWhenAccepted').html(''); //time for show

    };

    this.cancelTac = function(){

        jQuery(".trial .plan-col.tac").hide();

        jQuery('#acceptTac').prop('checked', false);
        jQuery('#addWhenAccepted').html('');

        jQuery(".trial .plan-col").not('.tac').show();

    };

}

// https://github.com/positionly/Typist

if (typeof Typist == 'function') {

    // proceed only if main script is embeded

    var typist;

    typist = document.querySelector("#typist-element");

    new Typist(typist, {
      letterInterval: 30,
      textInterval: 3000,
      onSlide: function(text, options) {
        var suffix;
        suffix = options.typist.getAttribute("data-typist-suffix");
        //return log(text + " " + suffix);
      }
    });
}

window.ParsleyConfig = {
    validators: {
        domain: {
            fn: function (value) {

                if(value.length && typeof punycode != 'undefined') value = punycode.toASCII(value);

                return (/^(([a-z]{1})|([a-z]{1}[a-z]{1})|([a-z]{1}[0-9]{1})|([0-9]{1}[a-z]{1})|([a-z0-9][a-z0-9-]{1,61}[a-z0-9]))\.([a-z]{2,6}|[a-z0-9-]{2,30}\.[a-z]{2,3})$/i).test(value);

            },
            priority: 32
        },
        sldslist: {
            fn: function (value) {

                var allvalues = value.split(/[\n]/g) || [];
                var valid = true;

                var values = allvalues.map(function(val){

                    if(val.length && typeof punycode != 'undefined') val = punycode.toASCII(val);

                    if(val.trim().length) return val.trim();
                });

                for(var i=0; i<values.length; i++){

                    if(!(/^[a-z0-9]+([a-z0-9-]{0,61})+[a-z0-9]$/i).test(values[i])) valid = false;

                }

                return valid;

            },
            priority: 32
        }
    },
    i18n: {
        en: {
            domain: 'Please enter valid domain name',
            sldslist: 'Please enter valid domain names without tlds. Each name must be on separate line'
        }
    }
};
