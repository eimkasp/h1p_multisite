var JsonForm = function(data, url, method){

    var form = this;

    form.fields = [];
    form.$el = jQuery('<form/>');

    form.$el.attr('method', method || 'post');
    form.$el.attr('action', url);

    form.submit = function(){

        form.$el.appendTo(jQuery('body'));
        form.$el.submit();

    };


    function validateJson(json){

        if(typeof json == 'number' || !isNaN(json)){

            return false;

        }

        try {

            JSON.parse(json);
            return true;

        } catch (e) {

            return false;

        }

    };

    function getFields(params, name){

        var fields = [];

        for (var key in params)
        {
            if (params.hasOwnProperty(key))
            {
                var fieldName = (typeof name != typeof undefined) ? name + '[' + key + ']' : key;

                if((typeof params[key] == 'string' && validateJson(params[key])) || typeof params[key] == 'object'){

                    fields = fields.concat(getFields(params[key], fieldName));

                } else {

                    var hiddenField = document.createElement('input');

                        hiddenField.setAttribute('type',  'hidden');
                        hiddenField.setAttribute('name',  fieldName);
                        hiddenField.setAttribute('value', params[key]);

                    fields.push(hiddenField);

                }
            }
        }

        return fields;

    };


    var fields = getFields(data);

    for (var index in fields)
    {
        form.$el.append(fields[index]);
    }

    return form;

}










