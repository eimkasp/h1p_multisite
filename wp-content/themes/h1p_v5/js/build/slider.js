var ContentSlider = function(container){

    var slider = this;

    slider.id = jQuery(container).data('slider');
    slider.$container = jQuery(container);
    slider.$slidesContainer = slider.$container.find('[data-slides]');
    slider.$slides = slider.$slidesContainer.children('.slide');
    slider.$navLinks = jQuery('*[data-slider-nav="'+slider.id+'"]');

    slider.updateNav = function(index){

        slider.$navLinks.filter('.active').removeClass('active');
        slider.$navLinks.filter('[data-slider-slide="'+index+'"]').addClass('active');

    };

    slider.showSlide = function(index){

        var $activeSlide = slider.$slides.eq(index);

        slider.$slides.filter('.active').removeClass('active');
        $activeSlide.addClass('active');

        slider.slideTo($activeSlide.position().left + slider.$container.scrollLeft());

        slider.updateNav(index);
    };

    slider.getClosestSlide = function(scrollLeft){

        var containerScrollLeft = slider.$container.scrollLeft();
        var $closestSlide = slider.$slides.first();
        var closestLeft = $closestSlide.position().left + containerScrollLeft;
        var closestDif = Math.abs(closestLeft - scrollLeft);

        slider.$slides.each(function(i, slide){

            var $slide = jQuery(slide);
            var slideLeft = $slide.position().left + containerScrollLeft;
            var slideDif = Math.abs(slideLeft - scrollLeft);

            if(slideDif < closestDif){

                $closestSlide = $slide;
                closestLeft = slideLeft;
                closestDif = slideDif;

            }

        });

        return $closestSlide;


    };

    // animate sliding
    slider.slideTo = function(scrollLeft){

        slider.$container.stop(true, true).animate({
            'scrollLeft': scrollLeft
        }, 800);


    };

    // stops easing on touchstart/mousdown
    slider.onMoveStart = function(){

        slider.$container.stop(true, false);

    };


    // handles drag events
    slider.onMoveTo = function(x,y){

        var containerScrollLeft = slider.$container.scrollLeft();
        var direction = x.split('=');
        var scrollLeft = containerScrollLeft - (direction[0]+direction[1]);
        var final = slider.$slidesContainer.is('.pep-start') ? false : true;
        var duration = final ? 800 : 0;
        var maxScroll = slider.$slidesContainer.width() - slider.$container.width();


        if(scrollLeft < 0) scrollLeft = 0;
        if(scrollLeft > maxScroll) scrollLeft = maxScroll;

        if(final) {

            var $closestSlide = slider.getClosestSlide(scrollLeft);

            scrollLeft = $closestSlide.position().left + containerScrollLeft;

            slider.$slides.filter('.active').removeClass('active');
            $closestSlide.addClass('active');


            slider.updateNav(slider.$slides.index($closestSlide));

            slider.slideTo(scrollLeft);


        } else {

            slider.$container.stop(true, true);

            slider.$container.scrollLeft(scrollLeft);

        }



    };


    slider.onMoveStop = function(event, pep){

        // disable css animations
        pep.cssAnimationsSupport = false;

    };

    slider.init = function(){

        if(slider.$slides.filter(':visible').first().width() == slider.$container.width() && slider.$slides.length > 1){

            var slideWidth = slider.$container.width();

            // fix slides and container widths
            slider.$slides.css('width', slideWidth);
            slider.$slidesContainer.css('width', slider.$slides.length*slideWidth);

            // add slider class
            slider.$container.addClass('slider-active');

            // bind draggable
            slider.$slidesContainer.pep(slider.pepOptions);

            slider.$container.on('mousedown.slider', slider.onMoveStart);

            slider.$container.scrollLeft(slider.$slides.filter('.active').position().left + slider.$container.scrollLeft());

        }

    };

    slider.destroy = function(){

        // remove fixed widths
        slider.$slides.css('width', '');
        slider.$slidesContainer.css('width', '');

        // remove slider class
        slider.$container.removeClass('slider-active');

        // unbind draggable
        jQuery.pep.unbind(slider.$slidesContainer);

        slider.$container.off('mousedown.slider', slider.onMoveStart);

    };

    // initialize slider navigation
    slider.initNav = function(){

        slider.$navLinks.each(function(i, link){

            jQuery(link).on('click', function(){

                slider.showSlide(jQuery(this).data('slider-slide'));

            });

        });

    };

    slider.pepOptions = {
        // initiate:                       function(){},
        start:                          slider.onMoveStart,
        // drag:                           function(){},
        stop:                           slider.onMoveStop,
        // easing:                         function(){},
        // rest:                           function(){},
        moveTo:                         slider.onMoveTo,
        callIfNotStarted:               ['stop', 'rest'],
        startThreshold:                 [0,0],
        grid:                           [1,1],
        debug:                          false,
        activeClass:                    'pep-active',
        multiplier:                     1,
        velocityMultiplier:             2.5,
        shouldPreventDefault:           true,
        allowDragEventPropagation:      true,
        stopEvents:                     '',
        hardwareAccelerate:             false,
        useCSSTranslation:              false,
        disableSelect:                  false,
        cssEaseString:                  "",
        cssEaseDuration:                0,
        shouldEase:                     true,
        droppable:                      false,
        droppableActiveClass:           'pep-dpa',
        overlapFunction:                false,
        constrainTo:                    false,
        removeMargins:                  false,
        place:                          false,
        deferPlacement:                 false,
        axis:                           'x',
        elementsWithInteraction:        'input',
        revert:                         false,
        revertAfter:                    'stop',
        // revertIf:                       function(){ return true; },
        ignoreRightClick:               true
    };


    jQuery(window).on('resize', jQuery.debounce(500, true, slider.destroy));
    jQuery(window).on('resize', jQuery.debounce(500, false, slider.init));

    slider.init();
    slider.initNav();

};

jQuery(document).ready(function(event){

    // initialize slider

    if(typeof window.data == typeof undefined) window.data = {};
    window.data.contentslider = {};

    jQuery('*[data-slider]').each(function(i, slider){

       window.data.contentslider[jQuery(slider).data('slider').toLowerCase()] =  new ContentSlider(slider);

    });

});
