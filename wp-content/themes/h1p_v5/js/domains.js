/* --------------------------------------------------------------- *\
    Toggle Tlds Pricing table
\* --------------------------------------------------------------- */

jQuery(document).ready(function(event){

    // should be even!!!
    tlds_to_show = 16;

    jQuery('.tbl-col:gt(' + (tlds_to_show+1) + ')').hide();

    jQuery('.expand-collapse-exts.expand').on('click', function(e){

        jQuery(this).hide();
        jQuery('.expand-collapse-exts.collapse').show();

        jQuery('.tbl-col').show();

    });

    jQuery('.expand-collapse-exts.collapse').on('click', function(e){

        jQuery(this).hide();
        jQuery('.expand-collapse-exts.expand').show();


        jQuery('.tbl-col:gt(' + (tlds_to_show+1) + ')').hide();

    });

});


/* --------------------------------------------------------------- *\
    Domainsearch app
\* --------------------------------------------------------------- */

angular.module('domainsearch', ['debounce']).

directive('multilinePlaceholder', function(){

    return {
        restrict: 'A',
        link: function(scope, element, attr) {

            var placeholder = attr.multilinePlaceholder.replace(/\\n/g,'\n');

            element.attr('placeholder', placeholder);

        }

    };

}).

directive('modelElement', function(){

    return function(scope, element, attr) {

        scope[attr.modelElement] = element;

    }

}).

directive('parsleyValidate', function(){

    return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModelController) {

            if(ngModelController) ngModelController.$validators = {};

            element.data('parsley', new Parsley(element));

        }
    }

}).


directive('domainsTextarea', function(){

    return {
        require: 'ngModel',
        restrict: 'A',
        scope: { ngModel: '=' },
        link: function(scope, element, attr, ngModelController) {

            ngModelController.$formatters.unshift(function(valueFromModel) {
                var value = valueFromModel || [];
                return value.join('\n');
            });

            ngModelController.$parsers.push(function(valueFromInput) {
                return valueFromInput.split(/[\n]/g) || [];
            });
        }

    };

}).

directive('onEnterKey', function($parse){

    return {
        restrict: 'A',
        compile: function($element, attr) {

            var fn = $parse(attr['onEnterKey'], null, true);

            return function ngEventHandler(scope, element) {

                element.on('keypress', function(event) {

                    if(event.which == 13){

                        var callback = function(){
                            fn(scope, {$event:event});
                        };

                        scope.$apply(callback);

                    }

                });

            };

        }

    };

}).

factory('tlds', function () {

    var tlds = {};

    jQuery.each(window.WhmcsTlds, function(index, tld){

        tlds[tld.tld] = {
            name: tld.tld,
            sortindex: index,
            prices:{}
        };

        jQuery.each(tld.prices, function(type, prices){

            tlds[tld.tld].prices[type] = [];

            jQuery.each(prices, function(period, price){

                tlds[tld.tld].prices[type].push(jQuery.extend(true,{period:period},price));

            });

        });

    });

    return tlds;

}).

factory('socket', function ($rootScope) {

    var socket = io( WHOIS_URL );

    return {

        on: function(eventName, callback) {

            socket.on(eventName, function() {
                var args = arguments;
                $rootScope.$apply(function() {
                    callback.apply(socket, args);
                });
            });

        },

        emit: function (eventName, data, callback) {

            socket.emit(eventName, data, function() {
                var args = arguments;
                $rootScope.$apply(function(){
                    if (callback) callback.apply(socket, args);
                });
            });

        }

    };

}).

controller('searchCtrl', function($scope, tlds){

    $scope.tlds = JSON.parse(JSON.stringify(tlds));
    $scope.domain;
    $scope.domainSearchInput;

    $scope.submitSearch = function(){

        if($scope.domainSearchInput.length && $scope.domainSearchInput.data('parsley')){

            if($scope.domainSearchInput.data('parsley').validate() !== true){ return; }
            else { $scope.domainSearchInput.data('parsley').reset(); }

        };

        var sep = $scope.domain.indexOf('.');

        var sld = (sep > -1) ? $scope.domain.substr(0, sep) : domain;
        var tld = (sep > -1) ? $scope.domain.substr(sep+1) : null;

        var searchSlds = (sld && sld.length) ? [sld.trim()] : [];
        var searchTlds = (tld) ? [tld.trim()] : [];

        $scope.$emit('search', {sld:searchSlds, tld:searchTlds});

    };

}).

controller('bulksearchCtrl', function($scope, tlds){

    $scope.tlds = JSON.parse(JSON.stringify(tlds));
    $scope.slds = [];
    $scope.tldstoggleall = false;
    $scope.domainSearchInput;

    $scope.toggleAllTlds = function(){

        jQuery.each($scope.tlds, function(tld, params){
            params.selected = $scope.tldstoggleall;
        });

    };

    $scope.submitSearch = function(){

        if($scope.domainSearchInput.length && $scope.domainSearchInput.data('parsley')){

            if($scope.domainSearchInput.data('parsley').validate() !== true){ return; }
            else { $scope.domainSearchInput.data('parsley').reset(); }

        };

        var searchTlds = jQuery.map($scope.tlds, function(tld, ti){
            if(tld.selected) return tld.name;
        });

        var searchSlds = jQuery.map($scope.slds, function(sld, si){
            if(sld.trim().length) return sld.trim();
        });

        $scope.$emit('bulksearch', {slds: searchSlds, tlds: searchTlds});

    };

}).

controller('bulktransferCtrl', function($scope, tlds){

    $scope.tlds = JSON.parse(JSON.stringify(tlds));
    $scope.slds = [];
    $scope.tldstoggleall = false;
    $scope.domainSearchInput;

    $scope.toggleAllTlds = function(){

        jQuery.each($scope.tlds, function(tld, params){
            params.selected = $scope.tldstoggleall;
        });

    };

    $scope.submitSearch = function(){

        if($scope.domainSearchInput.length && $scope.domainSearchInput.data('parsley')){

            if($scope.domainSearchInput.data('parsley').validate() !== true){ return; }
            else { $scope.domainSearchInput.data('parsley').reset(); }

        };


        var searchTlds = jQuery.map($scope.tlds, function(tld, ti){
            if(tld.selected) return tld.name;
        });

        var searchSlds = jQuery.map($scope.slds, function(sld, si){
            if(sld.trim().length) return sld.trim();
        });

        $scope.$emit('bulktransfer', {slds: searchSlds, tlds: searchTlds});

    };

}).

controller('resultsCtrl', function($scope, $rootScope, $filter, debounce, tlds, socket){

    $scope.searchResultsVisible = false;
    $scope.searchMethod = 'register';

    $scope.pager = {
        step: 10,
        limit: 0,
        total: 0
    }

    $scope.primary = false;
    $scope.defaultTlds = ['com', 'net', 'org', 'eu', 'lt', 'biz', 'info']
    $scope.domains = [];

    $scope.search = {};
    $scope.filters = {};


    // start new search

    $scope.newSearch = function(searchSlds, searchTlds, searchTaken){

        socket.emit('clearQueue');

        $scope.searchMethod = searchTaken ? 'transfer' : 'register';

        // reset
        $scope.domains = [];
        $scope.primary = false;
        $scope.pager.limit = $scope.pager.step;
        $scope.filters.price = $scope.filters.priceinput = null;

        // check if search is for only one domain
        if(searchSlds.length == 1 && searchTlds.length == 1){
            $scope.primary = {sld: searchSlds[0], tld: searchTlds[0]};
            jQuery.unique(jQuery.merge(searchTlds, $scope.defaultTlds));
        }

        // construct tlds
        searchTlds = (searchTlds.length) ? searchTlds : $scope.defaultTlds;
        var sTlds = JSON.parse(JSON.stringify(tlds));
        jQuery.each(sTlds, function(i, tld){
            tld.selected = (searchTlds.indexOf(tld.name) > -1) ? true : false;
        });



        // setup new search
        $scope.search.slds = JSON.parse(JSON.stringify(searchSlds));
        $scope.search.tlds = sTlds;

        // trigger new search
        $scope.search();

        // toggle results container visibility
        if($scope.search.slds.length > 0 && searchTlds.length > 0){

            $scope.searchResultsVisible = true;
            scrollTo('.content.domains');

        }

    };


    // render domains list

    $scope.search = debounce(function(){

        var priceFilter = parseFloat($scope.filters.price) || false;


        // reset already constructed domains

        jQuery.each($scope.domains, function(index, domain){

            domain.visible = domain.selected ||  domain.primary || false;

        });


        // loop throw tlds and domains

        jQuery.each($scope.search.tlds || [], function(ti, tld){

            var tldPrice = (tld.prices[$scope.searchMethod] && tld.prices[$scope.searchMethod].length) ? parseFloat(tld.prices[$scope.searchMethod][0].main + '.' + tld.prices[$scope.searchMethod][0].sub) : 0;

            if(
                tldPrice && tld.selected && priceFilter && tldPrice <= priceFilter ||
                tldPrice && tld.selected && !priceFilter
            ){

                jQuery.each($scope.search.slds || [], function(si, sld){

                    var existing = $filter('filter')($scope.domains, {sld:sld, tld: tld.name}, true);
                    var primary = ($scope.primary && $scope.primary.sld == sld && $scope.primary.tld == tld.name);

                    if(existing.length){

                        var d = existing[0];

                    } else {

                        var d = {
                            name: sld + '.' + tld.name,
                            sld: sld,
                            tld: tld.name,
                            selected: false,
                            prices: JSON.parse(JSON.stringify(tld.prices[$scope.searchMethod])),
                            period: 0,
                            sortindex: (parseInt(tld.sortindex)+1) * $scope.search.slds.length,
                            visible: true,
                            primary: primary
                        }

                        $scope.domains.push(d);

                    }

                    d.visible = true;
                    d.primary = primary;

                    if(primary) {

                        $scope.primary = d
                        $scope.loadDomain(d.sld, d.tld);

                    };


                });

            }

        });


        $scope.sortDomains();


        // update pager

        $scope.pager.total = $filter('filter')($scope.domains, {visible:true}, true).length;
        $scope.pager.limit = Math.min($scope.pager.total, Math.max($scope.pager.limit, $scope.pager.step));


        $scope.searchResultsVisible = $scope.domains.length > 0;

    }, 100);


    // expand domains list

    $scope.loadMore = function(){

        $scope.pager.limit = Math.min($scope.pager.limit+$scope.pager.step, $scope.pager.total);

    };


    // push message to socket

    $scope.loadDomain = function(sld, tld){

        var existing = $filter('filter')($scope.domains, {sld:sld, tld: tld}, true);

        if(!existing.length || !existing[0].loaded){

            socket.emit('checkAvailability', {
                sld: sld,
                tld: tld
            });

        }

    };

    // sort domains by tld and domain

    $scope.sortDomains = function(){

        $scope.domains = $filter('orderBy')($scope.domains, function(domain){

            return domain.primary ? 0 : parseInt(domain.sortindex)+1;

        });

    };


    $scope.submit = function(){

        var url = WHMCS_ADD_DOMAINS_URL + '?a=add&laguage=' + WHMCS_LANG + '&currency=' + WHMCS_CURR + '&domain=' + $scope.searchMethod;

        var data = {
            domains: [],
            domainsregperiod: {}
        };


        jQuery.each($scope.domains, function(index, domain){

            if(domain.selected){

                data.domains.push(domain.sld + '.' + domain.tld);
                data.domainsregperiod[domain.sld + '.' + domain.tld] = domain.prices[domain.period].period;

            }


        });

        if(data.domains.length){

            new JsonForm(data, url).submit();

        }

    };


    // search events

    $rootScope.$on('search', function(event, args){
        $scope.newSearch(args.sld, args.tld);
    });

    $rootScope.$on('bulksearch', function(event, args){
        $scope.newSearch(args.slds, args.tlds);
    });

    $rootScope.$on('bulktransfer', function(event, args){
        $scope.newSearch(args.slds, args.tlds, true);
    });


    // watch for changes

    $scope.$watch('search.tlds', $scope.search, true);
    $scope.$watch('search.domains', $scope.search, true);
    $scope.$watch('filters.price', $scope.search, true);



    socket.on('checkAvailability', function(data){

        jQuery.each($filter('filter')($scope.domains, {sld:data.sld, tld:data.tld}, true), function(index, domain){

            domain.loaded = true;
            domain.available = ($scope.searchMethod == 'register') ? data.available || false : (data.available === false) ? true : false;

        });

    });


});



jQuery(document).ready(function(){

    angular.bootstrap(jQuery('#domains-search'), ['domainsearch']);

});
