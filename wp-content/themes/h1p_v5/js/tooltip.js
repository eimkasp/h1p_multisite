var Tooltips = new function(){

    function showTooltip(trigger){

        var $trigger= jQuery(trigger);
        var id = $trigger.data('tooltip');
        var $container = jQuery('[data-tooltip-content="' + id + '"]');
        var $arrow = $container.children('.triangle');

        $container.css({
            'position': 'absolute',
            'top': $trigger.offset().top - jQuery(window).scrollTop() + $trigger.height(),
            'left': $trigger.offset().left,
            'max-width': ''
        });

        $arrow.css({
            'width': $trigger.width(),
            'left': 0
        });


        $container.addClass('active');

        var offset = $container.outerWidth() + $container.offset().left - jQuery(document).width() + 10

        if($container.outerWidth() > (jQuery(document).width() - 20)) {

            $container.css({
                'max-width': (jQuery(document).width() - 20),
                'left': 10
            });

            $arrow.css('left', $trigger.offset().left - 10);

        } else if(offset > 0) {

            $container.css('left', '-=' + offset);
            $arrow.css('left', offset);

        }


    };

    function hideTooltip(trigger){

        var $trigger= jQuery(trigger);
        var id = $trigger.data('tooltip');
        var $container = jQuery('[data-tooltip-content="' + id + '"]');

        $container.removeClass('active');

    };

    function hideAllTooltips(){

        jQuery('[data-tooltip]').each(function(i, trigger){

            hideTooltip(trigger);

        });

    };


    jQuery(document).on('mouseover', '[data-tooltip]', function(){showTooltip(this)});
    jQuery(document).on('mouseout', '[data-tooltip]', function(){hideTooltip(this)});

    jQuery(window).on('scroll resize', hideAllTooltips);

    jQuery(document).on("scrollstart", hideAllTooltips);

};
