var scrollTo = function(target, stopOnAction, container){

    var $container = container ? jQuery(container) : jQuery("html, body");
    var $target = jQuery(target);

    if($target.length){

        var to = $target.offset().top;

        $container.animate({

            scrollTop: to + 'px'

        },{

            duration: 800,
            easing: 'easeInOutQuart',
            start: function () {

                if(stopOnAction){

                    setTimeout(function () {

                        jQuery(window).on('mousedown.scrolling DOMMouseScroll.scrolling mousewheel.scrolling keyup.scrolling', function () {

                            $container.stop(true);

                        });

                    }, 100);

                }

            },

            always: function () {

                jQuery(window).off('mousedown.scrolling DOMMouseScroll.scrolling mousewheel.scrolling keyup.scrolling');

            }

        });

    }


};


jQuery(document).ready(function(event){

    jQuery('*[data-scrollto]').each(function(i, el){

        jQuery(el).on('click', function(e){

            var target = jQuery(this).data('scrollto');
            var $target = target == 'self' ? jQuery(this) : jQuery('#' + target);

            if(target == 'self' && $target.is('.active') || target != 'self' && $target.length){

                e.preventDefault();

                scrollTo($target, true);

            }

        });

    });

});
