window.ParsleyConfig = {
    validators: {
        domain: {
            fn: function (value) {

                if(value.length && typeof punycode != 'undefined') value = punycode.toASCII(value);

                return (/^(([a-z]{1})|([a-z]{1}[a-z]{1})|([a-z]{1}[0-9]{1})|([0-9]{1}[a-z]{1})|([a-z0-9][a-z0-9-]{1,61}[a-z0-9]))\.([a-z]{2,6}|[a-z0-9-]{2,30}\.[a-z]{2,3})$/i).test(value);

            },
            priority: 32
        },
        sldslist: {
            fn: function (value) {

                var allvalues = value.split(/[\n]/g) || [];
                var valid = true;

                var values = allvalues.map(function(val){

                    if(val.length && typeof punycode != 'undefined') val = punycode.toASCII(val);

                    if(val.trim().length) return val.trim();
                });

                for(var i=0; i<values.length; i++){

                    if(!(/^[a-z0-9]+([a-z0-9-]{0,61})+[a-z0-9]$/i).test(values[i])) valid = false;

                }

                return valid;

            },
            priority: 32
        }
    },
    i18n: {
        en: {
            domain: 'Please enter valid domain name',
            sldslist: 'Please enter valid domain names without tlds. Each name must be on separate line'
        }
    }
};
