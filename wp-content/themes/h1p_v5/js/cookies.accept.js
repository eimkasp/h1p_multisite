(function(){

    var cookiesPolicyVersion = 1;

    function accepCookiesPolicy(){

        var d = new Date(); d.setTime(d.getTime() + (365*24*60*60*1000));

        document.cookie = "h1pcp="+cookiesPolicyVersion+"; expires=" + d.toUTCString() + "; path=/";

        jQuery('#cookies-policy-accept').hide();
        jQuery('body').removeClass('cookies-alert-visible');

    }

    jQuery(document).ready(function(){

        var cookies = document.cookie.split(';');
        var accepted = false;

        jQuery.each(cookies, function(i, cookie){

            if(cookie.trim() == 'h1pcp=' + cookiesPolicyVersion) accepted = true;

        });

        if(!accepted){

            jQuery('body').addClass('cookies-alert-visible');
            jQuery('#cookies-policy-accept').show();

        }

    });

    window.accepCookiesPolicy = accepCookiesPolicy;

})();
