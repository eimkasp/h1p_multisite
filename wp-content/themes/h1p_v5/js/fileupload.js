(function(){

    function getSizeString(bytes){

        var units = {
            'B': 1,
            'KB':1024,
            'MB':1048576,
            'GB':1073741824
        };

        var val = bytes;
        var unit = 'B';

        jQuery.each(units, function(key, mult){

            if((bytes/mult) > 1) {

                val = bytes/mult;
                unit = key;

            }

        });

        return Math.round(val*100)/100 + ' ' + unit;

    }

    function removeValidationMessages($container){

        $container.find('.wpcf7-not-valid-tip').remove();
        $container.removeClass('wpcf7-not-valid');

    }


    jQuery('[data-file-upload]').each(function(i, el){

        var $container = jQuery(el);

        var $input = $container.find('input[type="file"]');
        var $filename = $container.find('[data-file-upload-filename]');
        var $filename_nofile = $filename.text();
        var $filesize = $container.find('[data-file-upload-filesize]');
        var $filesize_nofile = $filesize.text();
        var $btn_remove = $container.find('[data-file-upload-removefile]');

        $input.on('change', function(){

            if($input[0]['files'] && $input[0]['files'].length){

                var filedata = $input[0]['files'][0];

                $filename.text(filedata['name'] ? filedata['name'] : 'Undefined');
                $filesize.text(filedata['size'] ? getSizeString(filedata['size']) : 'Undefined');

                $container.addClass('file-selected');

            } else {

                $filename.text($filename_nofile);
                $filesize.text($filesize_nofile);

                $container.removeClass('file-selected');
                removeValidationMessages($container);

            }

        });

        $btn_remove.click(function(){

            var $newInput = $input.clone(true, true);

            $input.replaceWith($newInput);

            $input = $newInput;

            $input.trigger('change');

        });

        setInterval(function(){

            if($input.parent().find('.wpcf7-not-valid-tip').length) {

                $container.children('.wpcf7-not-valid-tip').remove();
                $container.addClass('wpcf7-not-valid');

                jQuery.each($input.parent().find('.wpcf7-not-valid-tip'), function(i, el){

                    $container.append(jQuery(el)[0].outerHTML);

                });

            } else {

                removeValidationMessages($container);

            }

            $input.trigger('change');

        }, 1000)

    });

})();
