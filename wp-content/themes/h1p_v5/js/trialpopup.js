var TrialPopup = new function(){

    this.toogleplans = function(block) {


        jQuery(".trial .plan-col").not('.'+block).hide();
        jQuery(".trial .plan-col.tac").show();

    };


    this.acceptTac = function(){

        jQuery('#acceptTac').prop('checked') ? jQuery('#addWhenAccepted').html('<a href="javascript:void(0)" onclick="" class="button primary">Order Now</a>') : jQuery('#addWhenAccepted').html(''); //time for show

    };

    this.cancelTac = function(){

        jQuery(".trial .plan-col.tac").hide();

        jQuery('#acceptTac').prop('checked', false);
        jQuery('#addWhenAccepted').html('');

        jQuery(".trial .plan-col").not('.tac').show();

    };

}
