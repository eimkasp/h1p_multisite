// initialize Magnific Popups
jQuery(document).ready(function(){

    var visiblePopups = [];

    jQuery('*[data-popup]').each(function(i, popup){

        var $popup = jQuery(popup);

        var id = $popup.data('popup-id') || null;
        var type = $popup.data('popup-type') || 'image';
        var src = $popup.data('popup');

        if(src){

            $popup.magnificPopup({
                items: {
                    src: src,
                    type: type
                },
                iframe: {
                    patterns: {
                        youtube: {
                            index: 'youtube.com',
                            id: 'v=',
                            src: '//www.youtube.com/embed/%id%?autoplay=1&rel=0&wmode=opaque'
                        }
                    }
                }
            });

            if(id && window.location.hash && window.location.hash.replace(/^#/, '') == id && visiblePopups.indexOf(id) == -1){

                visiblePopups.push(id);

            }

        }


    });

    jQuery.each(visiblePopups, function(i, id){

        jQuery('[data-popup-id="' + id + '"]').first().click();

    });


});
