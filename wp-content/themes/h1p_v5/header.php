<!DOCTYPE html>
<!--[if lt IE 7]>
<html lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>
<html lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>
<html lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!-->
<html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta charset="<?php bloginfo('charset'); ?>">
  <title><?php
      //        separator, print immediately, separator position
      wp_title('·', TRUE, 'right');
      bloginfo('name');
      ?></title>
  <meta name="viewport"
        content="width=device-width, initial-scale=1.0, initial-scale=1, maximum-scale=1, user-scalable=no"/>

  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

  <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.ico"/>

  <link rel="stylesheet" id="style-css-font-default"
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&subset=latin,latin-ext&ver=4.4.2"
        type="text/css" media="all">
  <link rel="stylesheet" id="style-css-font-headers"
        href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&subset=latin,latin-ext&ver=4.4.2"
        type="text/css" media="all">

    <?php
    // Functions
    require_once('functions.php');

    // Styles
    wp_enqueue_style('style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('page-builder', get_template_directory_uri() . '/assets/css/page-builder.css');

    ?>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

    <?php

    // Scripts
    wp_deregister_script('jquery');

    //vendor
    wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-2.2.4.min.js', false, '2.2.4');
    wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);
    wp_enqueue_script('magnific-popup', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js', ['jquery'], false, true);
    wp_enqueue_script('es6-promise', 'https://cdnjs.cloudflare.com/ajax/libs/es6-promise/4.0.5/es6-promise.auto.min.js', null, '4.0.5', true);

    // concated and minified version
    wp_enqueue_script('h1p.build', get_template_directory_uri() . '/js/build/h1p.min.js', ['jquery'], false, true);

    ?>

    <?php
    wp_head();
    ?>

  <!--[if lt IE 9]>
  <link type="text/css" rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style_ie8.css" media="all"/>
  <script type="text/javascript"
          src="<?php bloginfo('template_directory'); ?>/js/vendor/modernizr-2.6.2.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/vendor/es5-shim.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/vendor/es5-sham.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/vendor/selectivizr-min.js"></script>
  <![endif]-->

</head>

<?php

$additional_body_class = '';
if (function_exists("get_main_header_type")) {
    $additional_body_class = get_main_header_type();
}
// add this var to body_class()

?>

<body <?php body_class($additional_body_class); ?><?php echo(defined('TOS') && TOS ? ' style="padding-top:0;"' : ''); ?>>

<!-- Google Tag Manager -->
<noscript>
  <iframe src="//www.googletagmanager.com/ns.html?id=GTM-PQHBT5"
          height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<script>(function (w, d, s, l, i) {
    w[l] = w[l] || [];
    w[l].push({
      'gtm.start': new Date().getTime(), event: 'gtm.js'
    });
    var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
    j.async = true;
    j.src =
        '//www.googletagmanager.com/gtm.js?id=' + i + dl;
    f.parentNode.insertBefore(j, f);
  })(window, document, 'script', 'dataLayer', 'GTM-PQHBT5');</script>
<!-- End Google Tag Manager -->


<!--[if lt IE 9]>
<p class="browsehappy"><i class="fa fa-exclamation-triangle"></i>&nbsp; You are using an <strong>outdated</strong>
  browser. Please <a target="_blank" href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</p>
<![endif]-->
<?php include('htmlblocks/cookies.php'); ?>
<?php
if (!defined('SKIP_HEADER_NAV')) {
    if (!defined('HEADER') || (defined('HEADER') && HEADER)) {
        include('header-navigation.php');
    }
}
?>
