<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Client area
*/

wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, NULL);

$custom_fields = get_post_custom();
$abuse_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

get_header();

?>

<article class="page clientarea">

    <header class="headline clientarea no-pad">

        <div class="container adjust-vertical-center">
            <h1 class="page-title"><?php _e('Client Area Overview')?></h1>
            <div class="title-descr"><?php _e('Designed exclusively for our clients.')?></div>

    <?php
        // Revolution slider
        putRevSlider('clientarea', 'clientarea');
    ?>
        </div>

    </header> <!-- end of .headline -->

    <section class="main page-content">


        <section class="ca-features extra-pad">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Easy Account Management')?></h2>
                </div>

                <div class="container-flex as-row">

                    <div class="block-col width-md-3-6 order-1 center-sm text-left-md text-left-lg">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/client_area/ca_landing_feat_1.png" alt="Multi language">
                    </div>
                    <div class="block-col width-md-3-6 order-2 p-h-30 p-v-60">
                        <h3 class="block-title-small"><?php _e('Multi-Language Support')?></h3>
                        <p><?php _e('Host1Plus Client Area is available in 4 languages: English, Portuguese, Spanish and Chinese.'); ?></p>
                    </div>

                    <div class="block-col width-md-3-6 order-4 center-sm text-right-md text-right-lg">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/client_area/ca_landing_feat_2.png" alt="Full Account Information">
                    </div>
                    <div class="block-col width-md-3-6 order-3 p-h-30 p-v-60">
                        <h3 class="block-title-small"><?php _e('Full Account Information')?></h3>
                        <p><?php _e('Check your account details, change your password, review your email history and access all information related to your services in case you lose any of your emails.'); ?></p>
                    </div>

                    <div class="block-col width-md-3-6 order-5 center-sm text-left-md text-left-lg">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/client_area/ca_landing_feat_3.png" alt="Clever Invoice Management">
                    </div>
                    <div class="block-col width-md-3-6 order-6 p-h-30 p-v-60">
                        <h3 class="block-title-small"><?php _e('Clever Invoice Management')?></h3>
                        <p><?php printf(__('Pay your invoices manually or set up an authorized payment to pay for your services automatically. Not sure how it works? %s Learn more%s.'), '<a href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1276/9/how-do-i-manage-authorized-payments">', '</a>' ); ?></p>
                    </div>

                    <div class="block-col width-md-3-6 order-8 center-sm text-right-md text-right-lg">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/client_area/ca_landing_feat_4.png" alt="Affiliate Stats" >
                    </div>
                    <div class="block-col width-md-3-6 order-7 p-h-30 p-v-60">
                        <h3 class="block-title-small"><?php _e('Affiliate Stats')?></h3>
                        <p><?php _e('Already an Affiliate? Track the status of your commissions directly at your Client Area or get a quick access to your Affiliate panel.'); ?></p>
                    </div>

                    <div class="block-col width-md-3-6 order-9 center-sm text-left-md text-left-lg">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/client_area/ca_landing_feat_5.png" alt="Upgrade and Downgrade" >
                    </div>
                    <div class="block-col width-md-3-6 order-10 p-h-30 p-v-60">
                        <h3 class="block-title-small"><?php _e('Upgrade & Downgrade')?></h3>
                        <p><?php _e('Add or remove resources on the go. Never again pay for resources you do not use. Note that downgrades are available for Cloud Servers only.'); ?></p>
                    </div>


                </div> <!-- end of .container-flex -->


            </div>

        </section>

        <section class="service-cta color-bg-style2">

            <div class="container">

                <div class="container-flex cta">
                    <div class="block-col width-md-1-2 cta-left">
                        <h2 class="push-left"><?php _e('Interested?'); ?></h2>
                        <p class="push-left"><?php _e('Join Host1Plus and benefit from powerful hosting solutions and effortless service management.');?></p>
                    </div>
                    <div class="block-col width-md-1-2 cta-buttons cta-right">
                        <a href="<?php echo $config['whmcs_links']['register']; ?>" class="button xlarge ghost "><?php _e('Sign Up')?></a>
                        <a href="#services" data-scrollto="services" class="button xlarge ghost "><?php _e('View Services')?></a>
                    </div>
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-cta -->

        <section class="service-features extra-pad">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Solid Service Control'); ?></h2>
                </div>

                <div class="features-list2">
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/mono/orange_live_stats.svg" alt="Live Stats">
                                    </td>
                                    <td class="title">
                                        <?php _e('Live Stats')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('Track your resource usage live! Enable alerts to avoid CPU, memory or network overuse.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/mono/orange_one_click.svg" alt="One-Click Add-Ons">
                                    </td>
                                    <td class="title">
                                        <?php _e('One-Click Add-Ons')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('Purchase SSL certificates, cPanel or WHMCS licenses, SpamExperts or add new domains to boost your service.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/mono/orange_scalable_resources.svg" alt="Scalable Resources">
                                    </td>
                                    <td class="title">
                                        <?php _e('Scalable Resources')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('Grab more resources to support your growing needs! Customize your server at the Client Area and stay on a budget!')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/mono/orange_dns_management.svg" alt="DNS Management">
                                    </td>
                                    <td class="title">
                                        <?php _e('DNS Management')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('Easily create your domain zones and edit records directly at your Client Area.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/mono/orange_rdns_control.svg" alt="rDNS Configuration">
                                    </td>
                                    <td class="title">
                                        <?php _e('rDNS Configuration')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('Simplified rDNS self-management means less time spent getting support and more time working on your own projects.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/mono/orange_virtual_console.svg" alt="Virtual Console">
                                    </td>
                                    <td class="title">
                                        <?php _e('Virtual Console Access')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('For instant recovery, retrieve access to your server by connecting via virtual console.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->

        <section class="customer-care extra-pad color-bg-style5">
            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Customer Care'); ?></h2>
                </div>

                <div class="layout-row two-col-row">
                    <div class="block-col center p-h-20">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/client_area/ca_landing_supportteam.png" alt="Support tickets">
                        <h3 class="block-title-small p-t-20"><?php _e('Support Tickets'); ?></h3>
                        <p><?php _e('Access your ticket history and contact our support team directly at your Client Area. While you‘re at it, integrated auto-suggestions will provide related answers while you‘re typing the question!'); ?></p>
                    </div>
                    <div class="block-col center p-h-20">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/client_area/ca_landing_feedback.png" alt="Feedback">
                        <h3 class="block-title-small p-t-20"><?php _e('Keep in Touch'); ?></h3>
                        <p><?php _e('Express your opinion and be heard! Provide us your feedback on our services and we‘ll take your ideas into account when performing upgrades and implementing new features.'); ?></p>
                    </div>
                </div>

            </div>

        </section> <!-- end of .customer-care -->

        <?php include_block("htmlblocks/hosting-solutions.php"); ?>

    </section>

</article>

<?php

universal_redirect_footer([
    'en' => $site_en_url.'/client-area/',
    'br' => $site_br_url.'/area-do-cliente/'
]);

get_footer(); 

?>

