<?php

global $config;
global $whmcs_promo;
global $whmcs;

$plansConfig = $whmcs->get_local_stepsconfig('vps_plans');

/* Old prices for presenting price differences */
$old_price = array (
    'en_US' => [
        /*'Amber' => '2.00',
        'Bronze' => '4.75',*/
        'Amber' => '',
        'Bronze' => '',
        'Silver' => '14.70',
        'Gold' => '29.30',
        'Platinum' => '64.50',
        'Diamond' => '105.00'
    ],
    'pt_BR' => [
        /*'Amber' => '6.74',
        'Bronze' => '16.00',*/
        'Amber' => '',
        'Bronze' => '',
        'Silver' => '47.22',
        'Gold' => '94.12',
        'Platinum' => '207.19',
        'Diamond' => '337.29'
    ]
);
?>

<div class="plans-pricing-table vps totalcol-6">

    <?php

    $locations = $config['products']['vps_hosting']['locations'];

    $i = 0;

    $most_popular_marking = "Silver";
    $most_popular_description = __('Most popular');

    foreach($plansConfig as $plan_key => $plan):

        $planConfig = $plan['configs'];
        $best = ($i == 0);


        // Checking if current plan is the Popular
        if ( $plan['name'] == $most_popular_marking ) {
            $is_popular_class = " popular";
            $is_popular_descr = "<span>" . $most_popular_description . "</span>";
        } else {
            $is_popular_class = "";
            $is_popular_descr = "";
        }

        ?>

        <div class="plan-col plan-col-<?php echo $i+1?>">
            <div class="plan<?php echo $is_popular_class ?>">
                <form class="pricing-table-js" action="<?php echo $config['whmcs_links']['checkout_vps']?>" method="get">

                    <input type="hidden" name="plan" value="<?php echo $plan_key;?>"/>
                    <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                    <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                    <input type="hidden" name="location" value="<?php echo $locations[$defaultLocationKey]['key']?>"/>
                    <input type="hidden" name="promocode" value="<?php echo $mypage->promo;?>"/>

                    <div class="plan-header-wrapper" data-submit-form>
                        <div class="cell plan-name"><?php echo $plan['name']?><?php echo $is_popular_descr ?></div>
                        <div class="cell price-wrapper <?php if(!$no_strikethrough_prices) echo 'taller-pricing'; ?>">
                            <div class="price">
                                <?php

                                $prices = $whmcs->getConfigurablePrice($locations[$cheapestLocationKey]['id'], $planConfig);

                                $page_locale = get_locale();

                                if ($old_price [$page_locale][$plan['name']] != '') {
                                    $current_old_price = $old_price [$page_locale][$plan['name']];
                                } else {
                                    $current_old_price = NULL;
                                }

                                ?>
                                <span class="from"><?php _e('from')?></span>

                                <?php if ( isset($current_old_price) AND (!$no_strikethrough_prices)) : ?>

                                    <div class="display-inline breakpoint-w1016-max">
                                        <span class="main-price old-price strikethrough"><span class="prefix old-price"><?php echo $whmcs::$settings['currency_prefix'] ?></span><?php echo $current_old_price; ?></span><span class="mo p-r-10"><?php _e('/mo')?></span>
                                    </div>

                                <?php endif ?>

                                <?php if ($plan['name'] == 'Amber'): ?>

                                    <span class="main-price"><span class="prefix"><?php echo $whmcs::$settings['currency_prefix'] ?></span><?php echo $prices[2]['price_monthly_value']; ?></span><span class="mo"><?php _e('/mo')?></span>

                                <?php else: ?>

                                    <span class="main-price"><span class="prefix"><?php echo $whmcs::$settings['currency_prefix'] ?></span><?php echo $prices[0]['price_value']; ?></span><span class="mo"><?php _e('/mo')?></span>

                                <?php endif ?>

                                <?php if ( isset($current_old_price) AND (!$no_strikethrough_prices)) : ?>

                                    <div class="display-block breakpoint-w1016-min m-up-5">
                                        <span class="main-price old-price strikethrough small-price"><span class="prefix old-price"><?php echo $whmcs::$settings['currency_prefix'] ?></span><?php echo $current_old_price; ?></span><span class="mo p-r-10"><?php _e('/mo')?></span>
                                    </div>

                                <?php endif ?>


                            </div>
                        </div>
                    </div>
                    <div class="plan-body-wrapper">
                        <div class="cell feature">
                            <div class="name"><?php _e('CPU')?></div>
                            <div class="value">
                                <?php
                                switch(get_numeric_case($plan['configs']['cpu']['value'], true)){
                                    case 0:
                                        echo $planConfig['cpu']['value'] . ' ' . _x_lc('Cores', '0 Cores');
                                        break;
                                    case 0.1:
                                        echo $planConfig['cpu']['value'] . ' ' . _x_lc('Core', '0.1 Core');
                                        break;
                                    case 1:
                                        echo $planConfig['cpu']['value'] . ' ' . _x_lc('Core', '1 Core');
                                        break;
                                    case 2:
                                        echo $planConfig['cpu']['value'] . ' ' . _x_lc('Cores', '2 Cores');
                                        break;
                                }
                                ?>
                            </div>
                        </div>
                        <div class="cell feature">
                            <div class="name"><?php _e('RAM')?></div>
                            <div class="value"><?php echo $planConfig['ram']['value']?> <?php _e('MB')?></div>
                        </div>
                        <div class="cell feature">
                            <div class="name"><?php _e('Disk space')?></div>
                            <div class="value"><?php echo $planConfig['hdd']['value']?> <?php _e('GB')?></div>
                        </div>
                        <div class="cell feature">
                            <div class="name"><?php _e('Bandwidth')?></div>
                            <div class="value"><?php echo $planConfig['bandwidth']['value']?> <?php _e('GB')?></div>
                        </div>
                        <div class="cell order">
                            <button type="submit" class="vps-checkout-link button primary"><?php _e('Select')?></button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <?php $i++; endforeach;?>
</div>

<script>
  (function($){

    $(document).ready(function() {
      $('[data-submit-form]').click(function(){
        $(".plan").removeClass('selected');
        $(this).closest(".plan").addClass('selected');
        $(this).closest("form").submit();
      });
    });

  })(jQuery)
</script>
