<div class="spaced land-color-bg-1 m-10">
  <div class="main-face push-flex horizontal-center wrap p-20" style="opacity: 1; top: 0px;">
    <div class="feature-image-wrapper">
      <img class="feature-image"
                                            src="https://www.host1plus.com/wp-content/themes/h1p_v5/landings2/images/round-feat-white-win.svg"
                                            alt="Windows Cloud Server icon" width="140"></div>
    <h3 class="feature-title block-title-small white max-w-150">Windows Server</h3></div>
  <div class="alternate-face p-20" style="opacity: 0; top: 300px;"><p class="white">Build modern business applications
      and bring the innovation behind the world's largest datacenters to your private workspace.</p></div>
</div>