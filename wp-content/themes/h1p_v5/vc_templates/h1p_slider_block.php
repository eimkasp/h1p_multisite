<section class="our-customers extra-pad">
  <div class="container">
    <div class="section-header">
      <h2 class="block-title"><?php _e('Meet our clients!') ?></h2>
    </div>
      <?php include("sliders/reviews.php"); ?>
  </div> <!-- end of .container -->
</section> <!-- end of .our-customers -->