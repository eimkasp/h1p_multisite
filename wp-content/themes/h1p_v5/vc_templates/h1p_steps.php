<?php
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

?>
<div class="three-col-row feature-block-below-pricing">
	<div class="block-col">
		<img class="push-left m-r-10" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/cloud_servers/plans_lin_win.svg" alt="Linux and Windows" width="70">
		<img class="feature-block-arrow" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/cloud_servers/plan_arrow_big_thin.svg" alt="" width="12">
		<p class="descr-text-feature size-18 p-v-10"><?php _e('Pick Linux or Windows OS'); ?></p>
	</div>
	<div class="block-col">
		<img class="push-left m-r-10" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/cloud_servers/plans_longer_billing_cycles.svg" alt="Save on billing cycles" width="70">
		<img class="feature-block-arrow" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/cloud_servers/plan_arrow_big_thin.svg" alt="" width="12">
		<p class="descr-text-feature size-18 p-v-10"><?php echo sprintf(_x_lc('Save %1$s up to 20%% %2$s for longer billing cycles!'), '<span class="highlight has-tooltip" data-tooltip="billing-cycle-savings">', '</span>'); ?></p>
		<div class="tooltip-body style2 w-600" data-tooltip-content="billing-cycle-savings"><p><?php _e('Save up to 20% with longer billing cycles ranging from 3 to 24 months. The discount increases according to the length of the billing cycle.'); ?></p></div>
		<div class="triangle"></div>
	</div>
	<div class="block-col">
		<img class="push-left m-r-10" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/cloud_servers/plans_customize.svg" alt="Flexible customization" width="70">
		<p class="descr-text-feature size-18 p-v-10"><?php _e('Customize your plan at your Client Area'); ?></p>
	</div>
</div>