<?php

extract( shortcode_atts( array(
	'feature_object' => '',
), $atts ) );

$feature_object = ! empty( $feature_object ) ? $feature_object : 1;

$args = array(
	'post_type' => 'features',
	'page_id' => $feature_object
);

$the_query              = new WP_Query( $args );
$singleFeatureToDisplay = get_post( $feature_object );
while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

  <div class="h1p-feature-block block-col w-300">
    <img src="<?php the_field( 'feature_icon' ); ?>"
         alt="<?php the_title(); ?>"
         width="130" height="130">
    <h3><?php the_title(); ?></h3>
    <p><?php the_field( 'feature_description' ); ?></p>
  </div>

<?php endwhile; ?>