<header id="particles-js" class="headline cloud-introduction">
  <div class="container adjust-vertical-center">

    <div class="hot-news-wrapper">
      <div class="hot-news-overflow-container">
        <div class="hot-news-group">
          <div class="hot-news-handle news"><?php _e('News'); ?> <i class="fa fa-angle-right"></i></div>
          <div class="hot-news-message"><a
                    href="http://www.host1plus.com/blog/news/host1plus-launches-cloud-servers-in-sao-paulo-brazil"
                    target="_blank"
                    title="<?php _e('Host1Plus Launches Cloud Servers ßeta in São Paulo, Brazil!'); ?>"><?php _e('Host1Plus Launches Cloud Servers ßeta in São Paulo, Brazil!'); ?></a>
          </div>
        </div>
      </div> <!-- end of .hot-news-overflow-container -->
    </div>

    <div class="wrapper">

      <h1 class="page-title">
                        <span id="flipper" class='flip'>
<?php

/*Only rules for 2 languages !!!*/

if (get_locale() == 'en_US') {

    ?>
  <span class="step step0 set">Fast Storage</span>
  <span class="step step1">IPv4 &amp; IPv6</span>
  <span class="step step2">DNS Control</span>
  <span class="step step4">Live Stats</span>
  <span class="step step5">App Templates</span>
  <span class="step step6">Backups</span>
    <?php

} elseif (get_locale() == 'pt_BR') {

    ?>
  <span class="step step0 set">Armazenamento</span>
  <span class="step step1">IPv4 &amp; IPv6</span>
  <span class="step step2">Controle de DNS</span>
  <span class="step step4">Estatísticas</span>
  <span class="step step5">Modelos de apps</span>
  <span class="step step6">Backups</span>
    <?php

}

?>

                        </span>
      </h1>

      <div class="title-follow-text"><?php _e('Cloud Servers') ?> <?php _e('from') ?> <span
                class="highlight">
              <?php //echo $mypage->getServiceMinPrice('cs_lin'); ?>
        </span>
      </div>

      <div class="home-slider-feat-wrap center m-v-20">
        <div class="feat-wrap-block">
          <i class="fa fa-check"></i> <?php _e('Powerful API') ?>
        </div>
        <div class="feat-wrap-block">
          <i class="fa fa-check"></i> <?php _e('KVM virtualization') ?>
        </div>
        <div class="feat-wrap-block">
          <i class="fa fa-check"></i> <?php _e('Windows & Linux') ?>
        </div>
        <div class="feat-wrap-block">
          <i class="fa fa-check"></i> <?php _e('Custom ISO') ?>
        </div>
      </div>


      <div class="center p-t-40 p-b-30">
        <a href="<?php echo $config['links']['cloud-servers']; ?>"
           class="button highlight large p-h-40"><?php _e('View plans'); ?></a>
      </div>
    </div>
  </div>
</header> <!-- end of .headline -->