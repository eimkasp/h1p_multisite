<?php
/**
 * @package WordPress
 * @subpackage h1p_v5
 *
 * Template Name: Affiliate signup
 */

define( 'SKIP_HEADER_NAV', '1' );
define( 'SKIP_FOOTER_NAV', '1' );

get_header();

?>

<div class="white-content-block-wrapper">
  <div class="white-content-block">

    <div class="white-content-logo">
      <i class="host1plus-logo"></i>
    </div>
    <h2 class="regular p-v-10 f-family-default" data-big-title>
      <?php _e('Become an affiliate - sign up!')?>
    </h2>

    <div class="p-v-20" style="display:none;" data-reg-success>
      <?php _e('Host1Plus is glad to have you!') ?>
      <br><br>
      <?php _e('Please wait until we look through your form and confirm you.') ?>
      <br>
      <?php _e('This might take up to 24 hours.') ?>
    </div>

    <div class="text-error f-size-default" style="display:none;" data-general-error>

    </div>

    <form id="aff_form">
      <h3 class="block-title-large-thin p-v-20 f-family-default">
        <?php _e('Account information')?>
      </h3>

      <div class="">
        <label for="acctypeperson">
            <input id="acctypeperson" class="h1p-radio" type="radio" name="acctype" value="person" checked>
            <label for="acctypeperson">
                <div class="radio">
                    <div class="fill"></div>
                </div>
            </label>
            <label for="acctypeperson"><?php _e('Personal')?></label>
        </label>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <label for="acctypebusiness">
            <input id="acctypebusiness" class="h1p-radio" type="radio" name="acctype" value="business">
            <label for="acctypebusiness">
                <div class="radio">
                    <div class="fill"></div>
                </div>
            </label>
            <label for="acctypebusiness"><?php _e('Bussiness')?></label>
        </label>
      </div>

      <div class="layout-row two-col-row">
      	<div class="block-col p-l-0 p-r-5">
            <div class="fields-group clearfix p-t-10">
                <div id="regerror" class="validation error" style="display:none"></div>
                <div class="form-field">
                    <div class="field input-label-inside">
                        <input id="accountref" type="text" name="accountref" value="" placeholder="<?php _e('Affiliate ID')?> *" data-required />
                        <span class="inside-label" style="display:none;"><?php _e('Affiliate ID')?> *</span>
                        <i class="fa fa-info-circle tooltip-icon" data-tooltip="ref-id" style="position: absolute;top: 16px;right: 15px;"></i>
                        <div class="tooltip-body style2" data-tooltip-content="ref-id">
                            <div class="triangle" style="width: 13px; left: 0px;"></div>
                            <p><?php _e('This name will appear within your referral link') ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fields-group clearfix p-t-10">
                <div id="regerror" class="validation error" style="display:none"></div>
                <div class="form-field">
                    <div class="field input-label-inside">
                        <input id="firstname" type="text" name="firstname" value="" placeholder="<?php _e('First Name')?> *" data-required />
                        <span class="inside-label" style="display:none;"><?php _e('First Name')?> *</span>
                    </div>
                </div>
            </div>
            <div class="fields-group clearfix p-t-10">
                <div id="regerror" class="validation error" style="display:none"></div>
                <div class="form-field">
                    <div class="field input-label-inside">
                        <input id="lastname" type="text" name="lastname" value="" placeholder="<?php _e('Last Name')?> *" data-required />
                        <span class="inside-label" style="display:none;"><?php _e('Last Name')?> *</span>
                    </div>
                </div>
            </div>
            <div class="fields-group clearfix p-t-10">
                <div class="form-field ">
                    <div class="field">
                        <select name="country" class="empty" data-country-select data-required>
                          <option value=""><?php _e('Country'); ?> *</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="block-col p-l-5 p-r-0">
            <div class="fields-group clearfix p-t-10">
                <div id="regerror" class="validation error" style="display:none"></div>
                <div class="form-field">
                    <div class="field input-label-inside">
                        <input class="" id="email" type="text" name="email" value="" placeholder="<?php _e('Email Address (Account Username)')?> *" data-required data-valide-email/>
                        <span class="inside-label" style="display:none;"><?php _e('Email Address (Account Username)')?> *</span>
                    </div>
                </div>
            </div>
            <div class="fields-group clearfix p-t-10">
                <div id="regerror" class="validation error" style="display:none"></div>
                <div class="form-field">
                    <div class="field input-label-inside">
                        <input id="phone" type="text" name="phone" value="" placeholder="<?php _e('Telephone')?> *" data-required/>
                        <span class="inside-label" style="display:none;"><?php _e('Telephone')?> *</span>
                    </div>
                </div>
            </div>
            <div class="fields-group clearfix p-t-10">
                <div id="regerror" class="validation error" style="display:none"></div>
                <div class="form-field">
                    <div class="field input-label-inside">
                        <input id="skype" type="text" name="skype" value="" placeholder="<?php _e('Skype ID')?>" />
                        <span class="inside-label" style="display:none;"><?php _e('Skype ID')?></span>
                    </div>
                </div>
            </div>
            <div class="fields-group clearfix p-t-10" style="display:none;" data-business-account>
                <div id="regerror" class="validation error" style="display:none"></div>
                <div class="form-field">
                    <div class="field input-label-inside">
                        <input id="company" type="text" name="company" value="" placeholder="<?php _e('Company Name')?> *" />
                        <span class="inside-label" style="display:none;"><?php _e('Company Name')?> *</span>
                    </div>
                </div>
            </div>
        </div>
      </div>

      <h3 class="block-title-large-thin p-v-20 p-b-0 f-family-default">
        <?php _e('Advertising information') ?>
      </h3>
      <h4 class="t-16-shrinked p-t-10 regular f-family-default" data-adv-title>
        <?php _e('How are you planing to advertise Host1Plus?') ?> *
      </h4>

      <div class="layout-row two-col-row f-family-default">
      	<div class="block-col p-l-0 p-r-5">
          <label class="input-checkbox green">
              <input type="checkbox" name="advertise[]" value="Reviews" data-adv-checkbox/>
               <span class="checkbox">
                   <i class="fa fa-check"></i>
                   &nbsp;&nbsp;<span><?php _e('Reviews')?></span>
               </span>
           </label>
           <label class="input-checkbox green">
               <input type="checkbox" name="advertise[]" value="Blog" data-adv-checkbox/>
               <span class="checkbox">
                   <i class="fa fa-check"></i>
                   &nbsp;&nbsp;<span><?php _e('Blogging')?></span>
               </span>
           </label>
           <label class="input-checkbox green">
               <input type="checkbox" name="advertise[]" value="Social Media" data-adv-checkbox/>
               <span class="checkbox">
                   <i class="fa fa-check"></i>
                   &nbsp;&nbsp;<span><?php _e('Social media')?></span>
               </span>
           </label>
           <label class="input-checkbox green">
               <input type="checkbox" name="advertise[]" value="My own website"/ onchange="if($(this).prop('checked')){ $('[data-website-input-wrap]').show() }else{ $('[data-website-input-wrap]').hide()}" data-adv-checkbox>
               <span class="checkbox">
                   <i class="fa fa-check"></i>
                   &nbsp;&nbsp;<span><?php _e('My own website')?></span>
               </span>
           </label>
           <div class="fields-group clearfix p-t-5" data-website-input-wrap style="display:none;">
               <div id="regerror" class="validation error" style="display:none"></div>
               <div class="form-field">
                   <div class="field">
                       <input id="website" type="text" name="website" value="" placeholder="<?php _e('Enter your website URL')?>" data-required-if-visible/>
                   </div>
               </div>
           </div>
           <label class="input-checkbox green">
               <input type="checkbox" name="advertise[]" value="Coupons" data-adv-checkbox/>
               <span class="checkbox">
                   <i class="fa fa-check"></i>
                   &nbsp;&nbsp;<span><?php _e('Coupons')?></span>
               </span>
           </label>
         </div>
       	<div class="block-col p-l-0 p-r-5">
           <label class="input-checkbox green">
               <input type="checkbox" name="advertise[]" value="Provider comparison" data-adv-checkbox/>
                <span class="checkbox">
                    <i class="fa fa-check"></i>
                    &nbsp;&nbsp;<span><?php _e('Provider comparison')?></span>
                </span>
            </label>
            <label class="input-checkbox green">
                <input type="checkbox" name="advertise[]" value="Banners" data-adv-checkbox/>
                <span class="checkbox">
                    <i class="fa fa-check"></i>
                    &nbsp;&nbsp;<span><?php _e('Banners')?></span>
                </span>
            </label>
            <label class="input-checkbox green">
                <input type="checkbox" name="advertise[]" value="Search engines" data-adv-checkbox/>
                <span class="checkbox">
                    <i class="fa fa-check"></i>
                    &nbsp;&nbsp;<span><?php _e('Search engines')?></span>
                </span>
            </label>
            <label class="input-checkbox green">
                <input type="checkbox" name="advertise[]" value="Other" onchange="if($(this).prop('checked')){ $('[data-other-input-wrap]').show() }else{ $('[data-other-input-wrap]').hide()}" data-adv-checkbox/>
                <span class="checkbox">
                    <i class="fa fa-check"></i>
                    &nbsp;&nbsp;<span><?php _e('Other')?></span>
                </span>
            </label>
            <div class="fields-group clearfix p-t-5" data-other-input-wrap style="display:none;">
                <div id="regerror" class="validation error" style="display:none"></div>
                <div class="form-field">
                    <div class="field">
                        <input id="other_adv" type="text" name="other_adv" value="" placeholder="<?php _e('Please specify...')?>" data-required-if-visible/>
                    </div>
                </div>
            </div>
        </div>
      </div>

      <h3 class="block-title-large-thin p-v-20 p-b-0 f-family-default">
        <?php _e('Payout information') ?>
      </h3>
      <h4 class="t-16-shrinked p-t-10 regular f-family-default" data-adv-title>
        <?php _e('To receive Affiliate commisions, please add your PayPal email address.') ?>
      </h4>

      <div class="layout-row two-col-row f-family-default">
      	<div class="block-col p-l-0 p-r-5 center-block">
          <div class="fields-group clearfix p-t-10">
            <div id="regerror" class="validation error" style="display:none"></div>
            <div class="form-field" data-payout="paypal">
                <div class="field input-label-inside">
                    <input id="paypal_email" type="text" name="paypal_email" value="" placeholder="<?php _e('Paypal email address')?> *" data-required data-valide-email/>
                    <span class="inside-label" style="display:none;"><?php _e('Paypal email address')?> *</span>
                </div>
            </div>
          </div>
        </div>
      </div>

      <div class="">
        <button id="aff_form_submit" class="button highlight" type="submit"><?php _e('Sign Up')?></button>
      </div>
      <div class="p-t-20">
        <small><?php echo sprintf(__('By clicking Sign Up, you agree to the %sTerms of Service%s'),'<a href="https://www.host1plus.com/terms-of-service/?_ga=1.124360970.1673565049.1474977106#tos-section-13-1">', '</a>'); ?></small>
      </div>
    </form>

  </div>

  <div class="wrapper-footer">
    <table>
      <tr>
        <td class="adjust-left"><small><?php _e('Back to:')?> <a href="<?php echo $config['links']['affiliate']?>"><?php _e('Home')?></a></small></td>
        <td class="adjust-right">
          <small><?php _e('Already a Host1Plus Affiliate Program client?')?> <a class="simple" href="<?php echo $config['links']['affiliate_login']?>"><?php _e('Log In')?></a></small>
        </td>
      </tr>
    </table>
  </div>
</div>

<script>

    var countries = {};
    var request_countries = $.ajax({
        url: "<?php echo $config['whmcs_links']['countries_json']; ?>",
        method: "GET",
        async: true,
        dataType: "json",
        crossDomain: false // force to send HTTP_X_REQUESTED_WITH header
    });
    request_countries.done(function( data ) {
        countries = data;

        for (var iso in countries) {
            if (countries.hasOwnProperty(iso)) {
            $('[data-country-select]').append( '<option value="'+iso+'">'+countries[iso]+'</option>' );
            }
        }

    });

    $('[data-country-select]').change(function(){
      if( $(this).val().length < 1 ){
        $(this).addClass('empty');
      }
      else{
        $(this).removeClass('empty');
      }
    });

    $('.input-label-inside input').keyup(function(){
      if( $(this).val().length > 0 ){
        $(this).parent().find('.inside-label').show();
        $(this).addClass('filled');
      }
      else{
        $(this).parent().find('.inside-label').css('display', 'none');
        $(this).removeClass('filled');
      }
    });

    $(window).resize(fullheight);

    function fullheight() {
        var window_height = $(window).height();
        var main_wrapper_height = $('.white-content-block-wrapper').height();
        var h = window_height - main_wrapper_height;

        if (h > 0)
            $('html, body').css('height', '100%');
        else
            $('html, body').css('height', 'auto');
    }

    $(document).ready(function () {
        fullheight();
    }
    );

    $('[name="acctype"]').change(function(){
      if( $(this).val() == 'business' ){
        $('[data-business-account]').show();
      }
      else{
        $('[data-business-account]').hide();
      }
    });

    $('[name="payout_type"]').change(function(){
        $('[data-payout]').hide();
        $('[data-payout="'+$(this).val()+'"]').show();
    });

    $('#aff_form').submit(function(e){
      e.preventDefault();
      var $submitButton = $(e.target).find('[type="submit"]');
     
      var $general_error_block = $('[data-general-error]');

      $general_error_block.text('').hide();

      var can_submit = true;

      //validate required inputs
      $('[data-required]').each(function(){
        if( $(this).val().length < 1 ){
          $(this).addClass('error');
          can_submit = false;
        }
        else{
          $(this).removeClass('error');
        }
      });
      $('[data-required-if-visible]:visible').each(function(){
        if( $(this).val().length < 1 ){
          $(this).addClass('error');
          can_submit = false;
        }
        else{
          $(this).removeClass('error');
        }
      });

      //validate company name
      var acc_type = $('[name="acctype"]:checked').val();
      if( acc_type == 'business' ){
        var $company_input = $('[name="company"]');
        if( $company_input.val().length < 1 ){
          $company_input.addClass('error');
          can_submit = false;
        }
        else{
          $company_input.removeClass('error');
        }
      }

      //validate advertising information
      var $adv_checked = $('[name="advertise[]"]:checked');
      var $adv_title = $('[data-adv-title]');
      if( $adv_checked.length < 1 ){
        $adv_title.addClass('text-error');
        can_submit = false;
      }
      else{
        $adv_title.removeClass('text-error');
      }

      //validate emails
      $('[data-valide-email]').each(function(){
        if( ! IsEmail( $(this).val() ) ){
          $(this).addClass('error');
          $(this).closest('.fields-group').find('#regerror').text('<?php _e('Invalid email address.') ?>').show();
          can_submit = false;
        }
        else{
          $(this).closest('.fields-group').find('#regerror').text('').hide();
          $(this).removeClass('error');
        }
      });

      if( can_submit ){

          $submitButton.prop('disabled', true);
          $submitButton.prepend('<i class="fa fa-spinner fa-pulse"></i> ');

          //do ajax
          var request = $.ajax({
              url: "/wp-content/themes/<?= wp_get_theme()->template ?>/PAP/pap_registration.php",
              method: "POST",
              data: $(this).serialize(),
              async: true,
              dataType: "json",
              crossDomain: false // force to send HTTP_X_REQUESTED_WITH header
          });

          request.done(function( respnse ) {

            if( respnse.success ){
              $('#aff_form').hide();
              $('[data-big-title]').hide();
              $('[data-reg-success]').show();
            }
            else{
              $general_error_block.html( respnse.message ).show();
              $('html,body').animate({scrollTop: $general_error_block.offset().top},'slow');

              $submitButton.find('i').remove();
              $submitButton.prop('disabled', false);
            }

          });

          request.fail(function( error ) {

          });

      }

    });


function IsEmail(email) {
    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
};
</script>

<?php

get_footer();

?>
