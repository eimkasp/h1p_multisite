<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Reseller program
*/

wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);
wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);
wp_enqueue_script('jquery.validate', get_template_directory_uri() . '/js/vendor/jquery.validate.js', ['jquery'], '1.13.1', true);
wp_enqueue_script('tablesaw', get_template_directory_uri() . '/js/vendor/tablesaw.js', ['jquery'], '3.0.0-beta.4', true);
wp_enqueue_script('tablesaw-init', get_template_directory_uri() . '/js/vendor/tablesaw-init.js', ['tablesaw'], '3.0.0-beta.4', true);

global $whmcs;
global $config;

wp_enqueue_script('phonecodes', $config['whmcs_links']['phonecodes_js'], ['jquery'], '1', true);
wp_enqueue_script('states', $config['whmcs_links']['states_js'], ['jquery'], '1', true);

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";


// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing reseller-program">

    <header class="headline reseller-program color-bg-style-grad1">
        <div class="container adjust-vertical-center">

            <h1 class="page-title"><?php _e('Reseller Program')?></h1>

            <div class="descr-text-feature p-v-10 size-18 white"><?php _e('Searching for fresh ways to deliver complete hosting solutions to your customers? We have the answer - full range of simple, scalable, and easy-to-manage hosting products powered by our in-house developed API to increase the variety of products you can provide for your customers.')?></div>

            <a href="#registration" class="button ghost large m-t-30" data-scrollto="registration"><?php _e('Apply Now');?></a>

            <p class="white p-v-10"><?php _e('Existing users'); ?> &mdash; <a class="white" data-popup="<?php echo $config['links']['reseller-sign-in']; ?>" data-popup-id="login" data-popup-type="ajax" href="#" data-callback="$('#login-username').focus()" data-prevent>Apply here</a></p>

            <div class="resellers-pic-main-spacer"></div>

        </div>
    </header> <!-- end of .headline.reseller-program -->



    <section class="main page-content">

        <section class="how-it-works">

            <div class="container">

                <div class="scroll-id-target" id="how-it-works"></div>

                <div class="layout-row">
                    <div class="block-col resellers-pic-main center">
                        <img src="<?php echo $images ?>img/resellers/resellers-set.png" alt="Powered by developer tools">
                    </div>
                </div>

                <div class="section-header">
                    <h1 class="page-title"><?php _e('How does reseller program work?')?></h1><div class="p-v-10 descr-text-feature size-18">
                        <?php _e('We provide you with Cloud Servers and API to integrate it to your own already existing client management and billing systems.')?>
                    </div>
                </div>

                <div class="m-v-30 breakpoint-w600-min">

                        <?php include_block("htmlblocks/svg-schemes/resellers_integrated_clients_scheme_lgscreen.php"); ?>

                </div>

                <div class="m-v-30 m-h-20 breakpoint-w600-max">

                        <?php include_block("htmlblocks/svg-schemes/resellers_integrated_clients_scheme_smscreen.php"); ?>

                </div>


            </div> <!-- end of .container -->

        </section> <!-- end of .how-it-works -->



        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>



        <section class="resellers-benefits extra-pad color-bg-style-grad1" data-section-title="<?php _e('Benefits');?>">

            <div id="particles-js" style="position: absolute; width: 100%; height: 100%; top: 0;"></div>
            <div id="features" class="scroll-id-target"></div>

            <div class="container p-0" style="z-index: 1;">

                <div class="section-header">
                    <h2 class="block-title white"><?php _e('Program Benefits')?></h2>
                </div>

                <div class="p-t-20"></div>

                <div class="four-col-row-biface-cards">

                    <div class="block-col larger color-bg-white op-35">
                        <div class="main-face push-flex horizontal-center nowrap">
                            <div class="feature-image-wrapper">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/resellers/res_vol_based_discounts.svg" alt="Volume-Based Discounts">
                            </div>
                            <h3 class="feature-title white adjust-left"><?php _e('Volume-Based Discounts')?></h3>
                        </div>
                        <div class="alternate-face description push-flex vertical-center">
                            <h3 class="p-b-20"><?php _e('Volume-Based Discounts')?></h3>
                            <p><?php _e('Build, customize and sell Host1Plus solutions to your customers and receive generous discounts for all active services to minimize your costs and gain competitive advantage.'); ?></p>
                        </div>
                    </div>

                    <div class="block-col color-bg-white op-50">
                        <div class="main-face push-flex horizontal-center wrap">
                            <div class="feature-image-wrapper">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/resellers/res_full_support.svg" alt="Priority Support">
                            </div>
                            <h3 class="feature-title white"><?php _e('Priority Support')?></h3>
                        </div>
                        <div class="alternate-face description push-flex vertical-center">
                            <h3 class="p-b-20"><?php _e('Priority Support')?></h3>
                            <p><?php _e('Your ticket or support call is put to the front of the line, so you get help when you need it most. Save your time for business development instead of waiting in the queue to deal with minor issues.'); ?></p>
                        </div>
                    </div>

                    <div class="block-col color-bg-white op-15">
                        <div class="main-face push-flex horizontal-center wrap">
                            <div class="feature-image-wrapper">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/resellers/res_personal_manager.svg" alt="Personal Manager">
                            </div>
                            <h3 class="feature-title white"><?php _e('Personal Manager')?></h3>
                        </div>
                        <div class="alternate-face description push-flex vertical-center">
                            <h3 class="p-b-20"><?php _e('Personal Manager')?></h3>
                            <p><?php _e('Reach out to your account manager with any inquiries you have - we make sure that all your questions are answered and all requests are executed.'); ?></p>
                        </div>
                    </div>

                </div>
                <div class="four-col-row-biface-cards">

                    <div class="block-col color-bg-white center op-25">
                        <div class="main-face push-flex horizontal-center wrap">
                            <div class="feature-image-wrapper">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/resellers/res_white_label.svg" alt="White Label">
                            </div>
                            <h3 class="feature-title white"><?php _e('White Label')?></h3>
                        </div>
                        <div class="alternate-face description push-flex vertical-center">
                            <h3 class="p-b-20"><?php _e('White Label')?></h3>
                            <p><?php _e('Create, store and deploy unlimited virtual environments under your own brand and offer tailored solutions for your customers.'); ?></p>
                        </div>
                    </div>

                    <div class="block-col color-bg-white op-50">
                        <div class="main-face push-flex horizontal-center wrap">
                            <div class="feature-image-wrapper">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/resellers/res_custom_os_templ.svg" alt="Custom OS Templates">
                            </div>
                            <h3 class="feature-title white"><?php _e('Custom OS Templates')?></h3>
                        </div>
                        <div class="alternate-face description push-flex vertical-center">
                            <h3 class="p-b-20"><?php _e('Custom OS templates')?></h3>
                            <p><?php _e('Create your own OS templates for deployment. Templates may include customization, software packages, security templates or other components.'); ?></p>
                        </div>
                    </div>

                    <div class="block-col larger color-bg-white op-35">
                        <div class="main-face push-flex horizontal-center nowrap">
                            <div class="feature-image-wrapper">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/resellers/res_custom_api_call_dev.svg" alt="Custom API Call Development">
                            </div>
                            <h3 class="feature-title white adjust-left"><?php _e('Custom API Calls')?></h3>
                        </div>
                        <div class="alternate-face description push-flex vertical-center">
                            <h3 class="p-b-20"><?php _e('Custom API Calls')?></h3>
                            <p><?php _e('Each business has unique environment in terms of market, vendors, and operation methods. This diversity often calls for customization of the API & we promise to develop custom API solutions for your business.'); ?></p>
                        </div>
                    </div>

                </div>

            </div>

        </section> <!-- end of .resellers-benefits -->


<script>

    (function($) {

        /*section.resellers-benefits*/
        /*Interactive blocks handler*/

        $(document).ready(function(){
            function isElemActive($elem) {
                return ($elem.hasClass('active'));
            }
            function toggleBlock($elem) {
                var active = isElemActive($elem);
                if (!active) {
                    $elem.addClass('active');
                    $elem.find('.main-face').animate({
                        opacity: 0,
                        top: '-300px',
                    }, 200, function(){
                    });
                    $elem.find('.alternate-face').animate({
                        opacity: 1,
                        top: '0',
                    }, 300, function(){
                    });

                } else {
                    $elem.removeClass('active');
                    $elem.find('.main-face').animate({
                        opacity: 1,
                        top: '0',
                    }, 200, function(){
                    });
                    $elem.find('.alternate-face').animate({
                        opacity: 0,
                        top: '300px',
                        display: 'toggle'
                    }, 300, function(){
                    });
                }
            }

            $('.resellers-benefits .block-col').click(function(){
                toggleBlock($(this));
            });
        });

    })(jQuery);

</script>


        <section class="reseller-levels extra-pad" data-section-title="<?php _e('Levels');?>">

            <div id="levels" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Reseller Levels')?></h2>
                </div>

                <table class="benefits-t tablesaw tablesaw-swipe" data-tablesaw-mode="swipe">
                    <thead>
                        <tr>
                            <th class="benefits-t-header title" scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">
                                <?php _e('Benefits');?>
                            </th>
                            <th class="benefits-t-header" scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="1">
                                <?php _e('Personal');?>&nbsp<i class="fa fa-info-circle tooltip-icon" data-tooltip="personal-level-info"></i>
                                <div class="tooltip-body style2 w400" data-tooltip-content="personal-level-info">
                                    <div class="triangle"></div>
                                    <p><?php _e('All new reseller program participants are assigned to Personal tier until they generate more than $4,999 total sales value.'); ?></p>
                                </div>
                                <span class="short-descr"><?php _e('Up to $4,999');?></span>
                            </th>
                            <th class="benefits-t-header" scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">
                                <?php _e('Business');?>&nbsp<i class="fa fa-info-circle tooltip-icon" data-tooltip="business-level-info"></i>
                                <div class="tooltip-body style2 w400" data-tooltip-content="business-level-info">
                                    <div class="triangle"></div>
                                    <p><?php _e('Resellers who generate between $5,000 and $14,999 total sales value will be assigned to Business tier.'); ?></p>
                                </div>
                                <span class="short-descr"><?php _e('$5,000 - $14,999');?></span>
                            </th>
                            <th class="benefits-t-header" scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">
                                <?php _e('Enterprise');?>&nbsp<i class="fa fa-info-circle tooltip-icon" data-tooltip="enterprise-level-info"></i>
                                <div class="tooltip-body style2 w400" data-tooltip-content="enterprise-level-info">
                                    <div class="triangle"></div>
                                    <p><?php _e('Resellers who generate $15,000 and more total sales value are assigned to Enterprise tier and can take advantage of all the benefits.'); ?></p>
                                </div>
                                <span class="short-descr"><?php _e('$15,000 +');?></span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th class="title descr-text-feature size-18"><?php _e('Volume-based discounts');?></td>
                            <td class="t-benefits-em">5% OFF</td>
                            <td class="t-benefits-em">10% OFF</td>
                            <td class="t-benefits-em">15% OFF</td>
                        </tr>
                        <tr>
                            <th class="title descr-text-feature size-18"><?php _e('White-label');?></td>
                            <td class="availability yes"></td>
                            <td class="availability yes"></td>
                            <td class="availability yes"></td>
                        </tr>
                        <tr>
                            <th class="title descr-text-feature size-18"><?php _e('Personal manager');?></td>
                            <td class="availability yes"></td>
                            <td class="availability yes"></td>
                            <td class="availability yes"></td>
                        </tr>
                        <tr>
                            <th class="title descr-text-feature size-18">
                                <?php _e('Free webinars & events');?>&nbsp<i class="fa fa-info-circle tooltip-icon" data-tooltip="limited-benefits"></i>
                                <div class="tooltip-body style2 w400" data-tooltip-content="limited-benefits">
                                    <div class="triangle"></div>
                                    <p><?php _e('These benefits are limited and require additional Host1Plus approvals & arrangements.'); ?></p>
                                </div>
                            </td>
                            <td class="availability yes"></td>
                            <td class="availability yes"></td>
                            <td class="availability yes"></td>
                        </tr>
                        <tr>
                            <th class="title descr-text-feature size-18"><?php _e('Custom OS templates');?></td>
                            <td class="availability yes"></td>
                            <td class="availability yes"></td>
                            <td class="availability yes"></td>
                        </tr>
                        <tr>
                            <th class="title descr-text-feature size-18"><?php _e('Powerful API');?></td>
                            <td class="availability yes"></td>
                            <td class="availability yes"></td>
                            <td class="availability yes"></td>
                        </tr>
                        <tr>
                            <th class="title descr-text-feature size-18"><?php _e('Priority support');?></td>
                            <td class="availability no"></td>
                            <td class="availability yes"></td>
                            <td class="availability yes"></td>
                        </tr>
                        <tr>
                            <th class="title descr-text-feature size-18"><?php _e('Custom API calls development');?></td>
                            <td class="availability no"></td>
                            <td class="availability no"></td>
                            <td class="availability yes"></td>
                        </tr>
                        <tr>
                            <th class="title descr-text-feature size-18">
                                <?php _e('Joint marketing campaigns');?>&nbsp<i class="fa fa-info-circle tooltip-icon" data-tooltip="limited-benefits"></i>
                                <div class="tooltip-body style2 w400" data-tooltip-content="limited-benefits">
                                    <div class="triangle"></div>
                                    <p><?php _e('These benefits are limited and require additional Host1Plus approvals & arrangements.'); ?></p>
                                </div>
                            </td>
                            <td class="availability no"></td>
                            <td class="availability no"></td>
                            <td class="availability yes"></td>
                        </tr>
                    </tbody>

                </table>

                <div class="center m-t-40 m-b-30">
                    <a href="#registration" class="button primary large" data-scrollto="registration"><?php _e('Become a Reseller');?></a>
                </div>


<script>

    (function($) {

        /*section.reseller-levels*/
        /*Table column highlight handler*/

        $(document).ready(function(){

            function column_highligher($elem) {
                var $currentTable = $elem.closest('table');
                var indexTd = $elem.index();
                var indexTr = $elem.parent().index();

                if (indexTd > 0) {

                    $currentTable.find('tr').each(function() {
                        var $clickRow = $(this);
                        $clickRow.children('td').eq(indexTd-1).addClass('selected');
                    });

                    $currentTable.find('thead tr th').eq(indexTd).addClass('selected');

                }
            }
            function column_nohighlight($elem) {
                var $currentTable = $elem.closest('table');
                $currentTable.find('td').removeClass('selected');
                $currentTable.find('th').removeClass('selected');
            }

            $('.benefits-t tbody td').on('mouseenter', function() {
                var $mouseTarget = $(this);
                column_nohighlight($mouseTarget);
                column_highligher($mouseTarget);
            });
            $('.benefits-t thead th').on('click', function() {
                var $clickTarget = $(this);
                column_nohighlight($clickTarget);
                column_highligher($clickTarget);
            });

        });

    })(jQuery);

</script>


                <h3 class="block-title-small m-t-60 center"><?php _e('All levels include')?></h3>

                <div class="container-flex features-list center">

                    <div class="block-col w-300 m-v-30 p-h-40">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/resellers/res_green_unlimited_scalability.svg" alt="Unlimited scalability" width="56" height="56">
                        <h4 class="descr-text-feature size-18 m-t-20"><?php _e('Unlimited scalability')?></h4>
                    </div>
                    <div class="block-col w-300 m-v-30 p-h-40">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/resellers/res_green_easy_management.svg" alt="No hardware maintenance" width="56" height="56">
                        <h4 class="descr-text-feature size-18 m-t-20"><?php _e('No hardware maintenance')?></h4>
                    </div>
                    <div class="block-col w-300 m-v-30 p-h-40">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/resellers/res_green_no_contracts.svg" alt="No contracts" width="56" height="56">
                        <h4 class="descr-text-feature size-18 m-t-20"><?php _e('No contracts')?></h4>
                    </div>
                    <div class="block-col w-300 m-v-30 p-h-40">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/resellers/res_green_ipv6.svg"  alt="IPv6 support" width="56" height="56">
                        <h4 class="descr-text-feature size-18 m-t-20"><?php _e('IPv6 support')?></h4>
                    </div>
                    <div class="block-col w-300 m-v-30 p-h-40">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/resellers/res_green_ipv4.svg" alt="Your requested amount of IPv4 addresses" width="56" height="56">
                        <h4 class="descr-text-feature size-18 m-t-20"><?php _e('Your requested amount of IPv4 addresses')?></h4>
                    </div>
                    <div class="block-col w-300 m-v-30 p-h-40">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/resellers/res_green_multiple_locations.svg" alt="Multiple locations worldwide" width="56" height="56">
                        <h4 class="descr-text-feature size-18 m-t-20"><?php _e('Multiple locations worldwide')?></h4>
                    </div>

                </div>

            </div>

        </section> <!-- end of .reseller-levels -->



        <section class="program-faq extra-pad color-bg-style1" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('FAQ')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/resellers.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .program-faq -->



        <section class="program-register extra-pad-top" data-section-button="<?php _e('Apply Now');?>">

            <div class="container">

                <div class="scroll-id-target" id="registration"></div>

                <div class="section-header">
                    <h1 class="page-title"><?php _e('Become a registered reseller')?></h1>
                </div>

                <div class="registration-invitation center">
                    <a id="start-registration" class="button primary orange large"><?php _e('Register Now');?></a>
                    <p class="p-v-10"><?php _e('Existing users'); ?> &mdash; <a data-popup="<?php echo $config['links']['reseller-sign-in']; ?>" data-popup-id="login" data-popup-type="ajax" href="#" data-callback="$('#login-username').focus()" data-prevent>Apply here</a></p>
                </div>

                 <div id="reseller-sign-up-block" class="registration-form-frame hidden">

                    <form id="regform" name="regform" method="POST" action="">
                        <div class="account-information">
                            <div class="content-block">
                                <h3 class="block-title block-title-small"><?php _e('Account information');?></h3>
                                <p class=""><?php _e('All fields are required');?></p>
                                <div class="block-content">
                                    <div class="fields-group clearfix">
                                        <div id="regerror" class="validation error" style="display:none"></div>
                                        <div class="col span-6">
                                            <div class="form-field ">
                                                <div class="field-title"><label for="email"><?php _e('Email Address');?></label></div>
                                                <div class="field">
                                                    <input class="inspectletIgnore" data-rule-required="true" minlength="3" data-rule-text="true" data-msg-required="<div class='validation error'><?php _e('Please enter a valid email address') ?></div>" data-msg-text="<div class='validation error'><?php _e('Please enter a valid email address') ?></div>" id="email" type="text" name="email" value="" onchange="setCookie('email', 1);" tabindex="1" placeholder="<?php _e('Email Address');?>" />
                                                    <div id="double-submit-email" class="validation prevent-double-submit hidden"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="fields-group clearfix">
                                        <div class="col span-6 clear-left">
                                            <div class="form-field ">
                                                <div class="field-title"><label for="password"><?php _e('Password');?></label></div>
                                                <div class="field">
                                                    <input class="inspectletIgnore" data-rule-required="true" minlength="3" data-rule-text="true" data-msg-required="<div class='validation error'><?php _e('The password you entered is not strong enough - please enter a more complex password') ?></div>" data-msg-text="<div class='validation error'><?php _e('The password you entered is not strong enough - please enter a more complex password') ?></div>" id="password" type="password" name="password" value="" tabindex="2" placeholder="<?php _e('Password');?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col span-6">
                                            <div class="form-field ">
                                                <div class="field-title"><label for="password2"><?php _e('Confirm Password');?></label></div>
                                                <div class="field">
                                                    <input class="inspectletIgnore" data-rule-required="true" minlength="3" data-rule-text="true" data-msg-required="<div class='validation error'><?php _e('The password you entered did not match') ?></div>" data-msg-text="<div class='validation error'><?php _e('The password you entered did not match') ?></div>" id="password2" type="password" name="password2" value="" tabindex="3" placeholder="<?php _e('Confirm Password');?>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="billing-information">
                            <div class="content-block">
                                <h3 class="block-title block-title-small"><?php _e('Billing information') ?></h3>
                                <p><?php _e('All fields are required');?></p>
                                <div class="block-content clearfix">
                                    <div class="col span-6">
                                        <div class="fields-group">
                                            <div class="form-field ">
                                                <div class="field-title"><label for="acctype"><?php _e('Account Type') ?></label></div>
                                                <div class="field" data-dropdown data-dropdown-default>
                                                    <select id="acctype" name="acctype" tabindex="4" data-chosen-element>
                                                        <option value="person"><?php _e('Personal') ?></option>
                                                        <option value="business"><?php _e('Business') ?></option>
                                                    </select>
                                                    <div data-dropdown-label></div>
                                                    <ul data-dropdown-list></ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="fields-group" data-business-account>
                                            <div class="form-field ">
                                                <div class="field-title"><label for="companyname"><?php _e('Company Name') ?></label></div>
                                                <div class="field">
                                                    <input class="inspectletIgnore" required='' data-rule-required="true" minlength="2" data-rule-text="true" data-msg-required="<div class='validation error'><?php _e('Value is not valid') ?></div>" data-msg-text="<div class='validation error'><?php _e('Value is not valid') ?></div>" id="companyname" type="text" name="companyname"  value="" onchange="setCookie('companyname', 1);" tabindex="5" placeholder="<?php _e('Company Name') ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="fields-group" data-business-account>
                                            <div class="form-field ">
                                                <div class="field-title"><label for="companynumber"><?php _e('Company Number') ?></label></div>
                                                <div class="field">
                                                    <input class="inspectletIgnore" required='' data-rule-required="true" minlength="2" data-rule-text="true" data-msg-required="<div class='validation error'><?php _e('Value is not valid') ?></div>" data-msg-text="<div class='validation error'><?php _e('Value is not valid') ?></div>" id="companynumber" type="text" name="companynumber"  value="" onchange="setCookie('companynumber', 1);" tabindex="6" placeholder="<?php _e('Company Number') ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="fields-group" data-business-account>
                                            <div class="form-field ">
                                                <div class="field-title"><label for="vatnumber"><?php _e('VAT number') ?></label></div>
                                                <div class="field">
                                                    <input class="inspectletIgnore" id="vatnumber" type="text" name="vatnumber"  value="" tabindex="7" placeholder="VAT" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="fields-group">
                                            <div class="form-field ">
                                                <div class="field-title">
                                                    <label for="firstname" data-personal-account><?php _e('First name') ?></label>
                                                    <label for="firstname" data-business-account><?php _e("Representitive's first name") ?></label>
                                                </div>
                                                <div class="field">
                                                    <input class="inspectletIgnore" data-rule-required="true" minlength="3" data-rule-text="true" data-msg-required="<div class='validation error'><?php _e('Please enter your first name') ?></div>" data-msg-text="<div class='validation error'><?php _e('Please enter your first name') ?></div>" id="firstname" type="text" name="firstname" value="" onchange="" tabindex="8" placeholder="<?php _e('First Name') ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="fields-group">
                                            <div class="form-field ">
                                                <div class="field-title"><label for="lastname"><?php _e('Last name') ?></label></div>
                                                <div class="field">
                                                    <input class="inspectletIgnore" data-rule-required="true" minlength="3" data-rule-text="true" data-msg-required="<div class='validation error'><?php _e('Please enter your last name') ?></div>" data-msg-text="<div class='validation error'><?php _e('Please enter your last name') ?></div>" id="lastname" type="text" name="lastname" value="" onchange="" tabindex="9" placeholder="<?php _e('Last Name') ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col span-6">
                                        <div class="fields-group">
                                            <div class="form-field ">
                                                <div class="field-title"><label for="country"><?php _e('Country') ?></label></div>
                                                <div class="field">
                                                    <select name="country" data-chosen-element data-country-select tabindex="10"></select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="fields-group statewrapper">
                                            <div class="form-field ">
                                                <div class="field-title"><label for="state"><?php _e('State/Region') ?></label></div>
                                                <div class="field">
                                                    <input required='' class="inspectletIgnore" minlength="2" aria-required="true" id="stateinput" type="text" name="state" value="" onchange="" placeholder="<?php _e('State/Region') ?>"/>
                                                    <select required='' class="inspectletIgnore makeitchosen stateselect" tabindex="11" aria-required="true" id="stateselect" name="stateselect" onchange="">
                                                        <option value=""><?php _e('Please choose one') ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="fields-group">
                                            <div class="form-field ">
                                                <div class="field-title"><label for="city"><?php _e('City') ?></label></div>
                                                <div class="field">
                                                    <input class="inspectletIgnore" data-rule-required="true" minlength="3" data-rule-text="false" data-msg-required="<div class='validation error'><?php _e('Please enter your city') ?></div>" data-msg-text="<div class='validation error'><?php _e('Please enter your city') ?></div>" id="city" type="text" name="city" value="" onchange="update_state();" tabindex="13" placeholder="<?php _e('City') ?>"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="fields-group">
                                            <div class="form-field ">
                                                <div class="field-title"><label for="address1"><?php _e('Address') ?></label></div>
                                                <div class="field">
                                                    <input class="inspectletIgnore" data-rule-required="true" minlength="3" data-rule-text="true" data-msg-required="<div class='validation error'><?php _e('Please enter your address') ?></div>" data-msg-text="<div class='validation error'><?php _e('Please enter your address') ?></div>" id="address1" type="text" name="address1" value="" onchange="" tabindex="14" placeholder="<?php _e('Address') ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="fields-group">
                                            <div class="form-field ">
                                                <div class="field-title"><label for="postcode"><?php _e('Zip Code') ?></label></div>
                                                <div class="field">
                                                    <input class="inspectletIgnore" data-rule-required="true" minlength="3" data-rule-text="true" data-msg-required="<div class='validation error'><?php _e('Please enter your postcode') ?></div>" data-msg-text="<div class='validation error'><?php _e('Please enter your postcode') ?></div>" id="postcode" type="text" name="postcode" value="" onchange="" tabindex="15" placeholder="<?php _e('Zip Code') ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="fields-group">
                                            <div class="form-field ">
                                                <div class="field-title"><label for="phonenumbersufix"><?php _e('Phone Number') ?></label></div>
                                                <div class="field">
                                                    <div class="phonenumberprefix" style="width:17%;">
                                                        <input class="inspectletIgnore" type="text" id="phonenrprefix" name="phonenumberprefix" size="6" value="" disabled="true" style="border-right: none;"  onchange=""/>
                                                    </div>
                                                    <div class="phonenumbersufix" style="width:82.5%;">
                                                        <input required="" minlength="5" aria-required="true" aria-invalid="true" data-rule-text="true" data-msg-required="<div class='validation error'><?php _e('Please enter a valid phone number') ?></div>" data-msg-text="<div class='validation error'><?php _e('Please enter a valid phone number') ?></div>" class="inspectletIgnore" id="phonenumbersufix" type="text" name="phonenumbersufix" size="20" value="" onchange="" tabindex="16" placeholder="<?php _e('Phone Number') ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input id="phonenumber" class="inspectletIgnore" type="hidden" name="phonenumber" value="" />
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="content-block center">
                          <div class="block-content">
                              <button type="submit" href="" class="button primary orange large registration-submit" tabindex="17"><?php _e('Register') ?></button>
                          </div>
                      </div>
                    </form>
                    

                 </div>
                
                <div class="reseller-sign-up-success hidden"></div>

<script>

    (function($){

        $(document).ready(function(){


            /*section.program-register*/
            /*Registration form fields interaction and data filling handler*/
            
            function resizeIframe(obj) {
                obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
            }

            $('a[data-scrollto="registration"]').on('click', function(){
                show_registration();
            });

            $('a#start-registration').click(function(){
                show_registration();
            });

            $('#acctype').on('change', function(){  // listener of account type changes
                toggleAccType(this);
            });

            function show_registration(){
                $('.registration-form-frame').slideDown();
                $('.registration-invitation').hide();

                toggleAccType('#acctype');

                //purge countries
                for (var iso in countries) {
                    if (countries.hasOwnProperty(iso)) {
                    $('[data-country-select]').append( '<option value="'+iso+'">'+countries[iso]+'</option>' );
                    }
                }
                $("input[name=phonenumberprefix]").val($phonecode[$("select[name=country]").val()]);
                update_state();
            }

            $("select[name=country]").change(function(){
                update_state();
                $("input[name=phonenumberprefix]").val($phonecode[$("select[name=country]").val()]);
            });

            function toggleAccType(dropdown){
                if ($(dropdown).val() == 'person')
                {
                    $('[data-personal-account]').show();
                    $('[data-business-account]').hide();
                }
                else if ($(dropdown).val() == 'business')
                {
                    $('[data-personal-account]').hide();
                    $('[data-business-account]').show();
                }
            };

            // Skipping implementation
            // $(document).ready(function(){
            //     var object = $('.chosen-select').chosen({});
            // });

            function update_state(){

                var stateInput = getCookie('stateInput');
                var stateSelect = getCookie('stateSelect');
                var $stateWrapper = $('.statewrapper');
                var $stateInput = $('[name="state"]');
                var $stateSelect = $('[name="state"]+select');
                var $cityInput = $('[name="city"]');

                // Custom dropdown
                var $countryInput = $('[name="country"]');
                var country = $countryInput.val();

                if (states[country]){

                    // dropdowns reset
                    $stateSelect.val($stateSelect.find('option:first').val());
                    $stateSelect.find('option:gt(0)').remove();
                    // show select

                    $stateWrapper.show();
                    $stateInput.val('');
                    $stateInput.hide();

                    // Put States/Regions into select
                    $.each(states[country], function(index, value) {
                        if (value == "end") {
                            return false;
                        }
                        $stateSelect.append('<option value="'+value+'"'+(stateSelect == value?' selected':'')+'>'+value+'</option>');
                    });
                    $stateSelect.change();
                    $stateSelect.trigger("chosen:updated");
                } else {
                    $stateInput.val( $cityInput.val() );
                    $stateInput.hide();
                    $stateWrapper.hide();
                    // dropdowns reset
                    $stateSelect.val($stateSelect.find('option:first').val());
                    $stateSelect.find('option:gt(0)').remove();
                    $stateSelect.change();
                }
            }


            /*---------------------------*/


            /*popup window*/
            /*Ajax communication to WHMCS handler - Sign In function*/

            var countries = {};
            var request_countries = $.ajax({
                url: "<?php echo $config['whmcs_links']['countries_json']; ?>",
                method: "GET",
                async: true,
                dataType: "json",
                crossDomain: false // force to send HTTP_X_REQUESTED_WITH header
            });
            request_countries.done(function( data ) {
                countries = data;
            });

            //ajax stuff for reseller SIGN IN
            $(document).on('submit', '#reseller_sign_in_form', function( e ){
                e.preventDefault();

                var emailSubmited = $(this).find('#login-username').val(),
                    emailSaved = 'reseller:' + emailSubmited;

                $('[data-ajax-message-wrapper]').hide().removeClass('error').removeClass('success');
                $('[data-ajax-message-text]').text('');

                var email = $.trim( $(this).find('#login-username').val() );
                var pass = $.trim( $(this).find('#login-password').val() );

                if ( email.length < 5 || pass.length < 5 ){
                    $('[data-ajax-message-text]').text( 'Login details are incorrect. Please try again.' );
                    $('[data-ajax-message-wrapper]').show().addClass('error');
                    return;
                }

                if ( getCookie(emailSaved) == 1 ){
                    $('[data-ajax-message-text]').text( 'Email is in reseller program' );
                    $('[data-ajax-message-wrapper]').show().addClass('error');
                    return;
                }

                //do ajax
                var request = $.ajax({
                    url: "<?php echo $config['whmcs_links']['reseller-sign-in']; ?>",
                    method: "POST",
                    data: { email: email, pass: pass },
                    async: true,
                    dataType: "json",
                    crossDomain: false // force to send HTTP_X_REQUESTED_WITH header
                });

                request.done(function( data ) {

                    /*preventing the same email registration*/
                    var emailSubmited = $('#reseller_sign_in_form').find('#login-username').val(),
                        emailSaved = 'reseller:' + emailSubmited;

                    if ( data.success == true ){
                        $('[data-ajax-message-text]').text('<?php _e('Thank you for submitting your application.') ?>');
                        $('[data-ajax-message-wrapper]').show().addClass('success');

                        //set program cookie
                        setCookie( emailSaved, 1, 356 );
                    } else {
                        $('[data-ajax-message-text]').text( data.message );
                        $('[data-ajax-message-wrapper]').show().addClass('error');
                    }

                });

                request.fail(function( error ) {
                    $('[data-ajax-message-text]').text( 'Failure.' );
                    $('[data-ajax-message-wrapper]').show().addClass('error');
                });

            });     // reseller sign in end


            /*---------------------------*/


            /* section.program-register #regform */
            /*Form validation Ajax communication to WHMCS handler */
            /*dependency on jquery.validate*/

            var form = $("#regform").validate({
                success: "valid",
                ignore: ":not(:visible)",
                rules: {
                    password: "required",
                    password2: {
                        equalTo: "#password"
                    },
                    //firstname: {lettersonly: true},
                    //lastname: {lettersonly: true},
                    //city: {lettersonly: false},
                    phonenumbersufix: {
                        required: true,
                        digits: true
                    }
                },
                onclick: false
            });


            function goToByScroll(id) {
                /*Used for scrolling to error spot*/
                
                id = id.replace("link", ""); // Remove "link" from the ID
                var deltaScroll = 0,
                    heightStickyHeader = $('.sticky-wrapper.is-sticky').height();

                if (heightStickyHeader > 0) {
                    deltaScroll = heightStickyHeader;
                }
                $('html,body').animate({scrollTop: $("#" + id).offset().top - deltaScroll},'slow');
            };

            // Reseller REGISTRATION 
            $('#regform').submit(function (ev) {
                ev.preventDefault();

                var emailSubmited = $(this).find('#email').val(),
                    emailSaved = 'reseller:' + emailSubmited;

                if ( getCookie(emailSaved) == 1 ){
                    $('.validation.prevent-double-submit').text( 'You have already applied for Reseller Program.' );
                    $('.validation.prevent-double-submit').show().addClass('error');
                    goToByScroll('reseller-sign-up-block');
                }

                if (form.valid() === true) {

                    //do ajax
                    $.ajax({
                        url: "<?php echo $config['whmcs_links']['reseller-sign-up']; ?>",
                        data: $("#regform").serialize(),
                        type: 'POST',
                        async: true,
                        dataType: "json",
                        crossDomain: false, // force to send HTTP_X_REQUESTED_WITH header
                        beforeSend: function () {
                            $('#regerror').hide();
                        },
                        success: function (response) {
                            if( response.success ){
                            setCookie( emailSaved, 1, 356 );

                            $('#reseller-sign-up-block').remove();
                            $('.reseller-sign-up-success').html('<p class="center"><strong>Thank you for submitting your application.</strong></p>');
                            $('.reseller-sign-up-success').show();
                            
                            //alert( 'thank u' );
                            } else {
                                $('#regerror').show();
                                $('#regerror').html( response.message );
                                goToByScroll('regerror');
                            }
                        }
                    });
                } else {
                    return false;
                }

                $('[name = phonenumber]').val(($('[name="phonenumbersufix"]').val()).replace(/\D/g, ''));

            }); // reseller registration end



        });

    })(jQuery);
    
</script>

            </div> <!-- end of .container -->

        </section> <!-- end of .program-register -->


    </section> <!-- end of .main -->

</article>



<?php

// no universal redirect to other locale

get_footer();

?>
