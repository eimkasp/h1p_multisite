<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Landing: Windows Cloud Server
*/

/* 
 * NOTE: Please add form_id custom field of corresponding contact-form-7 form in this WP page
 */

$custom_fields = get_post_custom();
$contact_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings2/landing.css', ['style']);

wp_enqueue_script('recaptcha', 'https://www.google.com/recaptcha/api.js?&hl=' . $lang_code, [], false, true);
wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);
wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);


global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'cs_win');

/*Get Cloud IP config option pricing
-------------------------*/
// id=342

$cloud_hosting_config_options = $whmcs->getConfigurablePrices(342);
$cloud_hosting_ip = $cloud_hosting_config_options['IP']['options']['IP']['pricing']['monthly'];

// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing cloud-servers cs-hosting">

    <header class="headline landing-wincs">
        <div class="container adjust-vertical-center">

            <img class="m-t-40" src="<?php echo $images ?>landings2/images/header_icon_wincs.svg" alt="Windows Cloud Servers" width="105" height="105">
            <h1 class="page-title m-up-40"><?php _e('Scalable Windows Cloud Servers')?></h1>
            <div class="title-descr"><?php _e('Deploy your projects on a solid Cloud infrastructure.');?></div>

            <div class="row center p-t-30">
                <a class="button highlight large" href="#plans" data-scrollto="plans"><?php _e('Compare Plans');?></a>
                <p class="white p-t-10"><?php printf(__('From %s only!'), $mypage->getMinPrice()); ?></p> 
            </div>

        </div>
    </header> <!-- end of .headline.cs-hosting -->

    

    <section class="main page-content">



        <section class="service-features extra-pad">

            <div class="container">

                <div class="section-header p-b-20">
                    <h2 class="block-title"><?php _e('Windows Servers in the Cloud')?></h2>
                </div>

                <div class="container-flex as-row">

                    <div class="block-col width-lg-1-3 order-1 center-sm text-left-md text-left-lg p-h-20 p-t-30">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/wserv_env_exp.svg" alt="<?php _e('Windows Cloud Server hands on experience feature image')?>">
                    </div>
                    <div class="block-col width-lg-2-3 order-2 p-h-30 p-v-60">
                        <h3 class="title-paragraph p-b-20"><?php _e('Hands-On Experience')?></h3>
                        <p class="t-18-spaced"><?php _e('A fully isolated Windows Cloud Server provides better performance, extensive configurability and solid security. It\'s much easier to work on your development projects with an already familiar graphical user interface.'); ?></p>
                    </div>

                    <div class="block-separator"></div> 

                    <div class="block-col width-lg-1-3 order-4 center-sm text-right-md text-right-lg p-h-20 p-t-30">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/wserv_workspace.svg" alt="<?php _e('Windows Cloud Servers well-known work-space feature image')?>">
                    </div>
                    <div class="block-col width-lg-2-3 order-3 p-h-30 p-v-60">
                        <h3 class="title-paragraph p-b-20"><?php _e('Well-Known Workspace')?></h3>
                        <p class="t-18-spaced"><?php _e('A simple Windows authentication mechanism will allow you to log in to your server in a couple of seconds using Windows built-in RDP client without the need to install any additional software to your Windows PC.'); ?></p>
                    </div>

                    <div class="block-separator"></div>

                    <div class="block-col width-lg-1-1 order-5 p-h-30 p-t-60 p-b-60-md">
                        <h3 class="title-paragraph p-b-20 center"><?php _e('Windows Software Components')?></h3>
                        <p class="center p-b-40"><?php _e('Install and use any Windows-only based software and software components on your Windows Cloud server.');?></p>
                        <img class="m-h-100-md" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/wserv_software_comp.svg" alt="<?php _e('Windows Cloud Servers software components such as MySQL, ASP.NET, Firewalls, etc., feature image')?>">
                    </div>

                <div class="block-separator"></div>
                
                    <div class="block-fluid order-6 p-h-30">
                        <ul class="features-list checklist brightblue">
                            <li><?php _e('IIS Server'); ?></li>
                            <li><?php _e('ASP & ASP.NET')?></li>
                            <li><?php _e('MS SQL Server')?></li>
                        </ul>
                    </div>
                    <div class="block-fluid order-7 p-h-30">
                        <ul class="features-list checklist brightblue">
                            <li><?php _e('Active Directory')?></li>
                            <li><?php _e('Windows Firewall')?></li>
                            <li><?php _e('DNS Server')?></li>
                        </ul>
                    </div>
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->


        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>


        

        <section class="service-features extra-pad color-bg-style-grad1" data-section-title="<?php _e('Features');?>">

            <div id="features" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title white"><?php _e('Cloud Server Features'); ?></h2>
                </div>

                <div class="four-col-row-biface-cards">

                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_flex-scalability_blue.svg" alt="<?php _e('Flexible Windows Cloud Server feature icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Flexible Scaling')?></h3>
                                <p class="white"><?php _e('No longer bound by the physical size of your server, quickly scale your Cloud based virtual machine at your Client Area.'); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_api_blue.svg" alt="<?php _e('Windows Cloud Server powerful API icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Powerful API')?></h3>
                                <p class="white"><?php _e('Directly access web control panel functionality and benefit from advanced Cloud networking features.'); ?></p>
                            </div> 
                        </div>
                    </div>

                </div>
                <div class="four-col-row-biface-cards p-b-40">

                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_auto-backups_blue.svg" alt="<?php _e('Scheduled backups for Windows Cloud Servers feature icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Scheduled Backups')?></h3>
                                <p class="white"><?php _e('Schedule daily, weekly or monthly automated backups. You can also create backups manually whenever you have to!'); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_custom-os_blue.svg" alt="<?php _e('Windows Cloud Server custom ISO feature icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Custom ISO')?></h3>
                                <p class="white"><?php _e('Custom ISO allows you to mount a configured image on your Cloud instance and run through the boot & setup processes as you would on a bare metal server.'); ?></p>
                            </div> 
                        </div>
                    </div>

                </div> 

            </div> <!-- end of .container -->

            <div class="row center p-t-20">
                <a href="<?php echo $config['links']['cloud-servers'].'#features'; ?>" class="button primary large"><?php _e('More Features');?></a>
            </div>

        </section> <!-- end of .service-features-more -->


        <section class="service-cta p-v-60">

            <div class="container">
                
                <div class="container-flex cta">
                    <div class="block-col width-md-2-3 cta-left">
                        <span class="block-title-xlarge-thin t-ff-header t-mspaced color-primary"><?php _e('Looking for something else?');?></span>
                    </div>
                    <div class="block-col width-md-1-3 cta-right">
                        <a href="/cheap-vps-hosting/" class="button xlarge ghost-primary"><?php _e('Cheap VPS');?></a>
                    </div> 
                </div> 

            </div>

        </section> <!--end of .service-cta-->
        

        <section class="service-locations extra-pad-top color-bg-style2 no-zoom" data-section-title="<?php _e('Locations');?>">

            <div id="locations" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Worldwide Server Locations')?></h2>
                        <p><?php _e('Multiple premium data centers ensure high connectivity and better reach.'); ?></p>
                </div>

                <?php include_block("htmlblocks/locations-services.php", array('type' => 'cloud', 'speedtest' => true )); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-locations -->



        <section class="service-pricing extra-pad-top" data-section-title="<?php _e('Plans');?>">

            <div id="plans" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Windows Server Pricing')?></h2>
                </div>

                <?php include_block("htmlblocks/pricing-tables/cs-hosting-win-v2.php"); ?>

                <a data-link-target="<?php echo $mypage->getMinPriceCheckoutLink(); ?>" data-section-button="<?php _e('Order Now');?>" data-button-styling="button highlight <?php echo $mypage->checkoutActivityClass($mypage->getCheapestLocation()); ?>" class="hidden"></a>

                <?php include_block("htmlblocks/pricing-tables/cs-all-plans-include.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-pricing -->

        
        
        <section class="contact-sales extra-pad-top color-bg-style5">

            <div id="contact-form" class="scroll-id-target"></div>

            <div class="container">

                <div class="container-flex">
                    <div class="block-col width-md-1-4 center p-b-20">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/question-baloon.svg" alt="<?php _e('Icon illustrating sales chat');?>" width="180">
                    </div> 
                    <div class="block-col width-md-3-4 p-h-30 center-sm">
                        <p class="title-paragraph-lite"><?php _e('Have questions about Cloud technology? Looking for a custom solution? Contact us to discuss your needs.'); ?></p>
                        <a href="" id="get-form" class="button primary large m-v-30"><?php _e('Get in Touch');?></a>
                        <div class="contact-form p-v-30" style="display:none;">
                        
                            <?php echo do_shortcode('[contact-form-7 id="'.$contact_form_id.'" title="Contact Sales" html_class="wpcf7-form captcha"]');?>

                        </div> 
                    </div>
                </div> 

            </div>

        </section> <!-- end of .contact-sales -->

<script>

    (function($){
        $(document).ready(function(){

            function goToByScroll(id) {
                /*Used for scrolling to error spot*/
                
                id = id.replace("link", ""); // Remove "link" from the ID
                var deltaScroll = 0,
                    heightStickyHeader = $('.sticky-wrapper.is-sticky').height();

                if (heightStickyHeader > 0) {
                    deltaScroll = heightStickyHeader;
                }
                $('html,body').animate({scrollTop: $("#" + id).offset().top - deltaScroll},'slow');
            };


            $('#get-form').click(function(e){
                e.preventDefault();
                $(this).hide();
                $(this).parent().find('.contact-form').show('slow');
            });

            // check for form postprocess hash '..wcpf7..' and show form if some errors occured
            var linkHash = location.hash;

            if (linkHash.indexOf('wpcf7') > 0) {
                $('#get-form').hide();
                $('.contact-form').show();
                goToByScroll('contact-form');
            }

        })
    })(jQuery)

</script>

        <section class="service-faq extra-pad-top" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/land-cs-win.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->



    </section> <!-- end of .main -->

</article>



<?php

include_block("htmlblocks/hosting/speedtest_enabling_js.php");

universal_redirect_footer([
    'en' => $site_en_url.'/windows-cloud-servers/',
    'br' => $site_br_url.'/servidor-cloud-windows/'
]);

get_footer();

?>
