<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Landing: Linux Server
*/

/* 
 * NOTE: Please add form_id custom field of corresponding contact-form-7 form in this WP page
 */

$custom_fields = get_post_custom();
$contact_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings2/landing.css', ['style']);

wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);


global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'vps');
/*This page is VPS and CS all together*/

// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing cloud-servers landing-linuxsrv">

    <header class="headline landing-linuxsrv min-h-450 adjust-vertical-center">
        <div class="container">

            <img class="responsive m-t-40" src="<?php echo $images ?>landings2/images/header_icon_vps.svg" alt="Linux server" width="105" height="105">
            <h1 class="responsive page-title m-up-40">Linux Server</h1>
            <div class="title-descr p-t-20 t-mspaced">Trying to grasp the difference between Cloud & VPS Linux servers? Wondering which Linux distribution is best tailored to your needs?</div>

            <div class="row center p-t-40">
                <a class="button highlight large" href="#features-start" data-scrollto="features-start">Get Answers</a>
            </div>

        </div>
    </header> <!-- end of .headline.cs-hosting -->

    

    <section class="main page-content">



        <section class="service-features-1 extra-pad">

            <div id="features-start" class="scroll-id-target" style="top: -50px;"></div>

            <div class="container">

                <div class="section-header p-b-20">
                    <h2 class="block-title">What is a Linux server?</h2>
                </div>

                <div class="container-flex as-row">

                    <div class="block-col width-lg-1-1 p-30">
                        <img class="m-h-100-md m-b-30 scale" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/linux_server_feat.png" alt="What is Linux server icon">
                        <p class="t-xspaced p-h-20 center">Breaking it down to simple terms a Linux server — a virtual machine with allocated resources running Linux open source operating system is what we call Linux server. Linux servers are frequently selected over other server operating systems for their stability, security, flexibility and come with a variety of distributions to choose from.</p>
                    </div>
                
                </div> <!-- end of .container-flex -->

                <div class="row center p-t-20 m-r-10">
                    <a href="#distributions" data-scrollto="distributions" class="button primary highlight large"><?php _e('Explore Distributions');?></a>
                </div>

            </div> <!--end of .container-->

        </section> <!--end of .service-features-1-->


        <section class="service-features-2 extra-pad color-bg-style5">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title">Linux VPS & Cloud Server Differences</h2>
                </div>

                <div class="container-flex as-row">

                    <div class="block-col width-lg-1-1 p-30">
                        <img class="m-b-30 scale" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/linux_vps_cs_differences.png" alt="Linux servers comparison"/>
                    </div>

                </div>

                <div class="container-flex features-list m-t-20 p-v-20">

                    <div class="block-col width-md-1-2 p-h-20 md-vert-pad container-flex">
                        <div class="inner-w-90">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/header_icon_linux_vps_orange.svg" alt="Linux VPS feature image" width="80">
                        </div>
                        <div class="inner-w-90-whatsleft">
                            <h3 class="title-paragraph-lower p-t-10 p-b-20">Linux VPS</h3>
                            <p class="adjust-left land-color-style1">Our Linux VPS servers are powered by OpenVZ virtualization offering simplified management, high performance, pre-installed templates and full control. Due to OpenVZ restrictions, Linux VPS share the same kernel, thus it cannot be modified by the user for advanced configuration.</p>
                            <a class="button highlight large m-t-40" href="<?php echo $config['links']['vps-hosting']; ?>">View Plans</a>
                        </div>
                    </div>
                    <div class="block-col width-md-1-2 p-h-20 md-vert-pad container-flex">
                        <div class="inner-w-90">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/header_icon_linux_cs_blue.svg" alt="Linux Cloud Server feature image" width="80">
                        </div>
                        <div class="inner-w-90-whatsleft">
                            <h3 class="title-paragraph-lower p-t-10 p-b-20">Linux Cloud Server</h3>
                            <p class="adjust-left land-color-style1">KVM technology enables every user to run their own isolated copy of Linux, providing a greater choice of distributions, higher performance, deeper configuration, stronger isolation and better security. API integration and kernel modifications are also available for power users.</p>
                            <a class="button highlight large m-t-40" href="<?php echo $config['links']['cloud-servers']; ?>">Explore</a>
                        </div>
                    </div>

                </div> <!-- end of .container-flex -->

            </div> <!-- end of .container -->
            
        </section> <!-- end of .service-features-2 -->
        


        <section class="service-description extra-pad-top">

            <div id="distributions" class="scroll-id-target" style="top: -50px;"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Linux Server Distributions')?></h2>
                    <span class="title-follow-text"><?php _e('Compare available Linux distributions and find out which one suits your needs.'); ?></span>
                </div>

                <div class="block-of-fourcross p-b-40">

                    <div class="layout-row two-col-row">
                        <div class="block-col center p-v-40">
                            <div class="icon-part p-v-20 center">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/hexa-feat-blue-centos.svg" alt="<?php _e('Linux CentOS distribution icon')?>" height="170">
                            </div>
                            <div class="text-part">
                                <div class="text-block">
                                    <h3 class="block-title-small"><?php _e('CentOS');?></h3>
                                    <p>A stable, predictable, manageable and reproducible platform derived from the source of Red Hat Enterprise Linux - free alternative to those seeking RHEL.</p>
                                </div>
                            </div>
                        </div>
                        <div class="block-col center p-v-40">
                            <div class="icon-part p-v-20 center">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/hexa-feat-blue-debian.svg" alt="<?php _e('Linux Debian distribution icon')?>" height="170">
                            </div>
                            <div class="text-part">
                                <div class="text-block">
                                    <h3 class="block-title-small"><?php _e('Debian');?></h3>
                                    <p>Easy management, high security and top-of-the-line stability. Debian comes loaded with more than 43,000 packages and relies only on stable software versions.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layout-row two-col-row">
                        <div class="block-col center p-v-40">
                            <div class="icon-part p-v-20 center">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/hexa-feat-blue-fedora.svg" alt="<?php _e('Linux Fedora distribution icon')?>" height="170">
                            </div>
                            <div class="text-part">
                                <div class="text-block">
                                    <h3 class="block-title-small"><?php _e('Fedora');?></h3>
                                    <p>Built and used by people across the globe Fedora is free to use, modify & distribute. This Linux distribution is known to handle graphics well and uses appealing interfaces.</p>
                                </div>
                            </div>
                        </div>
                        <div class="block-col center p-v-40">
                            <div class="icon-part p-v-20 center">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/hexa-feat-blue-ubuntu.svg" alt="<?php _e('Linux Ubuntu distribution icon')?>" height="170">
                            </div>
                            <div class="text-part">
                                <div class="text-block">
                                    <h3 class="block-title-small"><?php _e('Ubuntu');?></h3>
                                    <p>For those who like Debian, but want the latest software, Ubuntu is a perfect choice. Ubuntu is also well suited for new users who wants to familiarize with Linux.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> <!-- end of .block-of-fourcross -->

            </div> <!-- end of .container -->

        </section> <!-- end of .service-description -->


        <section class="contact-sales extra-pad-top color-bg-style5">

            <div id="contact-form" class="scroll-id-target"></div>

            <div class="container">

                <div class="container-flex">
                    <div class="block-col width-md-1-4 center p-b-20">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/question-baloon.svg" alt="<?php _e('Icon illustrating sales chat');?>" width="180">
                    </div> 
                    <div class="block-col width-md-3-4 p-h-30 center-sm">
                        <p class="title-paragraph-lite">Still have doubts about Linux servers? Looking for someone to consult you? We are here to help!</p>
                        <a href="" id="get-form" class="button primary large m-v-30"><?php _e('Get in Touch');?></a>
                        <div class="contact-form p-v-30" style="display:none;">
                        
                            <?php echo do_shortcode('[contact-form-7 id="'.$contact_form_id.'" title="Contact Sales" html_class="wpcf7-form captcha"]');?>

                        </div> 
                    </div>
                </div> 

            </div>

        </section> <!-- end of .contact-sales -->


<script>

    (function($){
        $(document).ready(function(){

            function goToByScroll(id) {
                /*Used for scrolling to error spot*/
                
                id = id.replace("link", ""); // Remove "link" from the ID
                var deltaScroll = 0,
                    heightStickyHeader = $('.sticky-wrapper.is-sticky').height();
                
                if (heightStickyHeader > 0) {
                    deltaScroll = heightStickyHeader;
                }
                $('html,body').animate({scrollTop: $("#" + id).offset().top - deltaScroll},'slow');
            };


            $('#get-form').click(function(e){
                e.preventDefault();
                $(this).hide();
                $(this).parent().find('.contact-form').show('slow');
            });

            // check for form postprocess hash '..wcpf7..' and show form if some errors occured
            var linkHash = location.hash;

            if (linkHash.indexOf('wpcf7') > 0) {
                $('#get-form').hide();
                $('.contact-form').show();
                goToByScroll('contact-form');
            }

        })
    })(jQuery)

</script>

        <section class="service-faq extra-pad-top" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/land-linux-server.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->



    </section> <!-- end of .main -->

</article>



<?php

include_block("htmlblocks/hosting/speedtest_enabling_js.php");

universal_redirect_footer([
    'en' => $site_en_url.'/linux-server/',
    'br' => $site_br_url.'/'
]);

get_footer();

?>
