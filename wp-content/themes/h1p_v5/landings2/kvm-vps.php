<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Landing: KVM VPS
*/

/*
-----------------------------------------------------------------
    ATTENTION!
    This is Windows Cloud Server actually, only called VPS
-----------------------------------------------------------------
*/


$custom_fields = get_post_custom();
$contact_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings2/landing.css', ['style']);

wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'cs_lin');

/*Get Cloud IP config option pricing
-------------------------*/
// id=342

$cloud_hosting_config_options = $whmcs->getConfigurablePrices(342);
$cloud_hosting_ip = $cloud_hosting_config_options['IP']['options']['IP']['pricing']['monthly'];

/*Windows licence addon pricing*/
$addons_winlic = $whmcs->getAddonsPrices(33);
$cloud_hosting_winlic =  $addons_winlic['price']; // it is monthly!

// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing vps-hosting cloud-servers">

    <header class="headline landing-kvmvps landing-header1">
        <div class="container adjust-vertical-center">
            <div class="wrapper">

                <h1 class="page-title">Scalable KVM VPS Hosting</h1>
                <div class="title-descr p-t-10">Store your projects in a virtual KVM environment.</div>

                <div class="home-slider-feat-wrap center m-v-20">
                    <div class="feat-wrap-block">
                        <i class="fa fa-check"></i> <?php _e('KVM Hypervisor')?>
                    </div>
                    <div class="feat-wrap-block">
                        <i class="fa fa-check"></i> <?php _e('Powerful API')?>
                    </div>
                    <div class="feat-wrap-block">
                        <i class="fa fa-check"></i> <?php _e('Simple Documentation')?>
                    </div>
                </div>

                <div class="center p-t-40">
                    <a href="#plans" data-scrollto="plans" class="button highlight large p-h-40"><?php _e('View plans');?></a>
                    <p class="white p-t-10"><?php printf(__('Starting from %s!'), $mypage->getMinPrice()); ?></p> 
                </div>
            </div>
        </header> <!-- end of .headline -->

        </div>
    </header> <!-- end of .headline.cs-hosting -->


    <section class="main page-content">

        <section class="service-description extra-pad">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title p-b-20">What is KVM VPS?</h2>
                </div>

                <div class="container-flex as-row">

                    <div class="block-col p-b-40 p-h-20">
                        <p class="center">Our KVM VPS servers operate on KVM Hyper-V, which is a hypervisor-based technology providing true hardware virtualization and ultimate isolation.</p>
                    </div> 

                    <div class="block-col width-lg-1-1 order-5 p-h-30 center">
                        <img class="scale" src="<?php echo $images; ?>landings2/images/win_vps_server2_stack.png" alt="<?php _e('VPS KVM image'); ?>">
                    </div>

                
                </div>
                
                <div class="container-flex features-list m-t-20 p-v-40 p-h-20 land-color-bg-style2">

                    <div class="block-col width-md-1-3 p-h-20 separator-line md-vert-pad">
                        <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_workload_friendly_bluemix.svg" alt="Workload friendly feature icon" width="90" height="90">
                        <h3 class="title-paragraph-lower p-v-20">Workload Friendly</h3>
                        <p class="adjust-left land-color-style1">KVM VPS gets all the allocated resources, without sharing them with anyone else to guarantee maximum performance for high-workload tasks.</p>
                    </div>
                    <div class="block-col width-md-1-3 p-h-20 separator-line md-vert-pad">
                        <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_customization_bluemix.svg" alt="Customization icon" width="90" height="90">
                        <h3 class="title-paragraph-lower p-v-20">Customization</h3>
                        <p class="adjust-left land-color-style1">The virtualization runs on an independent Linux kernel allowing you to modify your KVM VPS kernel as well as upgrade or downgrade it if needed.</p>
                    </div>
                    <div class="block-col width-md-1-3 p-h-20 separator-line md-vert-pad">
                        <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_safe_environment_bluemix.svg" alt="Safe environment icon" width="90" height="90">
                        <h3 class="title-paragraph-lower p-v-20">Safe Environment</h3>
                        <p class="adjust-left land-color-style1">KVM VPS provides increased stability, security and overall performance because of true virtualization resulting in full VPS isolation.</p>
                    </div>

                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-description -->



        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>


        <section class="service-features extra-pad color-bg-style-grad1" data-section-title="<?php _e('Features');?>">

            <div id="features" class="scroll-id-target"></div>

            <div class="container">

                <div class="block-of-two-1-4">

                    <div class="block-col p-b-40">
                        <h2 class="block-title white p-b-20">Build your projects on KVM VPS</h2>
                        <p class="p-v-10 white-dim1">Bringing the innovation behind the world's largest datacenters to your private workspace.</p>
                    </div> 
                    <div class="block-col p-h-20">

                        <div class="two-col-row">
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_kvm_white.svg" alt="<?php _e('KVM virtualization icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('KVM virtualization')?></h3>
                                <p class="t-14 t-xspaced white-dim1">The most desired hypervisor with Windows support, high stability, immaculate performance & solid security.</p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_scalable-resources_white.svg" alt="<?php _e('Scalable Resources feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Flexible Scaling')?></h3>
                                <p class="t-14 t-xspaced white-dim1">No longer bound by the physical size of your server, quickly scale your KVM VPS machine at your Client Area.</p>
                            </div>
                        </div> <!-- end of .two-col-row -->

                        <div class="row m-v-30-md"></div>

                        <div class="two-col-row">
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_api_white.svg" alt="<?php _e('Powerful API icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Powerful API')?></h3>
                                <p class="t-14 t-xspaced white-dim1">Directly access web control panel functionality and benefit from advanced KVM VPS networking features.</p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_auto-backups_white.svg" alt="<?php _e('Scheduled backups icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Scheduled Backups')?></h3>
                                <p class="t-14 t-xspaced white-dim1">Schedule daily, weekly or monthly automated backups. You can also create backups manually whenever you have to!</p>
                            </div>
                        </div> <!-- end of .two-col-row -->

                        <div class="row m-v-30-md"></div>

                        <div class="two-col-row">
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_custom-os-iso_white.svg" alt="<?php _e('Custom ISO icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Custom ISO')?></h3>
                                <p class="t-14 t-xspaced white-dim1">Custom ISO allows you to mount a configured image on your KVM VPS and run through the boot & setup processes as you would on a bare metal server.</p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_live-stats1_white.svg" alt="<?php _e('Live stats icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Live Stats')?></h3>
                                <p class="t-14 t-xspaced white-dim1"><?php _e('Track your resource usage live! Enable alerts to avoid CPU, memory or network overuse.'); ?></p>
                            </div>
                        </div> <!-- end of .two-col-row -->

                        <div class="row m-v-30-md"></div>

                        <div class="two-col-row">
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_dns_white.svg" alt="<?php _e('DNS icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('DNS Management')?></h3>
                                <p class="t-14 t-xspaced white-dim1"><?php _e('Easily create your domain zones and edit records directly at your Client Area.'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_rdns_white.svg" alt="<?php _e('rDNS icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('rDNS Control')?></h3>
                                <p class="t-14 t-xspaced white-dim1"><?php _e('Simplified rDNS self-management means less time spent getting support and more time working on your own projects.'); ?></p>
                            </div>
                        </div> <!-- end of .two-col-row -->

                    </div> 

                </div> <!-- end of .block-of-two-1-4 -->


            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->


        <section class="service-locations extra-pad-top color-bg-style2 no-zoom" data-section-title="<?php _e('Locations');?>">

            <div id="locations" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title">KVM VPS Locations</h2>
                        <p><?php _e('Multiple premium data centers ensure high connectivity and better reach.'); ?></p>
                </div>

                <?php include_block("htmlblocks/locations-services.php", array('type' => 'cloud', 'speedtest' => true )); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-locations -->


        <section class="service-pricing extra-pad-top" data-section-button="<?php _e('Order Now');?>" data-button-styling="button highlight">

            <div id="plans" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header p-b-10">
                    <h2 class="block-title">KVM VPS Pricings</h2>
                </div>

                <div class="segment-wrapper roundlike size-large non-responsive p-t-20">
                    <select name="plan-switch" class="segment-select">
                        <option value="linux" data-icosrc="<?php echo $images;?>img/ico_os_linux.svg"><?php _e('Linux OS'); ?></option>
                        <option value="windows" data-icosrc="<?php echo $images;?>img/ico_os_windows.svg"><?php _e('Windows OS'); ?> </option>
                    </select>
                </div>

                <div id="block-linux" class="block-switchable">

                    <?php include_block("htmlblocks/pricing-tables/cs-hosting-lin-v2.php"); ?>

                </div>

                <div id="block-windows" class="block-switchable">

                    <?php include_block("htmlblocks/pricing-tables/cs-hosting-win-v2.php"); ?>

                </div>

                <?php include_block("htmlblocks/pricing-tables/cs-all-plans-include.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-pricing -->


        <section class="contact-sales extra-pad-top color-bg-style5">

            <div id="contact-form" class="scroll-id-target"></div>

            <div class="container">

                <div class="container-flex">
                    <div class="block-col width-md-1-4 center p-b-20">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/question-baloon.svg" alt="<?php _e('Icon illustrating sales chat');?>" width="180">
                    </div> 
                    <div class="block-col width-md-3-4 p-h-30 center-sm">
                        <p class="title-paragraph-lite">Have questions about KVM VPS services? Looking for a custom solution? Contact us to discuss your needs.</p>
                        <a href="" id="get-form" class="button primary large m-v-30"><?php _e('Get in Touch');?></a>
                        <div class="contact-form p-v-30" style="display:none;">
                        
                            <?php echo do_shortcode('[contact-form-7 id="'.$contact_form_id.'" title="Contact Sales" html_class="wpcf7-form captcha"]');?>

                        </div> 
                    </div>
                </div> 

            </div>

        </section> <!-- end of .contact-sales -->        



<script>

    (function($){
        $(document).ready(function(){

            function goToByScroll(id) {
                /*Used for scrolling to error spot*/
                
                id = id.replace("link", ""); // Remove "link" from the ID
                var deltaScroll = 0,
                    heightStickyHeader = $('.sticky-wrapper.is-sticky').height();

                if (heightStickyHeader > 0) {
                    deltaScroll = heightStickyHeader;
                }
                $('html,body').animate({scrollTop: $("#" + id).offset().top - deltaScroll},'slow');
            };


            $('#get-form').click(function(e){
                e.preventDefault();
                $(this).hide();
                $(this).parent().find('.contact-form').show('slow');
            });

            // check for form postprocess hash '..wcpf7..' and show form if some errors occured
            var linkHash = location.hash;

            if (linkHash.indexOf('wpcf7') > 0) {
                $('#get-form').hide();
                $('.contact-form').show();
                goToByScroll('contact-form');
            }

        });

        /*jQuery Segment.js plugin*/
        $.fn.extend({
            Segment: function ( ) {
                $(this).each(function (){
                    var self = $(this);
                    var onchange = self.attr('onchange');
                    var wrapper = $("<div>",{class: "ui-segment"});
                    $(this).find("option").each(function (){
                        var option = $("<span>",{class: 'option',onclick:onchange,text: $(this).text(),value: $(this).val()});
                        if ($(this).is(":selected")){
                            option.addClass("active");
                        }
                        wrapper.append(option);
                    });
                    wrapper.find("span.option").click(function (){
                        wrapper.find("span.option").removeClass("active");
                        $(this).addClass("active");
                        self.val($(this).attr('value'));
                    });
                    $(this).after(wrapper);
                    $(this).hide();

                    /*Icon enhancements*/
                    $(this).find("option").each(function (){
                        var icosrc = $(this).attr('data-icosrc');
                        var optionval = $(this).val();
                        var $wrapperoption;
                        if (typeof(icosrc) !== 'undefined') {
                            $wrapperoption = wrapper.find("span.option[value='"+ optionval + "']")[0];
                            var icon = document.createElement("img");
                            $(icon).attr("src", icosrc);
                            //$wrapperoption.prepend(icon);     // no prepend() support for EDGE
                            $wrapperoption.insertBefore(icon, $wrapperoption.firstChild);
                        }

                    });

                });
            }
        });

        // before initializing Segment switch check if URL provides hash for plans selection
        if(window.location.hash) {
            var hash_val = window.location.hash.substring(1),
                $selector_options = $('select.segment-select');

            $selector_options.children().each(function(){
                var a = $(this).val();
                if ($(this).val() === hash_val) {
                    $(this).attr('selected','selected');
                }
            })
        }


        $(document).ready(function(){

            // initialize Segment switch
            $(".segment-select").Segment();

            var currentBlockActive = $('.ui-segment .option.active').attr('value');
            $('.block-switchable').filter('#block-' + currentBlockActive).addClass('active');

            $('.ui-segment .option').click(function(e){
                var newVal = $(this).attr('value');
                $('.block-switchable').removeClass('active');
                $('.block-switchable').filter('#block-' + newVal).addClass('active');
            })

        });

    })(jQuery)

</script>  
        
      
        <section class="service-faq extra-pad-top" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/land-kvm-vps.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->



    </section> <!-- end of .main -->

</article>


<?php

include_block("htmlblocks/hosting/speedtest_enabling_js.php");

universal_redirect_footer([
    'en' => $site_en_url.'/cheap-vps-windows/',
    'br' => $site_br_url.'/vps-windows-barato/'
]);

get_footer();

?>
