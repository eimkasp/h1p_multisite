<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Landing: Cheap VPS Hosting
*/

wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings2/landing.css', ['style']);

wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);
wp_enqueue_script('tablesaw', get_template_directory_uri() . '/js/vendor/tablesaw.js', ['jquery'], '3.0.0-beta.4', true);
wp_enqueue_script('tablesaw-init', get_template_directory_uri() . '/js/vendor/tablesaw-init.js', ['tablesaw'], '3.0.0-beta.4', true);


global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'vps');


/*Pricing comparison table data*/
$service_pricing = array(
    'en' => array (
        'host1plus' => '$10.00',
        'mediatemple' => '$30.00',
        'vpsnet' => '$24.00',
        'ramnode' => '$14.00' 
    ),
    'br' => array (
        'host1plus' => '$10.00',
        'mediatemple' => '$30.00',
        'vpsnet' => '$24.00',
        'ramnode' => '$14.00'
    )
);

if(get_locale() == 'en_US'){
    $lang = 'en';
} elseif (get_locale() == 'pt_BR'){
    $lang = 'br';
}

// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing vps-hosting">

    <header class="headline landing-cheapvps">
        <div class="container adjust-vertical-center">

            <img class="m-t-40" src="<?php echo $images ?>landings2/images/header_icon_vps.svg" alt="VPS Hosting" width="105" height="105">
            <h1 class="page-title m-up-40"><?php _e('Cheap VPS Servers from')?> <?php echo $mypage->getMinPrice(); ?></h1>
            <div class="title-descr"><?php _e('Low cost VPS server solutions on the market.');?></div>

            <div class="row center p-t-30">
                <a class="button highlight large" href="#plans" data-scrollto="plans"><?php _e('View Cheap VPS Plans');?></a>
            </div>

        </div>
    </header> <!-- end of .headline.cs-hosting -->

    

    <section class="main page-content">



        <section class="service-features extra-pad">

            <div class="container">

                <div class="section-header p-b-20">
                    <h2 class="block-title"><?php _e('Get your VPS on a budget!')?></h2>
                </div>

                <div class="container-flex as-row">

                    <div class="block-col width-lg-1-1 p-h-30 p-v-20 m-h-100-md">
                        <p class="center p-b-40"><?php _e('Are you dissapointed in unreliable free VPS hosting, but you still need cheap VPS server? We are ready to equip you with the best value VPS services for the lowest price.');?></p>
                    </div>

                </div>

                <div class="block-of-fourcross p-b-40">

                    <div class="layout-row two-col-row">
                        <div class="block-col center p-v-40">
                            <div class="icon-part p-v-20 center">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/style-orng1-money-back-guarantee.svg" alt="<?php _e('14-Day Money-Back Guarantee icon')?>">
                            </div>
                            <div class="text-part">
                                <div class="text-block">
                                    <h3 class="block-title-small"><?php _e('14-Day Money-Back Guarantee');?></h3>
                                    <p><?php _e('No commitments! Try our unmanaged VPS services for 14 days for free to make sure we meet your needs.')?></p>
                                </div>
                            </div>
                        </div>
                        <div class="block-col center p-v-40">
                            <div class="icon-part p-v-20 center">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/style-orng1-billingcycle-discounts.svg" alt="<?php _e('Billing Cycle Discount icon')?>">
                            </div>
                            <div class="text-part">
                                <div class="text-block">
                                    <h3 class="block-title-small"><?php _e('Billing Cycle Discounts');?></h3>
                                    <p><?php _e('Save on your VPS server by choosing longer billing cycles and get up to 20% off your order. The longer the billing cycle, the more you save!')?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layout-row two-col-row">
                        <div class="block-col center p-v-40">
                            <div class="icon-part p-v-20 center">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/style-orng1-flex-payment-meth.svg" alt="<?php _e('Flexible Payment Methods icon')?>">
                            </div>
                            <div class="text-part">
                                <div class="text-block">
                                    <h3 class="block-title-small"><?php _e('Flexible Payment Methods');?></h3>
                                    <p><?php printf(__('We offer a wide range of %spayment options%s - Credit Card, Paypal, authorized PayPal payments, Alipay, Bitcoin, Ebanx and Paysera.'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1312/0/what-payment-method-can-i-use&_ga=1.156236472.2072236792.1476963328">', '</a>')?></p>
                                </div>
                            </div>
                        </div>
                        <div class="block-col center p-v-40">
                            <div class="icon-part p-v-20 center">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/style-orng1-bulk-orders.svg" alt="<?php _e('Bulk Order icon')?>">
                            </div>
                            <div class="text-part">
                                <div class="text-block">
                                    <h3 class="block-title-small"><?php _e('Bulk Orders');?></h3>
                                    <p><?php _e('Multiple virtual machines with the same configuration will be ready for you in just a few clicks at the checkout.')?></p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> <!-- end of .block-of-fourcross -->

                

            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->


        <section class="service-comparison extra-pad color-bg-style5">
            
            <div class="container">

                <div class="section-header p-b-20">
                    <h2 class="block-title"><?php _e('VPS Provider Comparison')?></h2>
                </div>

                <div class="container-flex as-row">

                    <div class="block-col width-lg-1-1 p-h-30 p-t-20 m-h-100-md">
                        <p class="center p-b-40"><?php _e('We are one of the cheapest VPS server providers on the market. If you don\'t trust our words, let the numbers do the talking!');?></p>
                    </div>

                    <table class="table tablesaw tablesaw-swipe landing-cheapvps landing-table-color-1 landing-table-text-1 table-col-5" data-tablesaw-mode="swipe" data-tablesaw-minimap>
                    <thead>
                        <tr>
                            <th class="" scope="col">
                                <?php _e('Data taken on');?><br> 2016/11/22
                            </th>
                            <th class="selected p-0 m-0" data-tablesaw-priority="persist" scope="col">
                                <div class="provider host1plus" title="Host1Plus"></div> 
                            </th>
                            <th class="" scope="col" data-tablesaw-priority="1">
                                <div class="provider mediatemple" title="mediatemple"></div>
                            </th>
                            <th class="" scope="col" data-tablesaw-priority="1">
                                <div class="provider vpsnet" title="VPS.NET"></div>
                            </th>
                            <th class="" scope="col" data-tablesaw-priority="1">
                                <div class="provider ramnode" title="RamNode"></div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th class="title size-18"><?php _e('Price');?></td>
                            <td class="selected"><?php echo $service_pricing[$lang]['host1plus'] ?></td>
                            <td class=""><?php echo $service_pricing[$lang]['mediatemple'] ?></td>
                            <td class=""><?php echo $service_pricing[$lang]['vpsnet'] ?></td>
                            <td class=""><?php echo $service_pricing[$lang]['ramnode'] ?></td>
                        </tr>
                        <tr>
                            <th class="title size-18"><?php _e('RAM');?></td>
                            <td class="selected">2048 MB</td>
                            <td class="">2048 MB</td>
                            <td class="">2048 MB</td>
                            <td class="">2048 MB</td>
                        </tr>
                        <tr>
                            <th class="title size-18"><?php _e('Disk Space');?></td>
                            <td class="selected">80 GB</td>
                            <td class="">20 GB</td>
                            <td class="">50 GB</td>
                            <td class="">80 GB</td>
                        </tr>
                        <tr>
                            <th class="title size-18"><?php _e('Bandwidth');?></td>
                            <td class="selected">2000 GB</td>
                            <td class="">2000 GB</td>
                            <td class="">4000 GB</td>
                            <td class="">3000 GB</td>
                        </tr>
                        <tr>
                            <th class="title size-18"><?php _e('Available OS');?></td>
                            <td class="selected">CentOS, Ubuntu, Debian, Suse, Fedora</td>
                            <td class="">CentOS, Ubuntu, Debian, Suse</td>
                            <td class="">CentOS. Ubuntu</td>
                            <td class="">CentOS, Ubuntu, Debian, Suse, Scientific, Fedora</td>
                        </tr>
                        <tr>
                            <th class="title size-18"><?php _e('Locations');?></td>
                            <td class="selected"><?php echo __('USA').', '.__('Germany').', '.__('Brazil').', '.__('South Africa'); ?></td>
                            <td class=""><?php echo __('USA'); ?></td>
                            <td class=""><?php echo __('USA').', '.__('Netherlands').', '.__('UK').', '.__('Canada'); ?></td>
                            <td class=""><?php echo __('USA').', '.__('Netherlands'); ?></td>
                        </tr>
                    </tbody>

                </table>

                <p class="t-14 p-t-30">* <?php _e('Third-party logos and marks are registered trademarks of their respective owners. All rights reserved.'); ?></p>

                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-comparison -->


        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>


        

        <section class="service-features extra-pad" data-section-title="<?php _e('Features');?>">

            <div id="features" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Cheap Virtual Private Server Features')?></h2>
                </div>

                <div class="container-flex features-list">

                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-fullroot.svg" alt="<?php _e('Full root access icon'); ?>" width="130" height="130">
                        <h3 class="header-small-18"><?php _e('Full Root Access')?></h3>
                        <p><?php _e('Get full flexibility and control on various Linux OS instances - CentOS, Debian, Fedora, Suse, Ubuntu.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-dns.svg" alt="<?php _e('DNS & rDNS feature icon'); ?>" width="130" height="130">
                        <h3 class="header-small-18"><?php _e('DNS & rDNS Management')?></h3>
                        <p><?php _e('Simplified DNS and rDNS self-management means less time spent getting support and more time working on your own projects.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-scale.svg"  alt="<?php _e('Scalability feature icon'); ?>" width="130" height="130">
                        <h3 class="header-small-18"><?php _e('Scalable resources')?></h3>
                        <p><?php _e('Grab more resources to support your growing needs! Customize your VPS at the Client Area and stay on a budget!')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-instant.svg" alt="<?php _e('Instant deployment feature icon'); ?>" width="130" height="130">
                        <h3 class="header-small-18"><?php _e('Instant Deployment')?></h3>
                        <p><?php _e('Get your cheap VPS in 3 simple steps! Configure your server, fill in billing information and start using your VPS without delays.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-livestats.svg" alt="<?php _e('Live Stats feature icon'); ?>" width="130" height="130">
                        <h3 class="header-small-18"><?php _e('Live Stats')?></h3>
                        <p><?php _e('Track your resource usage live! Enable alerts to avoid CPU, memory or network overuse.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-geolocatedips.svg" alt="<?php _e('Geolocated IPs feature icon'); ?>" width="130" height="130">
                        <h3 class="header-small-18"><?php _e('Geolocated IPs')?></h3>
                        <p><?php _e('Benefit from geolocated IPs to reduce latency, boost your SEO capability and localize your online business.')?></p>
                    </div>
                    
                </div>


            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->        
        

        <section class="service-locations-list extra-pad color-bg-style5" data-section-title="<?php _e('Locations');?>">

            <div id="locations" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header p-b-40">
                    <h2 class="block-title"><?php _e('Choose your VPS server location!')?></h2>
                </div>

                <?php 

                    /*Calculate cheapest USA location pricing*/
                    $price_la = $mypage->getMinPrice('los_angeles');
                    $price_ch = $mypage->getMinPrice('sao_paulo');
                    $price_usa_final = min(array($price_la, $price_ch));

                ?>

                <div class="container-flex block-offer2-container four">
                    <div class="block-col block-offer2">
                        <a class="wrapper-content" href="/germany-vps/">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_Germany.svg" alt="Germany VPS icon" width="90">
                            <div class="text-wrap">
                                    <h3 class="title-paragraph-lower bold"><?php _e('Frankfurt'); ?>, DE</h3>
                                    <span class="t-mshrinked">from <?php echo $mypage->getMinPrice('frankfurt') ?>/mo</span>
                            </div>
                        </a>
                    </div>
                    <div class="block-col block-offer2">
                        <a class="wrapper-content" href="/vps-brazil/">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_Brazil.svg" alt="Brazil VPS icon" width="90">
                            <div class="text-wrap">
                                <h3 class="title-paragraph-lower bold"><?php _e('São Paulo'); ?>, BR</h3>
                                <span class="t-mshrinked">from <?php echo $mypage->getMinPrice('sao_paulo') ?>/mo</span>
                            </div>
                        </a>
                    </div>
                    <div class="block-col block-offer2">
                        <a class="wrapper-content" href="/vps-usa/">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_USA.svg" alt="USA VPS icon" width="90">
                            <div class="text-wrap">
                                <h3 class="title-paragraph-lower bold"><?php _e('Los Angeles'); ?> <br/> <?php _e('Chicago'); ?>, US</h3>
                                <span class="t-mshrinked">from <?php echo $price_usa_final ?>/mo</span>
                            </div>
                        </a>
                    </div>
                    <div class="block-col block-offer2">
                        <a class="wrapper-content" href="/vps-hosting/">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_South_Africa.svg" alt="South Africa VPS icon" width="90">
                            <div class="text-wrap">
                                <h3 class="title-paragraph-lower bold"><?php _e('Johannesburg'); ?>, ZA</h3>
                                <span class="t-mshrinked">from <?php echo $mypage->getMinPrice('johannesburg') ?>/mo</span>
                            </div>
                        </a>
                    </div>
                </div> 


            </div> <!-- end of .container -->

        </section> <!-- end of .service-locations-list -->



        <section class="service-pricing extra-pad-top" data-section-title="<?php _e('Plans');?>">

            <div id="plans" class="scroll-id-target"></div>


            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Cheap VPS Server Hosting Plans')?></h2>
                </div>

                <p class="p-b-30 center"><?php _e('We offer unmanaged VPS servers on a budget. Take a look at our well-tailored plans!'); ?></p>

                <div>

                    <?php include_block("htmlblocks/pricing-tables/vps-hosting-v2.php"); ?>

                </div>
            
                <a data-link-target="<?php echo $mypage->getMinPriceCheckoutLink();?>" data-section-button="<?php _e('Order Cheap VPS Server Now');?>" data-button-styling="button highlight <?php echo $mypage->checkoutActivityClass($mypage->getCheapestLocation()); ?>" class="hidden"></a>

                <?php include_block("htmlblocks/pricing-tables/vps-all-plans-include.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-pricing -->

        
        
      
        <section class="service-faq extra-pad" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/land-cheap-vps.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->

        <section class="service-cta extra-pad color-bg-style-grad2a">

            <div class="container">
                
                <div class="container-flex cta">
                    <div class="block-col width-md-2-3 cta-left">
                        <span class="block-title-xlarge-thin white t-ff-header t-mspaced"><?php _e('Looking for affordable Windows VPS?');?></span>
                    </div>
                    <div class="block-col width-md-1-3 cta-right">
                        <a href="/cheap-vps-windows/" class="button xlarge ghost"><?php _e('Windows KVM VPS');?></a>
                    </div> 
                </div> 

            </div>

        </section> <!--end of .extra-offer-cta-->



    </section> <!-- end of .main -->

</article>


<?php

universal_redirect_footer([
    'en' => $site_en_url.'/cheap-vps-hosting/',
    'br' => $site_br_url.'/hospedagem-vps-barata/'
]);

get_footer();

?>
