<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Landing: Linux Cloud Server
*/

$custom_fields = get_post_custom();
$contact_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings2/landing.css', ['style']);

if(get_locale() == 'pt_BR') {
    $lang_code = 'pt-BR';
} else {
    $lang_code = 'en';
}

wp_enqueue_script('recaptcha', 'https://www.google.com/recaptcha/api.js?&hl=' . $lang_code, [], false, true);
wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);


global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'cs_lin');

/*Get Cloud IP config option pricing
-------------------------*/
// id=342

$cloud_hosting_config_options = $whmcs->getConfigurablePrices(342);
$cloud_hosting_ip = $cloud_hosting_config_options['IP']['options']['IP']['pricing']['monthly'];

// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing cloud-servers">

    <header class="headline landing-lincs">
        <div class="container adjust-vertical-center">

            <img class="m-t-40" src="<?php echo $images ?>landings2/images/header_icon_lincs.svg" alt="VPS Hosting" width="105" height="105">
            <h1 class="page-title m-up-40"><?php _e('Linux Cloud Servers powered by KVM')?></h1>
            <div class="title-descr"><?php _e('Deploy your development projects on a pre-installed Linux Cloud Server running the most popular Linux OS.');?></div>

            <div class="row center p-t-30">
                <a class="button highlight large" href="#plans" data-scrollto="plans"><?php _e('View Linux Cloud Servers Plans');?></a>
                <p class="white p-t-10"><?php printf(__('From %s only!'), $mypage->getMinPrice()); ?></p> 
            </div>

        </div>
    </header> <!-- end of .headline.cs-hosting -->

    

    <section class="main page-content">



        <section class="service-description extra-pad">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Pre-Installed Cloud Linux Servers')?></h2>
                    <span class="title-follow-text"><?php _e('Find the perfect match with a selection of the most popular Cloud Linux OS.'); ?></span>
                </div>

                <div class="block-of-fourcross p-b-40">

                    <div class="layout-row two-col-row">
                        <div class="block-col center p-v-40">
                            <div class="icon-part p-v-20 center">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/hexa-feat-blue-centos.svg" alt="<?php _e('Linux CentOS distribution icon')?>" height="170">
                            </div>
                            <div class="text-part">
                                <div class="text-block">
                                    <h3 class="block-title-small"><?php _e('CentOS');?></h3>
                                    <p><?php _e('CentOS Linux distribution is a stable, predictable, manageable and reproducible platform derived from the sources of Red Hat Enterprise Linux (RHEL).')?></p>
                                </div>
                            </div>
                        </div>
                        <div class="block-col center p-v-40">
                            <div class="icon-part p-v-20 center">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/hexa-feat-blue-debian.svg" alt="<?php _e('Linux Debian distribution icon')?>" height="170">
                            </div>
                            <div class="text-part">
                                <div class="text-block">
                                    <h3 class="block-title-small"><?php _e('Debian');?></h3>
                                    <p><?php _e('Users rave about easy management, security and stability of Debian. This distro comes loaded with more than 43000 packages!')?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layout-row two-col-row">
                        <div class="block-col center p-v-40">
                            <div class="icon-part p-v-20 center">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/hexa-feat-blue-fedora.svg" alt="<?php _e('Linux Fedora distribution icon')?>" height="170">
                            </div>
                            <div class="text-part">
                                <div class="text-block">
                                    <h3 class="block-title-small"><?php _e('Fedora');?></h3>
                                    <p><?php _e('Fedora is built and used by people across the globe, free for anyone to use, modify and distribute.'); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="block-col center p-v-40">
                            <div class="icon-part p-v-20 center">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/hexa-feat-blue-ubuntu.svg" alt="<?php _e('Linux Ubuntu distribution icon')?>" height="170">
                            </div>
                            <div class="text-part">
                                <div class="text-block">
                                    <h3 class="block-title-small"><?php _e('Ubuntu');?></h3>
                                    <p><?php _e('Ubuntu is an open source software platform that runs everywhere from IoT devices - smartphones, tablets and PC - to your server and Cloud.')?></p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> <!-- end of .block-of-fourcross -->

                <div class="row center p-t-20 m-r-10">
                    <a href="#plans" data-scrollto="plans" class="button primary highlight large"><?php _e('Compare Linux Cloud Plans');?></a>
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-description -->


        <section class="custom-installation-guide extra-pad-top color-bg-style5">
            
            <div class="container">

                <div class="container-flex p-t-20 p-b-30">
                    <div class="block-col width-md-1-3 center p-b-20">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/landing_custom_os_iso.png" alt="<?php _e('Empty Template at Client Area icon');?>" width="272">
                    </div> 
                    <div class="block-col width-md-2-3 p-h-40 center-sm push-flex vertical-center">
                        <h3 class="block-title-small"><?php _e('Cannot find the Linux OS you are looking for?'); ?></h3>
                        <p><?php _e('We\'ve got you covered! Opt for an empty virtual machine and install the OS you love at your Client Area.'); ?></p>
                        <p class="m-v-30 descr-text-feature size-18 bold">
                            <?php printf(__('Your guide to %sLinux OS installation%s'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1353/114/how-do-i-install-linux-os-using-custom-iso">','</a>');?>
                        </p>  
                    </div>
                </div> 

            </div> <!-- end of .container -->


        </section> <!-- end of .custom-installation-guide -->


        <section class="linux-cs-hosting-specials extra-pad">

            <div class="container">

                <div class="section-header p-b-60">
                    <h2 class="block-title"><?php _e('Cloud Linux Hosting Done Right')?></h2>
                </div>

                <div class="layout-row three-col-row">

                    <div class="block-col center">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-cs-quickdeploy.svg" alt="Quick Deployment" height="140">
                        <h3 class="block-title-small m-v-15" style="font-size: 1.3em"><?php _e('Solid Isolation')?></h3>
                        <p class="details-description"><?php _e('The most desired Linux Cloud Server powered by KVM hypervisor guarantees high stability, immaculate performance and solid security.'); ?></p>
                    </div>
                    <div class="block-col center">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-cs-bulkorders.svg" alt="Bulk Orders" height="140">
                        <h3 class="block-title-small m-v-15" style="font-size: 1.3em"><?php _e('Bulk Orders')?></h3>
                        <p class="details-description"><?php _e('Save your time! Deploy up to 50 pre-installed Linux Cloud Servers with a single order - one location, same configuration and less hassle!'); ?></p>
                    </div>
                    <div class="block-col center">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-cs-scalability1.svg" alt="Unlimited Scalability" height="140">
                        <h3 class="block-title-small m-v-15"><?php _e('Scalability')?></h3>
                        <p class="details-description"><?php _e('Scale your Linux Cloud on demand! Downgrade or upgrade your resources just in a few clicks.'); ?></p>
                    </div>

                </div>

                <div class="row center p-t-20">
                    <a href="<?php echo $config['whmcs_links']['checkout_cloud'].'?force_clean=1&plan=lin4&pid=345';?> ?>" class="button primary highlight large"><?php _e('Deploy my Linux Server');?></a>
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .linux-cs-hosting-specials -->  


        
        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>



        <section class="service-features extra-pad color-bg-style-grad1" data-section-title="<?php _e('Features');?>">

            <div id="features" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title white"><?php _e('Cloud Server Features'); ?></h2>
                </div>

                <div class="four-col-row-biface-cards">

                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_flex-scalability_blue.svg" alt="<?php _e('Flexible Windows Cloud Server feature icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Flexible Scaling')?></h3>
                                <p class="white"><?php _e('No longer bound by the physical size of your server, quickly scale your Cloud based virtual machine at your Client Area.'); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_api_blue.svg" alt="<?php _e('Windows Cloud Server powerful API icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Powerful API')?></h3>
                                <p class="white"><?php _e('Directly access web control panel functionality and benefit from advanced Cloud networking features.'); ?></p>
                            </div> 
                        </div>
                    </div>

                </div>
                <div class="four-col-row-biface-cards p-b-40">

                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_auto-backups_blue.svg" alt="<?php _e('Scheduled backups for Windows Cloud Servers feature icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Scheduled Backups')?></h3>
                                <p class="white"><?php _e('Schedule daily, weekly or monthly automated backups. You can also create backups manually whenever you have to!'); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_custom-os_blue.svg" alt="<?php _e('Windows Cloud Server custom ISO feature icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Custom ISO')?></h3>
                                <p class="white"><?php _e('Custom ISO allows you to mount a configured image on your Cloud instance and run through the boot & setup processes as you would on a bare metal server.'); ?></p>
                            </div> 
                        </div>
                    </div>

                </div> 

            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->

        

        <section class="service-locations extra-pad-top color-bg-style2 no-zoom" data-section-title="<?php _e('Locations');?>">

            <div id="locations" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Worldwide Server Locations')?></h2>
                        <p><?php _e('Multiple premium data centers ensure high connectivity and better reach.'); ?></p>
                </div>

                <?php include_block("htmlblocks/locations-services.php", array('type' => 'cloud', 'speedtest' => true )); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-locations -->



        <section class="service-pricing extra-pad-top" data-section-title="<?php _e('Plans');?>">

            <div id="plans" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header p-b-0">
                    <h2 class="block-title"><?php _e('Linux Server Pricing')?></h2>
                </div>

                <?php include_block("htmlblocks/pricing-tables/cs-hosting-lin-v2.php"); ?>

                <a data-link-target="<?php echo $mypage->getMinPriceCheckoutLink(); ?>" data-section-button="<?php _e('Order Linux Server Now');?>" data-button-styling="button highlight <?php echo $mypage->checkoutActivityClass($mypage->getCheapestLocation()); ?>" class="hidden"></a>

                <?php include_block("htmlblocks/pricing-tables/cs-all-plans-include.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-pricing -->


        <section class="contact-sales extra-pad-top color-bg-style5">

            <div id="contact-form" class="scroll-id-target"></div>

            <div class="container">

                <div class="container-flex">
                    <div class="block-col width-md-1-4 center p-b-20">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/question-baloon.svg" alt="<?php _e('Icon illustrating sales chat');?>" width="180">
                    </div> 
                    <div class="block-col width-md-3-4 p-h-30 center-sm">
                        <p class="title-paragraph-lite"><?php _e('Have questions about Cloud technology? Looking for a custom solution? Contact us to discuss your needs.'); ?></p>
                        <a href="" id="get-form" class="button primary large m-v-30"><?php _e('Get in Touch');?></a>
                        <div class="contact-form p-v-30" style="display:none;">
                        
                            <?php echo do_shortcode('[contact-form-7 id="'.$contact_form_id.'" title="Contact Sales" html_class="wpcf7-form captcha"]');?>

                        </div> 
                    </div>
                </div> 

            </div>

        </section> <!-- end of .contact-sales -->

<script>

    (function($){
        $(document).ready(function(){

            function goToByScroll(id) {
                /*Used for scrolling to error spot*/
                
                id = id.replace("link", ""); // Remove "link" from the ID
                var deltaScroll = 0,
                    heightStickyHeader = $('.sticky-wrapper.is-sticky').height();

                if (heightStickyHeader > 0) {
                    deltaScroll = heightStickyHeader;
                }
                $('html,body').animate({scrollTop: $("#" + id).offset().top - deltaScroll},'slow');
            };


            $('#get-form').click(function(e){
                e.preventDefault();
                $(this).hide();
                $(this).parent().find('.contact-form').show('slow');
            });

            // check for form postprocess hash '..wcpf7..' and show form if some errors occured
            var linkHash = location.hash;

            if (linkHash.indexOf('wpcf7') > 0) {
                $('#get-form').hide();
                $('.contact-form').show();
                goToByScroll('contact-form');
            }

        })
    })(jQuery)

</script>            
        
      
        <section class="service-faq extra-pad" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/land-cs-lin.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->


        <section class="service-cta extra-pad color-bg-style-grad1a">

            <div class="container">
                
                <div class="container-flex cta">
                    <div class="block-col width-md-2-3 cta-left">
                        <span class="block-title-xlarge-thin white t-ff-header t-mspaced"><?php _e('Looking for something else?');?></span>
                    </div>
                    <div class="block-col width-md-1-3 cta-right">
                        <a href="/cheap-vps-hosting/" class="button xlarge ghost"><?php _e('Cheap VPS');?></a>
                    </div> 
                </div> 

            </div>

        </section> <!--end of .extra-offer-cta-->



    </section> <!-- end of .main -->

</article>


<?php

include_block("htmlblocks/hosting/speedtest_enabling_js.php");

universal_redirect_footer([
    'en' => $site_en_url.'/cloud-server-linux/',
    'br' => $site_br_url.'/servidor-cloud-linux/'
]);

get_footer();

?>
