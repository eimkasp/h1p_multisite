<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Landing: Germany CS KVM
*/

$custom_fields = get_post_custom();
$contact_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings2/landing.css', ['style']);

wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);


global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'cs_lin');

/*Get Cloud IP config option pricing
-------------------------*/
// id=342

$cloud_hosting_config_options = $whmcs->getConfigurablePrices(342);
$cloud_hosting_ip = $cloud_hosting_config_options['IP']['options']['IP']['pricing']['monthly'];

// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing cloud-servers landing-germanycs">

    <header class="headline landing-germanycs">
        <div class="container adjust-vertical-center">

            <img class="m-t-40" src="<?php echo $images ?>landings2/images/header_icon_locationcs.svg" alt="Germany Cloud Servers header image" width="105" height="105">
            <h1 class="page-title m-up-40"><?php _e('Germany Cloud Servers powered by KVM'); ?></h1>
            <div class="title-descr"><?php _e('Deploy your development projects on a pre-installed Germany Cloud Server running Windows Server or one of the most popular Linux OS.');?></div>

            <div class="row center p-t-30">
                <a class="button highlight large" href="#plans" data-scrollto="plans"><?php _e('View Plans');?></a>
                <p class="white p-t-10"><?php _e('From'); echo ' ' . $mypage->getMinPrice() . ' '; _e('only!'); ?></p> 
            </div>

        </div>


    </header> <!-- end of .headline.cs-hosting -->


    <section class="main page-content">


        <section class="service-description extra-pad">

            <div class="container">


                <div class="section-header">
                    <h2 class="block-title"><?php _e('Pre-Installed Germany Cloud Servers'); ?></h2>
                    <span class="title-follow-text center">
                        <?php _e('Find the perfect match with a selection of the most popular Windows and Linux OS.');?>
                    </span>
                </div>

                <div class="container-flex p-t-30 p-b-60">
                    <div class="block-col has-special-background-positioning1"></div>
                </div>

                <div class="container-flex justify-center biface-cards">

                    <div class="block-col width-md-1-3 spaced land-color-bg-1 m-10">
                        <div class="main-face push-flex horizontal-center wrap p-20">
                            <div class="feature-image-wrapper">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/round-feat-white-win.svg" alt="<?php _e('Windows Cloud Server icon');?>" width="140">
                            </div>
                            <h3 class="feature-title block-title-small white max-w-150"><?php _e('Windows Server');?></h3>
                        </div>
                        <div class="alternate-face p-20">
                            <p class="white"><?php _e('Build modern business applications and bring the innovation behind the world\'s largest datacenters to your private workspace.'); ?></p>
                        </div> 
                    </div>

                    <div class="block-col width-md-1-3 spaced land-color-bg-2 m-10">
                        <div class="main-face push-flex horizontal-center wrap p-20">
                            <div class="feature-image-wrapper">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/round-feat-white-centos.svg" alt="<?php _e('Linux CentOS distribution icon');?>" width="140">
                            </div>
                            <h3 class="feature-title block-title-small white"><?php _e('Centos');?></h3>
                        </div>
                        <div class="alternate-face p-20">
                            <p class="white"><?php _e('CentOS Linux distribution is a stable, predictable, manageable and reproducible platform derived from the sources of Red Hat Enterprise Linux (RHEL).'); ?></p>
                        </div>
                    </div>

                    <div class="block-col width-md-1-3 spaced land-color-bg-3 m-10">
                        <div class="main-face push-flex horizontal-center wrap p-20">
                            <div class="feature-image-wrapper">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/round-feat-white-debian.svg" alt="<?php _e('Linux Debian distribution icon');?>" width="140">
                            </div>
                            <h3 class="feature-title block-title-small white"><?php _e('Debian');?></h3>
                        </div>
                        <div class="alternate-face p-20">
                            <p class="white"><?php _e('Users rave about easy management, security and stability of Debian. This distro comes loaded with more than 43000 packages!'); ?></p>
                        </div>
                    </div>
                    
                    <div class="block-col width-md-1-3 spaced land-color-bg-4 m-10">
                        <div class="main-face push-flex horizontal-center wrap p-20">
                            <div class="feature-image-wrapper">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/round-feat-white-fedora.svg" alt="<?php _e('Linux Fedora distribution icon');?>" width="140">
                            </div>
                            <h3 class="feature-title block-title-small white"><?php _e('Fedora');?></h3>
                        </div>
                        <div class="alternate-face p-20">
                            <p class="white"><?php _e('Fedora is built and used by people across the globe, free for anyone to use, modify and distribute.'); ?></p>
                        </div>
                    </div>

                    <div class="block-col width-md-1-3 spaced land-color-bg-5 m-10">
                        <div class="main-face push-flex horizontal-center wrap p-20">
                            <div class="feature-image-wrapper">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/round-feat-white-ubuntu.svg" alt="<?php _e('Linux Ubuntu distribution icon');?>" width="140">
                            </div>
                            <h3 class="feature-title block-title-small white"><?php _e('Ubuntu');?></h3>
                        </div>
                        <div class="alternate-face p-20">
                            <p class="white"><?php _e('Ubuntu is an open source software platform that runs everywhere from IoT devices - smartphones, tablets and PC - to your server and Cloud.'); ?></p>
                        </div>
                    </div>

                </div> <!-- end of .container-flex.biface-cards -->


            </div> <!-- end of .container -->

        </section> <!-- end of .service-description -->

<script>

    (function($) {

        /*biface-cards*/
        /*Interactive blocks handler*/

        $(document).ready(function(){
            function isElemActive($elem) {
                return ($elem.hasClass('active'));
            }
            function toggleBlock($elem) {
                var active = isElemActive($elem);
                if (!active) {
                    $elem.addClass('active');
                    $elem.find('.main-face').animate({
                        opacity: 0,
                        top: '-300px',
                    }, 200, function(){
                    });
                    $elem.find('.alternate-face').animate({
                        opacity: 1,
                        top: '0',
                    }, 300, function(){
                    });

                } else {
                    $elem.removeClass('active');
                    $elem.find('.main-face').animate({
                        opacity: 1,
                        top: '0',
                    }, 200, function(){
                    });
                    $elem.find('.alternate-face').animate({
                        opacity: 0,
                        top: '300px',
                        display: 'toggle'
                    }, 300, function(){
                    });
                }
            }

            $('.service-description .biface-cards .block-col').click(function(){
                toggleBlock($(this));
            });
        });

    })(jQuery);

</script>

        <section class="custom-installation-guide extra-pad-top color-bg-style5">
            
            <div class="container">

                <div class="container-flex p-t-20 p-b-30">
                    <div class="block-col width-md-1-3 center p-b-20">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/landing_custom_os_iso.png" alt="<?php _e('Empty Template at Client Area icon');?>" width="272">
                    </div> 
                    <div class="block-col width-md-2-3 p-h-40 center-sm push-flex vertical-center">
                        <h3 class="block-title-small"><?php _e('Cannot find the OS you are looking for?'); ?></h3>
                        <p><?php _e('We\'ve got you covered! Opt for an empty virtual machine and install the OS you love at your Client Area.'); ?></p>
                        <p class="m-v-30 descr-text-feature size-18 bold">
                            <?php printf(__('Your guide to %sCustom ISO installation%s'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1353/114/how-do-i-install-linux-os-using-custom-iso">','&nbsp;<i class="fa fa-angle-right" style="display:inline"></i> </a>');?>
                        </p>  
                    </div>
                </div> 

            </div> <!-- end of .container -->


        </section> <!-- end of .custom-installation-guide -->


        <section class="linux-cs-hosting-specials extra-pad">

            <div class="container">

                <div class="section-header p-b-60">
                    <h2 class="block-title"><?php _e('Germany Cloud Hosting Done Right')?></h2>
                </div>

                <div class="layout-row three-col-row">

                    <div class="block-col center">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-cs-quickdeploy.svg" alt="Germany Cloud Server solid isolation feature icon" height="140">
                        <h3 class="block-title-small m-v-15" style="font-size: 1.3em"><?php _e('Solid Isolation')?></h3>
                        <p class="details-description"><?php _e('The most desired Windows & Linux Cloud Servers powered by KVM hypervisor guarantees high stability, immaculate performance and solid security.'); ?></p>
                    </div>
                    <div class="block-col center">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-cs-bulkorders.svg" alt="Germany Cloud Server bulk orders feature icon" height="140">
                        <h3 class="block-title-small m-v-15" style="font-size: 1.3em"><?php _e('Bulk Orders')?></h3>
                        <p class="details-description"><?php _e('Save your time! Deploy up to 50 pre-installed Germany Cloud Servers with a single order - one location, same configuration and less hassle!'); ?></p>
                    </div>
                    <div class="block-col center">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-cs-scalability1.svg" alt="Germany Cloud Server scalability feature icon" height="140">
                        <h3 class="block-title-small m-v-15"><?php _e('Scalability')?></h3>
                        <p class="details-description"><?php _e('Scale your Germany Cloud server on demand! Downgrade or upgrade your resources just in a few clicks.'); ?></p>
                    </div>

                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .linux-cs-hosting-specials -->  


        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>


        <section class="service-features extra-pad color-bg-style-grad1" data-section-title="<?php _e('Features');?>">

            <div id="features" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title white"><?php _e('Cloud Server Features'); ?></h2>
                </div>

                <div class="four-col-row-biface-cards">

                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_flex-scalability_blue.svg" alt="<?php _e('Flexible scaling icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Flexible Scaling')?></h3>
                                <p class="white"><?php _e('No longer bound by the physical size of your server, quickly scale your Cloud based virtual machine at your Client Area.'); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_api_blue.svg" alt="<?php _e('Powerful API icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Powerful API')?></h3>
                                <p class="white"><?php _e('Directly access web control panel functionality and benefit from advanced Cloud networking features.'); ?></p>
                            </div> 
                        </div>
                    </div>

                </div>
                <div class="four-col-row-biface-cards">

                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_custom-os_blue.svg" alt="<?php _e('Custom ISO icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Custom ISO')?></h3>
                                <p class="white"><?php _e('Custom ISO allows you to mount configured image on your instance and run through the boot & setup process as you would on a bare metal server.'); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_team-collab_blue.svg" alt="<?php _e('Team collaboration icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Team Collaboration')?></h3>
                                <p class="white"><?php _e('Easily manage your Germany Cloud Server with your team by inviting others and setting access permissions.'); ?></p>
                            </div> 
                        </div>
                    </div>

                </div> 

                <div class="four-col-row-biface-cards p-b-40">

                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_full-root-access_blue.svg" alt="<?php _e('Full root access icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Full Root Access')?></h3>
                                <p class="white"><?php _e('Maintain your Linux Cloud Server in any preferred way with root permissions and full access to install any software needed.'); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_simple-control_blue.svg" alt="<?php _e('Simple control panel icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Simple Control Panel')?></h3>
                                <p class="white"><?php _e('Clean Cloud Server control panel is designed to provide you with all the necessary information and management capabilities on the fly.'); ?></p>
                            </div> 
                        </div>
                    </div>

                </div> 

            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->

        
        <section class="service-pricing extra-pad-top" data-section-title="<?php _e('Plans');?>">

            <div id="plans" class="scroll-id-target"></div>


            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Germany Cloud Servers Pricing')?></h2>
                </div>

                <div class="segment-wrapper roundlike size-large non-responsive p-t-20">
                    <select name="plan-switch" class="segment-select">
                        <option value="linux" data-icosrc="<?php echo $images;?>img/ico_os_linux.svg"><?php _e('Linux OS'); ?></option>
                        <option value="windows" data-icosrc="<?php echo $images;?>img/ico_os_windows.svg"><?php _e('Windows OS'); ?> </option>
                    </select>
                </div>

                <div id="block-linux" class="block-switchable">

                    <?php include_block("htmlblocks/pricing-tables/cs-hosting-lin-v2.php", array('location' => 'de')); ?>

                </div>

                <div id="block-windows" class="block-switchable">

                    <?php include_block("htmlblocks/pricing-tables/cs-hosting-win-v2.php", array('location' => 'de')); ?>

                </div>

                <?php include_block("htmlblocks/pricing-tables/cs-all-plans-include.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-pricing -->


<script>

 /*jQuery Segment.js plugin*/
    (function( $ ){
        $.fn.extend({
            Segment: function ( ) {
                $(this).each(function (){
                    var self = $(this);
                    var onchange = self.attr('onchange');
                    var wrapper = $("<div>",{class: "ui-segment"});
                    $(this).find("option").each(function (){
                        var option = $("<span>",{class: 'option',onclick:onchange,text: $(this).text(),value: $(this).val()});
                        if ($(this).is(":selected")){
                            option.addClass("active");
                        }
                        wrapper.append(option);
                    });
                    wrapper.find("span.option").click(function (){
                        wrapper.find("span.option").removeClass("active");
                        $(this).addClass("active");
                        self.val($(this).attr('value'));
                    });
                    $(this).after(wrapper);
                    $(this).hide();

                    /*Icon enhancements*/
                    $(this).find("option").each(function (){
                        var icosrc = $(this).attr('data-icosrc');
                        var optionval = $(this).val();
                        var $wrapperoption;
                        if (typeof(icosrc) !== 'undefined') {
                            $wrapperoption = wrapper.find("span.option[value='"+ optionval + "']")[0];
                            var icon = document.createElement("img");
                            $(icon).attr("src", icosrc);
                            //$wrapperoption.prepend(icon);     // no prepend() support for EDGE
                            $wrapperoption.insertBefore(icon, $wrapperoption.firstChild);
                        }

                    });

                });
            }
        });
    })(jQuery);

    // before initializing Segment switch check if URL provides hash for plans selection
    if(window.location.hash) {
         var hash_val = window.location.hash.substring(1),
             $selector_options = $('select.segment-select');

         $selector_options.children().each(function(){
             var a = $(this).val();
             if ($(this).val() === hash_val) {
                 $(this).attr('selected','selected');
             }
         })
     }

    // initialize Segment switch
    $(".segment-select").Segment();

</script>        

<script>

    $(document).ready(function(){

        var currentBlockActive = $('.ui-segment .option.active').attr('value');
        $('.block-switchable').filter('#block-' + currentBlockActive).addClass('active');

        $('.ui-segment .option').click(function(e){
            var newVal = $(this).attr('value');
            $('.block-switchable').removeClass('active');
            $('.block-switchable').filter('#block-' + newVal).addClass('active');
        })
    });

</script>


        <section class="contact-sales extra-pad-top color-bg-style5">

            <div id="contact-form" class="scroll-id-target"></div>

            <div class="container">

                <div class="container-flex">
                    <div class="block-col width-md-1-4 center p-b-20">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/question-baloon.svg" alt="<?php _e('Sales chat icon');?>" width="180">
                    </div> 
                    <div class="block-col width-md-3-4 p-h-30 center-sm">
                        <p class="title-paragraph-lite"><?php _e('Have questions about Cloud technology? Looking for a custom solution? Contact us to discuss your needs.'); ?></p>
                        <a href="" id="get-form" class="button primary large m-v-30"><?php _e('Get in Touch');?></a>
                        <div class="contact-form p-v-30" style="display:none;">
                        
                            <?php echo do_shortcode('[contact-form-7 id="'.$contact_form_id.'" title="Contact Sales" html_class="wpcf7-form captcha"]');?>

                        </div> 
                    </div>
                </div> 

            </div>

        </section> <!-- end of .contact-sales -->



<script>

    (function($){
        $(document).ready(function(){

            function goToByScroll(id) {
                /*Used for scrolling to error spot*/
                
                id = id.replace("link", ""); // Remove "link" from the ID
                var deltaScroll = 0,
                    heightStickyHeader = $('.sticky-wrapper.is-sticky').height();

                if (heightStickyHeader > 0) {
                    deltaScroll = heightStickyHeader;
                }
                $('html,body').animate({scrollTop: $("#" + id).offset().top - deltaScroll},'slow');
            };


            $('#get-form').click(function(e){
                e.preventDefault();
                $(this).hide();
                $(this).parent().find('.contact-form').show('slow');
            });

            // check for form postprocess hash '..wcpf7..' and show form if some errors occured
            var linkHash = location.hash;

            if (linkHash.indexOf('wpcf7') > 0) {
                $('#get-form').hide();
                $('.contact-form').show();
                goToByScroll('contact-form');
            }

        })
    })(jQuery)

</script>            
        
      
        <section class="service-faq extra-pad-top" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/land-germany-cs.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->



    </section> <!-- end of .main -->

</article>


<?php

include_block("htmlblocks/hosting/speedtest_enabling_js.php");

get_footer();

?>
