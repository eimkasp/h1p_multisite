<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Landing: Ubuntu VPS
*/

wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings2/landing.css', ['style']);

wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);
wp_enqueue_script('tablesaw', get_template_directory_uri() . '/js/vendor/tablesaw.js', ['jquery'], '3.0.0-beta.4', true);
wp_enqueue_script('tablesaw-init', get_template_directory_uri() . '/js/vendor/tablesaw-init.js', ['tablesaw'], '3.0.0-beta.4', true);


global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'vps');


/*Pricing comparison table data*/
$service_pricing = array(
    'en' => array (
        'host1plus' => '$10.00',
        'mediatemple' => '$30.00',
        'vpsnet' => '$24.00',
        'ramnode' => '$14.00' 
    ),
    'br' => array (
        'host1plus' => '$10.00',
        'mediatemple' => '$30.00',
        'vpsnet' => '$24.00',
        'ramnode' => '$14.00'
    )
);

if(get_locale() == 'en_US'){
    $lang = 'en';
} elseif (get_locale() == 'pt_BR'){
    $lang = 'br';
}

// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing vps-hosting landing-ubuntuvps">

    <header class="headline landing-ubuntuvps">
        <div class="container adjust-vertical-center">

            <img class="m-t-40" src="<?php echo $images ?>landings2/images/header_icon_vps.svg" alt="VPS Hosting" width="105" height="105">
            <h1 class="page-title m-up-40"><?php _e('Ubuntu VPS hosting from')?> <?php echo $mypage->getMinPrice(); ?></h1>
            <div class="title-descr"><?php _e('High-performance unmanaged Linux Ubuntu hosting with full root access provides you with the control you\'ll love.');?></div>

            <div class="row center p-t-30">
                <a class="button highlight large" href="#plans" data-scrollto="plans"><?php _e('View Plans');?></a>
            </div>

        </div>
    </header> <!-- end of .headline.cs-hosting -->

    

    <section class="main page-content">



        <section class="service-features extra-pad">

            <div class="container">

                <div class="section-header p-b-20">
                    <h2 class="block-title"><?php _e('Pre-installed Ubuntu VPS servers at your figertips!')?></h2>
                </div>

                <div class="container-flex as-row">

                    <div class="block-col width-lg-1-1 p-h-30 p-v-20 m-h-100-md">
                        <p class="center p-b-40-md"><?php _e('Ubuntu is a popular GNU/Linux distribution widely used by many developers and considered to be one of the most stable distros. With all its perks deployed on our affordable servers we will save you lots of time and hassle.');?></p>
                    </div>

                </div>

                <div class="block-of-fourcross p-b-40-md">

                    <div class="layout-row two-col-row">
                        <div class="block-col center p-v-40">
                            <div class="icon-part p-v-20 center">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/style-orng1-multiple-versions.svg" alt="<?php _e('Multiple Ubuntu version feature icon')?>">
                            </div>
                            <div class="text-part">
                                <div class="text-block">
                                    <h3 class="block-title-small"><?php _e('Multiple versions');?></h3>
                                    <p><?php _e('Choose from a wide range of Ubuntu versions - 16.04 x86_64, 15.10 x86_64, 14.04 x86_64 and 14.04 x86.')?></p>
                                </div>
                            </div>
                        </div>
                        <div class="block-col center p-v-40">
                            <div class="icon-part p-v-20 center">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/style-orng1-quick-deployment.svg" alt="<?php _e('Ubuntu VPS quick deployment feature icon')?>">
                            </div>
                            <div class="text-part">
                                <div class="text-block">
                                    <h3 class="block-title-small"><?php _e('Quick deployment');?></h3>
                                    <p><?php _e('Get your Ubuntu VPS in 3 simple steps! Configure your server, fill in billing information and start using your VPS without delays.')?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layout-row two-col-row">
                        <div class="block-col center p-v-40">
                            <div class="icon-part p-v-20 center">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/style-orng1-full-control.svg" alt="<?php _e('Full control feature icon')?>">
                            </div>
                            <div class="text-part">
                                <div class="text-block">
                                    <h3 class="block-title-small"><?php _e('Full control');?></h3>
                                    <p><?php _e('As Ubuntu VPS system is partitioned, so you can efficiently utilize guaranteed resources what come with your plan.'); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="block-col center p-v-40">
                            <div class="icon-part p-v-20 center">z
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/style-orng1-scalability.svg" alt="<?php _e('Scalability feature icon')?>">
                            </div>
                            <div class="text-part">
                                <div class="text-block">
                                    <h3 class="block-title-small"><?php _e('Scalability');?></h3>
                                    <p><?php _e('Upgrade your Linux Ubuntu VPS anytime at your Client Area by adding more resources or simply changing your plan.')?></p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> <!-- end of .block-of-fourcross -->

                

            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->


        <section class="service-tutorials extra-pad land-color-bg-style2">

            <div class="row two-col-row has-special-background-positioning1 m-t-30 m-b-10">
                <div class="container">
                    
                    <div class="block-col color-bg-white rounded4 p-h-40 p-v-60 center">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_tutorials_orange.svg" alt="Is VPS server the right choice image" width="65">
                        <h3 class="t-mspaced p-b-20 p-h-60-md"><?php _e('Ubuntu Tutorials')?></h3>
                        <p class="t-spaced p-b-30"><?php _e('While Ubuntu is called "Linux for human beings", you may still use some help. Take a look at our Ubuntu tutorials to get some tips!'); ?></p>
                        <ul class="features-list fa-ul left spaced">
                            <li><i class="fa-li fa fa-angle-right"></i><a target="_blank" href="https://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/limit-cpu-usage-ubuntu"><?php _e('How to limit CPU usage on Ubuntu?'); ?></a></li>
                            <li><i class="fa-li fa fa-angle-right"></i><a target="_blank" href="https://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/how-to-install-ispconfig-3-on-debianubuntu"><?php _e('How to install ISPConfig on Ubuntu?'); ?></a></li>
                            <li><i class="fa-li fa fa-angle-right"></i><a target="_blank" href="https://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/how-to-configure-postfix-gmail-smtp-server-on-your-ubuntu-server"><?php _e('How to configure Postfix GMAIL SMTP server on your Ubuntu server?'); ?></a></li>
                        </ul>
                        <a class="button ghost-primary large m-t-60 hidden-sm" target="_blank" href="https://www.host1plus.com/tutorials/operating-systems/linux"><?php _e('View more Linux tutorials'); ?></a>
                        <a class="button ghost-primary large m-t-60 hidden-md hidden-lg" target="_blank" href="https://www.host1plus.com/tutorials/operating-systems/linux"><?php _e('More Linux tutorials'); ?></a>
                    </div> 
                    <div class="block-col">
                    </div>

                </div> 
            </div>

        </section> <!-- end of .service-tutorials -->




        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>


        

        <section class="service-features extra-pad color-bg-style-grad2" data-section-title="<?php _e('Features');?>">

            <div id="features" class="scroll-id-target"></div>

            <div class="container">

                <div class="block-of-two-1-4">

                    <div class="block-col p-b-40">
                        <h2 class="block-title white p-b-20"><?php _e('Ubuntu VPS Features'); ?></h2>
                        <p class="p-v-10 white-dim1"><?php printf(__('%sCheap VPS%s Ubuntu servers are powered by OpenVZ virtualization — lightweight hypervisor efficiently handling resources for your projects.'), '<a class="white" href="/cheap-vps-hosting/">', '</a>'); ?></p>
                    </div> 
                    <div class="block-col p-h-20">

                        <div class="three-col-row">
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_virtual-console-access_white.svg" alt="<?php _e('Virtual Console Access feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Virtual Console Access')?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('For instant recovery, retrieve access to your server by connecting via virtual console.'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_dns-rdns-management_white.svg" alt="<?php _e('DNS & rDNS Management feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('DNS & rDNS Management'); ?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('Simplified DNS and rDNS self-management means less time spent getting support and more time working on your own projects.'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_scalable-resources_white.svg" alt="<?php _e('Scalable Resources feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Scalable Resources'); ?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('Grab more resources to support your growing needs! Customize your VPS at the Client Area and stay on a budget!'); ?></p>
                            </div>
                        </div> <!-- end of .three-col-row -->

                        <div class="row m-v-30-md"></div>

                        <div class="three-col-row">
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_instant-deployment_white.svg" alt="<?php _e('Instant Deployment feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Instant Deployment'); ?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('Get your VPS in 3 simple steps! Configure your server, fill in billing information and start using your VPS without delays.'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_live-stats_white.svg" alt="<?php _e('Live Stats feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Live Stats')?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('Track your resource usage live! Enable alerts to avoid CPU, memory or network overuse.'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_geolocated-ips_white.svg" alt="<?php _e('Geolocated IPs feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Geolocated IPs')?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('Benefit from geolocated IPs to reduce latency, boost your SEO capability and localize your online business.'); ?></p>
                            </div>
                        </div> <!-- end of .three-col-row -->

                    </div> 

                </div> <!-- end of .block-of-two-1-4 -->


            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->
        

        <section class="service-locations-list extra-pad color-bg-style5" data-section-title="<?php _e('Locations');?>">

            <div id="locations" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header p-b-40">
                    <h2 class="block-title"><?php _e('Ubuntu VPS Server Locations')?></h2>
                </div>

                <?php 

                    /*Calculate cheapest USA location pricing*/
                    $price_la = $mypage->getMinPrice('los_angeles');
                    $price_ch = $mypage->getMinPrice('sao_paulo');
                    $price_usa_final = min(array($price_la, $price_ch));

                ?>

                <div class="container-flex block-offer2-container four">
                    <div class="block-col block-offer2">
                        <a class="wrapper-content" href="/germany-vps/">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_Germany.svg" alt="Germany VPS icon" width="90">
                            <div class="text-wrap">
                                    <h3 class="title-paragraph-lower bold"><?php _e('Frankfurt'); ?>, DE</h3>
                                    <span class="t-mshrinked">from <?php echo $mypage->getMinPrice('frankfurt') ?>/mo</span>
                            </div>
                        </a>
                    </div>
                    <div class="block-col block-offer2">
                        <a class="wrapper-content" href="/vps-brazil/">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_Brazil.svg" alt="Brazil VPS icon" width="90">
                            <div class="text-wrap">
                                <h3 class="title-paragraph-lower bold"><?php _e('São Paulo'); ?>, BR</h3>
                                <span class="t-mshrinked">from <?php echo $mypage->getMinPrice('sao_paulo') ?>/mo</span>
                            </div>
                        </a>
                    </div>
                    <div class="block-col block-offer2">
                        <a class="wrapper-content" href="/vps-usa/">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_USA.svg" alt="USA VPS icon" width="90">
                            <div class="text-wrap">
                                <h3 class="title-paragraph-lower bold"><?php _e('Los Angeles'); ?> <br/> <?php _e('Chicago'); ?>, US</h3>
                                <span class="t-mshrinked">from <?php echo $price_usa_final ?>/mo</span>
                            </div>
                        </a>
                    </div>
                    <div class="block-col block-offer2">
                        <a class="wrapper-content" href="/vps-hosting/">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_South_Africa.svg" alt="South Africa VPS icon" width="90">
                            <div class="text-wrap">
                                <h3 class="title-paragraph-lower bold"><?php _e('Johannesburg'); ?>, ZA</h3>
                                <span class="t-mshrinked">from <?php echo $mypage->getMinPrice('johannesburg') ?>/mo</span>
                            </div>
                        </a>
                    </div>
                </div> 


            </div> <!-- end of .container -->

        </section> <!-- end of .service-locations-list -->


        <section class="service-pricing extra-pad-top" data-section-title="<?php _e('Plans');?>">

            <div id="plans" class="scroll-id-target"></div>


            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ubuntu VPS Pricing')?></h2>
                </div>

                <div>

                    <?php include_block("htmlblocks/pricing-tables/vps-hosting-v2.php"); ?>

                </div>
            
                <a data-link-target="<?php echo $mypage->getMinPriceCheckoutLink();?>" data-section-button="<?php _e('Order Now');?>" data-button-styling="button highlight <?php echo $mypage->checkoutActivityClass($mypage->getCheapestLocation()); ?>" class="hidden"></a>

                <?php include_block("htmlblocks/pricing-tables/vps-all-plans-include.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-pricing -->

        
        
      
        <section class="service-faq extra-pad" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/land-ubuntu-vps.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->


        <section class="service-cta extra-pad color-bg-style-grad2a">

            <div class="container">
                
                <div class="container-flex cta">
                    <div class="block-col width-md-2-3 cta-left">
                        <span class="block-title-xlarge-thin white t-ff-header t-mspaced"><?php _e('Looking for Windows VPS?');?></span>
                    </div>
                    <div class="block-col width-md-1-3 cta-right">
                        <a href="/cheap-vps-windows/" class="button xlarge ghost"><?php _e('Windows KVM VPS');?></a>
                    </div> 
                </div> 

            </div>

        </section> <!--end of .service-cta-->



    </section> <!-- end of .main -->

</article>


<?php

get_footer();

?>
