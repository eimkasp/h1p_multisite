<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Landing: IaaS Cloud provider
*/


$custom_fields = get_post_custom();
$contact_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings2/landing.css', ['style']);

wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'cs_lin');

/*Get Cloud IP config option pricing
-------------------------*/
// id=342

$cloud_hosting_config_options = $whmcs->getConfigurablePrices(342);
$cloud_hosting_ip = $cloud_hosting_config_options['IP']['options']['IP']['pricing']['monthly'];

/*Windows licence addon pricing*/
$addons_winlic = $whmcs->getAddonsPrices(33);
$cloud_hosting_winlic =  $addons_winlic['price']; // it is monthly!

// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing cloud-servers">

    <header class="headline landing-iaas landing-header1">
        <div class="container adjust-vertical-center">
            <div class="wrapper">

                <h1 class="page-title">IaaS Cloud Servers trusted by thousands</h1>
                <div class="title-descr p-t-10">Scale as you grow with our flexible IaaS services.</div>

                <div class="home-slider-feat-wrap center m-v-20">
                    <div class="feat-wrap-block">
                        <i class="fa fa-check"></i> <?php _e('KVM Hypervisor')?>
                    </div>
                    <div class="feat-wrap-block">
                        <i class="fa fa-check"></i> <?php _e('Powerful API')?>
                    </div>
                    <div class="feat-wrap-block">
                        <i class="fa fa-check"></i> <?php _e('Simple Documentation')?>
                    </div>
                </div>

                <div class="center p-t-40">
                    <a href="#plans-table" data-scrollto="plans-table" class="button highlight large p-h-40"><?php _e('View plans');?></a>
                    <p class="white p-t-10"><?php printf(__('Starting from %s!'), $mypage->getMinPrice()); ?></p> 
                </div>
            </div>
        </header> <!-- end of .headline -->

        </div>
    </header> <!-- end of .headline.cs-hosting -->


    <section class="main page-content">

        <section class="service-description extra-pad-top">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title">What is IaaS?</h2>
                </div>

                <div class="container-flex as-row">

                    <div class="block-col p-b-20 p-h-20">
                        <p class="center">IaaS or Infrastructure as a Service is the foundation of Cloud Servers. Rather than purchasing or renting space in an expensive data center, entrust us with your virtual server deployment, network, storage upgrades and maintenance.</p>
                    </div> 

                    <div class="block-col width-lg-1-1 order-5 p-h-30 center">
                        <img class="scale" src="<?php echo $images; ?>landings2/images/what-is-iaas.png" alt="What is IaaS illustration">
                    </div>

                
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-description 1 block -->


        <section class="service-description extra-pad ">

            <div class="container">

                <div class="container-flex as-row">

                    <div class="block-col width-md-1-2 order-1 p-h-30 p-v-60">
                        <h3 class="block-title p-b-20 t-mspaced">What do IaaS providers offer?</h3>
                        <p class="t-18-spaced">IaaS providers manage hardware, network and virtualization including software maintenance. An IaaS provider takes full responsibility for the hardware and performs all the maintenance to ensure the servers run smoothly.</p>
                    </div>
                    <div class="block-col width-md-1-2 order-2 center-sm text-right-md text-right-lg p-h-20 p-t-30">
                        <img class="scale" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/iaas-providers.png" alt="What IaaS providers offer ilustration">
                    </div>

                </div> <!-- end of .container-flex -->

            </div> <!-- end of .container -->

            <div class="block-separator bordered m-t-40"></div> 

            <div class="container">

                <div class="container-flex as-row">

                    <div class="block-col width-md-1-2 order-4 p-h-30 p-v-60">
                        <h3 class="block-title p-b-20 t-mspaced">Why IaaS?</h3>
                        <p class="t-18-spaced">The main benefit of the IaaS model is provisioning and de-provisioning resources quicker, thus enabling the infrastructure size to match the demand. For example, it can be used to support a website which is in need of reacting to various types of demand loads every day. It can also be used to support testing and development processes that cannot be predicted in advance.</p>
                    </div>
                    <div class="block-col width-md-1-2 order-3 center-sm text-left-md text-left-lg p-h-20 p-t-30">
                        <img class="scale" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/iaas-benefits.png" alt="Why should you choose IaaS illustration">
                    </div>
                
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-description 2 block -->



        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>



        <section class="service-features extra-pad color-bg-style-grad1" data-section-title="<?php _e('Features');?>">

            <div id="features" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title white"><?php _e('What do we offer?'); ?></h2>
                </div>

                <div class="four-col-row-biface-cards">

                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_solid-isolation-kvm_blue.svg" alt="Solid isolation feature icon">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10">Solid Isolation</h3>
                                <p class="white">The most desired Windows & Linux IaaS Cloud powered by KVM hypervisor guarantees high stability, immaculate performance and solid security.</p>
                            </div>
                        </div>
                    </div>
                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_auto-backups_blue.svg" alt="Scheduled backups feature icon">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10">Scheduled Backups</h3>
                                <p class="white">Schedule daily, weekly or monthly automated backups. You can also create backups manually whenever you have to!</p>
                            </div> 
                        </div>
                    </div>

                </div>
                <div class="four-col-row-biface-cards">

                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_custom-os_blue.svg" alt="<?php _e('Custom ISO icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10">Custom ISO</h3>
                                <p class="white">Custom ISO allows you to mount a configured image on your IaaS Cloud and run through the boot & setup processes as you would on a bare metal server.</p>
                            </div>
                        </div>
                    </div>
                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_live-stats_blue.svg" alt="Live stats feature icon">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10">Live Stats</h3>
                                <p class="white"><?php _e('Easily manage your Germany Cloud Server with your team by inviting others and setting access permissions.'); ?></p>
                            </div> 
                        </div>
                    </div>

                </div> 

                <div class="four-col-row-biface-cards p-b-40">

                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_dns_blue.svg" alt="DNS Management feature icon">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10">DNS Management</h3>
                                <p class="white">Easily create your domain zones and edit records directly at your Client Area.</p>
                            </div>
                        </div>
                    </div>
                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_rdns_blue.svg" alt="rDNS control feature icon">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10">rDNS Control</h3>
                                <p class="white">Simplified rDNS self-management means less time spent getting support and more time working on your own projects.</p>
                            </div> 
                        </div>
                    </div>

                </div> 

            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->


        <section class="service-api extra-pad">
            
            <div class="container">

                <div class="section-header">
                    <h2 class="block-title">Application Programming Interface</h2>
                </div>

                <p class="center t-18-spaced">Power-up with an <a target="_blank" href="https://www.host1plus.com/api">in-house developed API</a>. Every IaaS Cloud Servers user gets access to our API and can manage their assets, integrate monitoring to their already existing systems or create various applications and projects working hand-to-hand with our API.</p>

                <div class="block-col width-lg-1-1 p-h-30 p-t-60 center">
                    <img class="scale p-h-100-lg" src="<?php echo $images; ?>landings2/images/iaas-api.png" alt="IaaS Powered with API illustration">
                </div>

            </div> <!-- end of .container -->
        
        </section> <!-- end of .service-api -->


        <section class="service-locations extra-pad-top color-bg-style2 no-zoom" data-section-title="<?php _e('Locations');?>">

            <div id="locations" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Cloud Server Locations')?></h2>
                        <p><?php _e('Multiple premium data centers ensure high connectivity and better reach.'); ?></p>
                </div>

                <?php include_block("htmlblocks/locations-services.php", array('type' => 'cloud', 'speedtest' => true )); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-locations -->


        <section class="service-pricing extra-pad-top" data-section-title="<?php _e('Plans');?>">

            <div class="scroll-id-target" id="plans-table"></div>

            <div class="container">

                <div class="section-header p-b-10">
                    <h2 class="block-title"><?php _e('Cloud Server Pricing')?></h2>
                </div>

                <div class="segment-wrapper roundlike size-large non-responsive p-t-20">
                    <select name="plan-switch" class="segment-select">
                        <option value="linux" data-icosrc="<?php echo $images;?>img/ico_os_linux.svg"><?php _e('Linux OS'); ?></option>
                        <option value="windows" data-icosrc="<?php echo $images;?>img/ico_os_windows.svg"><?php _e('Windows OS'); ?> </option>
                    </select>
                </div>

                <div id="block-linux" class="block-switchable">

                    <?php include_block("htmlblocks/pricing-tables/cs-hosting-lin-v2.php"); ?>

                </div>

                <div id="block-windows" class="block-switchable">

                    <?php include_block("htmlblocks/pricing-tables/cs-hosting-win-v2.php"); ?>

                </div>

                <?php include_block("htmlblocks/pricing-tables/cs-all-plans-include.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-pricing -->


        <section class="contact-sales extra-pad-top color-bg-style5">

            <div id="contact-form" class="scroll-id-target"></div>

            <div class="container">

                <div class="container-flex">
                    <div class="block-col width-md-1-4 center p-b-20">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/question-baloon.svg" alt="<?php _e('Icon illustrating sales chat');?>" width="180">
                    </div> 
                    <div class="block-col width-md-3-4 p-h-30 center-sm">
                        <p class="title-paragraph-lite"><?php _e('Have questions about IaaS Cloud Servers? Looking for a custom solution? Contact us to discuss your needs.'); ?></p>
                        <a href="" id="get-form" class="button primary large m-v-30"><?php _e('Get in Touch');?></a>
                        <div class="contact-form p-v-30" style="display:none;">
                        
                            <?php echo do_shortcode('[contact-form-7 id="'.$contact_form_id.'" title="Contact Sales" html_class="wpcf7-form captcha"]');?>

                        </div> 
                    </div>
                </div> 

            </div>

        </section> <!-- end of .contact-sales -->        



<script>

    (function($){
        $(document).ready(function(){

            function goToByScroll(id) {
                /*Used for scrolling to error spot*/
                
                id = id.replace("link", ""); // Remove "link" from the ID
                var deltaScroll = 0,
                    heightStickyHeader = $('.sticky-wrapper.is-sticky').height();

                if (heightStickyHeader > 0) {
                    deltaScroll = heightStickyHeader;
                }
                $('html,body').animate({scrollTop: $("#" + id).offset().top - deltaScroll},'slow');
            };


            $('#get-form').click(function(e){
                e.preventDefault();
                $(this).hide();
                $(this).parent().find('.contact-form').show('slow');
            });

            // check for form postprocess hash '..wcpf7..' and show form if some errors occured
            var linkHash = location.hash;

            if (linkHash.indexOf('wpcf7') > 0) {
                $('#get-form').hide();
                $('.contact-form').show();
                goToByScroll('contact-form');
            }

        });

        /*jQuery Segment.js plugin*/
        $.fn.extend({
            Segment: function ( ) {
                $(this).each(function (){
                    var self = $(this);
                    var onchange = self.attr('onchange');
                    var wrapper = $("<div>",{class: "ui-segment"});
                    $(this).find("option").each(function (){
                        var option = $("<span>",{class: 'option',onclick:onchange,text: $(this).text(),value: $(this).val()});
                        if ($(this).is(":selected")){
                            option.addClass("active");
                        }
                        wrapper.append(option);
                    });
                    wrapper.find("span.option").click(function (){
                        wrapper.find("span.option").removeClass("active");
                        $(this).addClass("active");
                        self.val($(this).attr('value'));
                    });
                    $(this).after(wrapper);
                    $(this).hide();

                    /*Icon enhancements*/
                    $(this).find("option").each(function (){
                        var icosrc = $(this).attr('data-icosrc');
                        var optionval = $(this).val();
                        var $wrapperoption;
                        if (typeof(icosrc) !== 'undefined') {
                            $wrapperoption = wrapper.find("span.option[value='"+ optionval + "']")[0];
                            var icon = document.createElement("img");
                            $(icon).attr("src", icosrc);
                            //$wrapperoption.prepend(icon);     // no prepend() support for EDGE
                            $wrapperoption.insertBefore(icon, $wrapperoption.firstChild);
                        }

                    });
                    
                });
            }
        });

        // before initializing Segment switch check if URL provides hash for plans selection
        if(window.location.hash) {
            var hash_val = window.location.hash.substring(1),
                $selector_options = $('select.segment-select');

            $selector_options.children().each(function(){
                var a = $(this).val();
                if ($(this).val() === hash_val) {
                    $(this).attr('selected','selected');
                }
            })
        }


        $(document).ready(function(){

            // initialize Segment switch
            $(".segment-select").Segment();

            var currentBlockActive = $('.ui-segment .option.active').attr('value');
            $('.block-switchable').filter('#block-' + currentBlockActive).addClass('active');

            $('.ui-segment .option').click(function(e){
                var newVal = $(this).attr('value');
                $('.block-switchable').removeClass('active');
                $('.block-switchable').filter('#block-' + newVal).addClass('active');
            })

        });

    })(jQuery)

</script>

      
        <section class="service-faq extra-pad-top" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/land-iaas-cs.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->



    </section> <!-- end of .main -->

</article>


<?php

include_block("htmlblocks/hosting/speedtest_enabling_js.php");

get_footer();

?>
