<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Landing: Windows VPS (Windows CS)
*/

/*
-----------------------------------------------------------------
    ATTENTION!
    This is Windows Cloud Server actually, only called Windows VPS
-----------------------------------------------------------------
*/


$custom_fields = get_post_custom();
$contact_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings2/landing.css', ['style']);

wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'cs_win');

/*Get Cloud IP config option pricing
-------------------------*/
// id=342

$cloud_hosting_config_options = $whmcs->getConfigurablePrices(342);
$cloud_hosting_ip = $cloud_hosting_config_options['IP']['options']['IP']['pricing']['monthly'];

/*Windows licence addon pricing*/
$addons_winlic = $whmcs->getAddonsPrices(33);
$cloud_hosting_winlic =  $addons_winlic['price']; // it is monthly!

// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing vps-hosting cloud-servers">

    <header class="headline landing-windowsvps landing-header1">
        <div class="container adjust-vertical-center">
            <div class="wrapper">

                <h1 class="page-title"><?php _e('Windows VPS Hosting')?></h1>
                <div class="title-descr p-t-10"><?php _e('Store your projects on scalable Windows VPS.');?></div>

                <div class="home-slider-feat-wrap center m-v-20">
                    <div class="feat-wrap-block">
                        <i class="fa fa-check"></i> <?php _e('KVM Hypervisor')?>
                    </div>
                    <div class="feat-wrap-block">
                        <i class="fa fa-check"></i> <?php _e('Powerful API')?>
                    </div>
                    <div class="feat-wrap-block">
                        <i class="fa fa-check"></i> <?php _e('Simple Documentation')?>
                    </div>
                </div>

                <div class="center p-t-40">
                    <a href="#plans" data-scrollto="plans" class="button highlight large p-h-40"><?php _e('View Windows VPS Plans');?></a>
                    <p class="white p-t-10"><?php printf(__('Starting from %s!'), $mypage->getMinPrice()); ?></p> 
                </div>
            </div>
        </header> <!-- end of .headline -->

        </div>
    </header> <!-- end of .headline.cs-hosting -->


    <section class="main page-content">

        <section class="service-description extra-pad">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title p-b-20"><?php _e('What is Windows KVM VPS?'); ?></h2>
                </div>

                <div class="container-flex as-row">

                    <div class="block-col p-b-40 center">
                        <p class=""><?php printf(__('Our Windows VPS servers operate on KVM Hyper-V, which is a hypervisor-based technology providing true hardware virtualization and ultimate isolation. All Windows VPS plans come with Windows Server Standard license for %s per month.'),  $whmcs::$settings['currency_prefix'] . $cloud_hosting_winlic); ?></p>
                    </div> 

                    <div class="block-col width-lg-1-1 order-5 p-h-30 center">
                        <img class="scale" src="<?php echo $images; ?>landings2/images/win_vps_server1_stack.png" alt="Ícone VPS KVM Windows">
                    </div>

                
                </div>
                
                <div class="container-flex features-list m-t-20 p-v-40 p-h-20 land-color-bg-style2">

                    <div class="block-col width-md-1-3 p-h-20 separator-line md-vert-pad">
                        <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_workload_friendly_bluemix.svg" alt="High Availability" width="90" height="90">
                        <h3 class="title-paragraph-lower p-v-20"><?php _e('Workload Friendly')?></h3>
                        <p class="adjust-left land-color-style1"><?php _e('KVM VPS gets all the allocated resources, without sharing them with anyone else to guarantee maximum performance for high-workload tasks.')?></p>
                    </div>
                    <div class="block-col width-md-1-3 p-h-20 separator-line md-vert-pad">
                        <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_customization_bluemix.svg" alt="Customization icon" width="90" height="90">
                        <h3 class="title-paragraph-lower p-v-20"><?php _e('Customization')?></h3>
                        <p class="adjust-left land-color-style1"><?php _e('KVM virtualization runs on an independent Linux kernel allowing you to modify your KVM VPS kernel as well as upgrade or downgrade it if needed.')?></p>
                    </div>
                    <div class="block-col width-md-1-3 p-h-20 separator-line md-vert-pad">
                        <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_safe_environment_bluemix.svg" alt="Safe environment icon" width="90" height="90">
                        <h3 class="title-paragraph-lower p-v-20"><?php _e('Safe Environment')?></h3>
                        <p class="adjust-left land-color-style1"><?php _e('KVM VPS provides increased stability, security and overall performance because of true virtualization resulting in full VPS isolation.')?></p>
                    </div>

                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-description -->



        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>


        <section class="service-features extra-pad color-bg-style-grad1" data-section-title="<?php _e('Features');?>">

            <div id="features" class="scroll-id-target"></div>

            <div class="container">

                <div class="block-of-two-1-4">

                    <div class="block-col p-b-40">
                        <h2 class="block-title white p-b-20"><?php _e('Build your projects on Windows Server'); ?></h2>
                        <p class="p-v-10 white-dim1"><?php printf(__('Our %scheap Windows VPS%s are powered by Windows Server — bringing the innovation behind the world\'s largest datacenters to your private workspace.'), '<a class="white" href="/cheap-vps-windows/">', '</a>'); ?></p>
                    </div> 
                    <div class="block-col p-h-20">

                        <div class="two-col-row">
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_kvm_white.svg" alt="<?php _e('KVM virtualization icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('KVM virtualization')?></h3>
                                <p class="t-14 t-xspaced white-dim1"><?php _e('The most desired hypervisor with Windows support, high stability, immaculate performance & solid security.'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_scalable-resources_white.svg" alt="<?php _e('Scalable Resources feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Flexible Scaling')?></h3>
                                <p class="t-14 t-xspaced white-dim1"><?php _e('No longer bound by the physical size of your server, quickly scale your Windows VPS machine at your Client Area.'); ?></p>
                            </div>
                        </div> <!-- end of .two-col-row -->

                        <div class="row m-v-30-md"></div>

                        <div class="two-col-row">
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_api_white.svg" alt="<?php _e('Powerful API icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Powerful API')?></h3>
                                <p class="t-14 t-xspaced white-dim1"><?php _e('Directly access web control panel functionality and benefit from advanced Windows VPS networking features.'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_auto-backups_white.svg" alt="<?php _e('Scheduled backups icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Scheduled Backups')?></h3>
                                <p class="t-14 t-xspaced white-dim1"><?php _e('Schedule daily, weekly or monthly automated backups. You can also create backups manually whenever you have to!'); ?></p>
                            </div>
                        </div> <!-- end of .two-col-row -->

                        <div class="row m-v-30-md"></div>

                        <div class="two-col-row">
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_custom-os-iso_white.svg" alt="<?php _e('Custom ISO icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Custom ISO')?></h3>
                                <p class="t-14 t-xspaced white-dim1"><?php _e('Custom ISO allows you to mount a configured image on your Windows VPS and run through the boot & setup processes as you would on a bare metal server.'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_live-stats1_white.svg" alt="<?php _e('Live stats icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Live Stats')?></h3>
                                <p class="t-14 t-xspaced white-dim1"><?php _e('Track your resource usage live! Enable alerts to avoid CPU, memory or network overuse.'); ?></p>
                            </div>
                        </div> <!-- end of .two-col-row -->

                        <div class="row m-v-30-md"></div>

                        <div class="two-col-row">
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_dns_white.svg" alt="<?php _e('DNS icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('DNS Management')?></h3>
                                <p class="t-14 t-xspaced white-dim1"><?php _e('Easily create your domain zones and edit records directlyatyour Client Area.'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_rdns_white.svg" alt="<?php _e('rDNS icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('rDNS Control')?></h3>
                                <p class="t-14 t-xspaced white-dim1"><?php _e('Simplified rDNS self-management means less time spent getting support and more time working on your own projects.'); ?></p>
                            </div>
                        </div> <!-- end of .two-col-row -->

                    </div> 

                </div> <!-- end of .block-of-two-1-4 -->


            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->


        <section class="service-pricing extra-pad-top" data-section-title="<?php _e('Plans');?>">

            <div id="plans" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header p-b-0">
                    <h2 class="block-title"><?php _e('Windows VPS Pricing')?></h2>
                </div>

                <?php include_block("htmlblocks/pricing-tables/cs-hosting-win-v2.php"); ?>

                <a data-link-target="<?php echo $mypage->getMinPriceCheckoutLink(); ?>" data-section-button="<?php _e('Order Windows VPS Now');?>" data-button-styling="button highlight <?php echo $mypage->checkoutActivityClass($mypage->getCheapestLocation()); ?>" class="hidden"></a>

                <?php include_block("htmlblocks/pricing-tables/cs-all-plans-include.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-pricing -->


        <section class="contact-sales extra-pad-top color-bg-style5">

            <div id="contact-form" class="scroll-id-target"></div>

            <div class="container">

                <div class="container-flex">
                    <div class="block-col width-md-1-4 center p-b-20">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/question-baloon.svg" alt="<?php _e('Icon illustrating sales chat');?>" width="180">
                    </div> 
                    <div class="block-col width-md-3-4 p-h-30 center-sm">
                        <p class="title-paragraph-lite"><?php _e('Have questions about Windows VPS? Looking for a custom solution? Contact us to discuss your needs.'); ?></p>
                        <a href="" id="get-form" class="button primary large m-v-30"><?php _e('Get in Touch');?></a>
                        <div class="contact-form p-v-30" style="display:none;">
                        
                            <?php echo do_shortcode('[contact-form-7 id="'.$contact_form_id.'" title="Contact Sales" html_class="wpcf7-form captcha"]');?>

                        </div> 
                    </div>
                </div> 

            </div>

        </section> <!-- end of .contact-sales -->        



<script>

    (function($){
        $(document).ready(function(){

            function goToByScroll(id) {
                /*Used for scrolling to error spot*/
                
                id = id.replace("link", ""); // Remove "link" from the ID
                var deltaScroll = 0,
                    heightStickyHeader = $('.sticky-wrapper.is-sticky').height();

                if (heightStickyHeader > 0) {
                    deltaScroll = heightStickyHeader;
                }
                $('html,body').animate({scrollTop: $("#" + id).offset().top - deltaScroll},'slow');
            };


            $('#get-form').click(function(e){
                e.preventDefault();
                $(this).hide();
                $(this).parent().find('.contact-form').show('slow');
            });

            // check for form postprocess hash '..wcpf7..' and show form if some errors occured
            var linkHash = location.hash;

            if (linkHash.indexOf('wpcf7') > 0) {
                $('#get-form').hide();
                $('.contact-form').show();
                goToByScroll('contact-form');
            }

        })
    })(jQuery)

</script>            
        
      
        <section class="service-faq extra-pad" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/land-win1-vps.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->


        <section class="service-cta extra-pad color-bg-style-grad1a">

            <div class="container">
                
                <div class="container-flex cta">
                    <div class="block-col width-md-2-3 cta-left">
                        <span class="block-title-xlarge-thin white t-ff-header t-mspaced"><?php _e('Looking for something else?');?></span>
                    </div>
                    <div class="block-col width-md-1-3 cta-right">
                        <a href="/linux-vps/" class="button xlarge ghost"><?php _e('Linux KVM VPS');?></a>
                    </div> 
                </div> 

            </div>

        </section> <!--end of .extra-offer-cta-->



    </section> <!-- end of .main -->

</article>


<?php

include_block("htmlblocks/hosting/speedtest_enabling_js.php");

universal_redirect_footer([
    'en' => $site_en_url.'/windows-vps/',
    'br' => $site_br_url.'/windows-vps/'
]);

get_footer();

?>
