<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Landing: USA VPS
*/


$custom_fields = get_post_custom();
$contact_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings2/landing.css', ['style']);

wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'vps');

/*cPanel price. cPanel is a product with id=129*/
$cPanel_data = $whmcs->getConfigurablePrice(129,[]);
/*echo '<pre>';
var_dump($cPanel_data);
echo '</pre>';*/
$cPanel_price_mo = $cPanel_data[0]['price_monthly'];


// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing vps-hosting landing-usavps">

    <header class="headline landing-usavps">
        <div class="container adjust-vertical-center">

            <img class="m-t-60" src="<?php echo $images ?>landings2/images/header_icon_vps.svg" alt="USA VPS header image" width="105" height="105">
            <h1 class="page-title m-up-40"><?php _e('Deploy your VPS in USA'); ?></h1>
            <div class="title-descr"><?php _e('Choose between Chicago or Los Angeles and deploy your projects in seconds!'); ?></div>

            <div class="row center p-t-60">
                <a class="button highlight large" href="#plans" data-scrollto="plans"><?php _e('View USA VPS Plans'); ?></a>
                <p class="white p-t-10"><?php printf(__('Starting from %s/month!'), $mypage->getMinPrice()); ?></p> 
                <div class="block-title-strikethrough-close p-t-30"></div>
            </div>

            <div class="container-flex features-list p-b-60">

                <div class="block-col w-300 p-h-20">
                    <img class="m-t-20" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_manage-backups_orange.svg" alt="<?php _e('Manage backups icon');?>" width="130" height="100">
                    <h3 class="title-paragraph-lower white p-v-20"><?php _e('Manage Backups')?></h3>
                    <p class="white-dim2"><?php _e('USA VPS servers come with 2 free backups to make sure you never lose your work again!')?></p>
                </div>
                <div class="block-col w-300 p-h-20">
                    <img class="m-t-20" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_full-root-access_orange.svg" alt="<?php _e('Full Root Access icon');?>" width="130" height="100">
                    <h3 class="title-paragraph-lower white p-v-20"><?php _e('Full Root Access')?></h3>
                    <p class="white-dim2"><?php _e('Maintain your USA VPS server with root permissions and full access to install any software needed.')?></p>
                </div>
                <div class="block-col w-300 p-h-20">
                    <img class="m-t-20" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_variety_linux_os_orange.svg" alt="<?php _e('Linux OS variety icon');?>" width="130" height="100">
                    <h3 class="title-paragraph-lower white p-v-20"><?php _e('Variety of Linux OS')?></h3>
                    <p class="white-dim2"><?php _e('Get your favorite OS - Ubuntu, Debian, Fedora, CentOS or Suse and start working on your project.')?></p>
                </div>

            </div> <!-- end of .features-list -->


        </div>


    </header> <!-- end of .headline.landing-usavps -->



    <section class="main page-content">

        <section class="service-description extra-pad">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Locations to choose from!');?></h2>
                </div>

                <div class="container-flex as-row">

                    <div class="block-col width-lg-1-1 center">
                        <img class="scale p-h-100-lg" src="<?php echo $images; ?>landings2/images/north-america-locations-map.png" alt="<?php _e('VPS hosting locations in USA');?>">
                    </div>

                    <div class="container-flex features-list p-v-20">

                        <div class="block-col width-md-1-2 p-h-20 m-b-30 md-vert-pad container-flex">
                            <div class="inner-w-90">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_local-cust-supp_orange.svg" alt="<?php _e('Local support team icon');?>" width="60">
                            </div>
                            <div class="inner-w-90-whatsleft">
                                <h3 class="title-paragraph-lower p-b-20"><?php _e('Local Customer Support Team');?></h3>
                                <p class="adjust-left t-14 t-spaced land-color-style1"><?php _e('English speaking local support team is always ready to answer all your questions and inquiries.');?></p>
                            </div>
                        </div>
                        <div class="block-col width-md-1-2 p-h-20 m-b-30 md-vert-pad container-flex">
                            <div class="inner-w-90">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_known-payment-gtw_orange.svg" alt="<?php _e('Well-known payment gateways icon');?>" width="60">
                            </div>
                            <div class="inner-w-90-whatsleft">
                                <h3 class="title-paragraph-lower p-b-20"><?php _e('Well-Known Payment Gateways');?></h3>
                                <p class="adjust-left t-14 t-spaced land-color-style1"><?php printf(__('We accept popular payment gateways in USA, such as Ebanx, Boleto Bancário, Paypal and %smore%s!'),'<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1312/0/what-payment-method-can-i-use">', '</a>');?></p>
                            </div>
                        </div>
                        <div class="block-col width-md-1-2 p-h-20 m-t-30 md-vert-pad container-flex">
                            <div class="inner-w-90">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_multilingual-ca_orange.svg" alt="<?php _e('Multilingual Client Area icon');?>" width="60">
                            </div>
                            <div class="inner-w-90-whatsleft">
                                <h3 class="title-paragraph-lower p-b-20"><?php _e('Multilingual Client Area');?></h3>
                                <p class="adjust-left t-14 t-spaced land-color-style1"><?php _e('Designed to efficiently manage account & services our Client Area supports Portuguese, English, Spanish and Chinese.');?></p>
                            </div>
                        </div>
                        <div class="block-col width-md-1-2 p-h-20 m-t-30 md-vert-pad container-flex">
                            <div class="inner-w-90">
                                <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_best-prices_orange.svg" alt="<?php _e('Best prices icon');?>" width="60">
                            </div>
                            <div class="inner-w-90-whatsleft">
                                <h3 class="title-paragraph-lower p-b-20"><?php _e('Best Prices Across North America');?></h3>
                                <p class="adjust-left t-14 t-spaced land-color-style1"><?php _e('Our USA VPS stands out from the rest providing best value for money in the region.');?></p>
                            </div>
                        </div>
                    
                    </div> <!-- end of .features-list -->

                
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-description -->


        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>


        <section class="service-features extra-pad color-bg-style-grad2" data-section-title="<?php _e('Features');?>">

            <div id="features" class="scroll-id-target"></div>

            <div class="container">

                <div class="block-of-two-1-4">

                    <div class="block-col p-b-40">
                        <h2 class="block-title white p-b-20"><?php _e('USA VPS Features'); ?></h2>
                        <p class="p-v-10 white-dim2"><?php _e('Hassle-free United States VPS service management with features that put you in control.'); ?></p>
                    </div> 
                    <div class="block-col p-h-20">

                        <div class="three-col-row">
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_dns-rdns-management_white.svg" alt="<?php _e('DNS & rDNS Management feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('DNS & rDNS Management')?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('Simplified DNS and rDNS self-management means less time spent getting support and more time working on your own projects.'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_scalable-resources_white.svg" alt="<?php _e('Scalable Resources feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Scalable Resources')?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('Grab more resources to support your growing needs! Customize your USA VPS at the Client Area and scale while your project grows!'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_instant-deployment_white.svg" alt="<?php _e('Instant Deployment feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Instant Deployment')?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('Get your VPS in 3 simple steps! Configure your server, fill in billing information and start using your VPS without delays.'); ?></p>
                            </div>
                        </div> <!-- end of .three-col-row -->

                        <div class="row m-v-30-md"></div>

                        <div class="three-col-row">
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_live-stats_white.svg" alt="<?php _e('Live Stats feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Live Stats')?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('Track your resource usage live! Enable alerts to avoid CPU, memory or network overuse.'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_geolocated-ips_white.svg" alt="<?php _e('Geolocated IPs feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Geolocated IPs')?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('Benefit from geolocated IPs to reduce latency, boost your SEO capability and localize your online business.'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_virtual-console-access_white.svg" alt="<?php _e('Virtual Console Access feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Virtual Console Access')?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('For instant recovery, retrieve access to your server by connecting via virtual console.'); ?></p>
                            </div>
                        </div> <!-- end of .three-col-row -->

                    </div> 

                </div> <!-- end of .block-of-two-1-4 -->


            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->



        <section class="service-pricing extra-pad-top" data-section-title="<?php _e('Plans');?>">

            <div id="plans" class="scroll-id-target"></div>


            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('United States VPS Pricing');?></h2>
                </div>

                <div>

                    <?php include_block("htmlblocks/pricing-tables/vps-hosting-v2.php"); ?>

                </div>
            
                <a data-link-target="<?php echo $mypage->getMinPriceCheckoutLink('chicago'); ?>" data-section-button="<?php _e('Order USA VPS Now');?>" data-button-styling="button highlight <?php echo $mypage->checkoutActivityClass('chicago'); ?>" class="hidden"></a>

                <?php include_block("htmlblocks/pricing-tables/vps-all-plans-include.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-pricing -->




<script>

    (function($){
        $(document).ready(function(){

            function goToByScroll(id) {
                /*Used for scrolling to error spot*/
                
                id = id.replace("link", ""); // Remove "link" from the ID
                var deltaScroll = 0,
                    heightStickyHeader = $('.sticky-wrapper.is-sticky').height();

                if (heightStickyHeader > 0) {
                    deltaScroll = heightStickyHeader;
                }
                $('html,body').animate({scrollTop: $("#" + id).offset().top - deltaScroll},'slow');
            };


            $('#get-form').click(function(e){
                e.preventDefault();
                $(this).hide();
                $(this).parent().find('.contact-form').show('slow');
            });

            // check for form postprocess hash '..wcpf7..' and show form if some errors occured
            var linkHash = location.hash;

            if (linkHash.indexOf('wpcf7') > 0) {
                $('#get-form').hide();
                $('.contact-form').show();
                goToByScroll('contact-form');
            }

        })
    })(jQuery)

</script>
        

        <section class="service-cta color-bg-style2">

            <div class="container">

                <div class="row block-of-two-8-4 cta">
                    <div class="block-col">
                        <h2 class="push-left"><?php _e('Looking for something else?'); ?></h2>
                        <div class="clearfix" style="display: block;"></div>
                        <span class="push-left"><?php _e('Check out other VPS locations in Germany, South Africa and Brazil.');?></span>
                    </div>
                    <div class="block-col push-right">
                        <a target="_blank" href="<?php echo $config['links']['vps-hosting']; ?>" class="button xlarge ghost push-right"><?php _e('Discover')?></a>
                    </div>
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-cta -->

      
        <section class="service-faq extra-pad-top" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/land-usa-vps.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->



    </section> <!-- end of .main -->

</article>


<?php

include_block("htmlblocks/hosting/speedtest_enabling_js.php");

universal_redirect_footer([
    'en' => $site_en_url.'/vps-usa/',
    'br' => $site_br_url.'/vps-eua/'
]);

get_footer();

?>
