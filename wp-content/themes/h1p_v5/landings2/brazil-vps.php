<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Landing: Brazil VPS
*/

$custom_fields = get_post_custom();
$contact_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings2/landing.css', ['style']);

wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);


global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'vps');

// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing vps-hosting landing-brazilvps">

    <header class="headline landing-brazilvps">
        <div class="container adjust-vertical-center">

            <h1 class="page-title"><?php _e('Deploy your VPS in Brazil')?></h1>
            <div class="title-descr"><?php _e('We guarantee immaculate reach across South America!');?></div>

            <div class="row center p-t-30">
                <a class="button highlight large" href="#plans" data-scrollto="plans"><?php _e('View Plans');?></a>
                <p class="white p-t-10"><?php printf(__('From %s only!'), $mypage->getMinPrice('sao_paulo')); ?></p> 
            </div>

            <div class="margin-top-compensator h-300"></div>

        </div>


    </header> <!-- end of .headline.cs-hosting -->


    <section class="main page-content has-special-background-positioning1">

        <div class="container">
            <div class="row three-col-row no-spacing land-color-bg-style1 margin-top-pusher h-300">
                <div class="block-col white color-bg-white op-25 p-30 m-0 h-300 center has-special-text-behavior">
                    <img class="p-t-10" width="90" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/header_icon_teamcollab_orange.svg" alt="Manage Backups icon">
                    <h3 class="block-title-small"><?php _e('Manage Backups'); ?></h3>
                    <p class="white t-16-shrinked p-h-10"><?php _e('Brazil VPS Servers come with 2 free backups to make sure you never lose your work again!'); ?></p> 
                </div> 
                <div class="block-col white color-bg-white op-10 p-30 m-0 h-300 center has-special-text-behavior">
                    <img class="p-t-10" width="90" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/header_icon_access_orange.svg" alt="Full Root Access icon">
                    <h3 class="block-title-small"><?php _e('Full Root Access'); ?></h3>
                    <p class="white t-16-shrinked p-h-10"><?php _e('Maintain your VPS Server with root permissions and full access to install any software needed.'); ?></p>
                </div> 
                <div class="block-col white p-30 m-0 h-300 center has-special-text-behavior">
                    <img class="p-t-10" width="90" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/header_icon_varietyos_orange.svg" alt="Linux OS variety icon">
                    <h3 class="block-title-small"><?php _e('Variety of Linux OS'); ?></h3>
                    <p class="white t-16-shrinked p-h-10"><?php _e('Get your favorite OS - Ubuntu, Debian, Fedora, CentOS or Suse and start working on your project.'); ?></p>
                </div> 
            </div>
        </div>

        <section class="service-description extra-pad">

            <div class="container">

                <div class="row two-col-row">
                    <div class="block-col">
                        &nbsp;
                        <div class="breakpoint-w770-max has-special-background-positioning2 h-300 m-up-40 m-b-20">
                        </div> 
                    </div>
                    <div class="block-col p-h-20">
                        <div class="section-header adjust-left">
                            <h2 class="block-title center-sm p-b-10"><?php _e('Why choose VPS in São Paulo, Brazil?')?></h2>
                        </div>

                        <div class="features-list2">
                            <div class="feature-box">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="ico vertical-top p-t-10 p-r-20" rowspan=2>
                                                <img class="" width="55" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_local-cust-supp_orange.svg" alt="Local Customer Support Team icon">
                                            </td>
                                            <td class="title-paragraph-lower p-b-10">
                                                <?php _e('Local Customer Support Team')?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="content t-14">
                                                <?php _e('English and Portuguese speaking local support team is always ready to answer all your questions and inquiries.')?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> <!-- end of .feature-box -->
                            <div class="feature-box p-t-40">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="ico vertical-top p-t-10 p-r-20" rowspan=2>
                                                <img class="" width="55" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_known-payment-gtw_orange.svg" alt="Well-Known Payment Gateways icon">
                                            </td>
                                            <td class="title-paragraph-lower p-b-10">
                                                <?php _e('Well-Known Payment Gateways')?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="content t-14">
                                                <?php printf(__('We accept popular payment gateways in Brazil, such as Ebanx, Boleto Bancário, Paypal and %smore%s!'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1312/0/what-payment-method-can-i-use">', '</a>'); ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> <!-- end of .feature-box -->
                            <div class="feature-box p-t-40">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="ico vertical-top p-t-10 p-r-20" rowspan=2>
                                                <img class="" width="55" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_multilingual-ca_orange.svg" alt="Multilingual Client Area icon">
                                            </td>
                                            <td class="title-paragraph-lower p-b-10">
                                                <?php _e('Multilingual Client Area')?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="content t-14">
                                                <?php _e('Designed to efficiently manage account & services our Client Area supports Portuguese, English, Spanish and Chinese.'); ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> <!-- end of .feature-box -->
                            <div class="feature-box p-t-40">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="ico vertical-top p-t-10 p-r-20" rowspan=2>
                                                <img class="" width="55" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_best-prices_orange.svg" alt="Best Prices icon">
                                            </td>
                                            <td class="title-paragraph-lower p-b-10">
                                                <?php _e('Best Prices Across South America')?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="content t-14">
                                                <?php _e('Our Brazil VPS stands out from the rest providing best value for money in the region.'); ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> <!-- end of .feature-box -->
                        </div> <!-- end of .features-list2 -->


                    </div> <!-- end of .block-col -->
                </div> <!-- end of .two-col-row -->



            </div> <!-- end of .container -->

        </section> <!-- end of .service-description -->



        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>


        <section class="service-features extra-pad color-bg-style-grad2" data-section-title="<?php _e('Features');?>">

            <div id="features" class="scroll-id-target"></div>

            <div class="container">

                <div class="block-of-two-1-4">

                    <div class="block-col p-b-40">
                        <h2 class="block-title white p-b-20"><?php _e('Brazil VPS Features'); ?></h2>
                        <p class="p-v-10 white-dim2"><?php _e('Hassle-free Brazil VPS service management with features that put you in control.'); ?></p>
                    </div> 
                    <div class="block-col p-h-20">

                        <div class="three-col-row">
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_dns-rdns-management_white.svg" alt="<?php _e('DNS & rDNS Management feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('DNS & rDNS Management')?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('Simplified DNS and rDNS self-management means less time spent getting support and more time working on your own projects.'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_scalable-resources_white.svg" alt="<?php _e('Scalable Resources feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Scalable Resources')?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('Grab more resources to support your growing needs! Customize your VPS at the Client Area and scale while your project grows!'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_instant-deployment_white.svg" alt="<?php _e('Instant Deployment feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Instant Deployment')?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('Get your VPS in 3 simple steps! Configure your server, fill in billing information and start using your VPS without delays.'); ?></p>
                            </div>
                        </div> <!-- end of .three-col-row -->

                        <div class="row m-v-30-md"></div>

                        <div class="three-col-row">
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_live-stats_white.svg" alt="<?php _e('Live Stats feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Live Stats')?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('Track your resource usage live! Enable alerts to avoid CPU, memory or network overuse.'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_geolocated-ips_white.svg" alt="<?php _e('Geolocated IPs feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Geolocated IPs')?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('Benefit from geolocated IPs to reduce latency, boost your SEO capability and localize your online business.'); ?></p>
                            </div>
                            <div class="block-col adjust-left p-h-20 p-b-20 p-b-40-sm">
                                <img class="p-b-20" width="80" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_virtual-console-access_white.svg" alt="<?php _e('Virtual Console Access feature icon')?>">
                                <h3 class="title-paragraph-lower t-mshrinked white p-b-20"><?php _e('Virtual Console Access')?></h3>
                                <p class="t-14 t-xspaced white-dim2"><?php _e('For instant recovery, retrieve access to your server by connecting via virtual console.'); ?></p>
                            </div>
                        </div> <!-- end of .three-col-row -->

                    </div> 

                </div> <!-- end of .block-of-two-1-4 -->


            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->

        

        <section class="service-pricing extra-pad-top" data-section-title="<?php _e('Plans');?>">

            <div id="plans" class="scroll-id-target"></div>


            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Brazil VPS Pricing')?></h2>
                </div>

                <div>

                    <?php include_block("htmlblocks/pricing-tables/vps-hosting-v2.php", array('location' => 'br')); ?>

                </div>
            
                <a data-link-target="<?php echo $mypage->getMinPriceCheckoutLink('sao_paulo');?>" data-section-button="<?php _e('Order now');?>" data-button-styling="button highlight <?php echo $mypage->checkoutActivityClass('sao_paulo'); ?>" class="hidden"></a>

                <?php include_block("htmlblocks/pricing-tables/vps-all-plans-include.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-pricing -->


        <section class="service-locations-list extra-pad color-bg-style5" data-section-title="<?php _e('Locations');?>">

            <div id="locations" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header p-b-40">
                    <h2 class="block-title"><?php _e('Looking for something else?')?></h2>
                </div>

                <?php 

                    /*Calculate cheapest USA location pricing*/
                    $price_la = $mypage->getMinPrice('los_angeles');
                    $price_ch = $mypage->getMinPrice('sao_paulo');
                    $price_usa_final = min(array($price_la, $price_ch));

                ?>

                <div class="container-flex block-offer2-container four">
                    <div class="block-col block-offer2">
                        <a class="wrapper-content" href="/germany-vps/">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_Germany.svg" alt="Germany VPS icon" width="90">
                            <div class="text-wrap">
                                    <h3 class="title-paragraph-lower bold"><?php _e('Frankfurt'); ?>, DE</h3>
                                    <span class="t-mshrinked">from <?php echo $mypage->getMinPrice('frankfurt') ?>/mo</span>
                            </div>
                        </a>
                    </div>
                    <div class="block-col block-offer2">
                        <a class="wrapper-content" href="/vps-usa/">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_USA.svg" alt="USA VPS icon" width="90">
                            <div class="text-wrap">
                                <h3 class="title-paragraph-lower bold"><?php _e('Los Angeles'); ?> <br/> <?php _e('Chicago'); ?>, US</h3>
                                <span class="t-mshrinked">from <?php echo $price_usa_final ?>/mo</span>
                            </div>
                        </a>
                    </div>
                    <div class="block-col block-offer2">
                        <a class="wrapper-content" href="/vps-hosting/">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_South_Africa.svg" alt="South Africa VPS icon" width="90">
                            <div class="text-wrap">
                                <h3 class="title-paragraph-lower bold"><?php _e('Johannesburg'); ?>, ZA</h3>
                                <span class="t-mshrinked">from <?php echo $mypage->getMinPrice('johannesburg') ?>/mo</span>
                            </div>
                        </a>
                    </div>
                </div> 


            </div> <!-- end of .container -->

        </section> <!-- end of .service-locations-list -->

        

<script>

    (function($){
        $(document).ready(function(){

            function goToByScroll(id) {
                /*Used for scrolling to error spot*/
                
                id = id.replace("link", ""); // Remove "link" from the ID
                var deltaScroll = 0,
                    heightStickyHeader = $('.sticky-wrapper.is-sticky').height();

                if (heightStickyHeader > 0) {
                    deltaScroll = heightStickyHeader;
                }
                $('html,body').animate({scrollTop: $("#" + id).offset().top - deltaScroll},'slow');
            };


            $('#get-form').click(function(e){
                e.preventDefault();
                $(this).hide();
                $(this).parent().find('.contact-form').show('slow');
            });

            // check for form postprocess hash '..wcpf7..' and show form if some errors occured
            var linkHash = location.hash;

            if (linkHash.indexOf('wpcf7') > 0) {
                $('#get-form').hide();
                $('.contact-form').show();
                goToByScroll('contact-form');
            }

        })
    })(jQuery)

</script>            
        
      
        <section class="service-faq extra-pad-top" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/land-brazil-vps.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->



    </section> <!-- end of .main -->

</article>


<?php

include_block("htmlblocks/hosting/speedtest_enabling_js.php");

universal_redirect_footer([
    'en' => $site_en_url.'/vps-brazil/',
    'br' => $site_br_url.'/vps-brasil/'
]);

get_footer();


?>
