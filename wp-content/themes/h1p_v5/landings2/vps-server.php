<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Landing: VPS Server
*/

/* 
 * NOTE: Please add form_id custom field of corresponding contact-form-7 form in this WP page
 */

$custom_fields = get_post_custom();
$contact_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings2/landing.css', ['style']);

wp_enqueue_script('recaptcha', 'https://www.google.com/recaptcha/api.js?&hl=' . $lang_code, [], false, true);
wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);


global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'vps');

// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing cloud-servers landing-vpssrv">

    <header class="headline landing-vpssrv min-h-450 adjust-vertical-center">
        <div class="container">

            <img class="responsive m-t-40" src="<?php echo $images ?>landings2/images/header_icon_vps.svg" alt="VPS Servers" width="105" height="105">
            <h1 class="responsive page-title m-up-40">VPS Server</h1>
            <div class="title-descr p-t-20 t-mspaced">Trying to grasp the difference between VPS server and web hosting? Do not know which one you need? When would be the best time to go for VPS server?</div>

            <div class="row center p-t-40">
                <a class="button highlight large" href="#features-start" data-scrollto="features-start">Get Answers</a>
            </div>

        </div>
    </header> <!-- end of .headline.cs-hosting -->

    

    <section class="main page-content">



        <section class="service-features-1 extra-pad">

            <div id="features-start" class="scroll-id-target" style="margin-top: 65px;"></div>

            <div class="container">

                <div class="section-header p-b-20">
                    <h2 class="block-title">What is VPS server?</h2>
                </div>

                <div class="container-flex as-row">

                    <div class="block-col width-lg-1-1 p-30">
                        <img class="m-h-100-md" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/vps-server_img1.svg" alt="What is vps hosting image">
                    </div>
                
                </div> <!-- end of .container-flex -->

                <div class="container-flex features-list m-t-20 p-v-20">

                    <div class="block-col width-md-1-2 p-h-20 md-vert-pad container-flex">
                        <div class="inner-w-90">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_quick-overview_orange.svg" alt="Quick Overview" width="80">
                        </div>
                        <div class="inner-w-90-whatsleft">
                            <h3 class="title-paragraph-lower p-t-10 p-b-20">Quick Overview</h3>
                            <p class="adjust-left land-color-style1">VPS server is a part of a physical server that has been divided intro several virtual machines, each acting as an independent server with allocated resources to each.</p>
                        </div>
                    </div>
                    <div class="block-col width-md-1-2 p-h-20 md-vert-pad container-flex">
                        <div class="inner-w-90">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_main-advantage-vps_orange.svg" alt="Main Advantage of a VPS" width="80">
                        </div>
                        <div class="inner-w-90-whatsleft">
                            <h3 class="title-paragraph-lower p-t-10 p-b-20">Main Advantage of a VPS</h3>
                            <p class="adjust-left land-color-style1">While VPS servers are on the same physical machine they act independently and do not influence the performance of each other.</p>
                        </div>
                    </div>

                </div> <!-- end of .container-flex -->

            </div> <!-- end of .container -->
            
        </section> <!-- end of .service-features-1 -->
        
        <section class="service-features-2 extra-pad bordered">

            <div class="container">

                <div class="section-header p-b-20">
                    <h2 class="block-title">How is it different from shared web hosting?</h2>
                </div>

                <div class="container-flex as-row">

                    <div class="block-col width-md-1-2 order-1 center-sm text-left-md text-left-lg p-h-20 p-t-30">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/vps-server_img2.svg" alt="Shared hosting principle image">
                    </div>
                    <div class="block-col width-md-1-2 order-2 p-h-30 p-v-60">
                        <h3 class="title-paragraph p-b-20">Shared Web Hosting</h3>
                        <p class="t-18-spaced">Shared web hosting users share all allocated resources such as RAM, CPU, bandwidth and disk space with each other, so for example if one user has a traffic spike on his website and needs a bunch of resources, other users will suffer from bad performance.</p>
                    </div>

                    <div class="block-separator"></div> 

                    <div class="block-col width-md-1-2 order-4 center-sm text-right-md text-right-lg p-h-20 p-t-30">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/vps-server_img3.svg" alt="VPS hosting principle image">
                    </div>
                    <div class="block-col width-md-1-2 order-3 p-h-30 p-v-60">
                        <h3 class="title-paragraph p-b-20">VPS Server</h3>
                        <p class="t-18-spaced">Every VPS server gets a set amount of each resource individually and does not share it with other users on the same physical machine, meaning that if one user gets traffic spikes, others will not suffer from resource lack.</p>
                    </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-features-2 -->


        <section class="service-locations-list extra-pad color-bg-style5" data-section-title="<?php _e('Locations');?>">

            <div id="locations" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header p-b-40">
                    <h2 class="block-title"><?php _e('Choose your VPS server location!')?></h2>
                </div>

                <?php 

                    /*Calculate cheapest USA location pricing*/
                    $price_la = $mypage->getMinPrice('los_angeles');
                    $price_ch = $mypage->getMinPrice('sao_paulo');
                    $price_usa_final = min(array($price_la, $price_ch));

                ?>

                <div class="container-flex block-offer2-container four">
                    <div class="block-col block-offer2">
                        <a class="wrapper-content" href="/germany-vps/">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_Germany.svg" alt="Germany VPS icon" width="90">
                            <div class="text-wrap">
                                    <h3 class="title-paragraph-lower bold"><?php _e('Frankfurt'); ?>, DE</h3>
                                    <span class="t-mshrinked">from <?php echo $mypage->getMinPrice('frankfurt') ?>/mo</span>
                            </div>
                        </a>
                    </div>
                    <div class="block-col block-offer2">
                        <a class="wrapper-content" href="/vps-brazil/">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_Brazil.svg" alt="Brazil VPS icon" width="90">
                            <div class="text-wrap">
                                <h3 class="title-paragraph-lower bold"><?php _e('São Paulo'); ?>, BR</h3>
                                <span class="t-mshrinked">from <?php echo $mypage->getMinPrice('sao_paulo') ?>/mo</span>
                            </div>
                        </a>
                    </div>
                    <div class="block-col block-offer2">
                        <a class="wrapper-content" href="/vps-usa/">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_USA.svg" alt="USA VPS icon" width="90">
                            <div class="text-wrap">
                                <h3 class="title-paragraph-lower bold"><?php _e('Los Angeles'); ?> <br/> <?php _e('Chicago'); ?>, US</h3>
                                <span class="t-mshrinked">from <?php echo $price_usa_final ?>/mo</span>
                            </div>
                        </a>
                    </div>
                    <div class="block-col block-offer2">
                        <a class="wrapper-content" href="/vps-hosting/">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_South_Africa.svg" alt="South Africa VPS icon" width="90">
                            <div class="text-wrap">
                                <h3 class="title-paragraph-lower bold"><?php _e('Johannesburg'); ?>, ZA</h3>
                                <span class="t-mshrinked">from <?php echo $mypage->getMinPrice('johannesburg') ?>/mo</span>
                            </div>
                        </a>
                    </div>
                </div> 


            </div> <!-- end of .container -->

        </section> <!-- end of .service-locations-list -->


        <section class="order-vps extra-pad land-color-bg-style3">

            <div class="row two-col-row has-special-background-positioning1 m-t-30 m-b-10">
                <div class="container">
                    <div class="block-col color-bg-white rounded4 p-h-40 p-v-60 center">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/feat_layers_orange.svg" alt="Is VPS server the right choice image" width="65">
                        <h3 class="t-mspaced p-t-20 p-b-30 p-h-60-md">Is VPS server the right choice for you?</h3>
                        <p class="t-xspaced">VPS comes with a higher price than web hosting, but offers better performance & control over the hosting environment. If your website, project or application is growing and needs more resources, it is a good time to think about switching to VPS server!</p>
                        <a class="button highlight large m-t-40" href="<?php echo $mypage->getMinPriceCheckoutLink('frankfurt'); ?>">Order VPS Server Now</a>
                    </div> 
                    <div class="block-col">
                    </div>
                </div> 
            </div>

        </section> <!-- end of .order-vps -->

        
        <section class="contact-sales extra-pad-top color-bg-style5">

            <div id="contact-form" class="scroll-id-target"></div>

            <div class="container">

                <div class="container-flex">
                    <div class="block-col width-md-1-4 center p-b-20">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/question-baloon.svg" alt="<?php _e('Icon illustrating sales chat');?>" width="180">
                    </div> 
                    <div class="block-col width-md-3-4 p-h-30 center-sm">
                        <p class="title-paragraph-lite"><?php _e('Still have questions about VPS servers? Looking for someone to consult you? Write your questions for us!'); ?></p>
                        <a href="" id="get-form" class="button primary large m-v-30"><?php _e('Get in Touch');?></a>
                        <div class="contact-form p-v-30" style="display:none;">
                        
                            <?php echo do_shortcode('[contact-form-7 id="'.$contact_form_id.'" title="Contact Sales" html_class="wpcf7-form captcha"]');?>

                        </div> 
                    </div>
                </div> 

            </div>

        </section> <!-- end of .contact-sales -->

<script>

    (function($){
        $(document).ready(function(){

            function goToByScroll(id) {
                /*Used for scrolling to error spot*/
                
                id = id.replace("link", ""); // Remove "link" from the ID
                var deltaScroll = 0,
                    heightStickyHeader = $('.sticky-wrapper.is-sticky').height();
                
                if (heightStickyHeader > 0) {
                    deltaScroll = heightStickyHeader;
                }
                $('html,body').animate({scrollTop: $("#" + id).offset().top - deltaScroll},'slow');
            };


            $('#get-form').click(function(e){
                e.preventDefault();
                $(this).hide();
                $(this).parent().find('.contact-form').show('slow');
            });

            // check for form postprocess hash '..wcpf7..' and show form if some errors occured
            var linkHash = location.hash;

            if (linkHash.indexOf('wpcf7') > 0) {
                $('#get-form').hide();
                $('.contact-form').show();
                goToByScroll('contact-form');
            }

        })
    })(jQuery)

</script>

        <section class="service-faq extra-pad-top" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/land-vps-server.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->



    </section> <!-- end of .main -->

</article>



<?php

include_block("htmlblocks/hosting/speedtest_enabling_js.php");

universal_redirect_footer([
    'en' => $site_en_url.'/vps-server/',
    'br' => $site_br_url.'/'
]);

get_footer();

?>
