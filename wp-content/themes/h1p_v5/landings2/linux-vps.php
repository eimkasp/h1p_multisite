<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Landing: Linux VPS (Linux CS)
*/

/*
-----------------------------------------------------------------
    ATTENTION!
    This is Linux Cloud Server actually, only called Linux VPS
-----------------------------------------------------------------
*/


$custom_fields = get_post_custom();
$contact_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings2/landing.css', ['style']);

wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);
wp_enqueue_script('tablesaw', get_template_directory_uri() . '/js/vendor/tablesaw.js', ['jquery'], '3.0.0-beta.4', true);
wp_enqueue_script('tablesaw-init', get_template_directory_uri() . '/js/vendor/tablesaw-init.js', ['tablesaw'], '3.0.0-beta.4', true);


global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'cs_lin');

/*Get Cloud IP config option pricing
-------------------------*/
// id=342

$cloud_hosting_config_options = $whmcs->getConfigurablePrices(342);
$cloud_hosting_ip = $cloud_hosting_config_options['IP']['options']['IP']['pricing']['monthly'];

// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing vps-hosting cloud-servers landing-linuxvps">

    <header class="headline landing-linuxvps landing-header1">
        <div class="container adjust-vertical-center">
            <div class="wrapper">

                <h1 class="page-title"><?php _e('Linux KVM VPS Hosting')?></h1>
                <div class="title-descr p-t-10"><?php _e('Store your projects on scalable and isolated Linux VPS.');?></div>

                <div class="home-slider-feat-wrap center m-v-20">
                    <div class="feat-wrap-block">
                        <i class="fa fa-check"></i> <?php _e('KVM Hypervisor')?>
                    </div>
                    <div class="feat-wrap-block">
                        <i class="fa fa-check"></i> <?php _e('Powerful API')?>
                    </div>
                    <div class="feat-wrap-block">
                        <i class="fa fa-check"></i> <?php _e('Simple Documentation')?>
                    </div>
                </div>

                <div class="center p-t-40">
                    <a href="#plans" data-scrollto="plans" class="button highlight large p-h-40"><?php _e('Linux KVM VPS Plans');?></a>
                    <p class="white p-t-10"><?php printf(__('Starting from %s!'), $mypage->getMinPrice());  ?></p> 
                </div>
            </div>
        </header> <!-- end of .headline -->

        </div>
    </header> <!-- end of .headline.cs-hosting -->


    <section class="main page-content">

        <section class="service-description extra-pad">

            <div class="container">

                <div class="p-b-20 center">
                    <h2 class="block-title p-b-20"><?php _e('Linux KVM or Linux OpenVZ?'); ?></h2>
                    <p class=""><?php _e('See the differences of both hypervisors and decide wich one is the right choice for you.'); ?></p>
                </div> 
                
                <table class="table tablesaw tablesaw-swipe landing-linuxvps landing-table-default landing-table-text-2" data-tablesaw-priority="persist" data-tablesaw-mode="swipe" data-tablesaw-minimap>
                    <thead>
                        <tr>
                            <th class="landing-table-header emphasize title" scope="col" data-tablesaw-priority="persist" data-tablesaw-priority="1">
                                <?php _e('Features'); ?>
                            </th>
                            <th class="landing-table-header emphasize" scope="col" data-tablesaw-priority="2">
                                <?php _e('Linux OpenVZ'); ?> 
                            </th>
                            <th class="landing-table-header emphasize" scope="col" data-tablesaw-priority="3">
                                <?php _e('Linux KVM'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th class="title"><?php _e('Starting price');?></td>
                            <td class="emphasize"><?php echo $mypage->getServiceMinPrice('vps') ?></td>
                            <td class="emphasize"><?php echo $mypage->getMinPrice() ?></td>
                        </tr>
                        <tr>
                            <th class="title"><?php _e('Locations');?></td>
                            <td class=""><?php echo __('Frankfurt').', DE <br/>'.__('Chicago').', US <br/>'.__('Los Angeles').', US <br/>'.__('São Paulo').', BR <br/>'.__('Johannesburg').', ZA' ?></td>
                            <td class=""><?php echo __('Frankfurt').', DE <br/>'.__('Chicago').', US' ?></td>
                        </tr>
                        <tr>
                            <th class="title"><?php _e('Available OS');?></td>
                            <td class="">CentOS<br/>Debian<br/>Fedora<br/>Suse<br/>Ubuntu</td>
                            <td class="">CentOS<br/>Debian<br/>Fedora<br/>Ubuntu</td>
                        </tr>
                        <tr>
                            <th class="title"><?php _e('Isolation');?></td>
                            <td class=""><?php _e('Lower'); ?></td>
                            <td class=""><?php _e('Higher'); ?></td>
                        </tr>
                        <tr>
                            <th class="title"><?php _e('Security');?></td>
                            <td class=""><?php _e('Lower'); ?></td>
                            <td class=""><?php _e('Higher'); ?></td>
                        </tr>
                        <tr>
                            <th class="title"><?php _e('Stability');?></td>
                            <td class=""><?php _e('Lower'); ?></td>
                            <td class=""><?php _e('Higher'); ?></td>
                        </tr>
                        <tr>
                            <th class="title"><?php _e('API');?></td>
                            <td class="availability no"></td>
                            <td class="availability yes"></td>
                        </tr>
                        <tr>
                            <th class="title"><?php _e('API Documentation');?></td>
                            <td class="availability no"></td>
                            <td class="availability yes"></td>
                        </tr>
                        <tr>
                            <th class="title"><?php _e('Custom ISO');?></td>
                            <td class="availability no"></td>
                            <td class="availability yes"></td>
                        </tr>
                        <tr>
                            <th class="title"><?php _e('VNC Console');?></td>
                            <td class="availability no"></td>
                            <td class="availability yes"></td>
                        </tr>
                        <tr>
                            <th class="title"><?php _e('Netfilter firewall configurations');?></td>
                            <td class="availability no"></td>
                            <td class="availability yes"></td>
                        </tr>
                        <tr>
                            <th class="title"><?php _e('Kernel modifications');?></td>
                            <td class="availability no"></td>
                            <td class="availability yes"></td>
                        </tr>
                        <tr>
                            <th class="title"><?php _e('Custom kernel');?></td>
                            <td class="availability no"></td>
                            <td class="availability yes"></td>
                        </tr>
                        <tr>
                            <th class="title" style="border-bottom: 0"></td>
                            <td class="" style="border-bottom: 0"><a href="<?php echo $config['links']['vps-hosting']; ?>" target="_blank" class="button ghost-primary" data-scrollto="registration"><?php _e('OpenVZ Plans'); ?></a></td>
                            <td class="" style="border-bottom: 0"><a href="#plans" class="button ghost-primary" data-scrollto="plans"><?php _e('KVM Plans'); ?></a></td>
                        </tr>
                    </tbody>
                </table>


            </div> <!-- end of .container -->

        </section> <!-- end of .service-description -->



        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>


        <section class="service-features extra-pad color-bg-style-grad1" data-section-title="<?php _e('Features');?>">

            <div id="features" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title white"><?php _e('Linux KVM VPS Server Features'); ?></h2>
                </div>

                <div class="four-col-row-biface-cards">

                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_flex-scalability_blue.svg" alt="<?php _e('Flexible Windows Cloud Server feature icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Flexible Scaling')?></h3>
                                <p class="white"><?php _e('No longer bound by the physical size of your server, quickly scale your Linux KVM VPS at your Client Area.'); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_api_blue.svg" alt="<?php _e('Windows Cloud Server powerful API icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Powerful API')?></h3>
                                <p class="white"><?php _e('Directly access web control panel functionality and benefit from advanced Linux KVM VPS networking features.'); ?></p>
                            </div> 
                        </div>
                    </div>

                </div>
                <div class="four-col-row-biface-cards p-b-40">

                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_custom-os_blue.svg" alt="<?php _e('Windows Cloud Server custom ISO feature icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Custom ISO')?></h3>
                                <p class="white"><?php _e('Custom ISO allows you to mount a configured image on your Cloud instance and run through the boot & setup processes as you would on a bare metal server.'); ?></p>
                            </div> 
                        </div>
                    </div>
                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_auto-backups_blue.svg" alt="<?php _e('Scheduled backups for Windows Cloud Servers feature icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Manage Backups')?></h3>
                                <p class="white"><?php _e('Linux KVM VPS comes with 2 free backups for to make sure you never lose your work again!'); ?></p>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="four-col-row-biface-cards p-b-40">

                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_full-root-access_blue.svg" alt="<?php _e('Full root access icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Full Root Access')?></h3>
                                <p class="white"><?php _e('Maintain your Linux KVM VPS in any preferred way with root permissions and full access to install any software needed.'); ?></p>
                            </div> 
                        </div>
                    </div>
                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_simple-control-panel_blue.svg" alt="<?php _e('Simple control panel icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Simple Control Panel')?></h3>
                                <p class="white"><?php _e('Clean Linux KVM VPS control panel is designed to provide you with all the necessary information and management capabilities on the fly.'); ?></p>
                            </div>
                        </div>
                    </div>

                </div> 

            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->


        <section class="service-cta p-v-60">

            <div class="container">
                
                <div class="container-flex cta">
                    <div class="block-col width-md-2-3 cta-left">
                        <span class="block-title-xlarge-thin t-ff-header t-mspaced color-primary"><?php _e('Looking for something else?');?></span>
                    </div>
                    <div class="block-col width-md-1-3 cta-right">
                        <a href="/windows-vps/" class="button xlarge ghost-primary"><?php _e('Windows KVM VPS');?></a>
                    </div> 
                </div> 

            </div>

        </section> <!--end of .service-cta-->


        <section class="service-locations extra-pad-top color-bg-style2 no-zoom" data-section-title="<?php _e('Locations');?>">

            <div id="locations" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Worldwide Linux KVM VPS Server Locations')?></h2>
                        <p><?php _e('Multiple premium data centers ensure high connectivity and better reach.'); ?></p>
                </div>

                <?php include_block("htmlblocks/locations-services.php", array('type' => 'cloud', 'speedtest' => true )); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-locations -->
        

        <section class="service-pricing extra-pad-top" data-section-title="<?php _e('Plans');?>">

            <div id="plans" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header p-b-0">
                    <h2 class="block-title"><?php _e('Linux KVM VPS Pricing')?></h2>
                </div>

                <?php include_block("htmlblocks/pricing-tables/cs-hosting-lin-v2.php"); ?>

                <a data-link-target="<?php echo $mypage->getMinPriceCheckoutLink(); ?>" data-section-button="<?php _e('Order Linux VPS Now');?>" data-button-styling="button highlight <?php echo $mypage->checkoutActivityClass($mypage->getCheapestLocation()); ?>" class="hidden"></a>

                <?php include_block("htmlblocks/pricing-tables/cs-all-plans-include.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-pricing -->



<script>

    (function($){
        $(document).ready(function(){

            function goToByScroll(id) {
                /*Used for scrolling to error spot*/
                
                id = id.replace("link", ""); // Remove "link" from the ID
                var deltaScroll = 0,
                    heightStickyHeader = $('.sticky-wrapper.is-sticky').height();

                if (heightStickyHeader > 0) {
                    deltaScroll = heightStickyHeader;
                }
                $('html,body').animate({scrollTop: $("#" + id).offset().top - deltaScroll},'slow');
            };


            $('#get-form').click(function(e){
                e.preventDefault();
                $(this).hide();
                $(this).parent().find('.contact-form').show('slow');
            });

            // check for form postprocess hash '..wcpf7..' and show form if some errors occured
            var linkHash = location.hash;

            if (linkHash.indexOf('wpcf7') > 0) {
                $('#get-form').hide();
                $('.contact-form').show();
                goToByScroll('contact-form');
            }

        })
    })(jQuery)

</script>            
        
      
        <section class="service-faq extra-pad" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/land-linux1-vps.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->


        <section class="service-cta extra-pad color-bg-style-grad1a">

            <div class="container">
                
                <div class="container-flex cta">
                    <div class="block-col width-md-2-3 cta-left">
                        <span class="block-title-xlarge-thin white t-ff-header t-mspaced"><?php _e('Looking for OpenVZ VPS?');?></span>
                    </div>
                    <div class="block-col width-md-1-3 cta-right">
                        <a href="/cheap-vps-hosting/" class="button xlarge ghost"><?php _e('Cheap VPS');?></a>
                    </div> 
                </div> 

            </div>

        </section> <!--end of .service-cta-->



    </section> <!-- end of .main -->

</article>


<?php

include_block("htmlblocks/hosting/speedtest_enabling_js.php");

universal_redirect_footer([
    'en' => $site_en_url.'/linux-vps/',
    'br' => $site_br_url.'/linux-vps/'
]);

get_footer();

?>
