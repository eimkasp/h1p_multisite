<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Landing: cPanel Cloud Servers
*/


$custom_fields = get_post_custom();
$contact_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

wp_enqueue_style('landings.style', get_template_directory_uri() . '/landings2/landing.css', ['style']);

wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'cs_lin');

/*cPanel price. cPanel is a product with id=129*/
$cPanel_data = $whmcs->getConfigurablePrice(129, []);
/*echo '<pre>';
var_dump($cPanel_data);
echo '</pre>';*/
$cPanel_price_mo = $cPanel_data[0]['price_monthly'];


/*Get Cloud IP config option pricing
-------------------------*/
// id=342

$cloud_hosting_config_options = $whmcs->getConfigurablePrices(342);
$cloud_hosting_ip = $cloud_hosting_config_options['IP']['options']['IP']['pricing']['monthly'];


// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing cloud-servers landing-cpanelcs">

    <header class="headline landing-cpanelcs">
        <div class="container adjust-vertical-center">

            <img class="m-t-40" src="<?php echo $images ?>landings2/images/header_icon_cpanel.svg" alt="cPanel Cloud Servers header image" width="105" height="105">
            <h1 class="page-title m-up-40">cPanel Cloud Server Hosting</h1>
            <div class="title-descr">Linux Cloud Servers powered with cPanel hosting control panel.</div>

            <div class="row center p-t-30">
                <a class="button highlight large" href="#plans" data-scrollto="plans">View Plans</a>
            </div>

        </div>


    </header> <!-- end of .headline.landing-cpanelvps -->



    <section class="main page-content">

        <section class="service-description extra-pad">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title p-b-20">What is cPanel?</h2>
                </div>

                <div class="container-flex as-row">

                    <div class="block-col p-b-40">
                        <p class="">cPanel is one of the most popular web hosting control panels on the market. This control panel is well-known and widely used for Linux-based hosting services, as it ensures easy web administration, contains a simplified layout and multiple management options only for <?php echo $cPanel_price_mo; ?> per month.</p>
                    </div> 

                    <div class="block-col width-lg-1-1 p-t-20 p-h-30 center">
                        <img class="scale" src="<?php echo $images; ?>landings2/images/cpanel_screens.png" alt="widely used cPanel">
                    </div>

                
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-description -->


        <section class="service-features-1 extra-pad-top border-top-separation">

            <div id="features-start" class="scroll-id-target" style="margin-top: 65px;"></div>

            <div class="container">

                <div class="section-header p-b-20">
                    <img class="p-b-10" src="<?php echo $images; ?>landings2/images/cpanel_logo.svg" alt="cPanel" width="240">
                    <h2 class="block-title">Why cPanel Cloud Server hosting?</h2>
                </div>

                <div class="container-flex features-list m-t-20 p-v-20">

                    <div class="block-col width-md-1-2 p-h-20 m-b-30 md-vert-pad container-flex">
                        <div class="inner-w-90">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_databases_orange.svg" alt="Databases" width="80">
                        </div>
                        <div class="inner-w-90-whatsleft">
                            <h3 class="title-paragraph-lower p-t-10 p-b-20">Databases</h3>
                            <p class="adjust-left land-color-style1">Store large amounts of data and limit access using MySQL and PostgreSQL databases.</p>
                        </div>
                    </div>
                    <div class="block-col width-md-1-2 p-h-20 m-b-30 md-vert-pad container-flex">
                        <div class="inner-w-90">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_mail_orange.svg" alt="Mail" width="80">
                        </div>
                        <div class="inner-w-90-whatsleft">
                            <h3 class="title-paragraph-lower p-t-10 p-b-20">Mail</h3>
                            <p class="adjust-left land-color-style1">Create email accounts, forwarders, and autoresponders as well as account and user-level filtering to manage email.</p>
                        </div>
                    </div>
                    <div class="block-col width-md-1-2 p-h-20 m-b-30 md-vert-pad container-flex">
                        <div class="inner-w-90">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_logs_orange.svg" alt="Logs" width="80">
                        </div>
                        <div class="inner-w-90-whatsleft">
                            <h3 class="title-paragraph-lower p-t-10 p-b-20">Logs</h3>
                            <p class="adjust-left land-color-style1">Know your audience and track your website's performance using Webalizer and AWStats.</p>
                        </div>
                    </div>
                    <div class="block-col width-md-1-2 p-h-20 m-b-30 md-vert-pad container-flex">
                        <div class="inner-w-90">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_security_orange.svg" alt="Security" width="80">
                        </div>
                        <div class="inner-w-90-whatsleft">
                            <h3 class="title-paragraph-lower p-t-10 p-b-20">Security</h3>
                            <p class="adjust-left land-color-style1">Configure passwords, IP address denials, SSL/TLS, & GnuPG key settings to restrict access.</p>
                        </div>
                    </div>
                    <div class="block-col width-md-1-2 p-h-20 m-b-30 md-vert-pad container-flex">
                        <div class="inner-w-90">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_files_orange.svg" alt="Files" width="80">
                        </div>
                        <div class="inner-w-90-whatsleft">
                            <h3 class="title-paragraph-lower p-t-10 p-b-20">Files</h3>
                            <p class="adjust-left land-color-style1">Edit and back up files and folders while monitoring your website's disk space usage.</p>
                        </div>
                    </div>
                    <div class="block-col width-md-1-2 p-h-20 m-b-30 md-vert-pad container-flex">
                        <div class="inner-w-90">
                            <img class="" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_domains_orange.svg" alt="Domains" width="80">
                        </div>
                        <div class="inner-w-90-whatsleft">
                            <h3 class="title-paragraph-lower p-t-10 p-b-20">Domains</h3>
                            <p class="adjust-left land-color-style1">Set up subdomains, addon domains, parked domains, and redirects to point visitors in the right direction.</p>
                        </div>
                    </div>

                </div> <!-- end of .container-flex -->

            </div> <!-- end of .container -->
            
        </section> <!-- end of .service-features-1 -->


        <section class="flexibility-options extra-pad border-top-separation">
            
            <div class="container">

                <div class="section-header p-b-60">
                    <h2 class="block-title">Need more flexibility?</h2>
                    <span class="title-follow-text">Try our scalable KVM VPS servers with API, custom ISO and more.</span>
                </div>

                <div class="container-flex block-offer1-container">
                    <div class="block-col block-offer1">
                        <a class="wrapper-content" href="/windows-cloud-server/">
                            <img class="push-left m-r-20" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/header_icon_win_blue.svg" alt="Windows KVM VPS icon" width="90">
                            <div class="push-left text-wrap">
                                    <h3 class="title-paragraph-lower bold">Windows KVM VPS</h3>
                                    <span class="t-mshrinked">from <?php echo $mypage->getServiceMinPrice('cs_win') ?>/mo</span>
                            </div>
                        </a>
                    </div>
                    <div class="block-col block-offer1">
                        <a class="wrapper-content" href="/cloud-server-linux/">
                            <img class="push-left m-r-20" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/header_icon_linux_blue.svg" alt="Linux KVM VPS icon" width="90">
                            <div class="push-left text-wrap">
                                <h3 class="title-paragraph-lower bold">Linux KVM VPS</h3>
                                <span class="t-mshrinked">from <?php echo $mypage->getServiceMinPrice('cs_lin') ?>/mo</span>
                            </div>
                        </a>
                    </div>
                </div> 

            </div>

        </section>

        <section class="custom-installation-guide extra-pad-top color-bg-style5">
            
            <div class="container">

                <div class="container-flex p-t-20 p-b-30">
                    <div class="block-col width-md-1-3 center p-b-20">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cpanel_guide_block.png" alt="cPanel installation steps article icon" width="300">
                    </div> 
                    <div class="block-col width-md-2-3 p-h-40 center-sm push-flex vertical-center">
                        <h3 class="block-title-small">Curious about the whole process?</h3>
                        <p>We've got you covered! Take a look at the steps that will get you going with cPanel Cloud Server in no time!</p>
                        <p class="m-v-30 descr-text-feature size-18 bold">
                            Your guide to <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1356/0/how-to-setup-cpanel-on-your-linux-cloud-server">cPanel Cloud Server</a>
                        </p>  
                    </div>
                </div> 

            </div> <!-- end of .container -->


        </section> <!-- end of .custom-installation-guide -->


        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>


        <section class="service-features extra-pad color-bg-style-grad1" data-section-title="<?php _e('Features');?>">

            <div id="features" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title white">cPanel Cloud Server Features</h2>
                </div>

                <div class="four-col-row-biface-cards">

                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_flex-scalability_blue.svg" alt="<?php _e('Flexible Windows Cloud Server feature icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Flexible Scaling')?></h3>
                                <p class="white">Scale your cPanel Cloud Server on demand! Upgrade your resources just in a few clicks.</p>
                            </div>
                        </div>
                    </div>
                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_api_blue.svg" alt="<?php _e('Windows Cloud Server powerful API icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Powerful API')?></h3>
                                <p class="white">Directly access web control panel functionality and benefit from advanced Cloud networking features.</p>
                            </div> 
                        </div>
                    </div>

                </div>
                <div class="four-col-row-biface-cards p-b-40">

                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_custom-os_blue.svg" alt="<?php _e('Windows Cloud Server custom ISO feature icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Custom ISO')?></h3>
                                <p class="white">Custom ISO allows you to mount configured image on your instance and run through the boot & setup process as you would on a bare metal server.</p>
                            </div> 
                        </div>
                    </div>
                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_auto-backups_blue.svg" alt="<?php _e('Scheduled backups for Windows Cloud Servers feature icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Manage Backups')?></h3>
                                <p class="white">Schedule daily, weekly or monthly automated backups. You can also create backups manually whenever you have to!</p>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="four-col-row-biface-cards p-b-40">

                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_full-root-access_blue.svg" alt="<?php _e('Full root access icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Full Root Access')?></h3>
                                <p class="white">Get full flexibility and control with root permissions on Linux CentOS distributions with pre-installed cPanel control panel.</p>
                            </div> 
                        </div>
                    </div>
                    <div class="block-col larger">
                        <div class="main-face push-flex horizontal-center on-sm-to-vertical">
                            <div class="feature-image-wrapper m-b-30-in-sm">
                                <img class="feature-image" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>landings2/images/cs_simple-control-panel_blue.svg" alt="<?php _e('Simple control panel icon')?>">
                            </div>
                            <div class="feature-title-wrapper text-left text-center-on-sm">
                                <h3 class="white p-b-10"><?php _e('Simple Control Panel')?></h3>
                                <p class="white">Clean cPanel Cloud Server control panel is designed to provide you with all the necessary information and management capabilities on the fly.</p>
                            </div>
                        </div>
                    </div>

                </div> 

            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->


        <section class="service-locations extra-pad-top color-bg-style2 no-zoom" data-section-title="<?php _e('Locations');?>">

            <div id="locations" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Worldwide Server Locations')?></h2>
                        <p><?php _e('Multiple premium data centers ensure high connectivity and better reach.'); ?></p>
                </div>

                <?php include_block("htmlblocks/locations-services.php", array('type' => 'cloud', 'speedtest' => true )); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-locations -->


        <section class="service-pricing extra-pad-top" data-section-title="<?php _e('Plans');?>">

            <div id="plans" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header p-b-0">
                    <h2 class="block-title">cPanel Cloud Server Pricing</h2>
                </div>

                <!--Special insert via JS-->

                <?php include_block("htmlblocks/pricing-tables/cs-hosting-lin-v2.php"); ?>

                <a data-link-target="<?php echo $mypage->getMinPriceCheckoutLink(); ?>" data-section-button="<?php _e('Order Now');?>" data-button-styling="button highlight <?php echo $mypage->checkoutActivityClass($mypage->getCheapestLocation()); ?>" class="hidden"></a>

                <?php include_block("htmlblocks/pricing-tables/cs-all-plans-include.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-pricing -->



<script>

    (function($){
        $(document).ready(function(){

            <?php 
                /*
                 * Enhancement regarding cPanel constraints
                 */

                /* not this time
                
                $('.plans-pricing-table.cs .plan-col-1 button.cs-checkout-link').text("<?php _e('Unavailable');?>");
                $('.plans-pricing-table.cs .plan-col-1').append('<div class="plan-unavailable"></div>');
                */
            ?>

            $('.pricing-anouncement-before-table').html('<p><img style="vertical-align:middle;padding-bottom:3px;padding-right:3px" width="30" height="30" src="<?php echo $images; ?>landings2/images/cp_logo.svg" alt="cPanel"> Pre-installed cPanel template is recommended with LIN2 and higher plans to ensure sufficient resources.</p>');

            $('.location.loc-us-east .popup-action').remove();
            $('.location.loc-de .popup-action').remove();

            <?php 
                /*This will run prior to Sticky menu rendering, so Order button will get an updated link*/
            ?>
            var link = $('.service-pricing').find('a').attr('data-link-target');
            newlink = link.replace('plan=lin1', 'plan=LIN2');
            $('.service-pricing').find('a').attr('data-link-target', newlink);

        })
    })(jQuery)

</script>            
        
      
        <section class="service-faq extra-pad" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/land-cpanel-cs.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->


        <section class="service-cta extra-pad color-bg-style-grad1a">

            <div class="container">
                
                <div class="container-flex cta">
                    <div class="block-col width-md-2-3 cta-left">
                        <span class="block-title-xlarge-thin white t-ff-header t-mspaced"><?php _e('Looking for something else?');?></span>
                    </div>
                    <div class="block-col width-md-1-3 cta-right">
                        <a href="/vps-hosting/" class="button xlarge ghost"><?php _e('OpenVZ VPS');?></a>
                    </div> 
                </div> 

            </div>

        </section> <!--end of .extra-offer-cta-->



    </section> <!-- end of .main -->

</article>


<?php

include_block("htmlblocks/hosting/speedtest_enabling_js.php");

universal_redirect_footer([
    'en' => $site_en_url.'/cpanel-cloud-server/',
    'br' => $site_br_url.''
]);

get_footer();

?>
