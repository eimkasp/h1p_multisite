<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: About Us
*/

// Scripts for Horizontal-timeline
// https://codyhouse.co/gem/horizontal-timeline/



if(get_locale() == 'pt_BR') {
    $lang_code = 'pt-BR';
} else {
    $lang_code = 'en';
}
wp_enqueue_script('recaptcha', 'https://www.google.com/recaptcha/api.js?&hl=' . $lang_code, [], false, true);
wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);

$mypage = PageData::getInstance($config, NULL);

get_header();

?>

<article class="page about-us">

    <header class="headline about-us">
        <div class="container adjust-vertical-center">
            <h1 class="page-title"><?php _e('About Us')?></h1>
            <div class="title-descr"><?php _e('Our company was carefully built to create high-value hosting services and true long-term relationships with our clients and supporters.')?></div>

        </div>
    </header> <!-- end of .headline.about-us -->

    <section class="main page-content">


        <section class="story extra-pad-top">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Our Story')?></h2>
                </div>

                <div class="layout-row two-col-row">
                    <div class="block-col"><?php _e('Founded in 2008, our company was carefully built to create high-value hosting services and true long-term relationships with our clients and supporters. The values we own today – reliability, tolerance, partnership - were rooted by our customers over the years of successful collaboration.'); ?></div>
                    <div class="block-col"><?php _e('We currently have 5 server locations in Brazil, Germany, South Africa and the United States. Our team strives to ensure immaculate network stability, solid server reliability and excellent customer care.'); ?></div>
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .story -->


        <section class="timeline">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Major Events')?></h2>
                </div>

                <ul class="timeline-line">
                    <li class="milestone">
                        <div class="title">2008</div>
                        <div class="description">
                            <ul>
                                <li><?php printf(__('1st data center in %s'), __('London, UK'));?></span></li>
                                <li><?php printf(__('1st data center in %s'), __('Vilnius, LT'));?></li>
                            </ul>
                        </div>
                    </li>
                    <li class="milestone">
                        <div class="title">2009</div>
                        <div class="description w300">
                            <ul>
                                <li><?php printf(__('New data center in %s'), __('Amsterdam, NL'));?></li>
                                <li><?php printf(__('New data center in %s'), __('Frankfurt, DE'));?></li>
                                <li><?php printf(__('New data center in %s'), __('Singapore, SG'));?></li>
                            </ul>
                        </div>
                    </li>
                    <li class="milestone">
                        <div class="title">2010</div>
                        <div class="description w150">
                            5,000 <?php _e('clients')?>
                        </div>
                    </li>

                    <li class="milestone">
                        <div class="title">2011</div>
                        <div class="description w200">
                            <ul>
                                <li><?php _e('CDN implementation');?></li>
                                <li><?php _e('Extra-Care support service');?></li>
                            </ul>
                        </div>
                    </li>

                    <li class="milestone">
                        <div class="title">2012</div>
                        <div class="description w350">
                            <?php _e('New services – VPS and dedicated servers')?>
                        </div>
                    </li>

                    <li class="milestone">
                        <div class="title">2013</div>
                        <div class="description w300">
                            <ul>
                                <li><?php printf(__('New data center in %s'), __('Santiago, CL'));?></li>
                                <li><?php printf(__('New data center in %s'), __('Johannesburg, ZA'));?></li>
                            </ul>
                        </div>
                    </li>

                    <li class="milestone">
                        <div class="title">2014</div>
                        <div class="description w300">
                            <ul>
                                <li><?php printf(__('New data center in %s'), __('Sao Paulo, BR'));?></li>
                                <li><?php printf(__('New data center in %s'), __('Chicago, US'));?></li>
                                <li><?php printf(__('New data center in %s'), __('Los Angeles, US'));?></li>
                            </ul>
                        </div>
                    </li>

                    <li class="milestone">
                        <div class="title">2015</div>
                        <div class="description w150">
                            40,000 <?php _e('clients');?>
                        </div>
                    </li>

                    <li class="milestone">
                        <div class="title">2016</div>
                        <div class="description w150">
                            <ul>
                                <li><?php _e('New services - Cloud Servers ßeta');?></li>
                            </ul>
                        </div>
                    </li>

                    <li class="milestone">
                        <div class="title">2017</div>
                        <div class="description w300">
                            <ul>
                                <li>65,000 <?php _e('clients');?></li>
                                <li><?php printf(__('Cloud Servers launch in %s'), __('São Paulo, BR'));?></li> 
                                <li><?php printf(__('Cloud Servers launch in %s'), __('Johannesburg, ZA'));?></li> 
                            </ul>
                        </div>
                    </li>

                </ul>

            </div> <!-- end of .container -->

        </section> <!-- end of .timeline -->


        <section class="team">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Meet our team!')?></h2>
                </div>

                <div class="layout-row four-col-row-team">
                    <div class="block-col">
                        <div class="pic vingri"></div>
                        <h4>Vincentas Grinius</h4>
                        <span><?php _e('Founder & CEO');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic andkaz"></div>
                        <h4>Andrius Kazlauskas</h4>
                        <span><?php _e('Founder');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic aiszen"></div>
                        <h4>Aistis Zenkevičius</h4>
                        <span><?php _e('CTO');?></span>
                    </div>


                    <div class="block-col">
                        <div class="pic vikpod"></div>
                        <h4>Viktorija Poderskienė</h4>
                        <span><?php _e('CMO');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic povjau"></div>
                        <h4>Povilas Jauniškis</h4>
                        <span><?php _e('CFO');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic donfet"></div>
                        <h4>Donatas Fetingis</h4>
                        <span><?php _e('Head of Support');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic jursad"></div>
                        <h4>Juras Sadauskas</h4>
                        <span><?php _e('Sales Executive');?></span>
                    </div>


                    <div class="block-col">
                        <div class="pic nerjan"></div>
                        <h4>Nerijus Jankauskas</h4>
                        <span><?php _e('Sales Manager');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic gedbut"></div>
                        <h4>Gediminas Butkus</h4>
                        <span><?php _e('Sales Manager');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic vitjar"></div>
                        <h4>Vitalis Jarutkinas</h4>
                        <span><?php _e('Sales Manager');?></span>
                    </div>


                    <div class="block-col">
                        <div class="pic linzil"></div>
                        <h4>Linas Žilinskas</h4>
                        <span><?php _e('Head of Development');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic zilvai"></div>
                        <h4>Žilvinas Vaičkus</h4>
                        <span><?php _e('System Administrator');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic aurmat"></div>
                        <h4>Aurelija Matulytė</h4>
                        <span><?php _e('Office Manager');?></span>
                    </div>


                    <div class="block-col">
                        <div class="pic linkas"></div>
                        <h4>Lina Kasiliauskienė</h4>
                        <span><?php _e('Affiliate Marketing Manager');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic dailuk"></div>
                        <h4>Dainius Lukšta</h4>
                        <span><?php _e('Operations Manager');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic renpin"></div>
                        <h4>Renato Pinheiro</h4>
                        <span><?php _e('Customer Support Specialist');?></span>
                    </div>


                    <div class="block-col">
                        <div class="pic alvlim"></div>
                        <h4>Álvaro Lima</h4>
                        <span><?php _e('Customer Support Specialist');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic davcar"></div>
                        <h4>David Carmo</h4>
                        <span><?php _e('Customer Support Specialist');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic vytkit"></div>
                        <h4>Vytautas Kitovas</h4>
                        <span><?php _e('Customer Support Specialist');?></span>
                    </div>


                    <div class="block-col">
                        <div class="pic raista"></div>
                        <h4>Raimundas Stankevičius</h4>
                        <span><?php _e('Customer Support Specialist');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic ugnsku"></div>
                        <h4>Ugnius Skučas</h4>
                        <span><?php _e('Customer Support Specialist');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic deiand"></div>
                        <h4>Deividas Andrikis</h4>
                        <span><?php _e('Junior Support Engineer');?></span>
                    </div>


                    <div class="block-col">
                        <div class="pic jonbun"></div>
                        <h4>Jonas Bunevičius</h4>
                        <span><?php _e('System Administrator');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic darkas"></div>
                        <h4>Darius Kasparavičius</h4>
                        <span><?php _e('System Administrator');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic vytjon"></div>
                        <h4>Vytautas Jonaitis</h4>
                        <span><?php _e('System Administrator');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic andmad"></div>
                        <h4>Andrius Madeliauskas</h4>
                        <span><?php _e('Junior System Administrator');?></span>
                    </div>


                    <div class="block-col">
                        <div class="pic modjon"></div>
                        <h4>Modestas Jonaitis</h4>
                        <span><?php _e('Network Engineer');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic darban"></div>
                        <h4>Darius Banionis</h4>
                        <span><?php _e('Web Developer');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic edgsko"></div>
                        <h4>Edgaras Skorupskas</h4>
                        <span><?php _e('System Developer');?></span>
                    </div>


                    <div class="block-col">
                        <div class="pic tomlyg"></div>
                        <h4>Tomas Lygutas</h4>
                        <span><?php _e('System Developer');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic deinor"></div>
                        <h4>Deividas Norkaitis</h4>
                        <span><?php _e('Quality Assurance Engineer');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic sauvik"></div>
                        <h4>Saulius Vikerta</h4>
                        <span><?php _e('Technical Designer');?></span>
                    </div>


                    <div class="block-col">
                        <div class="pic julslu"></div>
                        <h4>Julius Slušnys</h4>
                        <span><?php _e('Web Designer');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic gieces"></div>
                        <h4>Giedrius Čėsna</h4>
                        <span><?php _e('Marketing Analyst');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic gietus"></div>
                        <h4>Giedrė Tūskienė</h4>
                        <span><?php _e('Financial Analyst');?></span>
                    </div>
                    <div class="block-col">
                        <div class="pic dovkal"></div>
                        <h4>Dovainis Kalėda</h4>
                        <span><?php _e('Senior Marketing Specialist');?></span>
                    </div>


                    <div class="block-col">
                        <div class="pic dovsob"></div>
                        <h4>Dovydas Šoblinskas</h4>
                        <span><?php _e('Junior SEO Manager');?></span>
                    </div>

                    

                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .team -->


        <section class="service-cta color-bg-style2 p-v-20">

            <div class="container">

                <div class="row block-of-two-8-4 cta">
                    <div class="block-col">
                        <h2 class="push-left"><?php _e('We hire!'); ?></h2>
                        <span class="push-left"><?php _e('Become a part of our team and work directly to support and expand our high-performance services worldwide.');?></span>
                    </div>
                    <div class="block-col push-right">
                        <a href="<?php echo $config['links']['careers']; ?>" class="button xlarge ghost push-right"><?php _e('Join Us')?></a>
                    </div>
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-cta -->



        <section class="service-locations extra-pad-top color-bg-style1 on-light-bg no-zoom">

            <div class="container">

                <div class="section-header m-b-50">
                    <h2 class="block-title"><?php _e('Worldwide Server Locations')?></h2>
                </div>

                <?php include_block("htmlblocks/locations-services.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-locations -->



        <section class="membership extra-pad">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Memberships')?></h2>
                    <span class="title-follow-text"><?php _e('We are proud members of multiple international and local organizations.'); ?></span>
                </div>

                <div class="layout-row four-col-row-flex">
                    <div class="block-col p-b-20">
                        <div class="frame">
                            <div class="pic-organization decix"></div>
                        </div>
                    </div>

                    <div class="block-col p-b-20">
                        <div class="frame">
                            <div class="pic-organization ixbr"></div>
                        </div>
                    </div>

                    <div class="block-col p-b-20">
                        <div class="frame">
                            <div class="pic-organization ispa"></div>
                        </div>
                    </div>

                    <div class="block-col p-b-20">
                        <div class="frame">
                            <div class="pic-organization zainx"></div>
                        </div>
                    </div>

                    <div class="block-col p-b-20">
                        <div class="frame">
                            <div class="pic-organization afrinic"></div>
                        </div>
                    </div>

                    <div class="block-col p-b-20">
                        <div class="frame">
                            <div class="pic-organization ripencc"></div>
                        </div>
                    </div>

                    <div class="block-col p-b-20">
                        <div class="frame">
                            <div class="pic-organization lacnic"></div>
                        </div>
                    </div>

                    <div class="block-col p-b-20">
                        <div class="frame">
                            <div class="pic-organization arin"></div>
                        </div>
                    </div>

                </div> <!-- end of .four-col-row-flex -->

            </div> <!-- end of .container -->

        </section> <!-- end of .why-us -->


        <section class="contacts color-bg-style1">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Contacts')?></h2>
                </div>

                <div class="horizontal-tabs">

                    <div class="tabs">
                        <ul>
                            <li class="tab"><a data-tab-link="contacts.uk" href="#" data-prevent class="active"><?php _e('London')?>, <?php _e('UK')?></a></li>
                            <li class="tab"><a data-tab-link="contacts.lt" href="#" data-prevent><?php _e('Kaunas')?>, <?php _e('LT')?></a></li>
                        </ul>
                    </div>

                    <div class="location-content">

                        <div class="contact-tabs" data-tabs="contacts">

                            <div data-tab-id="uk" class="contact-tab uk active">

                                <h3 class="tab-title" data-tab-link="contacts.uk"><?php _e('London')?> - <?php _e('United Kingdom')?></h3>

                                <div class="tab-content">

                                    <p>
                                        <?php _e('Digital Energy Technologies Limited')?><br/>
                                        <?php printf(__('Registration №: %s'), '06848949')?><br/>
                                        <?php printf(__('VAT №: %s'), 'GB989590342')?><br/>
                                        <?php _e('Registered In England & Wales')?>
                                    </p>

                                    <h6 class="title"><?php _e('Address')?></h6>
                                    <p><?php echo str_replace('%s', '<br/>', __('207 Regent Street %sLondon W1B 3HH %sUnited Kingdom'))?></p>
                                </div>

                            </div>

                            <div data-tab-id="lt" class="contact-tab lt">

                                <h3 class="tab-title" data-tab-link="contacts.lt"><?php _e('Kaunas')?> - <?php _e('Lithuania')?></h3>

                                <div class="tab-content">

                                    <p>
                                        <?php _e('Digital Energy Technologies Limited')?><br/>
                                        <?php printf(__('Registration №: %s'), '06848949')?><br/>
                                        <?php printf(__('VAT №: %s'), 'GB989590342')?><br/>
                                        <?php _e('Registered In England & Wales')?>
                                    </p>

                                    <h6 class="title"><?php _e('Address')?></h6>
                                    <p><?php echo str_replace('%s', '<br/>', __('Savanorių pr. 109 %sKaunas LT-44146 %sLithuania'))?></p>
                                </div>

                            </div>

                        </div>

                        <div class="contact-form">

                            <div class="form-title"><?php _e('Have a question?')?></div>
                            <div class="form-info">* <?php _e('fields are required')?></div>

                            <?php echo do_shortcode('[contact-form-7 id="1828" title="Contact Us" html_class="wpcf7-form captcha"]');?>

                        </div>

                    </div>

                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .contacts -->


    </section> <!-- end of section .main -->

</article>

<?php 

universal_redirect_footer([
    'en' => $site_en_url.'/about-us/',
    'br' => $site_br_url.'/sobre-host1plus/'
]);

get_footer(); 

?>
