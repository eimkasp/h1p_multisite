<?php
/**
* @package WordPress
* @subpackage h1p_v5
*
* Template Name: Logos and Badges
*/

/*Image resources url*/
$images = $site_current_url . '/wp-content/themes/h1p_v5/img/';
$branding_images = $images . 'branding/';

get_header();

?>

<article class="page logos-badges no-pad">

    <header class="headline logos-badges">
        <div class="container adjust-vertical-center">
            <h1 class="page-title"><?php _e('Logos & Badges')?></h1>
            <div class="title-descr max-w-800"><?php _e('Download our logos or badges to display on your website or press and learn how to use them.')?></div>
            <div class="row center m-t-30">
                <a href="<?php echo $branding_images ?>Host1Plus_logo_eps_svg_png.zip" class="button primary orange large"><?php _e('Download Full Pack');?></a>
            </div>
        </div>
    </header> <!-- end of .headline.logos-badges -->

    <section class="main page-content">


        <section class="assets extra-pad">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Download Assets')?></h2>
                </div>
                <p class="max-w-800 center"><?php _e('Let your visitors know that your website is proudly hosted by Host1Plus.  Choose the logos and badges below or download the full pack.');?></p>

                <div class="block-separator-extra"></div>

                <div class="row four-col-row">

                    <div class="block-col center">
                        <span class="badge b1-1"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b1-2"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b1-3"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_white_letters.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_white_letters.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_white_letters.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b1-4"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_all_white.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_all_white.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_all_white.png">PNG</a>
                        </span>
                    </div>

                </div>

                <div class="row four-col-row">

                    <div class="block-col">
                        <span class="badge b2-1"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_icon.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_icon.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_icon.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b2-2"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_icon.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_icon.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_icon.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b2-3"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_icon.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_icon.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_icon.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b2-4"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_icon_white.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_icon_white.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_icon_white.png">PNG</a>
                        </span>
                    </div>

                </div>

                <div class="row four-col-row">

                    <div class="block-col">
                        <span class="badge b3-1"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_hosted_by.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_hosted_by.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_hosted_by.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b3-2"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_hosted_by.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_hosted_by.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_hosted_by.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b3-3"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_hosted_by_white_letters.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_hosted_by_white_letters.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_hosted_by_white_letters.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b3-4"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_hosted_by_all_white.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_hosted_by_all_white.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_hosted_by_all_white.png">PNG</a>
                        </span>
                    </div>

                </div>

                <div class="row four-col-row">

                    <div class="block-col">
                        <span class="badge b4-1"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_hospedado_pela.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_hospedado_pela.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_hospedado_pela.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b4-2"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_hospedado_pela.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_hospedado_pela.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_hospedado_pela.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b4-3"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_hospedado_pela_white_letters.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_hospedado_pela_white_letters.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_hospedado_pela_white_letters.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b4-4"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_hospedado_pela_all_white.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_hospedado_pela_all_white.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_hospedado_pela_all_white.png">PNG</a>
                        </span>
                    </div>

                </div>

                <div class="row four-col-row">

                    <div class="block-col">
                        <span class="badge b5-1"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_driven_by_dev.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_driven_by_dev.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_driven_by_dev.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b5-2"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_driven_by_dev.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_driven_by_dev.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_driven_by_dev.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b5-3"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_driven_by_dev_white_letters.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_driven_by_dev_white_letters.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_driven_by_dev_white_letters.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b5-4"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_driven_by_dev_all_white.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_driven_by_dev_all_white.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_driven_by_dev_all_white.png">PNG</a>
                        </span>
                    </div>

                </div>

                <div class="row four-col-row">

                    <div class="block-col">
                        <span class="badge b6-1"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_insp_por_desenv.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_insp_por_desenv.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_insp_por_desenv.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b6-2"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_insp_por_desenv.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_insp_por_desenv.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_insp_por_desenv.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b6-3"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_insp_por_desenv_white_letters.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_insp_por_desenv_white_letters.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_insp_por_desenv_white_letters.png">PNG</a>
                        </span>
                    </div>
                    <div class="block-col">
                        <span class="badge b6-4"></span>
                        <span class="download-links">
                            <a href="<?php echo $branding_images ?>host1plus_logo_insp_por_desenv_all_white.eps">EPS</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_insp_por_desenv_all_white.svg">SVG</a>
                            |
                            <a href="<?php echo $branding_images ?>host1plus_logo_insp_por_desenv_all_white.png">PNG</a>
                        </span>
                    </div>

                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .assets -->

        <section class="guide extra-pad color-bg-style1">

            <div class="container">

                <div class="section-header p-b-0">
                    <h2 class="block-title"><?php _e('Identity Guide')?></h2>
                </div>

                <div class="block-title-strikethrough-close"></div>

                <div class="row block-of-two-4-8">
                    <div class="block-col">
                        <h3><?php _e('Clear Space');?></h3>
                        <p><?php _e('When using the logo, make sure you maintain required size and clear space around it to ensure its visibility and impact on your website.');?><br>
                        <?php _e('Displayed X size is always equal to the height of letter “s” and reflects a minimum clear space. However, its size may be increased proportionally.');?></p>
                    </div>
                    <div class="block-col images">
                        <img src="<?php echo $branding_images;?>host1plus_logo_space.png" alt="Spacing">
                    </div>
                </div>

                <div class="block-title-strikethrough-close"></div>

                <div class="row block-of-two-4-8">
                    <div class="block-col">
                        <h3><?php _e('Sizing on Screen');?></h3>
                        <p><?php _e('On screen, the minimum logo width should be 110px. For 72PPI screen density, the optimum width is 160px.');?>
                        <?php _e('The displayed minimum sizes do not include clear space.');?><br>
                        <?php _e('If your display screen has higher PPI, minimum sizes should be adapted proportionally.');?></p>
                    </div>
                    <div class="block-col images">
                        <img src="<?php echo $branding_images;?>host1plus_logo_onscreen.png" alt="On screen application">
                    </div>
                </div>

                <div class="block-title-strikethrough-close"></div>

                <div class="row block-of-two-4-8">
                    <div class="block-col">
                        <h3><?php _e('Sizing for Print');?></h3>
                        <p><?php _e('For print, always maintain the minimum width of 30mm and optimal size of 45mm for A4.');?><br>
                        <?php _e('Please note, that displayed width does not include clear spaces and should be adapted proportionally for different print sizes.');?></p>
                    </div>
                    <div class="block-col images">
                        <img src="<?php echo $branding_images;?>host1plus_logo_forprint.png" alt="For print application">
                    </div>
                </div>

                <div class="block-title-strikethrough-close"></div>

                <div class="row center m-t-30">
                    <a href="<?php echo $branding_images ?>Host1Plus_logo_eps_svg_png.zip" class="button primary orange large"><?php _e('Download Full Pack');?></a>
                </div>

            </div>

        </section> <!-- end of .guide -->


    </section> <!-- end of .main -->

</article>

<?php

universal_redirect_footer([
    'en' => $site_en_url.'/branding/',
    'br' => $site_br_url.'/nossa-marca/'
]);

get_footer(); 

?>
