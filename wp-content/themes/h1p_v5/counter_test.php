<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Counter test
*/

wp_enqueue_script('soon', get_template_directory_uri() . '/js/vendor/soon.min.js', ['jquery'], false, true);
wp_enqueue_style('oswald-font', 'https://fonts.googleapis.com/css?family=Oswald:700', false);


global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$custom_fields = get_post_custom();
$abuse_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

get_header();

?>

<article class="page abuse">

    <header class="headline office">
        <div class="container adjust-vertical-center">
            <h1 class="page-title">Counter test page</h1>
            <div class="title-descr">Some demo text just to demonstrate.</div>

            <div class="soon" id="cloud-servers-counter"
                 data-due="2016-08-31T12:00:00"
                 data-layout="group tight label-uppercase label-small"
                 data-format="d,h,m,s"
                 data-separator="."
                 data-face="flip color-light corners-sharp"
                 data-labels-days="<?php _e('Days'); ?>"
                 data-labels-hours="<?php _e('Hours'); ?>"
                 data-labels-minutes="<?php _e('Minutes'); ?>"
                 data-labels-seconds="<?php _e('Seconds'); ?>">
            </div>

        </div>
    </header> <!-- end of .headline.careers -->

    <section class="main page-content">


        <section class="positions extra-pad-top">

            <div class="container">

            </div>

        </section>

    </section>

</article>

<?php get_footer(); ?>
