<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Reseller Hosting
*/

wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);
wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);

global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'res');

// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>

<article class="page landing reseller-hosting">

    <header class="headline reseller-hosting">
        <div class="container adjust-vertical-center">

            <h1 class="page-title"><?php _e('Reseller Hosting')?> <span class="highlight"><?php _e('from')?> <?php echo $mypage->getMinPrice(); ?></span></h1>
            <div class="title-descr"><?php _e('Easily customizable plans for your business to grow.')?></div>

        </div>
    </header> <!-- end of .headline.reseller-hosting -->

    <div class="reseller-promo-withheader"></div>

    <section class="main page-content">


        <section class="service-pricing extra-pad-top" data-section-button="<?php _e('View Plans');?>">

            <div class="scroll-id-target" id="plans-table"></div>

            <div class="reseller-promo-section-wide"></div>

            <div class="container">

                <div class="scroll-id-target" id="plans-table"></div>

                <div class="reseller-promo-container-wide"></div>

                <div>

                    <?php include_block("htmlblocks/pricing-tables/reseller-hosting-v2.php"); ?>

                </div>

                <span class="title-follow-text center m-t-30">
                    <?php echo sprintf(_x_lc('Save %1$s up to 20%% %2$s for longer billing cycles!'), '<span class="highlight has-tooltip" data-tooltip="billing-cycle-savings">', '</span>');?>
                    <div class="triangle"></div>
                    <div class="tooltip-body style2 w-600" data-tooltip-content="billing-cycle-savings"><p><?php _e('Save up to 20% with longer billing cycles ranging from 3 to 24 months. The discount increases according to the length of the billing cycle.'); ?></p></div>
                </span>

                <div class="block-title-strikethrough">
                    <h3 class="block-title"><?php _e('All plans include')?></h3>
                </div>

                <div class="container-flex">
                    <div class="block-fluid">
                    <ul class="features-list checklist orange">
                        <li><?php _e('Free dedicated IP')?></li>
                        <li><?php _e('Unlimited domains')?></li>
                        <li><?php _e('Unlimited sub-domains')?></li>
                    </ul>
                    </div>
                    <div class="block-fluid">
                        <ul class="features-list checklist orange">
                            <li><?php _e('Unlimited databases')?></li>
                            <li><?php _e('DirectAdmin & cPanel')?></li>
                            <li><?php _e('Unlimited email accounts')?></li>
                        </ul>
                    </div>
                    <div class="block-fluid">
                        <ul class="features-list checklist orange">

                            <li><?php _e('Application installer')?></li>
                            <li><?php _e('Backup management')?></li>
                            <li><?php _e('1Gbit network uplink')?></li>
                        </ul>
                    </div>
                </div>

                <div class="block-title-strikethrough-close"></div>

            </div>

        </section> <!-- end of .service-pricing -->


        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>


        <section class="service-locations extra-pad-top color-bg-style2 no-zoom" data-section-title="<?php _e('Locations');?>">

            <div id="locations" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Reseller Hosting'); echo ' '; _e('Locations');?></h2>
                        <p><?php _e('Multiple premium data centers ensure high connectivity and better reach.'); ?></p>
                </div>

                <?php include_block("htmlblocks/locations-services.php", array('type' => 'reseller')); ?>

            </div> <!-- end of .container -->

        </section>


        <section class="service-features extra-pad" data-section-title="<?php _e('Features');?>">

            <div id="features" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Reseller Hosting Features')?></h2>
                </div>

                <div class="container-flex features-list" style="justify-content: space-around;">

                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/features/feat-intspeed.svg" alt="High Internet Speed" width="140" height="140">
                        <h3><?php _e('High Internet Speed')?></h3>
                        <p><?php _e('Our powerful network delivers high connection speed providing you ultra-fast data downloads and uploads.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/features/feat-accounts.svg" alt="Unlimited Accounts" width="140" height="140">
                        <h3><?php _e('Unlimited Accounts')?></h3>
                        <p><?php _e('Create unlimited service accounts and set up as many domains as you or your clients need.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/features/feat-ip.svg" alt="Dedicated IP" width="140" height="140">
                        <h3><?php _e('Free Dedicated IP')?></h3>
                        <p><?php _e('Preserve your online identity! All reseller hosting plans include a dedicated IP for free.')?></p>
                    </div>

                </div>
                <div class="container-flex features-list" style="justify-content: center;">

                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/features/feat-disk.svg" alt="Generous Disk Space" width="140" height="140">
                        <h3><?php _e('Generous Disk Space')?></h3>
                        <p><?php _e('Get up to 150 GB and divide your resources into as many pieces as you wish.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/features/feat-locations.svg" alt="2 Locations" width="140" height="140">
                        <h3><?php _e('2 Locations')?></h3>
                        <p><?php _e('Starting small but growing fast? Point your business to any location to expand to new markets.')?></p>
                    </div>

                </div>

                <div class="row center">
                    <a href="#plans-table" class="button primary orange large" data-scrollto="plans-table"><?php _e('Compare Plans');?></a>
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->


        <section class="service-cta p-v-40 color-bg-style2">

            <div class="container">

                <div class="container-flex cta">
                    <div class="block-col width-md-2-3 cta-left">
                        <h2 class=""><?php echo _e('Looking for more resources?'); ?></h2>
                        <span class=""><?php _e('Explore VPS hosting and expand your business now.');?></span>
                    </div>
                    <div class="block-col width-md-1-3 cta-right">
                        <a href="<?php echo $config['links']['vps-hosting']; ?>" class="button xlarge ghost push-right"><?php _e('VPS Hosting')?></a>
                    </div>
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-cta -->


        <section class="service-faq extra-pad-top color-bg-style1" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/reseller-hosting.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->

    </section>

</article>

<?php

universal_redirect_footer([
    'en' => $site_en_url.'/reseller-hosting/',
    'br' => $site_br_url.'/revenda-de-hospedagem/'
]);

get_footer(); 

?>
