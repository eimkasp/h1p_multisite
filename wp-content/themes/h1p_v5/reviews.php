<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Reviews
*/

//wp_enqueue_script('cs.slider', get_template_directory_uri() . '/landings/cs_slider.js', ['jquery'], false, true);
wp_enqueue_script('jquery.jcarousel', get_template_directory_uri() . '/js/vendor/jquery.jcarousel.min.js', ['jquery'], false, true);
wp_enqueue_script('jquery.jcarousel-swipe', get_template_directory_uri() . '/js/vendor/jquery.jcarousel-swipe.min.js', ['jquery'], false, true);

get_header();

?>

<article class="page reviews">

<section class="main page-content">


    <section class="reviews extra-pad-top">

        <div class="container">

            <div class="row">

                <div class="reviews-tabs">
                    <div class="review-tab active" data-tab-id="sitereviews" onclick="jQuery('[data-tab-id]').removeClass('active');jQuery(this).addClass('active');jQuery('[data-tab-content-id]').removeClass('active');jQuery('[data-tab-content-id='+jQuery(this).data('tab-id')+']').addClass('active');"><?php _e('General Reviews')?></div>
                    <div class="review-tab" data-tab-id="vpshosting" onclick="jQuery('[data-tab-id]').removeClass('active');jQuery(this).addClass('active');jQuery('[data-tab-content-id]').removeClass('active');jQuery('[data-tab-content-id='+jQuery(this).data('tab-id')+']').addClass('active');"><?php _e('VPS Hosting')?></div>
                    <div class="review-tab" data-tab-id="cshosting" onclick="jQuery('[data-tab-id]').removeClass('active');jQuery(this).addClass('active');jQuery('[data-tab-content-id]').removeClass('active');jQuery('[data-tab-content-id='+jQuery(this).data('tab-id')+']').addClass('active');"><?php _e('Cloud Servers')?></div>
                    <div class="review-tab" data-tab-id="webhosting" onclick="jQuery('[data-tab-id]').removeClass('active');jQuery(this).addClass('active');jQuery('[data-tab-content-id]').removeClass('active');jQuery('[data-tab-content-id='+jQuery(this).data('tab-id')+']').addClass('active');"><?php _e('Web Hosting')?></div>
                    <div class="review-tab" data-tab-id="resellerhosting" onclick="jQuery('[data-tab-id]').removeClass('active');jQuery(this).addClass('active');jQuery('[data-tab-content-id]').removeClass('active');jQuery('[data-tab-content-id='+jQuery(this).data('tab-id')+']').addClass('active');"><?php _e('Reseller Hosting')?></div>
                </div>

                <div class="reviews-content">

                    <div class="review-content active site-reviews" data-tab-content-id="sitereviews">
                        <div class="section-header m-v-30">
                            <h1 class="block-title">Host1Plus Reviews</h1>
                            <span class="title-text m-t-20">Express your opinion and be heard! Share your personal experience with others and turn considerations into reasoned decisions. Your feedback is highly important to us because it allows our company to improve and make a better place for your projects.</span>
                        </div>

                        <?php include_block("htmlblocks/reviews_home.php"); ?>

                        <div id='yotpo-testimonials-custom-tab'></div>
                    </div>
                    <div class="review-content" data-tab-content-id="webhosting">
                        <div class="section-header m-v-30">
                            <h1 class="block-title">Web Hosting Reviews</h1>
                            <span class="title-text m-t-20">Express your opinion and be heard! Share your personal experience with others and turn considerations into reasoned decisions. Your feedback is highly important to us because it allows our company to improve and make a better place for your projects.</span>
                        </div>
                        <div class="yotpo yotpo-main-widget"
                            data-product-id="webhosting"
                            data-name="Web Hosting"
                            data-url="<?=$config['links']['web-hosting']?>"></div>
                    </div>
                    <div class="review-content" data-tab-content-id="vpshosting">
                        <div class="section-header m-v-30">
                            <h1 class="block-title">VPS Hosting Reviews</h1>
                            <span class="title-text m-t-20">Express your opinion and be heard! Share your personal experience with others and turn considerations into reasoned decisions. Your feedback is highly important to us because it allows our company to improve and make a better place for your projects.</span>
                        </div>
                        <div class="yotpo yotpo-main-widget"
                            data-product-id="vpshosting"
                            data-name="VPS Hosting"
                            data-url="<?=$config['links']['vps-hosting']?>"></div>
                    </div>
                    <div class="review-content" data-tab-content-id="cshosting">
                        <div class="section-header m-v-30">
                            <h1 class="block-title">Cloud Servers Reviews</h1>
                            <span class="title-text m-t-20">Express your opinion and be heard! Share your personal experience with others and turn considerations into reasoned decisions. Your feedback is highly important to us because it allows our company to improve and make a better place for your projects.</span>
                        </div>
                        <div class="yotpo yotpo-main-widget"
                             data-product-id="cshosting"
                             data-name="Cloud Servers"
                             data-url="<?=$config['links']['cloud-servers']?>"></div>
                    </div>
                    <div class="review-content" data-tab-content-id="resellerhosting">
                        <div class="section-header m-v-30">
                            <h1 class="block-title">Reseller Hosting Reviews</h1>
                            <span class="title-text m-t-20">Express your opinion and be heard! Share your personal experience with others and turn considerations into reasoned decisions. Your feedback is highly important to us because it allows our company to improve and make a better place for your projects.</span>
                        </div>
                        <div class="yotpo yotpo-main-widget"
                            data-product-id="resellerhosting"
                            data-name="Reseller Hosting"
                            data-url="<?=$config['links']['reseller-hosting']?>"></div>
                    </div>

                </div> <!-- end of .reviews-content -->

            </div> <!-- end of .row -->

        </div> <!-- end of .container -->

    </section> <!-- end of .reviews -->

</article>

<?php if( isset( $_GET['tab'] ) ): ?>
    <script>
        $(document).ready(function() { $('[data-tab-id="<?php echo $_GET['tab'] ?>"]').click(); });
    </script>
<?php endif; ?>

<?php get_footer(); ?>
