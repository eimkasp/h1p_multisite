<footer id="footer">

    <div class="full-search-container">
        <div class="container">
            <?php include 'htmlblocks/footer/fsearch.php'; ?>
        </div>
    </div>

    <div class="full-nav-container">
        <div class="container">
            <?php include 'htmlblocks/footer/fnav.php'; ?>
        </div>
    </div>

    <div class="full-info-container">
        <div class="container">
            <div class="info-container">
                <?php include 'htmlblocks/footer/fsubscribe.php'; ?>
                <?php include 'htmlblocks/footer/fpayment.php'; ?>
                <div class="footer-info-block">
                    <span class="info">
                    &copy; 2008 &ndash; <?php echo date("Y"); ?> Host1Plus. <?php _e('All rights reserved.')?>
                    Host1Plus a division of <a target="_blank" href="http://digitalenergytech.net/">Digital Energy Technologies Ltd.</a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</footer>
