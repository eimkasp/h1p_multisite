<?php

// This file should exist only on dev environments
//  and should be used to override variables
if (file_exists(__DIR__.'/config.vars.php')) {
    include __DIR__.'/config.vars.php';
}

$dev_hosts = [
    'testssl.host1plus.com',
    'wp-en.v4.git.host1plus.com',
    'www.wp-en-blog.v4.git.host1plus.com',
    'wp-en-press.v4.git.host1plus.com',
    'wp-en-tutorials.v4.git.host1plus.com',
    'www.wp-multi.h1p.git.host1plus.com',
    'www.host1plus.dev'
];
$live_host = 'www.host1plus.com';
$live_host_br = 'www.host1plus.com.br';
$current_host = $_SERVER['HTTP_HOST'];

$site_url = in_array($current_host, $dev_hosts) ? '//' . $current_host : '//' . $live_host;
$site_current_url = site_url();

$site_en_url = 'https://'.$live_host;
$site_br_url =  'https://'.$live_host_br;

# exeption for /press
$subdirs = explode('/', parse_url($site_current_url, PHP_URL_PATH));
$subdir = count($subdirs) > 1 ? $subdirs[1] : '/';
if( $subdir == 'press' || $subdir == 'tutorials' || $subdir == 'blog') $site_current_url = $site_url;
#################

// uses variable from config.vars.php if defined
$WHMCS_URL = isset($DEV_WHMCS_URL) ? $DEV_WHMCS_URL : 'https://manage.host1plus.com';

$domain_segment_parts = explode('.',$site_current_url);
$last_domain_segment = end($domain_segment_parts);

switch($last_domain_segment){
    case 'br':
        $WHMCS_LANG = 'portuguese-br';
        $WHMCS_CURR = '11';
        $PAP_LANG = 'pt-br';
        break;
    case 'es':
        $WHMCS_LANG = 'spanish';
        $WHMCS_CURR = '4';
        $PAP_LANG = 'en-US';
        break;
    case 'lt':
        $WHMCS_LANG = 'lithuanian';
        $WHMCS_CURR = '4';
        $PAP_LANG = 'en-US';
        break;
    case 'cn':
        $WHMCS_LANG = 'chinese';
        $WHMCS_CURR = '1';
        $PAP_LANG = 'en-US';
        break;
    case 'ru':
        $WHMCS_LANG = 'russian';
        $WHMCS_CURR = '9';
        $PAP_LANG = 'en-US';
        break;
    default:
        $WHMCS_LANG = 'english';
        $WHMCS_CURR = '1';
        $PAP_LANG = 'en-US';
        break;
}



$links = require_once __DIR__.'/config/links.php';
$products = require_once __DIR__.'/config/products.php';



$config = array(

    'pap_lang' => $PAP_LANG,

    'whmcs_lang' => $WHMCS_LANG,

    'whmcs_curr' => $WHMCS_CURR,

    'cookies_policy_version' => 1,

    'contacts' => array(

        'phone_uk' => '+44.870-8200222',
        'phone_de' => '0-800-723-71712',
        'phone_lt' => '8-700-11555',
        'phone_cl' => '+56.229-382-322',

        'support-phone' => '+1.888-804-2926'

    ),

    'links' => $links[0],

    'social_links' => array(

        'facebook' => "https://www.facebook.com/Host1Plus",
        'twitter' => "https://twitter.com/Host1Plus",
        'googleplus' => "//plus.google.com/u/0/112055797868753237894?prsrc=3",
        'youtube' => "https://www.youtube.com/Host1Plus",
        'linkedin' => "https://www.linkedin.com/company/host1plus/",
        'pinterest' => "https://pinterest.com/host1plus/"

    ),

    'whmcs_links' => array(

        'login' => $WHMCS_URL . '/login.php',
        'login_post' => $WHMCS_URL . '/dologin.php',
        'reseller-sign-in' => $WHMCS_URL . '/h1p/reseller_signin.php',
        'reseller-sign-up' => $WHMCS_URL . '/h1p/reseller_program_reg.php',
        'register' => $WHMCS_URL . '/register.php',
        'passwordreset' => $WHMCS_URL . '/pwreset.php',

        'checkout' => $WHMCS_URL . '/cart.php',
        'checkout_web' => $WHMCS_URL . '/h1p/web-hosting.php',
        'checkout_vps' => $WHMCS_URL . '/h1p/vps.php',
        'checkout_cloud' => $WHMCS_URL . '/h1p/cloud.php',
        'checkout_domains' => $WHMCS_URL . '/h1p/addDomain.php',

        'countries_json' => $WHMCS_URL . '/h1p/available_countries.php',
        'phonecodes_js' => $WHMCS_URL . '/templates/orderforms/h1psteps/js/countries_phone_codes.js',
        'states_js' => $WHMCS_URL . '/templates/orderforms/h1psteps/js/countries_states.js',

    ),

    'landings' => array(

        'windows-cloud-servers' => $site_current_url . '/windows-cloud-servers/',
        'cheap-vps' => $site_current_url . '/cheap-vps-hosting/'
    ),

    'products' => $products[0]

);

define('AFF_SYSTEM_LINK','https://affiliates.host1plus.com');
define('AFF_MERCHANT_EMAIL','affiliates@host1plus.com');
define('AFF_MERCHANT_PASS','UXmJFu6ajh889zrCGLD2');
