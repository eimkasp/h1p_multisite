<?php

/**
 * @package WordPress
 * @subpackage h1p_v4
 */
/*
Template Name: Out Of Stock
*/

get_header();

?>

<div class="container">
    <h1 style="text-align: center;  line-height: 80px;"><?php _e('We are sorry, but the product is temporarily out of stock.')?></h1>
    <h1 style="font-size:42px;color:#ed621b;text-align: center;  line-height: 80px;"><?php _e('Please get back to us later.')?></h1>
    <h4 style="text-align: center;padding-top: 10px;padding-bottom: 70px;font-weight: 400;max-width: 730px;margin: 0 auto;"><?php printf(__('For more information contact us at %s, via live chat or call-in service.'), "<a href='mailto:support@host1plus.com'>support@host1plus.com</a>"); ?></h4>
</div>

<?php get_footer(); ?>
