<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Cloud Servers Registration process
*/

global $config;

/*
--------------------
  ATTENTION PLEASE:
--------------------

    - This template should be assigned to the WP page (must be created)
    - The page should have a permalink "/cloud-servers-registration" - as this is the link for ajax post

*/
// 1. Form general output (on success and on failure)
$op_output = '';
$can_send = true;

// 2. Check the posted data, see if fields are not empty
$data = [];
$data[0] = addslashes( $_POST['os'] );
$data[1] = addslashes( $_POST['first_name'] );
$data[2] = addslashes( $_POST['last_name'] );
$data[3] = addslashes( $_POST['email'] );
$data[4] = addslashes( $_POST['phone'] );
$data[5] = addslashes( $_POST['company'] );          // can be empty
$data[6] = addslashes( $_POST['website'] );          // can be empty
$data[7] = addslashes( $_POST['projects'] );         // can be empty
$data[8] = addslashes( $_POST['expectations'] );     // can be empty


    // Validation purpose - do not make queries if fields are empty

    // all must be not be empty
    if ( empty($data[0]) OR
         empty($data[1]) OR
         empty($data[2]) OR
         empty($data[3]) OR
         empty($data[4]) ) {

        $can_send = ($can_send AND false);
    } else {
        $can_send = ($can_send AND true);
    }

    // at least one should be not be empty
    if ( empty($data[7]) AND empty($data[8]) ) {
        $can_send = ($can_send AND false);
    } else {
        $can_send = ($can_send AND true);
    }



if ( $can_send ) {
    $op_result = 'success';
} else {
    $op_result = 'validation_error';
}



//    For testing purpose

/*$op_output .= '<h3>Got these via ajax</h3>';

foreach ($data as $key => $value) {
    $op_output .= '<p>'. $value .'</p>';
}
*/

/*
    CURL TRANSACTION
    ----------------
*/


if ( $can_send ) {

    $whmcs_addon_url = $WHMCS_URL . '/modules/addons/h1p_leads_management/external_registration.php';
    //$whmcs_addon_url = 'https://local-dev.manage.host1plus.com' . '/modules/addons/h1p_leads_management/external_registration.php'; //local vm


    $postfields = [];
    $postfields['leads_first_name'] = $data[1];
    $postfields['leads_last_name'] = $data[2];
    $postfields['leads_email'] = $data[3];
    $postfields['leads_phone'] = $data[4];
    $postfields['leads_os'] = $data[0];
    $postfields['leads_company'] = $data[5];
    $postfields['leads_website'] = $data[6];
    $postfields['leads_projects'] = $data[7];
    $postfields['leads_expectations'] = $data[8];


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $whmcs_addon_url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 300);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields) );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $whmcs_result = curl_exec($ch);

    if (!curl_errno($ch)) {
        $info = curl_getinfo($ch);
    }

    // --- Deal with the response from WHMCS

    $result_offset = strpos( $whmcs_result, ';');
    $whmcs_result_state = substr( $whmcs_result, 0, $result_offset );
    $whmcs_result_message = substr( $whmcs_result, $result_offset + 1 );

    //$op_output .= $whmcs_result_state;

    //$op_output .= '<h3>Got these via CURL</h3>';

    if( $whmcs_result && !empty( $whmcs_result ) ){


        if( $whmcs_result_state == 'result=error' ){

            // ! Change operation result, because further validation was failure
            $op_result = 'validation_error';
            $op_output .= '<h3 class="pcc-title">'._('Oops!').'</h3>' .
                        '<p class="pcc-sub-title">'._('Looks like there are some typos or empty fields.').'</p>';
            $op_output .= $whmcs_result_message;
            //todo: back button

            $op_output .= '<br><p class=""><strong>'._('You can go back to correct your information.').'</strong></p><br>' .
                    '<div style="text-align:center;"><button class="button primary blue" onclick="$.magnificPopup.instance.close();">Back</button></div>';

        } elseif ( $whmcs_result_state == 'result=success' ){
            // all good
            //$op_output .= $whmcs_result_message;    // MONITORING
            $op_output .= '<h2 class="center white m-b-10">'._('Thank you for joining our waitlist!').'</h2>' .
                '<p class="center white m-b-20">'._('In the meantime, share the news with your friends.').'</p>' .
                '<p class="center white m-b-20"><a href="http://twitter.com/share?url=www.host1plus.com/cloud-servers&text=Host1Plus%20is%20launching%20Cloud%20Servers%20in%20August%202016!%20Get%20early%20access%20for%20free!%20http%3A%2F%2Fwww.host1plus.com%2Fcloud-servers&via=host1plus&related=yarrcat" target="_blank" class="twitter-share"><i class="fa fa-twitter"></i> '._('Sounds good!').'</a></p>' .
                '<p class="center white m-b-40"><a href="http://www.facebook.com/sharer.php?u=www.host1plus.com/cloud-servers" target="_blank" class="fb-share"><i class="fa fa-facebook"></i> '._("Let's do it!").'</a></p>';
        } else {
            $op_output .= '<p class="center white m-b-40"><strong>'._('Unexpected error. Please contact support.').'<strong></p>';
        }

    }

    curl_close($ch);

}

    // So if we get 'result=error, we pass validation messages from WHMCS addon
    // if we get 'result=success', we create Thank you block to insert in the calling landing page.



$json_package = [
    "response" => $op_result,
    "output" => $op_output
];

//var_dump($json_package);
echo json_encode($json_package);
