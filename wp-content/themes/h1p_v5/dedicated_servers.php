<?php

/**
 * @package WordPress
 * @subpackage h1p_v4
 */
/*
Template Name: Dedicated Servers
*/

get_header();

?>


<div class="page landing dedicated-servers">

    <?php include_block('htmlblocks/hosting/content-header.php', array('selected_tab'=>'dedicated-servers'));?>

    <div class="page-content">

        <section class="content">
            <div class="hosting">
                <div class="row dedicated landing show">
                    <div class="container">
                        <div class="page-path">
                            <span class="text-item"><?php _e('Home')?></span>
                            <i class="fa fa-angle-right text-item"></i>
                            <span class="text-item"><?php _e('Hosting Services')?></span>
                            <i class="fa fa-angle-right text-item"></i>
                            <span class="text-item active"><?php _e('Dedicated Servers')?></span>
                        </div>
                        <div class="page-title-block">
                            <h1 class="page-title"><?php _e('Dedicated Servers')?></h1>
                            <span class="title-text"><?php _e('With lightning-fast Intel® Core processors, up to 32 GB of RAM and 20 TB of bandwidth, you have the resources you need to create a powerful platform for your business. And with control panel options, increased bandwidth, backup plans and firewall protection, we have you covered 24/7.')?></span>
                            <span class="small-title"><?php _e('Choose your dedicated server hosting plan')?>:</span>
                        </div>
                        <?php include_block("htmlblocks/pricing-tables/dedicated-servers.php"); ?>
                    </div>
                </div>
            </div>
        </section>

        <?php include_block("htmlblocks/hosting/whydedicated.php"); ?>
        <?php //include("htmlblocks/dedicated_speed_test.php"); // FileSystem API JS ?>
        <?php include_block("htmlblocks/hosting/why_dedicated_text.php"); ?>
        <?php include_block("htmlblocks/faq/dedicated-servers.php"); ?>
        <?php include_block("htmlblocks/tutorials.php", array('tags' => 'dedicated')); ?>
        <?php include_block("htmlblocks/blogposts.php", array('set' => 'dedicated-servers')); ?>
        <?php if(get_locale() == 'en_US'): ?>
            <?php include_block("htmlblocks/dedicated-servers-reviews.php"); ?>
        <?php endif; ?>

    </div>

</div>

<?php get_footer(); ?>
