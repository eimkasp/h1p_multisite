<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 *
 * Template Name: Main Page\
 *
*/

global $config;


get_header();

?>
<div class="page not-found">

    <section class="headline not-found">

        <div class="container">

            <h1 class="page-title"><?php _e('Hmm, something\'s not right.')?></h1>
            <p class="title-descr"><?php _e('Error 404: The page you are looking for does not exist.')?></p>

        </div>


        <div class="container">

            <div class="section-content">

                <div class="search">
                    <span class="description"><?php _e('Get your answer in seconds!'); ?></span>
                    <form action="<?php echo $config['links']['search']?>" method="get">
                        <input type="text" class="site-search-text" name="q" value="" placeholder="<?php _e('Search')?>">
                        <input type="submit" class="site-search-btn icon search" value="">
                    </form>
                </div>

            </div>

        </div>

    </section>

    <section class="supplementary-links">

        <div class="container">

            <p class="description center p-b-20"><?php _e('Search our online resources and find the information you need'); ?></p>

            <div class="row ">
                <div class="info-block">
                    <h3><?php _e('Tutorials');?></h3>
                    <p><?php _e('Check out more than 700 professional tutorials.'); ?></p>
                    <p><a class="m-t-20 info-block-link" href="<?php echo $config['links']['tutorials']?>"><?php _e('Explore')?> <i class="fa fa-angle-right"></i></a></p>
                </div>
                <div class="info-block">
                    <h3><?php _e('Knowledge Base');?></h3>
                    <p><?php _e('Quickly find detailed information about your services.'); ?></p>
                    <p><a class="m-t-20 info-block-link" href="<?php echo $config['links']['knowledge_base']?>"><?php _e('Explore')?> <i class="fa fa-angle-right"></i></a></p>
                </div>
                <div class="info-block">
                    <h3><?php _e('Blog');?></h3>
                    <p><?php _e('Read latest company news and discover industry trends.'); ?></p>
                    <p><a class="m-t-20 info-block-link" href="<?php echo $config['links']['blog']?>"><?php _e('Explore')?> <i class="fa fa-angle-right"></i></a></p>
                </div>
            </div>

        </div> <!-- end of .page-content-supplementary -->

    </section>

</div>

<?php get_footer(); ?>
