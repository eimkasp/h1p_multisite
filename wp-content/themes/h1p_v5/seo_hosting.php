<?php

/**
 * @package WordPress
 * @subpackage h1p_v4
 */
/*
Template Name: SEO Hosting
*/

get_header();

?>

<div class="page landing seo-hosting">

    <?php include_block('htmlblocks/hosting/content-header.php', array('selected_tab'=>'seo-hosting'));?>

    <div class="page-content">

        <section class="block seo">

            <div class="container">
                <div class="row">
                    <div class="page-path">
                        <span class="text-item"><?php _e('Home')?></span>
                        <i class="fa fa-angle-right text-item"></i>
                        <span class="text-item"><?php _e('Hosting Services')?></span>
                        <i class="fa fa-angle-right text-item"></i>
                        <span class="text-item active"><?php _e('SEO Hosting')?></span>
                    </div>
                    <div class="page-title-block">
                        <h1 class="page-title"><?php printf(__('SEO Hosting from %s per month'), '<span class="highlight">' . $whmcs->getMinPrice($config['products']['seo_vps_hosting']['plans'][1]['id']) . '</span>')?></h1>
                        <p class="title-text"><?php _e('Get noticed, increase traffic, boost sales! VPS SEO hosting plans enable users to deploy both traditional and non-traditional methods in order to improve the search results of a website, thus increasing traffic and sales.')?></p>
                    </div>
                    <div class="block-tabs">
                        <?php include("htmlblocks/pricing-tables/seo_vps.php"); ?>
                    </div>
                </div>
            </div>

        </section>

        <?php include_block("htmlblocks/hosting/seo_texts.php"); ?>
        <?php include_block("htmlblocks/faq/seo-hosting.php"); ?>
        <?php include_block("htmlblocks/tutorials.php", array('tags' => 'seo')); ?>
        <?php include_block("htmlblocks/blogposts.php", array('set' => 'seo-hosting')); ?>

    </div>

</div>

<?php get_footer(); ?>
