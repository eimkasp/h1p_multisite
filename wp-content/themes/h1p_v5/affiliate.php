<?php
/**
 * @package WordPress
 * @subpackage h1p_v5
 *
 * Template Name: Affiliate
 */

get_header();

?>

<article class="page affiliate">

    <header class="headline affiliate">
        <div class="container adjust-vertical-center">
            <h1 class="page-title"><?php _e('Affiliate Program')?></h1>
            <div class="title-descr"><?php _e('Spread the word about a trustworthy service to your friends and followers, and we’ll turn your sincere recommendations into earnings.')?></div>
            <a href="<?php echo $config['links']['affiliate_register'];?>" class="button ghost adjust-vertical-tm20"><?php _e('Sign Up');?></a>
            <span class="space-10"></span>
            <a href="<?php echo $config['links']['affiliate_login'];?>" class="button ghost adjust-vertical-tm20"><?php _e('Log In');?></a>

        </div>
    </header> <!-- end of .headline.affiliate -->

     <section class="main page-content">


        <section class="process extra-pad-top">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('How does it work?')?></h2>
                </div>

                <div class="steps">

                    <div class="step step-1">

                        <span class="step-index">1</span>

                        <div class="step-container">
                            <i class="step-icon icon affiliate-process signup"></i>
                            <h3 class="step-title"><?php _e('Sign up!')?></h3>
                            <span class="step-description"><?php _e('To get started, create your affiliate account and get a $10 sign-up bonus! Enjoy a free Affiliate membership without extra fees and complicated conditions!')?></span>
                        </div>

                    </div>

                    <div class="step-separator"></div>

                    <div class="step step-2">

                        <span class="step-index">2</span>

                        <div class="step-container">
                            <i class="step-icon icon affiliate-process banner"></i>
                            <h3 class="step-title"><?php _e('Spread the word!')?></h3>
                            <span class="step-description"><?php _e('Choose links or banners from our database and add them to your website. Share the message with your followers, friends and family!  It’s as easy as that!')?></span>
                        </div>

                    </div>

                    <div class="step-separator"></div>

                    <div class="step step-3">

                        <span class="step-index">3</span>

                        <div class="step-container">
                            <i class="step-icon icon affiliate-process dollar"></i>
                            <h3 class="step-title"><?php _e('Earn money!')?></h3>
                            <span class="step-description"><?php _e('We value your recommendations! Start with 100&#37; or $65 for each referred client and earn extra for each sub-referral. The more people you reach, the more you earn!')?></span>
                        </div>

                    </div>

                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .process -->

        <section class="features">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('What exactly do you get?')?></h2>
                </div>

                <ul class="features-list checklist green">
                    <li><?php _e('Earn up to $115 per referral! Start small, aim big.')?></li>
                    <li><?php _e('Commissions are handed out each month.')?></li>
                    <li><?php _e('Receive a $10 sign-up bonus. Get off to a good start!')?></li>
                    <li><?php _e('Real-time statistics and reporting. Track your progress!')?></li>
                    <li><?php _e('No risk. Join for free and get everything you need.')?></li>
                    <li><?php _e('No limits. Get every penny you earn.')?></li>
                </ul>

                </div> <!-- end of .container -->

        </section> <!-- end of .features -->

        <div class="container">
            <div class="block-separator-line"></div>
        </div>

        <section class="revenue">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('How much can you earn?')?></h2>
                </div>

                <div class="row">

                    <div class="block under-40">

                        <h3 class="block-title"><?php _e('For sales under $40')?></h3>

                        <div class="chart">

                            <div class="human orange">

                                <div class="head"></div>
                                <div class="body"></div>
                                <div class="legs"></div>

                            </div>

                            <div class="values orange">

                                <div class="value value-1">

                                    <h4 class="value-title"><?php _e('Tier')?> 3 - 150%</h4>

                                    <span class="value-description"><?php _e('If your referrals top 25 sales per month, you get extra 50&#37; for each one.')?></span>

                                </div>

                                <div class="value value-2">

                                    <h4 class="value-title"><?php _e('Tier')?> 2 - 125%</h4>

                                    <span class="value-description"><?php _e('If your referrals top 15 sales per month, you get extra 25&#37; for each one.')?></span>

                                </div>

                                <div class="value value-3">

                                    <h4 class="value-title"><?php _e('Tier')?> 1 - 100%</h4>

                                    <span class="value-description"><?php _e('If your referrals order services for less than $40 – you will still receive your commissions. Always get 100&#37; of the amount of money your referrals spend! Draw more and get extra for every purchase they make.')?></span>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="block more-than-40">

                        <h3 class="block-title"><?php _e('For sales over $40')?></h3>

                        <div class="chart">

                            <div class="human blue">

                                <div class="head"></div>
                                <div class="body"></div>
                                <div class="legs"></div>

                            </div>

                            <div class="values blue">

                                <div class="value value-1">

                                    <h4 class="value-title"><?php _e('Tier')?> 3 - $115</h4>

                                    <span class="value-description"><?php _e('If your referrals top 25 sales per month, you get extra $50 for each one.')?></span>

                                </div>

                                <div class="value value-2">

                                    <h4 class="value-title"><?php _e('Tier')?> 2 - $90</h4>

                                    <span class="value-description"><?php _e('If your referrals top 15 sales per month, you get extra $25 for each one.')?></span>

                                </div>

                                <div class="value value-3">

                                    <h4 class="value-title"><?php _e('Tier')?> 1 - $65</h4>

                                    <span class="value-description"><?php _e('If your referrals order services for over $40, you will receive a fixed amount of $65 to your Affiliate account. Climb tiers and earn extra for each referral you attract – opt for the max and get up to $115 for every single one.')?></span>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .revenue -->


        <section class="video highlight m-v-30 p-0">

            <div class="container">

                <div class="block-of-two p-v-30">
                    <div class="layout-row two-col-row">
                        <div class="block-col">
                            <h3><?php _e('Easy steps towards your success! Check our video and see how simple it can be.');?></h3>
                        </div>
                        <div class="block-col p-0">
                            <div class="video-iframe-container">
                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/ANoaM3O0j_I" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </section> <!-- end of .video -->


        <section class="success-tips">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('How to become a successful Affiliate?')?></h2>
                </div>

                <div class="layout-row two-col-row">

                    <div class="block-col accordion">

                        <div class="item tip-1">
                            <a class="item-title" href="#" data-prevent data-toggle-active><i class="fa fa-plus"></i> <h4><?php _e('Sign up')?></h4></a>
                            <div class="item-content">
                                <dl class="qa-list">
                                    <dt><?php _e('How do I sign up?')?></dt>
                                        <dd><?php printf(__('You can sign up for our Affiliate program at %s.'), '<a href="https://affiliates.host1plus.com/affiliates/signup.php">https://affiliates.host1plus.com</a>')?></dd>
                                    <dt><?php _e('Do I have to be a customer to become an Affiliate?')?></dt>
                                        <dd><?php _e('Absolutely not! Everyone interested in our Affiliate program is welcome.')?></dd>
                                    <dt><?php _e('How long will it take to become an Affiliate?')?></dt>
                                        <dd><?php _e('You become our Affiliate the moment you sign up.')?></dd>
                                    <dt><?php _e('How much does it cost?')?></dt>
                                        <dd><?php _e('Nothing! Host1Plus Affiliate program has an open membership and is available for free.')?></dd>
                                </dl>
                            </div>
                        </div>

                        <div class="item tip-2">
                            <a class="item-title" href="#" data-prevent data-toggle-active><i class="fa fa-plus"></i> <h4><?php _e('Promote our services')?></h4></a>
                            <div class="item-content">
                                <dl class="qa-list">
                                    <dt><?php _e('Where can I get links and banners?')?></dt>
                                        <dd><?php _e('You may access all promotional material at your Affiliate account.')?></dd>
                                    <dt><?php _e('Do I need any specific skills?')?></dt>
                                        <dd><?php _e('No specific IT or marketing skills are required.')?></dd>
                                </dl>
                            </div>
                        </div>

                        <div class="item tip-3">
                            <a class="item-title" href="#" data-prevent data-toggle-active><i class="fa fa-plus"></i> <h4><?php _e('Track your progress')?></h4></a>
                            <div class="item-content">
                                <dl class="qa-list">
                                    <dt><?php _e('How do I track my sales activity and commissions?')?></dt>
                                        <dd><?php _e('To track your sales and Affiliate commissions, log in to your Affiliate Account.')?></dd>
                                    <dt><?php _e('What should I do if my generated sales don’t show up on my Affiliate account?')?></dt>
                                        <dd><?php _e('If you have any questions or experience any problems with your Affiliate Account or commissions, please contact us at <a href="mailto:affiliates@host1plus.com">affiliates@host1plus.com</a>.')?></dd>
                                </dl>
                            </div>
                        </div>

                    </div>

                    <div class="block-col accordion">

                        <div class="item tip-4">
                            <a class="item-title" href="#" data-prevent data-toggle-active><i class="fa fa-plus"></i> <h4><?php _e('Generate sales')?></h4></a>
                            <div class="item-content">
                                <dl class="qa-list">
                                    <dt><?php _e('How do you track referral transactions?')?></dt>
                                        <dd><?php _e('Each Affiliate receives a unique code that is placed along with the promotional material on the Affiliate’s website. Once his visitors click on the banners or links, this code tracks the behavior of the visitor and directs the information about his purchases directly to the Affiliate account.')?></dd>
                                    <dt><?php _e('How do you know which Affiliate to pay for the sale?')?></dt>
                                        <dd><?php _e('The unique Affiliate code, described above, contains particular code, used to identify each Affiliate and relate their generated sales to their Affiliate accounts.')?></dd>
                                    <dt><?php _e('How long does it take for a sale to show up on my Affiliate account?')?></dt>
                                        <dd><?php _e('All generated sales show up immediately. However, all of the sales must be verified by our team.')?></dd>
                                </dl>
                            </div>
                        </div>

                        <div class="item tip-5">
                            <a class="item-title" href="#" data-prevent data-toggle-active><i class="fa fa-plus"></i> <h4><?php _e('Earn commissions')?></h4></a>
                            <div class="item-content">
                                <dl class="qa-list">
                                    <dt><?php _e('How will I get my earned Affiliate commissions?')?></dt>
                                        <dd><?php _e('Earned Affiliate commissions are transferred directly to your PayPal account.')?></dd>
                                    <dt><?php _e('When will I be paid?')?></dt>
                                        <dd><?php _e('All commissions are handed out at the end of each month.')?></dd>
                                    <dt><?php _e('What is the minimum payout in this Affiliate program?')?></dt>
                                        <dd><?php _e('Cash out your earned Affiliate commissions when you reach $50 or more.')?></dd>
                                </dl>
                            </div>
                        </div>

                    </div>

                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .success-tips -->


    </section> <!-- end of .main -->

    <?php include_block("htmlblocks/faq/affiliate.php"); ?>
    <?php /* include_block("htmlblocks/tutorials.php", array('tags' => 'affiliate')); */ ?>
    <?php include_block("htmlblocks/blogposts.php", array('set' => 'affiliate')); ?>



</article>

<?php

universal_redirect_footer([
    'en' => $site_en_url.'/affiliate-program/',
    'br' => $site_br_url.'/programa-afiliados/'
]);

get_footer(); 

?>
