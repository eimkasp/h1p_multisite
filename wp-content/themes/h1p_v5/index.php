<?php

/**
 * The main template file
 */

get_header();
?>
<div class="page press-releases">

    <section class="page-content">

        <div class="container">

            <?php if (have_posts()):?>

                <?php while (have_posts()): ?>

                    <?php the_post(); ?>

                    <div class="page-path">
                        <span class="text-item"><?php _e('Home')?></span>
                        <i class="fa fa-angle-right text-item"></i>
                        <span class="text-item active"><?php the_title() ?></span>
                    </div>

                    <section class="content" id="post-<?php the_ID(); ?>">

                        <div class="section-header">
                            <div class="section-title">
                                <h2 class="title"><?php  the_title() ?></h2>
                            </div>
                        </div>

                        <div class="post"><?php the_content(); ?></div>

                    </section>


                <?php endwhile; ?>

            <?php endif;?>

        </div>
    </section>

</div>
<?php
get_footer();
?>
