<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Links by tag
*/

get_header();

    $tag_to_search = get_post_meta( get_the_ID(), 'tag_to_search' )[0];

    $tag_to_search = isset( $tag_to_search ) && !empty( $tag_to_search ) ? $tag_to_search : 'landing';

?>


<article class="page links-by-tag">


    <header class="headline links">
        <div class="container adjust-vertical-center">
            <h1 class="page-title"><?php _e('Useful Links')?></h1>
            <div class="title-descr"><?php _e('Quickly find what you\'re looking for - navigate and compare different hosting packages.')?></div>

        </div>
    </header> <!-- end of .headline.about-us -->

    <section class="main page-content">


        <section class="list-of-pages extra-pad-top">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Special Hosting Packages')?></h2>
                </div>

                    <?php
                    //Protect against arbitrary paged values
                    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

                    $limit_per_page = 30;

                    $args = array(
                        'posts_per_page' => $limit_per_page,
                        'tag' => $tag_to_search,
                        'paged' => $paged,
                    );

                    $the_query = new WP_Query( $args );
                        if ( $the_query->have_posts() ) : ?>

                    <ul>
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                                <li><a href="<?php echo get_permalink() ?>"><?php the_title() ?></a></li>

                        <?php endwhile; ?>
                    </ul>

                    <div class="paging"><?php

                        $big = 999999999; // need an unlikely integer

                        echo paginate_links( array(
                            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                            'format' => '?paged=%#%',
                            'current' => max( 1, get_query_var('paged') ),
                            'total' => $the_query->max_num_pages,
                            'prev_text' => '<',
                            'next_text' => '>',
                        ) );

                        wp_reset_postdata(); ?></div>

                    <?php else : ?>
                        <p class="error center"><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>

            </div> <!-- end of .container -->

        </section>

    </section> <!-- end of .main -->

</article>

<?php 

universal_redirect_footer([
    'en' => $site_en_url.'/links/',
    'br' => $site_br_url.'/links/'
]);

get_footer(); 

?>
