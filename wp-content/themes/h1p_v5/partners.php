<?php
/**
 * @package WordPress
 * @subpackage h1p_v5
 *
 * Template Name: Partners
 */

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

get_header();

?>

<article class="page partners">

    <header class="headline partners">
        <div class="container adjust-vertical-center">
            <h1 class="page-title"><?php _e('Our Partners')?></h1>
            <div class="title-descr"><?php _e('We partner with innovative organizations whose audiences are engaged, passionate and ready to act.')?></div>
        </div>
    </header> <!-- end of .headline.partners -->

     <section class="main page-content">


        <section class="partners extra-pad">

            <div class="container">

                <div class="row two-col-row" data-js-block-slide-content data-action-class="block-partner">
                    <div class="block-col block-partner block-bordered block-top-space-20 no-padding partner-42on" data-element>
                        <div class="block-overlay">
                            <div class="block-overlay-deco">
                                <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/partners/partners_42on_logo.jpg" alt="42on" height="50">
                            </div>
                            <div class="block-overlay-content p-30" data-content>
                                <h3 class="t-ff-default regular m-b-20">42on</h3>
                                <p class="t-14 t-spaced m-b-20">42on specializes in high quality Ceph consultancy and training helping organizations of all sizes to design, implement and run Ceph storage platform.</p>
                                <p class="t-14"><a href="#">https://www.42on.com/</a></p>
                            </div>
                        </div> <!-- end of .block-overlay wrapper-->
                    </div>
                    <div class="block-col block-partner block-bordered block-top-space-20 no-padding partner-terabits" data-element>
                        <div class="block-overlay">
                            <div class="block-overlay-deco">
                                <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/partners/partners_terabit_logo.jpg" alt="Terabit system" height="50">
                            </div>
                            <div class="block-overlay-content p-30" data-content>
                                <h3 class="t-ff-default regular m-b-20">Terabit Systems</h3>
                                <p class="t-14 t-spaced m-b-20">Being more than a supplier of top-quality networking hardware Terabit Systems is a customer-centered business that strives to provide high quality, hassle-free customer service in the network hardware industry.</p>
                                <p class="t-14"><a href="#">http://www.terabitsystems.com</a></p>
                            </div>
                        </div> <!-- end of .block-overlay wrapper-->
                    </div> 
                    <div class="block-col block-partner block-bordered block-top-space-20 no-padding partner-isp" data-element>
                        <div class="block-overlay">
                            <div class="block-overlay-deco">
                                <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/partners/partners_isp_logo.jpg" alt="ISP system" height="50">
                            </div>
                            <div class="block-overlay-content p-30" data-content>
                                <h3 class="t-ff-default regular m-b-20">ISP System</h3>
                                <p class="t-14 t-spaced m-b-20">Software development company specializing in web technologies and business automation since 2004, ISPsystem strives to reshape the hosting market based around the principles of quality and reliability at affordable prices. ISPsystem is the developer of ISPmanager - popular hosting control panel.</p>
                                <p class="t-14"><a href="#">https://www.ispsystem.com/</a></p>
                            </div>
                        </div> <!-- end of .block-overlay wrapper-->
                    </div>
                    <div class="block-col block-partner block-bordered block-top-space-20 no-padding partner-halon" data-element>
                        <div class="block-overlay">
                            <div class="block-overlay-deco">
                                <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/partners/partners_halon_logo.jpg" alt="HALON" height="50">
                            </div>
                            <div class="block-overlay-content p-30" data-content>
                                <h3 class="t-ff-default regular m-b-20">Halon</h3>
                                <p class="t-14 t-spaced m-b-20">Halon is a scriptable SMTP software to maximize deliverability and security of all in-flight email. It is tailored to fit large-scale email service providers. With Halon you get a future-proof and more efficient email infrastructure, without making compromises.</p>
                                <p class="t-14"><a href="#">https://halon.io/</a></p>
                            </div>
                        </div> <!-- end of .block-overlay wrapper-->
                    </div> 
                </div> 

            </div> <!-- end of .container -->

        </section> <!-- end of .partners -->

    </section> <!-- end of .main -->


</article>

<script>
    (function($){
        var $interactionElement = $('[data-js-block-slide-content]'),
            actionTriggerClass = $interactionElement.data('actionClass'),
            //$interactionBlocks = $('.' + actionTriggerClass),
            $interactionBlocks = $('[data-element]'),
            $sectionWrapper = $interactionElement.parent().parent();    // step twice (above container)

        /* Tasks:
            + pass target elem param via html data attributes;
            + prepare el. sizing;
            + bind click event
            + make animations (JS or CSS)
            + check and refine RWD
        */

        function doSizingPreps($elSelector) {
            var elWidth,
                $elChildTarget,
                $elChildTargetContent;

            $.each($elSelector, function(index, $el){
                elWidth = $(this).width();
                $elChildTarget = $(this).children().first();
                $elChildTargetContent = $elChildTarget.find('[data-content]');
                $elChildTarget.css('width', elWidth * 2);
                $elChildTargetContent.css('width', elWidth);
                $(this).addClass('ready');
            })
        }

        function resetTransitions($elSelector){
            $.each($elSelector, function(index, $el){
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    $(this).addClass('passive');
                }
            })
        }


        // prepare

        doSizingPreps($interactionBlocks);

        $(window).resize(function(){
            doSizingPreps($interactionBlocks);
        });


        // main actions

        $interactionBlocks.on('click', function(e) {
            var targetElem;
            resetTransitions($interactionBlocks);

            var $elOverlayContent = $(this).children().first().find('[data-content]');
            $(this).addClass('active');
            $(this).removeClass('passive');
        });

        $sectionWrapper.on('click', function(e) {
            // an event might have bubbled from $interactionBlocks listener
            if ($.contains($interactionBlocks.parent().get(0), e.target)) {
                // if click was on any of target element no action required
                if ($(e.target).hasClass('active')) {
                    console.log('was click on block');
                }
            } else {
                resetTransitions($interactionBlocks);
            }
        });

        
    })(jQuery)
</script>

<?php

universal_redirect_footer([
    'en' => $site_en_url.'/partners/',
    'br' => $site_br_url.'/partners/'
]);

get_footer(); 

?>
