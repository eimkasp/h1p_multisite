<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Web Hosting facelift
*/

wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);
wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'web');

// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

$expand = (isset($_GET['expand']) && $_GET['expand'] === 'yes') ? true : false;

?>

<article class="page landing web-hosting">

    <header class="headline web-hosting">
        <div class="container adjust-vertical-center">

<?php /*
            <div class="hot-news-wrapper">
                <div class="hot-news-overflow-container">
                    <div class="hot-news-group">
                        <div class="hot-news-handle notice"><?php _e('Upgrades'); ?> <i class="fa fa-angle-right"></i></div>
                        <?php 
                            if (get_locale() == 'en_US') {
                                $news_link = 'https://www.host1plus.com/press/host1plus-hardware-upgrades-sao-paulo-brazil';
                            } elseif (get_locale() == 'pt_BR') {
                                $news_link = 'https://www.host1plus.com/press/host1plus-anuncia-upgrade-de-hardware-em-sao-paulo-brasil/';
                            }
                        ?>
                        <div class="hot-news-message"><a href="<?php echo $news_link ?>" target="_blank" title="<?php _e('Hardware upgrades in São Paulo, Brazil'); ?>!"><?php _e('Hardware upgrades in São Paulo, Brazil'); ?>!</a></div>
                    </div>
                </div> <!-- end of .hot-news-overflow-container -->
            </div>
*/
?>            
        
            <h1 class="page-title"><?php _e('Web Hosting')?> <span class="highlight"><?php _e('from')?> <?php echo $mypage->getMinPrice(); ?></span></h1>
            <div class="title-descr"><?php _e('Unlimited space and traffic for your website.')?></div>

        </div>
    </header> <!-- end of .headline.web-hosting -->

    <div class="web-promo-withheader">
    </div>

    <section class="main page-content">


        <section class="service-pricing extra-pad" data-section-button="<?php _e('View Plans');?>">

            <div class="web-promo-section-wide"></div>

            <div class="scroll-id-target" id="plans-table"></div>

            <div class="container">

                <div class="web-promo-container-wide"></div>

                <div>

                    <?php include_block("htmlblocks/pricing-tables/web-hosting-v2.php"); ?>

                </div>
                <span class="title-follow-text center m-t-30">
                    <?php echo sprintf(_x_lc('Save %1$s up to 15%% %2$s for longer billing cycles!'), '<span class="highlight has-tooltip" data-tooltip="billing-cycle-savings">', '</span>');?>
                    <div class="tooltip-body style2 w-600" data-tooltip-content="billing-cycle-savings"><p><?php _e('Save up to 15% with longer billing cycles ranging from 6 to 24 months. The discount increases according to the length of the billing cycle.'); ?></p></div>
                    <div class="triangle"></div>
                </span>

            </div>

        </section> <!-- end of .service-pricing -->


        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>



        <section class="service-locations extra-pad-top color-bg-style2 no-zoom" data-section-title="<?php _e('Locations');?>">

            <div id="locations" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Web Hosting'); echo ' '; _e('Locations');?></h2>
                        <p><?php _e('Multiple premium data centers ensure high connectivity and better reach.'); ?></p>
                </div>

                <?php include_block("htmlblocks/locations-services.php", array('type' => 'web')); ?>

            </div> <!-- end of .container -->

        </section>


        <section class="service-features extra-pad" data-section-title="<?php _e('Features');?>">

            <div id="features" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Web Hosting Features')?></h2>
                </div>

                <div class="container-flex features-list">

                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-netspeed.svg" alt="High Network Speed" width="140" height="140">
                        <h3><?php _e('High Network Speed')?></h3>
                        <p><?php _e('1 Gbit network uplink ensures quick data downloads, uploads and speedy access to your server.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-controlpanels.svg" alt="Intuitive Control Panels" width="140" height="140">
                        <h3><?php _e('Intuitive Control Panels')?></h3>
                        <p><?php _e('Enjoy effortless website management! Choose from popular control panels - cPanel or DirectAdmin.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-1click.svg" alt="One-Click App Installation" width="140" height="140">
                        <h3><?php _e('Application Installer')?></h3>
                        <p><?php _e('Build your website on WordPress, Joomla, Drupal and enjoy more than 100 other useful apps.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-backup.svg" alt="Backup Management" width="140" height="140">
                        <h3><?php _e('Backup Management')?></h3>
                        <p><?php _e('Secure your data! Enjoy simplified backup management to make sure you have a file copy in case of emergency.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-emails.svg" alt="Unlimited Email Accounts" width="140" height="140">
                        <h3><?php _e('Unlimited Email Accounts')?></h3>
                        <p><?php _e('Create and manage multiple email addresses. Enhanced privacy and virus protection ensures flawless mailing activity.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-php.svg" alt="PHP Selector" width="140" height="140">
                        <h3><?php _e('PHP Selector')?></h3>
                        <p><?php _e('Choose your PHP version ranging from PHP 4.4 to PHP 7 to achieve the best work experience.')?></p>
                    </div>

                </div>

                <div class="row center">
                    <a href="#plans-table" class="button primary orange large" data-scrollto="plans-table"><?php _e('Compare Plans');?></a>
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-features -->


        <section class="service-cta p-v-40 color-bg-style2">

            <div class="container">

                <div class="container-flex cta">
                    <div class="block-col width-md-2-3 cta-left">
                        <h2 class=""><?php _e('Looking for more power?'); ?></h2>
                        <span class=""><?php _e('Try VPS hosting and create your own virtual machine.');?></span>
                    </div>
                    <div class="block-col width-md-1-3 cta-right">
                        <a href="<?php echo $config['links']['vps-hosting']; ?>" class="button xlarge ghost push-right"><?php _e('VPS Hosting')?></a>
                    </div>
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-cta -->


        <section class="service-faq extra-pad-top color-bg-style1" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/web-hosting.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->

    </section> <!-- end of .main -->

</article>

<?php

universal_redirect_footer([
    'en' => $site_en_url.'/web-hosting/',
    'br' => $site_br_url.'/hospedagem-de-sites/'
]);

get_footer(); 

?>
