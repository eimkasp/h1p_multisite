<?php 

session_start();

if( isset( $_GET['i'] ) ){
    $_SESSION['trial_i'] = $_GET['i'];
        
    $protocol = ( !empty( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] !== 'off' 
                    || $_SERVER['SERVER_PORT'] == 443 ) ? 'https://' : 'http://';
                    
    header('Location: '.$protocol.$_SERVER['HTTP_HOST'].'/trial-products/');
    exit;
}

if( !isset( $_SESSION['trial_i'] ) ){
    $protocol = ( !empty( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] !== 'off' 
                    || $_SERVER['SERVER_PORT'] == 443 ) ? 'https://' : 'http://';

    header('Location: '.$protocol.$_SERVER['HTTP_HOST']);
    exit;
}

$trial_i = $_SESSION['trial_i'];
$trial_data = unserialize( dariaus_decode( $trial_i, 'salT123' ) );

//cloud configs
$cloud_config_a[0] = dariaus_encode( serialize( [ 
        308 => [ 'optionid' => 2108, 'qty' => 15 ], //RAM
        297 => [ 'optionid' => 2045, 'qty' => 2 ],
        295 => [ 'optionid' => 1990, 'qty' => 2 ],
        294 => [ 'optionid' => 1989, 'qty' => 2 ],
        292 => [ 'optionid' => 1987, 'qty' => 2 ],
] ), 'salT123' );
//////////////////////////cloud configs end
//vps configs
$vps_config_a[0] = dariaus_encode( serialize( [ 
        518 => 3209, //cpu
        517 => 3202, //ram
        516 => 3196, //hdd
        515 => 3184, //IP
        514 => 3169, //Bandwidth
        513 => 3165, //Backups
] ), 'salT123' );
//////////////////////////vps configs end

get_header();

?>
<section class="block feedback">
    <div class="container">
        <div class="row">
            <h2 class="block-title">trial products feedback!</h2>
            
            <?php

                echo'<pre>';
                echo'<br>trial data:<br>';
                
                var_dump( $trial_data );
                echo'</pre><br>';


             ?>
            <h4 class="block-subtitle">Lorem Ipsum is simply dummy text of the printing  and typesetting industry.</h4>
            <div class="block-content"> 
                <?php if( $trial_data['trial_group'] == 'webhosting' ): ?>
                    <?php 
                        /*$wh_packages = file_get_contents( __DIR__ . '/../../htmlblocks/pricing-tables/web-hosting.php' );
                        // thanks to eval , i can edit included php code here
                        $wh_packages = str_replace( '', '', $wh_packages );
                        ////////////////////////////////////
                        eval( "?> $wh_packages <?php " );*/
                    ?>
                    <div class="plans-pricing-table web labeled-third labeled-fourth<?php echo (isset($expand) && $expand ? ' features-1' : '' ); ?>">
                        <div class="plan-col plan-col-1">
                            <div class="plan">
                                <div class="plan-block">
                                    <span class="plan-title">Starter</span>
                                    <span class="plan-descr">
                                        Suitable for business email and starter websites and for business email and starter websites...
                                        <span class="dash-underline-link" data-tooltip="pricing-plan-web-starter">
                                            More
                                            <div class="info-pop-up" data-tooltip-content="pricing-plan-web-starter">
                                                Usu quod tincidunt efficiantur ei
                                                <ul>
                                                    <li>Diceret deserunt disputationi ex pro,</li>
                                                    <li>est et sententiae deterruisset signiferumque</li>
                                                    <li>Insolens adipisci ad eos,</li>
                                                    <li>Vix ea enim essent probatus.</li>
                                                    <li>Simul tamquam invidunt ad sea</li>
                                                    <li>Pri magna soluta maiestatis ad, est id erroribus suscipiantur.</li>
                                                </ul>
                                                <span class="triangle"></span>
                                            </div>
                                        </span>
                                    </span>
                                    <span class="price"><span>$0.83</span><span class="sub full">/ month</span><span class="sub sort">/mo</span></span>
                                    <div class="drop-link-table">
                                        <form action="https://whmcs-clnom.host1plus.com/h1p_trials.php" method="post">
                                        <div class="cell-drop">
                                            <select name="bc" class="default-dropdown">
                                                <option value="monthly">12 months      <span>$0.83</span> / month</option>
                                                <option value="quarterly">3 months       <span>$0.83</span> / month</option>
                                                <option value="semiannually">6 months       <span>$0.83</span> / month</option>
                                                <option value="annually">12 months      <span>$0.83</span> / month</option>
                                                <option value="biennially">24 months      <span>$0.83</span> / month</option>
                                            </select>
                                        </div>
                                        <div class="cell-link">
                                            <input type="hidden" style="display:none;" name="product" value="263" />
                                            <input type="hidden" style="display:none;" name="i" value="<?= $trial_i ?>" />
                                            <input type="submit" class="button primary" value="Order Now" />
                                        </div>
                                        </form>
                                    </div>
                                    <div class="features">
                                        <div class="feature-col">
                                            <div class="feature">
                                                <span class="cell left">Bandwidth</span>
                                                <span class="cell right bold">50GB</span>
                                            </div>
                                        </div>
                                        <div class="feature-col">
                                            <div class="feature">
                                                <span class="cell left">Disk Space</span>
                                                <span class="cell right bold">1GB</span>
                                            </div>
                                        </div>
                                        <div class="feature-col">
                                            <div class="feature">
                                                <span class="cell left">Domain Name</span>
                                                <span class="cell right bold">$12.65 USD</span>
                                            </div>
                                        </div>
                                        <div class="feature-col">
                                            <div class="feature">
                                                <span class="cell left">Network Uptime</span>
                                                <span class="cell right bold green">99%</span>
                                            </div>
                                        </div>
                                        <div class="feature-col">
                                            <div class="feature">
                                                <span class="cell left">Email Accounts</span>
                                                <span class="cell right bold green">Unlimited</span>
                                            </div>
                                        </div>
                                        <div class="feature-col">
                                            <div class="feature">
                                                <span class="cell left">Email Accounts</span>
                                                <span class="cell right bold green">Unlimited</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php elseif(  $trial_data['trial_group'] == 'cloud' ): ?>
                <div class="plans-pricing-table labeled-first">
                    <div class="plan-col plan-col-1">
                        <div class="plan">
                            <div class="plan-block active">
                            <form action="https://whmcs-clnom.host1plus.com/h1p_trials.php" method="post">
                                <div class="cell-drop">
                                    <select name="bc" class="default-dropdown">
                                        <option value="monthly">12 months      <span>$0.83</span> / month</option>
                                        <option value="quarterly">3 months       <span>$0.83</span> / month</option>
                                        <option value="semiannually">6 months       <span>$0.83</span> / month</option>
                                        <option value="annually">12 months      <span>$0.83</span> / month</option>
                                        <option value="biennially">24 months      <span>$0.83</span> / month</option>
                                    </select>
                                </div>
                                <div class="drop-link-table">
                                    <div class="cell-link">
                                            <input type="hidden" style="display:none;" name="product" value="271" />
                                            <input type="hidden" style="display:none;" name="i" value="<?= $trial_i ?>" />
                                            <input type="hidden" style="display:none;" name="config" value="<?= $cloud_config_a[0] ?>" />
                                            <input type="submit" class="button primary" value="Order Now" />
                                    </div>
                                </div>
                                <span class="vps-price">VPS start from <span class="highlight">$2.00</span> /month</span>
                                <div class="features">
                                    <div class="feature-col">
                                        <div class="feature first left">
                                            <span class="cell left simple">CPU</span>
                                            <span class="cell right bold">0.5 Core</span>
                                        </div>
                                    </div>
                                    <div class="feature-col">
                                        <div class="feature first right">
                                            <span class="cell left">RAM</span>
                                            <span class="cell right bold">256 MB</span>
                                        </div>
                                    </div>
                                    <div class="feature-col">
                                        <div class="feature left">
                                            <span class="cell left">HDDD+</span>
                                            <span class="cell right bold">20 GB</span>
                                        </div>
                                    </div>
                                    <div class="feature-col">
                                        <div class="feature right">
                                            <span class="cell left">Bandwidth</span>
                                            <span class="cell right bold">500 GB</span>
                                        </div>
                                    </div>
                                    <div class="feature-col">
                                        <div class="feature last left">
                                            <span class="cell left">IPv4 Address</span>
                                            <span class="cell right bold">1 IP</span>
                                        </div>
                                    </div>
                                    <div class="feature-col">
                                        <div class="feature very-last right">
                                            <span class="cell left">Backup</span>
                                            <span class="cell right bold green">Available</span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php elseif(  $trial_data['trial_group'] == 'vps' ): ?>
                <div class="plans-pricing-table vps labeled-first">
                    <div class="plan-col plan-col-1">
                        <div class="plan">
                            <span class="info-text orange">Best value for your money</span>
                            <div class="plan-block active">
                                <span class="plan-title">Bronze</span>
                                <div class="features">
                                    <div class="feature-col">
                                        <div class="feature first left">
                                            <span class="cell left simple">CPU</span>
                                            <span class="cell right bold">0.5 Core</span>
                                        </div>
                                    </div>
                                    <div class="feature-col">
                                        <div class="feature first right">
                                            <span class="cell left">RAM</span>
                                            <span class="cell right bold">256MB</span>
                                        </div>
                                    </div>
                                    <div class="feature-col">
                                        <div class="feature left">
                                            <span class="cell left">HDD+SSD</span>
                                            <span class="cell right bold">20GB</span>
                                        </div>
                                    </div>
                                    <div class="feature-col">
                                        <div class="feature right">
                                            <span class="cell left">Bandwidth</span>
                                            <span class="cell right bold">0.5TB</span>
                                        </div>
                                    </div>
                                </div>
                                <form action="https://whmcs-clnom.host1plus.com/h1p_trials.php" method="post">
                                <div class="cell-drop">
                                    <select name="bc" class="default-dropdown">
                                        <option value="monthly">12 months      <span>$0.83</span> / month</option>
                                        <option value="quarterly">3 months       <span>$0.83</span> / month</option>
                                        <option value="semiannually">6 months       <span>$0.83</span> / month</option>
                                        <option value="annually">12 months      <span>$0.83</span> / month</option>
                                        <option value="biennially">24 months      <span>$0.83</span> / month</option>
                                    </select>
                                </div>
                                <div class="cell-link">
                                        <input type="hidden" style="display:none;" name="product" value="298" />
                                        <input type="hidden" style="display:none;" name="i" value="<?= $trial_i ?>" />
                                        <input type="hidden" style="display:none;" name="config" value="<?= $vps_config_a[0] ?>" />
                                        <input type="submit" class="button primary" value="Order Now" />
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php

get_footer();

function dariaus_encode($string, $key) {
    $string = $string . $key;

    $strLen = strlen($string);
    $key = md5($key);
    $keyLen = strlen($key);
    
    $hash = '';
    $j = 0;
    
    for( $i = 0; $i < $strLen; $i++ ){
        $ordStr = ord( substr( $string, $i, 1) );
        if( $j == $keyLen ) $j = 0;
        $ordKey = ord( substr( $key, $j, 1) );
        $j++;
        $hash .= strrev( base_convert( dechex( $ordStr + $ordKey ), 16, 36) );
    }
    return $hash;
}

function dariaus_decode($string, $key) {
    $string = $string;

    $realkeyLen = strlen($key);
    
    $key = md5($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    
    $hash = '';
    $j = 0;
    
    for( $i = 0; $i < $strLen; $i+=2 ) {
        $ordStr = hexdec( base_convert( strrev( substr( $string, $i, 2) ), 36, 16) );
        if($j == $keyLen) $j = 0;
        $ordKey = ord( substr( $key, $j, 1) );
        $j++;
        $hash .= chr( $ordStr - $ordKey );
    }
    $hash = substr( $hash, 0, -( $realkeyLen ) );
    return $hash;
}
?>