<?php get_header(); ?>

<section class="block feedback">
    <div class="container">
        <div class="row">


            <?php if (checkUserAlreadyReviewed("", get_client_ip()) < 1) { ?>

                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                    <input type="hidden" name="feedbackuserform" value="welcomeback" />
                    <input type="hidden" name="user" value="marius@host1plus.com" />
                    <h2 class="block-title">Leave your feedback here!</h2>
                    <h4 class="block-subtitle">Lorem Ipsum is simply dummy text of the printing  and typesetting industry.</h4>
                    <div class="block-content"> 

                        <div class="feedback-service">
                            <div class="feedback-block-title">Select the service you were using:</div>

                            <div class="feedback-content">   

                                <div class="bigcheckboxes left">
                                    <ul>
                                        <li>
                                            <label class="bigcheck">
                                                <input type="checkbox" class="bigcheck" name="service[sharedhosting]" value="yes"/>
                                                <span class="bigcheck-target"></span>
                                                Shared Hosting
                                            </label>
                                        </li>
                                        <li>
                                            <label class="bigcheck">
                                                <input type="checkbox" class="bigcheck" name="service[vpshosting]" value="yes"/>
                                                <span class="bigcheck-target"></span>
                                                VPS Hosting
                                            </label>
                                        </li>
                                        <li>
                                            <label class="bigcheck">
                                                <input type="checkbox" class="bigcheck" name="service[cloudhosting]" value="yes"/>
                                                <span class="bigcheck-target"></span>
                                                Cloud Hosting
                                            </label>
                                        </li>
                                    </ul>
                                </div>

                                <div class="bigcheckboxes">
                                    <ul>
                                        <li>
                                            <label class="bigcheck ">
                                                <input type="checkbox" class="bigcheck" name="service[resellerhosting]" value="yes"/>
                                                <span class="bigcheck-target"></span>
                                                Reseller Hosting
                                            </label>
                                        </li>
                                        <li>
                                            <label class="bigcheck">
                                                <input type="checkbox" class="bigcheck" name="service[dedicatedservers]" value="yes"/>
                                                <span class="bigcheck-target"></span>
                                                Dedicated Servers
                                            </label>
                                        </li>
                                        <li>
                                            <label class="bigcheck">
                                                <input type="checkbox" class="bigcheck" name="service[other]" value="yes"/>
                                                <span class="bigcheck-target"></span>
                                                Other
                                            </label>
                                        </li>
                                    </ul>
                                </div>                        

                            </div>

                        </div>

                        <div class="feedback-service">
                            <div class="feedback-block-title">Rate your overall experience:</div>
                            <div class="feedback-content">                           

                                <script>
                                    $(document).ready(function () {
                                        //  Check Radio-box
                                        $(".rating input:radio").attr("checked", false);
                                        $('.rating input').click(function () {

                                            var group = $(this).attr('name');
                                            console.log(group);
                                            var selectedVal = $(this).attr('value');

                                            console.log(selectedVal);
                                            $('#' + group + '').val(selectedVal);
                                            console.log('#' + group + '' + selectedVal);

                                            $(".rating span." + group).removeClass('checked');

                                            $("input[name$='" + group + "']").each(function (i) {
                                                if (selectedVal >= $(this).attr('value')) {
                                                    $(this).parent().addClass('checked');

                                                }
                                            });
                                        });
                                    });
                                </script>


                                <div class="ratings-block">
                                    <span class="rating-label">Features:</span>
                                    <input type="hidden" name="question[1]" value="Features">
                                    <span class="rating">
                                        <span class="rating1"><input type="radio" name="rating1" id="str1-5" value="5"><label for="str1-5"></label></span>
                                        <span class="rating1"><input type="radio" name="rating1" id="str1-4" value="4"><label for="str1-4"></label></span>
                                        <span class="rating1"><input type="radio" name="rating1" id="str1-3" value="3"><label for="str1-3"></label></span>
                                        <span class="rating1"><input type="radio" name="rating1" id="str1-2" value="2"><label for="str1-2"></label></span>
                                        <span class="rating1"><input type="radio" name="rating1" id="str1-1" value="1"><label for="str1-1"></label></span>
                                        <input type="hidden" id="rating1" name="answer[1]" value="0" />
                                    </span>                         
                                    <!--<span class="rating">
                                        <span class="rating1"><input type="radio" name="answer[1] rating[1]" id="str1-5" value="5"><label for="str1-5"></label></span>
                                        <span class="rating1"><input type="radio" name="rating[1]" id="str1-4" value="4"><label for="str1-4"></label></span>
                                        <span class="rating1"><input type="radio" name="rating[1]" id="str1-3" value="3"><label for="str1-3"></label></span>
                                        <span class="rating1"><input type="radio" name="rating[1]" id="str1-2" value="2"><label for="str1-2"></label></span>
                                        <span class="rating1"><input type="radio" name="rating[1]" id="str1-1" value="1"><label for="str1-1"></label></span>
                                    </span>            -->                   

                                </div>

                                <div class="ratings-block">
                                    <span class="rating-label">Performance:</span> 
                                    <input type="hidden" name="question[2]" value="Performance">
                                    <span class="rating">
                                        <span class="rating2"><input type="radio" name="rating2" id="str2-5" value="5"><label for="str2-5"></label></span>
                                        <span class="rating2"><input type="radio" name="rating2" id="str2-4" value="4"><label for="str2-4"></label></span>
                                        <span class="rating2"><input type="radio" name="rating2" id="str2-3" value="3"><label for="str2-3"></label></span>
                                        <span class="rating2"><input type="radio" name="rating2" id="str2-2" value="2"><label for="str2-2"></label></span>
                                        <span class="rating2"><input type="radio" name="rating2" id="str2-1" value="1"><label for="str2-1"></label></span>
                                        <input type="hidden" id="rating2" name="answer[2]" value="0" />
                                    </span>                                       
                                </div>

                                <div class="ratings-block">
                                    <span class="rating-label">Network Speed:</span> 
                                    <input type="hidden" name="question[3]" value="Network speed">
                                    <span class="rating">
                                        <span class="rating3"><input type="radio" name="rating3" id="str3-5" value="5"><label for="str3-5"></label></span>
                                        <span class="rating3"><input type="radio" name="rating3" id="str3-4" value="4"><label for="str3-4"></label></span>
                                        <span class="rating3"><input type="radio" name="rating3" id="str3-3" value="3"><label for="str3-3"></label></span>
                                        <span class="rating3"><input type="radio" name="rating3" id="str3-2" value="2"><label for="str3-2"></label></span>
                                        <span class="rating3"><input type="radio" name="rating3" id="str3-1" value="1"><label for="str3-1"></label></span>
                                        <input type="hidden" id="rating3" name="answer[3]" value="0" />
                                    </span>                                          
                                </div>

                                <div class="ratings-block">
                                    <span class="rating-label">Reliability:</span> 
                                    <input type="hidden" name="question[4]" value="Reliability">
                                    <span class="rating">
                                        <span class="rating4"><input type="radio" name="rating4" id="str4-5" value="5"><label for="str4-5"></label></span>
                                        <span class="rating4"><input type="radio" name="rating4" id="str4-4" value="4"><label for="str4-4"></label></span>
                                        <span class="rating4"><input type="radio" name="rating4" id="str4-3" value="3"><label for="str4-3"></label></span>
                                        <span class="rating4"><input type="radio" name="rating4" id="str4-2" value="2"><label for="str4-2"></label></span>
                                        <span class="rating4"><input type="radio" name="rating4" id="str4-1" value="1"><label for="str4-1"></label></span>
                                        <input type="hidden" id="rating4" name="answer[4]" value="0" />
                                    </span>    
                                </div>

                                <div class="ratings-block">
                                    <span class="rating-label">Support:</span> 
                                    <input type="hidden" name="question[5]" value="Support">
                                    <span class="rating">
                                        <span class="rating5"><input type="radio" name="rating5" id="str5-5" value="5"><label for="str5-5"></label></span>
                                        <span class="rating5"><input type="radio" name="rating5" id="str5-4" value="4"><label for="str5-4"></label></span>
                                        <span class="rating5"><input type="radio" name="rating5" id="str5-3" value="3"><label for="str5-3"></label></span>
                                        <span class="rating5"><input type="radio" name="rating5" id="str5-2" value="2"><label for="str5-2"></label></span>
                                        <span class="rating5"><input type="radio" name="rating5" id="str5-1" value="1"><label for="str5-1"></label></span>
                                        <input type="hidden" id="rating5" name="answer[5]" value="0" />
                                    </span>                                     
                                </div>

                                <div class="ratings-block">
                                    <span class="rating-label">Customer Care:</span> 
                                    <input type="hidden" name="question[6]" value="Customer Care">
                                    <span class="rating">
                                        <span class="rating6"><input type="radio" name="rating6" id="str6-5" value="5"><label for="str6-5"></label></span>
                                        <span class="rating6"><input type="radio" name="rating6" id="str6-4" value="4"><label for="str6-4"></label></span>
                                        <span class="rating6"><input type="radio" name="rating6" id="str6-3" value="3"><label for="str6-3"></label></span>
                                        <span class="rating6"><input type="radio" name="rating6" id="str6-2" value="2"><label for="str6-2"></label></span>
                                        <span class="rating6"><input type="radio" name="rating6" id="str6-1" value="1"><label for="str6-1"></label></span>
                                        <input type="hidden" id="rating6" name="answer[6]" value="0" />
                                    </span>                   
                                </div>

                                <div class="ratings-block">
                                    <span class="rating-label">Price:</span> 
                                    <input type="hidden" name="question[7]" value="Price">
                                    <span class="rating">
                                        <span class="rating7"><input type="radio" name="rating7" id="str7-5" value="5"><label for="str7-5"></label></span>
                                        <span class="rating7"><input type="radio" name="rating7" id="str7-4" value="4"><label for="str7-4"></label></span>
                                        <span class="rating7"><input type="radio" name="rating7" id="str7-3" value="3"><label for="str7-3"></label></span>
                                        <span class="rating7"><input type="radio" name="rating7" id="str7-2" value="2"><label for="str7-2"></label></span>
                                        <span class="rating7"><input type="radio" name="rating7" id="str7-1" value="1"><label for="str7-1"></label></span>
                                        <input type="hidden" id="rating7" name="answer[7]" value="0" />
                                    </span>                                        
                                </div>

                            </div>
                        </div>

                        <div class="feedback-service">
                            <div class="feedback-block-title">How did we do ?</div>
                            <div class="feedback-content">
                                <textarea name="comment" rows="10" style="height: 200px;" placeholder="Enter your comment"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="block-footer">
                        <button type="submit" class="button primary">Submit</button>
                    </div>
                </form>
            <?php } else { ?>
                <h2 class="block-title">Leave your feedback here!</h2>
                <h4 class="block-subtitle">Thank you for review.</h4>
            <?php } ?>
        </div>
    </div>
</section>



<?php

function insertFeedback($postData) {
    if (sizeof($postData > 0)) {
        global $wpdb;

        $feedbackform = $postData['feedbackuserform'];
        $user = $postData['user'];
        $services = json_encode($postData['service']);
        $questions = json_encode($postData['question']);
        $answers = json_encode($postData['answer']);
        $comment = $postData['comment'];
        $ip = get_client_ip();
        $time = time();
        
        $hasReview = checkUserAlreadyReviewed($user, $ip);
        
        if ($hasReview >= 1) {
            //echo 'PASOL NAXUJ';
            //echo '<h4 class="block-subtitle">Thank you for review.</h4>';
        } else {
            $wpdb->query(
                    $wpdb->prepare(
                            "INSERT INTO `wp_feedbacks` SET `unixtimestamp`=$time, `services`='$services', `questions`='$questions', `answers`='$answers', `comment`='$comment', `user`='$user', `feedbackform`='$feedbackform', `ipaddress`='$ip'", 8
                    )
            );
            echo "<meta http-equiv=refresh content=\"0; URL=\">";
        }
    }
}

function checkUserAlreadyReviewed($user, $ip) {

    $userStr = $ipStr = "";
    if ($user != "") {
        $userStr = " WHERE `user`='$user' ";
    }
    if ($ip != "") {
        if ($userStr == "") {
            $ipStr = " WHERE `ipaddress`='$ip' ";
        } else {
            $ipStr = " OR `ipaddress`='$ip' ";
        }
    }

    if ($userStr != "" || $ipStr != "") {
        global $wpdb;
        $result = $wpdb->query("SELECT id FROM `wp_feedbacks` $userStr $ipStr");
        return $result;
    } else {
        return 0;
    }
}

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

if (isset($_POST) && sizeof($_POST) > 0) {
    insertFeedback($_POST);
}

get_footer();
?>