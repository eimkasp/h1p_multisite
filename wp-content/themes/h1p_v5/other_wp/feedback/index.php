<?php 

$tpl_file = __DIR__ . '/' . $post->post_name . '.php';

if( file_exists( $tpl_file ) ){
    require_once $tpl_file;
}
else{
    require_once __DIR__ . '/feedback.php';
}