<?php

require_once __DIR__ . '/PapApi.class.php';

require_once __DIR__ . '/../../../../wp-load.php';
require_once __DIR__ . '/../config.php';



// Some values mapping for PAP response language selection
$lang = 'pt-br';  // default
if (get_locale() == 'pt_BR') {
    $lang = 'pt-br';
}
// Setup some translations:
// Just declarations for gettext to catch
$t1 = __('No rows found');
$t2 = __('No rows found!');


$response = ['success' => false, 'message' => __('Unknown error.') ];
if( !isset( $_POST['email'] ) ){
    $response = ['success' => false, 'message' => __('Missing data.') ];
}
else{

    $session = new Gpf_Api_Session( AFF_SYSTEM_LINK."/scripts/server.php");

    if(!@$session->login( AFF_MERCHANT_EMAIL, AFF_MERCHANT_PASS, Gpf_Api_Session::MERCHANT, $lang)) {
        $response = ['success' => false, 'message' => __('Error while communicating with PAP: ') . $session->getMessage() ];
    }
    else{
      var_dump ($session);
      
      $affiliate = new Pap_Api_Affiliate($session);
      $affiliate->setUsername( $_POST['email'] );
      //$affiliate->setLanguage($lang);
      try {
        $affiliate->load();

        $userid= $affiliate->getUserid();

        // request for the password reset email
        $request = new Gpf_Rpc_Request('Pap_Merchants_User_AffiliateForm', 'sendRequestPassword', $session);
        /*
        * if you know the ID you can insert it directly here in place of $userid.
        * you can also enter multiple IDs if you want to send such email to multiple affiliates at once,
        * in such case it would be for example $request->addParam('ids', new Gpf_Rpc_Array(array('11111111','979ccb9b','761b2a5d')));
        */
        $request->addParam('ids', new Gpf_Rpc_Array(array($userid))); //ID of the affiliate to whom we are sending the email

        try {
          $request->sendNow();
        } catch(Exception $e) {
          $response = ['success' => false, 'message' => __($e->getMessage()) ];
        }

        $response = $request->getStdResponse();

        if ($response->success == 'Y') {
          $response = ['success' => true, 'message' => __($response->infoMessage) ];
        } else {
          $response = ['success' => false, 'message' => __($response->errorMessage) ];
        }

      } catch (Exception $e) {
        $response = ['success' => false, 'message' => __($e->getMessage()) ];
      }
    }

}
echo json_encode( $response );
