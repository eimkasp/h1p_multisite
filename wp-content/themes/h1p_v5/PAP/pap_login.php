<?php

require_once __DIR__ . '/PapApi.class.php';

require_once __DIR__ . '/../../../../wp-load.php';
require_once __DIR__ . '/../config.php';



// Response will be in PAP with the language that is set from $session->login()

if( !isset( $_POST['email'] ) || !isset( $_POST['password'] ) ){
    $response = ['success' => false, 'message' => __('Missing data.') ];
}
else{

    $session = new Gpf_Api_Session( AFF_SYSTEM_LINK."/scripts/server.php");
    //$session->setDebug(true);

	if( !@$session->login( $_POST['email'], $_POST['password'], Gpf_Api_Session::AFFILIATE, $_POST['language']) ) {
        $response = ['success' => false, 'message' => $session->getMessage() ];
    }
    else{
        
        try {
        	$response = ['success' => true, 'redirect' => $session->getUrlWithSessionInfo( AFF_SYSTEM_LINK .'/affiliates/panel.php')];
        } catch(Exception $e) {
            $response = ['success' => false, 'message' => __($e->getMessage()) ];
        }
    }

}

echo json_encode( $response );
