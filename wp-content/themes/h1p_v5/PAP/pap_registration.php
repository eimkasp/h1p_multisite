<?php

require_once __DIR__ . '/PapApi.class.php';

require_once __DIR__ . '/../../../../wp-load.php';
require_once __DIR__ . '/../config.php';


// Some values mapping for PAP response language selection
$lang = 'en-US';  // default
if (get_locale() == 'pt_BR') {
    $lang = 'pt-br';
}
// So PAP will respond in the selected language. No local translations needed.



if( !isset( $_POST['paypal_email'] ) || filter_var( $_POST['paypal_email'], FILTER_VALIDATE_EMAIL) === false ){
    $response = ['success' => false, 'message' => __('Paypal email is incorrect.'), 'error' => 'ppe' ];
}
else{

    $session = new Gpf_Api_Session( AFF_SYSTEM_LINK."/scripts/server.php");
    if(!@$session->login( AFF_MERCHANT_EMAIL, AFF_MERCHANT_PASS, Gpf_Api_Session::MERCHANT, $lang)) {
        $response = ['success' => false, 'message' => __('Error while communicating with PAP: ') . __($session->getMessage()) ];
    }
    else{
      //username - email
      //firstname
      //lastname
      //refID
      //parrentuserid

      //data1 [All of Your Websites URL]
      //data2 [Company name]
      //data3 [Address]
      //data4 [City]
      //data5 [State]
      //data6 [Country]
      //data7 [ZIP]
      //data8 [Phone]
      //data9 [Fax] --
      //data10 [Skype ID]
      //data11 [How do you plan to advertise Host1Plus?]
      //data12 -- ...

      $affiliate = new Pap_Api_Affiliate( $session );
      $affiliate->setUsername( $_POST['email'] );
      $affiliate->setFirstname( $_POST['firstname'] );
      $affiliate->setLastname( $_POST['lastname'] );
      $affiliate->setRefid( $_POST['accountref'] );
      /* ! language must be set in the $session->login() */

      //var_dump( $_POST );die();

      if( in_array('My own website', $_POST['advertise']) ){
        $affiliate->setData( 1, $_POST['website'] );
      }
      else{
        $affiliate->setData( 1, '-' );
      }

      if( $_POST['acctype'] == 'business' ){
        $affiliate->setData( 2, $_POST['company'] );
      }
      else{
        $affiliate->setData( 2, '-' );
      }
      $affiliate->setData( 6, $_POST['country'] );
      $affiliate->setData( 8, $_POST['phone'] );

      $affiliate->setData( 10, $_POST['skype'] );

      $how_to_adv = '';
      foreach( $_POST['advertise'] as $adv ){
        if( $adv == 'Other' ){
          $how_to_adv .= $adv.": ".$_POST['other_adv'].";\r\n";
        }
        else{
          $how_to_adv .= $adv.";\r\n";
        }
      }
      $affiliate->setData( 11, $how_to_adv );

      $affiliate->setPayoutOptionId( '8444af30' ); // 8444af30 - paypal
      $affiliate->setPayoutOptionField( 'pp_email', $_POST['paypal_email'] );

      $affiliate->setStatus('P');
      $affiliate->setNote('Registered through WP');

      $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      $temp_aff_pass = substr(str_shuffle($chars), 0, 8);

      $affiliate->setPassword( $temp_aff_pass );


      try {
        if ($affiliate->add()) {
          $response = ['success' => true, 'message' => '' ];
        } else {
          $response = ['success' => false, 'message' => __($affiliate->getMessage()) ];
        }
      } catch (Exception $e) {
          $response = ['success' => false, 'message' => __('Error while communicating with PAP: ') . __($e->getMessage()) ];
      }
    }

}

echo json_encode( $response );
