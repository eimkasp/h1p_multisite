<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Abuse report
*/

if(get_locale() == 'pt_BR') {
    $lang_code = 'pt-BR';
} else {
    $lang_code = 'en';
}

wp_enqueue_script('recaptcha', 'https://www.google.com/recaptcha/api.js?&hl=' . $lang_code, [], false, true);
wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);


global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$custom_fields = get_post_custom();
$abuse_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

get_header();

?>

<article class="page abuse">

    <header class="headline office">
        <div class="container adjust-vertical-center">
            <h1 class="page-title"><?php _e('Report Abuse')?></h1>
            <div class="title-descr"><?php _e('All abuse reports related to websites hosted by us are immediately put under investigation.')?></div>
        </div>
    </header> <!-- end of .headline.careers -->

    <section class="main page-content">


        <section class="positions extra-pad-top">

            <div class="container">

                <p><?php printf(__('All reports are reviewed and inspected promptly to ensure that our network is used in accordance to our %sTerms of Service%s. If you are aware of someone breaching our terms or the law whilst using our services, please use the form below to report this - including as much detail as possible to enable us to effectively investigate your complaint.'), '<a href="'. $config['links']['terms_of_service'].'">','</a>'); ?></p>

                <div>
                    <h3 class="title-follow-text center p-t-30 p-b-10"><?php _e('What type of malicious behavior have you encountered?')?></h3>
                </div>

                <div class="layout-row one-col-row">

                    <div class="block-col">

                        <div class="expand-onclick radio" data-topic="Infringing on Copyright">
                            <?php _e('Infringing on Copyright')?>

                            <div class="description">
                                <p><?php _e('If you believe a website hosted with Host1Plus is infringing on your copyright you can report this here and we can pass on your complaint to our customer where possible and if necessary, we may also take action.');?></p>
                                <p><?php printf(__('We do however advise that you try to contact the website operator first, you can usually find their information on the Domain WHOIS result, you can check this %shere%s.'),'<a target="_blank" href="https://apps.db.ripe.net/search/query.html">','</a>');?></p>
                                <a class="m-v-20 display-block" href="#abuse-form" data-activate="form-block" data-scrollto="abuse-form"><?php _e('Report');?></a>
                            </div>
                        </div> <!-- end of .expand-onclick -->

                        <div class="expand-onclick radio" data-topic="Malware/Phishing">
                            <?php _e('Malware/Phishing')?>

                            <div class="description">
                                <p><?php _e('We take malware and other fraudulent activity on our network(s) very seriously and will take all action necessary to remove it once detected.');?></p>
                                <p><?php _e('Please ensure you provide as much information as possible to ensure we can investigate malware/phishing reports. Please include screenshots, URLs, email headers and any other information which will assist.');?></p>
                                <a class="m-v-20 display-block" href="#abuse-form" data-activate="form-block" data-scrollto="abuse-form"><?php _e('Report');?></a>
                            </div>
                        </div> <!-- end of .expand-onclick -->

                        <div class="expand-onclick radio" data-topic="SPAM">
                            <?php _e('SPAM')?>

                            <div class="description">
                                <p><?php _e('We do not tolerate the sending of UBE/UCE or "SPAM" on our network, please ensure you provide as much detail as possible in your report, including Email headers and any other relevant information to ensure we can properly track the source of unsolicited email.');?></p>
                                <a class="m-v-20 display-block" href="#abuse-form" data-activate="form-block" data-scrollto="abuse-form"><?php _e('Report');?></a>
                            </div>
                        </div> <!-- end of .expand-onclick -->

                        <div class="expand-onclick radio" data-topic="Unacceptable or Illegal Material">
                            <?php _e('Unacceptable or Illegal Material')?>

                            <div class="description">
                                <p><?php _e('We will not tolerate any unacceptable or illegal material hosted on our servers. Such material includes (but is not limited to):');?></p>
                                <ul class="p-v-10">
                                    <li><?php _e('Gambling websites;');?></li>
                                    <li><?php _e('Websites promoting illegal activities;');?></li>
                                    <li><?php _e('Child pornography;');?></li>
                                    <li><?php _e('Racist content;');?></li>
                                    <li><?php _e('Incitement of strife, hatred or violence;');?></li>
                                    <li><?php _e('Other abusive websites.');?></li>
                                </ul>
                                <p><?php _e('Please include screenshots, URLs or correspondence and any other information which will assist.');?></p>
                                <a class="m-v-20 display-block" href="#abuse-form" data-activate="form-block" data-scrollto="abuse-form"><?php _e('Report');?></a>
                            </div>
                        </div> <!-- end of .expand-onclick -->

                        <div class="expand-onclick radio" data-topic="Other">
                            <?php _e('Other')?>

                            <div class="description">
                                <a class="m-v-20 display-block" href="#abuse-form" data-activate="form-block" data-scrollto="abuse-form"><?php _e('Report');?></a>
                            </div>

                        </div> <!-- end of .expand-onclick -->

                    </div>

                </div>

                <div class="scroll-id-target" id="abuse-form"></div>

                <div id="form-abuse" class="form-block hidden">

                    <?php echo do_shortcode('[contact-form-7 id="'.$abuse_form_id.'" title="Report abuse" html_class="wpcf7-form captcha"]');?>

                    <script>

                        $(document).ready(function(){

                            $('#form-abuse').hide();
                            $('#form-abuse').removeClass('hidden');

                            function updateFormSelect (elemVal){
                                $('select[name=subject]').val(elemVal);
                            }

                            $('.expand-onclick').click(function(){
                                var topic = $(this).attr('data-topic');
                                updateFormSelect(topic);
                            });

                            $('a[data-activate="form-block"]').click(function(){
                                $('#form-abuse').slideDown();
                            });

                        });

                    </script>

                </div>

            </div>

        </section>

    </section>

</article>

<?php

universal_redirect_footer([
    'en' => $site_en_url.'/abuse/',
    'br' => $site_br_url.'/informar-abuso/'
]);

get_footer(); 

?>