<?php
/**
* @package WordPress
* @subpackage h1p_v4
*
* Template Name: Sponsorship
*/

wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);

if(get_locale() == 'pt_BR') {
    $lang_code = 'pt-BR';
} else {
    $lang_code = 'en';
}
wp_enqueue_script('recaptcha', 'https://www.google.com/recaptcha/api.js?&hl=' . $lang_code, [], false, true);


get_header();


$custom_fields = get_post_custom();
$internship_form_id = (isset($custom_fields['form_id']) && count($custom_fields['form_id'])) ? $custom_fields['form_id'][0] : false;

?>

<article class="page sponsorship">

    <header class="headline sponsorship">
        <div class="container adjust-vertical-center">
            <h1 class="page-title"><?php _e('Sponsorship Program')?></h1>
            <div class="title-descr"><?php _e('Helping others is one of the core values of our business – we are happy to work for a good cause and contribute to projects that make a difference.')?></div>

        </div>
    </header> <!-- end of .headline.sponsorship -->

    <section class="main page-content">


        <section class="extra-pad-top">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Let us know about your work!')?></h2>
                    <span class="title-text m-t-20"><?php _e('Host1Plus supports worldwide initiatives, aimed at creating opportunities, building communities, promoting education and innovation. We are always ready to give a hand and make brilliant ideas persist!'); ?></span>
                </div>

                <div class="info-apply-block">
                    <div class="info-block white-bg">
                        <h3 class="info-block-title"><?php _e('The areas we look to sponsor are:')?></h3>
                        <div class="lists">
                            <ul>
                                <li><?php _e('Arts and culture')?></li>
                                <li><?php _e('Sports')?></li>
                                <li><?php _e('Environment')?></li>
                                <li><?php _e('Health and wellness')?></li>
                                <li><?php _e('Social communities')?></li>
                            </ul>
                            <ul>
                                <li><?php _e('Education')?></li>
                                <li><?php _e('Non-profit/charity')?></li>
                                <li><?php _e('Science and research')?></li>
                                <li><?php _e('Open source projects')?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="info-block">
                        <div class="info-block-title"><?php _e('Interested?')?></div>
                        <div><?php printf(__('If you are interested in our sponsorship program, contact us at %ssponsorship@host1plus.com%s for more information or apply now by filling the form below.'), '<a href="mailto:sponsorship@host1plus.com">', '</a>');?></div>
                        <a href="#application-form-header" class="button primary apply" data-scrollto="application-form-header"><?php _e('Apply Now')?></a>
                    </div>
                </div>

                <div class="separator"></div>


                <div class="content-block our-sponsorships">

                    <div class="section-header m-t-20">
                        <h3 class="our-title"><?php _e('Our Sponsorships')?></h3>
                    </div>

                    <div class="block-of-fourcross">
                        <div class="layout-row two-col-row">
                            <div class="block-col center">
                                <div class="ss-table">
                                    <div class="icon-part">
                                        <div class="ss-icon centos"></div>
                                    </div>
                                    <div class="text-part">
                                        <div class="text-block">
                                            <?php _e('The CentOS Project is a community-driven free software effort focused around the goal of providing a rich base platform for open source communities to build upon.')?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block-col center">
                                <div class="ss-table">
                                    <div class="icon-part">
                                        <div class="ss-icon chakra"></div>
                                    </div>
                                    <div class="text-part">
                                        <div class="text-block">
                                            <?php _e('Chakra is a GNU/Linux distribution based on free and open source software with a focus on the Plasma desktop by KDE and Qt applications. It is intended for new and experienced users alike.')?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layout-row two-col-row">
                            <div class="block-col center">
                                <div class="ss-table">
                                    <div class="icon-part">
                                        <div class="ss-icon fedora"></div>
                                    </div>
                                    <div class="text-part">
                                        <div class="text-block">
                                            <?php _e('The Fedora Project is a global community of free software enthusiasts and professionals who build leading-edge, community-supported Linux solutions for servers, workstations and the Cloud.')?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block-col center">
                                <div class="ss-table">
                                    <div class="icon-part">
                                        <div class="ss-icon limsa"></div>
                                    </div>
                                    <div class="text-part">
                                        <div class="text-block">
                                            <?php _e('Lithuanian Medical Students\' Association (LiMSA) is a non-governmental youth organization uniting more than 500 medical students for common goals of improving public health, spreading ideas of human rights and educating young people.')?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layout-row two-col-row">
                            <div class="block-col center ">
                                <div class="ss-table">
                                    <div class="icon-part">
                                        <div class="ss-icon mb"></div>
                                    </div>
                                    <div class="text-part">
                                        <div class="text-block">
                                            <?php _e('Maisto Bankas is a non-profit, charitable organization. In cooperation with other social non-profit organizations and institutions, it provides food aid for the needy.')?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block-col center last-block-col">
                                <div class="ss-table">
                                    <div class="icon-part">
                                        <div class="ss-icon archlinux"></div>
                                    </div>
                                    <div class="text-part">
                                        <div class="text-block">
                                            <?php _e('Arch Linux Brasil is a community project dedicated to help new Arch Linux distribution users as well as the development of Arch Linux itself.')?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div> <!-- end of .container -->

        </section>

        <section id="application-form">

            <div class="container">
                <div class="section-header">
                    <h2 id="application-form-header" class="form-title"><?php _e('Application Form')?></h2>
                    <span class="title-text m-t-20"><?php _e('Apply for our sponsorship program and leave all your worries to us! It is our duty to make sure that you have all you need to keep your project up and running smoothly.')?></span>
                </div>

                <?php echo do_shortcode('[contact-form-7 id="'.$internship_form_id.'" title="Internships" html_class="wpcf7-form captcha"]'); ?>
            </div>

        </section>

    </section>


</article>

<?php 

universal_redirect_footer([
    'en' => $site_en_url.'/sponsorship/',
    'br' => $site_br_url.'/patrocinio/'
]);

get_footer(); 

?>
