<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Cloud servers landing
*/

wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);
wp_enqueue_script('jquery.sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.js', ['jquery'], '1.0.3', true);
wp_enqueue_script('particles', 'https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js', ['jquery'], '2.0.0', true);


global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

$mypage = PageData::getInstance($config, 'cs_lin');

/*Get Cloud IP config option pricing
-------------------------*/
// id=342

$cloud_hosting_config_options = $whmcs->getConfigurablePrices(342);
$cloud_hosting_ip = $cloud_hosting_config_options['IP']['options']['IP']['pricing']['monthly'];
/*echo'<pre>';
var_dump($cloud_hosting_config_options['IP']['options']['IP']['pricing']['monthly']);
echo'</pre>';
exit;*/

// ---------------------------------
$header_not_fixed = true;   // this will be global var in header
// ---------------------------------

get_header();

?>
<article class="page landing cloud-servers cs-hosting">

    <header class="headline cs-hosting">
        <div class="container adjust-vertical-center">

        
            <div class="hot-news-wrapper">
                <div class="hot-news-overflow-container">
                    <div class="hot-news-group">
                        <div class="hot-news-handle news"><?php _e('News'); ?> <i class="fa fa-angle-right"></i></div>
                        <div class="hot-news-message"><a href="http://www.host1plus.com/blog/news/host1plus-launches-cloud-servers-in-sao-paulo-brazil" target="_blank" title="<?php _e('Host1Plus Launches Cloud Servers ßeta in São Paulo, Brazil!'); ?>"><?php _e('Host1Plus Launches Cloud Servers ßeta in São Paulo, Brazil!'); ?></a></div>
                    </div>
                </div> <!-- end of .hot-news-overflow-container -->
            </div>
        
        
            <h1 class="page-title"><?php _e('Cloud Servers')?> <span class="highlight"><?php _e('from')?> <?php echo $mypage->getMinPrice(); ?></span></h1>
            <div class="title-descr"><?php _e('Cloud computing made simple.')?></div>

        </div>
    </header> <!-- end of .headline.vps-hosting -->

    <?php 
    
        if(get_locale() == 'en_US'){
            $promo_loc = 'en';
        } elseif (get_locale() == 'pt_BR'){
            $promo_loc = 'br';
        }            
        
    ?>

    <div class="cs-promo-withheader">
    </div>

    <section class="main page-content">

        <section class="service-pricing" data-section-button="<?php _e('View Plans');?>">

            <div class="cs-promo-section-wide"></div>


            <div class="container">

                <div class="scroll-id-target" id="plans-table"></div>

                
                <div class="cs-promo-container-wide"></div>

                <div class="segment-wrapper roundlike size-large non-responsive p-t-20">
                    <select name="plan-switch" class="segment-select">
                        <option value="linux" data-icosrc="<?php echo $images;?>img/ico_os_linux.svg"><?php _e('Linux OS'); ?></option>
                        <option value="windows" data-icosrc="<?php echo $images;?>img/ico_os_windows.svg"><?php _e('Windows OS'); ?> </option>
                    </select>
                </div>

                <div id="block-linux" class="block-switchable">

                    <?php include_block("htmlblocks/pricing-tables/cs-hosting-lin-v2.php"); ?>

                </div>

                <div id="block-windows" class="block-switchable">

                    <?php include_block("htmlblocks/pricing-tables/cs-hosting-win-v2.php"); ?>

                </div>

                <?php include_block("htmlblocks/pricing-tables/cs-all-plans-include.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-pricing -->

        <div id="context-menu-cloud-servers" class="sticky-menu sticky-nav-landing">
            <div class="container">
                <!-- will be populated -->
            </div>
        </div>


        <section class="service-features service-bluepic-bg extra-pad color-bg-style4" data-section-title="<?php _e('Features');?>">

            <div id="particles-js" style="position: absolute; width: 100%; height: 100%; top: 0;"></div>
            <div id="features" class="scroll-id-target"></div>

            <div class="container" style="z-index: 1;">

                <div class="section-header">
                    <h2 class="block-title white"><?php _e('Core Features')?></h2>
                    <span class="title-follow-text white">
                        <?php _e('Essential functionality covered by a single service.'); ?>
                    </span>
                </div>

                <div class="p-t-20"></div>

                <?php 

                        if(get_locale() == 'en_US') {
                            $link_param1 = '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1340/114/what-os-distributions-you-provide-for-cloud-servers">';
                        } else {
                            $link_param1 = '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1362/114/quais-distribuicoes-de-so-voces-fornecem-para-servidores-cloud">';
                        }

                ?>

                <div class="layout-row three-col-row breakpoint-w770-max">

                    <div class="block-col color-bg-white block-col-separate-w770-max center">
                        <img class="p-t-20" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/cloud/powerful_api.svg" alt="Powerful API" height="110">
                        <h3 class="title-paragraph m-t-30 m-b-20"><?php _e('Powerful API')?></h3>
                        <p class="details-description"><?php _e('Directly access web control panel functionality and benefit from advanced networking features.'); ?></p>
                        <a target="_blank" href="<?php echo $config['links']['api'] ?>" class="button ghost-primary m-t-30"><?php _e('Details');?></a>
                    </div>
                    <div class="block-col color-bg-white block-col-separate-w770-max center">
                        <img class="p-t-20" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/cloud/kvm.svg" alt="KVM virtualization" height="110">
                        <h3 class="title-paragraph m-t-30 m-b-20"><?php _e('KVM virtualization')?></h3>
                        <p class="details-description"><?php _e('The most desired hypervisor with Windows support, high stability, immaculate performance & solid security.'); ?></p>
                        <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1341/114/what-is-the-difference-between-openvz--kvm" class="button ghost-primary m-t-30"><?php _e('Compare');?></a>
                    </div>
                    <div class="block-col color-bg-white block-col-separate-w770-max center">
                        <img class="p-t-20" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/cloud/os.svg" alt="Windows and Linux" height="110">
                        <h3 class="title-paragraph m-t-30 m-b-20"><?php _e('Windows and Linux')?></h3>
                        <p class="details-description"><?php printf(__('Pick the OS that you love from %savailable templates%s and deploy your virtual machine in seconds.'), $link_param1, '</a>'); ?></p>
                        <a href="/cloud-server-linux/" class="button ghost-primary m-t-30"><?php _e('Linux');?></a> 
                        <a href="/windows-cloud-server/" class="button ghost-primary m-t-30 m-l-10"><?php _e('Windows');?></a>
                    </div>

                </div>

                <div class="layout-row block-of-two-8-4 breakpoint-w770-min">

                    <div class="block-col no-padding">
                        <div class="layout-row one-col-row m-b-20">

                            <div class="block-col color-bg-white extra-spacious" style="min-height: 230px">
                                <img class="push-right p-t-20 p-h-20" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/cloud/powerful_api.svg" alt="Powerful API" height="130">
                                <h3 class="block-title-small p-t-0"><?php _e('Powerful API')?></h3>
                                <p class="details-description"><?php _e('Directly access web control panel functionality and benefit from advanced networking features.'); ?></p>
                                <a target="_blank" href="<?php echo $config['links']['api'] ?>" class="button ghost-primary m-t-30"><?php _e('Details');?></a>
                            </div>

                        </div>
                        <div class="layout-row one-col-row">

                            <div class="block-col color-bg-white extra-spacious" style="min-height: 230px">
                                <img class="push-right p-t-20 p-h-20" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/cloud/kvm.svg" alt="KVM virtualization" height="130">
                                <h3 class="block-title-small p-t-0"><?php _e('KVM virtualization')?></h3>
                                <p class="details-description"><?php _e('The most desired hypervisor with Windows support, high stability, immaculate performance & solid security.'); ?></p>
                                <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1341/114/what-is-the-difference-between-openvz--kvm" class="button ghost-primary m-t-30"><?php _e('Compare');?></a>
                            </div>

                        </div>
                    </div>

                    <div class="block-col color-bg-white center">
                        <img class="p-t-40" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/cloud/os.svg" alt="Windows and Linux" height="130">
                        <h3 class="block-title-small m-t-30 m-b-20"><?php _e('Windows and Linux')?></h3>
                        <p class="details-description"><?php printf(__('Pick the OS that you love from %savailable templates%s and deploy your virtual machine in seconds.'), $link_param1, '</a>'); ?></p>
                        <a href="/cloud-server-linux/" class="button ghost-primary m-t-30"><?php _e('Linux');?></a> 
                        <a href="/windows-cloud-server/" class="button ghost-primary m-t-30 m-l-10"><?php _e('Windows');?></a>
                    </div>

                </div>

            </div>

        </section> <!-- end of .service-features -->

        <section class="service-delivery extra-pad-top p-b-30 color-bg-style5">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Clever Service Delivery')?></h2>
                </div>

                <div class="layout-row four-col-row">

                    <div class="block-col center">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-cs-quickdeploy.svg" alt="Quick Deployment" height="130">
                        <h3 class="m-v-10 block-title-smaller"><?php _e('Quick Deployment')?></h3>
                        <p class="details-description dark-grey1 t-mspaced t-styled"><?php _e('Create and deploy your virtual machine in seconds.'); ?></p>
                    </div>
                    <div class="block-col center">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-cs-preinstall-templ.svg" alt="Pre-Installed Templates" height="130">
                        <h3 class="m-v-10 block-title-smaller"><?php _e('Pre-Installed Templates')?></h3>
                        <?php 

                            if(get_locale() == 'en_US') {
                                $link_param1 = '<a href="/cpanel-cloud-server/">';
                                $link_param2 = '</a>';
                            } else {
                                $link_param1 = $link_param2 = '';   // link is removed in BR page
                            }

                        ?>
                        <p class="details-description dark-grey1 t-mspaced t-styled"><?php printf(__('Deploy your VM with pre-configured templates such as %scPanel%s to manage your website with ease.'),$link_param1, $link_param2); ?></p>
                    </div>
                    <div class="block-col center">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-cs-scalability1.svg" alt="Unlimited Scalability" height="130">
                        <h3 class="m-v-10 block-title-smaller"><?php _e('Unlimited Scalability')?></h3>
                        <p class="details-description dark-grey1 t-mspaced t-styled"><?php _e('No longer bound by the physical size of your server, quickly scale your virtual machine at your Client Area.'); ?></p>
                    </div>
                    <div class="block-col center">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-cs-bulkorders.svg" alt="Bulk Orders" height="130">
                        <h3 class="m-v-10 block-title-smaller"><?php _e('Bulk Orders')?></h3>
                        <p class="details-description dark-grey1 t-mspaced t-styled"><?php _e('Multiple virtual machines with the same configuration will be ready for you in just a few clicks at the checkout.'); ?></p>
                    </div>

                </div>

            </div>

        </section> <!-- end of .service-delivery -->

        <section class="service-features extra-pad">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Simplified Management'); ?></h2>
                    <span class="title-follow-text"><?php _e('Simplicity. Power. Control.'); ?></span>
                </div>

                <div class="features-list2">
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/mono/blue_dns_management.svg" alt="DNS Management">
                                    </td>
                                    <td class="title">
                                        <?php _e('DNS Management')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('Easily create your domain zones and edit records directly at your Client Area.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/mono/blue_auto_backups.svg" alt="Auto backups">
                                    </td>
                                    <td class="title">
                                        <?php _e('Auto Backups')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('Schedule automatic backups daily, weekly or monthly. You can also make manual backups when you need!')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/mono/blue_rdns_control.svg" alt="rDNS Control">
                                    </td>
                                    <td class="title">
                                        <?php _e('rDNS Control')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('Simplified rDNS self-management means less time spent getting support and more time working on your own projects.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/mono/blue_vnc_console.svg" alt="VNC Console">
                                    </td>
                                    <td class="title">
                                        <?php _e('VNC Console')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('Ease up management of your virtual machine via the VNC console.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/mono/blue_rescue_mode.svg" alt="Rescue Mode">
                                    </td>
                                    <td class="title">
                                        <?php _e('Rescue Mode')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('If you suspect that your primary filesystem is corrupted, use our rescue mode - safe environment for performing many system recovery and disk management tasks.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
                                        <img class="" width="40" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/mono/blue_custom_os.svg" alt="Custom OS">
                                    </td>
                                    <td class="title">
                                        <?php _e('Custom ISO')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('Custom ISO allows you to mount configured image on your instance and run through the boot & setup process as you would on a bare metal server.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
<?php /*                                        
                                        <span class="new-icon position-left"><?php _e('N'); ?></span> 
*/ ?>                                        
                                        <img class="" width="40" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/mono/blue_ssh_keys.svg" alt="SSH Keys">
                                    </td>
                                    <td class="title">
                                        <?php _e('SSH Keys')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('Attach an SSH key to your Cloud Server and you will no longer rely on root password. Benefit from automated login for applications that needs access to your instance.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feature-box">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="ico vertical-top" rowspan=2>
<?php /*                                        
                                        <span class="new-icon position-left"><?php _e('N'); ?></span> 
*/ ?>                                        
                                        <img class="" width="40" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/mono/blue_live_stats.svg" alt="Live Stats">
                                    </td>
                                    <td class="title">
                                        <?php _e('Live Stats')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content">
                                        <?php _e('Track your resource usage live! Enable alerts to avoid CPU, memory or network overuse.')?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>

            </div> <!-- end of .container -->

            <div class="row center p-t-20">
                <a href="<?php echo $config['links']['client_area']; ?>" class="button primary orange large"><?php _e('Overview');?></a>
            </div>

        </section> <!-- end of .service-features-more -->

        <section class="service-networking extra-pad color-bg-style5 smart-underline-container">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Advanced Networking')?></h2>
                    <span class="p-v-20 display-block">
                        <?php _e('Unlock extra functionality for your environment built on high-end hardware and high-speed network.'); ?>
                    </span>
                </div>

                <ul class="icons-connected-horizontal style1">
                    <li class="feat-icon w-120 smart-underline">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-cs-ipv6.svg" alt="IPv6 support" height="110">
                        <div class="h-50 p-v-10">
                            <a class="feat-icon-descr" data-tooltip="cs-network-ipv6"><?php _e('IPv6 support'); ?></a>
                        </div>
                        <div class="tooltip-body style1 w400" data-tooltip-content="cs-network-ipv6">
                            <div class="triangle"></div>
                            <p><?php _e('Enable IPv6 during Cloud Server creation or on existing virtual machine through your Client Area without having to reboot.'); ?></p>
                        </div>
                    </li>
                    <li class="feat-connector"></li>
                    <li class="feat-icon w-120 smart-underline">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-cs-rdns.svg" alt="Geolocated IPs" height="110">
                        <div class="h-50 p-v-10">
                            <a class="feat-icon-descr" data-tooltip="cs-network-rdns"><?php _e('Geolocated IPs'); ?></a>
                        </div>
                        <div class="tooltip-body style1 w400" data-tooltip-content="cs-network-rdns">
                            <div class="triangle"></div>
                            <p><?php _e('Benefit from geolocated IPs to reduce latency, boost your SEO capability and localize your online business.'); ?></p>
                        </div>
                    </li>
                    <li class="feat-connector"></li>
                    <li class="feat-icon w-120 smart-underline">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-cs-ipv4.svg" alt="Multiple IPv4 addresses" height="110">
                        <div class="h-50 p-v-10">
                            <a class="feat-icon-descr" data-tooltip="cs-network-ipv4"><?php _e('Multiple IPv4 addresses'); ?></a>
                        </div>
                        <div class="tooltip-body style1 w400" data-tooltip-content="cs-network-ipv4">
                            <div class="triangle"></div>
                            <p><?php printf(__('Get access to 32 IPv4 addresses for your project for only %s per IP. More IPs are available on request.'), $whmcs::$settings['currency_prefix'] . $cloud_hosting_ip); ?></p>
                        </div>
                    </li>
                    <li class="feat-connector"></li>
                    <li class="feat-icon w-120 smart-underline">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-cs-speed.svg" alt="10G network" height="110">
                        <div class="h-50 p-v-10">
                            <a class="feat-icon-descr" data-tooltip="cs-network-speed"><?php _e('10G network'); ?></a>
                        </div>
                        <div class="tooltip-body style1 w400" data-tooltip-content="cs-network-speed">
                            <div class="triangle"></div>
                            <p><?php _e('Built-in 10G fiber optic ports for network troubleshooting and performance testing.'); ?></p>
                        </div>
                    </li>
                    <li class="feat-connector"></li>
                    <li class="feat-icon w-120 smart-underline">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-cs-permanentip.svg" alt="Permanent IP assignment" height="110">
                        <div class="h-50 p-v-10">
                            <a class="feat-icon-descr" data-tooltip="cs-network-permip"><?php _e('Permanent IP assignment'); ?></a>
                        </div>
                        <div class="tooltip-body style1 w400" data-tooltip-content="cs-network-permip">
                            <div class="triangle"></div>
                            <p><?php _e('Get a free dedicated unique IP only for you! Permanent IP provides direct access to your website all the time.'); ?></p>
                        </div>
                    </li>
                </ul>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-networking -->

        <section class="service-locations extra-pad-top color-bg-style2 no-zoom" data-section-title="<?php _e('Locations');?>">

            <div id="locations" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Cloud Servers'); echo ' '; _e('Locations');?></h2>
                        <p><?php _e('Multiple premium data centers ensure high connectivity and better reach.'); ?></p>
                </div>

                <?php include_block("htmlblocks/locations-services.php", array('type' => 'cloud', 'speedtest' => true )); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-locations -->

        <section class="service-faq extra-pad-top bordered" data-section-title="<?php _e('FAQ');?>">

            <div id="faq" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Ask questions, get answers!')?></h2>
                </div>

                <?php include_block("htmlblocks/faq/cs-hosting.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-faq -->


    </section> <!-- end of .main -->

</article>

<script>

 /*jQuery Segment.js plugin*/
    (function( $ ){
        $.fn.extend({
            Segment: function ( ) {
                $(this).each(function (){
                    var self = $(this);
                    var onchange = self.attr('onchange');
                    var wrapper = $("<div>",{class: "ui-segment"});
                    $(this).find("option").each(function (){
                        var option = $("<span>",{class: 'option',onclick:onchange,text: $(this).text(),value: $(this).val()});
                        if ($(this).is(":selected")){
                            option.addClass("active");
                        }
                        wrapper.append(option);
                    });
                    wrapper.find("span.option").click(function (){
                        wrapper.find("span.option").removeClass("active");
                        $(this).addClass("active");
                        self.val($(this).attr('value'));
                    });
                    $(this).after(wrapper);
                    $(this).hide();

                    /*Icon enhancements*/
                    $(this).find("option").each(function (){
                        var icosrc = $(this).attr('data-icosrc');
                        var optionval = $(this).val();
                        var $wrapperoption;
                        if (typeof(icosrc) !== 'undefined') {
                            $wrapperoption = wrapper.find("span.option[value='"+ optionval + "']")[0];
                            var icon = document.createElement("img");
                            $(icon).attr("src", icosrc);
                            //$wrapperoption.prepend(icon);     // no prepend() support for EDGE
                            $wrapperoption.insertBefore(icon, $wrapperoption.firstChild);
                        }

                    });

                });
            }
        });
    })(jQuery);

    // before initializing Segment switch check if URL provides hash for plans selection
    if(window.location.hash) {
         var hash_val = window.location.hash.substring(1),
             $selector_options = $('select.segment-select');

         $selector_options.children().each(function(){
             var a = $(this).val();
             if ($(this).val() === hash_val) {
                 $(this).attr('selected','selected');
             }
         })
     }

    // initialize Segment switch
    $(".segment-select").Segment();

</script>




<script>

    $(document).ready(function(){

        var currentBlockActive = $('.ui-segment .option.active').attr('value');
        $('.block-switchable').filter('#block-' + currentBlockActive).addClass('active');

        $('.ui-segment .option').click(function(e){
            var newVal = $(this).attr('value');
            $('.block-switchable').removeClass('active');
            $('.block-switchable').filter('#block-' + newVal).addClass('active');
        })
    });

</script>

<script>
    $(document).ready(function(){
        particlesJS('particles-js', particlesConfig());
    });
</script>

<?php

include_block("htmlblocks/hosting/speedtest_enabling_js.php");

universal_redirect_footer([
    'en' => $site_en_url.'/cloud-servers/',
    'br' => $site_br_url.'/servidores-cloud/'
]);

get_footer();

?>
