<?php

include("header-blocks/h1p_header_block.php");
include("sliders/h1p_slider.php");
include("maps/h1p_locations_map.php");
include("pricing-tables/pricing-tables.php");
include("features/h1p_feature-block.php");
include("features/h1p_interactive-feature.php");
include("h1p_process.php");
include("header-blocks/h1p_heading.php");