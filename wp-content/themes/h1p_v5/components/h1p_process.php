<?php
add_action( 'vc_after_init', 'h1p_steps_integrateWithVC' );
function h1p_steps_integrateWithVC() {


	$settings = array(
		"name" => __( "Process block", "h1p_composer" ),
		"base" => "h1p_steps",
		"category" => 'Host1Plus',
		"icon" => get_bloginfo( 'template_url' ) . "/components/h1p.png",
		'params' => array(
			array(
				"type" => "attach_image",
				"heading" => __( "Block icon", "h1p_composer" ),
				"param_name" => "header-background",
			),
			array(
				"type" => "dropdown",
				"heading" => __("Steps count", "h1p_composer"),
				"param_name" => "h1p_steps_count",
				"value" => array("2" => 2, "3" => 3, "4" => 4,"5" => 5)
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Hover title', 'js_composer' ),
				'param_name' => 'hover_title',
				'value' => 'Hover Box Element',
				'description' => __( 'Step 1 Element', 'js_composer' ),
				'group' => __( 'Step 1 Block', 'js_composer' ),
				'edit_field_class' => 'vc_col-sm-9',
			),
			array(
				'type' => 'textarea_html',
				'holder' => 'div',
				'class' => 'custom_class_for_element', //will be outputed in the backend editor
				'heading' => __( 'Hover/click content', 'h1p_composer' ),
				'param_name' => 'content', //param_name for textarea_html must be named "content"
				'value' => __( '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo..</p>', 'h1p_composer' ),
			)
		)
	);

	vc_map( $settings );

}

class WPBakeryShortCode_h1p_steps extends WPBakeryShortCode {
	public function __construct( $settings ) {
		parent::__construct( $settings ); // !Important to call parent constructor to active all logic for shortcode.
	}
}

