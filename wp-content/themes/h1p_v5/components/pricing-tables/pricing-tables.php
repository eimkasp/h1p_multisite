<?php
add_action( 'vc_after_init', 'h1p_pricing_table_integrateWithVC' );
function h1p_pricing_table_integrateWithVC() {
    $settings = array(
        "name" => __("Pricing table", "h1p_composer"),
        "base" => "h1p_pricing_table",
        "category" => 'Host1Plus',
        "icon" => get_bloginfo('template_url') . "/components/h1p.png",
        'params' => array(
            array(
                "type" => "textfield",
                "heading" => __("Sekcijos antraštė", "h1p_composer"),
                "param_name" => "h1p_header_title",
                "holder" => "h1p-heading-title"
            ),
        )
    );

    vc_map($settings);

}
class WPBakeryShortCode_h1p_pricing_table extends WPBakeryShortCode {
    public function __construct($settings)
    {
        parent::__construct($settings); // !Important to call parent constructor to active all logic for shortcode.
    }
}
