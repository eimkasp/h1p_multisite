<?php


/* Get link to other language */
$args                = array();
$currentSiteLanguage = get_bloginfo( 'language' ); // Get current language of website
$allLanguages        = mlp_get_interlinked_permalinks( $args ); ?>
<li class="dropdown language">
	<a class="simple p-0" href="#" data-prevent>
		<span class="lang-ico"><i class="fa fa-angle-down"></i></span><span
			class="txt"><?= $currentSiteLanguage; ?>, CURRENCY<i
				class="fa fa-angle-down"></i></span>
	</a>
	<ul class="submenu">
		<?php
		foreach ( $allLanguages as $lang ) : ?>
			<li>
				<a class="js-lang-switch en" href="http://www.host1plus.com">
                        <span class="lang-title"><?= $lang["lang"]; ?> , CURRENCY
                        </span>
				</a>
			</li>
		<?php endforeach; ?>
	</ul>

	<?php $langArgs = [];
	$my             = Mlp_Helpers::show_linked_elements( $langArgs );
	if ( ! empty( $my ) ) {
		preg_match( '/href=(["\'])([^\1]*)\1/i', $my, $m );
	}
	?>
</li>