<?php
add_action( 'vc_after_init', 'h1p_header_block_integrateWithVC' );
function h1p_header_block_integrateWithVC() {
    $settings = array(
        "name" => __("Header block", "h1p_composer"),
        "base" => "h1p_header_block",
        "category" => 'Host1Plus',
        "icon" => get_bloginfo('template_url') . "/components/h1p.png",
        'params' => array(
            array(
                "type" => "textfield",
                "heading" => __("Sekcijos antraštė", "h1p_composer"),
                "param_name" => "ktu_header_title",
                "holder" => "ktu-heading-title"
            ),
        )
    );

    vc_map($settings);

}
class WPBakeryShortCode_h1p_header_block extends WPBakeryShortCode {
    public function __construct($settings)
    {
        parent::__construct($settings); // !Important to call parent constructor to active all logic for shortcode.
    }
}
