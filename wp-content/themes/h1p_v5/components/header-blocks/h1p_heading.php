<?php
add_action( 'vc_after_init', 'h1p_heading_block_integrateWithVC' );
function h1p_heading_block_integrateWithVC() {
	$settings = array(
		"name" => __( "Heading block", "h1p_composer" ),
		"base" => "h1p_heading_block",
		"category" => 'Host1Plus',
		"icon" => get_bloginfo( 'template_url' ) . "/components/h1p.png",
		'params' => array(
			array(
				"type" => "textfield",
				"heading" => __( "Heading text", "h1p_composer" ),
				"param_name" => "h1p_heading",
				"holder" => "h1p_heading"
			),
			array(
				"type" => "dropdown",
				"heading" => __( "Heading style", "h1p_composer" ),
				"param_name" => "h1p_heading_style",
				"value" => array( "h1" => 1, "h2" => 2, "h3" => 3, "h4" => 4, 'h5' => 5 )
			),
			array(
				"type" => "dropdown",
				"heading" => __( "Heading alignment", "h1p_composer" ),
				"param_name" => "h1p_heading_align",
				"value" => array( "center" => 1, "left" => 2, "right" => 3)
			),
			array(
				"type" => "checkbox",
				"heading" => __( "Strikethrough?", "h1p_composer" ),
				"param_name" => "h1p_heading_style",
				"value" => "1"
			),
		)
	);

	vc_map( $settings );

}

class WPBakeryShortCode_h1p_heading_block extends WPBakeryShortCode {
	public function __construct( $settings ) {
		parent::__construct( $settings ); // !Important to call parent constructor to active all logic for shortcode.
	}
}
