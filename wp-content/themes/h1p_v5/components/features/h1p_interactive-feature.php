<?php
add_action( 'vc_after_init', 'h1p_interactive_feature_block_integrateWithVC' );
function h1p_interactive_feature_block_integrateWithVC() {


	$settings = array(
		"name" => __("Interactive Feature block", "h1p_composer"),
		"base" => "h1p_interactive_feature_block",
		"category" => 'Host1Plus',
		"icon" => get_bloginfo('template_url') . "/components/h1p.png",
		'params' => array(
			array(
				"type" => "attach_image",
				"heading" => __("Block icon", "h1p_composer"),
				"param_name" => "header-background",
			),
			array(
				"type" => "textfield",
				"heading" => __("Block heading", "h1p_composer"),
				"param_name" => "h1p_interactive_feature_title",
				"holder" => "h1p_interactive_feature_title"
			),
			array(
				'type' => 'textarea_html',
				'holder' => 'div',
				'class' => 'custom_class_for_element', //will be outputed in the backend editor
				'heading' => __('Hover/click content', 'ktu_composer'),
				'param_name' => 'content', //param_name for textarea_html must be named "content"
				'value' => __('<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo..</p>', 'ktu_composer'),
			),
			array(
				"type" => "dropdown",
				"heading" => __("Sekcijos tipas", "h1p_composer"),
				"param_name" => "h1p_box_style",
				"value" => array("Centered icon" => 1, "Icon on the left" => 2)
			)
		)
	);

	vc_map($settings);

}
class WPBakeryShortCode_h1p_interactive_feature_block extends WPBakeryShortCode {
	public function __construct($settings)
	{
		parent::__construct($settings); // !Important to call parent constructor to active all logic for shortcode.
	}
}
