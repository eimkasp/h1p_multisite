<?php
add_action( 'vc_after_init', 'h1p_feature_block_integrateWithVC' );
function h1p_feature_block_integrateWithVC() {

	$args = array(
		'post_type' => 'features',
		'order' => "DESC",
		'orderby' => 'date',
		'posts_per_page' => -1
	);

	// the query
	$newsCount = 0;
	$the_query = new WP_Query($args);

	$featuresObject = array();

	while ($the_query->have_posts()) : $the_query->the_post();
		$singleFeature = array('label' => get_the_title(), 'value' => get_the_ID());
		array_push($featuresObject, $singleFeature);
	endwhile;

	$settings = array(
		"name" => __("Feature block", "h1p_composer"),
		"base" => "h1p_feature_block",
		"category" => 'Host1Plus',
		"icon" => get_bloginfo('template_url') . "/components/h1p.png",
		'params' => array(
			array(
				"type" => "image",
				"heading" => __("Overide icon", "h1p_composer"),
				"param_name" => "h1p_icon_overide",
				"holder" => "ktu-heading-title"
			),
			array(
				"type" => "dropdown",
				"heading" => __("Sekcijos tipas", "ktu_composer"),
				"param_name" => "ktu_box_style",
				"value" => array("Centered icon" => 1, "Icon on the left" => 2)
			),
			array(
				'type' => 'autocomplete',
				'heading' => __('Feature', 'js_composer'),
				'param_name' => 'feature_object',
				'settings' => array(
					'multiple' => false,
					'sortable' => true,
					'min_length' => 1,
					'max_length' => 1,
					'no_hide' => false, // In UI after select doesn't hide an select list
					'groups' => true, // In UI show results grouped by groups
					'unique_values' => true, // In UI show results except selected. NB! You should manually check values in backend
					'display_inline' => true, // In UI show results inline view
					'values' => $featuresObject
				),
			)
		)
	);

	vc_map($settings);

}
class WPBakeryShortCode_h1p_feature_block extends WPBakeryShortCode {
	public function __construct($settings)
	{
		parent::__construct($settings); // !Important to call parent constructor to active all logic for shortcode.
	}
}
