<?php
add_action( 'vc_after_init', 'h1p_locations_map_integrateWithVC' );
function h1p_locations_map_integrateWithVC() {
    $settings = array(
        "name" => __("Locations map", "h1p_composer"),
        "base" => "h1p_locations_map",
        "category" => 'Host1Plus',
        "icon" => get_bloginfo('template_url') . "/components/h1p.png",
        'params' => array(
            array(
                "type" => "textfield",
                "heading" => __("Sekcijos antraštė", "h1p_composer"),
                "param_name" => "h1p_header_title",
                "holder" => "h1p-heading-title"
            ),
			array(
				"type" => "checkbox",
				"heading" => __( "Panels", "h1p_composer" ),
				"param_name" => "h1p_locations_panels",
				"value" => array( "cpanel" => 1, "dadmin" => 2)
			),
			array(
				"type" => "checkbox",
				"heading" => __( "Type", "h1p_composer" ),
				"param_name" => "h1p_locations_type",
				"value" => array( "VPS" => 1, "Cloud" => 2, "web" => 3, "Reseller" => 4)
			),
        )
    );

    vc_map($settings);

}
class WPBakeryShortCode_h1p_locations_map extends WPBakeryShortCode {
    public function __construct($settings)
    {
        parent::__construct($settings); // !Important to call parent constructor to active all logic for shortcode.
    }
}
