<?php
// gets included in h1p_v5/config.php

return [
    array(

        'website' => $site_current_url,

        'whois' => 'https://whois.host1plus.com',

        // hosting services

        'web-hosting' => $site_current_url . '/web-hosting/',
        'vps-hosting' => $site_current_url . '/vps-hosting/',
        'cloud-servers' => $site_current_url . '/cloud-servers/',
        //'cloud-hosting' => $site_current_url . '/cloud-hosting/',
        'dedicated-servers' => $site_current_url . '/dedicated-servers/',
        'reseller-hosting' => $site_current_url . '/reseller-hosting/',
        'seo-hosting' => $site_current_url . '/seo-hosting/',
        'forex-hosting' => $site_current_url . '/forex-hosting/',


        // company

        'about_us' => $site_current_url . '/about-us/',
        'clients_and_partners' => $site_current_url . '/clients-partners/',
        'we_hire' => $site_current_url . '/we-hire/',
        'contacts' => $site_current_url . '/contacts/',
        'affiliate' =>  $site_current_url . '/affiliate-program/',
        'services_map' => $site_current_url . '/service-map/',
        'branding' => $site_current_url . '/branding/',


        // Products

        'ssl-certificates' => $site_current_url . '/ssl-certificates/',
        'tools' => $site_current_url . '/tools/',


        // Domains search

        'domain-search' => $site_current_url . '/domain-search/',
        'domain-search-bulk' => $site_current_url . '/domain-search/?search=bulk',
        'domain-search-transfer' => $site_current_url . '/domain-search/?search=transfer',


        // Other sites

        'blog' => $site_url . '/blog/',
        'press' => $site_url . '/press/',
        'tutorials' => $site_url . '/tutorials/',
        'support' => 'http://support.host1plus.com/',

        'knowledge_base' => $site_url . '/knowledge-base/',
        'github' => 'https://github.com/host1plus',

        'affiliate_login' => $site_current_url . '/affiliate-login/',
        'affiliate_register' => $site_current_url . '/affiliate-signup/',
        'affiliate_pw_reset' => $site_current_url . '/affiliate-pwreset/',


        // TOS

        'terms_of_service' => $site_url . '/terms-of-service/',
        'privacy_policy' => $site_url . '/privacy-policy/',
        'spam_policy' => $site_url . '/anti-spam-policy/',
        'unlimited_policy' => $site_url . '/unlimited-policy/',
        'refund_policy' => $site_url . '/refund-policy/',


        // popups

        'login' => $site_current_url . '/login',
        'reseller-sign-in' => $site_current_url . '/reseller-sign-in',
        'sign-in' => $site_current_url . '/sign-in',

        'livechat' => 'https://support.host1plus.com/visitor/index.php?/LiveChat/Chat/Request/_proactive=0',

        // other

        'search' => $site_current_url . '/search/',
        'careers' => $site_current_url . '/jobs/',
        'sponsorship' => $site_current_url . '/sponsorship/',
        'sitemap' => $site_url . '/sitemap.xml',
        'links' => $site_current_url . '/links/',
        'reviews' => $site_url . '/reviews/',
        'speedtest' => $site_current_url . '/speed-test/',
        'data_centers' => $site_current_url . '/data-centers/',
        'abuse' => $site_current_url . '/abuse/',
        'api' => $site_current_url . '/api/',
        'developers_api' => 'https://developers.host1plus.com/',
        'client_area' => $site_current_url . '/client-area/',
        'reseller_program' => $site_current_url . '/reseller-program/',
        'feature_requests' => 'https://features.host1plus.com/'

    )
];

?>
