<?php 
// gets included in h1p_v5/config.php

return [
    array(

        'tools' => array(

            'stopthehacker' => array(

                'basic' => array(
                    'id' => 135,
                    'title' => 'Basic'
                ),

                'professional' => array(
                    'id' => 137,
                    'title' => 'Professional'
                ),

                'business' => array(
                    'id' => 139,
                    'title' => 'Business'
                ),

                'businessplus' => array(
                    'id' => 141,
                    'title' => 'BusinessPlus'
                )

            ),

            'spamexperts' => array(
                'id' => 71
            ),

            'softaculous' => array(

                'dedicated' => array(
                    'id' => 78,
                    'title' => 'Softaculous Dedicated License'
                ),

                'vps' => array(
                    'id' => 309,
                    'title' => 'Softaculous VPS License'
                )
            )

        ),

        'certificates' => array(

            'comodo' => array(

                'comodo' => array(
                    'id' => 91
                ),

                'positive' => array(
                    'id' => 104
                )

            ),

            'geotrust' => array(

                'rapid' => array(
                    'id' => 23
                ),

                'quick' => array(
                    'id' => 12
                ),

                'truebusiness' => array(
                    'id' => 15
                )
            )

        ),


        'web_hosting' => array(

            'plans' => array(

                '1' => array(
                    'id' => 364,
                    'title' => 'Personal',
                    'params' => array(
                        'disk_space' => -1,
                        'bandwidth' => -1,
                        'domains' => 3,
                        'databases' => 5,
                        'free_ip' => 0,   // 0 -> check WHMCS addons pricing
                        'free_domain' => true,
                        'uptime' => '99%',
                        'email_accounts' => true,
                        'domain_pointers' => 5,
                        'sub_domains' => 5,
                        'cpu_cores' => '2x80%',
                        'inodes' => 150000,
                        'memory' => '96 MB',
                        'processes' => 20,
                        'php_version' => '4.4/5.2/5.3/5.4/5.5',
                        'softacolous' => true,
                        'installatron' => true,
                        'backup_management' => true,
                        'ftp_accounts' => -1,
                        'network_uplink' => true,
                        'technical_support' => true,
                        'free_ssl' => 0 //No
                    )
                ),

                '2' => array(
                    'id' => 365,
                    'title' => 'Business',
                    'params' => array(
                        'disk_space' => -1,
                        'bandwidth' => -1,
                        'domains' => 10,
                        'databases' => 20,
                        'free_ip' => 0,   // 0 -> check WHMCS addons pricing
                        'free_domain' => true,
                        'uptime' => '99%',
                        'email_accounts' => true,
                        'domain_pointers' => 30,
                        'sub_domains' => 20,
                        'cpu_cores' => '2x100%',
                        'inodes' => 250000,
                        'memory' => '256 MB',
                        'processes' => 30,
                        'php_version' => '4.4/5.2/5.3/5.4/5.5',
                        'softacolous' => true,
                        'installatron' => true,
                        'backup_management' => true,
                        'ftp_accounts' => -1,
                        'network_uplink' => true,
                        'technical_support' => true,
                        'free_ssl' => 0 // No
                    )
                ),

                '3' => array(
                    'id' => 366,
                    'title' => 'Business Pro',
                    'params' => array(
                        'disk_space' => -1,
                        'bandwidth' => -1,
                        'domains' => -1,
                        'databases' => -1,
                        'free_ip' => 1,   // 1 = FREE
                        'free_domain' => true,
                        'uptime'  => '99%',
                        'email_accounts' => true,
                        'domain_pointers' => 100,
                        'sub_domains' => -1,
                        'cpu_cores' => '4x100%',
                        'inodes' => 400000,
                        'memory' => '512 MB',
                        'processes' => 50,
                        'php_version' => '4.4/5.2/5.3/5.4/5.5',
                        'softacolous' => true,
                        'installatron' => true,
                        'backup_management' => true,
                        'ftp_accounts' => -1,
                        'network_uplink' => true,
                        'technical_support' => true,
                        'free_ssl' => 1 // with the annual
                    )
                )

            ),

            'best_value' => 3,
            'good_for_seo' => 4

        ),

        'vps_hosting' => array(

            'locations' => array(
                'los_angeles' => array(
                    'id' => '348',
                    'title' => 'Los Angeles',
                    'country_code' => 'us',
                    'key' => 'us_west'
                ),
                'chicago' => array(
                    'id' => '349',
                    'title' => 'Chicago',
                    'country_code' => 'us',
                    'key' => 'us_east'
                ),
                'sao_paulo' => array(
                    'id' => '351',
                    'title' => 'São Paulo',
                    'country_code' => 'br',
                    'key' => 'brazil'
                ),
                'frankfurt' => array(
                    'id' => '347',
                    'title' => 'Frankfurt',
                    'country_code' => 'de',
                    'key' => 'germany'
                ),
                'johannesburg' => array(
                    'id' => '350',
                    'title' => 'Johannesburg',
                    'country_code' => 'za',
                    'key' => 'africa'
                )
            )

        ),

        // Cloud Servers: all resource configuration comes from WHMCS plugin

        'dedicated_servers' => array(

            'plans' => array(

                '1' => array(

                    'id' => 313, // Celeron
                    'title' => 'Celeron',
                    'params' => array(
                        'CPU' => 'Celeron Quad Core J1900',
                        'RAM' => '8 GB',
                        'STORAGE' => array(
                            'HDD' => '0.5 - 8 TB HDD',
                            'SSD' => '120 - 1920 GB SSD'
                        ),
                        'BANDWIDTH' => '1 Gbps'
                    )

                ),

                /*'2' => array(

                    'id' => 314, // Core i3
                    'title' => 'Core i3',
                    'params' => array(
                        'CPU' => 'Intel Dual Core i3',
                        'RAM' => '8 - 32 GB',
                        'STORAGE' => array(
                            'HDD' => '0.5 - 20 TB HDD',
                            'SSD' => '120 - 5760 GB SSD',
                            'RAID' => '2 - 8 CH RAID Card'
                        ),
                        'BANDWIDTH' => '1 Gbps'
                    )

                ),*/
                /*
                '3' => array(

                    'id' => 318, // Atom
                    'title' => 'Atom',
                    'params' => array(
                        'CPU' => 'Atom Octa Core C2750',
                        'RAM' => '8 - 32 GB',
                        'STORAGE' => array(
                            'HDD' => '1 - 24 TB HDD',
                            'SSD' => '120 - 5760 GB SSD',
                            'RAID' => '2 - 8 CH RAID Card'
                        ),
                        'BANDWIDTH' => '1 Gbps'
                    )

                ),*/

                '4' => array(

                    'id' => 315, // Core i5
                    'title' => 'Core i5',
                    'params' => array(
                        'CPU' => 'Intel Quad Core i5',
                        'RAM' => '8 - 32 GB',
                        'STORAGE' => array(
                            'HDD' => '1 - 24 TB HDD',
                            'SSD' => '120 - 5760 GB SSD',
                            'RAID' => '2 - 8 CH RAID Card'
                        ),
                        'BANDWIDTH' => '1 Gbps'
                    )

                ),

                '5' => array(

                    'id' => 316, // Core i7
                    'title' => 'Core i7',
                    'params' => array(
                        'CPU' => 'Intel Quad Core i7',
                        'RAM' => '16 - 32 GB',
                        'STORAGE' => array(
                            'HDD' => '750 GB - 24 TB HDD',
                            'SSD' => '120 - 5760 GB SSD',
                            'RAID' => '2 - 8 CH RAID Card'
                        ),
                        'BANDWIDTH' => '1 Gbps'
                    ),
                    'most-popular' => true

                ),

                '6' => array(

                    'id' => 317, // Xeon
                    'title' => 'Xeon',
                    'params' => array(
                        'CPU' => 'Intel Quad Core Xeon E3',
                        'RAM' => '8 - 32 GB',
                        'STORAGE' => array(
                            'HDD' => '1 - 24 TB HDD',
                            'SSD' => '120 - 5760 GB SSD',
                            'RAID' => '2 - 8 CH RAID Card'
                        ),
                        'BANDWIDTH' => '1 Gbps'
                    )

                )


            )

        ),

        'reseller_hosting' => array(

            'plans' => array(

                '1' => array(
                    'id' => 332,
                    'title' => 'Starter',
                    'params' =>  array(
                        'disk_space' => '50 GB',
                        'bandwidth' => '500 GB',
                        'hosted_domains' => true,
                        'sub_domains' => true,
                        'database' =>true,
                        'free_ip' => 1,   // 1 = FREE
                        'whmcs' => false
                    )

                ),

                '2' => array(
                    'id' => 333,
                    'title' => 'Personal',
                    'params' =>  array(
                        'disk_space' => '75 GB',
                        'bandwidth' => '750 GB',
                        'hosted_domains' => true,
                        'sub_domains' => true,
                        'database' => true,
                        'free_ip' => 1,   // 1 = FREE
                        'whmcs' => false
                    )

                ),

                '3' => array(
                    'id' => 334,
                    'title' => 'Business',
                    'params' =>  array(
                        'disk_space' => '125 GB',
                        'bandwidth' => '1250 GB',
                        'hosted_domains' => true,
                        'sub_domains' => true,
                        'database' => true,
                        'free_ip' => 1,   // 1 = FREE
                        'whmcs' => false
                    ),
                    'tags' => array('best-value')

                ),

                '4' => array(
                    'id' => 335,
                    'title' => 'Business Pro',
                    'params' =>  array(
                        'disk_space' => '150 GB',
                        'bandwidth' => '1500 GB',
                        'hosted_domains' => true,
                        'sub_domains' => true,
                        'database' => true,
                        'free_ip' => 1,   // 1 = FREE
                        'whmcs' => false
                    )

                )

            )

        ),

        'seo_vps_hosting' => array(

            'plans' => array(

                '1' => array(

                    'id' => 247,
                    'title' => 'VPS SEO Light',
                    'params' => array(
                        'locations' => array(
                            array('code' => 'us', 'title' => 'US')
                        ),
                        'ip_classes' => 4,
                        'unique_ips' => '50',
                        'bandwidth' => '500 GB',
                        'ram' => '2 GB',
                        'disk_space' => '40 GB',
                        'cpu' => '1-Core, Xeon',
                        'domains' => -1
                    )

                ),

                '2' => array(

                    'id' => 248,
                    'title' => 'VPS SEO Advanced',
                    'params' => array(
                        'locations' => array(
                            array('code' => 'us', 'title' => 'US'),
                            array('code' => 'uk', 'title' => 'UK')
                        ),
                        'ip_classes' => 6,
                        'unique_ips' => '100',
                        'bandwidth' => '750 GB',
                        'ram' => '3 GB',
                        'disk_space' => '50 GB',
                        'cpu' => '2-Core, Xeon',
                        'domains' => -1
                    )

                ),

                '3' => array(

                    'id' => 249,
                    'title' => 'VPS SEO PRO',
                    'params' => array(
                        'locations' => array(
                            array('code' => 'us', 'title' => 'US'),
                            array('code' => 'uk', 'title' => 'UK')
                        ),
                        'ip_classes' => 12,
                        'unique_ips' => '200',
                        'bandwidth' => '1000 GB',
                        'ram' => '4 GB',
                        'disk_space' => '60 GB',
                        'cpu' => '4-Core, Xeon',
                        'domains' => -1
                    )

                )

            ),

            'best_value' => 1

        ),

        'seo_dedicated_hosting' => array(

            'plans' => array(

                /*'1' => array(

                    'id' => 250,
                    'title' => 'Dedicated SEO Light',
                    'params' => array(
                        'locations' => array(
                            array('code' => 'us', 'title' => 'US'),
                            array('code' => 'uk', 'title' => 'UK')
                        ),
                        'ip_classes' => 16,
                        'unique_ips' => '300',
                        'bandwidth' => '2 TB',
                        'ram' => '4 GB',
                        'disk_space' => '1TB',
                        'cpu' => 'i3 Intel',
                        'domains' => -1
                    )

                ),*/

                '1' => array(

                    'id' => 253,
                    'title' => 'Dedicated SEO Advanced',
                    'params' => array(
                        'locations' => array(
                            array('code' => 'us', 'title' => 'US'),
                            array('code' => 'uk', 'title' => 'UK'),
                            array('code' => 'ca', 'title' => 'CA'),
                            array('code' => 'de', 'title' => 'DE')
                        ),
                        'ip_classes' => 20,
                        'unique_ips' => '500',
                        'bandwidth' => '5 TB',
                        'ram' => '8 GB',
                        'disk_space' => '1 TB',
                        'cpu' => 'i5 Intel',
                        'domains' => -1
                    )

                ),

                '2' => array(

                    'id' => 252,
                    'title' => 'Dedicated SEO PRO',
                    'params' => array(
                        'locations' => array(
                            array('code' => 'us', 'title' => 'US'),
                            array('code' => 'uk', 'title' => 'UK'),
                            array('code' => 'ca', 'title' => 'CA'),
                            array('code' => 'de', 'title' => 'DE')
                        ),
                        'ip_classes' => 30,
                        'unique_ips' => '1000',
                        'bandwidth' => '10 TB',
                        'ram' => '16 GB',
                        'disk_space' => '2 TB',
                        'cpu' => 'i7 Intel',
                        'domains' => -1
                    )

                )

            )

        )

    )
];

?>