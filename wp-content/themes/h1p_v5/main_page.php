<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 *
 * Template Name: Main Page
 *
*/

wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);
wp_enqueue_script('jquery.jcarousel', 'https://cdnjs.cloudflare.com/ajax/libs/jcarousel/0.3.4/jquery.jcarousel.min.js', ['jquery'], false, true);
wp_enqueue_script('jquery.jcarousel-swipe', get_template_directory_uri() . '/js/vendor/jquery.jcarousel-swipe.min.js', ['jquery'], false, true);
wp_enqueue_script('flippingtext', get_template_directory_uri() . '/js/vendor/flippingtext.js', [], false, true);
wp_enqueue_script('particles', 'https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js', ['jquery'], '2.0.0', true);

// removing unnecessary script for this page to prevent Google blocking in China
//wp_dequeue_script('contact-form-7');



/*
    In case you need somewhere some old-school

$cloud_hosting_min_price = $whmcs->getCloudConfigurablePrice( 'linux' , 'germany', 'LIN1' )[0]['price_monthly'];
$vps_hosting_min_price = $whmcs->getMinPrice($confisg['products']['vps_hosting']['locations']['chicago']['id']);
$web_hosting_min_price = $whmcs->getPrice($config['products']['web_hosting']['plans'][1]['id'], 1)['price_monthly'];
$reseller_hosting_min_price = $whmcs->getPrice($config['products']['reseller_hosting']['plans'][1]['id'], 1)['price_monthly'];
*/

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";
global $config;

$mypage = PageData::getInstance($config, NULL);

get_header();


    // Code snippet for email subscribtion confirmation page.

    if ((isset($_GET['r']) && $_GET['r'] == 'subscription_confirmed')) {
?>

    <div id="subscription-confirmed" class="white-popup mfp-hide">
        <div class="subscription-msg-wrapper">
            <img class="popup-icon-thumbnail" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/subscriptions/subscription_confirm.svg" alt="Subscription Confirmed" width="250" height="250">
            <h4 class="m-t-140 regular"><?php _e('Your subscription is now complete.');?></h4>
            <p><?php _e('Thank you for subscribing to our contact list.');?></p>
        </div>
    </div>

    <script>

    (function($) {
        $(window).load(function () {
            console.log ('Event for subscription confirmation popup');
            // retrieved this line of code from http://dimsemenov.com/plugins/magnific-popup/documentation.html#api
            $.magnificPopup.open({
                items: {
                    src: '#subscription-confirmed',
                    type: 'inline'
                }
            });
        });
    })(jQuery);

    </script>

<?php

    }   // end of subscription_confirmed


    // Code snippet for email subscribtion confirmation page.

    if ((isset($_GET['r']) && $_GET['r'] == 'unsubscribe_confirmed')) {
?>

    <div id="unsubscribe-confirmed" class="white-popup mfp-hide">
        <div class="subscription-msg-wrapper">
            <img class="popup-icon-thumbnail" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/subscriptions/unsubscribe_confirm.svg" alt="Unsubscribe Confirmed" width="250" height="250">
            <h4 class="m-t-140 regular"><?php _e('You have been unsubscribed from our list.');?></h4>
            <p><?php _e('We\'re sorry to see you go.');?></p>
        </div>
    </div>

    <script>

    (function($) {
        $(window).load(function () {
            console.log ('Event for subscription confirmation popup');
            // retrieved this line of code from http://dimsemenov.com/plugins/magnific-popup/documentation.html#api
            $.magnificPopup.open({
                items: {
                    src: '#unsubscribe-confirmed',
                    type: 'inline'
                }
            });
        });
    })(jQuery);

    </script>

<?php

    }   // end of unsubscribe_confirmed

  
?>

    <article class="page homepage no-pad">

        <header id="particles-js" class="headline cloud-introduction">
            <div class="container adjust-vertical-center">

                <div class="hot-news-wrapper">
                    <div class="hot-news-overflow-container">
                        <div class="hot-news-group">
                            <div class="hot-news-handle news"><?php _e('News'); ?> <i class="fa fa-angle-right"></i></div>
                            <div class="hot-news-message"><a href="http://www.host1plus.com/blog/news/host1plus-launches-cloud-servers-in-sao-paulo-brazil" target="_blank" title="<?php _e('Host1Plus Launches Cloud Servers ßeta in São Paulo, Brazil!'); ?>"><?php _e('Host1Plus Launches Cloud Servers ßeta in São Paulo, Brazil!'); ?></a></div>
                        </div>
                    </div> <!-- end of .hot-news-overflow-container -->
                </div>

                <div class="wrapper">

                    <h1 class="page-title">
                        <span id="flipper" class='flip'>
<?php

    /*Only rules for 2 languages !!!*/

if (get_locale() == 'en_US')
{

?>
                            <span class="step step0 set">Fast Storage</span>
                            <span class="step step1">IPv4 &amp; IPv6</span>
                            <span class="step step2">DNS Control</span>
                            <span class="step step4">Live Stats</span>
                            <span class="step step5">App Templates</span>
                            <span class="step step6">Backups</span>
<?php

} elseif (get_locale() == 'pt_BR') {

?>
                            <span class="step step0 set">Armazenamento</span>
                            <span class="step step1">IPv4 &amp; IPv6</span>
                            <span class="step step2">Controle de DNS</span>
                            <span class="step step4">Estatísticas</span>
                            <span class="step step5">Modelos de apps</span>
                            <span class="step step6">Backups</span>
<?php

}

?>
                        </span>
                    </h1>

                    <div class="title-follow-text"><?php _e('Cloud Servers')?> <?php _e('from')?> <span class="highlight"><?php echo $mypage->getServiceMinPrice('cs_lin'); ?></span></div>

                    <div class="home-slider-feat-wrap center m-v-20">
                        <div class="feat-wrap-block">
                            <i class="fa fa-check"></i> <?php _e('Powerful API')?>
                        </div>
                        <div class="feat-wrap-block">
                            <i class="fa fa-check"></i> <?php _e('KVM virtualization')?>
                        </div>
                        <div class="feat-wrap-block">
                            <i class="fa fa-check"></i> <?php _e('Windows & Linux')?>
                        </div>
                        <div class="feat-wrap-block">
                            <i class="fa fa-check"></i> <?php _e('Custom ISO')?>
                        </div>
                    </div>


                    <div class="center p-t-40 p-b-30">
                        <a href="<?php echo $config['links']['cloud-servers']; ?>" class="button highlight large p-h-40"><?php _e('View plans');?></a>
                    </div>
                </div>
        </div>
    </header> <!-- end of .headline -->

    <section class="main page-content">


        <section class="why-us service-features extra-pad-top no-pad-bottom">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Why choose us?')?></h2>
                </div>

                <div class="container-flex features-list">

                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-moneybackguarantee.svg" alt="Money-back Guarantee" width="140" height="140">
                        <h3><?php _e('Money-back Guarantee')?></h3>
                        <p><?php _e('If you\'re not happy with our service we will generously compensate you by offering a 14-day money-back guarantee.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-extremescalability.svg" alt="Extreme Scalability" width="140" height="140">
                        <h3><?php _e('Extreme Scalability')?></h3>
                        <p><?php _e('Rely on a fully scalable environment! Enjoy flexibility and full control - upgrade to higher plans and boost necessary resources to benefit from cost-effective solutions for your growing demands.')?></p>
                    </div>
                    <div class="block-col w-300">
                        <img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/feat-fullsupport.svg" alt="Full Support" width="140" height="140">
                        <h3><?php _e('Full Support')?></h3>
                        <p><?php _e('Our experienced team is always ready to give you a hand! From general questions to complex technical issues – seek assistance 24/7 and leave your worries behind. We NEVER leave issues unresolved.')?></p>
                    </div>

                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .why-us -->

        <div class="container">
            <div class="block-separator-line"></div>
        </div>

        <section class="service-locations on-light-bg no-zoom">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Worldwide Server Locations')?></h2>
                </div>

                <?php include_block("htmlblocks/locations-services.php"); ?>

            </div> <!-- end of .container -->

        </section> <!-- end of .service-locations -->

        <section class="our-customers extra-pad color-bg-style1">

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Meet our clients!')?></h2>
                </div>

                <?php include_block("htmlblocks/reviews_home.php"); ?>

                <div class="center">
                    <a href="<?php echo $config['links']['reviews'] ?>" class="button ghost-primary large"><?php _e('All Reviews');?></a>
                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .our-customers -->

        <?php include_block("htmlblocks/hosting-solutions.php"); ?>




    </section> <!-- end of .main -->

</article>

<script>
    (function($){

        $(document).ready(function(){
            particlesJS('particles-js', particlesConfig());
        });

        $(function() {
            var f;
            f = new Flip(document.getElementById('flipper'));
            var period = 1500;

            var myTimer = function(){
                clearInterval(interval);

                var thisCurrentStep = f.currentStep;
                var thisElement = f.el.find(".step" + thisCurrentStep);
                if (thisElement.is('[data-pause-next]')) {
                    period = 5000;
                } else {
                    period = 1500;
                }
                interval = setInterval(myTimer, period);

                return f.next();
            }

            var interval = setInterval(myTimer, period);
        });

    })(jQuery)
</script>

<?php get_footer(); ?>
