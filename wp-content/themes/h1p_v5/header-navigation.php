<?php

global $whmcs;
global $config;
global $stepsConfig;

/*
Checking which page is active from main serives menu

$is_active_webhosting
$is_active_vpshosting
$is_active_cloudservers
$is_active_resellerhosting

$site_current_url - comes from config.php

*/
$is_active_vpshosting      = "";
$is_active_webhosting      = "";
$is_active_cloudservers    = "";
$is_active_resellerhosting = "";

if ( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] != 'off' ) {
	$protocol = "https";
} else {
	$protocol = "http";
}

$actual_link = $protocol . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

//$current_product_in_menu = trim ( parse_url($site_current_url, PHP_URL_PATH), "/");

switch ( $actual_link ) {
	case $config['links']['web-hosting']:
		$is_active_webhosting = ' active';
		break;

	case $config['links']['vps-hosting']:
		$is_active_vpshosting = ' active';
		break;

	case $config['links']['cloud-servers']:
		$is_active_cloudservers = ' active';
		break;

	case $config['links']['reseller-hosting']:
		$is_active_resellerhosting = ' active';
		break;
}

$additional_header_class = 'header-regular';
if ( function_exists( "get_main_header_type" ) ) {
	$additional_header_class = get_main_header_type();
}

?>
<header id="page-header" class="header <?php echo $additional_header_class ?>">
  <div class="row first">
    <div class="container">
      <a class="logo" href="<?php echo $config['links']['website']; ?>">
		  <?php if ( get_field( 'logo', 'options' ) ) :
			  $h1p_logo = get_field( 'logo', 'options' );
			  ?>
            <img class="default" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"
                 src="<?php echo $h1p_logo['sizes']['full']; ?>"/>
		  <?php else: ?>
            <img class="default" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"
                 src="<?php echo get_template_directory_uri(); ?>/img/logo.svg"/>
		  <?php endif; ?>

      </a>
      <div class="slogan"><?php _e( 'Driven by developers.' ); ?></div>
      <ul class="nav simple-menu company">

		  <?php
		  # This element is only visible in the public EN/BR website and no other websites.

		  $this_site_url = site_url();
		  $subdirs       = explode( '/', parse_url( $this_site_url, PHP_URL_PATH ) );
		  $subdir        = count( $subdirs ) > 1 ? $subdirs[1] : '/';
		  if ( $subdir != 'press' && $subdir != 'tutorials' && $subdir != 'blog' ) :

			  // Assuming there are the same permalinks in all websites, that are used with quick-redirect plugin to transfer to local language permalink.
			  if ( $_SERVER['REQUEST_URI'] ) {
				  $redirect_query_part = $_SERVER['REQUEST_URI'];
			  } else {
				  $redirect_query_part = '';
			  }

			  ?>
            <?php include_once("components/header-blocks/language-switcher.php"); ?>
		  <?php endif; ?>

		  <?php
		  $lang_redirection_val = '';
		  if ( get_locale() == 'en_US' ) {
			  $lang_redirection_val = '?language=english';
		  }
		  if ( get_locale() == 'pt_BR' ) {
			  $lang_redirection_val = '?language=portuguese-br';
		  }
		  ?>

        <li class="login">
          <a id="menu-header-log-in" class="simple" data-popup="<?php echo $config['links']['login'] ?>"
             data-popup-id="login" data-popup-type="ajax" href="#" data-callback="$('#login-username').focus()"
             data-prevent><?php _e( 'Log In' ) ?></a>
          <span class="separator"></span>
          <a id="menu-header-register" class="simple" rel="nofollow"
             href="<?php echo $config['whmcs_links']['register'] . $lang_redirection_val ?>"><?php _e( 'Register' ) ?></a>
        </li>
        <li class="login-toggle">
          <a data-popup="<?php echo $config['links']['login'] ?>" data-popup-type="ajax" data-popup-id="login" href="#"
             data-prevent><i class="fa fa-user"></i></a>
        </li>
      </ul>
    </div>
  </div>
  <div class="row second">
    <div class="container">
      <a class="nav-toggle" href="#" data-prevent data-toggle-active
         data-toggle-callback="jQuery(this).parent().find('.open[data-dropdown]').removeClass('open');">
        <span class="icon"><i class="fa fa-bars"></i></span>
      </a>
		<?php wp_nav_menu(
			array(
				'theme_location' => 'header-menu',
				'walker'         => new My_Walker_Nav_Menu(),
				'menu_class'     => "nav main-menu products"
			) ); ?>

      <!--<ul class="nav main-menu products">
        <li><a id="menu-vps-hosting" class="tbl-cell<?php /*echo $is_active_vpshosting */ ?>"
               href="<?php /*echo $config['links']['vps-hosting'] */ ?>">
            <span class="title"><?php /*_e('VPS Hosting') */ ?></span>
          </a></li>
        <li><a id="menu-cloud-servers" class="tbl-cell<?php /*echo $is_active_cloudservers */ ?>"
               href="<?php /*echo $config['links']['cloud-servers'] */ ?>">
            <span class="title"><?php /*_e('Cloud Servers') */ ?></span>
            <span class="new-item"><?php /*_e('New'); */ ?></span>
          </a></li>
        <li><a id="menu-web-hosting" class="tbl-cell<?php /*echo $is_active_webhosting */ ?>"
               href="<?php /*echo $config['links']['web-hosting'] */ ?>"><span
                    class="title"><?php /*_e('Web Hosting') */ ?></span></a></li>
        <li><a id="menu-reseller-hosting" class="tbl-cell<?php /*echo $is_active_resellerhosting */ ?>"
               href="<?php /*echo $config['links']['reseller-hosting'] */ ?>"><span
                    class="title"><?php /*_e('Reseller Hosting') */ ?></span></a></li>

      </ul>-->
      <ul class="nav main-menu support-links">
		  <?php if ( get_locale() == 'en_US' ): ?>
            <li class="menu-item blog">
              <a class="simple" href="<?php echo $config['links']['blog'] ?>"><?php _e( 'Blog' ) ?></a>
            </li>
		  <?php endif; ?>
        <li class="menu-item affiliate">
          <a class="simple" href="<?php echo $config['links']['affiliate'] ?>"><?php _e( 'Affiliate Program' ) ?></a>
        </li>
        <li data-dropdown data-dropdown-group="main-menu" data-keep-position>
          <a href="#" data-prevent data-dropdown-toggle><?php _e( 'Support' ) ?><i class="fa fa-angle-down"></i></a>
          <ul class="submenu">
            <li><a id="menu-features" class="tbl-cell" href="<?php echo $config['links']['feature_requests'] ?>"><span
                        class="title"><?php _e( 'Feature Requests' ) ?></span><span
                        class="new-item"><?php _e( 'New' ); ?></span></a></li>
            <li><a id="menu-support" class="tbl-cell" href="<?php echo $config['links']['support'] ?>"><span
                        class="title"><?php _e( 'Support Center' ) ?></span></a></li>
            <li><a class="tbl-cell" href="<?php echo $config['links']['knowledge_base'] ?>"><span
                        class="title"><?php _e( 'Knowledge Base' ) ?></span></a></li>
			  <?php if ( get_locale() == 'en_US' ): ?>
                <li><a id="menu-tutorials" class="tbl-cell" href="<?php echo $config['links']['tutorials'] ?>"><span
                            class="title"><?php _e( 'Tutorials' ) ?></span></a></li>
			  <?php endif; ?>
            <li><a id="menu-abuse" class="tbl-cell" href="<?php echo $config['links']['abuse'] ?>"><span
                        class="title"><?php _e( 'Report Abuse' ) ?></span></a></li>
          </ul>
        </li>
      </ul>
		<?php
		if (
			get_locale() == 'en_US'
			&& ! in_array( $post->ID, [
				68, // about us
				16447, //search
				16426, //Affiliate program
				18223, //Sponsorship
				18151, //Careers
				18681 //links (of all landings)
			] )
		): ?>
          <ul class="nav main-menu support">
            <li id="menu-header-live-chat" class="livechat online"><a class="simple" href="#"
                                                                      onclick="window.open('<?php echo $config['links']['livechat'] ?>', '_blank', 'width=500,height=500,location=0,menubar=0,status=0,titlebar=0')"><i
                        class="fa fa-comments"></i><i class="icon-online fa fa-check-circle"></i><span
                        class="txt"><?php _e( 'Sales Chat' ) ?></span></a></li>
          </ul>
		<?php endif; ?>
    </div>
  </div>
</header>
