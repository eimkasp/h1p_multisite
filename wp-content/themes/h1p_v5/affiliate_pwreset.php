<?php
/**
 * @package WordPress
 * @subpackage h1p_v5
 *
 * Template Name: Affiliate PwReset
 */

define( 'SKIP_HEADER_NAV', '1' );
define( 'SKIP_FOOTER_NAV', '1' );

get_header();

?>

<div class="white-content-block-wrapper w-450">
  <div class="white-content-block">

    <div class="white-content-logo">
      <i class="host1plus-logo"></i>
    </div>

    <div class="p-v-20" style="display:none;" data-pwreset-success>

    </div>

    <form id="aff_form">
      <h3 class="block-title-large-thin p-v-20 f-family-default">
        <?php _e('Forgot your password?')?>
      </h3>

      <div class="f-size-default adjust-left p-b-10">
        <?php _e('Enter your registered email address to reset your password.')?>
      </div>

      <div class="text-error f-size-default" style="display:none;" data-general-error>

      </div>

      <div class="layout-row one-col-row f-family-default">
        <div class="block-col p-l-0 p-r-5 p-t-0 center-block">
          <div class="fields-group clearfix">
            <div class="form-field">
              <div class="field">
                  <input id="email" type="text" name="email" value="" placeholder="<?php _e('Email Address')?>" data-required data-valide-email/>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="">
        <button id="aff_form_submit" class="button highlight fullwidth large" type="submit"><?php _e('Reset Password')?></button>
      </div>
    </form>

  </div>

  <div class="wrapper-footer">
    <table>
      <tr>
        <td class="adjust-left"><small><?php _e('Back to:')?> <a href="<?php echo $config['links']['affiliate']?>"><?php _e('Home')?></a></small></td>
        <td class="adjust-right">
          <small><?php _e("Don't have account yet?")?> <a class="simple" href="<?php echo $config['links']['affiliate_register']?>"><?php _e('Register')?></a></small>
        </td>
      </tr>
    </table>
  </div>
</div>

<script>
$('#aff_form').submit(function(e){
  e.preventDefault();
  var $submitButton = $(e.target).find('[type="submit"]');

  var $general_error_block = $('[data-general-error]');

  $general_error_block.text('').hide();

  var can_submit = true;

  //validate required inputs
  $('[data-required]').each(function(){
    if( $(this).val().length < 1 ){
      $(this).addClass('error');
      can_submit = false;
    }
    else{
      $(this).removeClass('error');
    }
  });

  //validate emails
  $('[data-valide-email]').each(function(){
    if( ! IsEmail( $(this).val() ) ){
      $(this).addClass('error');
      $general_error_block.html( '<?php _e('Invalid email address.') ?>' ).show();
      can_submit = false;
    }
    else{
      $general_error_block.text('').hide();
      $(this).removeClass('error');
    }
  });

  if( can_submit ){

      $submitButton.prop('disabled', true);
      $submitButton.prepend('<i class="fa fa-spinner fa-pulse"></i> ');

      //do ajax
      var request = $.ajax({
          url: "/wp-content/themes/<?= wp_get_theme()->template ?>/PAP/pap_pwreset.php",
          method: "POST",
          data: $(this).serialize(),
          async: true,
          dataType: "json",
          crossDomain: false // force to send HTTP_X_REQUESTED_WITH header
      });

      request.done(function( response ) {

        if( response.success ){
          $('#aff_form').hide();
          $('[data-pwreset-success]').html( response.message ).show();
        }
        else{
          $general_error_block.html( response.message ).show();
          $('html,body').animate({scrollTop: $general_error_block.offset().top},'slow');

          $submitButton.find('i').remove();
          $submitButton.prop('disabled', false);
        }

      });

      request.fail(function( error ) {

      });

  }

});

function IsEmail(email) {
    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
};
</script>

<?php

get_footer();

?>
