<?php
/**
 * @package WordPress
 * @subpackage h1p_v4
 */
/*
  Template Name: Tos
 */
define("TOS", true);
define("HEADER", false);
define("FOOTER", false);

wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);

get_header();
?>
<div class="page tos">

    <section class="page-content">

        <div class="container">

            <div class="scroll-id-target" id="tos-top"></div>

            <div class="page-title-block p-b-60">
                <h1 class="page-title">Host1Plus General Terms and Conditions</h1>
            </div>

            <?php include_block('htmlblocks/tos/sidebar.php');?>

            <div class="tos-block">

                <div class="tos-content">

                    <div class="scroll-id-target" id="tos-section-glossary"></div>

                    <h2 class="tos-top-level-title">Glossary</h2>

                    <div class="tos-desc">
                        <ul class="simple-list">
                            <li>User - A person or an entity who uses services in accordance with the Agreement signed with the service provider.</li>
                            <li>Active User - A person or an entity who has one or more active services.</li>
                            <li>Inactive User - A person or an entity without any services or the services are cancelled or terminated.</li>
                            <li>Visitor - A person or an entity that is browsing the Provider’s website at http://www.host1plus.com/.</li>
                            <li>Services – Either or both services and products provided by the Provider and available via Providers’ website.</li>
                            <li>Downtime - A period of time when a service is inactive or inaccessible.</li>
                            <li>Monthly Services – Services that are applied instantly or provided for a 1-month period.</li>
                            <li>Long-Term Services – Services that are provided for more than a 1-month period.</li>
                            <li>Primary Language – English.</li>
                            <li>Secondary Languages - Portuguese, Spanish, Chinese.</li>
                            <li>Primary Currency - USD.</li>
                            <li>Secondary Currencies - GBP, EUR, BRL, CLP, MXN, PLN, RUB.</li>
                            <li>Billing Cycle – A predefined interval of time between two recurring payments.</li>
                            <li>Billing Account - The account at http://www.host1plus.com holding a summary of all types of services provided to the User. Billing account may be is also entitled as Client Area.</li>
                            <li>Credit Balance – The amount of funds in the User’s account.</li>
                            <li>Confirmed affiliate income – The amount of money that the User earned through the Affiliate System.</li>
                            <li>Reseller – A person or an entity who is accepted to Host1Plus Reseller Program and gets access to the services and benefits of Host1Plus Reseller Program.</li>
                            <li>Affiliate Earned Funds – The amount of money that the User received.</li>
                            <li>Complaining Party – the User or an interested 3rd party that submits a report, concerning the violation of Acceptable Use policy.</li>
                            <li>Authorized payments - Automatic transactions that are pre-approved by the User.</li>
                        </ul>

                    </div> <!-- end of .tos-descr -->

                    <div class="scroll-id-target" id="tos-section-1"></div>

                    <h2 class="tos-top-level-title">I. Application of Terms</h2>

                    <div class="tos-desc">
                        <ul class="terms">
                            <li>The terms regulate the conditions and procedures for the provision of services provided by Host1Plus ("Provider", "we", "us", "our") to the user of the services (“User”, “he”, “his”).</li>
                            <li>Terms of Service regulate the rights and commitments of both the Provider and the User.</li>
                            <li>Terms of Service take effect from the moment of the use of the website or the date of electronic acceptance.</li>
                            <li>In Terms of Service, except for cases when context requires otherwise, words indicating singular include plural and vice versa. Similarly, words indicating natural person also indicate legal person.</li>
                            <li>The names of the articles of these rules are simply used for convenience and do not have any influence on interpretation by both parties.</li>
                        </ul>
                    </div>


                    <div class="scroll-id-target" id="tos-section-2"></div>

                    <h2 class="tos-top-level-title">II. General Terms and Conditions</h2>

                    <div class="tos-desc">

                        <div class="scroll-id-target" id="tos-section-2-1"></div>

                        <h3 class="tos-top-sublevel-title">1. Provider’s Rights and Responsibilities</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">1.1.</span> The Provider does not guarantee that its services, personnel, or anything affiliated with Host1Plus and/or Digital Energy Technologies Ltd. is completely without fault and will not cause unintentional damages.</li>
                            <li><span class="nmb">1.2.</span> The Provider does not take responsibility to cover any damages caused by possible downtime of any service. Nevertheless in knowledge of any issues which caused damage with its services, the Provider reserves the right to limit any amount paid in return to the User, for any of his services, to a maximum of 3 months of the monthly service fee.</li>
                            <li><span class="nmb">1.3.</span> The Provider does not take responsibility for any service maintenance or delivery delays due which were produced by third party services or other actions directly not controllable by the Provider. In cases where delivery is postponed due to external causes the customer has the right to request the next payment date to be moved accordingly.</li>
                            <li><span class="nmb">1.4.</span> The Provider reserves the right to demand upgrade of any service in case when the Provider deems for it to be necessary. This is done to ensure the stability of the service and the rights to equal performance for any neighboring service user.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-2-2"></div>

                        <h3 class="tos-top-sublevel-title">2. User’s Rights and Responsibilities</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">2.1.</span> In any case of interaction the User agrees with the Terms of Service.</li>
                            <li><span class="nmb">2.2.</span> If not in compliance with Terms of Service, the User agrees not to use any of the services provided by Host1Plus or Digital Energy Technologies Ltd.</li>
                            <li><span class="nmb">2.3.</span> No abusive act or omission may be justified by involvement of third parties. The User is always held liable for any actions carried out using resources bought or leased for temporary use.</li>
                        </ul>


                        <div class="scroll-id-target" id="tos-section-2-3"></div>

                        <h3 class="tos-top-sublevel-title">3. User’s Non-Compliance</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">3.1.</span> In case of User’s non-compliance with Terms of Service, the Provider will act under the sole discretion in order to restore compliance.</li>
                            <li><span class="nmb">3.2.</span> The Provider may deny new orders and halt tasks, including support tasks that are already in progress.</li>
                            <li><span class="nmb">3.3.</span> The Provider will continue communication with the User with emphasis on the non-compliance issue only.</li>
                            <li><span class="nmb">3.4.</span> If the Provider’s efforts to restore compliance with Terms of Service fail because of the User’s non-cooperation, or the violation is too severe, any actions, based on Terms of Service, carried out by the Provider will be considered necessary including suspension or termination of service. In case of such suspension or termination the User will have no right to demand a refund/be refunded. Moreover, the User will be held responsible for this unilateral termination of service agreement initiated by the Provider.</li>
                            <li><span class="nmb">3.5.</span> In case of abuse or any similar adverse circumstances the Provider will take necessary actions to suppress the incident as soon as possible. Considering the severity of the incident the Provider may not contact the User prior to suspension. Such inaccessibility is not considered downtime. The Provider will try to cooperate toward a problem resolution as long as the User puts relative effort into mutual cooperation. However, the User has no right to demand re-activation of the service if the Provider believes that it will cause risk of another incident.</li>
                            <li><span class="nmb">3.6.</span> If any illegal activities occur of which the Provider is aware of, the Provider will contact appropriate legal authorities.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-2-4"></div>

                        <h3 class="tos-top-sublevel-title">4. Uptime Guarantee</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">4.1.</span> The Provider guarantees a 99% uptime on all Host1Plus hardware and network connectivity. In any given month, if the uptime guarantee is breached:</li>
                            <ul class="simple-list">
                                <li>Service downtime for additional 1 hour equals to additional 1 day to service due date.</li>
                                <li>Service downtime for additional 4 hours equals to 1 week service due date (8 hours = 2 weeks etc).</li>
                                <li>Compensations are provided up to additional maximum of 1 month period to service due date.</li>
                                <li>The uptime guarantee does not apply to 3rd party products (domains, cpanel, ssl and etc).</li>
                                <li>Compensations are calculated on the User's service lifetime. Compensations can only be issued for services which are active for at least for 3 month or more in such periods: 3 month (total of 22 hours downtime), 6 month (total of 43 hours downtime), 9 month (total of 66 hours downtime), 12 month (total of 87 hours downtime).</li>
                                <li>Scheduled service maintenance does not apply as a downtime.</li>
                            </ul>
                            <li><span class="nmb">4.2.</span> Compensation will be applied only if all invoices are paid for the impacted service.</li>
                            <li><span class="nmb">4.3.</span> Compensation will be applied in up to 7 days after customer report and confirmation from Host1Plus team.</li>
                        </ul>

                    </div>

                    <div class="scroll-id-target" id="tos-section-3"></div>

                    <h2 class="tos-top-level-title">III. Billing Account</h2>

                    <div class="tos-desc">

                        <div class="scroll-id-target" id="tos-section-3-1"></div>

                        <h3 class="tos-top-sublevel-title">1. Account Eligibility</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">1.1.</span> Billing account - the account at the Provider’s website http://www.host1plus.com/, holding a summary of any type of services provided to the User.</li>
                            <li><span class="nmb">1.2.</span> By registering with the Provider, the User confirms that:
                                <ul class="simple-list">
                                    <li>He is the owner of the primary email address used to set up the billing account and is held the owner of the billing account. Therefore email address cannot be changed.</li>
                                    <li>He will not pass, sell, or transfer the ownership of the billing account to another person or entity in any form. Otherwise the Provider may limit access to both parties until the User is verified.</li>
                                    <li>He will own one billing account. Acts when multiple accounts are made to bypass any restrictions or commit illegal activities will result in permanent termination of all services.</li>
                                    <li>He will provide correct and verifiable information in the registration form and any further communication. It is the User’s responsibility to make sure the information is up to date.</li>
                                    <li>He is fully responsible for the use of his billing account and for any actions that take place through the User’s account.</li>
                                </ul>
                            </li>
                            <li><span class="nmb">1.3.</span> The Provider reserves the right to deny service to the User in case there is doubt of compliance with Terms of Service for as long as the non-compliance persists.</li>
                            <li><span class="nmb">1.4.</span> The Provider reserves the right to deny services to the User located in a country with a high fraud rate.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-3-2"></div>

                        <h3 class="tos-top-sublevel-title">2. Additional Information Request</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">2.1.</span> The Provider has the right to request additional information from the User in order to verify the provided information or any orders made by the User.</li>
                            <li><span class="nmb">2.2.</span> Additional information includes (but is not limited to) ID card or passport copies. If the User chooses to pay for his services by credit card, a credit card scan may be requested as well.</li>
                            <li><span class="nmb">2.3.</span> The User must provide original, high quality, easily readable, uncropped scans or photos of the specified documents.</li>
                            <li><span class="nmb">2.4.</span> Requests to provide additional information might be made for various reasons such as (but not limited to) credit card ownership validation or IP order justification.</li>
                            <li><span class="nmb">2.5.</span> In case of unreasonable delay or refusal to provide such information, the User account and services provided by the Provider may be suspended or terminated.</li>
                            <li><span class="nmb">2.6.</span> Refusing to comply with Terms of Service will result in unilateral termination of service agreement initiated by the Provider with no refund possibility. The Provider will act based on “User's non-compliance" terms.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-3-3"></div>

                        <h3 class="tos-top-sublevel-title">3. User's Content Policy</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">3.1.</span> The User of the account is responsible for any actions carried out within the account and all resources given by the Provider.</li>
                            <li><span class="nmb">3.2.</span> The User is responsible to take care of their account and its safety.</li>
                            <li><span class="nmb">3.3.</span> It is the User's responsibility to make backups.</li>
                            <li><span class="nmb">3.4.</span> The User is solely responsible for the preservation of his data. While the backups are being done by the Provider (if such feature is included within the User's service), we do not guarantee full data safety and have no responsibility to preserve the data at all times. It is the User's responsibility to make backups of the information that is critical to him.</li>
                            <li><span class="nmb">3.5.</span> The Provider may not be held liable in any case of data loss.</li>
                        </ul>

                    </div>

                    <div class="scroll-id-target" id="tos-section-4"></div>

                    <h2 class="tos-top-level-title">IV. Billing and Payment Policy</h2>

                    <div class="tos-desc">

                        <div class="scroll-id-target" id="tos-section-4-1"></div>

                        <h3 class="tos-top-sublevel-title">1. Billing Policy</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">1.1.</span> The invoice for the services is calculated according to the rates that are present on the website.</li>
                            <li><span class="nmb">1.2.</span> It is the User's responsibility to review billing information.</li>
                            <li><span class="nmb">1.3.</span> If the User uses monthly services, invoices are generated 15 days before the due date.</li>
                            <li><span class="nmb">1.4.</span> If the User uses long-term services, invoices are generated 25 days before the due date, if not stated otherwise.</li>
                            <li><span class="nmb">1.5.</span> Due date reminders are sent respectively each day after the payment date for 3 consecutive days.</li>
                            <li><span class="nmb">1.6.</span> The due date for the payment may not be changed by either party.</li>
                            <li><span class="nmb">1.7.</span> An invoice generated for the next billing period of an active service may not be cancelled on request.</li>
                            <li><span class="nmb">1.8.</span> If any outstanding invoices are unpaid within the specified period, related services will be suspended automatically.</li>
                            <li><span class="nmb">1.9.</span> In case of service cancellation, all related invoices will be cancelled automatically.</li>
                            <li><span class="nmb">1.10.</span> In case of order reconsideration, the former order is terminated and the related invoice is canceled.</li>
                            <li><span class="nmb">1.11.</span> The Provider reserves the right to refuse some service upgrades if there are outstanding invoices for the service in question. In such case, the User is asked to pay for the outstanding invoices prior to the upgrade.</li>
                            <li><span class="nmb">1.12.</span> The Provider reserves the right to change the rates of services anytime. The User always receives a notice about any upcoming changes in advance.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-4-2"></div>

                        <h3 class="tos-top-sublevel-title">2. Payment Policy</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">2.1.</span> The currency initially assigned to the billing account may not be changed.</li>
                            <li><span class="nmb">2.2.</span> Discounts provided for first-time accounts may not be used multiple times by the same person. Coupon/promotion abuse will not be tolerated and may result in the suspension or termination of the account.</li>
                            <li><span class="nmb">2.3.</span> The Provider is not responsible for the price changes that may occur due to currency exchange rates.</li>
                            <li><span class="nmb">2.4.</span> Prices do not include any taxes imposed by any tax authority of any kind if not stated otherwise. It is the User’s responsibility to cover additional taxes that may be applied.</li>
                            <li><span class="nmb">2.5.</span> Only the User himself may pay for his services. The Provider reserves the right to ask to prove the ownership of the credit card or any other medium of payment used.</li>
                            <li><span class="nmb">2.6.</span> The Provider is not responsible for automatic transactions which are issued by third party checkout services. It is the User’s responsibility to manage their third party checkout services account settings to disable and avoid unwanted automatic transactions.</li>
                            <li><span class="nmb">2.7.</span> By creating a billing agreement for authorized PayPal payments through the Client Area, the User gives permission for the Provider to process automatic transactions from his PayPal account. It is the User’s responsibility to create, manage and terminate authorized PayPal payments in the Client Area.</li>
                            <li><span class="nmb">2.8.</span> By creating authorized credit card payments through the Client Area, the User gives permission for the Provider to process automatic transactions from his credit card. It is the User’s responsibility to create, manage and terminate authorized credit card payments in the Client Area.</li>
                            <li><span class="nmb">2.9.</span> By using his PayPal account with Host1Plus, the User agrees to set PayPal as his default payment method for all future automatic transactions. If the User chooses to add a credit card to his account, it will be set as the default payment method for all automatic transactions. It is the User‘s responsibility to create, manage and terminate authorized payments in the Client Area.</li>
                            <li><span class="nmb">2.10.</span> The Provider does not tolerate any type of fraud regarding the payment for the services. If any illegal actions occur, the services are terminated and the User is reported to legal authorities without prior notice.</li>
                        </ul>

                    </div>

                    <div class="scroll-id-target" id="tos-section-5"></div>

                    <h2 class="tos-top-level-title">V. Cancellation and Refunds</h2>

                    <div class="tos-desc">

                        <div class="scroll-id-target" id="tos-section-5-1"></div>

                        <h3 class="tos-top-sublevel-title">1. Service Cancellation</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">1.1.</span> Services may be canceled by either party – the Provider or the User. However, Provider cannot cancel the service on the User's behalf.</li>
                            <li><span class="nmb">1.2.</span> The Provider has the right cancel the services if the User violates Terms of Service. The Provider reserves the right to cancel the service without prior notice.</li>
                            <li><span class="nmb">1.3.</span> The User has the right to cancel his service anytime by submitting the cancellation request through his billing account.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-5-2"></div>

                        <h3 class="tos-top-sublevel-title">2. Refund Policy</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">2.1.</span> The Provider ensures 14 day money–back guarantee. The guarantee applies from the moment the service is activated. The guarantee applies for each service separately. Please note that the 14 day money–back guarantee is not valid for 3rd party products and services. If the User has purchased a service that includes a free domain name and the user canceled the purchased product, the standard price for the domain name will be deducted from the refund amount. The refund excludes the value of 3rd party products and services without any exceptions.</li>
                            <li><span class="nmb">2.2.</span> To apply for a refund, the User must submit a refund request through his registered billing account.</li>
                            <li><span class="nmb">2.3.</span> Refunds are provided to the User's credit balance or via the same payment medium that was used by the User if applicable - the User's payment gateway account or the User's bank account.</li>
                            <li><span class="nmb">2.4.</span> Refunds are applicable only if the User is in compliance with Terms of Service, has not undergone suspension or termination due to violations of Terms of Service before and has a clear abuse history.</li>
                            <li><span class="nmb">2.5.</span> Refunds are provided for shared hosting, reseller hosting, VPS hosting and Cloud hosting services.</li>
                            <li><span class="nmb">2.6.</span> Refunds are provided for each service accordingly. However, the User may receive a refund for a specific service only once.</li>
                            <li><span class="nmb">2.7.</span> Refunds are applicable only if the service is provided directly by the Provider. 3rd party products or additional services are not refundable. If the User has purchased a service that includes a free domain name and the user canceled the purchased product, the standard price for the domain name will be deducted from the refund amount. The refund excludes the value of 3rd party products and services without any exceptions.</li>
                            <li><span class="nmb">2.8.</span> Refunds are not provided if the service was not accessible because of 3rd party illegal activities.</li>
                            <li><span class="nmb">2.9.</span> The Provider reserves the right to make the final decision on any refund requests issued.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-5-3"></div>

                        <h3 class="tos-top-sublevel-title">3. Chargebacks, Reversals and Retrievals</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">3.1.</span> Chargebacks are not considered an acceptable form of refunding. All payment refunds must be requested in accordance to the refund policy instead of issuing a chargeback or opening a transaction dispute. Chargebacks and/or disputes will be considered as payment fraud and will be a subject to full investigation.</li>
                            <li><span class="nmb">3.2.</span> The Provider will use all information including the User’s billing account profile, login history data and any communication between the Provider and the User in order to appeal the chargeback or other payment dispute.</li>
                            <li><span class="nmb">3.3.</span> If the Provider receives a chargeback or a payment dispute from a Credit Card company, bank, via PayPal or any other payment gateway, all services related to the billing account of the User may be suspended without prior notice. Where applicable, domain disposal may be limited and requests to perform a transfer may be denied. These conditions apply to third party products as well.</li>
                            <li><span class="nmb">3.4.</span> For every instance of chargeback or any other form of transaction retrieval, the User agrees to pay a $30 administration fee.</li>
                            <li><span class="nmb">3.5.</span> To restore the services, which were suspended due to a chargeback, a reversal or a retrieval, the User is bound to make sure that all chargebacks, reversals and/or retrievals would be withdrawed. Moreover, any financial mismatch caused by the chargebacks, reversals and/or retrievals must be fully covered by the User before the services are restored. Any double payment resulting from this process will be added to the User’s billing account in the form of account credit.</li>
                        </ul>

                    </div>

                    <div class="scroll-id-target" id="tos-section-6"></div>

                    <h2 class="tos-top-level-title">VI. Suspension and Termination</h2>

                    <div class="tos-desc">

                        <div class="scroll-id-target" id="tos-section-6-1"></div>

                        <h3 class="tos-top-sublevel-title">1. Suspension</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">1.1.</span> The Provider reserves the right to suspend and/or terminate any service for the User who either deliberately and intentionally or unintentionally violates Terms of Service.</li>
                            <li><span class="nmb">1.2.</span> The Provider reserves the right not to contact the User prior to suspension. However the Provider may contact the User prior to suspension in attempt to stop and avoid further adverse actions carried out by the User or third parties through the User's resources.</li>
                            <li><span class="nmb">1.3.</span> The User must act immediately after receiving a notice from the Provider and take necessary actions. A time-frame may be specified by the Provider for the User to restore compliance with Terms of Service.</li>
                            <li><span class="nmb">1.4.</span> Failing to cooperate will result in suspension and/or termination of services.</li>
                            <li><span class="nmb">1.5.</span> The Provider will act based on "User's non-compliance" terms.</li>
                            <li><span class="nmb">1.6.</span> The Provider reserves the right to temporarily suspend the server because of security issues until they are eliminated. This is done to secure all the users using the server and it is not considered downtime therefore the User is not eligible for a refund.</li>
                            <li><span class="nmb">1.7.</span> Service data is kept for 45 days after suspension. However, the User may renew the services during this period for an additional fee.</li>
                            <li><span class="nmb">1.8.</span> 45 days after the suspension the services are terminated and the Provider no longer preserves any service data.</li>
                            <li><span class="nmb">1.9.</span> If the User violates Acceptable Use Policy, the Provider reserves the right to terminate the services immediately as to avoid damage to other users and/or 3rd parties.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-6-2"></div>

                        <h3 class="tos-top-sublevel-title">2. Termination</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">2.1.</span> In cases where the service was used deliberately and intentionally to cause damage to any property in any shape or form, the Provider has the right to terminate the service immediately, with or without prior notification.</li>
                            <li><span class="nmb">2.2.</span> If the User acts deliberately and intentionally to cause damage to any property in any shape or form, a refund is not applicable.</li>
                            <li><span class="nmb">2.3.</span> The services are terminated immediately if the User:
                                <ul class="simple-list">
                                    <li>creates a proxy service;</li>
                                    <li>is carrying out actions related with copyrighted data without having copyrights to that material;</li>
                                    <li>is using the server for unwanted e-mail (SPAM) or harmful software sending;</li>
                                    <li>is using the server to generate bitcoins (is performing bitmining);</li>
                                    <li>is serving/sharing files using torrent protocol;</li>
                                    <li>is scamming/phishing or other harmful for other internet users activities;</li>
                                    <li>is performing any kind of other illegal activities.</li>
                                </ul>
                            </li>
                            <li><span class="nmb">2.4.</span> The Provider will act based on "User's non-compliance" terms.</li>
                        </ul>

                    </div>

                    <div class="scroll-id-target" id="tos-section-7"></div>

                    <h2 class="tos-top-level-title">VII. Acceptable Use Policy</h2>

                    <div class="tos-desc">

                        <div class="scroll-id-target" id="tos-section-7-1"></div>

                        <h3 class="tos-top-sublevel-title">1. Prohibited Uses</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">1.1.</span> The service may be used for lawful purposes only. The User is prohibited from transmitting any material in violation of any international, national and local regulations.</li>
                            <li><span class="nmb">1.2.</span> The prohibited uses include (but are not limited to) these actions:
                                <ul class="simple-list">
                                    <li>Fraud;</li>
                                    <li>Spam;</li>
                                    <li>Serving/sharing files using torrent protocol;</li>
                                    <li>Scamming/phishing;</li>
                                    <li>Crypto currency mining;</li>
                                    <li>Malicious usage to disturb the work of the server;</li>
                                    <li>Abusive actions against other servers or users;</li>
                                    <li>DDos flooding or any other kind of flooding.</li>
                                </ul>
                            </li>
                            <li><span class="nmb">1.3.</span> The Provider reserves the right to refuse the service if the User uses the service for prohibited uses that violate Terms of Service, are not compatible with our policies or are considered illegal, harmful or threatening in any way.</li>
                            <li><span class="nmb">1.4.</span> If the User chooses to continue using the services for prohibited uses, the Provider will act under “User’s non-compliance” terms and may suspend or terminate the service with or without prior notice.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-7-2"></div>

                        <h3 class="tos-top-sublevel-title">2. Anti-Spam Policy</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">2.1.</span> The Provider does not tolerate spam and the spread of spam is strictly forbidden during the use of service.</li>
                            <li><span class="nmb">2.2.</span> While using the service for mailing the User undertakes the following obligations:
                                <ul class="simple-list">
                                    <li>Not to email to the people, who have not expressed any request in any way to receive emails;</li>
                                    <li>Not to provide and not to propose the service under the application of which 3rd parties could spread spam;</li>
                                    <li>To implement and support appropriate technical means that secure that any 3rd parties do not spread spam;</li>
                                    <li>Apply opt-in principle for any information sent via electronic means, and suggest effective means to refuse received spam;</li>
                                    <li>Not to collect, store, publish and spread data (e.g. email addresses) under the aid of which the spam can be sent;</li>
                                    <li>Not to create open or easily exploitable mailing relays.</li>
                                </ul>
                            </li>
                            <li><span class="nmb">2.3.</span> If the User does not follow the obligations listed above, the Provider will act under “User’s non-compliance” terms and may suspend or terminate the service with or without prior notice.</li>
                            <li><span class="nmb">2.4.</span> If the User accused of spam rejects the accusation on the grounds that the complaining party (in this case – the recipient) agreed to subscribe to the User’s mailing list, the User must present proof of the recipient’s subscription. The User must provide logs which contain the complaining party’s email address, the exact date and time of the subscription confirmation, and the IP address which the recipient used during the subscription confirmation.</li>
                            <li><span class="nmb">2.5.</span> If the User accused of spam refuses to cooperate and provide the required information, the Provider considers the User being guilty of the Spam Policy violation.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-7-3"></div>

                        <h3 class="tos-top-sublevel-title">3. Unlimited Resources Policy</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">3.1.</span> Shared hosting services are designed for small to medium size personal, business, organization websites. Shared hosting resources may be used for active email, web files and content of the User’s website(s) only.</li>
                            <li><span class="nmb">3.2.</span> Shared hosting resources cannot be used for:
                                <ul class="simple-list">
                                    <li>File sharing;</li>
                                    <li>Extensive video, photo, log files, software storage;</li>
                                    <li>Document or other archive storage;</li>
                                    <li>Backup storage;</li>
                                    <li>Website(s) created to drive traffic to another website(s).</li>
                                </ul>
                            </li>
                            <li><span class="nmb">3.3.</span> The Provider does not limit disk space resources but applies a limited amount of iNode resources depending on the shared hosting plan.</li>
                            <li><span class="nmb">3.4.</span> The Provider does not limit disk space and bandwidth resources usage as long as the usage of aforementioned resources complies with Terms of Service.</li>
                            <li><span class="nmb">3.5.</span> If disk space and bandwidth resource usage presents a risk to data storage, networking, stability, security, performance and uptime of a web hosting server and affects other services or users, the Provider reserves the right to suspend the User’s service.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-7-4"></div>

                        <h3 class="tos-top-sublevel-title">4. Copyright Policy</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">4.1.</span> The User is prohibited to carry out any actions opposing legal acts, violations of rights to third parties, including (but not limited to) storing, publishing, sending, distributing, making publicly available or otherwise transferring any content considered by legal acts to be prohibited, limited, threatening, discriminative, violating others’ rights.</li>
                            <li><span class="nmb">4.2.</span> The User’s content must not oppose any legal acts within United Kingdom as well as the location of the service, or any party entitled to the content, as well as international regulations.</li>
                            <li><span class="nmb">4.3.</span> If the User uploads copyrighted material without permission, the copyright holder or an interested 3rd party may report copyright infringement to the Provider.</li>
                            <li><span class="nmb">4.4.</span> The Provider reserves the right to act upon judgement and suspend or terminate the services for the User if the User is reasonably suspected of copyright infringement.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-7-5"></div>

                        <h3 class="tos-top-sublevel-title">5. Unacceptable Material</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">5.1.</span> The Provider does not tolerate certain unacceptable material, stored on his servers. Such material includes (but is not limited to):
                                <ul class="simple-list">
                                    <li>Gambling websites;</li>
                                    <li>Websites promoting illegal activities;</li>
                                    <li>Child pornography;</li>
                                    <li>Racist  content;</li>
                                    <li>Incitement of strife, hatred or violence;</li>
                                    <li>Other abusive websites.</li>
                                </ul>
                            </li>
                            <li><span class="nmb">5.2.</span> The Provider reserves the right to refuse the service if the material stored by the User violates Terms of Service, is not compatible with our policies or is considered illegal, harmful or threatening in any way.</li>
                            <li><span class="nmb">5.3.</span> If the User chooses to store unacceptable material, the Provider will act under “User’s non-compliance” terms and may suspend or terminate the service with or without prior notice.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-7-6"></div>

                        <h3 class="tos-top-sublevel-title">6. Unacceptable Resource Usage</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">6.1.</span> Over-usage of resources and malicious usage is strictly prohibited.</li>
                            <li><span class="nmb">6.2.</span> In cases where outside malicious activity affects neighboring users, the Provider reserves the right take action against the User's VPS to stop any effects to the performance of service.</li>
                            <li><span class="nmb">6.3.</span> This is taken against (but not limited to): containers under DDoS, containers used for scanning. As such, suspensions, null routes or connection restrictions may be applied.</li>
                            <li><span class="nmb">6.4.</span> Any of these common techniques and tools will be used to restore balance of service neighborhood for as long as necessary.</li>
                            <li><span class="nmb">6.5.</span> The User agrees that such service outage is not considered downtime.</li>
                        </ul>

                    </div>

                    <div class="scroll-id-target" id="tos-section-8"></div>

                    <h2 class="tos-top-level-title">VIII. Report Submission Policy</h2>

                    <div class="tos-desc">

                        <div class="scroll-id-target" id="tos-section-8-1"></div>

                        <h3 class="tos-top-sublevel-title">1. General Guidelines</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">1.1.</span> The Provider accepts reports from the complaining party if the User is conducting illegal activities while using Host1Plus services.</li>
                            <li><span class="nmb">1.2.</span> All reports must be prepared in fluent English.</li>
                            <li><span class="nmb">1.3.</span> The report is applicable only if it includes legal grounds which sustain the claim.</li>
                            <li><span class="nmb">1.4.</span> The complaining party must file the report in person, through a lawyer or a legal firm, authorized to represent the person or the company. If the report is submitted by a lawyer or a legal firm, a proof of representation agreement must be presented along with the report.</li>
                            <li><span class="nmb">1.5.</span> The complaining party must include sufficient and accurate contact information – an email and a telephone number.</li>
                            <li><span class="nmb">1.6.</span> The report must be submitted via email or through the Ticket System only.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-8-2"></div>

                        <h3 class="tos-top-sublevel-title">2. Copyright Violation Report</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">2.1.</span> In case of copyright violation, the Provider acts as a mediator of the complaining party and the User, if the User agrees to cooperate.</li>
                            <li><span class="nmb">2.2.</span> The copyright infringement report must contain (but is not limited to) the reasoning of the report and direct links to the copyrighted material. If the report is made by the copyright owner, it must contain evidence of the copyright ownership.</li>
                            <li><span class="nmb">2.3.</span> The Provider reserves the right to request additional information in order to confirm the violation of the copyright policy.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-8-3"></div>

                        <h3 class="tos-top-sublevel-title">3. Phishing Report</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">3.1.</span> An email phishing report must contain a copy of the phishing email in question. It must be attached in text form with full headers, timestamps, source and destination data.</li>
                            <li><span class="nmb">3.2.</span> A screenshot of the phishing email in question is optional.</li>
                            <li><span class="nmb">3.3.</span> A website based-phishing report must contain a full URL path (not only a domain or a subdomain) to the phishing page.</li>
                            <li><span class="nmb">3.4.</span> A screenshot of the phishing website with a brief description is mandatory. The exact time of the screenshot take must be provided as well.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-8-4"></div>

                        <h3 class="tos-top-sublevel-title">4. SPAM Report</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">4.1.</span> An email spam report must contain a copy of the email in question. It must be attached to the report in text, with full headers, timestamps, source and destination data.</li>
                            <li><span class="nmb">4.2.</span> The email spam report must contain a brief explanation of why the complaining party considers the email to be spam.</li>
                            <li><span class="nmb">4.3.</span> A screenshot of the email in question is optional.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-8-5"></div>

                        <h3 class="tos-top-sublevel-title">5. Network Abuse Report</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">5.1.</span>In case of network abuse, including (but not limited to) flooding, spoofing, scanning, brute-forcing, exploiting and botnets, the complaining party must provide sufficient amount of log excerpts and indicate the exact time of the incident in question.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-8-6"></div>

                        <h3 class="tos-top-sublevel-title">6. Defamation and Libel Report</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">6.1.</span>In case of defamation and libel, the Provider reserves the right not to be put in the position to determine the veracity of the User.</li>
                            <li><span class="nmb">6.2.</span>The Provider will not judge, censor or control the content in any other form, even if the complaining party finds it offensive.</li>
                            <li><span class="nmb">6.3.</span>The complaining party has the right to express its concerns directly to the creator of the content only.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-8-7"></div>

                        <h3 class="tos-top-sublevel-title">7. Child Pornography Report</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">7.1.</span> A child pornography report must contain the exact URL where the abuse is taking place.</li>
                            <li><span class="nmb">7.2.</span> Any additional information to help the Provider determine the exact source of the content is strongly appreciated.</li>
                            <li><span class="nmb">7.3.</span> Suspected offenders are immediately reported to the legal authorities. The Provider fully cooperates with any ongoing investigation.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-8-8"></div>

                        <h3 class="tos-top-sublevel-title">8. Credit Card Fraud Report</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">8.1.</span> In case of a credit card fraud, the complaining party must provide the first and the last name of the card holder along with the first 4 and last 4 numbers of the credit card in question, card type and expiration date.</li>
                            <li><span class="nmb">8.2.</span> In case of unauthorized charges, the complaining party must provide the exact time and the amount of the charge in question.</li>
                        </ul>

                    </div>

                    <div class="scroll-id-target" id="tos-section-9"></div>

                    <h2 class="tos-top-level-title">IX. Privacy Policy</h2>

                    <div class="tos-desc">

                        <div class="scroll-id-target" id="tos-section-9-1"></div>

                        <h3 class="tos-top-sublevel-title">1. Intellectual Property</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">1.1.</span> Terms of Service does not give or transfer the User any rights to intellectual property that is not in his possession.</li>
                            <li><span class="nmb">1.2.</span> All rights to the software used during the process of service provision are protected and belong to the Provider or the Provider is authorized to use them.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-9-2"></div>

                        <h3 class="tos-top-sublevel-title">2. Collected Private Information</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">2.1.</span> In order to use the website and the services, the User is required to register and provide his or her personal information during the registration.</li>
                            <li><span class="nmb">2.2.</span> Collected private information is not disclosed to any third party by the Provider, unless specifically stated otherwise in these terms, for example in cases when appropriate legal authorities request such data in case of criminal activity performed by the User or through the User’s account.</li>
                            <li><span class="nmb">2.3.</span> The Provider reserves the right to use all of the information provided by any visitor, which he or she submitted during the registration process, as well as any other information about his activity within our services.</li>
                            <li><span class="nmb">2.4.</span> The Provider reserves the right to use all the collected private information, including (but not limited to) the User’s email address for service related and marketing purposes. </li>
                            <li><span class="nmb">2.5.</span> By signing up or providing his or her personal information, the User agrees to receive service related and marketing information, sent to him or her by the Provider. </li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-9-3"></div>

                        <h3 class="tos-top-sublevel-title">3. Collected Anonymous Information</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">3.1.</span> The Provider may collect certain information every time the visitor or the User visits the Website in order to provide higher quality services to the users of the service.</li>
                            <li><span class="nmb">3.2.</span> The collected anonymous information includes (but is not limited to) visitors' IP addresses, related data (e.g., the internet address of the last website, which the person has visited before his/her visit to the website, browser and operating system types, common search yields from various search programs, oriented towards internal advertising, popular search words, etc.).</li>
                            <li><span class="nmb">3.3.</span> The Provider may collect and conserve the indicated information about all and any visitors, regardless if they are registered or not.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-9-4"></div>

                        <h3 class="tos-top-sublevel-title">4. Cookies Policy</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">4.1.</span> The Provider may collect certain information using cookies in compliance with privacy and data protection laws and regulatory framework.</li>
                            <li><span class="nmb">4.2.</span> The tracked information is used to supply, improve and manage the services. The use of the tracked information includes research, statistics and advertising purposes.</li>
                            <li><span class="nmb">4.3.</span> The tracked information is not transmitted to 3rd parties in any circumstances.</li>
                        </ul>

                    </div>

                    <div class="scroll-id-target" id="tos-section-10"></div>

                    <h2 class="tos-top-level-title">X. Customer Support</h2>

                    <div class="tos-desc">

                        <div class="scroll-id-target" id="tos-section-10-1"></div>

                        <h3 class="tos-top-sublevel-title">1. Basic Support Service Agreement</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">1.1.</span> Basic support is available to every User under limited service basis.</li>
                            <li><span class="nmb">1.2.</span> The Provider reserves the right to deny any support service request if it is considered not eligible for non-paid support.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-10-2"></div>

                        <h3 class="tos-top-sublevel-title">2. Support Availability</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">2.1.</span> The User may contact the Provider with a professional assistance request, in which case the Provider will respond based on the type of request and the User's acquired service level.</li>
                            <li><span class="nmb">2.2.</span> The Provider reserves the right to refuse service request under such conditions as insufficient service level or another tenable reason. Accordingly, a different service level may be offered by the Provider in order to carry out the request.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-10-3"></div>

                        <h3 class="tos-top-sublevel-title">3. Non-Active User Support Service</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">3.1.</span> The User who does not own any active services provided by the Provider, has the right to request support for sales, general guidance and assistance questions.</li>
                            <li><span class="nmb">3.2.</span> The Provider reserves the right to refuse service to any requests which are intolerable or harmful in any way.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-10-4"></div>

                        <h3 class="tos-top-sublevel-title">4. Active User Support Service</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">4.1.</span> The User who owns an active service provided by the Provider has the right to request basic support to ensure proper working conditions of any service from the Provider.</li>
                            <li><span class="nmb">4.2.</span> Basic support service comes free with any service provided by the Provider.</li>
                            <li><span class="nmb">4.3.</span> The User has the right to request additional support that he might need.</li>
                            <li><span class="nmb">4.4.</span> The Provider has the right to provide guidance, direct support or request the User to commit to a higher service level of support. If the required support service level is not available or declined by the User, the Provider reserves the right to refuse any additional support.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-10-5"></div>

                        <h3 class="tos-top-sublevel-title">5. Advanced Support Service Agreement</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">5.1.</span> The Provider provides higher level support for more complicated issues and tasks.</li>
                            <li><span class="nmb">5.2.</span> The User may acquire higher level support for an additional fee.</li>
                            <li><span class="nmb">5.3.</span> Advanced support service request must be sent and registered in the form of a ticket. The request must include a detailed explanation on what needs to be done. The Provider will evaluate the request and suggest a certain type of support service, regarding the nature and complexity of the task. While evaluating, the Provider undertakes the obligation to suggest the most appropriate and least expensive method of billing.</li>
                            <li><span class="nmb">5.4.</span> The User is free to accept the offer and send a prepayment or withdraw the request at once. The User will only be billed after both parties reach an agreement.</li>
                            <li><span class="nmb">5.5.</span> This support service is based on-agreement basis following 4 steps described below:
                                <ul class="simple-list">
                                    <li>Full customer inquiry, with detailed explanation on what must be done;</li>
                                    <li>Administrator's reply with scheduling and invoicing;</li>
                                    <li>The User’s agreement and payment;</li>
                                    <li>Task execution.</li>
                                </ul>
                            </li>
                            <li><span class="nmb">5.6.</span> After the task is completed and the response is submitted in a form of a report, the inquiry is considered as fully fulfilled. The User has the right request additional information on the configuration done.</li>
                            <li><span class="nmb">5.7.</span> The Provider reserves the right to refuse any additional requests after the main task is complete if additional requests are considered not related to the issue.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-10-6"></div>

<?php 
/*
    -- temporary hiding

                        <h3 class="tos-top-sublevel-title">6. Extra-Care Support Service</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">6.1.</span> Extra-Care support is an advanced paid service provided by the Host1Plus Support Department.</li>
                            <li><span class="nmb">6.2.</span> Extra-Care support service covers advanced server management assistance for multiple services used by the User.</li>
                            <li><span class="nmb">6.3.</span> Extra-Care support is divided into two levels of technical assistance depending on the complexity of the issue.</li>
                            <li><span class="nmb">6.4.</span> 1st level Extra-Care support may be acquired as a monthly service. It covers 4 hours of server management assistance per single month. The monthly price for the service is $60.</li>
                            <li><span class="nmb">6.5.</span> 1st level Extra-Care support includes:
                                <ul class="simple-list">
                                    <li>Lamp setup;</li>
                                    <li>DirectAdmin installation, configuration, migration;</li>
                                    <li>WHM/cPanel management;</li>
                                    <li>CentOS, webmin web panel installation;</li>
                                    <li>OpenVPN setup and configuration;</li>
                                    <li>Desktop, VNC installation;</li>
                                    <li>SSH configuration;</li>
                                    <li>cPanel, ISPconfig migration;</li>
                                    <li>OS reinstallation.</li>
                                </ul>
                            </li>
                            <li><span class="nmb">6.6.</span> Unused monthly hours of 1st level Extra-Care support are not combined neither with subsequent 1st level Extra-Care support hours, nor 2nd level Extra-Care support hours.</li>
                            <li><span class="nmb">6.7.</span> 2nd level Extra-Care support service is a server management service that may be acquired by contacting the Support Department directly at support@host1plus.com. 2nd level Extra-Care support is a one-time hourly service. The price for the service is $100 per hour.</li>
                            <li><span class="nmb">6.8.</span> 2nd level Extra-Care support includes:
                                <ul class="simple-list">
                                    <li>Network and firewall configuration;</li>
                                    <li>BGP setup;</li>
                                    <li>Load balancer setup and configuration;</li>
                                    <li>Apache and NGINX advanced configuration;</li>
                                    <li>Squid or other caching software setup and configuration;</li>
                                    <li>Custom monitoring setup and configuration;</li>
                                    <li>GRE tunnel setup and configuration.</li>
                                </ul>
                            </li>
                            <li><span class="nmb">6.9.</span> Extra-Care support does not cover issues related to 3rd party software and scripts installed by the User on the server. However, if such software is installed, the Provider may offer recommendations.</li>
                            <li><span class="nmb">6.10.</span> A single inquiry is at least 30 minutes, depending on the complexity of the issue. The Provider reserves the right to change these measures.</li>
                            <li><span class="nmb">6.11.</span> Extra-Care support service may be cancelled by the User anytime at the Client Area.</li>
                        </ul>

*/
?>
<?php 
/*

                        <div class="scroll-id-target" id="tos-section-10-7"></div>

                        <h3 class="tos-top-sublevel-title">7. Dedicated Administrator Support Service</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">7.1.</span> Dedicated administrator support service is the second level of paid support provided by the Host1Plus Support Department and Host1Plus Technical Department.</li>
                            <li><span class="nmb">7.2.</span> This support level is a one-time payment service. It covers all intermediate and advanced tasks. Tasks covered may include basic custom installations to full system customization.</li>
                        </ul>
*/                        
?>

                    </div>

                    <div class="scroll-id-target" id="tos-section-11"></div>

                    <h2 class="tos-top-level-title">XI. Transfer Policy</h2>

                    <div class="tos-desc">

                        <ul class="terms">
                            <li>The Provider may transfer one master account for shared hosting users and up to 30 accounts for reseller. The Provider can transfer up to 2GB size accounts only.</li>
                            <li>Free transfer service is only offered for “as it is” transfers if website content and data with no updates to the site configuration, aside from database connection details. Copying an existing site to a new domain or changing the URL of an existing site is NOT a free service.</li>
                            <li>The Provider reserves the right to refuse transfer service to the User if it is considered impossible to migrate some or all account data.</li>
                            <li>In no event shall the Provider be held responsible for any lost or missing data or files from a transfer to Host1Plus. The User is responsible for backing up his data.</li>
                        </ul>

                    </div>

                    <div class="scroll-id-target" id="tos-section-12"></div>

                    <h2 class="tos-top-level-title">XII. Reseller program</h2>

                    <div class="tos-desc">

                        <p>Please read these terms carefully - it is a condition of your being a member of the Reseller Program that you comply with them.</p>

                        <div class="scroll-id-target" id="tos-section-12-1"></div>

                        <h3 class="tos-top-sublevel-title">1. Terms of Agreement</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">1.1.</span> Our agreement with Reseller will commence when the Reseller accepts these Terms of Service and will continue until either of us terminates the agreement by  written notice to the other, or else as these terms provide.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-12-2"></div>

                        <h3 class="tos-top-sublevel-title">2. Amendments</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">2.1.</span> We have the right to make amendments to these terms, the pricing structure applicable to the Reseller Program and the Service generally, and the terms of operation of any Service at any time.</li>
                            <li><span class="nmb">2.2.</span> We will post a general notice of amendments on a page of Press Release. Your use of the Services after publication of a notice of amendments will be an acceptance of those amendments in respect of all Services. If you disagree with amended terms, you may terminate your agreement with us by 90 days’ notice in writing. You must keep yourself informed of changes to Terms of Service by checking Press Release or Terms of Service regularly.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-12-3"></div>

                        <h3 class="tos-top-sublevel-title">3. Services</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">3.1.</span> We will assign you a Reseller client group, which will provide you with access to the Host1Plus Reseller discounts and other benefits. We reserve the right to make changes to discounts and other benefits at any time.</li>
                            <li><span class="nmb">3.2.</span> Reseller agree that all Services ordered from Host1Plus will be supplied to Reseller upon the terms of our online Terms of Service, as amended from time to time.</li>
                            <li><span class="nmb">3.3.</span> Reseller agree that Host1Plus's Services do not include sale, lease or other grant of the right to possession of web servers or related equipment. All physical equipment used in delivery of the Services will remain the property of and in the possession or control of Host1Plus.</li>
                            <li><span class="nmb">3.4.</span> Reseller grants Host1Plus a non-exclusive, royalty free license to use, transmit, display, adapt and reproduce all information, data, text, logos, images, audio, movie clips and/or content in any form which constitutes your customers' web sites ("Customer Data"):</li>
                            <ul class="terms-list">
                                <li><span class="nmb">a.</span> in order to fulfil our obligations under this agreement; and</li> 
                                <li><span class="nmb">b.</span> for the purpose of providing ongoing Services directly to customers where Reseller becomes insolvent, cease to operate business, or</li> 
                                <li><span class="nmb">c.</span> if Reseller fails to respond for a period of 28 days or more to reasonable attempts to contact (where Reseller has not given Host1Plus prior notice to be out of contact).</li> 
                            </ul>
                        </ul>
                        
                        <div class="scroll-id-target" id="tos-section-12-4"></div>

                        <h3 class="tos-top-sublevel-title">4. Charges and Payment</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">4.1.</span> Reseller must pay for:</li>
                            <ul class="terms-list">
                                <li><span class="nmb">a.</span> All charges for the Services in advance as notified by Host1Plus;</li> 
                                <li><span class="nmb">b.</span> Additional products, third party software and product upgrades that Reseller orders at the rates set out as per the Service pricing. Note: no refunds are issued for the cancellation of third party products.</li> 
                            </ul>
                            <li><span class="nmb">4.2.</span> Members of the Host1Plus Reseller Program who have agreed to these Terms of Service and otherwise fulfil the criteria set out in the table below will be entitled to acquire Services at the following discounted rates.</li>
                        </ul>

                        <table class="reseller-info-table">
                            <thead>
                                <tr>
                                    <th width="25%">Reseller Level</th> 
                                    <th width="50%">Total amount of income generated for Host1Plus</th> 
                                    <th width="25%">Volume-based Discount</th> 
                                </tr> 
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Personal</td> 
                                    <td>Up to $4,999</td> 
                                    <td>5%</td> 
                                </tr>
                                <tr>
                                    <td>Business</td> 
                                    <td>$5,000 - $14,999</td> 
                                    <td>10%</td> 
                                </tr>
                                <tr>
                                    <td>Enterprise</td> 
                                    <td>$15,000+</td> 
                                    <td>15%</td> 
                                </tr>
                            </tbody>
                        </table>

                        <style>
                            .reseller-info-table {
                                margin: 0 0 20px;
                                border: 1px solid #ccc;
                                font-size: 14px;
                            }
                            .reseller-info-table th,
                            .reseller-info-table td {
                                padding: 2px 10px;
                            }
                            .reseller-info-table tbody tr {
                                border-top: 1px solid #ccc;
                            } 

                            .reseller-info-table tbody tr td {
                                text-align: center;
                            }
                            .reseller-info-table thead tr th:first-child,
                            .reseller-info-table tbody tr td:first-child {
                                text-align: left;
                            }
                        </style>

                        <div class="scroll-id-target" id="tos-section-12-5"></div>

                        <h3 class="tos-top-sublevel-title">5. Suspension and Termination</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">5.1.</span> Reseller may continue to be a member of the Reseller Program so long as Reseller is not in breach of these terms or the specific terms of supply applicable to any Service Reseller has contracted for. If Reseller is in breach of either, and (where the breach can be remedied) Reseller has not remedied the breach, we will have the right to suspend or terminate Reseller participation in the Reseller Program and the provision of any or all of Reseller accounts without notice.</li>
                            <li><span class="nmb">5.2.</span> Reseller will remain liable for all amounts owing to Host1Plus if we suspend or terminate this agreement for any reason.</li>
                            <li><span class="nmb">5.3.</span> Subject to clause 5.5, Reseller has the right to terminate participation in the Reseller Program by giving written notice to us. Doing so will not terminate Reseller agreement for supply of Services, which can be terminated only in accordance with the terms of supply of the particular Services.</li>
                            <li><span class="nmb">5.4.</span> Once Reseller participation in the Reseller Program has been terminated, from the date of such termination Reseller will no longer be entitled to access the discounts and other benefits of the Reseller Program.</li>
                            <li><span class="nmb">5.5.</span> If Host1Plus terminates this agreement pursuant to clause 6.1, or if a receiver, a trustee in bankruptcy, a liquidator, an administrator or other like person is appointed to part or all of Reseller assets or business, then Host1Plus may contact Reseller customers without notice to Reseller in order to ensure continued provision of Services to those customers on the basis that they contract and deal directly with Host1Plus.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-12-6"></div>

                        <h3 class="tos-top-sublevel-title">6. Acceptable Use Policy and Privacy Policy</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">6.1.</span> In accepting these Terms of Service, the Reseller also acknowledges he/she has read and accepted our Acceptable Use Policy and Privacy Policy on Host1Plus's website as amended from time to time.</li>
                            <li><span class="nmb">6.2.</span> The Acceptable Use Policy and the Privacy Policy form part of these Terms of Service. Failure to abide by either policy will constitute a breach of these Terms of Service, and will give rise to our right to terminate or suspend the Services, as set out in clause 5.1 above.</li>
                            <li><span class="nmb">6.3.</span> Reseller is responsible for the use of the Services ordered through the Reseller Account and you must ensure that your customers and any other person using the Service though your Reseller Account also complies with our Terms of Service of supply of Services, the Acceptable Use Policy and the Privacy Policy, and any other instructions that Host1Plus gives you from time to time.</li>
                            <li><span class="nmb">6.4.</span> You must comply with any rules imposed by any third party whose content or service you access using the Service.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-12-7"></div>

                        <h3 class="tos-top-sublevel-title">7. Limitation of liability</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">7.1.</span> We have no responsibility for, or liability in relation to, any person accessing data on Reseller sites or Reseller customers' sites.</li>
                            <li><span class="nmb">7.2.</span> Reseller is responsible for providing direct communication and support to his customers. Host1Plus has no direct communication or obligations to the Clients of the Reseller.</li>
                            <li><span class="nmb">7.3.</span> Other than where clause 7.1 applies, in all other respects Host1Plus will have no liability to Reseller pursuant to this agreement for all loss or damage, howsoever arising.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-12-8"></div>

                        <h3 class="tos-top-sublevel-title">8. Reseller obligation</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">8.1.</span> If you are a Reseller of the Services you must procure that any customers that you have enter into Terms of Service in substantially the same form as these Terms of Service (including the Acceptable Use Policy and Privacy Policy).</li>
                            <li><span class="nmb">8.2.</span> Reseller must ensure that his customers comply with the above Terms of Service. If Resellers’ customers do not do so, and this results in any loss or damage being suffered by us, Reseller agree to indemnify us for any such loss or damage.</li>
                            <li><span class="nmb">8.3.</span> In order to remain on the Reseller Program, Reseller must:</li>
                            <ul class="terms-list">
                                <li><span class="nmb">a.</span> maintain at least one full-paying account with Host1Plus;</li> 
                                <li><span class="nmb">b.</span> meet Host1Plus's credit checking requirements; and</li> 
                                <li><span class="nmb">b.</span> exhibit consistent reselling of Host1Plus products and services in an ongoing manner over time.</li> 
                            </ul>
                            <li><span class="nmb">8.4.</span> If you cease reselling Host1Plus products and services for a period longer than six months, Host1Plus reserves the right to cancel your membership of the Reseller Program without notice.</li>
                        </ul>

                    </div>

                    <div class="scroll-id-target" id="tos-section-13"></div>

                    <h2 class="tos-top-level-title">XIII. Affiliate Policy</h2>

                    <div class="tos-desc">

                        <div class="scroll-id-target" id="tos-section-13-1"></div>

                        <h3 class="tos-top-sublevel-title">1. Joining the Affiliate Program</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">1.1.</span> The User’s participation in the program is solely for this purpose: to legally advertise Host1Plus and its partners and receive a commission on sales generated by the User’s referrals. The User also agrees to receive emails regarding the Affiliate program.</li>
                            <li><span class="nmb">1.2.</span> By registering as an Affiliate, the User agrees to the terms of the <a href="#tos-section-9-1" data-scrollto="#tos-section-9-1">Privacy Policy</a> and the Affiliate Policy.</li>
                            <li><span class="nmb">1.3.</span> Affiliate accounts with inaccurate contact information are not approved. The Provider reserves the right to decline any application that does not meet the criteria.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-13-2"></div>

                        <h3 class="tos-top-sublevel-title">2. Affiliate Account Suspension</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">2.1.</span> The Provider reserves the right to suspend the User's account anytime based on violations of Terms of Service and/or the affiliate policy.</li>
                            <li><span class="nmb">2.2.</span> The Provider reserves the right to determine if the User’s website is not suitable for our Affiliate program, including (but not limited to) the content that is not compatible with the Provider’s policies or is considered illegal, harmful or threatening in any way.</li>
                            <li><span class="nmb">2.3.</span> Affiliates are prohibited from using an identical or virtually identical Host1Plus trademark as a part of their domain, second level domain name and/or subdomain.</li>
                            <li><span class="nmb">2.4.</span> Affiliate accounts referring a large number of fraudulent clients, will be suspended. The Provider reserves the right to determine fraud following internal procedures.</li>
                            <li><span class="nmb">2.5.</span> Affiliate accounts referring a large number of clients who do chargebacks after the 45- day pending period may be suspended.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-13-3"></div>

                        <h3 class="tos-top-sublevel-title">3. Linking and Advertising</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">3.1.</span> The Affiliate may use graphic and text links both on his website and e-mails.</li>
                            <li><span class="nmb">3.2.</span> Host1Plus may also be advertised offline in classified ads, magazines and newspapers.</li>
                            <li><span class="nmb">3.3.</span> The Affiliate may only use the graphics and texts provided to him by the Provider.</li>
                            <li><span class="nmb">3.4.</span> The Publisher is not allowed to generate incentivized traffic (any kind of compensations for click, purchase or advertiser offer completion).</li>
                            <li><span class="nmb">3.5.</span> The Affiliate may not use cookie stuffing techniques, setting tracking cookie without the referral clicking on the Affiliate link.</li>
                            <li><span class="nmb">3.6.</span> The Affiliate may not use redirected pages and links to send a referral to the Provider’s website.</li>
                            <li><span class="nmb">3.7.</span> Affiliates may not develop browser extensions, pop-ups or other tools that redirect the browser through affiliate links when visiting a site where affiliate offers are available.</li>
                            <li><span class="nmb">3.8.</span> Domain forwarding is strictly prohibited – the Affiliate may not use a domain to forward directly to the Provider’s website using his Affiliate link.</li>
                            <li><span class="nmb">3.9.</span> If the Provider determines that the Affiliate’s advertising or linking activity can be identified as spam, The Provider reserves the right to suspend his Affiliate account. The Provider reserves the right to determine spam following internal procedures.</li>
                            <li><span class="nmb">3.10.</span> It is the Affiliate’s responsibility to ensure that his tracking code is working properly before sending traffic to the Provider’s servers. The Provider is not responsible for any modification made to Affiliate links. Affiliate commissions are not paid for tracking errors caused by editing, masking, redirecting or tampering with Affiliate links.</li>
                            <li><span class="nmb">3.11.</span> Default cookie tracking is set for 90 days.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-13-4"></div>

                        <h3 class="tos-top-sublevel-title">4. Search Engine Marketing Campaigns</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">4.1.</span> Affiliates are prohibited from bidding on Host1Plus branded and trademarked terms that are directly associated with Host1Plus brand, products and services. Affiliates are also prohibited from bidding on terms that are misspellings, variations or other keywords used to deceive users to believe that the searched keyword and ad copy is related to Host1Plus.</li>
                            <li><span class="nmb">4.2.</span> Affiliates are prohibited from using Host1Plus branded keywords or other keywords used to deceive users to believe that the ad copy is directly related to Host1Plus brand within the display URL of the ad copy.</li>
                            <li><span class="nmb">4.3.</span> Affiliates are also prohibited from utilizing any content that could fall into the following categories within their ad copy: adult content, content not yet rated, sensitive social issues, tragedy &amp; conflict, etc.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-13-5"></div>

                        <h3 class="tos-top-sublevel-title">5. Website Content</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">5.1.</span> Affiliates are prohibited from using any content on their website (social account, public post, etc.) that could deceive users to believe that the website (social account, public post, etc.) is directly related to the Provider.</li>
                            <li><span class="nmb">5.2.</span> Affiliates are not allowed to replicate or reproduce the look, feel and structure of the Provider’s website. Also, Affiliates are prohibited from using any content that could fall into the following categories: adult content, content not yet rated, sensitive social issues, tragedy & conflict, etc.</li>
                            <li><span class="nmb">5.3.</span> Affiliates are prohibited from using an identical or virtually identical Host1Plus trademark as a part of their domain, second level domain name and/or subdomain.</li>
                            <li><span class="nmb">5.4.</span> Affiliates are allowed to only use trademarked creatives that are provided by Host1Plus.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-13-6"></div>

                        <h3 class="tos-top-sublevel-title">6. Affiliate Commissions and Rates</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">6.1.</span> Commissions are approved only from new and unique referred clients, or clients who buy a new type of hosting service. Commissions from existing clients are approved only if they buy a new type of hosting service for the first time. In this case, commissions will be valid for all bought services during the 90-day cookie period.</li>
                            <li><span class="nmb">6.2.</span> The Affiliate receives a fixed amount of $65 if his referrals purchase services from the Provider for $40 or more.</li>
                            <li><span class="nmb">6.3.</span> The Affiliate receives 100% if his referrals purchase services from the Provider for less than $40.</li>
                            <li><span class="nmb">6.4.</span> The Affiliate receives extra $25 for each of his tier 1 referrals who purchase services from the Provider for $40 or more.</li>
                            <li><span class="nmb">6.5.</span> The Affiliate receives extra 50$ USD for each of his tier 2 referrals who purchase services from the Provider for $40 or more.</li>
                            <li><span class="nmb">6.6.</span> The Affiliate receives extra 25% for each of his tier 1 referrals who purchase services from the Provider for less than $40.</li>
                            <li><span class="nmb">6.7.</span> The Affiliate receives extra 50% for each of his tier 2 referrals who purchase services from the Provider for less than $40.</li>
                            <li><span class="nmb">6.8.</span> All confirmed Affiliate income is kept under pending supervision up to 45 days until the Provider confirms the referrals and assigns the income to Affiliate earned funds.</li>
                            <li><span class="nmb">6.9.</span> In order for an Affiliate to qualify for Affiliate earned funds, his referral must have an active service for at least 45 days.</li>
                            <li><span class="nmb">6.10.</span> Affiliate accounts must be active at the time of the referral sale. The Affiliate is not eligible for commissions from referrals when his Affiliate account was suspended or before it was approved.</li>
                            <li><span class="nmb">6.11.</span> If the Affiliate buys a service through his own Affiliate link - the commissions for that sale will not be approved and such actions may lead to account suspension.</li>
                            <li><span class="nmb">6.12.</span> Missing or untracked Affiliate referrals must be reported during 45-day period after the purchase. Each case is reviewed individually. The Provider reserves the right to the final decision whereas the sale in question should be credited to the Affiliate.</li>
                            <li><span class="nmb">6.13.</span> The commissions are paid once a month.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-13-7"></div>

                        <h3 class="tos-top-sublevel-title">7. Affiliate Payments</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">7.1.</span> Affiliate system is limited to primary currency (USD).</li>
                            <li><span class="nmb">7.2.</span> Affiliate payments are issued via PayPal.</li>
                            <li><span class="nmb">7.3.</span> A minimum amount of $50 affiliate earned funds must be collected before commissions are paid. Earned funds are paid once a month.</li>
                            <li><span class="nmb">7.4.</span> Affiliates are responsible for ensuring that the payment information is accurate and up to date. The Provider is not responsible for any lost payments due to inaccurate Affiliate information.</li>
                            <li><span class="nmb">7.5.</span> If the Affiliate‘s PayPal account details are incorrect or PayPal refuses to accept the payment, the commission payment will be reissued within 60 days. If the Provider does not receive the correct Affiliate payment information within 120 days of commission generation, earned Affiliate commissions will be declined.</li>
                            <li><span class="nmb">7.6.</span> Affiliates are responsible for any taxes, fees, exchange rates or other expenses in regard of approved Affiliate commissions.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-13-8"></div>

                        <h3 class="tos-top-sublevel-title">8. Affiliate Account Cancellation</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">8.1.</span> The User has the right to cancel his affiliate account, while keeping his active service account. To do so, a written request must be submitted by submitting a ticket.</li>
                            <li><span class="nmb">8.2.</span> Affiliate accounts are tied together with a billing account. In cases when affiliate accounts or billing accounts are suspended due to breach of Terms of Service, the Provider reserves the right to close either or both accounts.</li>
                        </ul>
                    </div>

                    <div class="scroll-id-target" id="tos-section-14"></div>

                    <h2 class="tos-top-level-title">XIV. Obligations for Service Provision</h2>

                    <div class="tos-desc">

                        <div class="scroll-id-target" id="tos-section-14-1"></div>

                        <h3 class="tos-top-sublevel-title">1. Obligations for Domain Registration and Domain Transfer</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">1.1.</span> The Provider does not guarantee the successful registration of a domain. The domain may be secretly reserved in the registry and deceptively appear to be free for registration.</li>
                            <li><span class="nmb">1.2.</span> It is the User’s responsibility to provide the correct information to sustain the requirements for each domain registry. The Provider is not responsible if the domain is rejected due to non-compliance with the terms of a specific domain registry.</li>
                            <li><span class="nmb">1.3.</span> The Provider does not guarantee the successful transfer of a domain. It is the User’s responsibility to make sure the domain is transferable before placing a transfer order.</li>
                            <li><span class="nmb">1.4.</span> To complete the domain transfer successfully, the User must have access to the domain's management page to remove locks and change privacy settings to prepare the domain for the transfer procedure. The User also must be in control of the email address associated with the domain to be able to confirm the transfer by receiving an automated confirmation email.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-14-2"></div>

                        <h3 class="tos-top-sublevel-title">2. Obligations for Shared and Reseller Hosting</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">2.1.</span> Shared and Reseller hosting services are to be used for standard web hosting use only. As so, usage for (but not limited to) file sharing, torrents, streaming, proxies is not allowed.</li>
                            <li><span class="nmb">2.2.</span> Shared hosting services may not be used to resell web hosting.</li>
                            <li><span class="nmb">2.3.</span> The Provider reserves the right to determine whether the service usage is valid.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-14-3"></div>

                        <h3 class="tos-top-sublevel-title">3. Obligations for VPS</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">3.1.</span> VPS hosting services are under Shared hosting resource restriction. VPS hosting service usage might be restricted based by the demand of neighboring users of the same service.</li>
                            <li><span class="nmb">3.2.</span> VPS CPU threshold is calculated if over usage is detected for a prolonged period of time. If this occurs, it will be limited until the appropriate usage is restored. The User is free to upgrade his plans to benefit from higher CPU speeds if necessary.</li>
                            <li><span class="nmb">3.3.</span> VPS Bandwidth is calculated for every month and is reset every month regardless of the billing cycle. If the bandwidth limit is reached, the network speed is set to a bare minimum. In such cases the User is asked to upgrade his system.</li>
                        </ul>

                        <div class="scroll-id-target" id="tos-section-14-4"></div>

                        <h3 class="tos-top-sublevel-title">4. Obligations for Cloud Servers</h3>

                        <ul class="terms-list">
                            <li><span class="nmb">4.1.</span> Cloud Servers hosting services are not bound by any resource usage restrictions except for Bandwidth.</li>
                            <li><span class="nmb">4.2.</span> Cloud Servers User can upgrade his resources if there is higher demand. After executing Upgrades the User will be charged with an invoice for the upgrades and after paying the invoice upgraded resources will be instantly added.</li>
                            <li><span class="nmb">4.3.</span> Cloud Servers hosting service User is free to downgrade his resources if there is no need for them. Note that the User cannot downgrade IPv4 addresses and disk space.</li>
                        </ul>

                    </div>

                    <div class="scroll-id-target" id="tos-section-15"></div>

                    <h2 class="tos-top-level-title">XV. Compliance with Local Organizations</h2>

                    <div class="tos-desc">

                        <ul class="terms">
                            <li>As a member of the ISPA the Provider upholds and abides by the Code of Conduct described here: <a target="_blank" href="http://ispa.org.za/code-of-conduct">http://ispa.org.za/code-of-conduct</a>.</li>
                        </ul>

                    </div>

                </div> <!-- end of .tos-content -->

                <div class="go-top">
                    <a href="#tos-top" data-scrollto="tos-top"><i class="fa fa-angle-up"></i></a>
                </div>

            </div> <!-- end of .tos-block -->


        </div> <!-- end of .container -->

    </section>   <!-- end of .page-content -->

</div> <!-- end of .page.tos -->

<script>
    var windowSize = function(){
        return $(window).height();
    }

    $(document).ready(function(){
        windowSize();
    });
    $(window).resize(function(){
        windowSize();
    });

    $(window).bind('scroll', function(){
        if($(this).scrollTop() > windowSize()) {
            $(".go-top").fadeIn('fast');
        } else {
            $(".go-top").fadeOut('slow');
        }
    });
</script>

<?php
get_footer();
?>
