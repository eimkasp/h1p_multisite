<?php

global $whmcs;
global $whmcs_promo;
global $config;

$plansConfig = $whmcs->get_local_stepsconfig('cloud_plans');

$base = $plansConfig['base'];
$plans = $plansConfig['plans'];

?>

<div class="plans-pricing-table cloud">

    <?php

    $planIndex = 0;

    foreach($plans as $plan_key => $plan):

        $planPrices = $whmcs->getConfigurablePrice($plan['id'], $plan['params']);
        $selectedCycle = 3;
        $selectedPrice = $whmcs->getPrice($plan['id'], $selectedCycle, 'price_monthly', $planPrices);

        ?>

        <div class="plan-col plan-col-<?php echo $planIndex+1;?>">

            <div class="plan">

                <form class="plan-block<?php if($planIndex == 1):?> active<?php endif;?>" action="<?php echo $config['whmcs_links']['checkout_cloud']?>" method="get">

                    <?php // Remove disabled form select name="billingcycle", cloud-checkout-link and order now buttons as well. ?>
                    <input disabled type="hidden" name="plan" value="<?php echo $plan_key?>"/>
                    <input disabled type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                    <input disabled type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                    <input disabled type="hidden" name="promocode" value="<?php echo $whmcs_promo;?>"/>

                    <span class="plan-title"><?php echo $plan['name']?></span>

                    <span class="price"><span data-select-value="selectedprice-cloud-<?php echo $plan_key?>"><?php echo $selectedPrice;?></span><span class="sub full"><?php _e('per month')?></span><span class="sub sort"><?php _e('/mo')?></span></span>

                    <div class="drop-link-table">
                        <div class="cell-drop">
                            <select disabled class="default-dropdown" name="billingcycle" data-select="selectedprice-cloud-<?php echo $plan_key?>">
                                <?php foreach($planPrices as $index => $option):?>
                                    <option data-value="<?php echo $option['price_monthly']?>" value="<?php echo $option['cycle']?>"<?php if($option['months'] == $selectedCycle):?> selected="selected"<?php endif;?>><?php echo $option['months']?> <?php _e('months')?> <?php echo $option['price_monthly']?><?php _e('/month')?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="cell-link">
                            <button id="cloud-checkout-link" type="submit" class="button disabled" disabled><?php _e('Order Now')?></button>
                        </div>
                    </div>

                    <div class="features">
                        <div class="feature-col">
                            <div class="feature first left">
                                <span class="cell left simple"><?php _e('CPU')?></span>
                                <span class="cell right bold"><?php echo ($plan['params']['cpu']['value']*0.6) + $base['cpu']['value'] ?> <?php _e('Ghz')?></span>
                            </div>
                        </div>
                        <div class="feature-col">
                            <div class="feature first right">
                                <span class="cell left"><?php _e('RAM')?></span>
                                <span class="cell right bold"><?php echo $plan['params']['ram']['value'] + $base['ram']['value'] ?> <?php _e('MB')?></span>
                            </div>
                        </div>
                        <div class="feature-col">
                            <div class="feature left">
                                <span class="cell left"><?php _e('HDD')?></span>
                                <span class="cell right bold"><?php echo $plan['params']['hdd']['value'] + $base['hdd']['value'] ?> <?php _e('GB')?></span>
                            </div>
                        </div>
                        <div class="feature-col">
                            <div class="feature right">
                                <span class="cell left"><?php _e('Network Speed')?></span>
                                <span class="cell right bold"><?php echo $plan['params']['port_speed']['value'] + $base['port_speed']['value'] ?> <?php _e('Mbps')?></span>
                            </div>
                        </div>
                        <div class="feature-col">
                            <div class="feature last left">
                                <span class="cell left"><?php _e('IPv4 Address')?></span>
                                <span class="cell right bold"><?php echo $plan['params']['ips']['value'] + $base['ips']['value'] ?> <?php _e('IP')?></span>
                            </div>
                        </div>
                    </div>

                </form>
            </div>

        </div>

        <?php

        $planIndex++;

    endforeach;

    ?>

    <div class="plan-col plan-col-4">
        <div class="plan">
            <div class="plan-block">
                <span class="plan-title"><?php _e('Build your own!')?></span>
                <span class="plan-descr"><?php _e('Select your individual preferences.')?></span>
                <div class="drop-link-table">
                    <div class="cell-link">
                        <a id="cloud-checkout-link" href="<?php //echo $config['whmcs_links']['checkout_cloud']?>" class="button disabled" disabled onclick="return false;"><?php _e('Get Started')?></a>
                    </div>
                </div>
                <div class="features">
                    <div class="feature-col">
                        <div class="feature first left">
                            <span class="cell left simple"><?php _e('CPU')?></span>
                            <span class="cell right bold"><?php _e('Config')?></span>
                        </div>
                    </div>
                    <div class="feature-col">
                        <div class="feature first right">
                            <span class="cell left"><?php _e('RAM')?></span>
                            <span class="cell right bold"><?php _e('Config')?></span>
                        </div>
                    </div>
                    <div class="feature-col">
                        <div class="feature left">
                            <span class="cell left"><?php _e('HDD+')?></span>
                            <span class="cell right bold"><?php _e('Config')?></span>
                        </div>
                    </div>
                    <div class="feature-col">
                        <div class="feature right">
                            <span class="cell left"><?php _e('Bandwidth')?></span>
                            <span class="cell right bold"><?php _e('Config')?></span>
                        </div>
                    </div>
                    <div class="feature-col">
                        <div class="feature last left">
                            <span class="cell left"><?php _e('IPv4 Address')?></span>
                            <span class="cell right bold"><?php _e('Config')?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
