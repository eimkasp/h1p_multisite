<?php
global $config;
global $whmcs_promo;
global $whmcs;
?>
<div class="plans-pricing-table seo seo-vps">


    <?php

    foreach($config['products']['seo_vps_hosting']['plans'] as $plan_key => $plan):

        $planPrices = $whmcs->getPriceOptions($plan['id'], null, false);
        $selectedCycle = 3;
        $selectedPrice = $whmcs->getPrice($plan['id'], $selectedCycle, 'price_monthly');

        ?>

        <div class="plan-col plan-col-<?php echo $plan_key;?>">
            <div class="plan">

                <form action="<?php echo $config['whmcs_links']['checkout']?>" method="GET" class="plan-block<?php if($plan_key == 1):?> active<?php endif;?>">

                    <input type="hidden" name="a" value="add"/>
                    <input type="hidden" name="pid" value="<?php echo $plan['id'];?>"/>
                    <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                    <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                    <input type="hidden" name="promocode" value="<?php echo $whmcs_promo;?>"/>

                    <?php if((float)$planPrices[0]['setup_value'] == 0):?><span class="setup-label"><?php _e('FREE SETUP')?></span><?php endif;?>

                    <span class="plan-title"><?php echo $plan['title'];?></span>

                    <div class="price-row">
                        <span class="main-price">
                            <span class="full" data-select-value="selectedprice-<?php echo $plan['id']?>"><?php echo $selectedPrice;?></span>
                            <span class="sub"><?php _e('/mo')?></span>
                        </span>
                        <span class="setup-price<?php if((float)$planPrices[0]['setup_value'] == 0):?> free<?php endif;?>">
                            <span class="full">+ <?php echo $planPrices[0]['setup'];?></span>
                            <span class="sub"><?php _e('/setup')?></span>
                        </span>
                    </div>

                    <div class="drop-button">
                        <div class="cell drop">
                            <select name="billingcycle" class="default-dropdown" data-select="selectedprice-<?php echo $plan['id']?>">
                                <?php foreach($planPrices as $index => $option):?>
                                    <option
                                        data-value="<?php echo $option['price_monthly']?>"
                                        value="<?php echo $option['cycle']?>"<?php if($option['months'] == $selectedCycle):?>
                                        selected="selected"<?php endif;?>>
                                            <?php
                                            switch(get_numeric_case($option['months'])){
                                                case 0:
                                                    echo $option['months'] . ' ' .  _x_lc('months', '0 months') .  ' ' . $option['price_monthly'] . ' ' . __('/ month');
                                                    break;
                                                case 1:
                                                    echo $option['months'] . ' ' .  _x_lc('month', '1 month') .  ' ' . $option['price_monthly'] . ' ' . __('/ month');
                                                    break;
                                                case 2:
                                                    echo $option['months'] . ' ' .  _x_lc('months', '2 months') .  ' ' . $option['price_monthly'] . ' ' . __('/ month');
                                                    break;
                                            }
                                            ?>
                                    </option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="cell order-now">
                            <button id="vps-seo-checkout-link" type="submit" class="button primary"><?php _e('Order Now')?></button>
                        </div>
                    </div>

                    <div class="features">

                        <div class="flags-block">
                            <?php foreach($plan['params']['locations'] as $location):?>
                                <span class="loc">
                                    <i class="icon seo-hosting-location <?php echo $location['code']?>"></i>
                                    <span class="location-title"><?php echo $location['title']?></span>
                                </span>
                            <?php endforeach;?>
                        </div>

                        <div class="feature">
                            <span class="cell left"><?php _e('IP Classes')?></span>
                            <span class="cell right bold">
                            <?php
                            switch(get_numeric_case($plan['params']['ip_classes'])){
                                case 0:
                                    echo sprintf(_x_lc('%s different C classes', '0 class'), $plan['params']['ip_classes']);
                                    break;
                                case 1:
                                    echo sprintf(_x_lc('%s different C classes', '1 class'), $plan['params']['ip_classes']);
                                    break;
                                case 2:
                                    echo sprintf(_x_lc('%s different C classes', '2 class'), $plan['params']['ip_classes']);
                                    break;
                            }
                            ?>
                            </span>
                        </div>
                        <div class="feature">
                            <span class="cell left"><?php _e('Unique IPs')?></span>
                            <span class="cell right bold"><?php echo $plan['params']['unique_ips'];?></span>
                        </div>
                        <div class="feature">
                            <span class="cell left"><?php _e('Bandwidth')?></span>
                            <span class="cell right bold"><?php echo $plan['params']['bandwidth'];?></span>
                        </div>
                        <div class="feature">
                            <span class="cell left"><?php _e('RAM')?></span>
                            <span class="cell right bold"><?php echo $plan['params']['ram'];?></span>
                        </div>
                        <div class="feature">
                            <span class="cell left"><?php _e('Disk Space')?></span>
                            <span class="cell right bold"><?php echo $plan['params']['disk_space'];?></span>
                        </div>
                        <div class="feature">
                            <span class="cell left"><?php _e('CPU')?></span>
                            <span class="cell right bold"><?php echo $plan['params']['cpu'];?></span>
                        </div>
                        <div class="feature">
                            <span class="cell left"><?php _e('Domains')?></span>
                            <span class="cell right bold"><?php echo $plan['params']['domains'] > -1 ? $plan['params']['domains'] : _x_lc('Unlimited', 'plural musculine');?></span>
                        </div>

                    </div>

                </form>

            </div>
        </div>


    <?php endforeach;?>

</div>
