<?php

global $whmcs;

/*Get VPS backup config option pricing
--------------------------------------*/
// id=347 - VPS Frankfurt

$vps_hosting_config_options = $whmcs->getConfigurablePrices(347);
$backup_1_value = (float) $vps_hosting_config_options['Backups']['options']['2']['pricing']['monthly'] / 2; // two backups
$backup_1_formatted = number_format ( $backup_1_value, 2);
/*echo '<pre>';
var_dump ($vps_hosting_config_options);
echo '</pre>';
exit;*/

/*
        <section class="service-pricing extra-pad-top" data-section-title="<?php _e('Plans');?>">
            <div class="container">
*/
?>            

                <span class="title-follow-text center">
                    <?php echo sprintf(_x_lc('Save %1$s up to 20%% %2$s for longer billing cycles!'), '<span class="highlight has-tooltip" data-tooltip="billing-cycle-savings">', '</span>'); ?>
                    <div class="triangle"></div>
                    <div class="tooltip-body style2 w-600" data-tooltip-content="billing-cycle-savings"><p><?php _e('Save up to 20% with longer billing cycles ranging from 3 to 24 months. The discount increases according to the length of the billing cycle.'); ?></p></div>
                </span>

                <div class="block-title-strikethrough">
                    <h3 class="block-title"><?php _e('All unmanaged plans include')?></h3>
                </div>

                <div class="container-flex">
                    <div class="block-fluid">
                    <ul class="features-list checklist orange">
                        <li><?php _e('Intel Xeon processors')?></li>
                        <li><?php _e('SSD caching')?></li>
                        <li><?php _e('500 Mbps uplink')?></li>
                    </ul>
                    </div>
                    <div class="block-fluid">
                        <ul class="features-list checklist orange">
                            <li><?php _e('IPv4 and IPv6 support')?></li>
                            <li><?php _e('Backups')?> <i class="fa fa-info-circle tooltip-icon" data-tooltip="manage-backup"></i>
                                <div class="tooltip-body style2 w-300" data-tooltip-content="manage-backup">
                                    <div class="triangle"></div>
                                    <p><?php printf(__('You can manage backups manually at your Client Area for %s%s each.'), $whmcs::$settings['currency_prefix'], $backup_1_formatted); ?></p>
                                </div>
                            </li>
                            <li><?php _e('OpenVZ virtualization technology')?></li>
                        </ul>
                    </div>
                    <div class="block-fluid">
                        <ul class="features-list checklist orange">
                            <li><?php _e('Linux operating system')?></li>
                            <li><?php _e('Up to 32 IPv4 addresses')?></li>
                            <li><?php _e('RAID data storage')?></li>
                        </ul>
                    </div>
                </div>

                <div class="block-title-strikethrough-close"></div>

<?php         
/*
            </div> <!-- end of .container -->
        </section> <!-- end of .service-pricing -->
*/
?>                     