<?php
global $config;
global $whmcs_promo;
global $whmcs;
?>
<div class="plans-pricing-table dedicated">

    <div class="head row">
        <div class="cell"><?php _e('CPU')?></div>
        <div class="cell"><?php _e('RAM')?></div>
        <div class="cell"><?php _e('HDD')?></div>
        <div class="cell"><?php _e('Port Speed')?></div>
        <div class="cell"><?php _e('Price From')?></div>
        <div class="cell"></div>
    </div>

    <?php foreach($config['products']['dedicated_servers']['plans'] as $key => $plan):?>

        <?php $price = $whmcs->getPriceOptions($plan['id'], 'quarterly'); ?>

        <form action="<?php echo $config['whmcs_links']['checkout']?>" method="GET" class="row row-<?php echo $key;?> info<?php if(isset($plan['most-popular']) && $plan['most-popular']):?> most-pop<?php endif;?>">

            <input type="hidden" name="a" value="add"/>
            <input type="hidden" name="pid" value="<?php echo $plan['id']?>"/>
            <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
            <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
            <input type="hidden" name="billingcycle" value="quarterly"/>
            <input type="hidden" name="promocode" value="<?php echo $whmcs_promo;?>"/>

            <div class="cell title-cell">
                <span class="core"><?php echo $plan['title']?></span>
                <span class="cpu"><?php echo $plan['params']['CPU']?></span>
                <?php if(isset($plan['most-popular']) && $plan['most-popular']):?>
                    <span class="info-box orange"><strong><?php _e('MOST POPULAR')?></strong></span>
                <?php endif;?>
            </div>
            <div class="cell ram-cell"><span><?php echo $plan['params']['RAM']?></span></div>
            <div class="cell hdd-cell">
                <?php foreach($plan['params']['STORAGE'] as $key => $storage):?>
                <span><?php echo $storage ?></span>
                <?php endforeach;?>
            </div>
            <div class="cell bandwidth-cell"><span><?php echo $plan['params']['BANDWIDTH']?></span></div>
            <div class="cell image-cell">
                <span class="icon cpu <?php echo strtolower($plan['title'])?>"></span>
            </div>
            <div class="cell main-price-cell">
                <span class="price-table">
                    <span class="price-cell from"><?php _e('From')?></span>
                    <span class="price-cell">
                        <?php if($plan['id'] == '313'): ?>
                            <div class="price">
                                <span class="price-text new"><?php echo $price['price_monthly']?></span>
                                <span><?php _e('/ month')?></span>
                            </div>
                            <div class="price">
                                <span class="price-text old"><?php echo $whmcs->formatPrice(($price['price_value']/3*0.2)+($price['price_value']/3))?>
                                <span><?php _e('/ month')?></span>
                            </div>
                        <?php else: ?>
                            <div class="price">
                                <span class="price-text"><?php echo $price['price_monthly']?></span>
                                <span><?php _e('/ month')?></span>
                            </div>
                        <?php endif; ?>
                    </span>
                </span>
            </div>
            <div class="cell order-btn-cell"><button id="dedicated-checkout-link" class="button primary" type="submit"><?php _e('Configure & Order')?></button></div>

        </form>

    <?php endforeach;?>

</div>

<div class="build-server">
    <div class="content-col">
        <div class="title"><?php _e('Build an individual plan!')?></div>
        <div class="descr"><?php _e('We offer the oportunity to adjust and customize dedicated server hosting plans to fit your business.')?></div>
        <div class="bottom-block clear">
            <div class="icon cpu all"></div>
        </div>
    </div>
    <div class="bg-col"></div>
</div>
