<?php

global $whmcs;
global $whmcs_promo;
global $config;



/*Uses $mypage data from PageData*/



/*  NEW function to WHCMS:
$whmcs->getAddonsPrices($addonid = null);
this is needed for dedicated IP price (addon price) retrieval from WHMCS

*/

/* Default location is 
    <input type="hidden" name="configoption[301]" value="3999">

    Los angeles - 2058
*/


$destination = isset($params['destination']) ? $params['destination'] : 'default';

$best = isset($config['products']['web_hosting']['best_value']) ? $config['products']['web_hosting']['best_value'] : null;
$good_for_seo = isset($config['products']['web_hosting']['good_for_seo']) ? $config['products']['web_hosting']['good_for_seo'] : null;

$total_plans = sizeof($config['products']['web_hosting']['plans']);

$tooltip_info = 'Websites may use unlimited resources in compliance with our <a target="_blank" href="http://www.host1plus.com/terms-of-service/">Unlimited Resources Policy</a>. If resource usage puts overall server security and performance at risk, temporary resource restrictions may occur until the source of extensive resource usage is removed or data is transferred to a virtual private server.';


// Popular plan setting
$most_popular_marking = "Business";
$most_popular_description = __('Most popular');


?>

<div class="plans-pricing-table web totalcol-3 horizontal">

    <?php

    // Since addons price does not depend upon the plan, we get it first
    // Check this ID !!!
    $addons_dedicated_ip = $whmcs->getAddonsPrices(25);


    foreach($config['products']['web_hosting']['plans'] as $plan_key => $plan):

        $planPrices = $whmcs->getPriceOptions($plan['id']);
        $selectedCycle = ($plan_key == 1) ? 12 : ($destination == 'domain-search') ? 12 : 12;
        $selectedPrice = $whmcs->getPrice($plan['id'], $selectedCycle, 'price_monthly');

        if($destination == 'domain-search' && $plan_key < 3) continue;


        // Checking if current plan is the Popular

        if ( $plan['title'] == $most_popular_marking ) {
            $is_popular_class = " popular";
            $is_popular_descr = "<span>" . $most_popular_description . "</span>";
        } else {
            $is_popular_class = "";
            $is_popular_descr = "";
        }


        ?>

        <div class="plan-col plan-col-<?php echo $i+1?>">
            <div class="plan<?php echo $is_popular_class ?>">
                <form class="pricing-table-js" action="<?php echo $config['whmcs_links']['checkout_web']?>" method="get">

                    <input type="hidden" name="a" value="add"/>
                    <input type="hidden" name="pid" value="<?php echo $plan['id']?>"/>
                    <input type="hidden" name="configoption[301]" value="2058">
                    <input type="hidden" name="billingcycle" value="annually">
                    <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                    <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                    <input type="hidden" name="promocode" value="<?php echo $mypage->promo; ?>"/>

                    <div class="plan-header-wrapper" data-submit-form>
                        <div class="cell plan-name"><?php echo $plan['title']?><?php echo $is_popular_descr ?></div>
                        <div class="cell price-wrapper">
                            <div class="price">
                                <?php

                                    // No discount
                                    
                                    $prices = $planPrices;

                                    $system_price = $prices[0]['price_monthly_value'];
                                    // next time use PageData functions to get $price_with_promo

                                    $main_price = $system_price;

                                ?>
                                <span class="from above-block-center"><?php _e('from')?></span>

                                <?php if ( isset($price_with_promo) ) { ?>
                                    <span class="main-price old-price strikethrough"><span class="prefix old-price"><?php echo $whmcs::$settings['currency_prefix'] ?></span><?php echo $system_price; ?></span><span class="mo p-r-10"><?php _e('/mo')?></span>
                                <?php } ?>

                                <span class="main-price"><span class="prefix"><?php echo $whmcs::$settings['currency_prefix'] ?></span><?php echo $main_price; ?></span><span class="mo"><?php _e('/mo')?></span>

                            </div>
                        </div>
                    </div>
                    <div class="plan-body-wrapper">

                        <div class="cell feature">
                            <div class="name"><?php _e('Disk space')?></div>
                            <div class="value has-tooltip" data-tooltip="unlimited-description-<?php echo strtolower($plan['title']);?>"><?php echo $plan['params']['disk_space'] > -1 ? $plan['params']['disk_space'] : _x_lc('Unlimited', 'plural feminine') ?>
                                <div class="has-tooltip-content" data-tooltip-content="unlimited-description-<?php echo strtolower($plan['title']);?>"><?php echo $tooltip_info ?></div>
                            </div>
                        </div>
                        <div class="cell feature">
                            <div class="name"><?php _e('Bandwidth')?></div>
                            <div class="value has-tooltip" data-tooltip="unlimited-description-<?php echo strtolower($plan['title']);?>"><?php echo $plan['params']['bandwidth'] > -1 ? $plan['params']['bandwidth'] : _x_lc('Unlimited', 'singular masculine');?> </div>
                        </div>
                        <div class="cell feature">
                            <div class="name"><?php _e('Add-on domains')?></div>
                            <div class="value"><?php echo $plan['params']['domains'] > -1 ? $plan['params']['domains'] :  _x_lc('Unlimited', 'plural masculine');?></div>
                        </div>
                        <div class="cell feature">
                            <div class="name"><?php _e('Sub-domains')?></div>
                            <div class="value"><?php echo $plan['params']['sub_domains'] > -1 ? $plan['params']['sub_domains'] : _x_lc('Unlimited', 'plural masculine');?></div>
                        </div>
                        <div class="cell feature">
                            <div class="name"><?php _e('Databases')?></div>
                            <div class="value"><?php echo $plan['params']['databases'] > -1 ? $plan['params']['databases'] : _x_lc('Unlimited', 'plural feminine');?></div>
                        </div>
                        <div class="cell feature">
                            <div class="name"><?php _e('Dedicated IP')?></div>
                            <div class="value">
                                <?php if ( $plan['params']['free_ip'] == 1 ) {
                                    _e('FREE');
                                } else {
                                    echo $whmcs::$settings['currency_prefix'];
                                    echo $addons_dedicated_ip['price'];
                                    _e('/mo');
                                }
                                ?>
                            </div>
                        </div>
                        <div class="cell feature">
                            <div class="name has-tooltip highlight" data-tooltip="freedomain-description"><?php _e('Free domain')?></div>
                            <div class="value"><?php echo $plan['params']['free_domain'];?>
                                <div class="has-tooltip-content" data-tooltip-content="freedomain-description"><?php _e('Free domain registration is applicable for the first year with annual and biannual billing cycles. <br>Available TLDs: .com, .net, .org, .biz, .info, .lt.') ?></div>
                            </div>
                        </div>
                        <div class="cell order">
                            <button type="submit" class="web-hosting-checkout-link button primary"><?php _e('Select')?></button>
                        </div>

                    </div>

                </form>
            </div>
        </div>


    <?php endforeach; ?>


</div>

<script>
    (function($){

        $(document).ready(function() {
            $('[data-submit-form]').click(function(){
                $(".plan").removeClass('selected');
                $(this).closest(".plan").addClass('selected');
                $(this).closest("form").submit();
            });
        });

    })(jQuery)
</script>
