<?php

global $whmcsPrices;
global $whmcs_promo;
global $config;


$sslPrices = array(

    'ComodoSSL' => $whmcsPrices[$config['products']['certificates']['comodo']['comodo']['id']],
    'PositiveSSL' => $whmcsPrices[$config['products']['certificates']['comodo']['positive']['id']]

);

?>

<div class="plans-pricing-table comodo">

    <div class="plan-col plan-col-1">

        <div class="plan">

            <div class="plan-block">

                <h3 class="plan-title"><?php _e('Comodo SSL')?></h3>
                <span class="plan-descr"><?php _e('Secure your website with Comodo SSL – provide the security your customers expect with a set of powerful tools by Comodo.')?></span>
                <span class="price"><span><?php echo $sslPrices['ComodoSSL']['price']?></span><span class="sub full"> <?php _e('/ year')?></span><span class="sub sort"><?php _e('/year')?></span></span>
                <a href="<?php echo $config['whmcs_links']['checkout']?>?a=add&pid=<?php echo $config['products']['certificates']['comodo']['comodo']['id']?>&language=<?php echo $config['whmcs_lang']; ?>&currency=<?php echo $config['whmcs_curr']; ?>&promocode=<?php echo $whmcs_promo;?>" class="order-btn button primary"><?php _e('Order Now')?></a>

                <div class="features">
                    <div class="feature-col">
                        <div class="feature first">
                            <i class="fa fa-check"></i> <?php _e('Extra-fast online validation')?>
                        </div>
                    </div>
                    <div class="feature-col">
                        <div class="feature">
                            <i class="fa fa-check"></i> <?php _e('Standard 2048-bit digital signature')?>
                        </div>
                    </div>
                    <div class="feature-col">
                        <div class="feature">
                            <i class="fa fa-check"></i> <?php _e('$250K relying party warranty')?>
                        </div>
                    </div>
                    <div class="feature-col last">
                        <div class="feature">
                            <i class="fa fa-check"></i> <?php _e('99.9&#37; browser compatibility')?>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="plan-col plan-col-2">

        <div class="plan">

            <div class="plan-block">

                <h3 class="plan-title"><?php _e('Positive SSL')?></h3>
                <span class="plan-descr"><?php _e('Conserve your data and ensure the highest level of protection with one of the most affordable SSL certificates on the market.')?></span>
                <span class="price"><span><?php echo $sslPrices['PositiveSSL']['price']?></span><span class="sub full"><?php _e('/ year')?></span><span class="sub sort"><?php _e('/year')?></span></span>
                <a href="<?php echo $config['whmcs_links']['checkout']?>?a=add&pid=<?php echo $config['products']['certificates']['comodo']['positive']['id']?>&language=<?php echo $config['whmcs_lang']; ?>&currency=<?php echo $config['whmcs_curr']; ?>&promocode=<?php echo $whmcs_promo;?>" class="order-btn button primary"><?php _e('Order Now')?></a>

                <div class="features">
                    <div class="feature-col">
                        <div class="feature first">
                            <i class="fa fa-check"></i> <?php _e('Extra-fast online validation')?>
                        </div>
                    </div>
                    <div class="feature-col">
                        <div class="feature">
                            <i class="fa fa-check"></i> <?php _e('Standard 2048-bit digital signature')?>
                        </div>
                    </div>
                    <div class="feature-col">
                        <div class="feature">
                            <i class="fa fa-check"></i> <?php _e('$10K relying party warranty')?>
                        </div>
                    </div>
                    <div class="feature-col last">
                        <div class="feature">
                            <i class="fa fa-check"></i> <?php _e('99.9&#37; browser compatibility')?>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

</div>
