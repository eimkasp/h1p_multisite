<?php

global $whmcsPrices;
global $whmcs_promo;
global $config;

$sslPrices = array(

    'RapidSSL' => $whmcsPrices[$config['products']['certificates']['geotrust']['rapid']['id']],
    'QuickSSLPremium' => $whmcsPrices[$config['products']['certificates']['geotrust']['quick']['id']],
    'TrueBizID' => $whmcsPrices[$config['products']['certificates']['geotrust']['truebusiness']['id']]

);

?>

<div class="plans-pricing-table geotrust">

    <div class="plan-col plan-col-1">

        <div class="plan">

            <div class="plan-block">

                <h3 class="plan-title"><?php _e('RapidSSL')?></h3>
                <span class="plan-descr"><?php _e('Add value to your website with RapidSSL – please your visitors with a safe online environment and benefit from extra features.')?></span>
                <span class="price"><span><?php echo $sslPrices['RapidSSL']['price']?></span><span class="sub full"><?php _e('/ year')?></span><span class="sub sort"><?php _e('/year')?></span></span>
                <a href="<?php echo $config['whmcs_links']['checkout']?>?a=add&pid=<?php echo $config['products']['certificates']['geotrust']['rapid']['id']?>&language=<?php echo $config['whmcs_lang']; ?>&currency=<?php echo $config['whmcs_curr']; ?>&promocode=<?php echo $whmcs_promo;?>" class="order-btn button primary"><?php _e('Order Now')?></a>

                <div class="features">
                    <div class="feature-col">
                        <div class="feature first">
                            <i class="fa fa-check"></i> <?php _e('Extra-fast online validation')?>
                        </div>
                    </div>
                    <div class="feature-col">
                        <div class="feature">
                            <i class="fa fa-check"></i> <?php _e('Standard 2048-bit digital signature')?>
                        </div>
                    </div>
                    <div class="feature-col">
                        <div class="feature">
                            <i class="fa fa-check"></i> <?php _e('$10K relying party warranty')?>
                        </div>
                    </div>
                    <div class="feature-col last">
                        <div class="feature">
                            <i class="fa fa-check"></i> <?php _e('99.9&#37; browser compatibility')?>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="plan-col plan-col-2">

        <div class="plan">

            <div class="plan-block">

                <h3 class="plan-title"><?php _e('QuickSSL Premium')?></h3>
                <span class="plan-descr"><?php _e('QuickSSL Premium certificate is the quickest way to secure your website – give a significant boost to your level of security for a reasonable cost.')?></span>
                <span class="price"><span><?php echo $sslPrices['QuickSSLPremium']['price']?></span><span class="sub full"><?php _e('/ year')?></span><span class="sub sort"><?php _e('/year')?></span></span>
                <a href="<?php echo $config['whmcs_links']['checkout']?>?a=add&pid=<?php echo $config['products']['certificates']['geotrust']['quick']['id']?>&language=<?php echo $config['whmcs_lang']; ?>&currency=<?php echo $config['whmcs_curr']; ?>&promocode=<?php echo $whmcs_promo;?>" class="order-btn button primary"><?php _e('Order Now')?></a>

                <div class="features">
                    <div class="feature-col">
                        <div class="feature first">
                            <i class="fa fa-check"></i> <?php _e('Extra-fast online validation')?>
                        </div>
                    </div>
                    <div class="feature-col">
                        <div class="feature">
                            <i class="fa fa-check"></i> <?php _e('Standard 2048-bit digital signature')?>
                        </div>
                    </div>
                    <div class="feature-col">
                        <div class="feature">
                            <i class="fa fa-check"></i> <?php _e('$500K relying party warranty')?>
                        </div>
                    </div>
                    <div class="feature-col last">
                        <div class="feature">
                            <i class="fa fa-check"></i> <?php _e('99.9&#37; browser compatibility')?>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="plan-col plan-col-3">

        <div class="plan">

            <div class="plan-block">

                <h3 class="plan-title"><?php _e('True BusinessID')?></h3>
                <span class="plan-descr"><?php _e('Preserve your sensitive data with True Business ID – the best security solution for demanding online businesses and projects.')?></span>
                <span class="price"><span><?php echo $sslPrices['TrueBizID']['price']?></span><span class="sub full"><?php _e('/ year')?></span><span class="sub sort"><?php _e('/year')?></span></span>
                <a href="<?php echo $config['whmcs_links']['checkout']?>?a=add&pid=<?php echo $config['products']['certificates']['geotrust']['truebusiness']['id']?>&language=<?php echo $config['whmcs_lang']; ?>&currency=<?php echo $config['whmcs_curr']; ?>&promocode=<?php echo $whmcs_promo;?>" class="order-btn button primary"><?php _e('Order Now')?></a>

                <div class="features">
                    <div class="feature-col">
                        <div class="feature first">
                            <i class="fa fa-check"></i> <?php _e('Extra-fast online validation')?>
                        </div>
                    </div>
                    <div class="feature-col">
                        <div class="feature">
                            <i class="fa fa-check"></i> <?php _e('Standard 2048-bit digital signature')?>
                        </div>
                    </div>
                    <div class="feature-col">
                        <div class="feature">
                            <i class="fa fa-check"></i> <?php _e('$1.25M relying party warranty')?>
                        </div>
                    </div>
                    <div class="feature-col last">
                        <div class="feature">
                            <i class="fa fa-check"></i> <?php _e('99.9&#37; browser compatibility')?>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

</div>
