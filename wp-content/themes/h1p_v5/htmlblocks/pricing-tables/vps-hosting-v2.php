<?php
global $config;
global $whmcs_promo;
global $whmcs;

$plansConfig = $whmcs->get_local_stepsconfig('vps_plans');

/*echo '<pre>';
var_dump($plansConfig);
echo '</pre>';
*/


/* Old prices for presenting price differences */

$old_price = array (
    'en_US' => [
                /*'Amber' => '2.00',
                'Bronze' => '4.75',*/
                'Amber' => '',
                'Bronze' => '',
                'Silver' => '14.70',
                'Gold' => '29.30',
                'Platinum' => '64.50',
                'Diamond' => '105.00'
    ],
    'pt_BR' => [
                /*'Amber' => '6.74',
                'Bronze' => '16.00',*/
                'Amber' => '',
                'Bronze' => '',
                'Silver' => '47.22',
                'Gold' => '94.12',
                'Platinum' => '207.19',
                'Diamond' => '337.29'
    ]
    );


/*
ATTENTION !
This pricing block may have a param 'location', so the pricing must be shown exactly for this location
------------------------------------------------------------------------------------------------------
*/

$forced_location = isset($params['location']) ? $params['location'] : false;

/*Mapping check
$locations = $config['products']['vps_hosting']['locations'];
echo '<pre>';
var_dump($locations);
echo '</pre>';    */

if ($forced_location != false) {
    switch ($forced_location) {
        case 'br':
            $cheapestLocationKey = 'sao_paulo';
            $defaultLocationKey = 'sao_paulo';
            $no_strikethrough_prices = true;    // hide old prices, since it cannot be managed properly
            break;
        case 'de':
            $cheapestLocationKey = 'frankfurt';
            $defaultLocationKey = 'frankfurt';
            $no_strikethrough_prices = true;    // hide old prices, since it cannot be managed properly
            break;
    }
} else {

    /*This is general default option*/

    $cheapestLocationKey = $mypage->getCheapestLocation();
    $defaultLocationKey = $mypage->getDefaultLocation();
    $no_strikethrough_prices = false;
}


?>



<script>
    (function($){
        
        $(document).ready(function() {
            $('[data-submit-form]').click(function(){
                $(".plan").removeClass('selected');
                $(this).closest(".plan").addClass('selected');
                $(this).closest("form").submit();
            });
        });
        
    })(jQuery)
</script>
