<?php         

global $whmcs;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

/*Get CS backup config option pricing
-------------------------------------*/
// id=356 - CS Frankfurt

$cs_hosting_config_options = $whmcs->getConfigurablePrices(356);

$backup_1_value = (float) $cs_hosting_config_options['Backups']['options']['Backup']['pricing']['monthly']; // one backup
$backup_1_formatted = number_format ( $backup_1_value, 2);

$cloud_hosting_ip = $cs_hosting_config_options['IP']['options']['IP']['pricing']['monthly'];
?>            

                <div class="three-col-row feature-block-below-pricing">
                    <div class="block-col">
                        <img class="push-left m-r-10" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/cloud_servers/plans_lin_win.svg" alt="Linux and Windows" width="70">
                        <img class="feature-block-arrow" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/cloud_servers/plan_arrow_big_thin.svg" alt="" width="12">
                        <p class="descr-text-feature size-18 p-v-10"><?php _e('Pick Linux or Windows OS'); ?></p>
                    </div> 
                    <div class="block-col">
                        <img class="push-left m-r-10" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/cloud_servers/plans_longer_billing_cycles.svg" alt="Save on billing cycles" width="70">
                        <img class="feature-block-arrow" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/cloud_servers/plan_arrow_big_thin.svg" alt="" width="12">
                        <p class="descr-text-feature size-18 p-v-10"><?php echo sprintf(_x_lc('Save %1$s up to 20%% %2$s for longer billing cycles!'), '<span class="highlight has-tooltip" data-tooltip="billing-cycle-savings">', '</span>'); ?></p>
                        <div class="tooltip-body style2 w-600" data-tooltip-content="billing-cycle-savings"><p><?php _e('Save up to 20% with longer billing cycles ranging from 3 to 24 months. The discount increases according to the length of the billing cycle.'); ?></p></div>
                        <div class="triangle"></div>
                    </div> 
                    <div class="block-col">
                        <img class="push-left m-r-10" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/cloud_servers/plans_customize.svg" alt="Flexible customization" width="70">
                        <p class="descr-text-feature size-18 p-v-10"><?php _e('Customize your plan at your Client Area'); ?></p>
                    </div> 
                </div>
                
                <div class="row">
                    <div class="block-title-strikethrough">
                        <h3 class="block-title"><?php _e('All unmanaged plans include')?></h3>
                    </div>
                </div>

                <div class="container-flex">
                    <div class="block-fluid">
                        <ul class="features-list checklist brightblue">
                            <li><?php printf(__('Intel%s Xeon%s E5 v3 processors'),'<sup>&reg;</sup>','<sup>&reg;</sup>')?></li>
                            <li><?php _e('DDR4 error correcting-code RAM')?></li>
                            <li><?php _e('Up to 32 IPv4 addresses')?></li>
                        </ul>
                    </div>
                    <div class="block-fluid">
                        <ul class="features-list checklist brightblue">
                            <li><?php _e('KVM virtualization technology')?></li>
                            <li><?php _e('API')?></li>
                            <li><?php _e('Scheduled backups')?> <i class="fa fa-info-circle tooltip-icon" data-tooltip="schedulled-backups"></i>
                                <div class="tooltip-body style2 w400" data-tooltip-content="schedulled-backups">
                                    <div class="triangle"></div>
                                    <p><?php printf(__('Schedule daily, weekly, monthly auto-backups or add a backup manually at your Client Area for %s%s each.'), $whmcs::$settings['currency_prefix'], $backup_1_formatted); ?></p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="block-fluid">
                        <ul class="features-list checklist brightblue">
                            <li><?php _e('IPv6 support')?></li>
                            <li><?php _e('10G network')?></li>
                            <li><?php _e('1 free IPv4')?> <i class="fa fa-info-circle tooltip-icon" data-tooltip="free-ipv4"></i>
                                <div class="tooltip-body style2 w400" data-tooltip-content="free-ipv4">
                                    <div class="triangle"></div>
                                    <p><?php printf(__('Need more? Add up to 32 IPv4 addresses at your Client Area for %s each.'), $whmcs::$settings['currency_prefix'] . $cloud_hosting_ip); ?></p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="block-title-strikethrough-close"></div>

<?php         
/*
            </div> <!-- end of .container -->
        </section> <!-- end of .service-pricing -->
*/
?>                     