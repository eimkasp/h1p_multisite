<?php

global $whmcs;
global $whmcs_promo;
global $config;


/*Uses $mypage data from PageData*/


// Popular plan setting
$most_popular_marking = "Personal";
$most_popular_description = __('Most popular');

/* Default location
    <input type="hidden" name="configoption[416]" value="4002">

    Frankfurt - 2661
*/

?>
<div class="plans-pricing-table reseller totalcol-4 horizontal">

    <?php

    foreach($config['products']['reseller_hosting']['plans'] as $plan_key => $plan):

        $planPrices = $whmcs->getPriceOptions($plan['id']);
        $selectedCycle = 3;
        $selectedPrice = $whmcs->getPrice($plan['id'], $selectedCycle, 'price_monthly');


        // Checking if current plan is the Popular

        if ( $plan['title'] == $most_popular_marking ) {
            $is_popular_class = " popular";
            $is_popular_descr = "<span>" . $most_popular_description . "</span>";
        } else {
            $is_popular_class = "";
            $is_popular_descr = "";
        }

        ?>

        <div class="plan-col plan-col-<?php echo $plan_key;?>">
            <div class="plan<?php echo $is_popular_class ?>">

                <form action="<?php echo $config['whmcs_links']['checkout_web']?>" method="GET" class="pricing-table-js plan-block<?php if(isset($plan['tags']) && in_array('best-value', $plan['tags'])):?> active<?php endif;?>">

                    <input type="hidden" name="a" value="add"/>
                    <input type="hidden" name="pid" value="<?php echo $plan['id'];?>"/>
                    <input type="hidden" name="configoption[416]" value="2661">
                    <input type="hidden" name="billingcycle" value="quarterly">
                    <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                    <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                    <input type="hidden" name="promocode" value="<?php echo $mypage->promo;?>"/>

                    <div class="plan-header-wrapper" data-submit-form>
                        <div class="cell plan-name"><?php echo $plan['title']?><?php echo $is_popular_descr ?></div>
                        <div class="cell price-wrapper">
                            <div class="price">
                                <?php
                                $prices = $planPrices;
                                ?>
                                <span class="from"><?php _e('from')?></span>
                                <span class="main-price"><span class="prefix"><?php echo $whmcs::$settings['currency_prefix'] ?></span><?php echo $prices[0]['price_value']; ?></span><span class="mo"><?php _e('/mo')?></span>
                            </div>
                        </div>
                    </div>

                    <div class="plan-body-wrapper">

                        <div class="cell feature">
                            <div class="name"><?php _e('Disk space')?></div>
                            <div class="value" data-tooltip="unlimited-description-<?php echo strtolower($plan['title']);?>"><?php echo $plan['params']['disk_space'] > -1 ? $plan['params']['disk_space'] : _x_lc('Unlimited', 'plural feminine') ?>
                            </div>
                        </div>
                        <div class="cell feature">
                            <div class="name"><?php _e('Bandwidth')?></div>
                            <div class="value"><?php echo $plan['params']['bandwidth'];?></div>
                        </div>
                        <div class="cell feature">
                            <div class="name"><?php _e('Hosted domains')?></div>
                            <div class="value"><?php echo $plan['params']['hosted_domains'] > -1 ? $plan['params']['hosted_domains'] : _x_lc('Unlimited', 'plural musculine');?></div>
                        </div>
                        <div class="cell feature">
                            <div class="name"><?php _e('Sub-domains')?></div>
                            <div class="value"><?php echo $plan['params']['sub_domains'] > -1 ? $plan['params']['sub_domains'] : _x_lc('Unlimited', 'plural musculine');?></div>
                        </div>
                        <div class="cell feature">
                            <div class="name"><?php _e('Databases')?></div>
                            <div class="value"><?php echo $plan['params']['database'] > -1 ? $plan['params']['database'] : _x_lc('Unlimited', 'plural feminine');?></div>
                        </div>
                        <div class="cell feature">
                            <div class="name"><?php _e('Dedicated IP')?></div>
                            <div class="value">
                                <?php if ( $plan['params']['free_ip'] == 1 ) {
                                    _e('FREE');
                                } else {
                                    echo $whmcs::$settings['currency_prefix'];
                                    echo $addons_dedicated_ip['price'];
                                    _e('/mo');
                                }
                                ?>
                            </div>
                        </div>
                        <div class="cell order">
                            <button id="reseller-checkout-link" type="submit" class="button primary"><?php _e('Select')?></button>
                        </div>

                    </div>

                </form>

            </div>
        </div>

    <?php endforeach;?>

</div>

<script>
    (function($){

        $(document).ready(function() {
            $('[data-submit-form]').click(function(){
                $(".plan").removeClass('selected');
                $(this).closest(".plan").addClass('selected');
                $(this).closest("form").submit();
            });
        });

    })(jQuery)
</script>
