<?php
global $config;
global $whmcs_promo;
global $whmcs;


/*Uses $mypage data from PageData*/


$priceOs = 'linux';
$plansConfigGroup = $whmcs->get_local_stepsconfig('cloud_plans');
$plansConfigLinux = $plansConfigGroup[$priceOs];

$plansConfigGroupBaseValues = $whmcs->get_local_stepsconfig('cloud_base_values');
/*echo '<pre>';
var_dump($plansConfigGroup);
echo '</pre>';
*/
/*SCHEME for exact plan resource calculation
-------------------------------------------
Plan resource value = $plansConfigGroup * $plansConfigGroupBaseValues
*/


/*
ATTENTION !
This pricing block may have a param 'location', so the pricing must be shown exactly for this location
------------------------------------------------------------------------------------------------------
*/

$forced_location = isset($params['location']) ? $params['location'] : false;

/*Mapping check
$locations = $config['products']['vps_hosting']['locations'];
echo '<pre>';
var_dump($locations);
echo '</pre>';    */

if ($forced_location != false) {
    switch ($forced_location) {
        case 'de':
            $cheapestLocationKey = 'germany';
            $defaultLocationKey = 'germany';
            break;
    }
} else {

    /*This is general default option*/

    $cheapestLocationKey = $mypage->translateLocation($mypage->getCheapestLocation('cs_lin'));
    $defaultLocationKey = $mypage->translateLocation($mypage->getDefaultLocation('cs_lin'));

}

$location = $plansConfigLinux[$defaultLocationKey]['pid'];

?>

<div class="pricing-anouncement-before-table">
</div>

<div class="plans-pricing-table cs totalcol-6">

<?php
    
/*echo '<pre>';
var_dump($plansConfigLinux);
echo '</pre>';*/

/*
Build plans structure on this default location, and make checkout link on it.
*/

$plansSet = $plansConfigLinux[$defaultLocationKey]['plans'];  // chosen as default, assuming resource structure will not change

/*echo '<pre>';
var_dump($plansSet);
echo '</pre>';
*/




    $most_popular_marking = "LIN2";
    $most_popular_description = __('Most popular');

    $i = 0; // plan column counter

    foreach($plansSet as $plan_key => $plan):

        $planOptions = $plan['options'];

        // Checking if current plan is the Popular

        if (isset($most_popular_marking))
        {
            if ( $plan['name'] == $most_popular_marking ) {
                $is_popular_class = " popular";
                $is_popular_descr = "<span>" . $most_popular_description . "</span>";
            } else {
                $is_popular_class = "";
                $is_popular_descr = "";
            }
        } else {
            $is_popular_class = "";
            $is_popular_descr = "";
        }


        $prices = $whmcs->getCloudConfigurablePrice( $priceOs , $defaultLocationKey, $plan_key );

        /*echo '<pre>';
        var_dump($prices);
        echo '</pre>';*/

?>

        <div class="plan-col plan-col-<?php echo $i+1?>">
            <div class="plan<?php echo $is_popular_class ?>">
                <form class="pricing-table-js" action="<?php echo $config['whmcs_links']['checkout_cloud']?>" method="get">

                    <input type="hidden" name="force_clean" value="1"/>
                    <input type="hidden" name="plan" value="<?php echo $plan_key;?>"/>
                    <input type="hidden" name="language" value="<?php echo $config['whmcs_lang']; ?>"/>
                    <input type="hidden" name="currency" value="<?php echo $config['whmcs_curr']; ?>"/>
                    <input type="hidden" name="pid" value="<?php echo $location; ?>"/>
                    <input type="hidden" name="billingcycle" value="quarterly"/>
                    <input type="hidden" name="promocode" value="<?php echo $mypage->promo;?>"/>

                    <div class="plan-header-wrapper" data-submit-form>
                        <div class="cell plan-name"><?php echo $plan['name']?><?php echo $is_popular_descr ?></div>
                        <div class="cell price-wrapper">
                            <div class="price">
                                <?php

                                    // No discount

                                    $system_price = $prices[0]['price_value'];
                                    // next time use PageData functions to get $price_with_promo

                                    $main_price = $system_price;

                                ?>

                                <span class="from"><?php _e('from')?></span>

                                <?php if ( isset($price_with_promo) ) { ?>

                                <div class="display-inline breakpoint-w1016-max">
                                    <span class="main-price old-price strikethrough"><span class="prefix old-price"><?php echo $whmcs::$settings['currency_prefix'] ?></span><?php echo $system_price; ?></span><span class="mo p-r-10"><?php _e('/mo')?></span>
                                </div>

                                <?php } ?>

                                <span class="main-price"><span class="prefix"><?php echo $whmcs::$settings['currency_prefix'] ?></span><?php echo $main_price; ?></span><span class="mo"><?php _e('/mo')?></span>

                                <?php if ( isset($price_with_promo) ) { ?>

                                <div class="display-block breakpoint-w1016-min m-up-5">
                                    <span class="main-price old-price strikethrough small-price"><span class="prefix old-price"><?php echo $whmcs::$settings['currency_prefix'] ?></span><?php echo $system_price; ?></span><span class="mo p-r-10"><?php _e('/mo')?></span>
                                </div>

                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="plan-body-wrapper">
                        <div class="cell feature">
                            <div class="name"><?php _e('CPU')?></div>
                            <div class="value">
                                    <?php
                                    $thisval = $plansConfigGroupBaseValues['cpu'] * $planOptions['cpu'];
                                    switch(get_numeric_case($thisval, true))
                                    {
                                        case 0:
                                            echo $thisval . ' ' . _x_lc('Cores', '0 Cores');
                                            break;
                                        case 1:
                                            echo $thisval . ' ' . _x_lc('Core', '1 Core');
                                            break;
                                        default:
                                            echo $thisval . ' ' . _x_lc('Cores', '2 Cores');
                                            break;
                                    }
                                    ?>
                            </div>
                        </div>
                        <div class="cell feature">
                            <div class="name"><?php _e('RAM')?></div>
                            <div class="value">
                                <?php
                                    $thisval = $plansConfigGroupBaseValues['ram'] * $planOptions['ram'];
                                    echo $thisval . ' ';
                                    _e('MB');
                                ?>
                                </div>
                        </div>
                        <div class="cell feature">
                            <div class="name"><?php _e('Disk space')?></div>
                            <div class="value">
                            <?php
                                $thisval = $plansConfigGroupBaseValues['hdd'] * $planOptions['hdd'];
                                echo $thisval . ' ';
                                _e('GB');
                            ?>
                            </div>
                        </div>
                        <div class="cell feature">
                            <div class="name"><?php _e('Bandwidth')?></div>
                            <div class="value">
                            <?php
                                $thisval = $plansConfigGroupBaseValues['bandwidth'] * $planOptions['bandwidth'];
                                echo $thisval . ' ';
                                _e('TB');
                            ?>
                            </div>
                        </div>
                        <div class="cell order">
                            <button type="submit" class="cs-checkout-link button primary"><?php _e('Select')?></button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

    <?php $i++; endforeach;?>

</div>

<script>
    (function($){

        $(document).ready(function() {

            $('[data-submit-form]').click(function(){
                $(".plan").removeClass('selected');
                $(this).closest(".plan").addClass('selected');
                $(this).closest("form").submit();
            });
        });

    })(jQuery)
</script>
