<section class="block installer bg-white">

        <div class="container">
            <div class="row">

                <h2 class="block-title"><?php _e('One-Click Application Auto-Installer')?></h2>

                <div class="video-container">
                   <a id="app-installer-video" href="javascript:" class="video" data-popup="https://www.youtube.com/watch?v=wUzL-eJ7klE" data-popup-type="iframe"><span class="button-play"></span></a>
                </div>

                <ul class="apps">
                    <li><i class="icon joomla"></i></li>
                    <li><i class="icon magento"></i></li>
                    <li><i class="icon oxid"></i></li>
                    <li><i class="icon prestashop"></i></li>
                    <li><i class="icon oscommerce"></i></li>
                    <li><i class="icon wordpress"></i></li>
                </ul>

                <div class="app-description">
                    <p class="title"><?php _e('Build your online project with ease!')?></p>
                    <p class="description"><?php _e('Select the ultimate solution for easier management! Ultra-fast one-step auto-installers will power-up your online project with a single click.')?></p>
                    <p class="description"><?php printf(__('%sInstallatron%s or %sSoftaculous%s cover all your needs – enjoy the most popular applications for community building, content management, e-commerce and business, galleries, statistics, surveys, and many more.'), '<strong>', '</strong>', '<strong>', '</strong>')?></p>
                    <a id="app-installer-promo" class="button primary get-started" href="#hosting-1" data-scrollto="web-hosting-plans"><?php _e('Get Started')?></a>
                </div>


            </div>
        </div>

</section>
