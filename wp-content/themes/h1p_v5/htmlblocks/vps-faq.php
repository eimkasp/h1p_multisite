<div class="faq2">
    <h2 class="title"><?php _e('Ask questions, get answers!');?></h2>
    <div class="tabs container">
        <span class="tab active" data-tab-index="faq"><?php _e('FAQ');?></span>
        <span class="tab" data-tab-index="tutorials"><?php _e('Tutorials');?></span>
        <span class="tab-spacing"></span>
    </div>
    <div class="tabs-content container">
        <div class="tab-content active" data-tab-content="faq">
            <div class="question">
                <?php _e('How long will my VPS setup take?');?>
                <div class="answer">
                    <?php _e('After you order a VPS, the service is usually set up instantly. However, if you pay using a credit card, it might take up to a few hours.');?>
                </div>
            </div>
            <div class="question">
                <?php _e('What virtualization do you use for VPS hosting?');?>
                <div class="answer">
                    <?php _e('We are currently using OpenVZ virtualization.');?>
                </div>
            </div>
            <div class="question">
                <?php _e('What is the network speed for VPS hosting?');?>
                <div class="answer">
                    <?php printf(__('We limit the network speed provided depending on the location of your VPS hosting service.<br>
                    Outgoing (data that is being sent from your server to another one) network speed is limited up to 500 Mbps in all VPS hosting locations except São Paulo, Brazil, where network speed limit is higher - up to 200 Mbps.<br>
                    Incoming (data that is being transferred to your server from another one) network speed does not have any limitations.<br>
                    If your network usage seems still too slow, you can sign up for a dedicated hosting plan and boost your network speed up to 1 Gbps.<br>
                    However, if you wish to switch to dedicated server hosting, you need to migrate your data. You can either do this yourself or contact us at %s to help you out.'),'<a href="mailto:support@host1plus.com">support@host1plus.com</a>')?>
                </div>
            </div>
            <div class="question">
                <?php _e('Which control panels can I install on my VPS?');?>
                <div class="answer">
                    <?php _e('You can choose to automatically install cPanel or ISPconfig control panels at the checkout.<br>
                    However, you can also install free open source control panels or any other web server management panel by yourself such as Plesk, Ajenti, Kloxo, OpenPanel, ZPanel, EHCP, ispCP, VHCS, RavenCore, Virtualmin, Webmin, DTC, DirectAdmin, InterWorx, SysCP, BlueOnyx and more.');?>
                </div>
            </div>
            <div class="question">
                <?php _e('Is it possible to install a custom OS to OpenVZ VPS?');?>
                <div class="answer">
                    <?php _e('We are using only official OpenVZ templates only so it is not possible to install any custom OS.');?>
                </div>
            </div>
            <div class="question">
                <?php _e('What happens when my CPU limit is reached?');?>
                <div class="answer">
                    <?php _e('If you are overusing your CPU you will receive a notification that the power of your processor is temporarily limited.<br>
                    You can review your CPU power usage on the statistics page in your Client Area (for more information <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1244/108/where-can-i-find-my-vps-statistics-page">click here</a>).<br>
                    If you want to avoid reaching the limit, you can upgrade your CPU in your Client Area (for more information <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1241/108/how-to-upgradedowngrade-my-vps">click here</a>).');?>
                </div>
            </div>
        </div>
        <div class="tab-content" data-tab-content="tutorials">
            <div class="tutorial-list">
                <div class="tutorial-item">
                    <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/how-to-create-virtualhost-on-apache"><i class="fa fa-file-text-o file-ico"></i> <?php _e('How to create virtualhost on Apache?');?></a>
                </div>
                <div class="tutorial-item">
                    <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-csf-on-linux"><i class="fa fa-file-text-o file-ico"></i> <?php _e('How to install CSF on Linux?');?></a>
                </div>
                <div class="tutorial-item">
                    <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/introduction-to-ssh-keys"><i class="fa fa-file-text-o file-ico"></i> <?php _e('Introduction to SSH keys');?></a>
                </div>
                <div class="tutorial-item">
                    <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-gnome-desktop"><i class="fa fa-file-text-o file-ico"></i> <?php _e('How to install GNOME desktop?');?></a>
                </div>
                <div class="tutorial-item">
                    <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-change-a-time-zone-on-a-vps-server"><i class="fa fa-file-text-o file-ico"></i> <?php _e('How to change a time zone on a VPS server?');?></a>
                </div>
                <div class="tutorial-item">
                    <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-add-ip-address-on-centos-operating-systems"><i class="fa fa-file-text-o file-ico"></i> <?php _e('How to add IP address on CentOS operating system?');?></a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $('[data-tab-index]').click(function(){
        var tab_index = $(this).attr('data-tab-index');

        $('[data-tab-index]').removeClass('active');
        $(this).addClass('active');

        $('[data-tab-content]').removeClass('active');
        $('[data-tab-content="'+ tab_index +'"]').addClass('active');
    });

    $('.question:not(.active)').click(function(){
        $('.question').removeClass('active');
        $(this).addClass('active');
    });
});
</script>