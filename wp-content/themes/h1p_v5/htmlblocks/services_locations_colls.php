<?php

if( isset( $params ) && !empty( $params ) ):

    global $whmcs;
    global $config;

    $available = [
        'services' => [
            'web_hosting' => [
                'title' => '<div class="big">WEB</div> hosting',
                'minPrice' => $whmcs->getMinPrice($config['products']['web_hosting']['plans'][1]['id']),
                'link' => $config['links']['web-hosting']
            ],
            'vps_hosting' => [
                'title' => '<div class="big">VPS</div> hosting',
                'minPrice' => $whmcs->getMinPrice($config['products']['vps_hosting']['locations']['frankfurt']['id']),
                'link' => $config['links']['website'] . '/germany-vps-hosting/'
            ],
            'reseller_hosting' => [
                'title' => '<div class="big">RESELLER</div> hosting',
                'minPrice' => $whmcs->getMinPrice($config['products']['reseller_hosting']['plans'][1]['id']),
                'link' => $config['links']['reseller-hosting']
            ],
            'dedicated_servers' => [
                'title' => '<div class="big">DEDICATED</div> servers',
                'minPrice' => $whmcs->getPriceOptions($config['products']['dedicated_servers']['plans'][1]['id'], 'quarterly')['price_monthly'],
                'link' => $config['links']['dedicated-servers']
            ]
        ],
        'locations' => [
            'los_angeles' => [
                'title' => 'Los Angeles',
                'countryISO' => 'US'
            ],
            'chicago' => [
                'title' => 'Chicago',
                'countryISO' => 'US'
            ],
            'san_paulo' => [
                'title' => 'São Paulo',
                'countryISO' => 'BR'
            ],
            'johannesburg' => [
                'title' => 'Johannesburg',
                'countryISO' => 'ZA'
            ],
            'london' => [
                'title' => 'London',
                'countryISO' => 'GB'
            ],
            'frankfurt' => [
                'title' => 'Frankfurt',
                'countryISO' => 'DE'
            ],
            'amsterdam' => [
                'title' => 'Amsterdam',
                'countryISO' => 'NL'
            ],
            'siauliai' => [
                'title' => 'Šiauliai',
                'countryISO' => 'LT'
            ]
        ]
    ];

    ?>

    <div class="services_locations_container">
        <div class="trow">
        <?php
            foreach( $params as $service_key => $locations_list ):
                if( ! array_key_exists( $service_key, $available['services'] ) ) continue; // if array key(service) doesnt exists just skip
                $service = $available['services'][ $service_key ];
        ?>
        <div class="service_block no-border-bot">
            <div class="service_name row">
                <?php echo $service['title'] ?>
            </div>
            <div class="price_block row">
                <div class="price">
                    <span class="from">from</span>
                    <span class="prefix"></span><span class="main-price"><?php echo $service['minPrice'] ?></span><span class="mo">/mo</span>
                </div>
            </div>
            <div class="locations_block row">
                Locations
                <?php
                    foreach( $locations_list as $loc ):
                        if( ! array_key_exists( $loc, $available['locations'] ) ) continue; // if array key(service) doesnt exists just skip
                        $location = $available['locations'][ $loc ];
                ?>
                <div class="location">
                    <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/16/<?php echo $location['countryISO'] ?>.png" alt="" /><?php echo $location['title'] ?>
                </div>
            <?php endforeach; ?>
            </div>
            <div class="button_block row mobile">
                <a href="<?php echo $service['link'] ?>" class="button blue-full">Learn more</a>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
    <div class="trow desktop">

        <?php
            foreach( $params as $service_key => $locations_list ):
                if( ! array_key_exists( $service_key, $available['services'] ) ) continue; // if array key(service) doesnt exists just skip
                $service = $available['services'][ $service_key ];
        ?>
        <div class="service_block">
            <div class="button_block row">
                <a href="<?php echo $service['link'] ?>" class="button blue-full">Learn more</a>
            </div>
        </div>
        <?php endforeach; ?>

    </div>
</div>
<?php

endif;