<div class="partners-2 container">

    <div class="title"><?php _e('Meet some of our clients!');?></div>

    <div class="content">

        <div class="partners-list">
            <div class="partner-block">
                <div class="logo fedora"></div>
            </div>
            <div class="partner-block">
                <div class="logo scrapinghub"></div>
            </div>
            <div class="partner-block">
                <div class="logo cormack"></div>
            </div>
            <div class="partner-block">
                <div class="logo corephp"></div>
            </div>
            <div class="partner-block">
                <div class="logo ghostbsd"></div>
            </div>
        </div>

    </div>

</section>
