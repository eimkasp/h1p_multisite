<?php

global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

?>

        <section class="hosting-solutions extra-pad-top">

            <div id="services" class="scroll-id-target"></div>

            <div class="container">

                <div class="section-header">
                    <h2 class="block-title"><?php _e('Optimized Hosting Solutions')?></h2>
                </div>


                <div class="hosting-solutions-list row four-col-row m-t-20">

                    <div class="block-col">
                        <h3><?php _e('VPS Hosting'); ?></h3>
                        <span><?php printf(__('from %s/mo'), $mypage->getServiceMinPrice('vps'))?></span>
                        <br>
                        <p class="p-v-20 descr"><?php _e('A sustainable platform built for developers.'); ?></p>

                        <div class="row plan-feature">
                            <img class="plan-feature-ico" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/plan-feat-uplink.svg" alt="Uplink">
                            <span class="plan-feature-descr"><?php _e('500 Mbps uplink'); ?></span>
                        </div>
                        <div class="row plan-feature">
                            <img class="plan-feature-ico" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/plan-feat-cpu.svg" alt="CPU" width="30" height="30">
                            <span class="plan-feature-descr"><?php _e('Intel Xeon processors'); ?></span>
                        </div>
                        <div class="row plan-feature">
                            <img class="plan-feature-ico" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/plan-feat-global.svg" alt="DNS" width="30" height="30">
                            <span class="plan-feature-descr"><?php _e('Integrated DNS management'); ?></span>
                        </div>
                        <div class="row plan-feature">
                            <img class="plan-feature-ico" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/plan-feat-locations.svg" alt="Locations" width="30" height="30">
                            <span class="plan-feature-descr"><a href="/vps-brazil/"><?php _e('Brazil'); ?></a>, <a href="/germany-vps/"><?php _e('Germany'); ?></a>, <?php _e('South Africa'); ?>, <a href="/vps-usa/"><?php _e('USA'); ?></a></span>
                        </div>
                        <a href="<?php echo $config['links']['vps-hosting']; ?>" class="button highlight m-v-20"><?php _e('View Plans');?></a>
                    </div>

                    <div class="block-col">
                        <h3><?php _e('Cloud Servers'); ?></h3>
                        <span><?php printf(__('from %s/mo'), $mypage->getServiceMinPrice('cs_lin')); ?></span>
                        <br>
                        <p class="p-v-20 descr"><?php _e('Cloud computing made simple.'); ?></p>

                        <div class="row plan-feature">
                            <img class="plan-feature-ico" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/plan-feat-api.svg" alt="Uplink">
                            <span class="plan-feature-descr"><?php _e('API'); ?></span>
                        </div>
                        <div class="row plan-feature">
                            <img class="plan-feature-ico" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/plan-feat-kvm.svg" alt="CPU" width="30" height="30">
                            <span class="plan-feature-descr"><?php _e('KVM'); ?></span>
                        </div>
                        <div class="row plan-feature">
                            <img class="plan-feature-ico" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/plan-feat-os.svg" alt="DNS" width="30" height="30">
                            <span class="plan-feature-descr"><?php _e('Windows & Linux'); ?></span>
                        </div>
                        <div class="row plan-feature">
                            <img class="plan-feature-ico" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/plan-feat-locations.svg" alt="Locations" width="30" height="30">
                            <span class="plan-feature-descr"><a href="/cloud-germany/"><?php _e('Germany'); ?></a>, <?php _e('USA'); ?></span>
                        </div>                        
                        <a href="<?php echo $config['links']['cloud-servers']; ?>" class="button highlight m-v-20"><?php _e('View Plans');?></a>
                    </div>
          
                    <div class="block-col">
                        <h3><?php _e('Web Hosting'); ?></h3>
                        <span><?php printf(__('from %s/mo'), $mypage->getServiceMinPrice('web'))?></span>
                        <br>
                        <p class="p-v-20 descr"><?php _e('A secure environment for small to medium websites.'); ?></p>

                        <div class="row plan-feature">
                            <img class="plan-feature-ico" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/plan-feat-infinity.svg" alt="Unlimited">
                            <span class="plan-feature-descr"><?php _e('Unlimited space and traffic'); ?></span>
                        </div>
                        <div class="row plan-feature">
                            <img class="plan-feature-ico" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/plan-feat-click.svg" alt="Instant" width="30" height="30">
                            <span class="plan-feature-descr"><?php _e('One-click app installation'); ?></span>
                        </div>
                        <div class="row plan-feature">
                            <img class="plan-feature-ico" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/plan-feat-cpanel.svg" alt="Management" width="30" height="30">
                            <span class="plan-feature-descr"><?php _e('cPanel'); ?></span>
                        </div>
                        <div class="row plan-feature">
                            <img class="plan-feature-ico" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/plan-feat-locations.svg" alt="Locations" width="30" height="30">
                            <span class="plan-feature-descr"><?php _e('Brazil'); ?>, <?php _e('Germany'); ?>, <?php _e('USA'); ?></span>
                        </div>
                        <a href="<?php echo $config['links']['web-hosting']; ?>" class="button highlight m-v-20"><?php _e('View Plans');?></a>
                    </div>

                    <div class="block-col">
                        <h3><?php _e('Reseller Hosting'); ?></h3>
                        <span><?php printf(__('from %s/mo'), $mypage->getServiceMinPrice('res'))?></span>
                        <p class="p-v-20 descr"><?php _e('Reliable services for smooth web hosting business.'); ?></p>

                        <div class="row plan-feature">
                            <img class="plan-feature-ico" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/plan-feat-location.svg" alt="Unlimited">
                            <span class="plan-feature-descr"><?php _e('Free dedicated IP'); ?></span>
                        </div>
                        <div class="row plan-feature">
                            <img class="plan-feature-ico" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/plan-feat-user.svg" alt="Accounts" width="30" height="30">
                            <span class="plan-feature-descr"><?php _e('Unlimited accounts'); ?></span>
                        </div>
                        <div class="row plan-feature">
                            <img class="plan-feature-ico" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/plan-feat-app-installer.svg" alt="Installer" width="30" height="30">
                            <span class="plan-feature-descr"><?php _e('App Installer'); ?></span>
                        </div>
                        <div class="row plan-feature">
                            <img class="plan-feature-ico" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/plan-feat-locations.svg" alt="Locations" width="30" height="30">
                            <span class="plan-feature-descr"><?php _e('Brazil'); ?>, <?php _e('Germany'); ?></span>
                        </div>
                        <a href="<?php echo $config['links']['reseller-hosting']; ?>" class="button highlight m-v-20"><?php _e('View Plans');?></a>
                    </div>

                </div>

            </div> <!-- end of .container -->

        </section> <!-- end of .hosting-solutions -->
