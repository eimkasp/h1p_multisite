<section class="block whyus">

    <div class="container">
        <div class="row">

            <h2 class="block-title"><?php _e('Why choose us?')?></h2>

            <div class="block-content">
                <div class="features">
                    <div class="feature feature-1">
                        <span class="image">
                            <i class="icon whyus support"></i>
                        </span>
                        <span class="label"><?php _e('Quick Professional Support')?></span>
                    </div>
                    <div class="feature feature-2">
                        <span class="image">
                            <i class="icon whyus multilocational"></i>
                        </span>
                        <span class="label"><?php _e('Multi-Locational')?></span>
                    </div>
                    <div class="feature feature-3">
                        <span class="image">
                            <i class="icon whyus price"></i>
                        </span>
                        <span class="label"><?php _e('Best Price')?><br/><?php _e('Top Performance')?></span>
                    </div>
                    <div class="feature feature-4">
                        <span class="image">
                            <i class="icon whyus vpsconfig"></i>
                        </span>
                        <span class="label"><?php _e('Custom VPS Server Configuration')?></span>
                    </div>
                    <div class="feature feature-5">
                        <span class="image">
                            <i class="icon whyus hddssd"></i>
                        </span>
                        <span class="label"><?php _e('HDD + SSD')?></span>
                    </div>
                    <div class="feature feature-6">
                        <span class="image">
                            <i class="icon whyus installer"></i>
                        </span>
                        <span class="label"><?php _e('Application Auto-Installer')?></span>
                    </div>
                </div>
                <div class="description">
                    <?php _e('Host1Plus takes pride in its enduring experience in the field. Our team of highly-trained IT professionals is constantly growing and enhancing their expertise. However, the most valuable knowledge came from hard work and learning from our mistakes.')?>
                </div>
            </div>

        </div>
    </div>

</section>
