<?php

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

// Check these IDs !!!
$addons_extracare_1level = $whmcs->getAddonsPrices(27);
$addons_extracare_2level = $whmcs->getAddonsPrices(28);

// print_r ($addons_extracare_1level);
// print_r ($addons_extracare_2level);

//Get Cloud IP config option pricing
// id=342

$cloud_hosting_config_options = $whmcs->getConfigurablePrices(342);
$cloud_hosting_ip = $cloud_hosting_config_options['IP']['options']['IP']['pricing']['monthly'];


function get_addon_billing_cycle ( $addon_set ) {
    switch ( $addon_set['billingcycle'] ) {
        case 'Free':
            return 'free';
            break;
        case 'One Time':
            return '/hour';
            break;
        case 'Monthly':
            return '/month';
            break;
        case 'Quarterly':
            return '/quarter';
            break;
        case 'Semi-Annually':
            return '/half-year';
            break;
        case 'Annually':
            return '/year';
            break;
        case 'Biennially':
            return '/biennially';
            break;
        case 'Triennially':
            return '/triennially';
            break;

        default:
            return false;
            break;
    }
}
?>

    <div class="faq3">
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq"><?php _e('FAQ');?></h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    <?php _e('What are the main differences between VPS and Cloud Servers?');?>
                    <div class="answer">
                        <p><?php printf (__('VPS offers simplified management options, while Cloud Servers provide significantly higher performance, scalability, increased control and the choice between Windows and Linux operating systems as well as a %spowerful API%s.'), '<a target="_blank" href="https://www.host1plus.com/api/">', '</a>'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Do you provide a money-back guarantee?');?>
                    <div class="answer">
                        <p><?php printf (__('Of course! You can check out if our Germany Cloud services meet your needs for a 14-day period. If you do not like it, you can request a refund by contacting us at %ssupport@host1plus.com%s or by submitting a ticket at your Client Area.'), '<a href="mailto:support@host1plus.com">', '</a>');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('What payment method can I use?');?>
                    <div class="answer">
                        <p><?php _e('We accept Credit Card, Paypal, authorized PayPal payments, Alipay, Bitcoin, and other popular payment gateways.');?></p>
                        <p><?php printf (__('Please note that payment options vary depending on your location. Additional information can be found %shere%s.'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1312/0/what-payment-method-can-i-use">', '</a>'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Can I downgrade my  Germany Cloud Server?');?>
                    <div class="answer">
                        <p><?php _e('Yes! Our scalable Germany Cloud Servers can be configured by your preference. You can either boost your resources or downgrade. Please note that after you downgrade your service the money always comes back to your Credit Balance. You can use the amount to upgrade, order new services or add-ons to your existing service.');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Can I install another Linux distributions or only the templates that are available?');?>
                    <div class="answer">
                        <p><?php _e('On Germany Cloud Server, you can choose to install pre-configured Windows or Linux OS that we provide, or choose an empty template and install your own desired OS after you complete your purchase.'); ?></p>
                    </div>
                </div>
                

            </div>

            

            </div>
        </div>
    </div> <!-- end of .faq3 -->
