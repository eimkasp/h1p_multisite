<?php

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

/*cPanel price. cPanel is a product with id=129*/
$cPanel_data = $whmcs->getConfigurablePrice(129,[]);
/*echo '<pre>';
var_dump($cPanel_data);
echo '</pre>';*/
$cPanel_price_mo = $cPanel_data[0]['price_monthly'];

?>

    <div class="faq3">
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq"><?php _e('FAQ');?></h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    How cPanel Cloud Server hosting is different from regular Cloud Server hosting?
                    <div class="answer">
                        <p>cPanel Cloud Server hosting resembles regular Cloud Server hosting, however, cPanel Cloud Server hosting comes with a pre-equipped cPanel cotrol panel for simplified Cloud Server management and is especially suited for website management.</p>
                    </div>
                </div>
                <div class="question">
                    Do I have to pay for cPanel separately?
                    <div class="answer">
                        <p>You don't have to pay for cPanel separately if you have your own cPanel license already. However if you don't have cPanel license you can order it at the checkout or later via your Client Area. cPanel license comes as an add-on for <?php echo $cPanel_price_mo ?> / month.</p>
                    </div>
                </div>
                <div class="question">
                    Can I add more resources to my cPanel Cloud Server?
                    <div class="answer">
                        <p>You can <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1346/114/how-to-scale-my-cloud-server">upgrade</a> your cPanel Cloud Server anytime at your Client Area by either switching to a higher plan or adding particular resources.</p>
                    </div>
                </div>
                <div class="question">
                    Where Windows servers are available?
                    <div class="answer">
                        <p><?php printf (__('Currently you can choose from two locations - %sFrankfurt, Germany%s and %sChicago, US%s.'), '<a href="/cloud-germany/">', '</a>', '<a href="/cloud-servers/#windows">', '</a>');?></p>
                    </div>
                </div>
                <div class="question">
                    How long will my cPanel Cloud Server setup take?
                    <div class="answer">
                        <p>After you order a cPanel Cloud Server, the service is usually set up instantly. However, if you pay using a credit card, it might take up to a few hours.</p>
                    </div>
                </div>
                <div class="question">
                    Can I install another Linux distributions or only the templates that are available?
                    <div class="answer">
                        <p>On Linux Cloud Server, you can choose to install pre-configured CentOS distribution along with cPanel, or choose an empty template and install your Linux distribution after you complete your purchase. Note that other Linux distributions such as Debian, Fedora, Ubuntu do not come with pre-installed cPanel at the moment.</p>
                    </div>
                </div>
            </div>

            

            </div>
        </div>
    </div> <!-- end of .faq3 -->
