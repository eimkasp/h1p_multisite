<section class="block faq">

    <div class="container">
        <div class="row">
            <h2 class="block-title"><?php _e('Frequently Asked Questions')?></h2>
            <div class="block-content">
                <div class="faq-table">


                    <div class="faq-row">
                        <div class="faq-col mob">
                            <div class="content">
                                <div class="title"><?php _e('How long does it take set up my server?')?></div>
                                <div class="text"><?php _e('We guarantee that all our standard server configurations will be set up within 1 business day.')?></div>
                            </div>
                        </div>
                        <div class="faq-col space"></div>
                        <div class="faq-col">
                            <div class="content">
                                <div class="title"><?php _e('How many websites can I host on a dedicated server?')?></div>
                                <div class="text"><?php _e('There are no limits for the number of websites you can host on a dedicated server.')?></div>
                            </div>
                        </div>
                    </div>
                    <div class="faq-row">
                        <div class="faq-col mob">
                            <div class="content">
                                <div class="title"><?php _e('What operating system can I choose?')?></div>
                                <div class="text"><?php _e('You can choose from Windows or Linux dedicated servers.')?></div>
                            </div>
                        </div>
                        <div class="faq-col space"></div>
                        <div class="faq-col">
                            <div class="content">
                                <div class="title"><?php _e('Who is responsible for the server hardware?')?></div>
                                <div class="text"><?php _e('We guarantee the functioning of all dedicated server leased hardware components and will replace any failed component without additional charges. Hardware replacement or repair will begin immediately upon identification of a hardware problem.')?></div>
                            </div>
                        </div>
                    </div>
                    <div class="faq-row">
                        <div class="faq-col mob">
                            <div class="content">
                                <div class="title"><?php _e('What are the advantages of purchasing a dedicated server?')?></div>
                                <div class="text"><?php _e('The main advantage is that you get complete access to all of the resources of the hardware. Dedicated servers are much more secure because you get exclusive access to the physical machine and complete control of all the software installed. You may also enjoy unlimited bandwidth')?></div>
                            </div>
                        </div>
                        <div class="faq-col space"></div>
                        <div class="faq-col">
                            <div class="content">
                                <div class="title"><?php _e('Can I upgrade my server?')?></div>
                                <div class="text"><?php _e('You may always upgrade your server by logging in to your Client Area and adjusting your preferences.')?></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</section>


