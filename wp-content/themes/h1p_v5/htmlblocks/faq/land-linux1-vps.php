<?php

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

/*cPanel price. cPanel is a product with id=129*/
$cPanel_data = $whmcs->getConfigurablePrice(129,[]);
/*echo '<pre>';
var_dump($cPanel_data);
echo '</pre>';*/
$cPanel_price_mo = $cPanel_data[0]['price_monthly'];

?>

    <div class="faq3">
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq"><?php _e('FAQ');?></h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    <?php _e('How long will my Linux KVM VPS setup take?');?>
                    <div class="answer">
                        <p><?php _e('After you order a Linux KVM VPS, the service is usually set up instantly. However, if you pay using a credit card, it might take up to a few hours.'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Is cPanel included with my Linux KVM VPS?');?>
                    <div class="answer">
                        <p><?php printf (__('cPanel comes as an add-on for %s per month. You can order a cPanel license at the checkout or at your Client Area later after the purchase.'), $cPanel_price_mo); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('What virtualization do you use for Linux KVM VPS hosting?');?>
                    <div class="answer">
                        <p><?php printf (__('Linux KVM VPS hosting services are powered by %sKVM virtualization%s.'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1341/0/what-is-the-difference-between-openvz--kvm">', '</a>' );?><br>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Can I scale my Linux KVM VPS?');?>
                    <div class="answer">
                        <p><?php printf (__('You can %supgrade%s your Linux KVM VPS anytime at your Client Area by either switching to a higher plan or adding particular resources. Note that downgrades are also available for Linux KVM VPS hosting services.'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1241/0/how-to-upgrade-my-vps">', '</a>');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Can I install another Linux distributions or only the templates that are available?');?>
                    <div class="answer">
                        <p><?php _e('On Linux KVM VPS server, you can choose to install pre-configured Linux distributions that we provide, or choose an empty template and install your Linux distribution after you complete your purchase.'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('How does longer billing cycle discounts work?');?>
                    <div class="answer">
                        <p><?php printf (__('Opting in for a longer billing cycle will give you an increased discount on the base month price before taxes. Read more information on %sBilling Cycle discounts%s.'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1352/0/how-does-the-billing-cycle-discount-work">', '</a>');?></p>
                    </div>
                </div>
                

            </div>

            

            </div>
        </div>
    </div> <!-- end of .faq3 -->
