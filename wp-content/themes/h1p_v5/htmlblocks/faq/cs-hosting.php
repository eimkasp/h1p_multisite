<?php

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

// Check these IDs !!!
$addons_extracare_1level = $whmcs->getAddonsPrices(27);
$addons_extracare_2level = $whmcs->getAddonsPrices(28);

// print_r ($addons_extracare_1level);
// print_r ($addons_extracare_2level);

//Get Cloud IP config option pricing
// id=342

$cloud_hosting_config_options = $whmcs->getConfigurablePrices(342);
$cloud_hosting_ip = $cloud_hosting_config_options['IP']['options']['IP']['pricing']['monthly'];


function get_addon_billing_cycle ( $addon_set ) {
    switch ( $addon_set['billingcycle'] ) {
        case 'Free':
            return 'free';
            break;
        case 'One Time':
            return '/hour';
            break;
        case 'Monthly':
            return '/month';
            break;
        case 'Quarterly':
            return '/quarter';
            break;
        case 'Semi-Annually':
            return '/half-year';
            break;
        case 'Annually':
            return '/year';
            break;
        case 'Biennially':
            return '/biennially';
            break;
        case 'Triennially':
            return '/triennially';
            break;

        default:
            return false;
            break;
    }
}
?>

    <div class="faq3">
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq"><?php _e('FAQ');?></h3>
            <h3 class="tab" data-tab-index="tutorials"><?php _e('Tutorials');?></h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    <?php _e('What OS distributions you provide for Cloud Servers?');?>
                    <div class="answer">
                        <p><?php _e('You can choose from Windows Server (2012 Standard, 2016 Standard) or Linux distributions (CentOS, Debian, Fedora and Ubuntu).'); ?></p>
                        <p><?php printf (__('Detailed list of templates can be found %shere%s.'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1340/114/what-os-distributions-you-provide-for-cloud-servers">', '</a>'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('What is the difference between OpenVZ & KVM hypervisors?');?>
                    <div class="answer">
                        <p><?php _e('OpenVZ offers simplified management options, while KVM provides significantly higher performance, scalability, increased control and the choice between Windows and Linux operating systems.'); ?></p>
                        <p><?php printf (__('To get detailed information of both hypervisors characteristics, click %shere%s.'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1341/114/what-is-the-difference-between-openvz--kvm">', '</a>'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Do you provide IPv6 addresses?');?>
                    <div class="answer">
                        <p><?php _e('Your Cloud Server is already equipped with /64 IPv6 addresses with no extra fees.');?><br>
                    </div>
                </div>
                <div class="question">
                    <?php _e('How do I add IPv4 addresses to my Cloud Server?');?>
                    <div class="answer">
                        <p><?php _e('All plans include 1 IPv4 address free of charge.');?></p>
                        <p><?php printf(__('In order to add additional IPv4 addresses, log in to your Client Area and go to your Cloud Server control panel, click on Network tab where you will find Add IPs option. Please note that additional IPv4 addresses are %s each.'), $whmcs::$settings['currency_prefix'] . $cloud_hosting_ip);?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('What payment method can I use?');?>
                    <div class="answer">
                        <p><?php _e('We accept Credit Card, Paypal, authorized PayPal payments, Alipay, Bitcoin, Ebanx and Paysera payments. WebMoney and CashU payments are also available via Paysera platform.');?></p>
                        <p><?php printf (__('Please note that payment options vary depending on your location. Additional information can be found %shere%s.'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1312/0/what-payment-method-can-i-use">', '</a>'); ?></p>
                    </div>
                </div>

                <div class="center p-v-30"><a target="_blank" href="<?php echo $config['links']['knowledge_base']; ?>"><?php _e('View more'); ?></a> <i class="fa fa-external-link"></i></div>

            </div>

            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/cloud-hosting-services/how-to-install-cygwin-on-windows-for-linux-tools">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to Install Cygwin on Windows for Linux Tools');?></a> <i class="fa fa-external-link"></i>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/cloud-hosting-services/increase-cloud-servers-performance">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to increase your Cloud Servers Performance');?></a> <i class="fa fa-external-link"></i>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/cloud-hosting-services/connect-windows-server-using-rdp">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to Connect to a Windows Server Using RDP');?></a> <i class="fa fa-external-link"></i>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/cloud-hosting-services/how-to-set-up-openvpn-on-linux-cloud-server">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to Set up OpenVPN on Linux Cloud Server');?></a> <i class="fa fa-external-link"></i>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/vps-hosting-services/logs-on-linux-server">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('Logs on Linux Server');?></a> <i class="fa fa-external-link"></i>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/cloud-hosting-services/how-to-assign-additional-ip-in-debian">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to Assign Additional IP Addresses in Debian');?></a> <i class="fa fa-external-link"></i>
                        </div>
                    </div>
                </div>

                <div class="center p-v-30"><a target="_blank" href="<?php echo $config['links']['tutorials']; ?>"><?php _e('View more'); ?></a> <i class="fa fa-external-link"></i></div>

            </div>
        </div>
    </div> <!-- end of .faq3 -->
