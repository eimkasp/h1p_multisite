<?php

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

/*cPanel price. cPanel is a product with id=129*/
$cPanel_data = $whmcs->getConfigurablePrice(129,[]);
/*echo '<pre>';
var_dump($cPanel_data);
echo '</pre>';*/
$cPanel_price_mo = $cPanel_data[0]['price_monthly'];

?>

    <div class="faq3">
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq"><?php _e('FAQ');?></h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    What is the difference between OpenVZ & KVM hypervisors?
                    <div class="answer">
                        <p>OpenVZ offers simplified management options, on the other hand KVM provides significantly higher performance, scalability, increased control and customization.</p>
                        <p>To get detailed information of both hypervisors' characteristics, click <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1341/114/what-is-the-difference-between-openvz--kvm">here</a>.</p>
                    </div>
                </div>
                <div class="question">
                    What payment method can I use to buy Linux server?
                    <div class="answer">
                        <p>We accept Credit Card, Paypal, authorized PayPal payments, Alipay, Bitcoin, and other popular payment gateways.</p>
                        <p>Please note that payment options vary depending on your location. Additional information can be found <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1312/0/what-payment-method-can-i-use">here</a>.</p>
                    </div>
                </div>
                <div class="question">
                    How long will my Linux server setup take?
                    <div class="answer">
                        <p>If you decide to order a Linux server it is usually set up instantly. However, if you pay using a credit card, on rare occasions it might take up to a few hours.</p>
                    </div>
                </div>
                <div class="question">
                    Do you provide a free Linux server trial?
                    <div class="answer">
                        <p>At the moment we are not providing free trials for testing out our service, but you can test our services for 14 days and cancel them hassle-free if we do not meet your project needs.</p>
                    </div>
                </div>
                <div class="question">
                    Help me get around my Linux server!
                    <div class="answer">
                        <p>If you have never tried Linux server — do not worry! We have plenty of <a target="_blank" href="https://www.host1plus.com/tutorials/">tutorials</a> to help you out getting started with your Linux server. Step by step guides from basics such as Linux terminal to advanced installations and more!</p>
                    </div>
                </div>
                

            </div>

            

            </div>
        </div>
    </div> <!-- end of .faq3 -->
