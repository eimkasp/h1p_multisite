<?php

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

// Check these IDs !!!
$addons_extracare_1level = $whmcs->getAddonsPrices(27);
$addons_extracare_2level = $whmcs->getAddonsPrices(28);

// print_r ($addons_extracare_1level);
// print_r ($addons_extracare_2level);

//Get Cloud IP config option pricing
// id=342

$cloud_hosting_config_options = $whmcs->getConfigurablePrices(342);
$cloud_hosting_ip = $cloud_hosting_config_options['IP']['options']['IP']['pricing']['monthly'];

/*Windows licence addon pricing*/
$addons_winlic = $whmcs->getAddonsPrices(33);
$cloud_hosting_winlic =  $addons_winlic['price']; // it is monthly!


function get_addon_billing_cycle ( $addon_set ) {
    switch ( $addon_set['billingcycle'] ) {
        case 'Free':
            return 'free';
            break;
        case 'One Time':
            return '/hour';
            break;
        case 'Monthly':
            return '/month';
            break;
        case 'Quarterly':
            return '/quarter';
            break;
        case 'Semi-Annually':
            return '/half-year';
            break;
        case 'Annually':
            return '/year';
            break;
        case 'Biennially':
            return '/biennially';
            break;
        case 'Triennially':
            return '/triennially';
            break;

        default:
            return false;
            break;
    }
}
?>

    <div class="faq3">
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq"><?php _e('FAQ');?></h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    <?php _e('What is Windows KVM VPS?');?>
                    <div class="answer">
                        <p><?php printf(__('A %sWindows KVM VPS%s is a server which utilizes KVM virtualization, meaning that resources are dedicated to the KVM VPS and are not shared with other users, offering tremendous reliability, isolation, performance and security.'), '<a href="/kvm-vps-hosting/">', '</a>'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('What kind of OS will my Windows VPS run on?');?>
                    <div class="answer">
                        <p><?php printf (__('You can deploy your personal Windows VPS with a pre-installed Windows Server (2012 Standard, 2016 Standard) license. The license comes with all Windows VPS plans and costs %s/mo. Please note that Windows VPS pricing does not include the license fee.'), $whmcs::$settings['currency_prefix'] . $cloud_hosting_winlic ); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('What payment method can I use?');?>
                    <div class="answer">
                        <p><?php _e('We accept Credit Card, Paypal, authorized PayPal payments, Alipay, Bitcoin, and other popular payment gateways.');?></p>
                        <p><?php printf (__('Please note that payment options vary depending on your location. Additional information can be found %shere%s.'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1312/0/what-payment-method-can-i-use">', '</a>'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Can I downgrade my Windows VPS?');?>
                    <div class="answer">
                        <p><?php _e('Yes! Our scalable Windows VPS servers can be configured by your preference. You can either boost your resources or downgrade. Please note that after you downgrade your service price will be recalculated and lowered immediately, but the money is not returned to your Credit Balance.');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Do you provide a money-back guarantee?');?>
                    <div class="answer">
                        <p><?php printf (__('Of course! You can check out if our Windows VPS services meet your needs for a 14-day period. If you do not like it, you can request a refund by contacting us at %ssupport@host1plus.com%s or by submitting a ticket at your Client Area.'), '<a href="mailto:support@host1plus.com">', '</a>');?></p>
                    </div>
                </div>
                

            </div>

            

            </div>
        </div>
    </div> <!-- end of .faq3 -->
