<?php global $config; ?>

    <div class="faq3">
        
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    <?php _e('What does it mean to be a white-label reseller?');?>
                    <div class="answer">
                        <p><?php _e('When you sell white-label products or services, the name of the company that produces those services is replaced with your company name. So the only name your customers will ever see is yours.');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Is there someone who can assist me if I have sales or other related questions?');?>
                    <div class="answer">
                        <p><?php _e('Of course! Each reseller is assigned a personal manager who is available to discuss sales training, product overviews, and general questions.');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Who determines the pricing on my reseller store?');?>
                    <div class="answer">
                        <p><?php _e('Only you. You can set the pricing for products and services individually, offer special sales and promotions or discount pricing.');?><br>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Is there a fee to become a Host1Plus reseller partner?');?>
                    <div class="answer">
                        <p><?php _e('No. The program is currently free to apply. Please fill out the registration form and an assigned personal manager will contact you.');?></p>
                    </div>
                </div>

            </div>

        </div>
    </div> <!-- end of .faq3 -->
