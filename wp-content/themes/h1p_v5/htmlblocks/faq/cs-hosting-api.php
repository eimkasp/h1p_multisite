<?php
global $config;

?>

    <div class="faq3">
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq"><?php _e('FAQ');?></h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container ">

        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    <?php _e('How do I get the API access token?');?>
                    <div class="answer">
                        <p><?php _e('API access tokens are automatically generated for clients with a Cloud service.');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('When will I be able to use the API token?');?>
                    <div class="answer">
                        <p><?php _e('You will be able to use your API access token upon purchasing a Cloud service.');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('How to use the API?');?>
                    <div class="answer">
                        <p><?php _e('You can pick any coding or command line language of your choice. The only requirement is that it has to support custom HTTP requests. More in-depth information on API utilization can be found in our documentation.');?><br>
                    </div>
                </div>
                <div class="question">
                    <?php _e('What are the capabilities of the API?');?>
                    <div class="answer">
                        <p><?php _e('Our API allows you to manage services and resources within your Cloud Server in a simple, programmatic way using HTTP requests. The vast majority of the functionality of our Client Area is also available through the API.');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('How to scale my Cloud Server?');?>
                    <div class="answer">
                        <p><?php printf(__('You can view and add or remove your resources in the Client Area > Cloud Servers > Upgrade tab. Several limitations apply. %sRead more%s'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1346/114/how-to-scale-my-cloud-server">', '</a>');?></p>
                    </div>
                </div>

                <div class="center p-v-30"><a target="_blank" href="<?php echo $config['links']['knowledge_base']; ?>"><?php _e('View more'); ?></a> <i class="fa fa-external-link"></i></div>

            </div>

        </div>
    </div> <!-- end of .faq3 -->
