<section class="block faq">

    <div class="container">
        <div class="row">
            <h2 class="block-title"><?php _e('Frequently Asked Questions')?></h2>
            <div class="block-content">
                <div class="faq-table">
                    <div class="faq-row">
                        <div class="faq-col mob">
                            <div class="content">
                                <div class="title"><?php _e('Is SEO hosting like the typical hosting services?')?></div>
                                <div class="text"><?php _e('Yes! SEO is exactly like regular hosting services. However, this service comes with additional benefits – you can host your websites on multiple dedicated IPs, thus reaching better ranking and better results.')?></div>
                            </div>
                        </div>
                        <div class="faq-col space"></div>
                        <div class="faq-col">
                            <div class="content">
                                <div class="title"><?php _e('How long does it take to activate my server?')?></div>
                                <div class="text"><?php _e('All new orders are approved by our billing department and may require additional details for the order to be processed. Usually it takes up to 12 hours or less.')?></div>
                            </div>
                        </div>
                    </div>
                    <div class="faq-row">
                        <div class="faq-col mob">
                            <div class="content">
                                <div class="title"><?php _e('What class IPs will I get?')?></div>
                                <div class="text"><?php _e('You may choose from different C class IP\'s.')?></div>
                            </div>
                        </div>
                        <div class="faq-col space"></div>
                        <div class="faq-col">
                            <div class="content">
                                <div class="title"><?php _e('What OS can I have on SEO hosting plans?')?></div>
                                <div class="text"><?php _e('For VPS SEO hosting we provide multiple Linux OS distributions, including Ubuntu, Fedora, Debian, CentOS and SUSE.')?></div>
                            </div>
                        </div>
                    </div>
                    <div class="faq-row">
                        <div class="faq-col mob">
                            <div class="content">
                                <div class="title"><?php _e('How to choose a SEO hosting plan?')?></div>
                                <div class="text"><?php _e('When choosing a SEO hosting plan, you need to decide on the amount of resources and dedicated IPs you require. Any VPS SEO hosting plan can be upgraded if your preferences change. To change your plan, contact your personal SEO hosting manager.')?></div>
                            </div>
                        </div>
                        <div class="faq-col space"></div>
                        <div class="faq-col">
                            <div class="content">
                                <div class="title"><?php _e('Can I add more resources on the same server?')?></div>
                                <div class="text"><?php _e('Yes, you can upgrade your VPS SEO server if you are running low on resources. Your virtual machine will be upgraded while runing so no downtime will affect it. To upgrade you service, please contact you personal SEO hosting manager.')?></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</section>
