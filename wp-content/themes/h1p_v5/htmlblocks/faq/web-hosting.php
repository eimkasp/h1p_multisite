<?php global $config; ?>

    <div class="faq3">
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq"><?php _e('FAQ');?></h3>
            <h3 class="tab" data-tab-index="tutorials"><?php _e('Tutorials');?></h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    <?php _e('How long will it take to set up my web hosting account?'); ?>
                    <div class="answer">
                        <p><?php _e('After you order and pay for your web hosting service, your account will be set up immediately.'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Can I upgrade my web hosting plan?');?>
                    <div class="answer">
                        <p><?php _e('You can upgrade your service to another web hosting plan with more resources at the Client Area.'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('How can I buy a dedicated IP for my web hosting service?'); ?>
                    <div class="answer">
                        <p><?php _e('If you are using Business Pro plan, a dedicated IP is already included. However, if you have signed up for Personal or Business plan or you need an additional dedicated IP, you can add more resources at your Client Area.'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('What versions of PHP do you offer?'); ?>
                    <div class="answer">
                        <p><?php _e('We currently have 5 PHP versions – 4.4, 5.2, 5.3, 5.4, 5.5 and 7.'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('What databases do you support?'); ?>
                    <div class="answer">
                        <p><?php _e('Our web hosting service supports both MySQL and PostgreSQL databases.'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Can I buy a domain together with web hosting?');?>
                    <div class="answer">
                        <p><?php _e('You can purchase a new domain together with your hosting service or order later directly at your Client Area. Note that we provide a free domain for all annual web hosting plans.'); ?></p>
                    </div>
                </div>

                <div class="center p-v-30"><a target="_blank" href="<?php echo $config['links']['knowledge_base']; ?>"><?php _e('View more'); ?></a> <i class="fa fa-external-link"></i></div>

            </div>

            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/cms-tutorials/wordpress/wordpress-transfer/how-to-move-site-on-same-server-but-different-domain">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to move a site on the same server but different domain?'); ?></a> <i class="fa fa-external-link"></i>

                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/cms-tutorials/drupal/drupal-installation/manual-drupal-installation">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to install Drupal manually?');?></a> <i class="fa fa-external-link"></i>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/web-hosting-services/all-web-hosting-tutorials/how-to-create-a-subdomain-cpanel">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to create a subdomain in cPanel');?></a> <i class="fa fa-external-link"></i>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/web-hosting-services/all-web-hosting-tutorials/setting-dns-in-cpanel">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to set DNS using cPanel');?></a> <i class="fa fa-external-link"></i>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/hosting-services/web-hosting-services/all-web-hosting-tutorials/how-to-add-domain-in-directadmin">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to add a domain in DirectAdmin');?></a> <i class="fa fa-external-link"></i>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/cms-tutorials/wordpress/wordpress-installation/how-to-run-wordpress-installation-script">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to run WordPress installation script?');?></a> <i class="fa fa-external-link"></i>
                        </div>
                    </div>
                </div>

                <div class="center p-v-30"><a target="_blank" href="<?php echo $config['links']['tutorials']; ?>"><?php _e('View more'); ?></a> <i class="fa fa-external-link"></i></div>

            </div>
        </div>
    </div> <!-- end of .faq3 -->
