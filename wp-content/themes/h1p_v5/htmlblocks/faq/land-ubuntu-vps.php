<?php

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

/*cPanel price. cPanel is a product with id=129*/
$cPanel_data = $whmcs->getConfigurablePrice(129,[]);
/*echo '<pre>';
var_dump($cPanel_data);
echo '</pre>';*/
$cPanel_price_mo = $cPanel_data[0]['price_monthly'];

?>

    <div class="faq3">
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq"><?php _e('FAQ');?></h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    <?php _e('Which Ubuntu distributions do you offer?');?>
                    <div class="answer">
                        <p><?php _e('While ordering your Ubuntu VPS hosting services, you can choose between 16.04 x86_64, 15.10 x86_64, 14.04 x86_64 and 14.04 x86.'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('How long will my VPS server setup take?');?>
                    <div class="answer">
                        <p><?php _e('If you decide to order a VPS server it is usually set up instantly. However, if you pay using a credit card, on rare occasions it might take up to a few hours.'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Can I upgrade my Ubuntu VPS?');?>
                    <div class="answer">
                        <p><?php printf(__('You can %supgrade%s your VPS anytime at your Client Area by either switching to a higher plan oradding particular resources. Note that downgrades are currently not available for VPS hosting services.'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1241/0/how-to-upgrade-my-vps">', '</a>'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Do you offer a trial?');?>
                    <div class="answer">
                        <p><?php _e('At the moment we are not providing free trials to test out our service, but you can test our services for 14 days and cancel them hassle-free if we do not meet your project needs.'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('What virtualization do you use for Linux Ubuntu VPS servers?');?>
                    <div class="answer">
                        <p><?php printf (__('VPS hosting services are powered by %sOpenVZ virtualization%s.'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1341/0/what-is-the-difference-between-openvz--kvm">', '</a>' );?><br>
                    </div>
                </div>
                <div class="question">
                    <?php _e('How do longer billing cycle discounts work?');?>
                    <div class="answer">
                        <p><?php printf (__('Opting in for a longer billing cycle will give you an increased discount on the base month price before taxes. Read more information on %sBilling Cycle discounts%s.'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1352/0/how-does-the-billing-cycle-discount-work">', '</a>'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Can I change my OS after the purchase?');?>
                    <div class="answer">
                        <p><?php printf (__('You can change to 30+ other %savailable Linux OS templates%s anytime during your service period.'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1240/0/how-to-reinstall-an-operating-system-on-my-vps">', '</a>');?></p>
                    </div>
                </div>
                

            </div>

            

            </div>
        </div>
    </div> <!-- end of .faq3 -->
