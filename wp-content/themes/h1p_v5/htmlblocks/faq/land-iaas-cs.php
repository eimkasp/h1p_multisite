<?php

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

// Check these IDs !!!
$addons_extracare_1level = $whmcs->getAddonsPrices(27);
$addons_extracare_2level = $whmcs->getAddonsPrices(28);

// print_r ($addons_extracare_1level);
// print_r ($addons_extracare_2level);

//Get Cloud IP config option pricing
// id=342

$cloud_hosting_config_options = $whmcs->getConfigurablePrices(342);
$cloud_hosting_ip = $cloud_hosting_config_options['IP']['options']['IP']['pricing']['monthly'];

/*Windows licence addon pricing*/
$addons_winlic = $whmcs->getAddonsPrices(33);
$cloud_hosting_winlic =  $addons_winlic['price']; // it is monthly!


function get_addon_billing_cycle ( $addon_set ) {
    switch ( $addon_set['billingcycle'] ) {
        case 'Free':
            return 'free';
            break;
        case 'One Time':
            return '/hour';
            break;
        case 'Monthly':
            return '/month';
            break;
        case 'Quarterly':
            return '/quarter';
            break;
        case 'Semi-Annually':
            return '/half-year';
            break;
        case 'Annually':
            return '/year';
            break;
        case 'Biennially':
            return '/biennially';
            break;
        case 'Triennially':
            return '/triennially';
            break;

        default:
            return false;
            break;
    }
}
?>

    <div class="faq3">
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq"><?php _e('FAQ');?></h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    What kind of OS will my IaaS Cloud instance run on?
                    <div class="answer">
                        <p>You can choose from a <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1340/114/what-os-distributions-you-provide-for-cloud-servers">wide range of Linux distributions</a> - CentOS, Fedora, Debian and Ubuntu, or you can order an empty virtual machine to upload a template of your choice. If you require pre-installed Windows, you can deploy your IaaS Cloud instance with a Windows Server (2012 Standard, 2016 Standard) license. The license comes with all IaaS Cloud Windows plans and costs <?php echo $whmcs::$settings['currency_prefix'] . $cloud_hosting_winlic; ?>/mo. Please note that Windows Server pricing table does not include the fee.</p>
                    </div>
                </div>
                <div class="question">
                    I have my own Windows license, do I still have to buy one?
                    <div class="answer">
                        <p>If you have a Windows license already, you can order an empty virtual machine. Just go to <a target="_blank" href="<?php echo $config['links']['cloud-servers'].'#linux';?>">IaaS Cloud Linux plans</a>, click to go to the checkout and while you are choosing your OS template, simply select None. After the purchase, log in to your Client Area and upload your OS at your Cloud Server management panel.</p>
                    </div>
                </div>
                <div class="question">
                    Can I downgrade my IaaS Cloud instance?
                    <div class="answer">
                        <p>Yes! Our scalable IaaS Cloud isntances can be <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1346/0/how-to-scale-my-cloud-server">configured</a> by your preference. You can either boost your resources or downgrade. Please note that after you downgrade your service the money always comes back to your Credit Balance. You can use the amount to upgrade, order new services or add-ons to your existing service.</p>
                    </div>
                </div>
                <div class="question">
                    Do you provide a money-back guarantee?
                    <div class="answer">
                        <p>Of course! You can check out if our IaaS Cloud services meet your needs for a 14-day period. If you do not like it, you can request a refund by contacting us at <a href="mailto:support@host1plus.com">support@host1plus.com</a> or by submitting a ticket at your Client Area.</p>
                    </div>
                </div>
                

            </div>

            

            </div>
        </div>
    </div> <!-- end of .faq3 -->
