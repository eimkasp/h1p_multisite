<?php 

global $config; 
if (get_locale() == 'pt_BR') {
    $tuntappp_link = 'https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1361/0/posso-utilizar-os-modulos-ppptuntap-em-meu-vps/';
} else {
    $tuntappp_link = 'https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1359/0/can-i-use-ppptuntap-module-on-my-vps';
}

?>

    <div class="faq3">
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq"><?php _e('FAQ');?></h3>
            <h3 class="tab" data-tab-index="tutorials"><?php _e('Tutorials');?></h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    <?php _e('How long will my VPS setup take?');?>
                    <div class="answer">
                        <p><?php _e('After you order a VPS, the service is usually set up instantly. However, if you pay using a credit card, it might take up to a few hours.');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Can I use PPP/TUN/TAP Module on my VPS?');?>
                    <div class="answer">
                        <p><?php printf(__('Please note that VPS services do not support the %sTUN/TAP/PPP%s (Point-to-Point Tunneling Protocol) module.'), '<a target="_blank" href="' . $tuntappp_link . '">', '</a>');?></p>
                        <p><?php printf(__('New or existing clients that wish to have the PPTP module should order a %sCloud Server%s instead, which has no such limitations.'), '<a href="'. $config['links']['cloud-servers'] .'">', '</a>');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('What is the network speed of your Linux VPS hosting?');?>
                    <div class="answer">
                        <p><?php _e('We limit the network speed provided depending on the location of your VPS hosting service.');?><br>
                        <br>
                        <?php echo sprintf(_x_lc('%1$sOutgoing%2$s (data that is being sent from your server to another one) network speed is limited up to 500 Mbps in all VPS hosting locations except São Paulo, Brazil, where network speed limit is higher - up to 200 Mbps.'), '<strong>', '</strong>');?><br>
                        <br>
                        <?php echo sprintf(_x_lc('%1$sIncoming%2$s (data that is being transferred to your server from another one) network speed does not have any limitations.'), '<strong>', '</strong>');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Is it possible to install a custom OS to OpenVZ VPS?');?>
                    <div class="answer">
                        <p><?php _e('We are using only official OpenVZ templates only so it is not possible to install any custom OS.');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Which control panels can I install on my VPS?');?>
                    <div class="answer">
                        <p><?php _e('You can manually install any free open source control panel that supports Ubuntu operating system, such as Sentora, Ajenti, Vesta and many more.');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('What happens when my CPU limit is reached?');?>
                    <div class="answer">
                        <p><?php _e('If you are overusing your CPU you will receive a notification that the power of your processor is temporarily limited.');?><br>
                        <br>
                        <?php printf(__('You can review your CPU power usage on the statistics page in your Client Area (for more information click %shere%s)'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1244/108/where-can-i-find-my-vps-statistics-page">', '</a>');?><br>
                        <br>
                        <?php printf(__('If you want to avoid reaching the limit, you can upgrade your CPU in your Client Area (for more information click %shere%s)'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1241/108/how-to-upgradedowngrade-my-vps">', '</a>');?>.</p>
                    </div>
                </div>

                <div class="center p-v-30"><a target="_blank" href="<?php echo $config['links']['knowledge_base']; ?>"><?php _e('View more'); ?></a> <i class="fa fa-external-link"></i></div>

            </div>

            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/how-to-create-virtualhost-on-apache">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to create virtualhost on Apache?');?></a> <i class="fa fa-external-link"></i>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-csf-on-linux">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to install CSF on Linux?');?></a> <i class="fa fa-external-link"></i>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/introduction-to-ssh-keys">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('Introduction to SSH keys');?></a> <i class="fa fa-external-link"></i>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-install-gnome-desktop">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to install GNOME desktop?');?></a> <i class="fa fa-external-link"></i>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-change-a-time-zone-on-a-vps-server">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to change a time zone on a VPS server?');?></a> <i class="fa fa-external-link"></i>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="http://www.host1plus.com/tutorials/operating-systems/linux/administration-linux/how-to-add-ip-address-on-linux-operating-systems">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to add IP address on CentOS operating systems?');?></a> <i class="fa fa-external-link"></i>
                        </div>
                    </div>
                </div>

                <div class="center p-v-30"><a target="_blank" href="<?php echo $config['links']['tutorials']; ?>"><?php _e('View more'); ?></a> <i class="fa fa-external-link"></i></div>

            </div>
        </div>
    </div> <!-- end of .faq3 -->
