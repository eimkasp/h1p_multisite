<?php

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

// Check these IDs !!!
$addons_extracare_1level = $whmcs->getAddonsPrices(27);
$addons_extracare_2level = $whmcs->getAddonsPrices(28);

// print_r ($addons_extracare_1level);
// print_r ($addons_extracare_2level);

//Get Cloud IP config option pricing
// id=342

$cloud_hosting_config_options = $whmcs->getConfigurablePrices(342);
$cloud_hosting_ip = $cloud_hosting_config_options['IP']['options']['IP']['pricing']['monthly'];

/*Windows licence addon pricing*/
$addons_winlic = $whmcs->getAddonsPrices(33);
$cloud_hosting_winlic =  $addons_winlic['price']; // it is monthly!


function get_addon_billing_cycle ( $addon_set ) {
    switch ( $addon_set['billingcycle'] ) {
        case 'Free':
            return 'free';
            break;
        case 'One Time':
            return '/hour';
            break;
        case 'Monthly':
            return '/month';
            break;
        case 'Quarterly':
            return '/quarter';
            break;
        case 'Semi-Annually':
            return '/half-year';
            break;
        case 'Annually':
            return '/year';
            break;
        case 'Biennially':
            return '/biennially';
            break;
        case 'Triennially':
            return '/triennially';
            break;

        default:
            return false;
            break;
    }
}
?>

    <div class="faq3">
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq"><?php _e('FAQ');?></h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    <?php _e('What kind of OS will my Windows server run on?');?>
                    <div class="answer">
                        <p><?php printf (__('You can deploy your personal Cloud Server with a pre-installed Windows Server (2012 Standard, 2016 Standard) license. The license comes with all Windows Cloud Server plans and costs %s/mo. Please note that Windows Server pricing does not include the fee.'), $whmcs::$settings['currency_prefix'] . $cloud_hosting_winlic ); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('I have my own Windows license, do I still have to buy one?');?>
                    <div class="answer">
                        <p><?php printf (__('If you have a Windows license already, you can order an empty virtual machine. Just go to %sLinux Cloud Server%s plans, click to go to the checkout and while you are choosing your OS template, simply select None. After the purchase, log in to your Client Area and upload your OS at your Cloud Server management panel.'), '<a target="_blank" href="'.$config['links']['cloud-servers'].'#linux">', '</a>'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Can I downgrade my Windows server?');?>
                    <div class="answer">
                        <p><?php _e('Yes! Our scalable Windows Cloud Servers can be configured by your preference. You can either boost your resources or downgrade. Please note that after you downgrade your service price will be recalculated and lowered immediately, but the money is not returned to your Credit Balance.');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('What locations are available for Windows Cloud Servers?');?>
                    <div class="answer">
                        <p><?php printf (__('Currently you can choose from two locations - %sFrankfurt, Germany%s and %sChicago, US%s.'), '<a href="/cloud-germany/">', '</a>', '<a href="/cloud-servers/#windows">', '</a>');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Do you provide a money-back guarantee?');?>
                    <div class="answer">
                        <p><?php printf (__('Of course! You can check out if our Cloud services meet your needs for a 14-day period. If you do not like it, you can request a refund by contacting us at %ssupport@host1plus.com%s or by submitting a ticket at your Client Area.'), '<a href="mailto:support@host1plus.com">', '</a>');?></p>
                    </div>
                </div>
                

            </div>

            

            </div>
        </div>
    </div> <!-- end of .faq3 -->
