<?php

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

/*cPanel price. cPanel is a product with id=129*/
$cPanel_data = $whmcs->getConfigurablePrice(129,[]);
/*echo '<pre>';
var_dump($cPanel_data);
echo '</pre>';*/
$cPanel_price_mo = $cPanel_data[0]['price_monthly'];

?>

    <div class="faq3">
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq"><?php _e('FAQ');?></h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    <?php _e('How long will my cheap VPS setup take?');?>
                    <div class="answer">
                        <p><?php _e('After you order a VPS, the service is usually set up instantly. However, if you pay using a credit card, it might take up to a few hours.'); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Is cPanel included with my VPS?');?>
                    <div class="answer">
                        <p><?php printf (__('cPanel comes as an add-on for %s per month. You can order a cPanel license at the checkout or at your Client Area later after the purchase.'), $cPanel_price_mo); ?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('What virtualization do you use for cheap VPS hosting?');?>
                    <div class="answer">
                        <p><?php printf (__('VPS hosting services are powered by %sOpenVZ virtualization%s.'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1341/0/what-is-the-difference-between-openvz--kvm">', '</a>' );?><br>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Can I upgrade my VPS?');?>
                    <div class="answer">
                        <p><?php printf (__('You can %supgrade%s your VPS anytime at your Client Area by either switching to a higher plan or adding particular resources. Note that downgrades are currently not available for VPS hosting services.'), '<a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1241/0/how-to-upgrade-my-vps">', '</a>');?></p>
                    </div>
                </div>
                

            </div>

            

            </div>
        </div>
    </div> <!-- end of .faq3 -->
