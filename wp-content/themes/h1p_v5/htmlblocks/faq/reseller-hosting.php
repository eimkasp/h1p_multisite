<?php global $config; ?>

    <div class="faq3">
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq"><?php _e('FAQ');?></h3>
            <h3 class="tab" data-tab-index="tutorials"><?php _e('Tutorials');?></h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    <?php _e('How long will my reseller account setup take?');?>
                    <div class="answer">
                        <p><?php _e('All reseller hosting accounts are set up immediately after your purchase. However, if you pay using a credit card, it might take up to a few hours for us to proceed your payment.');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Will my IP address be the same as my reseller hosting clients?');?>
                    <div class="answer">
                        <p><?php _e('When you purchase a reseller hosting plan you will get an IP address that will remain the same for your clients as well.');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Can I host CDN on a web hosting server?');?>
                    <div class="answer">
                        <p><?php printf(__('Web and reseller hosting services are meant for standard web hosting use only. Thus the use for file sharing, torrents, streaming and proxies is not allowed. To find out more, please read our %sTerms of Service%s.'), '<a target="_blank" href="https://www.host1plus.com/terms-of-service/">', '</a>');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Do you allow overselling for reseller hosting?');?>
                    <div class="answer">
                        <p><?php _e('No, we do not allow overselling. If our customers overuse the resources we offer, reseller hosting plans might become unsustainable.');?></p>
                    </div>
                </div>
                <div class="question">
                    <?php _e('Is the WHM transfer tool available for my reseller hosting customers?');?>
                    <div class="answer">
                        <p><?php printf(__('No, WHM transfer tool is not available, all accounts (up to 30) can be transferred by our support team only (see Section XI). To find out more, please read our %sTerms of Service%s.'), '<a target="_blank" href="https://www.host1plus.com/terms-of-service/">', '</a>');?></p>
                    </div>
                </div>

                <div class="center p-v-30"><a target="_blank" href="<?php echo $config['links']['knowledge_base']; ?>"><?php _e('View more'); ?></a> <i class="fa fa-external-link"></i></div>

            </div>

            <div class="tab-content" data-tab-content="tutorials">
                <div class="tutorial-list">
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="https://www.host1plus.com/tutorials/control-panels/directadmin/all-directadmin-tutorials/importing-cpanel-user-to-directadmin">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to import cPanel user to DirectAdmin?');?></a> <i class="fa fa-external-link"></i>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="https://www.host1plus.com/tutorials/control-panels/cpanel/all-cpanel-tutorials/how-to-create-ftp-account-in-cpanel">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to create an FTP account using cPanel?');?></a> <i class="fa fa-external-link"></i>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="https://www.host1plus.com/tutorials/hosting-services/web-hosting-services/all-web-hosting-tutorials/how-to-create-a-subdomain-cpanel">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to create a subdomain using cPanel?');?></a> <i class="fa fa-external-link"></i>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="https://www.host1plus.com/tutorials/hosting-services/web-hosting-services/all-web-hosting-tutorials/how-to-import-database">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to import a database?');?></a> <i class="fa fa-external-link"></i>
                        </div>
                    </div>
                    <div class="tutorial-row">
                        <div class="tutorial-item">
                            <a target="_blank" href="https://www.host1plus.com/tutorials/hosting-services/web-hosting-services/all-web-hosting-tutorials/cpanel-dns-management">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to manage DNS using cPanel?');?></a> <i class="fa fa-external-link"></i>
                        </div>
                        <div class="tutorial-item">
                            <a target="_blank" href="https://www.host1plus.com/tutorials/cms-tutorials/wordpress/wordpress-transfer/how-to-move-site-on-same-server-but-different-domain">
                            <i class="fa fa-file-text-o file-ico"></i> <?php _e('How to move a site on the same server but different domain?');?></a> <i class="fa fa-external-link"></i>
                        </div>
                    </div>
                </div>

                <div class="center p-v-30"><a target="_blank" href="<?php echo $config['links']['tutorials']; ?>"><?php _e('View more'); ?></a> <i class="fa fa-external-link"></i></div>

            </div>
        </div>
    </div> <!-- end of .faq3 -->

