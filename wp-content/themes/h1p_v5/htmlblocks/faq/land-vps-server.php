<?php

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

/*cPanel price. cPanel is a product with id=129*/
$cPanel_data = $whmcs->getConfigurablePrice(129,[]);
/*echo '<pre>';
var_dump($cPanel_data);
echo '</pre>';*/
$cPanel_price_mo = $cPanel_data[0]['price_monthly'];

?>

    <div class="faq3">
        <div class="tabs container">
            <h3 class="tab active" data-tab-index="faq"><?php _e('FAQ');?></h3>
            <span class="tab-spacing"></span>
        </div>
        <div class="tabs-content container">
            <div class="tab-content active" data-tab-content="faq">
                <div class="question">
                    What are other shared web hosting limitations?
                    <div class="answer">
                        <p>Shared web hosting has higher security issues because everyone on the same shared physical machine can be affected by each other.</p>
                        <p>Also, hosting providers offer unlimited web hosting services as long as the user abides by the fair usage policy, but you always should take a look at them if you think your website, application or project might exceed the <a target="_blank" href="https://www.host1plus.com/terms-of-service#tos-section-7-3">Unlimited Resources Policy</a>.</p> 
                    </div>
                </div>
                <div class="question">
                    What virtualization do you use for VPS servers?
                    <div class="answer">
                        <p>VPS hosting services are powered by <a target="_blank" href="https://support.host1plus.com/index.php?/Knowledgebase/Article/View/1341/0/what-is-the-difference-between-openvz--kvm">OpenVZ virtualization</a>.<br>
                    </div>
                </div>
                <div class="question">
                    What OS do you provide with a VPS server?
                    <div class="answer">
                        <p>We offer VPS servers along with most popular pre-configured Linux distributions such as CentOS, Debian, Fedora and <a href="/vps-ubuntu/">Ubuntu</a>.</p>
                    </div>
                </div>
                <div class="question">
                    Do you provide a free VPS server trial?
                    <div class="answer">
                        <p>At the moment we are not providing free trials for testing out our service, but you can test our services for 14 days and cancel them hassle-free if we do not meet your project needs.</p>
                    </div>
                </div>
                <div class="question">
                    How long will my VPS server setup take?
                    <div class="answer">
                        <p>If you decide to order a VPS server it is usually set up instantly. However, if you pay using a credit card, on rare occasions it might take up to a few hours.</p>
                    </div>
                </div>
                <div class="question">
                    Help me get around with my VPS server!
                    <div class="answer">
                        <p>If you have never tried Linux VPS do not worry! We have plenty of <a target="_blank" href="https://www.host1plus.com/tutorials/">tutorials</a> to help you out getting started with your VPS server. Step by step guides from basics such as Linux terminal to advanced LAMP stack installations and more!</p>
                    </div>
                </div>
                

            </div>

            

            </div>
        </div>
    </div> <!-- end of .faq3 -->
