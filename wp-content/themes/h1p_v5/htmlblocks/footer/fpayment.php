<?php
// Payment icons and subfooter text.
// Mind that it is already inside containers, and that it is vied differently in responsive views
?>


	<div class="footer-payment-block footer-payment-block-adjust">
		<div class="payment-item paypal"></div>
		<div class="payment-item mastercard"></div>
		<div class="payment-item visa"></div>
		<div class="payment-item amex"></div>
		<div class="payment-item ebanx"></div>
		<div class="payment-item boleto"></div>
		<div class="payment-item alipay"></div>
		<div class="payment-item paysera"></div>
		<div class="payment-item bitcoin"></div>
  </div>
