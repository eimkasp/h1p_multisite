<?php global $config;?>
<div class="social-container">

    <div class="footer-search-block">
        <div class="search">
            <form id="footer-search" action="<?php echo $config['links']['search']?>" method="get">
                <input type="text" class="site-search-text" name="q" value="" placeholder="<?php _e('Search')?>">
                <input type="submit" class="site-search-btn icon search" value="">
            </form>
        </div>
    </div>

    <div class="footer-social-block">
        <div class="social_icons">
            <a target="_blank" href="<?php echo $config['social_links']['facebook'];?>" title="Facebook"><i class="fa fa-facebook-square"></i></a>
            <a target="_blank" href="<?php echo $config['social_links']['twitter'];?>" title="Twitter"><i class="fa fa-twitter-square"></i></a>
            <a rel="publisher" target="_blank" href="<?php echo $config['social_links']['googleplus'];?>" title="Google+"><i class="fa fa-google-plus-square"></i></a>
            <a target="_blank" href="<?php echo $config['social_links']['youtube'];?>" title="Youtube"><i class="fa fa-youtube-square"></i></a>
            <a target="_blank" href="<?php echo $config['social_links']['linkedin'];?>" title="LinkedIn"><i class="fa fa-linkedin-square"></i></a>
            <a target="_blank" href="<?php echo $config['social_links']['pinterest'];?>" title="Pinterest"><i class="fa fa-pinterest-square"></i></a>
        </div>
    </div>

</div>
