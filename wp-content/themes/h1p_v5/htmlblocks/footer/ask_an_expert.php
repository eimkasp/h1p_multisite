<?php global $config; ?>
<div class="help-container">
    <div class="footer-expert-block">

        <div class="footer-expert-block-subcontainer">
            <div class="footer-expert-block-icon-question">
                <span class="icon askanexpert"></span>
            </div>

            <div class="footer-expert-block-title">
                <span class="footer-expert-block-title-main"><?php _e('ASK AN EXPERT')?></span>
                <span class="footer-expert-block-title-seperator">|</span>
                <span class="footer-expert-block-title-sub"><?php _e('CALL-FREE')?> </span>
                <span class="footer-expert-block-title-sub-number"><?php echo $config['contacts']['support-phone']?></span>
                <div class="footer-expert-block-description">
                    <span class="footer-expert-block-description-main"><?php _e('Seek professional assistance from Host1Plus support team')?></span>
                </div>
            </div>
        </div>

    </div>

    <div class="footer-livechat-block">

        <div class="footer-livechat-block-title">
            <ul>
                <li>
                    <a id="footer-live-chat" href="#" onclick="javascript:window.open('<?php echo $config['links']['livechat']?>', '_blank', 'width=500,height=500,location=0,menubar=0,status=0,titlebar=0'); return false;">
                        <span class="fa fa-comments icon-bubbles"></span>
                        <span class="footer-livechat-block-title-main"><?php _e('Live Chat')?>:</span>
                        <span class="online"><?php _e('ONLINE')?></span>
                    </a>
                </li>
            </ul>



        </div>

    </div>
</div>
