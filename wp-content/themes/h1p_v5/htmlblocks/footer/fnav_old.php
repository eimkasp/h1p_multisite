<?php global $config;?>
<div class="footer-container">
    <div class="footer-nav-block">
        <div class="footer-nav-menu">

            <span class="footer-menu">
                <span class="footer-menu-title"><?php _e('HOSTING SERVICES')?></span>
                <div class="footer-menu-items">
                    <ul>
                        <li><a class="footer-hosting-service" href="<?php echo $config['links']['web-hosting']?>" title="<?php _e('Web Hosting')?>"><?php _e('Web Hosting')?></a></li>
                        <li><a class="footer-hosting-service" href="<?php echo $config['links']['reseller-hosting']?>" title="<?php _e('Reseller Hosting')?>"><?php _e('Reseller Hosting')?></a></li>
                        <li><a class="footer-hosting-service" href="<?php echo $config['links']['vps-hosting']?>" title="<?php _e('VPS Hosting')?>"><?php _e('VPS Hosting')?></a></li>
                        <li><a class="footer-hosting-service" href="<?php echo $config['links']['dedicated-servers']?>" title="<?php _e('Dedicated Servers')?>"><?php _e('Dedicated Servers')?></a></li>
                    </ul>
                </div>
            </span>

            <span class="footer-menu">
                <span class="footer-menu-title"><?php _e('DOMAIN NAMES')?></span>
                <div class="footer-menu-items">
                    <ul>
                        <li><a class="footer-domain-names" href="<?php echo $config['links']['domain-search']?>" title="<?php _e('Register a Domain')?>"><?php _e('Register a Domain')?></a></li>
                        <li><a class="footer-domain-names" href="<?php echo $config['links']['domain-search-transfer']?>" title="<?php _e('Domain Transfer')?>"><?php _e('Domain Transfer')?></a></li>
                    </ul>
                </div>
            </span>

            <span class="footer-menu">
                <span class="footer-menu-title"><?php _e('COMPANY')?></span>
                <div class="footer-menu-items">
                    <ul>
                        <li><a class="footer-company" href="<?php echo $config['links']['about_us']?>" title="<?php _e('About Us')?>"><?php _e('About Us')?></a></li>
                        <li><a class="footer-company" href="<?php echo $config['links']['careers']?>" title="<?php _e('Careers')?>"><?php _e('Careers')?></a></li>
                        <li><a class="footer-company" href="<?php echo $config['links']['affiliate']?>" title="<?php _e('Affiliate Program')?>"><?php _e('Affiliate Program')?></a></li>
                        <li><a class="footer-company" href="<?php echo $config['links']['terms_of_service']?>" title="<?php _e('Terms of Service')?>" target="_blank"><?php _e('Terms of Service')?></a></li>
                        <li><a class="footer-company" href="<?php echo $config['links']['sponsorship']?>" title="<?php _e('Sponsorship')?>"><?php _e('Sponsorship')?></a></li>
                    </ul>
                </div>
            </span>

            <span class="footer-menu">
                <span class="footer-menu-title"><?php _e('SUPPORT')?></span>
                <div class="footer-menu-items">
                    <ul>
                        <li><a class="footer-support" href="<?php echo $config['links']['support']?>" title=""><?php _e('Support Hub')?></a></li>
                        <?php if(get_locale() == 'en_US'): ?>
                            <li><a class="footer-support" href="<?php echo $config['links']['tutorials']?>" title="<?php _e('Tutorials')?>"><?php _e('Tutorials')?></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </span>

            <?php if(get_locale() == 'en_US'): ?>
            <span class="footer-menu">
                <span class="footer-menu-title"><?php _e('NEWS')?></span>
                <div class="footer-menu-items">
                    <ul>
                        <li><a class="footer-news" href="<?php echo $config['links']['press']?>" title="<?php _e('Press Releases')?>"><?php _e('Press Releases')?></a></li>
                        <li><a class="footer-news" href="<?php echo $config['links']['blog']?>" title="<?php _e('Blog')?>"><?php _e('Blog')?></a></li>
                    </ul>
                </div>
            </span>
            <?php endif; ?>

        </div>
    </div>
</div>
