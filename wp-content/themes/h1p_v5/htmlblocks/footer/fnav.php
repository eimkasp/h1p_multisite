<?php 

global $config;

if (get_locale() == 'en_US') {
    $new_string = 'New';
} elseif (get_locale() == 'pt_BR') {
    $new_string = 'N';
} else {
    $new_string = __('New');        // any other language
}

?>

<div class="footer-container">
    <div class="footer-nav-block">
        <div class="footer-nav-menu cols5">

            <div class="footer-menu expand-onclick">
                <span class="footer-menu-title"><?php _e('SERVICES')?></span>
                <div class="footer-menu-items">
                    <ul>
                        <li><a class="footer-hosting-service" href="<?php echo $config['links']['vps-hosting']?>" title="<?php _e('VPS Hosting')?>"><?php _e('VPS Hosting')?></a></li>
                        <li><a class="footer-hosting-service" href="<?php echo $config['links']['cloud-servers']?>" title="<?php _e('Cloud Servers')?>"><?php _e('Cloud Servers')?></a><span class="new-item-footer"><?php echo $new_string ?></span></li>
                        <li><a class="footer-hosting-service" href="<?php echo $config['links']['web-hosting']?>" title="<?php _e('Web Hosting')?>"><?php _e('Web Hosting')?></a></li>
                        <li><a class="footer-hosting-service" href="<?php echo $config['links']['reseller-hosting']?>" title="<?php _e('Reseller Hosting')?>"><?php _e('Reseller Hosting')?></a></li>
                        <li><a class="footer-hosting-service" href="<?php echo $config['links']['client_area']?>" title="<?php _e('Client Area')?>"><?php _e('Client Area')?></a></li>
                        <li><a class="footer-hosting-service" href="<?php echo $config['links']['api']?>" title="<?php _e('Cloud Servers API')?>"><?php _e('API')?></a></li>
                    </ul>
                </div>
            </div>

            <div class="footer-menu expand-onclick">
                <span class="footer-menu-title"><?php _e('SUPPORT')?></span>
                <div class="footer-menu-items">
                    <ul>
                        <li><a class="footer-support" href="<?php echo $config['links']['feature_requests']?>" title=""><?php _e('Feature Requests')?></a><span class="new-item-footer"><?php echo $new_string ?></span></li>
                        <li><a class="footer-support" href="<?php echo $config['links']['support']?>" title=""><?php _e('Support Center')?></a></li>
                        <li><a class="footer-support" href="<?php echo $config['links']['abuse']?>" title=""><?php _e('Report Abuse')?></a></li>
                        <li><a class="footer-support" href="<?php echo $config['links']['knowledge_base']?>" title=""><?php _e('Knowledge Base')?></a></li>
                        <li><a class="footer-support" href="<?php echo $config['links']['tutorials']?>" title="<?php _e('Tutorials')?>"><?php _e('Tutorials')?></a></li>
                    </ul>
                </div>
            </div>

            <div class="footer-menu expand-onclick">
                <span class="footer-menu-title"><?php _e('COMPANY')?></span>
                <div class="footer-menu-items">
                    <ul>
                        <li><a class="footer-company" href="<?php echo $config['links']['about_us']?>" title="<?php _e('About Us')?>"><?php _e('About Us')?></a></li>
                        <li><a class="footer-company" href="<?php echo $config['links']['terms_of_service']?>" title="<?php _e('Terms of Service')?>" target="_blank"><?php _e('Terms of Service')?></a></li>
                        <li><a class="footer-company" href="<?php echo $config['links']['careers']?>" title="<?php _e('Careers')?>"><?php _e('Careers')?></a></li>
                        <li><a class="footer-company" href="<?php echo $config['links']['branding']?>" title="<?php _e('Branding')?>"><?php _e('Logos & Badges')?></a></li>
                        <li><a class="footer-company" href="<?php echo $config['links']['blog']?>" title="<?php _e('Blog')?>"><?php _e('Blog')?></a></li>
                    </ul>
                </div>
            </div>

            <div class="footer-menu expand-onclick">
                <span class="footer-menu-title"><?php _e('OTHER')?></span>
                <div class="footer-menu-items">
                    <ul>
                        <?php if(get_locale() == 'en_US'): ?>

                            <li><a class="footer-other" href="<?php echo $config['links']['reviews']?>" title="<?php _e('Customer Reviews')?>"><?php _e('Customer Reviews')?></a></li>

                        <?php endif; ?>
                        <li><a class="footer-other" href="<?php echo $config['links']['affiliate']?>" title="<?php _e('Affiliate Program')?>"><?php _e('Affiliate Program')?></a></li>
                        <?php if (get_locale() == 'en_US') { ?>
                        
                            <li><a class="footer-other" href="<?php echo $config['links']['reseller_program']?>" title="<?php _e('Reseller Program')?>"><?php _e('Reseller Program')?></a></li>
                            
                        <?php } ?>
                        <li><a class="footer-other" href="<?php echo $config['links']['sponsorship']?>" title="<?php _e('Sponsorship')?>"><?php _e('Sponsorship')?></a></li>
                        <li><a class="footer-support" href="<?php echo $config['links']['data_centers']?>" title=""><?php _e('Speed Test')?></a></li>
                        <li><a class="footer-other" href="<?php echo $config['links']['links']?>" title="<?php _e('Links')?>"><?php _e('Links')?></a></li>
                    </ul>
                </div>
            </div>

            <div class="footer-menu expand-onclick last-col">
                <span class="footer-menu-title"><?php _e('FOLLOW US')?></span>
                <div class="footer-menu-items">
                    <ul>
                        <li><a class="footer-social" target="_blank" href="<?php echo $config['social_links']['facebook'];?>" title="Facebook"><i class="fa fa-facebook"></i> Facebook</a></li>
                        <li><a class="footer-social" target="_blank" href="<?php echo $config['social_links']['twitter'];?>" title="Twitter"><i class="fa fa-twitter"></i> Twitter</a></li>
                        <li><a class="footer-social" rel="publisher" target="_blank" href="<?php echo $config['social_links']['googleplus'];?>" title="Google+"><i class="fa fa-google-plus"></i> Google+</a></li>
                        <li><a class="footer-social" target="_blank" href="<?php echo $config['social_links']['linkedin'];?>" title="LinkedIn"><i class="fa fa-linkedin"></i> LinkedIn</a></li>
                    </ul>
                </div>
            </div>


        </div> <!-- end of .footer-nav-menu cols5 -->
    </div>  <!-- end of .footer-nav-block -->
</div>  <!-- end of .footer-container -->


<div class="footer-container">

	<div class="footer-social-block footer-social-block-adjust">
        <div class="social_icons">
            <a target="_blank" href="<?php echo $config['social_links']['facebook'];?>" title="Facebook"><i class="fa fa-facebook-square"></i></a>
            <a target="_blank" href="<?php echo $config['social_links']['twitter'];?>" title="Twitter"><i class="fa fa-twitter-square"></i></a>
            <a rel="publisher" target="_blank" href="<?php echo $config['social_links']['googleplus'];?>" title="Google+"><i class="fa fa-google-plus-square"></i></a>
            <a target="_blank" href="<?php echo $config['social_links']['linkedin'];?>" title="LinkedIn"><i class="fa fa-linkedin-square"></i></a>
        </div>
    </div>

</div>	<!-- end of .social-container -->
