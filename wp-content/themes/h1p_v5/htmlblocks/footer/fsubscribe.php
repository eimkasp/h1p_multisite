<?php

// Leads collection form. Uses MailChimp services

    global $post;
    if (is_home()) {
        $post_slug = 'home';
    } else {
        $post_slug = $post->post_name;
    }

    // Specify website
    $subdirs = explode('/', parse_url(get_site_url(), PHP_URL_PATH));
    $subdir = count($subdirs) > 1 ? $subdirs[1] : 'main';

?>

<div class="footer-subscribe-block">

    <div class="layout-row">
        <div class="block-col center p-t-10 p-b-20">
            <span class="block-title"><?php _e('Subscribe now!'); ?></span>
            <p><?php _e('Receive our monthly newsletter to get special offers and trending news straight to your inbox.'); ?></p>
        </div>
    </div>

    <form action="/subscribe" method="post" onSubmit="emailSubscr(event,$(this))">
        
        <input type="hidden" name="signup_source" value="<?php echo $subdir. ':' . $post_slug; ?>">

        <div class="container-flex justify-center as-row block-subscription-group p-h-60-md">
            <div class="block-col block-subscription-field block-subscription-first-name">
                <input type="text" name="first-name" size="25" value data-user-input placeholder="<?php echo _e('First name'); ?>">
            </div>
            <div class="block-col block-subscription-field block-subscription-last-name">
                <input type="text" name="last-name" size="25" value data-user-input placeholder="<?php echo _e('Last name'); ?>">
            </div>
            <div class="block-col block-subscription-field block-subscription-email">
                <input type="email" autocapitalize="off" autocorrect="off" name="email" size="25" value data-user-input placeholder="<?php echo _e('Email address'); ?>">
            </div>
            <div class="block-col block-subscription-field block-subscription-submit">
                <input type="submit" class="button highlight"  name="submit" value="<?php echo _e('Subscribe'); ?>" />
            </div>
        </div>

        <div id="subscription-response" class="block-subscription-response center" data-block-subscription-response></div>

    </form>

</div> <!-- end of .footer-subscribe-block -->

<script>

        //callback handler for form submit
        function emailSubscr(e,form)
        {
            e.preventDefault(); //STOP default action
            cleanResponse();   // clear previous error messages

            var postData = form.serializeArray();
            var formURL = form.attr("action");
            var emailVal = form.find('[name="email"]').val();

            if (checkForm6(emailVal) ) {
                // validation successful

                var storedEmail = getCookie ('subscriptionEmail');

                if ( emailVal == storedEmail) {
                    var msg = '<?php _e('You are already subscribed to our mailing list.')?>';
                    showError(msg);
                    
                    return false;
                }

                var subscribePromise = Promise.resolve($.ajax({
                    url: formURL,
                    type: 'post',
                    data: postData
                }));

                subscribePromise.then(
                    function fulfillHandler(data) {
                    // Got endpoint response

                        if (data.success === 1) {
                            console.log('GREAT SUCCESS !');
                            showSuccess (data.details);
                            setCookie ('subscriptionEmail', emailVal, 14);
                            cleanForm(form);

                        } else {
                            console.log('NO SUCCESS ' + data.details);
                            showError(data.details);
                        }
                    },

                    function rejectHandler(jqXHR, textStatus, errorThrown) {
                    // No endpoint listening
                        console.log('REJECT');
                        var msg = '<?php _e('Sorry, the subscription to newsletter is currently unavailable'); ?>';
                        showError(msg);
                    }
                        ).catch(function errorHandler(error) {
                        // ...
                    });

            } else {
                // failure of email validation
                form.find('[name="email"]').focus();
            }

        }

    function showError (msg){
        $('[data-block-subscription-response]').css('display','block');
        $('[data-block-subscription-response]').html('<p style="color:#EE6723">' + msg + '</p>');
    }
    
    function showSuccess (msg){
        $('[data-block-subscription-response]').css('display','block');
        $('[data-block-subscription-response]').html('<p>' + msg + '</p>');
    }

    function cleanResponse() {
        $('[data-block-subscription-response]').css('display','none');
        $('[data-block-subscription-response] *').slideUp("slow", function() { $(this).remove(); } );
    }

    function cleanForm(form) {
        form.find('[data-user-input]').each(function(){
            $(this).val('');
        });
    }

    function checkForm6(f) {
        var email_re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!email_re.test(f)) {
            alert("<?php echo _e('Please enter correct email address.');?>");
            return false;
        }

        return true;
    }


</script>
