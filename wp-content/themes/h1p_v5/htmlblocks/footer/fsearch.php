<?php
// Search block in footer
	global $config;
?>

<div class="footer-container">

	<div class="footer-search-block footer-search-block-adjust">
        <div class="search">
          <form id="footer-search" action="<?php echo $config['links']['search']?>" method="get">
            <input type="text" class="site-search-text" name="q" value="" placeholder="<?php _e('Search')?>">
            <input type="submit" class="site-search-btn icon search" value="">
          </form>
        </div>
    </div>

</div>  <!-- end of .footer-container -->