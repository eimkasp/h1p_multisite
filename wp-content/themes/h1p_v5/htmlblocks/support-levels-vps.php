<?php

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

// Check these IDs !!!
$addons_extracare_1level = $whmcs->getAddonsPrices(27);
$addons_extracare_2level = $whmcs->getAddonsPrices(28);

// print_r ($addons_extracare_1level);
// print_r ($addons_extracare_2level);

function get_addon_billing_cycle ( $addon_set ) {
    switch ( $addon_set['billingcycle'] ) {
        case 'Free':
            return 'free';
            break;
        case 'One Time':
            return '/hourly';
            break;
        case 'Monthly':
            return '/monthly';
            break;
        case 'Quarterly':
            return '/quarterly';
            break;
        case 'Semi-Annually':
            return '/semi-annually';
            break;
        case 'Annually':
            return '/annually';
            break;
        case 'Biennially':
            return '/biennially';
            break;
        case 'Triennially':
            return '/triennially';
            break;

        default:
            return false;
            break;
    }
}

?>

<div class="layout-row three-col-row">

    <div class="block-col white-styled green-styled center">
        <h3 data-toggle-wrapper><?php _e('General Support');?></h3>
        <span class="details-info" data-toggle-wrapper><?php _e('Unlimited, free');?></span>
        <div class="details-icon" data-toggle-wrapper><img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/supportlevel-general.svg" alt="Ulimited" width="120"></div>
        <div class="details-more">
            <a class="small" href="#" data-prevent data-toggle-active data-open-all-blocks="1"><?php _e('Details');?><i class="fa fa-chevron-down"></i></a>
            <div class="details-hidden small">
                <ul>
                    <li><?php _e('Service monitoring and performance control');?></li>
                    <li><?php _e('On-demand service connection to Host1Plus monitoring system');?></li>
                    <li><?php _e('Service management through your control panel');?></li>
                    <li><?php _e('Consultation regarding your service and software and more');?></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="block-col white-styled blue-styled center">
        <h3 data-toggle-wrapper><?php _e('1st Level Extra-Care');?></h3>
        <span class="details-info" data-toggle-wrapper>
        <?php
            echo $whmcs::$settings['currency_prefix'];
            echo $addons_extracare_1level['price'];
            $period1 = get_addon_billing_cycle ( $addons_extracare_1level );
            if ( $period1 ) echo _e( $period1 );
        ?>
        </span>
        <div class="details-icon" data-toggle-wrapper><img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/supportlevel-1st.svg" alt="Monthly" width="120"></div>
        <div class="details-more">
            <a class="small" href="#" data-prevent data-toggle-active data-open-all-blocks="1"><?php _e('Details');?><i class="fa fa-chevron-down"></i></a>
            <div class="details-hidden small">
                <ul>
                    <li><?php _e('LAMP setup');?></li>
                    <li><?php _e('DirectAdmin installation, configuration and migration');?></li>
                    <li><?php _e('WHM/cPanel management');?></li>
                    <li><?php _e('CentOS Web Panel, Webmin installation');?></li>
                    <li><?php _e('CMS installation (WordPress, Joomla, Drupal, etc.)');?></li>
                    <li><?php _e('OpenVPN setup and configuration');?></li>
                    <li><?php _e('Desktop and VNC installation');?></li>
                    <li><?php _e('SSH configuration');?></li>
                    <li><?php _e('cPanel, ISPconfig migration');?></li>
                    <li><?php _e('OS reinstallation');?></li>
                    <li><?php _e('Basic security check and guidance');?></li>
                    <li><?php _e('Service monitoring');?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="block-col white-styled red-styled center">
        <h3 data-toggle-wrapper><?php _e('2nd Level Extra-Care');?></h3>
        <span class="details-info" data-toggle-wrapper>
        <?php
            echo $whmcs::$settings['currency_prefix'];
            echo $addons_extracare_2level['price'];
            $period2 = get_addon_billing_cycle ( $addons_extracare_2level );
            if ( $period2 ) echo _e( $period2 );
        ?>
        </span>
        <div class="details-icon" data-toggle-wrapper><img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/supportlevel-2nd.svg" alt="Hourly" width="120"></div>
        <div class="details-more">
            <a class="small" href="#" data-prevent data-toggle-active data-open-all-blocks="1"><?php _e('Details');?><i class="fa fa-chevron-down"></i></a>
            <div class="details-hidden small">
                <ul>
                    <li><?php _e('Network and firewall configuration');?></li>
                    <li><?php _e('BGP setup');?></li>
                    <li><?php _e('Load balancer setup and configuration');?></li>
                    <li><?php _e('Apache and NGINX advanced configuration');?></li>
                    <li><?php _e('Squid or other caching software setup and configuration');?></li>
                    <li><?php _e('Custom monitoring setup and configuration');?></li>
                    <li><?php _e('GRE tunnel setup and configuration');?></li>
                </ul>
            </div>
        </div>
    </div>

</div> <!-- end of .layout-row .three-col-row -->

<script>
    jQuery(document).ready(function(){
        $('[data-toggle-wrapper]').click(function(e){
            $('[data-toggle-active]').click();
            /*$('[data-toggle-active="' + $(this).attr("data-toggle-wrapper") + '"]').click();*/
        });
    });
</script>
