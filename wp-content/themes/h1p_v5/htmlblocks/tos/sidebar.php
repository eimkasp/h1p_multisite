<div class="archive-block">

    <div class="archive">
        <div class="page-title-block">
            <h2 class="page-title">Contents</h2>
        </div>

        <ul class="archive-mainmenu">

            <li><a href="#tos-section-glossary" data-scrollto="tos-section-glossary"><i class="fa fa-chevron-circle-right"></i> Glossary</a></li>

            <li><a href="#tos-section-1" data-scrollto="tos-section-1"><i class="fa fa-chevron-circle-right"></i> I. Application of Terms</a></li>

            <li><a href="#" data-prevent data-toggle-open="parent"><i class="fa fa-chevron-circle-right"></i> II. General Terms and Conditions</a>
                <ul class="archive-submenu">
                    <li><a href="#tos-section-2-1" data-scrollto="tos-section-2-1"><i class="fa fa-angle-right"></i> 1. Provider’s Rights and Responsibilities</a></li>
                    <li><a href="#tos-section-2-2" data-scrollto="tos-section-2-2"><i class="fa fa-angle-right"></i> 2. User’s Rights and Responsibilities</a></li>
                    <li><a href="#tos-section-2-3" data-scrollto="tos-section-2-3"><i class="fa fa-angle-right"></i> 3. User’s Non-Compliance</a></li>
                    <li><a href="#tos-section-2-4" data-scrollto="tos-section-2-4"><i class="fa fa-angle-right"></i> 4. Uptime Guarantee</a></li>
                </ul>
            </li>

            <li><a href="#" data-prevent data-toggle-open="parent"><i class="fa fa-chevron-circle-right"></i> III. Billing Account</a>
                <ul class="archive-submenu">
                    <li><a href="#tos-section-3-1" data-scrollto="tos-section-3-1"><i class="fa fa-angle-right"></i> 1. Account Eligibility</a></li>
                    <li><a href="#tos-section-3-2" data-scrollto="tos-section-3-2"><i class="fa fa-angle-right"></i> 2. Additional Information Request</a></li>
                    <li><a href="#tos-section-3-3" data-scrollto="tos-section-3-3"><i class="fa fa-angle-right"></i> 3. User's Content Policy</a></li>
                </ul>
            </li>

            <li><a href="#" data-prevent data-toggle-open="parent"><i class="fa fa-chevron-circle-right"></i> IV. Billing and Payment Policy</a>
                <ul class="archive-submenu">
                    <li><a href="#tos-section-4-1" data-scrollto="tos-section-4-1"><i class="fa fa-angle-right"></i> 1. Billing Policy</a></li>
                    <li><a href="#tos-section-4-2" data-scrollto="tos-section-4-2"><i class="fa fa-angle-right"></i> 2. Payment Policy</a></li>
                </ul>
            </li>

            <li><a href="#" data-prevent data-toggle-open="parent"><i class="fa fa-chevron-circle-right"></i> V. Cancellation and Refunds</a>
                <ul class="archive-submenu">
                    <li><a href="#tos-section-5-1" data-scrollto="tos-section-5-1"><i class="fa fa-angle-right"></i> 1. Service Cancellation</a></li>
                    <li><a href="#tos-section-5-2" data-scrollto="tos-section-5-2"><i class="fa fa-angle-right"></i> 2. Refund Policy</a></li>
                    <li><a href="#tos-section-5-2" data-scrollto="tos-section-5-3"><i class="fa fa-angle-right"></i> 3. Chargebacks, Reversals and Retrievals</a></li>
                </ul>
            </li>

            <li><a href="#" data-prevent data-toggle-open="parent"><i class="fa fa-chevron-circle-right"></i> VI. Suspension and Termination</a>
                <ul class="archive-submenu">
                    <li><a href="#tos-section-6-1" data-scrollto="tos-section-6-1"><i class="fa fa-angle-right"></i> 1. Suspension</a></li>
                    <li><a href="#tos-section-6-2" data-scrollto="tos-section-6-2"><i class="fa fa-angle-right"></i> 2. Termination</a></li>
                </ul>
            </li>

            <li><a href="#" data-prevent data-toggle-open="parent"><i class="fa fa-chevron-circle-right"></i> VII. Acceptable Use Policy</a>
                <ul class="archive-submenu">
                    <li><a href="#tos-section-7-1" data-scrollto="tos-section-7-1"><i class="fa fa-angle-right"></i> 1. Prohibited Uses</a></li>
                    <li><a href="#tos-section-7-2" data-scrollto="tos-section-7-2"><i class="fa fa-angle-right"></i> 2. Anti-Spam Policy</a></li>
                    <li><a href="#tos-section-7-3" data-scrollto="tos-section-7-3"><i class="fa fa-angle-right"></i> 3. Unlimited Resources Policy</a></li>
                    <li><a href="#tos-section-7-4" data-scrollto="tos-section-7-4"><i class="fa fa-angle-right"></i> 4. Copyright Policy</a></li>
                    <li><a href="#tos-section-7-5" data-scrollto="tos-section-7-5"><i class="fa fa-angle-right"></i> 5. Unacceptable Material</a></li>
                    <li><a href="#tos-section-7-6" data-scrollto="tos-section-7-6"><i class="fa fa-angle-right"></i> 6. Unacceptable Resource Usage</a></li>
                </ul>
            </li>

            <li><a href="#" data-prevent data-toggle-open="parent"><i class="fa fa-chevron-circle-right"></i> VIII. Report Submission Policy</a>
                <ul class="archive-submenu">
                    <li><a href="#tos-section-8-1" data-scrollto="tos-section-8-1"><i class="fa fa-angle-right"></i> 1. General Guidelines</a></li>
                    <li><a href="#tos-section-8-2" data-scrollto="tos-section-8-2"><i class="fa fa-angle-right"></i> 2. Copyright Violation Report</a></li>
                    <li><a href="#tos-section-8-3" data-scrollto="tos-section-8-3"><i class="fa fa-angle-right"></i> 3. Phishing Report</a></li>
                    <li><a href="#tos-section-8-4" data-scrollto="tos-section-8-4"><i class="fa fa-angle-right"></i> 4. SPAM Report</a></li>
                    <li><a href="#tos-section-8-5" data-scrollto="tos-section-8-5"><i class="fa fa-angle-right"></i> 5. Network Abuse Report</a></li>
                    <li><a href="#tos-section-8-6" data-scrollto="tos-section-8-6"><i class="fa fa-angle-right"></i> 6. Defamation and Libel Report</a></li>
                    <li><a href="#tos-section-8-7" data-scrollto="tos-section-8-7"><i class="fa fa-angle-right"></i> 7. Child Pornography Report</a></li>
                    <li><a href="#tos-section-8-8" data-scrollto="tos-section-8-8"><i class="fa fa-angle-right"></i> 8. Credit Card Fraud Report</a></li>
                </ul>
            </li>

            <li><a href="#" data-prevent data-toggle-open="parent"><i class="fa fa-chevron-circle-right"></i> IX. Privacy Policy</a>
                <ul class="archive-submenu">
                    <li><a href="#tos-section-9-1" data-scrollto="tos-section-9-1"><i class="fa fa-angle-right"></i> 1. Intellectual Property</a></li>
                    <li><a href="#tos-section-9-2" data-scrollto="tos-section-9-2"><i class="fa fa-angle-right"></i> 2. Collected Private Information</a></li>
                    <li><a href="#tos-section-9-3" data-scrollto="tos-section-9-3"><i class="fa fa-angle-right"></i> 3. Collected Anonymous Information</a></li>
                    <li><a href="#tos-section-9-3" data-scrollto="tos-section-9-4"><i class="fa fa-angle-right"></i> 4. Cookies Policy</a></li>
                </ul>
            </li>

            <li><a href="#" data-prevent data-toggle-open="parent"><i class="fa fa-chevron-circle-right"></i> X. Customer Support</a>
                <ul class="archive-submenu">
                    <li><a href="#tos-section-10-1" data-scrollto="tos-section-10-1"><i class="fa fa-angle-right"></i> 1. Basic Support Service Agreement</a></li>
                    <li><a href="#tos-section-10-2" data-scrollto="tos-section-10-2"><i class="fa fa-angle-right"></i> 2. Support Availability</a></li>
                    <li><a href="#tos-section-10-3" data-scrollto="tos-section-10-3"><i class="fa fa-angle-right"></i> 3. Non-Active User Support Service</a></li>
                    <li><a href="#tos-section-10-4" data-scrollto="tos-section-10-4"><i class="fa fa-angle-right"></i> 4. Active User Support Service</a></li>
                    <li><a href="#tos-section-10-5" data-scrollto="tos-section-10-5"><i class="fa fa-angle-right"></i> 5. Advanced Support Service Agreement</a></li>
<?php 
/*
                    <li><a href="#tos-section-10-6" data-scrollto="tos-section-10-6"><i class="fa fa-angle-right"></i> 6. Extra Care Support Service</a></li>
                    <li><a href="#tos-section-10-7" data-scrollto="tos-section-10-7"><i class="fa fa-angle-right"></i> 7. Dedicated Administrator Support Service</a></li>
*/
?>
                </ul>
            </li>

            <li><a href="#tos-section-11" data-scrollto="tos-section-11"><i class="fa fa-chevron-circle-right"></i> XI. Transfer Policy</a></li>

            <li><a href="#" data-prevent data-toggle-open="parent"><i class="fa fa-chevron-circle-right"></i> XII. Reseller program</a>
                <ul class="archive-submenu">
                    <li><a href="#tos-section-12-1" data-scrollto="tos-section-12-1"><i class="fa fa-angle-right"></i> 1. Terms of Agreement</a></li>
                    <li><a href="#tos-section-12-2" data-scrollto="tos-section-12-2"><i class="fa fa-angle-right"></i> 2. Amendments</a></li>
                    <li><a href="#tos-section-12-3" data-scrollto="tos-section-12-3"><i class="fa fa-angle-right"></i> 3. Services</a></li>
                    <li><a href="#tos-section-12-4" data-scrollto="tos-section-12-4"><i class="fa fa-angle-right"></i> 4. Charges and Payment</a></li>
                    <li><a href="#tos-section-12-5" data-scrollto="tos-section-12-5"><i class="fa fa-angle-right"></i> 5. Suspension and Termination</a></li>
                    <li><a href="#tos-section-12-6" data-scrollto="tos-section-12-6"><i class="fa fa-angle-right"></i> 6. Acceptable Use Policy and Privacy Policy</a></li>
                    <li><a href="#tos-section-12-7" data-scrollto="tos-section-12-7"><i class="fa fa-angle-right"></i> 7. Limitation of liability</a></li>
                    <li><a href="#tos-section-12-8" data-scrollto="tos-section-12-8"><i class="fa fa-angle-right"></i> 8. Reseller obligation</a></li>
                </ul>
            </li>

            <li><a href="#" data-prevent data-toggle-open="parent"><i class="fa fa-chevron-circle-right"></i> XIII. Affiliate Policy</a>
                <ul class="archive-submenu">
                    <li><a href="#tos-section-13-1" data-scrollto="tos-section-13-1"><i class="fa fa-angle-right"></i> 1. Joining the Affiliate Program</a></li>
                    <li><a href="#tos-section-13-2" data-scrollto="tos-section-13-2"><i class="fa fa-angle-right"></i> 2. Affiliate Account Suspension</a></li>
                    <li><a href="#tos-section-13-3" data-scrollto="tos-section-13-3"><i class="fa fa-angle-right"></i> 3. Linking and Advertising</a></li>
                    <li><a href="#tos-section-13-4" data-scrollto="tos-section-13-4"><i class="fa fa-angle-right"></i> 4. Search Engine Marketing Campaigns</a></li>
                    <li><a href="#tos-section-13-5" data-scrollto="tos-section-13-5"><i class="fa fa-angle-right"></i> 5. Website Content</a></li>
                    <li><a href="#tos-section-13-6" data-scrollto="tos-section-13-6"><i class="fa fa-angle-right"></i> 6. Affiliate Commissions and Rates</a></li>
                    <li><a href="#tos-section-13-7" data-scrollto="tos-section-13-7"><i class="fa fa-angle-right"></i> 7. Affiliate Payments</a></li>
                    <li><a href="#tos-section-13-8" data-scrollto="tos-section-13-8"><i class="fa fa-angle-right"></i> 8. Affiliate Account Cancellation</a></li>
                </ul>
            </li>

            <li><a href="#" data-prevent data-toggle-open="parent"><i class="fa fa-chevron-circle-right"></i> XIV. Obligations for Service Provision</a>
                <ul class="archive-submenu">
                    <li><a href="#tos-section-14-1" data-scrollto="tos-section-14-1"><i class="fa fa-angle-right"></i> 1. Obligations for Domain Registration and Domain Transfer</a></li>
                    <li><a href="#tos-section-14-2" data-scrollto="tos-section-14-2"><i class="fa fa-angle-right"></i> 2. Obligations for Shared and Reseller Hosting</a></li>
                    <li><a href="#tos-section-14-3" data-scrollto="tos-section-14-3"><i class="fa fa-angle-right"></i> 3. Obligations for VPS and Cloud VPS</a></li>
                    <li><a href="#tos-section-14-4" data-scrollto="tos-section-14-4"><i class="fa fa-angle-right"></i> 4. Obligations for Cloud Servers</a></li>
                </ul>
            </li>
            <li><a href="#tos-section-15" data-scrollto="tos-section-15"><i class="fa fa-chevron-circle-right"></i> XV. Compliance with Local Organizations</a></li>


        </ul>

    </div>

</div>
<script>
$(document).ready(function(){
    $("[data-scrollto]").click(function() {
        $('html, body').animate({
            scrollTop: $("#"+$(this).attr("data-scrollto")).offset().top
        }, 2000);
    });
});
</script>


