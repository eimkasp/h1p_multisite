<div class="installer-3">
    <div class="container">
        <div class="row">
            <div class="side left">
                <img src="<?php echo get_template_directory_uri() ?>/img/installer/one-click.png" alt=""/>
            </div>
            <div class="side right">
                <div class="title">
                    <?php _e('One-Click App Auto-Installer');?>
                </div>
                <div class="subtitle">
                    <?php _e('Build your online projects with ease!');?>
                </div>
                <div class="description">
                    <?php _e('Get the tools you need in seconds! Our auto-installer will power-up your online project with a single click. Just pick your apps at the checkout and enjoy easier server setup!');?>
                </div>
                <div class="apps">
                    <div class="cell">
                        <span class="logo wp"></span>
                    </div>
                    <div class="cell sep">
                        <span class="separator"></span>
                    </div>
                    <div class="cell">
                        <span class="logo openvpn"></span>
                    </div>
                    <div class="cell sep">
                        <span class="separator"></span>
                    </div>
                    <div class="cell">
                        <span class="logo cpanel"></span>
                    </div>
                    <div class="cell sep">
                        <span class="separator"></span>
                    </div>
                    <div class="cell">
                        <span class="logo ispconfig"></span>
                    </div>
                </div>
                <div class="try-now">
                    <a href="#" class="button orange"><?php _e('Try it now');?></a>
                </div>
            </div>
        </div>
    </div>
</div>