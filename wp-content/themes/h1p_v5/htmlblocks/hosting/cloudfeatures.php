<section class="block cloudfeatures bg-white">

    <div class="container">
        <div class="row">

            <h2 class="block-title"><?php _e('Why settle on our Cloud?')?></h2>

            <div class="block-content">

                <ul class="features-grid">
                    <li class="feature feature-1">
                        <span class="feature-image">
                            <i class="icon cloudfeatures-icon pay-as-you-grow responsive"></i>
                        </span>
                        <span class="feature-title"><?php _e('Pay As You Grow')?></span>
                        <span class="feature-description"><?php _e('Benefit from flexible Cloud hosting solutions and improve your website step by step without wasting money for unnecessary resources.')?></span>
                    </li>
                    <li class="feature feature-2">
                        <span class="feature-image">
                            <i class="icon cloudfeatures-icon location-germany responsive"></i>
                        </span>
                        <span class="feature-title"><?php _e('A Premium Location')?></span>
                        <span class="feature-description"><?php _e('Our Cloud servers are located in Frankfurt, Germany. A reliable environment and top security measures ensure flawless performance for our customers.')?></span>
                    </li>
                    <li class="feature feature-3">
                        <span class="feature-image">
                            <i class="icon cloudfeatures-icon money-back-guarantee responsive"></i>
                        </span>
                        <span class="feature-title"><?php _e('A Money-Back Guarantee')?></span>
                        <span class="feature-description"><?php _e('Feel confident and secure - we always provide a 14-day money-back guarantee for all users – no exceptions.')?></span>
                    </li>
                    <li class="feature feature-4">
                        <span class="feature-image">
                            <i class="icon cloudfeatures-icon dedicated-ram responsive"></i>
                        </span>
                        <span class="feature-title"><?php _e('Dedicated RAM')?></span>
                        <span class="feature-description"><?php _e('Enjoy the space you need! Do not feel repressed and smothered by other users - escape interference and system slow-downs for good!')?></span>
                    </li>
                    <li class="feature feature-5">
                        <span class="feature-image">
                            <i class="icon cloudfeatures-icon windows-linux responsive"></i>
                        </span>
                        <span class="feature-title"><?php _e('Windows & Linux')?></span>
                        <span class="feature-description"><?php _e('Work with the OS you prefer! Our Cloud is powered by both – Windows OS and Linux OS – to react and adapt to diverse requirements.')?></span>
                    </li>
                    <li class="feature feature-6">
                        <span class="feature-image">
                            <i class="icon cloudfeatures-icon extra-care responsive"></i>
                        </span>
                        <span class="feature-title"><?php _e('On-Demand Extra Care')?></span>
                        <span class="feature-description"><?php _e('Seek support from our customer support team and receive quick and friendly assistance. Host1Plus professionals are always ready to help!')?></span>
                    </li>
                    <li class="feature feature-7">
                        <span class="feature-image">
                            <i class="icon cloudfeatures-icon instant-setup responsive"></i>
                        </span>
                        <span class="feature-title"><?php _e('Instant Setup')?></span>
                        <span class="feature-description"><?php _e('Don’t waste time! Set up your website immediately and avoid undergoing long and complex installation and customization processes.')?></span>
                    </li>
                    <li class="feature feature-8">
                        <span class="feature-image">
                            <i class="icon cloudfeatures-icon auto-backups responsive"></i>
                        </span>
                        <span class="feature-title"><?php _e('Auto Backups')?></span>
                        <span class="feature-description"><?php _e('We constantly make automated system backups, so you may never feel frustrated if you have forgotten to take care of them on time.')?></span>
                    </li>
                </ul>

            </div>

        </div>
    </div>

</section>
