<section class="block cloud-tabs bg-white">

        <div class="container">
            <div class="row">

                <div class="block-tabs">

                    <div class="app-tabs-container">
                        <div class="app-tabs">
                            <span data-tab-link="cloud.win" class="tab desktop active"><?php _e('Explore Windows on Cloud')?></span>
                            <?php /*
                            <span data-tab-link="cloud.linux" class="tab desktop">Cloud Linux</span>
                            <span data-tab-link="cloud.about" class="tab desktop">About Cloud Hosting</span>
                            */ ?>
                        </div>
                    </div>

                    <div class="tabs" data-tabs="cloud">
                        <span data-tab-link="cloud.win" data-tab-toggle="self" class="tab mob active"><?php _e('Try Windows on Cloud')?></span>
                        <div class="tab-content active" data-tab-id="win">
                            <?php include("cloud_tabs/windows_server.php"); ?>
                        </div>
                        <?php /*
                        <span data-tab-link="cloud.linux" data-tab-toggle="self" class="tab mob"><?php _e('Cloud Linux')?></span>
                        <div class="tab-content" data-tab-id="linux">
                            <?php include("cloud_tabs/linux-templates.php"); ?>
                        </div>
                        <span data-tab-link="cloud.about" data-tab-toggle="self" class="tab mob">About Cloud Hosting</span>
                        <div class="tab-content about" data-tab-id="about">
                            <?php include("cloud_tabs/about-cloud-hosting.php"); ?>
                        </div>
                        */ ?>
                    </div>

                </div>

            </div>
        </div>

</section>
