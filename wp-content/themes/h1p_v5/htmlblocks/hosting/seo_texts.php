<section class="block seo-texts">

    <div class="container">
        <div class="row">

            <div class="title-block two">
                <div class="text-col left">
                    <span class="title"><?php _e('What is SEO hosting?')?></span>
                    <div class="text">
                        <p><?php _e('SEO hosting is a specific hosting solution for companies and individuals seeking to increase their presence online. Hosting your websites on multiple dedicated IPs awards you with a better search engine ranking. With SEO hosting you get a set of resources and tools to implement your SEO strategy and reach better results.')?></p>
                    </div>
                </div>
                <div class="text-col right">
                    <span class="title"><?php _e('One stop for all matters!')?></span>
                    <div class="text">
                        <p><?php _e('We provide our clients with the best quality SEO hosting service for great prices, making Host1Plus the one-stop shop for all your hosting-related needs. You can host as many websites as you want, this way saving lots of time and trouble as you only have to deal with one service provider to fulfill your needs.')?></p>
                    </div>
                </div>
            </div>

            <div class="title-block one">
                <div class="text-col">
                    <div class="titles">
                        <span class="title left"><?php _e('How to choose a SEO hosting plan?')?></span>
                    </div>
                    <div class="texts">
                        <div class="text left">
                            <p><?php _e('Adjust to your specific needs by choosing from 3 SEO hosting plans. The more resources you need, the higher your budget is, and the wider the scope of your SEO activities will be, the more advanced plan you should opt for. Remember that you can always update your plan, if needed.')?></p>
                        </div>
                        <div class="text right">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>
