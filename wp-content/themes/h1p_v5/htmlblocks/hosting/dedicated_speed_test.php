<section class="block speed-test">

    <div class="container">
        <div class="row">
            <h2 class="block-title">Want to check our network? </h2>
            <span class="descr">You can check our Germany, Frankfurt data center network here and decide for yourself!</span>
            <div class="block-content">
                <div class="speed-circle">
                    <div class="title">tinklo greicio testas</div>
                    <div class="choose-table">
                        <div class="choose-row">
                            <div class="choose-col"><span class="col-header">File size</span></div>
                            <div class="choose-col"><span class="col-header">Port speed</span></div>
                        </div>
                        <div class="choose-row">
                            <div class="choose-col"><input id="size_10" type="radio" name="file_size" value="10"><label for="size_10">10 MB</label></div>
                            <div class="choose-col"><input id="speed_100" type="radio" name="port_speed" value="100"><label for="speed_100">100 Mbps</label></div>
                        </div>
                        <div class="choose-row">
                            <div class="choose-col"><input id="size_100" type="radio" name="file_size" value="100"><label for="size_100">100 MB</label></div>
                            <div class="choose-col"><input id="speed_1000" type="radio" name="port_speed" value="1000"><label for="speed_1000">1 Gbps</label></div>
                        </div>
                        <div class="choose-row">
                            <div class="choose-col"></div>
                            <div class="choose-col"><input id="speed_10000" type="radio" name="port_speed" value="10000"><label for="speed_10000">10 Gbps</label></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>