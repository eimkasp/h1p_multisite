<section class="block whydedicated bg-white">

    <div class="container">

        <h2 class="block-title"><?php _e('Extra-Care Customer Support')?></h2>

        <div class="row">

            <div class="block-content perfomance">

                <span class="description"><?php _e('If you need the power of a dedicated server, but don’t have the time or the skills to run it, our Extra-Care Support is the ultimate solution for you!')?></span>

                <div class="image">
                    <span class="img client"><i class="icon whydedicated client"></i></span>
                    <span class="img plus"><i class="icon whydedicated plus"></i></span>
                    <span class="img perfomance"><i class="icon whydedicated perfomance"></i></span>
                    <span class="img arrow"><i class="icon whydedicated arrow"></i></span>
                    <span class="img extracare"><i class="icon whydedicated extracare"></i></span>
                </div>

                <span class="extracare-description"><?php _e('Get the help you need for only $30 per month!')?></span>

            </div>

            <div class="block-content features">

                <ul class="features-list checklist green">
                    <li><?php _e('Service monitoring and performance control')?></li>
                    <li><?php _e('On-request service connection to our monitoring system')?></li>
                    <li><?php _e('Service management through your control panel')?></li>
                    <li><?php _e('Other services')?></li>
                </ul>

            </div>

        </div>
    </div>

</section>
