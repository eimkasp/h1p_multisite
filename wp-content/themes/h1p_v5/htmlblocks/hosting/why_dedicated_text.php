<section class="block why-dedicated-text">

    <div class="container">
        <div class="row">
            <h2 class="block-title"><?php _e('Why rely on our dedicated servers?')?></h2>
            <div class="block-content">
                <div class="text-table">
                    <div class="text-col first">
                        <span class="title"><?php _e('Worldwide connectivity!')?></span>
                        <span class="text"><?php _e('Host1Plus unmetered dedicated servers are stationed in Germany, Frankfurt. Stable and fast for users anywhere in the world.')?></span>
                    </div>
                    <div class="text-col">
                        <span class="title"><?php _e('Expandability for your needs!')?></span>
                        <span class="text"><?php _e('If you need to expand your services we will do our best to help you out! At Host1Plus we provide personalized offers based on your current and future services.')?></span>
                    </div>
                    <div class="text-col last">
                        <span class="title"><?php _e('Dedicated support!')?></span>
                        <span class="text"><?php _e('Our support team works hard to take care for our clients and ensure a reliable service. From answering simple questions to the resolution of complex technical problems – they are always ready to help.')?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

