<section class="block buy-reseller">

    <div class="container">
        <div class="row">

            <span class="title"><?php _e('Start your reseller business today!')?></span>

            <div class="scheme">
                <div class="step">
                    <div class="title-line">
                        <span class="circle-number">1</span>
                        <span class="step-title"><?php _e('Your business: local or world wide?')?></span>
                    </div>
                    <div class="step-scheme">
                        <table class="scheme-table">
                            <tr>
                                <td class="cell-1">&nbsp;</td>
                                <td class="cell-2">&nbsp;</td>
                                <td class="cell-3 vertical-top" rowspan="2">
                                    <div class="choice">
                                        <div class="cont-tbl">
                                            <div class="tbl-row">
                                                <div class="choice-title"><?php _e('Single Location')?></div>
                                            </div>
                                            <div class="tbl-row">
                                                <div class="single-loc">
                                                    <i class="icon reseller-buy big-loc"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="cell-4">&nbsp;</td>
                                <td class="cell-5 vertical-middle" rowspan="2">
                                    <div class="text-block">
                                        <?php _e('Starting small? Opt for a single location if your starter business is oriented to a single market or a specific region and provide your customers with the best connectivity and speed.')?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="cell-1 border top">&nbsp;</td>
                                <td class="cell-2 border top left">&nbsp;</td>
                                <td class="cell-4 border top">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="cell-1">&nbsp;</td>
                                <td class="cell-2 border left">&nbsp;</td>
                                <td class="or-separate"><?php _e('OR')?></td>
                                <td class="cell-4">&nbsp;</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="cell-1">&nbsp;</td>
                                <td class="cell-2 border left">&nbsp;</td>
                                <td class="cell-3 vertical-top" rowspan="2">
                                    <div class="choice">
                                        <div class="cont-tbl">
                                            <div class="tbl-row">
                                                <div class="choice-title"><?php _e('Multiple Locations')?></div>
                                            </div>
                                            <div class="tbl-row">
                                                <div class="multi-loc">
                                                    <div class="img-holder">
                                                        <i class="icon reseller-buy small-loc left"></i>
                                                        <i class="icon reseller-buy big-loc"></i>
                                                        <i class="icon reseller-buy small-loc right"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="cell-4">&nbsp;</td>
                                <td class="cell-5 vertical-middle" rowspan="2">
                                    <div class="text-block">
                                        <?php _e('Growing fast? Multiple locations will help you develop your reseller business and respond to your customer needs. By choosing locations in different regions you will ensure a stable service with top performance worldwide.')?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="cell-1">&nbsp;</td>
                                <td class="cell-2 border top">&nbsp;</td>
                                <td class="cell-4 border top">&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="step">
                    <div class="title-line">
                        <span class="circle-number">2</span>
                        <span class="step-title"><?php _e('WHMCS or manual billing?')?></span>
                    </div>
                    <div class="step-scheme">
                        <table class="scheme-table">
                            <tr>

                                <td class="cell-1">&nbsp;</td>
                                <td class="cell-2">&nbsp;</td>

                                <td class="cel-fluid vertical-top" rowspan="2">
                                    <div class="choice whmcs">
                                        <div class="cont-tbl">
                                            <div class="tbl-row">
                                                <div class="choice-title left"><?php _e('WHMCS')?></div>
                                            </div>
                                            <div class="tbl-row">
                                                <span class="price-descr"><?php printf(__('Monthly license %s'), '<span class="orange">$13.80</span>')?></span>
                                            </div>
                                            <div class="tbl-row">
                                                <p><?php _e('A fully automated all-in-one client management, billing and support system will get everything done for you –from account provisioning to e-mail reminders.')?></p>
                                            </div>
                                            <div class="tbl-row">
                                                <div class="whmcs-img">
                                                    <i class="icon reseller-buy whmcs"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>

                                <td class="cell-4">&nbsp;</td>
                                <td class="cell-3 vertical-middle or-separate or-separate-horizontal" rowspan="2"><?php _e('OR')?></td>
                                <td class="cell-4">&nbsp;</td>

                                <td class="cell-fluid vertical-middle" rowspan="2">

                                    <div class="choice bill">
                                        <div class="cont-tbl">
                                            <div class="tbl-row">
                                                <div class="choice-title left"><?php _e('Manual Billing')?></div>
                                            </div>
                                            <div class="tbl-row">
                                                <p><?php _e('Don’t have many clients yet? Use manual billing and cover everything yourself – prepare invoices, send reminders and request additional information.')?></p>
                                            </div>
                                        </div>
                                    </div>

                                </td>

                            </tr>
                            <tr>
                                <td class="cell-1 border top">&nbsp;</td>
                                <td class="cell-2 border top">&nbsp;</td>
                                <td class="cell-4 border top">&nbsp;</td>
                                <td class="cell-4 border top">&nbsp;</td>
                            </tr>

                        </table>
                    </div>
                </div>
                <div class="step">
                    <div class="title-line">
                        <span class="circle-number">3</span>
                        <span class="step-title"><?php _e('Done! You’re ready to kick off!')?></span>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>
