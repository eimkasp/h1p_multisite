<?php

global $config;

?>

<script>

    var rad = function(x) {
      return x * Math.PI / 180;
    };

    var getDistance = function(p1_lat, p1_lng, p2_lat, p2_lng) {
      var R = 6371000; // Earth’s mean radius in meter
      var dLat = rad(p2_lat - p1_lat);
      var dLong = rad(p2_lng - p1_lng);
      var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(rad(p1_lat)) * Math.cos(rad(p2_lat)) *
        Math.sin(dLong / 2) * Math.sin(dLong / 2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      var d = R * c;
      return d / 1000; // returns the distance in km
    };


    var linkClicked = false;
    var magnificPopup;

    $(document).on('click','.location-speedtest a[data-popup]', function(){
        linkClicked = false;
        magnificPopup = $.magnificPopup.instance;
        var winLocation = window.location.pathname;
        if (winLocation.indexOf('data-centers') > 0) {
            window.location.hash = $(this).attr('data-popup-id');
        }
    })

    $(document).on('click', '#speedtest-popup #file-links a', function(){

        var fileSizeId = $(this).attr('data-file-size-id');
        var fileSizeName = $(this).attr('data-file-size-name');

        $('#file-size').text('<?php _e('Sending'); ?> ' + fileSizeName + ' <?php _e('file'); ?>');
        $( "input[name=file_size]" ).val(fileSizeId);

        $('#rating input[data-question-num=7]').each(function(){
            if ($(this).attr('data-question-other') === fileSizeId) {
                $(this).prop('checked', true);
            } else {
                $(this).prop('checked', false);
            }
        })

        if (!linkClicked) {
            linkClicked = true;

            // check if elements are prepared
            var checkRating = $('#rating-icons');
            var checkGeo = $('#geo-location');
            var checkDistance = $('#geo-distance');

            if (checkRating.length == 0 && checkGeo.text().length == 0 && checkDistance.text().length == 0) {

                // enhance form elements
                var $ratingIcons = $('<ul id="rating-icons"></ul>');
                $ratingIcons.append('<li><a class="rating-btn" data-question-other="10003" href="#"><i class="fa fa-frown-o"></i> <?php _e('Slow'); ?></a></li>');
                $ratingIcons.append('<li><a class="rating-btn" data-question-other="10002" href="#"><i class="fa fa-meh-o"></i> <?php _e('Average'); ?></a></li>');
                $ratingIcons.append('<li><a class="rating-btn" data-question-other="10001" href="#"><i class="fa fa-smile-o"></i> <?php _e('Fast'); ?></a></li>');
                $('#rating').append($ratingIcons);

                var lat2 = $('#geo-distance').attr('data-server-latitude');
                var long2 = $('#geo-distance').attr('data-server-longitude');
                //console.log (lat2 + '/' + long2);

                $.ajax({
                    type: "GET",
                    url: "https://freegeoip.net/json",
                    crossDomain: true,
                    dataType: "jsonp",
                    success: function(data) {
                        //console.dir(data);
                        var geoInfo = data.country_name + ', ' + data.city;
                        $('#geo-location').append(geoInfo);
                        $('input[name=client_location]').val(geoInfo);
                        var lat1 = data.latitude;
                        var long1 = data.longitude;
                        //console.log (lat1 + '/' + long1);
                        var dist = getDistance (lat1,long1,lat2,long2);
                        $('#geo-distance').append('<?php _e('Distance');?> ' + Math.round(dist) + ' km');
                    },
                    error: function() {
                    }
                })

            }

        }
        $('#test-info').removeClass('hidden');

    });
    $(document).on('click', '#speedtest-popup .rating-btn', function(e){

        e.preventDefault;
        var experienceClicked = $(this);

        var experienceId = $(this).attr('data-question-other');
        $('#rating input[data-question-num=5]').each(function(){
            if ($(this).attr('data-question-other') === experienceId) {
                $(this).prop('checked', true);
            } else {
                $(this).prop('checked', false);
            }
        })

        linkClicked = false;
        var dataset = $("#speedtest-feedback").serialize();

        $.ajax({
            type: "POST",
            url: "<?php echo $config['links']['speedtest']; ?>",
            data: dataset,
            dataType: "json",
            success: function(data) {
                if (data.success == true) {

                    experienceClicked.addClass('selected');
                    var experienceVal = experienceClicked.html();
                    // prevent double feedback and remove rating buttons
                    $('#rating-icons').children().remove();
                    $('#rating-icons').append('<li class="center p-v-10 selected">'+experienceVal+'</li>');

                }
                if (data.success == false) {
                    $('#test-info').html('<p class="center p-v-20"><strong><?php _e('Sorry! Error when submitting feedback.');?></strong></p>');
                }
            },
            error: function() {
                // ajax error
            }
        });

        linkClicked = false;

    });

</script>
