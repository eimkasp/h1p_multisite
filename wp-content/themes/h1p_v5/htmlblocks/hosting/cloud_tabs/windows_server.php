<section class="windows-server">
    <div class="container">
        <div class="win-row">
            <div class="win-col win">
                <div class="title-block">
                    <div class="cell"><i class="icon cloud-tabs win"></i></div>
                    <div class="cell"><span class="win-title"><?php _e('Windows Server')?></span></div>
                </div>
                <div class="content">
                    <div class="text-block">
                        <span class="simple-text"><?php _e('Deploy Windows environment on the Cloud and make use of the diversity of Microsoft software - adapt its functionality to improve your performance. Enjoy the resources of a dedicated server for an affordable price.')?></span>
                    </div>
                    <div class="text-block">
                        <span class="text-block-title"><?php _e('Client Access Licenses')?></span>
                        <span class="text"><?php _e('Client Access Licenses allow the connection to Microsoft server software and services.  Client Access Licenses come in different modes, depending on the number of users or computers that are allowed to use Microsoft server software. Need to boost the productivity of your business? Get CAL Suites and embrace more functionality at once.')?></span>
                    </div>
                    <div class="text-block">
                        <span class="text-block-title"><?php _e('When do you need a CAL?')?></span>
                        <span class="text"><?php _e('Most of the Microsoft products and services don’t require a separate Client Access License for each unique user and some do not demand them at all. However, it is highly recommended to check the requirements before you opt for a specific product or a service.')?></span>
                    </div>
                </div>
            </div>
            <div class="win-col rem">
                <div class="title-block">
                    <div class="cell"><i class="icon cloud-tabs remote"></i></div>
                    <div class="cell"><span class="remote-title"><?php _e('Remote Desktop Connection')?></span></div>
                </div>
                <div class="content">
                    <div class="text-block">
                        <span class="simple-text"><?php _e('With Remote Desktop Connection, you can connect to a computer running Windows from another computer connected to the same network or to the Internet. You can use all of your remote computer’s programs, files, and network resources from your personal computer. It’s just like you’re sitting in front of your computer at work – ALL within your reach.')?></span>
                    </div>
                    <div class="text-block">
                        <span class="text-block-title"><?php _e('How to connect to a remote computer?')?></span>
                        <span class="text"><?php _e('Check if your remote computer is turned on and has an active network connection. Make sure that your remote computer is enabled and you have network access to the remote computer. Before you start a connection, check if remote desktop connections are allowed through its firewall.')?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
