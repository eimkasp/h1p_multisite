<div class="container">

    <div class="text-block">
        <span class="simple-text"><?php _e('While moving software and services, online businesses or online projects to the Cloud, exploit Linux functionality with greater flexibility and cost efficiency.  On-demand resources and instant setup save your time and ensure a stable working environment.')?></span>
    </div>

    <div class="text-block">
        <span class="text-block-title"><?php _e('Dedicated RAM and Kernel')?></span>
        <span class="text"><?php _e('All Virtual Machines are powered by XenServer – the leading open virtualization platform for Cloud virtual infrastructure management. Therefore, every Virtual Machine has a separate kernel, this means immaculate performance, higher speed and stability. Tired of network interruptions and limited resources? Exercise your Linux environment on our Cloud and leave your troubles behind.')?></span>
    </div>

</div>
