<div class="container">

    <div class="text-block">
        <span class="simple-text"><?php _e('Trying to figure out the best way how to put your projects online? Cloud Hosting may be the best solution for you.')?></span>
    </div>

    <div class="text-block">
        <ul class="checklist green">
            <li><?php _e('Don’t depend on a specific server which may be exposed to environmental and security threats.')?></li>
            <li><?php _e('Ensure the safety of your data as it is placed within multiple servers, thus preventing data loss or network interruptions.')?></li>
            <li><?php _e('Move to the Cloud and enjoy full control and unlimited scalability.')?></li>
            <li><?php _e('Increase the power and the resources of your server as you grow.')?></li>
        </ul>
    </div>

    <div class="text-block">
        <span class="simple-text"><?php _e('When you need more, you get more. We’re always there.')?></span>
    </div>


</div>
