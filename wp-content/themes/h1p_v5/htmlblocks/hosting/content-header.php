<?php

global $config;

$active_tab = isset($params['selected_tab']) ? $params['selected_tab'] : NULL;



?>

<section class="landing-header <?php echo $active_tab?>">

    <div class="container">
        <div class="title"><?php _e('Choose Your Hosting Solution')?></div>
        <div class="title-descr"><?php _e('Just starting, growing bigger or a real pro? We cover it all!')?></div>
        <div class="descr-text"><?php _e('Opt for the most suited hosting solution and never look back! Our hosting plans are gently built to adapt to your specific needs. Rely on a top performance service with extra-care customer support and a money-back guarantee.')?></div>
        <div class="menu">
            <a id="header-landing-web-hosting" href="<?php echo $config['links']['web-hosting']?>" class="item<?php if($active_tab == 'web-hosting') echo ' active';?>"><?php _e('Web Hosting')?></a>
            <a id="header-landing-vps-hosting" href="<?php echo $config['links']['vps-hosting']?>" class="item<?php if($active_tab == 'vps-hosting') echo ' active';?>"><?php _e('VPS Hosting')?></a>
            <?php /* <a id="header-landing-cloud-hosting" href="<?php echo $config['links']['cloud-hosting']?>" class="item<?php if($active_tab == 'cloud-hosting') echo ' active';?>"><?php _e('Cloud Hosting')?></a> */ ?>
            <a id="header-landing-dedicated-servers" href="<?php echo $config['links']['dedicated-servers']?>" class="item<?php if($active_tab == 'dedicated-servers') echo ' active';?>"><?php _e('Dedicated Servers')?></a>
            <a id="header-landing-reseller-hosting" href="<?php echo $config['links']['reseller-hosting']?>" class="item<?php if($active_tab == 'reseller-hosting') echo ' active';?>"><?php _e('Reseller Hosting')?></a>
            <?php if(get_locale() == 'en_US'): ?>
            <a id="header-landing-seo-hosting" href="<?php echo $config['links']['seo-hosting']?>" class="item<?php if($active_tab == 'seo-hosting') echo ' active';?>"><?php _e('SEO Hosting')?></a>
            <?php endif; ?>
        </div>
    </div>

</section>
