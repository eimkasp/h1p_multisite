<?php

$tags = array(
    'web' => 'web-hosting',
    'vps' => 'vps-hosting',
    'cloud' => 'cloud-hosting',
    'dedicated' => 'dedicated-hosting',
    'reseller' => 'reseller-hosting',
    'seo' => 'seo-hosting',
    'tools' => 'host1plus-tools',
    'ssl' => 'ssl-certificate',
    'affiliate' => ''
);

$tutorials_cat = get_category_by_slug("tutorials");
$tutorials_id = $tutorials_cat->term_id;


$query_params = array(
    'cat' => $tutorials_id,
    'posts_per_page' => 9,
    'orderby' => 'meta_value_num',
    'meta_key' => '_post_views_count',
    'order' => 'DESC'
);

if(isset($params['tags']) && isset( $tags[$params['tags']])) $query_params['tag'] = $tags[$params['tags']];
if(isset($params['queryparams'])) $query_params = array_merge($query_params, $params['queryparams']);

$tutorials = new WP_Query($query_params);

if(count($tutorials->posts)):

?>
<section class="block tutorials bg-white">

    <div class="container">
        <div class="row">

            <h2 class="block-title"><?php _e('Related Tutorials')?></h2>

            <div class="block-content">

                <ul class="tutorials-list">

                    <?php $i = 0; foreach ($tutorials->posts as $post): ?>

                        <li class="tutorial tutorials-item-<?php echo $i+1?>">
                            <i class="fa fa-file-text-o"></i>
                            <a target="_blank" href="<?php echo get_permalink($post->ID)?>" class="title"><?php echo $post->post_title;?></a>
                            <span class="tutorial-data">
                                <span class="rating">
                                    <span class="stars">
                                       <?php if(function_exists("kk_star_ratings_inact")) : echo kk_star_ratings_inact($post->ID); endif; ?>
                                    </span>
                                </span>
                                <a target="_blank" href="<?php echo get_permalink($post->ID)?>#comments" class="comments"><?php echo $post->comment_count ?> <?php _e('comments')?> <i class="fa fa-angle-right"></i></a>
                            </span>
                        </li>

                    <?php $i++; endforeach; ?>

                </ul>

            </div>

            <div class="block-footer">
                <a id="footer-tutorials-show" class="button primary" href="http://www.host1plus.com/tutorials/" target="_blank"><?php _e('Show All')?></a>
            </div>

        </div>
    </div>

</section>

<?php endif;?>
