<?php

$images = $site_current_url . "/wp-content/themes/h1p_v5/img/customers/";

// Change: not taking info from database, creating own version
$reviews_en = array (
        1 => array (
            'name' => 'Ksenia Votinova',
            'photo_url' => 'le-VPN.png',
            'photo_class' => 'le-vpn',
            'review_text' => 'We are very happy to be working with Host1Plus as they proved to be reliable, flexible and responsive to our needs as a VPN provider. Host1Plus helps us guarantee our customers with the fastest VPN tunnels and stable connections, and we couldn’t ask for more.',
            'title' => 'Chief Operating Officer',
            'company' => 'Le VPN'
        ),
        2 => array (
            'name' => 'H W Tovetjärn',
            'photo_url' => 'chakra.png',
            'photo_class' => 'chakra',
            'review_text' => 'Excellent customer service and response time! The Host1Plus support team has remained the same over the years which enhanced familiarity and trust in our relationship with them.',
            'title' => 'Administrator',
            'company' => 'Chakra'
        ),
        3 => array (
            'name' => 'Kevin Fenzi',
            'photo_url' => 'fedora.png',
            'photo_class' => 'fedora',
            'review_text' => 'Generous server, colocation space and bandwidth resources let us serve our content to our European users quickly and reliably.',
            'title' => 'Infrastructure Lead',
            'company' => 'Fedora'
        ),
        4 => array (
            'name' => 'Sameer Ali Khan',
            'photo_url' => 'pre-VPN.png',
            'photo_class' => 'pre-vpn',
            'review_text' => 'Our journey with Host1Plus started back in February, 2014, and we can proudly say that it has been a wonderful one. Host1Plus offers the most complete package with updated hardware, economical pricing and excellent support. With a network that is spanned across 8 data center locations worldwide, we always found Host1Plus offering the best services. One more thing that makes Host1Plus stand-out among the rest is its choice of upstream providers.',
            'title' => 'Associate Engineer',
            'company' => 'PureVPN'
        ),
        5 => array (
            'name' => 'Ali Amjad',
            'photo_url' => 'ivacy.png',
            'photo_class' => 'ivacy',
            'review_text' => 'After all this time with Host1Plus, I only have one word to describe their service, and that is phenomenal. No downtimes, no reduction in speed and no hiccups. Perfect servers, perfect service and great experience overall.',
            'title' => 'System Administrator',
            'company' => 'Ivacy'
        )
    );

$reviews_pt = array (
        1 => array (
            'name' => 'Emanuel Negromonte',
            'photo_url' => 'comunidadesempreupdate.png',
            'photo_class' => 'comunidadesempreupdate',
            'review_text' => 'A Host1Plus surpreendeu a todos em nossa instituição, rapidez no carregamento das páginas, suporte rápido e eficaz, atendimento 5 estrelas, a estabilidade de seus servidores é garantida através dos sistemas Linux e o melhor, os 99% de Uptime realmente existe e é garantido. Realmente o que compramos é entregue!',
            'title' => 'Fundador/CEO',
            'company' => 'Comunidade SempreUpdate'
        ),
        2 => array (
            'name' => 'Sidnei Silva',
            'photo_url' => 'toolse-commerce.png',
            'photo_class' => 'toolse-commerce',
            'review_text' => 'Optamos pela parceria com a Host1Plus, devido ao ótimo nível de atendimento e infraestrutura necessária para nossas aplicações de comércio eletrônico.',
            'title' => 'Gerente de Operações',
            'company' => 'Tools e-Commerce'
        ),
        3 => array (
            'name' => 'Ksenia Votinova',
            'photo_url' => 'le-VPN.png',
            'photo_class' => 'le-vpn',
            'review_text' => 'Estamos muito felizes em trabalhar com a Host1Plus como eles nos provaram serem confiáveis, flexíveis e responsivos às nossas necessidades como um provedor de VPN. Host1Plus nos ajuda a garantir aos nossos clientes túneis VPN mais rápidos e conexões estáveis, e nós não poderíamos pedir mais.',
            'title' => 'Chief Operating Officer',
            'company' => 'Le VPN'
        ),
        4 => array (
            'name' => 'H W Tovetjärn',
            'photo_url' => 'chakra.png',
            'photo_class' => 'chakra',
            'review_text' => 'Serviço ao cliente e tempo de resposta excelente! A equipe de suporte da Host1Plus permaneceu a mesma ao longo dos anos o que aumentou a familiaridade e confiança em nosso relacionamento com eles.',
            'title' => 'Administrator',
            'company' => 'Chakra'
        ),
        5 => array (
            'name' => 'Kevin Fenzi',
            'photo_url' => 'fedora.png',
            'photo_class' => 'fedora',
            'review_text' => 'Servidor generoso, espaço de alocação e recursos de largura de banda deixam-nos servir nosso conteúdo para nossos usuários Europeus de forma rápida e confiável.',
            'title' => 'Infrastructure Lead',
            'company' => 'Fedora'
        ),
        6 => array (
            'name' => 'Sameer Ali Khan',
            'photo_url' => 'pre-VPN.png',
            'photo_class' => 'pre-vpn',
            'review_text' => 'Nossa jornada com a Host1Plus começou em fevereiro de 2014, e podemos dizer com orgulho que tem sido maravilhoso. Host1Plus oferece o pacote mais completo com hardware atualizado, preços econômicos e excelente suporte. Com uma rede espalhada por 8 localidades em centros de dados em todo o mundo, nós sempre encontramos a Host1Plus oferecendo os melhores serviços. Uma coisa mais que faz a Host1Plus destaca-se entre os demais é a sua escolha de provedores upstream.',
            'title' => 'Associate Engineer',
            'company' => 'PureVPN'
        )
    );

$reviews = array();
// Populate and show reviews according to the locale language

if(get_locale() == 'pt_BR') {
    $reviews = $reviews_pt;
} else {
// if(get_locale() == 'en_US')
    $reviews = $reviews_en;
}

if($reviews && sizeof($reviews > 0)) : ?>

<div class="reviews-slider-3">

    <div class="jcarousel-wrapper">

        <div class="jcarousel">

            <ul>

            <?php foreach( $reviews as $key => $value ): ?>

                <li>
                    <span class="review-author-img<?php echo " " . $value['photo_class'];?>">
                        <?php //echo '<img src="' . $images.$value['photo_url'] . '" ' . 'width="86" height="86" />'; ?>
                    </span>
                    <span class="title"><?php echo $value['name']; ?></span>
                    <span class="review">
                        <?php echo $value['review_text']; ?>
                    </span>
                    <span class="review-author-company"><?php echo $value['title']; ?>, <?php echo $value['company']; ?></span>
                </li>

            <?php endforeach; ?>

            </ul>

        </div> <!-- end of .jcarousel -->

        <a href="#" class="jcarousel-control-prev"><i class="fa fa-angle-left fa-lg"></i></a>
        <a href="#" class="jcarousel-control-next"><i class="fa fa-angle-right fa-lg"></i></a>

        <p class="jcarousel-pagination"></p>

    </div>

</div>

<script>
(function($) {
    $(function() {
        $('.jcarousel')
            .on('jcarousel:create jcarousel:reload', function() {
                var element = $(this),
                    elWidth = element.innerWidth(),
                    elHeight = element.outerHeight(true);

                // This shows 1 item at a time.
                // Divide `width` to the number of items you want to display,
                // eg. `width = width / 3` to display 3 items at a time.
                element.jcarousel('items').css('width', elWidth + 'px');
                // position arrows
                element.parent().find('a').css('top',-elHeight);
            })
            .jcarousel({
                wrap: 'circular'
            })
            .jcarouselAutoscroll({
                interval: 4000,
                target: '+=1',
                autostart: true,
                create: $('.jcarousel').hover(function()
                {
                    $(this).jcarouselAutoscroll('stop');
                },
                function()
                {
                    $(this).jcarouselAutoscroll('start');
                })
            })
            .jcarouselSwipe();


        $('.jcarousel-control-prev')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '+=1'
            });


        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .jcarouselPagination({
                item: function(page) {
                    return '<a href="#' + page + '"></a>';
                }
            });
    });
})(jQuery);

</script>

<?php endif; ?>
