<div class="payment-methods">
    <div class="container">
        <h2 class="title">
            Available payment methods
        </h2>
        <div class="payment-methods-wrapper">
            <div class="payment-method">
                <span class="logo americanexpress"></span>
            </div>
            <div class="payment-method">
                <span class="logo visa"></span>
            </div>
            <div class="payment-method">
                <span class="logo mastercard"></span>
            </div>
            <div class="payment-method">
                <span class="logo paypal"></span>
            </div>
            <div class="payment-method">
                <span class="logo skrill"></span>
            </div>
            <div class="payment-method">
                <span class="logo paysera"></span>
            </div>
            <div class="payment-method">
                <span class="logo cashu"></span>
            </div>
            <div class="payment-method">
                <span class="logo ebanx"></span>
            </div>
            <div class="payment-method">
                <span class="logo alipay"></span>
            </div>
            <?php /*<div class="payment-method">
                <span class="logo braintree"></span>
            </div> */ ?>
            <div class="payment-method">
                <span class="logo bitcoin"></span>
            </div>
        </div>
    </div>
</div>