<?php

$reviews = get_reviews();

if ($reviews && sizeof($reviews > 0)) :

    ?>

    <section class="block reviews bg-white">
        <div class="container">
            <div class="row">
                <h2 class="block-title"><?php _e('What people are saying about Host1Plus')?></h2>
                <div class="block-content">
                    <div class="app-description" data-tabs="review" data-rotate="5">
                        <?php $i = 1; foreach ($reviews as $key => $value) { ?>
                            <div class="tab-content review-<?php echo $i; ?> <?php if ($i == 1) echo 'active'; ?>" data-tab-id="<?php echo $i; ?>">
                                <div class="author">
                                    <div class="author-info-primary">
                                        <div class="image">
                                            <?php
                                            if ($value['source'] == "facebook") {
                                                echo '<img src="' . $value['photo_url'] . '" />';
                                            } else if ($value['source'] == "twitter") {
                                                echo '<span class="img-icon"><i class="fa fa-twitter"></i></span>';
                                            }
                                            ?>
                                        </div>
                                        <div class="details">
                                            <div class="author-name"><?php echo $value['name']; ?></div>
                                        </div>
                                    </div>
                                    <div class="author-info-review">
                                        <div class="review">
                                            <?php if(strlen($value['review_text']) > 210):?>
                                                <span class="lesst">“<?php echo substr($value['review_text'], 0, 210);?>...“ <a id="seeMore" onclick="toggleSeeMore(<?php echo $i; ?>, true)" href="#" data-prevent><?php _e('See More')?> <i class="fa fa-angle-right"></i></a></span>
                                                <span class="moret" style="display:none;">“<?php echo $value['review_text'];?>“ <a id="seeMore" onclick="toggleSeeMore(<?php echo $i; ?>, false)" href="#" data-prevent><i class="fa fa-angle-left"></i> <?php _e('See Less')?></a></span>
                                            <?php else: ?>
                                                “<?php echo $value['review_text'];?>“
                                            <?php endif;?>
                                        </div>
                                        <div class="author-social-link"><a href="<?php echo $value['url'] ?>" target="_blank"><i class="fa fa-<?php echo $value['source']; ?>"></i> <?php echo ucfirst($value['source']); ?>  @<?php echo $value['name']; ?></a></div>
                                        <div class="authors-selection">
                                            <ul>
                                                <?php $ii = 1; foreach ($reviews as $key => $value) { ?>
                                                    <li><a href="#" data-prevent data-tab-link="review.<?php echo $ii; ?>"<?php if($ii == 1):?> class="active"<?php endif;?>></a></li>
                                                <?php $ii++; } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php $i++; } ?>
                    </div>
                </div>
                <?php /*
                <div class="block-footer">
                    <a href="https://www.youtube.com/watch?v=qAYqt7NPAMw" target="_blank" class="button primary">See more from our happy customers</a>
                </div>
                */ ?>
            </div>
        </div>
    </section>
    <script>

        function toggleSeeMore(i, show) {



            if (show) {

                jQuery('.block.reviews .review-'+i + ' .review .lesst').hide();
                jQuery('.block.reviews .review-'+i + ' .review .moret').show();
            }

            else {

                jQuery('.block.reviews .review-'+i + ' .review .moret').hide();
                jQuery('.block.reviews .review-'+i + ' .review .lesst').show();

            }
        }

    </script>

    <?php

    endif;

?>
