<?php

global $config;

if( class_exists( 'WPPostCatcher' ) ):

    $set = isset($params['set']) ? $params['set'] : '';

    $set_posts = WPPostCatcher::get_set_posts($set);
    $limit = isset($params['count']) ? $params['count'] : 6;
    $count = min(count($set_posts), $limit);

    if($count):

    ?>

    <section class="block blogposts bg-white">

        <div class="container">
            <div class="row">

                <h2 class="block-title"><?php _e('Related Blog Posts')?></h2>

                <div class="block-content">

                    <ul class="posts-list">

                        <?php for($i=0; $i < $count; $i++):

                            $sp = $set_posts[$i];

                            ?>

                            <li class="blogpost slide blogpost-<?php echo $sp['id'];?>">
                                <a class="image" target="_blank" href="<?php echo $sp['link'];?>">
                                    <img alt="" width="106" height="91" src="<?php echo WPPostCatcher::getThumb($sp['img_url'], 106, 91);?>"/>
                                </a>
                                <div class="description">
                                    <a class="title" target="_blank" href="<?php echo $sp['link'];?>"><?php echo $sp['id'];?> <?php echo $sp['title'];?></a>
                                    <a class="author" target="_blank" href="<?php echo $sp['link'];?>"><?php echo $sp['author'];?></a>
                                </div>
                            </li>

                        <?php endfor; ?>

                    </ul>

                </div>

                <div class="block-footer">
                    <a href="<?php echo $config['links']['blog']?>" target="_blank" class="button primary"><?php _e('Show All')?></a>
                </div>

            </div>
        </div>

    </section>

    <?php

    endif;

endif;

?>
