<section class="block installer-2">

        <div class="container">
            <div class="row">

                <div class="block-text">
                    <div class="title"><?php _e('One-Click Application Auto-Installer')?></div>
                    <div class="text"><strong><?php _e('Build your online project with ease!')?></strong></div>
                    <div class="text"><?php _e('Select the ultimate solution for easier management! Ultra-fast one-step auto-installers will power-up your online project with a single click.')?></div>
                    <div class="text"><?php printf(__('%sInstallatron%s or %sSoftaculous%s cover all your needs – enjoy the most popular applications for community building, content management, e-commerce and business, galleries, statistics, surveys, and many more.'), '<strong>', '</strong>', '<strong>', '</strong>')?></div>
                </div>

                <div class="video-container">
                    <a id="app-installer-video" href="javascript:" class="video" data-popup="https://www.youtube.com/watch?v=wUzL-eJ7klE" data-popup-type="iframe"><span class="button-play"></span></a>
                </div>

            </div>
        </div>

</section>
