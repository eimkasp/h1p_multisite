<?php

global $whmcs;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

function location_status ($status)
{
    switch ($status) {
        case 'active':
            return 'active';
            break;
        case 'pending':
            return 'pending';
            break;
        default:
            return 'pending';
            break;
    }
}


$titles = array(

    'cloud' => array(
        'us_east' => [
                        'title'  => __("Chicago"),
                        'key' => 'us_east',
                        'status' => 'active'
                     ],
        'de'      => [
                        'title'  => __("Frankfurt"),
                        'key' => 'germany',
                        'status' => 'active'
                     ],
        'br'      => [
                        'title'  => __("São Paulo"),
                        'key' => 'brazil',
                        'status' => 'active'
                     ],
        'hk'      => [
                        'title'  => __("Hong Kong"),
                        'key' => 'hongkong',
                        'status' => 'pending'
                     ]
    ),
    'vps' => array(
        'us_west' => [
                        'title'  => __("Los Angeles"),
                        'status' => 'active',
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ],
        'us_east' => [
                        'title'  => __("Chicago"),
                        'status' => 'active',
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ],
        'za'      => [
                        'title'  => __("Johannesburg"),
                        'status' => 'active',
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ],
        'de'      => [
                        'title'  => __("Frankfurt"),
                        'status' => 'active',
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ],
        'br'      => [
                        'title'  => __("São Paulo"),
                        'status' => 'active',
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ],
        'hk'      => [
                        'title'  => __("Hong Kong"),
                        'status' => 'pending',
                     ]
    ),

    'web' => array(
        'us_west' => [
                        'title'  => __("Los Angeles"),
                        'status' => 'active',
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => false
                                    ]
                     ],
        'de'      => [
                        'title'  => __("Frankfurt"),
                        'status' => 'active',
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => false
                                    ]
                     ],
        'br'      => [
                        'title'  => __("São Paulo"),
                        'status' => 'active',
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => false
                                    ]
                     ]
    ),

    'reseller' => array(
        'de'      => [
                        'title'  => __("Frankfurt"),
                        'status' => 'active',
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => false
                                    ]
                     ],
        'br'      => [
                        'title'  => __("São Paulo"),
                        'status' => 'active',
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => false
                                    ]
                     ]
    ),

    'default' => array(
        'us_west' => [
                        'title'  => __("Los Angeles"),
                        'status' => 'active',
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ],
                        'services' => [
                                        'vps',
                                        'web'
                        ]
                     ],
        'us_east' => [
                        'title'  => __("Chicago"),
                        'status' => 'active',
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ],
                        'services' => [
                                        'vps',
                                        'cloud'
                                        /*'web',
                                        'reseller'*/
                        ]
                     ],
        'za'      => [
                        'title'  => __("Johannesburg"),
                        'status' => 'active',
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ],
                        'services' => [
                                        'vps'
                                        /*'web',
                                        'reseller'*/
                        ]
                     ],
        'de'      => [
                        'title'  => __("Frankfurt"),
                        'status' => 'active',
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ],
                        'services' => [
                                        'vps',
                                        'cloud',
                                        'web',
                                        'reseller'
                        ]
                     ],
        'br'      => [
                        'title'  => __("São Paulo"),
                        'status' => 'active',
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ],
                        'services' => [
                                        'vps',
                                        'cloud',
                                        'web',
                                        'reseller'
                        ]
                     ],
        'hk'      => [
                        'title'  => __("Hong Kong"),
                        'status' => 'pending',
                        'services' => [
                                        'vps'
                        ]
                     ]
    )

);

global $config;
global $whmcs;
global $whmcs_promo;


//$client_count = floor($whmcs->getClientsCount()/100)*100;
$client_count = ($whmcs->getClientsCount());


/* VPS cheapest plans calculation
---------------------------------*/

$type = isset($params['type']) ? $params['type'] : 'default';

/*
    Old information. Only for memo

$web_locations_id = array (
        'los_angeles' => '2055',    // cP
        'chicago' => '4146',        // cP
        'london' => '2052',         // DirectAdmin
        'amsterdam' => '3999',      // cP
        'johannesburg' => '4000',   // cP
        'siauliai'  => '3486',      // cP
        'frankfurt' => '2190',      // cP
        'sao_paulo' => '5446'       // cP
    );
*/
$reseller_service_name = array (
        'vps' => __('VPS hosting'),
        'web' => __('Web hosting'),
        'reseller' => __('Reseller hosting'),
        'cloud0' => sprintf (__('Cloud Servers %sCOMING SOON%s'), '<span class="new-item">', '</span>'),
        'cloud' => __('Cloud Servers')
    );



/*Reading params*/
if (isset($params['speedtest'])) {
    $show_speedtest = $params['speedtest'];
} else $show_speedtest = false;

?>


<div class="locations-desktop-map-wrapper">

    <div class="row map">
        <div class="locations-grid">
            <?php

                if(isset($titles[$type]['us_west'])){
                    $location_state_class = location_status ($titles[$type]['us_west']['status']);
                    // closing if at this block end
                
            ?>
                <div class="location loc-us-west">
                    <div class="loc-cont" data-tooltip="locations-<?php echo $type;?>-us-west">
                        <i class="icon location-mark location-flag us"></i>
                        <span class="title"><?php echo $titles[$type]['us_west']['title']; ?> <span class="country"><?php _e('United States');?></span></span>

                        <div class="loc-pop-up" data-tooltip-content="locations-<?php echo $type;?>-us-west">
                            <div class="triangle"></div>
                            <div class="head">
                                <h3 class="pop-up-title"><a href="<?php echo $config['links']['data_centers'];?>#location-losangeles" data-scrollto="location-losangeles" class="location-link"><?php _e('Los Angeles')?>, <?php _e('United States')?></a></h3>
                                <span class="pop-up-descr"><?php _e('Premium data centers in LA, California, guarantee immaculate connectivity and ultra-fast response times.')?>
                                    <a href="<?php echo $config['links']['data_centers'];?>#location-losangeles" data-scrollto="location-losangeles" class="location-link"><?php _e('Read more');?></a>
                                </span>
                                <?php if (($type == 'vps') OR ($type == 'cloud')) { ?>
                                    <br>
                                    <span class="pop-up-descr"><em><?php _e("Best performance across West Coast and Asia-Pacific."); ?></em></span>
                                <?php } ?>
                                <?php if ($mypage->isValidLocation('los_angeles')) { ?>

                                    <div class="popup-action">
                                        <div class="popup-price white">
                                            <div class="inline-block">
                                                <?php _e('from'); ?>
                                                <strong>
                                                    <?php echo $mypage->getMinPrice('los_angeles'); ?>
                                                 </strong>
                                            </div>
                                            <div class="inline-block">

                                                <?php echo $mypage->checkoutFormLocations('los_angeles'); ?>
                                                
                                            </div>
                                        </div>
                                    </div>
                                <?php } elseif (($mypage->isUnavailableLocation('los_angeles')) AND (isset($mypage->service))) { ?>
                                    <div class="popup-action">
                                        <div class="popup-price white">
                                            <div class="inline-block"><?php _e('Temporarily unavailable'); ?></div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="content">
                                <?php if(isset($titles[$type]['us_west']['panels']['cpanel']) && $titles[$type]['us_west']['panels']['cpanel'] === true):?>
                                    <div class="info-row">
                                        <span class="left"><?php _e('cPanel')?></span>
                                        <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset($titles[$type]['us_west']['panels']['dadmin']) && $titles[$type]['us_west']['panels']['dadmin'] === true):?>
                                    <div class="info-row">
                                        <span class="left"><?php _e('DirectAdmin')?></span>
                                        <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                    </div>
                                <?php endif; ?>
                                <?php
                                    if ($type == 'default') {
                                        $loc_services = $titles[$type]['us_west']['services'];
                                        foreach ($loc_services as $s_key => $s_value) {
                                            echo '<div class="info-row">';
                                            echo '<span class="service">' . $reseller_service_name[$s_value] . '</span>';
                                            echo '</div>';
                                        }
                                    }
                                ?>
                                <?php
                                    if (($show_speedtest) AND ($location_state_class == 'active')) {
                                ?>
                                        <div class="location-speedtest">
                                            <a data-popup="<?php echo $config['links']['speedtest']?>?location=losangeles" data-popup-type="ajax" data-popup-id="location-losangeles-speedtest" href="#" data-prevent>
                                                <img class="pull-left" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/locations/speedtest.svg" alt="Speed test">
                                                <span><?php _e('Speed test'); ?></span>
                                            </a>
                                        </div>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>

                    </div> <!-- end of .loc-cont -->
                </div>
            <?php } ?>
            <?php

                if(isset($titles[$type]['us_east'])){
                    $location_state_class = location_status ($titles[$type]['us_east']['status']);
                    // closing if at this block end
                
            ?>
                <div class="location loc-us-east">
                    <div class="loc-cont" data-tooltip="locations-<?php echo $type;?>-us-east">
                        <i class="icon location-mark location-flag us"></i>
                        <span class="title"><?php echo $titles[$type]['us_east']['title']; ?> <span class="country"><?php _e('United States');?></span></span>

                        <div class="loc-pop-up" data-tooltip-content="locations-<?php echo $type;?>-us-east">
                            <div class="triangle"></div>
                            <div class="head">
                                <h3 class="pop-up-title"><a href="<?php echo $config['links']['data_centers'];?>#location-chicago" data-scrollto="location-chicago" class="location-link"><?php _e('Chicago')?>, <?php _e('United States')?></a></h3>
                                <span class="pop-up-descr"><?php _e('A modern data center in Chicago, Illinois, is distinguished by high security standards and thorough server upkeep.')?>
                                    <a href="<?php echo $config['links']['data_centers'];?>#location-chicago" data-scrollto="location-chicago" class="location-link"><?php _e('Read more');?></a>
                                </span>
                                <?php if (($type == 'vps') OR ($type == 'cloud')) { ?>
                                    <br>
                                    <span class="pop-up-descr"><em><?php _e("Best performance across North America."); ?></em></span>
                                <?php } ?>
                                <?php if ($mypage->isValidLocation('chicago')) { ?>
                                    <div class="popup-action">
                                        <div class="popup-price white">
                                            <div class="inline-block">
                                                <?php _e('from'); ?>
                                                <strong>
                                                    <?php echo $mypage->getMinPrice('chicago'); ?>
                                                </strong>
                                            </div>
                                            <div class="inline-block">
                                                
                                                <?php echo $mypage->checkoutFormLocations('chicago'); ?>

                                            </div>
                                        </div>
                                    </div>
                                <?php } elseif (($mypage->isUnavailableLocation('chicago')) AND (isset($mypage->service))) {?>
                                    <div class="popup-action">
                                        <div class="popup-price white">
                                            <div class="inline-block"><?php _e('Temporarily unavailable'); ?></div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="content">
                                <?php if(isset($titles[$type]['us_east']['panels']['cpanel']) && $titles[$type]['us_east']['panels']['cpanel'] === true):?>
                                    <div class="info-row">
                                        <span class="left"><?php _e('cPanel')?></span>
                                        <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset($titles[$type]['us_east']['panels']['dadmin']) && $titles[$type]['us_east']['panels']['dadmin'] === true):?>
                                    <div class="info-row">
                                        <span class="left"><?php _e('DirectAdmin')?></span>
                                        <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                    </div>
                                <?php endif; ?>
                                <?php
                                    if ($type == 'default') {
                                        $loc_services = $titles[$type]['us_east']['services'];
                                        foreach ($loc_services as $s_key => $s_value) {
                                            echo '<div class="info-row">';
                                            echo '<span class="service">' . $reseller_service_name[$s_value] . '</span>';
                                            echo '</div>';
                                        }
                                    }
                                ?>
                                <?php
                                    if (($show_speedtest) AND ($location_state_class == 'active')) {
                                ?>
                                        <div class="location-speedtest">
                                            <a data-popup="<?php echo $config['links']['speedtest']?>?location=chicago" data-popup-type="ajax" data-popup-id="location-chicago-speedtest" href="#" data-prevent>
                                                <img class="pull-left" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/locations/speedtest.svg" alt="Speed test">
                                                <span><?php _e('Speed test'); ?></span>
                                            </a>
                                        </div>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>

                    </div> <!-- end of .loc-cont -->
                </div>
            <?php } ?>
                       
            <?php

                if(isset($titles[$type]['za'])){
                    $location_state_class = location_status ($titles[$type]['za']['status']);

            ?>
                <div class="location loc-za">
                    <div class="loc-cont" data-tooltip="locations-<?php echo $type;?>-za">
                        <i class="icon location-mark location-flag za"></i>
                        <span class="title"><?php echo $titles[$type]['za']['title']; ?> <span class="country"><?php _e('South Africa');?></span></span>

                        <div class="loc-pop-up" data-tooltip-content="locations-<?php echo $type;?>-za">
                            <div class="triangle"></div>
                            <div class="head">
                                <h3 class="pop-up-title"><a href="<?php echo $config['links']['data_centers'];?>#location-johannesburg" data-scrollto="location-johannesburg" class="location-link"><?php _e('Johannesburg')?>, <?php _e('South Africa')?></a></h3>
                                <span class="pop-up-descr"><?php _e('A reliable server environment in Johannesburg guarantees stability and uninterruptible server performance.')?>
                                    <a href="<?php echo $config['links']['data_centers'];?>#location-johannesburg" data-scrollto="location-johannesburg" class="location-link"><?php _e('Read more');?></a>
                                </span>
                                <?php if (($type == 'vps') OR ($type == 'cloud')) { ?>
                                    <br>
                                    <span class="pop-up-descr"><em><?php _e("Best performance across Africa."); ?></em></span>
                                <?php } ?>
                                <?php if ($mypage->isValidLocation('johannesburg')) { ?>
                                    <div class="popup-action">
                                        <div class="popup-price white">
                                            <div class="inline-block">
                                                <?php _e('from'); ?>
                                                <strong>
                                                    <?php echo $mypage->getMinPrice('johannesburg'); ?>
                                                </strong>
                                            </div>
                                            <div class="inline-block">
                                                
                                                <?php echo $mypage->checkoutFormLocations('johannesburg'); ?>
                                                
                                            </div>
                                        </div>
                                    </div>
                                <?php } elseif (($mypage->isUnavailableLocation('johannesburg')) AND (isset($mypage->service))) { ?>
                                    <div class="popup-action">
                                        <div class="popup-price white">
                                            <div class="inline-block"><?php _e('Temporarily unavailable'); ?></div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="content">
                                <?php if(isset($titles[$type]['za']['panels']['cpanel']) && $titles[$type]['za']['panels']['cpanel'] === true):?>
                                    <div class="info-row">
                                        <span class="left"><?php _e('cPanel')?></span>
                                        <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset($titles[$type]['za']['panels']['dadmin']) && $titles[$type]['za']['panels']['dadmin'] === true):?>
                                    <div class="info-row">
                                        <span class="left"><?php _e('DirectAdmin')?></span>
                                        <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                    </div>
                                <?php endif; ?>
                                <?php
                                    if ($type == 'default') {
                                        $loc_services = $titles[$type]['za']['services'];
                                        foreach ($loc_services as $s_key => $s_value) {
                                            echo '<div class="info-row">';
                                            echo '<span class="service">' . $reseller_service_name[$s_value] . '</span>';
                                            echo '</div>';
                                        }
                                    }
                                ?>
                                <?php
                                    if (($show_speedtest) AND ($location_state_class == 'active')) {
                                ?>
                                        <div class="location-speedtest">
                                            <a data-popup="<?php echo $config['links']['speedtest']?>?location=johannesburg" data-popup-type="ajax" data-popup-id="location-johannesburg-speedtest" href="#" data-prevent>
                                                <img class="pull-left" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/locations/speedtest.svg" alt="Speed test">
                                                <span><?php _e('Speed test'); ?></span>
                                            </a>
                                        </div>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>

                    </div> <!-- end of .loc-cont -->
                </div>
            <?php } ?>
            <?php

                if(isset($titles[$type]['de'])){

                    $location_state_class = location_status ($titles[$type]['de']['status']);
                    // closing if at this block end

            ?>
                <div class="location loc-de">
                    <div class="loc-cont" data-tooltip="locations-<?php echo $type;?>-de">
                        <i class="icon location-mark location-flag de"></i>
                        <span class="title"><?php echo $titles[$type]['de']['title']; ?> <span class="country"><?php _e('Germany');?></span></span>

                        <div class="loc-pop-up" data-tooltip-content="locations-<?php echo $type;?>-de">
                            <div class="triangle"></div>
                            <div class="head">
                                <h3 class="pop-up-title"><a href="<?php echo $config['links']['data_centers'];?>#location-frankfurt" data-scrollto="location-frankfurt" class="location-link"><?php _e('Frankfurt')?>, <?php _e('Germany')?></a></h3>
                                <span class="pop-up-descr"><?php _e('Being one of the most secure and reliable data centers in the world, our server location in Frankfurt sustains uptime and high performance.')?>
                                    <a href="<?php echo $config['links']['data_centers'];?>#location-frankfurt" data-scrollto="location-frankfurt" class="location-link"><?php _e('Read more');?></a>
                                </span>
                                <?php if (($type == 'vps') OR ($type == 'cloud')) { ?>
                                    <br>
                                    <span class="pop-up-descr"><em><?php _e("Best performance across Europe and Middle East."); ?></em></span>
                                <?php } ?>
                                <?php if ($mypage->isValidLocation('frankfurt')) { ?>
                                    <div class="popup-action">
                                        <div class="popup-price white">
                                            <div class="inline-block">
                                                <?php _e('from'); ?>
                                                <strong>
                                                    <?php echo $mypage->getMinPrice('frankfurt'); ?>
                                                </strong>
                                            </div>
                                            <div class="inline-block">
                                                
                                                <?php echo $mypage->checkoutFormLocations('frankfurt'); ?>
                                                
                                            </div>
                                        </div>
                                    </div>
                                <?php } elseif (($mypage->isUnavailableLocation('frankfurt')) AND (isset($mypage->service))) { ?>
                                    <div class="popup-action">
                                        <div class="popup-price white">
                                            <div class="inline-block"><?php _e('Temporarily unavailable'); ?></div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="content">
                                <?php if(isset($titles[$type]['de']['panels']['cpanel']) && $titles[$type]['de']['panels']['cpanel'] === true):?>
                                    <div class="info-row">
                                        <span class="left"><?php _e('cPanel')?></span>
                                        <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset($titles[$type]['de']['panels']['dadmin']) && $titles[$type]['de']['panels']['dadmin'] === true):?>
                                    <div class="info-row">
                                        <span class="left"><?php _e('DirectAdmin')?></span>
                                        <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                    </div>
                                <?php endif; ?>
                                <?php
                                    if ($type == 'default') {
                                        $loc_services = $titles[$type]['de']['services'];
                                        foreach ($loc_services as $s_key => $s_value) {
                                            echo '<div class="info-row">';
                                            echo '<span class="service">' . $reseller_service_name[$s_value] . '</span>';
                                            echo '</div>';
                                        }
                                    }
                                ?>
                                <?php
                                    if (($show_speedtest) AND ($location_state_class == 'active')) {
                                ?>
                                        <div class="location-speedtest">
                                            <a data-popup="<?php echo $config['links']['speedtest']?>?location=frankfurt" data-popup-type="ajax" data-popup-id="location-frankfurt-speedtest" href="#" data-prevent>
                                                <img class="pull-left" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/locations/speedtest.svg" alt="Speed test">
                                                <span><?php _e('Speed test'); ?></span>
                                            </a>
                                        </div>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>

                    </div> <!-- end of .loc-cont -->
                </div>
            <?php } ?>
            <?php

                if(isset($titles[$type]['br'])){
                    $location_state_class = location_status ($titles[$type]['br']['status']);

            ?>
                <div class="location loc-br <?php echo $location_state_class; ?>">
                    <div class="loc-cont" data-tooltip="locations-<?php echo $type;?>-br">
                        <i class="icon location-mark location-flag br animate <?php echo $location_state_class; ?>"></i>
                        <span class="title"><?php echo $titles[$type]['br']['title']; ?> <span class="country"><?php _e('Brazil');?></span></span>

                        <div class="loc-pop-up" data-tooltip-content="locations-<?php echo $type;?>-br">
                            <div class="triangle"></div>
                            <div class="head">
                            <?php if ($type == 'cloud'): ?>
                                <div class="text-left p-b-10">
                                    <span class="badge new-item"><?php _e('New'); ?></span>
                                    <span class="badge beta-item" style="margin-left: 5px;"><?php _e('Beta'); ?></span>
                                </div>
                            <?php endif; ?>
                                <h3 class="pop-up-title"><a href="<?php echo $config['links']['data_centers'];?>#location-saopaulo" data-scrollto="location-saopaulo" class="location-link"><?php _e('São Paulo')?>, <?php _e('Brazil')?></a></h3>
                                <span class="pop-up-descr"><?php _e('With a wide range of security equipment, the facility in São Paulo upholds a secure and reliable environment.')?>
                                    <a href="<?php echo $config['links']['data_centers'];?>#location-saopaulo" data-scrollto="location-saopaulo" class="location-link"><?php _e('Read more');?></a>
                                </span>
                                <?php if (($type == 'vps') OR ($type == 'cloud')) { ?>
                                    <br>
                                    <span class="pop-up-descr"><em><?php _e("Best performance across South America."); ?></em></span>
                                <?php } ?>
                                <?php if ($mypage->isPendingLocation('sao_paulo')) { ?>
                                    <span class="attention-box">
                                        <span class="inline-block">
                                            <?php _e('Coming soon')?>
                                        </span>
                                    </span>
                                <?php } ?>

                                <?php if ($mypage->isValidLocation('sao_paulo')) { ?>
                                    <div class="popup-action">
                                        <div class="popup-price white">
                                            <div class="inline-block">
                                                <?php _e('from'); ?>
                                                <strong>
                                                    <?php echo $mypage->getMinPrice('sao_paulo'); ?>
                                                </strong>
                                            </div>
                                            <div class="inline-block">
                                                
                                                <?php echo $mypage->checkoutFormLocations('sao_paulo'); ?>                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                <?php } elseif (($mypage->isUnavailableLocation('sao_paulo')) AND (isset($mypage->service))) { ?>
                                    <div class="popup-action">
                                        <div class="popup-price white">
                                            <div class="inline-block"><?php _e('Temporarily unavailable'); ?></div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="content">
                                <?php if(isset($titles[$type]['br']['panels']['cpanel']) && $titles[$type]['br']['panels']['cpanel'] === true):?>
                                    <div class="info-row">
                                        <span class="left"><?php _e('cPanel')?></span>
                                        <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset($titles[$type]['br']['panels']['dadmin']) && $titles[$type]['br']['panels']['dadmin'] === true):?>
                                    <div class="info-row">
                                        <span class="left"><?php _e('DirectAdmin')?></span>
                                        <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                    </div>
                                <?php endif; ?>
                                <?php
                                    if ($type == 'default') {
                                        $loc_services = $titles[$type]['br']['services'];
                                        foreach ($loc_services as $s_key => $s_value) {
                                            echo '<div class="info-row">';
                                            echo '<span class="service">' . $reseller_service_name[$s_value] . '</span>';
                                            echo '</div>';
                                        }
                                    }
                                ?>
                                <?php
                                    if (($show_speedtest) AND ($location_state_class == 'active')) {
                                ?>
                                        <div class="location-speedtest">
                                            <a data-popup="<?php echo $config['links']['speedtest']?>?location=saopaulo" data-popup-type="ajax" data-popup-id="location-saopaulo-speedtest" href="#" data-prevent>
                                                <img class="pull-left" src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/locations/speedtest.svg" alt="Speed test">
                                                <span><?php _e('Speed test'); ?></span>
                                            </a>
                                        </div>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>

                    </div> <!-- end of .loc-cont -->
                </div>
            <?php } ?>
            <?php

                if(isset($titles[$type]['hk'])){
                    $location_state_class = location_status ($titles[$type]['hk']['status']);

            ?>
                <div class="location loc-hk <?php echo $location_state_class; ?>">
                    <div class="loc-cont" data-tooltip="locations-<?php echo $type;?>-hk">
                        <i class="icon location-mark location-flag hk <?php echo $location_state_class; ?>"></i>
                        <span class="title"><?php echo $titles[$type]['hk']['title']; ?> <span class="country"><?php _e('Hong Kong');?></span></span>

                        <div class="loc-pop-up" data-tooltip-content="locations-<?php echo $type;?>-hk">
                            <div class="triangle"></div>
                            <div class="head">
                                <h3 class="pop-up-title"><?php _e('Hong Kong')?></h3>
                                <span class="pop-up-descr"><?php _e('One of the world’s most networked data centers serves as a gateway to Asia.')?></span>
                                <?php if (($type == 'vps') OR ($type == 'cloud')) { ?>
                                    <br>
                                    <span class="pop-up-descr"><em><?php _e("Best performance across Asia."); ?></em></span>
                                <?php } ?>
                                <?php if ($mypage->isPendingLocation('hongkong')) { ?>
                                    <span class="attention-box">
                                        <span class="inline-block">
                                            <?php _e('Coming soon')?>
                                        </span>
                                    </span>
                                <?php } ?>

                            </div>
                            <div class="content">
                            <?php
                                if ($type == 'default') {
                            ?>
                                    <div class="info-row">
                                        <span class="left"><strong><?php _e('Coming soon')?></strong></span>
                                    </div>
                            <?php } ?>
                            </div>

                        </div>

                    </div> <!-- end of .loc-cont -->
                </div>
            <?php } ?>
        </div>


    </div> <!-- end of .row -->

<?php
    if ( $type != 'default' ) {

    // Display On SERVICE page

?>
    <div class="row">
        <div class="block-col why-location-link center">
            <div class="white has-tooltip" data-tooltip="why-choose-location">
                <?php _e('Why choose a location?'); ?>
            </div>
            <div class="loc-info-popup center w-600" data-tooltip-content="why-choose-location"><?php _e('When choosing a location you should consider the proximity of your target audience, as the distance between the server and the user affects your website loading time.'); ?>
                <div class="triangle"></div>
            </div>
        </div>
    </div>

<?php
    } else {

    // Display On HOME and ABOUT US page
?>

    <div class="row">
        <div class="block-col clients-total-info center">
            <div class="client-counter-wrapper">
                <div id="client-counter"><?php echo $client_count ?></div>
            </div>
            <div class="">
                <?php _e('Proudly hosting clients all around the world.'); ?>
            </div>
        </div>
    </div>

<script>
    (function($){

        $(document).ready(function(){

            function isScrolledIntoView(elem){
                var docViewTop = $(window).scrollTop();
                var docViewBottom = docViewTop + $(window).height();
                var elemTop = $(elem).offset().top;
                var elemBottom = elemTop + $(elem).height();
                return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom) && (elemBottom <= docViewBottom) && (elemTop >= docViewTop));
            }

            function counterAction(){
                if (!isStarted) {

                    if(isScrolledIntoView($('#client-counter'))){

                        isStarted = true;
                        var total = document.getElementById('client-counter');

                        $('#client-counter').each(function () {
                            var $this = $(this);
                            $(this).prop('Counter',0).animate({
                                Counter: $(this).text()
                            }, {
                                duration: 4000,
                                easing: 'easeOutCubic',

                                step: function() {
                                    var num = Math.ceil(this.Counter).toString();
                                    if(Number(num) > 999){
                                        while (/(\d+)(\d{3})/.test(num)) {
                                            num = num.replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
                                        }
                                    }
                                    $this.text(num);
                                }
                            });
                        });

                    }
                }
            }

            var isStarted = false;

            counterAction();

            $(window).scroll(function(){
                counterAction();
            })
            $(window).resize(function(){
                counterAction();
            })
        });

    })(jQuery)
</script>
<?php
    }
?>
</div> <!-- end of .locations-desktop-map-wrapper -->

<script>
    (function($){

        $(document).ready(function(){
            $('.loc-cont').hover(function(){

            })
        });

    })(jQuery)
</script>
