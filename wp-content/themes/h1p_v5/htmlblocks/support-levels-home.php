<?php

global $whmcs;
global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

// Check these IDs !!!
$addons_extracare_1level = $whmcs->getAddonsPrices(27);
$addons_extracare_2level = $whmcs->getAddonsPrices(28);

// print_r ($addons_extracare_1level);
// print_r ($addons_extracare_2level);

function get_addon_billing_cycle ( $addon_set ) {
    switch ( $addon_set['billingcycle'] ) {
        case 'Free':
            return __('free');
            break;
        case 'One Time':
            return __('/hourly');
            break;
        case 'Monthly':
            return __('/monthly');
            break;
        case 'Quarterly':
            return __('/quarterly');
            break;
        case 'Semi-Annually':
            return __('/semi-annually');
            break;
        case 'Annually':
            return __('/annually');
            break;
        case 'Biennially':
            return __('/biennially');
            break;
        case 'Triennially':
            return __('/triennially');
            break;

        default:
            return false;
            break;
    }
}

?>


<div class="layout-row three-col-row">

    <div class="block-col white-styled green-styled center">
        <h3 data-toggle-wrapper="1"><?php _e('General Support');?></h3>
        <span class="details-info" data-toggle-wrapper><?php _e('Unlimited, free');?></span>
        <div class="details-icon" data-toggle-wrapper="1"><img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/supportlevel-general.svg" alt="Ulimited" width="120"></div>
        <p class="details-description"><?php _e('All services are fully covered by our general support. Feel free to drop us a line and we’ll get back to you with all the answers.');?></p>
    </div>

    <div class="block-col white-styled blue-styled center">
        <h3 data-toggle-wrapper="2"><?php _e('1st Level Extra-Care');?></h3>
        <span class="details-info" data-toggle-wrapper>
        <?php
            echo $whmcs::$settings['currency_prefix'];
            echo $addons_extracare_1level['price'];
            $period1 = get_addon_billing_cycle ( $addons_extracare_1level );
            if ( $period1 ) echo _e( $period1 );
        ?>
        </span>
        <div class="details-icon" data-toggle-wrapper="2"><img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/supportlevel-1st.svg" alt="Monthly" width="120"></div>
        <p class="details-description"><?php _e('Leave advanced server management for us! You will get 4 hours of assistance for server management and configuration.');?></p>
    </div>
    <div class="block-col white-styled red-styled center">
        <h3 data-toggle-wrapper="3"><?php _e('2nd Level Extra-Care');?></h3>
        <span class="details-info" data-toggle-wrapper>
        <?php
            echo $whmcs::$settings['currency_prefix'];
            echo $addons_extracare_2level['price'];
            $period2 = get_addon_billing_cycle ( $addons_extracare_2level );
            if ( $period2 ) echo _e( $period2 );
        ?>
        </span>
        <div class="details-icon" data-toggle-wrapper="3"><img src="<?php echo $images ?>img/placeholder.svg" data-lazy="<?php echo $images ?>img/features/supportlevel-2nd.svg" alt="Hourly" width="120"></div>
        <p class="details-description"><?php _e('Need more? Opt for a one-time hourly service to receive dedicated support for the most complex server issue resolution.');?></p>
    </div>

</div> <!-- end of .layout-row .three-col-row -->
