<div class="what-else">
    <h2 class="title"><?php _e('Why choose Host1Plus?');?></h2>

    <div class="block-tabs">
        <div class="tabs-wrapper">
            <div class="app-tabs">
                <div class="table">
                    <div class="cell"><span data-tab-link="feature.money-back" class="tab desktop active"><?php _e('14-DAY MONEY-BACK GUARANTEE');?></span></div>
                    <div class="cell"><span data-tab-link="feature.flexible" class="tab desktop"><?php _e('FLEXIBLE PAYMENT OPTIONS');?></span></div>
                    <div class="cell"><span data-tab-link="feature.support" class="tab desktop"><?php _e('24/7 TECH SUPPORT');?></span></div>
                    <div class="cell"><span data-tab-link="feature.savings" class="tab desktop"><?php _e('MAJOR SAVINGS');?></span></div>
                    <div class="cell"><span data-tab-link="feature.responsive" class="tab desktop"><?php _e('RESPONSIVE CLIENT AREA');?></span></div>
                </div>
            </div>
            <div class="tabs" data-tabs="feature">

                <span data-tab-link="feature.money-back" data-tab-toggle="self" class="tab mob"><?php _e('14-DAY MONEY-BACK GUARANTEE');?></span>
                <div class="tab-content active" data-tab-id="money-back">
                    <img src="<?php bloginfo('template_directory'); ?>/img/features/guarantee.jpg">
                    <span class="title"><?php _e('14-DAY MONEY-BACK GUARANTEE');?></span>
                    <p><?php _e('Put us to the test! Try our services without any risk and get your money back if you change your mind.');?></p>
                </div>

                <span data-tab-link="feature.flexible" data-tab-toggle="self" class="tab mob"><?php _e('FLEXIBLE PAYMENT OPTIONS');?></span>
                <div class="tab-content" data-tab-id="flexible">
                    <img src="<?php bloginfo('template_directory'); ?>/img/features/payment.jpg">
                    <span class="title"><?php _e('FLEXIBLE PAYMENT OPTIONS');?></span>
                    <p><?php _e('Choose from a variety of different payment options, including Credit Card, Paypal, Skrill, Paysera, CashU, Ebanx, Braintree, Alipay transactions and Bitcoin payments.');?></p>
                </div>

                <span data-tab-link="feature.support" data-tab-toggle="self" class="tab mob"><?php _e('24/7 CUSTOMER SUPPORT');?></span>
                <div class="tab-content" data-tab-id="support">
                    <img src="<?php bloginfo('template_directory'); ?>/img/features/support.jpg">
                    <span class="title"><?php _e('24/7 TECH SUPPORT');?></span>
                    <p><?php _e('Problems with your VPS hosting? Our experienced in-house tech support is capable of solving your issues in seconds! Tech support is available in English, Portuguese, Spanish and Lithuanian.');?></p>
                </div>

                <span data-tab-link="feature.savings" data-tab-toggle="self" class="tab mob"><?php _e('MAJOR SAVINGS');?></span>
                <div class="tab-content" data-tab-id="savings">
                    <img src="<?php bloginfo('template_directory'); ?>/img/features/savings.jpg">
                    <span class="title"><?php _e('MAJOR SAVINGS');?></span>
                    <p><?php _e('Sign up for an extended billing cycle and save up to 15% for your purchase!');?></p>
                </div>

                <span data-tab-link="feature.responsive" data-tab-toggle="self" class="tab mob"><?php _e('RESPONSIVE CLIENT AREA');?></span>
                <div class="tab-content" data-tab-id="responsive">
                    <img src="<?php bloginfo('template_directory'); ?>/img/features/responsive.jpg">
                    <span class="title"><?php _e('RESPONSIVE CLIENT AREA');?></span>
                    <p><?php _e('Always on the go? Easily manage your services using any mobile device.');?></p>
                </div>
            </div>
        </div>
    </div>

</div>