<section class="block domain-how-to">
    <div class="container">
        <div class="row">

            <h2 class="block-title"><?php _e('How to choose a domain?')?></h2>

            <div class="how-to-blocks">
                <div class="text-row">
                    <div class="text-block col-1">
                        <div class="title"><?php _e('Protect your brand!')?></div>
                        <div class="subtitle"><?php _e('Secure your name before someone else does')?></div>
                        <div class="text"><?php _e('Think fast! Use pre-reservation - snap your domain and shield your brand name before your competition makes advantage of your hesitation.')?></div>
                    </div>
                    <div class="text-block col-2">
                        <div class="title"><?php _e('Keep it short!')?></div>
                        <div class="subtitle"><?php _e('Brevity is the soul of wit')?></div>
                        <div class="text"><?php _e('Opt for a short and simple name to avoid common mistakes – don’t ever use complex and long domain names as they are misleading your visitors.')?></div>
                    </div>
                </div>
                <div class="text-row">
                    <div class="text-block col-3">
                        <div class="title"><?php _e('Boost SEO!')?></div>
                        <div class="subtitle"><?php _e('Ensure a better search engine placement')?></div>
                        <div class="text"><?php _e('Enjoy higher ranking in search engine results – while your domain name is wisely selected, you don’t have to worry about SEO so much. Choose a proper title, get noticed faster and appeal new customers.')?></div>
                    </div>
                    <div class="text-block col-4">
                        <div class="title"><?php _e('Stand out!')?></div>
                        <div class="subtitle"><?php _e('Contrast with your competitors')?></div>
                        <div class="text"><?php _e('A variety of top level domains offer a great advantage over your contenders – clarify the focus of your web presence and settle in your niche.')?></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
