<div id="cookies-policy-accept">
    <p><?php global $config; printf(__('We use cookies to improve our services and provide a better experience. By continuing to use this site, you agree with our %sCookies Policy%s.'), '<a href="' . $config['links']['terms_of_service'] . '#tos-section-9-4" target="_blank">', '</a>')?> <a class="close" href="javascript:accepCookiesPolicy()"><i class="fa fa-times"></i></a></p>
</div>
