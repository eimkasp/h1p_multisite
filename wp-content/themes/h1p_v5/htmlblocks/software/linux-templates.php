<section class="block software">

    <div class="container">
        <div class="row">

            <div class="block-content">

                <div class="software-slider">

                    <div class="slide">

                        <div class="slide-block os linux-templates">
                                <span class="title">Linux OS Templates</span>
                                <span class="description">Quo dolor definiebas an. Mel duis possit invidunt at. Et vidit clita aeterno per, his congue omnium option ne.</span>
                                <ul class="os-grid">
                                    <li><i class="icon linux-os ubuntu"></i></li>
                                    <li><i class="icon linux-os centos"></i></li>
                                    <li><i class="icon linux-os opensuse"></i></li>
                                    <li><i class="icon linux-os fedora"></i></li>
                                    <li><i class="icon linux-os debian"></i></li>
                                    <li><i class="icon linux-os scientific"></i></li>
                                </ul>
                        </div>

                        <div class="slide-block-sep"></div>

                        <div class="slide-block os-software">
                            <div class="block-row cpanel">
                                <div class="row-header">
                                    <span class="title">cPanel</span>
                                    <span class="price">$12.00 per month</span>
                                </div>
                                <div class="row-content">
                                    <span class="description">Quo dolor definiebas an. Mel duis possit invidunt at. Et vidit clita aeterno per, his congue omnium option ne.</span>
                                    <span class="logo"><i class="icon software cpanel"></i></span>
                                </div>
                            </div>
                            <div class="block-row ispconfig">
                                <div class="row-header">
                                    <span class="title">ISPConfig</span>
                                    <span class="price">$12.00 per month</span>
                                </div>
                                <div class="row-content">
                                    <span class="description">Quo dolor definiebas an. Mel duis possit invidunt at. Et vidit clita aeterno per, his congue omnium option ne.</span>
                                    <span class="logo"><i class="icon software ispconfig"></i></span>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>

</section>