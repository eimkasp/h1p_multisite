<section class="block software">

    <div class="container">
        <div class="row">

            <div class="block-content">

                <div class="software-slider">

                    <div class="slide">

                        <div class="slide-block os windows-server">
                                <span class="title">Windows OS</span>
                                <span class="description">Quo dolor definiebas an. Mel duis possit invidunt at. Et vidit clita aeterno per, his congue omnium option ne.</span>
                                <span class="logo"><i class="icon windows-server"></i></span>
                        </div>

                        <div class="slide-block-sep"></div>

                        <div class="slide-block os-software">
                            <div class="block-row metatrader">
                                <div class="row-header">
                                    <span class="title">Meta Trader</span>
                                    <span class="price">$10.00 per month</span>
                                </div>
                                <div class="row-content">
                                    <span class="description">Quo dolor definiebas an. Mel duis possit invidunt at. Et vidit clita aeterno per, his congue omnium option ne.</span>
                                    <span class="logo"><i class="icon software metatrader"></i></span>
                                </div>
                            </div>
                            <div class="block-row enkompass">
                                <div class="row-header">
                                    <span class="title">Enkompass</span>
                                    <span class="price">$22.00 per month</span>
                                </div>
                                <div class="row-content">
                                    <span class="description">Quo dolor definiebas an. Mel duis possit invidunt at. Et vidit clita aeterno per, his congue omnium option ne.</span>
                                    <span class="logo"><i class="icon software enkompass"></i></span>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="slide">

                </div>

            </div>

        </div>
    </div>

</section>