<section class="block software">

    <div class="container">
        <div class="row">

            <div class="block-content">

                <div class="software-slider">

                    <div class="slide">

                        <div class="slide-block features">
                            <div class="block-row cpanel-whm">
                                <span class="title">All Plans Include</span>
                                <ul class="features-list">
                                    <li><span class="highlight">FREE</span> IPv4 Adresses and ability to get more!</li>
                                    <li><span class="highlight">FREE</span> DirectADmin or cPanel Setup!</li>
                                    <li><span class="highlight">FREE</span> Vis mentitum iudicabit maiestatis ut, un est nulla</li>
                                    <li><span class="highlight">100Mbps</span> or <span class="highlight">1Gbps Unlimited</span> Network</li>
                                    <li>Highly Availability Data Center Offering <span class="highlight">99.9% Up-Time</span></li>
                                    <li><span class="highlight">Quick Setup</span> and Instant Support</li>
                                </ul>
                            </div>
                        </div>

                        <div class="slide-block-sep"></div>

                        <div class="slide-block os windows-server small">
                            <span class="title">Windows OS</span>
                            <span class="description">Quo dolor definiebas an. Mel duis possit invidunt at. Et vidit clita aeterno per, his congue omnium option ne.</span>
                            <span class="logo"><i class="icon windows-server w-207"></i></span>
                        </div>

                        <div class="slide-block-sep"></div>

                        <div class="slide-block os linux small">
                            <span class="title">Linux OS</span>
                            <span class="description">Quo dolor definiebas an. Mel duis possit invidunt at. Et vidit clita aeterno per, his congue omnium option ne.</span>
                            <span class="logo"><i class="icon linux"></i></span>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>

</section>