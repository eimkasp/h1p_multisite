<section class="block software">

    <div class="container">
        <div class="row">

            <div class="block-content">

                <div class="software-slider">

                    <div class="slide">

                        <div class="slide-block software">
                            <span class="title">Webmin</span>
                            <span class="price">$10.00 per month</span>
                            <span class="description">Quo dolor definiebas an. Mel duis possit invidunt at. Et vidit clita aeterno per, his congue omnium option ne.</span>
                            <span class="logo"><i class="icon software webmin"></i></span>
                        </div>

                        <div class="slide-block-sep"></div>

                        <div class="slide-block software">
                            <span class="title">Webuzo</span>
                            <span class="price">$10.00 per month</span>
                            <span class="description">Quo dolor definiebas an. Mel duis possit invidunt at. Et vidit clita aeterno per, his congue omnium option ne.</span>
                            <span class="logo"><i class="icon software webuzo"></i></span>
                        </div>

                        <div class="slide-block-sep"></div>

                        <div class="slide-block software">
                            <span class="title">GHOST</span>
                            <span class="price">$10.00 per month</span>
                            <span class="description">Quo dolor definiebas an. Mel duis possit invidunt at. Et vidit clita aeterno per, his congue omnium option ne.</span>
                            <span class="logo"><i class="icon software"></i></span>
                        </div>

                        <div class="slide-block-sep"></div>

                        <div class="slide-block software">
                            <span class="title">Vesta CP</span>
                            <span class="price">$10.00 per month</span>
                            <span class="description">Quo dolor definiebas an. Mel duis possit invidunt at. Et vidit clita aeterno per, his congue omnium option ne.</span>
                            <span class="logo"><i class="icon software vesta"></i></span>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>

</section>