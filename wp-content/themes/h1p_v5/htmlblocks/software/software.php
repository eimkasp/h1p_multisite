<section class="block software">

    <div class="container">
        <div class="row">

            <div class="block-content">

                <div class="software-slider">

                    <div class="slide">

                        <div class="slide-block software">
                            <span class="title">CloudLinux</span>
                            <span class="price">$10.00 per month</span>
                            <span class="description">Quo dolor definiebas an. Mel duis possit invidunt at. Et vidit clita aeterno per, his congue omnium option ne.</span>
                            <span class="logo"><i class="icon software cloudlinux"></i></span>
                        </div>

                        <div class="slide-block-sep"></div>

                        <div class="slide-block software">
                            <span class="title">KernelCare</span>
                            <span class="price">$10.00 per month</span>
                            <span class="description">Quo dolor definiebas an. Mel duis possit invidunt at. Et vidit clita aeterno per, his congue omnium option ne.</span>
                            <span class="logo"><i class="icon software kernelcare"></i></span>
                        </div>

                        <div class="slide-block-sep"></div>

                        <div class="slide-block software">
                            <span class="title">LAMP</span>
                            <span class="price free">Free</span>
                            <span class="description">Quo dolor definiebas an. Mel duis possit invidunt at. Et vidit clita aeterno per, his congue omnium option ne.</span>
                            <span class="logo"><i class="icon software lamp"></i></span>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>

</section>