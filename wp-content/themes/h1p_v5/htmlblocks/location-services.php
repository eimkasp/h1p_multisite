<div class="location-services tablet-pad" data-selected-loc="us-ch">
    <div class="container">
        <h2 class="title">
            The closer – the better. Ensure lower latency and high connection speed by selecting a nearby Linux hosting location
        </h2>
        <div class="locations-container">
            <div class="flags">

                <div class="flag us-ch active" data-loc="us-ch">

                    <div class="ico"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/US.png" alt="" width="64px"/></div>
                    <div class="label">Chicago</div>
                    <div class="content mob">
                        A modern data center in Chicago, Illinois, is distinguished by high security standards and thorough server upkeep. Professional network administrators are always there to ensure high connection speed and low latency, especially to Middle and East Coast USA.
                        <br><br>
                        <div class="services">Available hosting services: <a href="<?php echo $config['links']['web-hosting']?>">web hosting</a>, <a href="<?php echo $config['links']['vps-hosting']?>">VPS hosting</a>, <a href="<?php echo $config['links']['reseller-hosting']?>">reseller hosting</a>.</div>
                    </div>

                </div><div class="flag us-la" data-loc="us-la">

                    <div class="ico"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/US.png" alt="" width="64px"/></div>
                    <div class="label">Los Angeles</div>
                    <div class="content mob">
                        Premium data center in LA, California, guarantees immaculate connectivity and ultra-fast response times. LA data center is the best solution for those looking for lowest latency and good connection speed to the West Coast USA and Asia regions.
                        <br><br>
                        <div class="services">Available hosting services: <a href="<?php echo $config['links']['web-hosting']?>">web hosting</a>, <a href="<?php echo $config['links']['vps-hosting']?>">VPS hosting</a>.</div>
                    </div>

                </div><div class="flag de" data-loc="de">
                    <div class="ico"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/DE.png" alt="" width="64px"/></div>
                    <div class="label">Frankfurt</div>
                    <div class="content mob">
                        Being one of the most secure and reliable data centers in the world, our server location in Frankfurt sustains uptime and high performance. If you are looking for the best connection speed and lowest latency to Northern Europe, our Frankfurt servers are a great deal.
                        <br><br>
                        <div class="services">Available hosting services: <a href="<?php echo $config['links']['web-hosting']?>">web hosting</a>, <a href="<?php echo $config['links']['vps-hosting']?>">VPS hosting</a>, <a href="<?php echo $config['links']['dedicated-servers']?>">dedicated servers</a>, <a href="<?php echo $config['links']['reseller-hosting']?>">reseller hosting</a>.</div>
                    </div>

                </div><div class="flag gb" data-loc="gb">
                    <div class="ico"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/GB.png" alt="" width="64px"/></div>
                    <div class="label">London</div>
                    <div class="content mob">
                        Data center in the most technically innovative city of UK brings hosting to the next level by ensuring secure, agile, scalable and robust services.
                        <br><br>
                        <div class="services">Available hosting services: <a href="<?php echo $config['links']['web-hosting']?>">web hosting</a>.</div>
                    </div>

                </div><div class="flag nl" data-loc="nl">

                    <div class="ico"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/NL.png" alt="" width="64px"/></div>
                    <div class="label">Amsterdam</div>
                    <div class="content mob">
                        In a city recognized as a digital gateway to Europe everyone can benefit from developed UPS systems and high data security levels.
                        <br><br>
                        <div class="services">Available hosting services: <a href="<?php echo $config['links']['web-hosting']?>">web hosting</a>, <a href="<?php echo $config['links']['reseller-hosting']?>">reseller hosting</a>.</div>
                    </div>

                </div><div class="flag lt" data-loc="lt">

                    <div class="ico"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/LT.png" alt="" width="64px"/></div>
                    <div class="label">Šiauliai</div>
                    <div class="content mob">
                        Constantly evolving data center in Šiauliai provides green hosting services together with smartest Hybrid servers and only certified Intel hardware. Experienced network professionals make sure that connection speed and latency always meet your expectations. The connection is especially good for the entire Eastern Europe.
                        <br><br>
                        <div class="services">Available hosting services: <a href="<?php echo $config['links']['web-hosting']?>">web hosting</a>, <a href="<?php echo $config['links']['reseller-hosting']?>">reseller hosting</a>.</div>
                    </div>

                </div><div class="flag br" data-loc="br">

                    <div class="ico"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/BR.png" alt="" width="64px"/></div>
                    <div class="label">São Paulo</div>
                    <div class="content mob">
                        With a wide range of security equipment, the facility in São Paulo upholds a secure and reliable environment and preserves server performance. We are also thoroughly taking care of the whole network to ensure low latency and good network connection to the whole South America region.
                        <br><br>
                        <div class="services">Available hosting services: <a href="<?php echo $config['links']['web-hosting']?>">web hosting</a>, <a href="<?php echo $config['links']['vps-hosting']?>">VPS hosting</a>, <a href="<?php echo $config['links']['reseller-hosting']?>">reseller hosting</a>.</div>
                    </div>

                </div><div class="flag za" data-loc="za">

                    <div class="ico"><img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/64/ZA.png" alt="" width="64px"/></div>
                    <div class="label">Johannesburg</div>
                    <div class="content mob">
                        A reliable server environment in Johannesburg guarantees stability, good network speed and lower latency for online projects, targeted to audiences in the whole African region.
                        <br><br>
                        <div class="services">Available hosting services: <a href="<?php echo $config['links']['web-hosting']?>">web hosting</a>, <a href="<?php echo $config['links']['vps-hosting']?>">VPS hosting</a>, <a href="<?php echo $config['links']['reseller-hosting']?>">reseller hosting</a>.</div>
                    </div>
                </div>
            </div>
            <div class="about-location">
                <div class="location-description us-ch active">
                    <div class="title">
                        Chicago, United States
                    </div>
                    <div class="content">
                        A modern data center in Chicago, Illinois, is distinguished by high security standards and thorough server upkeep. Professional network administrators are always there to ensure high connection speed and low latency, especially to Middle and East Coast USA.
                        <br><br>
                        <div class="services">Available hosting services: <a href="<?php echo $config['links']['web-hosting']?>">web hosting</a>, <a href="<?php echo $config['links']['website'] . '/chicago-vps/'; ?>">VPS hosting</a>, <a href="<?php echo $config['links']['reseller-hosting']?>">reseller hosting</a>.</div>
                    </div>
                </div>
                <div class="location-description us-la">
                    <div class="title">
                        Los Angeles, United States
                    </div>
                    <div class="content">
                        Premium data center in LA, California, guarantees immaculate connectivity and ultra-fast response times. LA data center is the best solution for those looking for lowest latency and good connection speed to the West Coast USA and Asia regions.
                        <br><br>
                        <div class="services">Available hosting services: <a href="<?php echo $config['links']['web-hosting']?>">web hosting</a>, <a href="<?php echo $config['links']['website'] . '/usa-vps-hosting/'; ?>">VPS hosting</a>.</div>
                    </div>
                </div>
                <div class="location-description de">
                    <div class="title">
                        Frankfurt, Germany
                    </div>
                    <div class="content">
                        Being one of the most secure and reliable data centers in the world, our server location in Frankfurt sustains uptime and high performance. If you are looking for the best connection speed and lowest latency to Northern Europe, our Frankfurt servers are a great deal.
                        <br><br>
                        <div class="services">Available hosting services: <a href="<?php echo $config['links']['web-hosting']?>">web hosting</a>, <a href="<?php echo $config['links']['vps-hosting']?>">VPS hosting</a>, <a href="<?php echo $config['links']['dedicated-servers']?>">dedicated servers</a>, <a href="<?php echo $config['links']['reseller-hosting']?>">reseller hosting</a>.</div>
                    </div>
                </div>
                <div class="location-description gb">
                    <div class="title">
                        London, United Kingdom
                    </div>
                    <div class="content">
                        Data center in the most technically innovative city of UK brings hosting to the next level by ensuring secure, agile, scalable and robust services.
                        <br><br>
                        <div class="services">Available hosting services: <a href="<?php echo $config['links']['web-hosting']?>">web hosting</a>.</div>
                    </div>
                </div>
                <div class="location-description nl">
                    <div class="title">
                        Amsterdam, The Netherlands
                    </div>
                    <div class="content">
                        In a city recognized as a digital gateway to Europe everyone can benefit from developed UPS systems and high data security levels.
                        <br><br>
                        <div class="services">Available hosting services: <a href="<?php echo $config['links']['web-hosting']?>">web hosting</a>, <a href="<?php echo $config['links']['reseller-hosting']?>">reseller hosting</a>.</div>
                    </div>
                </div>
                <div class="location-description lt">
                    <div class="title">
                        Šiauliai, Lithuania
                    </div>
                    <div class="content">
                        Constantly evolving data center in Šiauliai provides green hosting services together with smartest Hybrid servers and only certified Intel hardware. Experienced network professionals make sure that connection speed and latency always meet your expectations. The connection is especially good for the entire Eastern Europe.
                        <br><br>
                        <div class="services">Available hosting services: <a href="<?php echo $config['links']['web-hosting']?>">web hosting</a>, <a href="<?php echo $config['links']['reseller-hosting']?>">reseller hosting</a>.</div>
                    </div>
                </div>
                <div class="location-description br">
                    <div class="title">
                        São Paulo, Brazil
                    </div>
                    <div class="content">
                        With a wide range of security equipment, the facility in São Paulo upholds a secure and reliable environment and preserves server performance. We are also thoroughly taking care of the whole network to ensure low latency and good network connection to the whole South America region.
                        <br><br>
                        <div class="services">Available hosting services: <a href="<?php echo $config['links']['web-hosting']?>">web hosting</a>, <a href="<?php echo $config['links']['vps-hosting']?>">VPS hosting</a>, <a href="<?php echo $config['links']['reseller-hosting']?>">reseller hosting</a>.</div>
                    </div>
                </div>
                <div class="location-description za">
                    <div class="title">
                        Johannesburg, South Africa
                    </div>
                    <div class="content">
                        A reliable server environment in Johannesburg guarantees stability, good network speed and lower latency for online projects, targeted to audiences in the whole African region.
                        <br><br>
                        <div class="services">Available hosting services: <a href="<?php echo $config['links']['web-hosting']?>">web hosting</a>, <a href="<?php echo $config['links']['website'] . '/south-africa-vps/'; ?>">VPS hosting</a>, <a href="<?php echo $config['links']['reseller-hosting']?>">reseller hosting</a>.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hidden">
    <script>
        $(document).ready(function(){
            $('.location-services .flags .flag').click(function(){
                var main_container = $(this).closest('.location-services');
                var new_loc = $(this).attr('data-loc');

                main_container.attr('data-selected-loc', new_loc);

                main_container.find('.flags .flag').removeClass('active');
                main_container.find('.flags .flag.'+ new_loc +'').addClass('active');

                main_container.find('.location-description').removeClass('active');
                main_container.find('.location-description.'+ new_loc +'').addClass('active');
            });
        });

        <!--//--><![CDATA[//><!-- Preload images

            if (document.images) {
                img1 = new Image();
                img2 = new Image();
                img3 = new Image();
                img4 = new Image();
                img5 = new Image();
                img6 = new Image();
                img7 = new Image();
                img8 = new Image();

                img1.src = "<?php echo get_template_directory_uri() ?>/img/location_services_bgs/chicago_bg_v2.jpg";
                img2.src = "<?php echo get_template_directory_uri() ?>/img/location_services_bgs/frankfurt_bg_v2.jpg";
                img3.src = "<?php echo get_template_directory_uri() ?>/img/location_services_bgs/losangeles_bg_v2.jpg";
                img4.src = "<?php echo get_template_directory_uri() ?>/img/location_services_bgs/johannesburg_bg_v2.jpg";
                img5.src = "<?php echo get_template_directory_uri() ?>/img/location_services_bgs/sanpaulo_bg_v2.jpg";
                img6.src = "<?php echo get_template_directory_uri() ?>/img/location_services_bgs/london_bg_v2.jpg";
                img7.src = "<?php echo get_template_directory_uri() ?>/img/location_services_bgs/amsterdam_bg_v2.jpg";
                img8.src = "<?php echo get_template_directory_uri() ?>/img/location_services_bgs/siauliai_bg_v2.jpg";
            }
        //--><!]]>
    </script>
</div>