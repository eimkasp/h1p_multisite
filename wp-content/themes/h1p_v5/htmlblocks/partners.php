<section class="block partners">

    <div class="container">
        <div class="row">

            <h2 class="block-title">Our Partners</h2>

            <div class="block-content">

                <div class="description">
                    Vim esse impedit detraxit at. Ea iusto primis virtute vis. Purto definiebas interesset per et, cum ex agam detracto voluptua. Ei nostrum iudicabit molestiae vel, eos possit ponderum no, te vis eius propriae democritum. Nonumy alienum ut ius, porro choro soluta id vis. At usu dicam efficiendi, no sea doming consequat.
                </div>

                <ul class="logos">
                    <li><i class="icon partner cpanel responsive"></i></li>
                    <li><i class="icon partner whmcs responsive"></i></li>
                    <li><i class="icon partner ispconfig responsive"></i></li>
                    <li><i class="icon partner cloudlinux responsive"></i></li>
                    <li><i class="icon partner microsoft responsive"></i></li>
                    <li><i class="icon partner directadmin responsive"></i></li>
                </ul>

            </div>
        </div>
    </div>

</section>
