<?php

$titles = array(

    'vps' => array(
        'us_west' => [
                        'title'  => __("Los Angeles"),
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ],
        'us_east' => [
                        'title'  => __("Chicago"),
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ],
        'za'      => [
                        'title'  => __("Johannesburg"),
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ],
        'de'      => [
                        'title'  => __("Frankfurt"),
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ],
        'br'      => [
                        'title'  => __("São Paulo"),
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ]
    ),

    'web' => array(
        'us_west' => [
                        'title'  => __("Los Angeles"),
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => false
                                    ]
                     ],
        'us_east' => [
                        'title'  => __("Chicago"),
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => false
                                    ]
                     ],
        'uk'      => [
                        'title'  => __("London"),
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => true
                                    ]
                     ],
        'nl'      => [
                        'title'  => __("Amsterdam"),
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => true
                                    ]
                     ],
        'lt'      => [
                        'title'  => __("Šiauliai"),
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => true
                                    ]
                     ],
        'za'      => [
                        'title'  => __("Johannesburg"),
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => true
                                    ]
                     ],
        'de'      => [
                        'title'  => __("Frankfurt"),
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => true
                                    ]
                     ],
        'br'      => [
                        'title'  => __("São Paulo"),
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => true
                                    ]
                     ]
    ),

    'reseller' => array(
        'us_east' => [
                        'title'  => __("Chicago"),
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => false
                                    ]
                     ],
        'nl'      => [
                        'title'  => __("Amsterdam"),
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => false
                                    ]
                     ],
        'za'      => [
                        'title'  => __("Johannesburg"),
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => false
                                    ]
                     ],
        'lt'      => [
                        'title'  => __("Šiauliai"),
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => false
                                    ]
                     ],
        'de'      => [
                        'title'  => __("Frankfurt"),
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => true
                                    ]
                     ],
        'br'      => [
                        'title'  => __("São Paulo"),
                        'panels' => [
                                        'cpanel' => true,
                                        'dadmin' => false
                                    ]
                     ]
    ),

    'default' => array(
        'us_west' => [
                        'title'  => __("Los Angeles"),
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ],
        'us_east' => [
                        'title'  => __("Chicago"),
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ],
        'uk'      => [
                        'title'  => __("London"),
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ],
        'nl'      => [
                        'title'  => __("Amsterdam"),
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ],
        'lt'      => [
                        'title'  => __("Šiauliai"),
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ],
        'za'      => [
                        'title'  => __("Johannesburg"),
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ],
        'de'      => [
                        'title'  => __("Frankfurt"),
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ],
        'br'      => [
                        'title'  => __("São Paulo"),
                        'panels' => [
                                        'cpanel' => false,
                                        'dadmin' => false
                                    ]
                     ]
    )

);

$type = isset($params['type']) ? $params['type'] : 'default';

?>

    <div class="row">
        <div class="locations-grid">
            <?php if(isset($titles[$type]['us_west'])){ ?>
                <div class="location loc-us-west">
                    <div class="loc-cont" data-tooltip="locations-<?php echo $type;?>-us-west">
                        <i class="icon location-flag us"></i>
                        <span class="title"><?php echo $titles[$type]['us_west']['title']; ?> <span class="country"><?php _e('United States');?></span></span>
                    </div>
                    <div class="loc-pop-up" data-tooltip-content="locations-<?php echo $type;?>-us-west">
                        <div class="triangle"></div>
                        <div class="head">
                            <h3 class="pop-up-title"><?php _e('Los Angeles')?>, <?php _e('United States')?></h3>
                            <span class="pop-up-descr"><?php _e('Premium data centers in LA, California, guarantee immaculate connectivity and ultra-fast response times.')?></span>
                        </div>
                        <div class="content">
                            <?php if(isset($titles[$type]['us_west']['panels']['cpanel']) && $titles[$type]['us_west']['panels']['cpanel'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('cPanel')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                            <?php if(isset($titles[$type]['us_west']['panels']['dadmin']) && $titles[$type]['us_west']['panels']['dadmin'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('DirectAdmin')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="loc-pointer"></div>
                </div>
            <?php } ?>
            <?php if(isset($titles[$type]['us_east'])){ ?>
                <div class="location loc-us-east">
                    <div class="loc-cont" data-tooltip="locations-<?php echo $type;?>-us-east">
                        <i class="icon location-flag us"></i>
                        <span class="title"><?php echo $titles[$type]['us_east']['title']; ?> <span class="country"><?php _e('United States');?></span></span>
                    </div>
                    <div class="loc-pop-up" data-tooltip-content="locations-<?php echo $type;?>-us-east">
                        <div class="triangle"></div>
                        <div class="head">
                            <h3 class="pop-up-title"><?php _e('Chicago')?>, <?php _e('United States')?></h3>
                            <span class="pop-up-descr"><?php _e('A modern data center in Chicago, Illinois, is distinguished by high security standards and thorough server upkeep')?></span>
                        </div>
                        <div class="content">
                            <?php if(isset($titles[$type]['us_east']['panels']['cpanel']) && $titles[$type]['us_east']['panels']['cpanel'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('cPanel')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                            <?php if(isset($titles[$type]['us_east']['panels']['dadmin']) && $titles[$type]['us_east']['panels']['dadmin'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('DirectAdmin')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="loc-pointer"></div>
                </div>
            <?php } ?>
            <?php if(isset($titles[$type]['uk'])){ ?>
                <div class="location loc-uk">
                    <div class="loc-cont"  data-tooltip="locations-<?php echo $type;?>-uk">
                        <i class="icon location-flag uk"></i>
                        <span class="title"><?php echo $titles[$type]['uk']['title']; ?> <span class="country"><?php _e('United Kingdom');?></span></span>
                    </div>
                    <div class="loc-pop-up"  data-tooltip-content="locations-<?php echo $type;?>-uk">
                        <div class="triangle"></div>
                        <div class="head">
                            <h3 class="pop-up-title"><?php _e('London')?>, <?php _e('United Kingdom')?></h3>
                            <span class="pop-up-descr"><?php _e('A fully-staffed data center in London has a professional team of skilled technicians, highest levels of security and uninterrupted availability.')?></span>
                        </div>
                        <div class="content">
                            <?php if(isset($titles[$type]['uk']['panels']['cpanel']) && $titles[$type]['uk']['panels']['cpanel'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('cPanel')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                            <?php if(isset($titles[$type]['uk']['panels']['dadmin']) && $titles[$type]['uk']['panels']['dadmin'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('DirectAdmin')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="loc-pointer"></div>
                </div>
            <?php } ?>
            <?php if(isset($titles[$type]['nl'])){ ?>
                <div class="location loc-nl">
                    <div class="loc-cont" data-tooltip="locations-<?php echo $type;?>-nl">
                        <i class="icon location-flag nl"></i>
                        <span class="title"><?php echo $titles[$type]['nl']['title']; ?> <span class="country"><?php _e('The Netherlands');?></span></span>
                    </div>
                    <div class="loc-pop-up" data-tooltip-content="locations-<?php echo $type;?>-nl">
                        <div class="triangle"></div>
                        <div class="head">
                            <h3 class="pop-up-title"><?php _e('Amsterdam')?>, <?php _e('The Netherlands')?></h3>
                            <span class="pop-up-descr"><?php _e('A custom-designed facility in Amsterdam stands out because of excellent functional and physical security and lowest chance of downtime.')?></span>
                        </div>
                        <div class="content">
                            <?php if(isset($titles[$type]['nl']['panels']['cpanel']) && $titles[$type]['nl']['panels']['cpanel'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('cPanel')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                            <?php if(isset($titles[$type]['nl']['panels']['dadmin']) && $titles[$type]['nl']['panels']['dadmin'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('DirectAdmin')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="loc-pointer"></div>
                </div>
            <?php } ?>
            <?php if(isset($titles[$type]['lt'])){ ?>
                <div class="location loc-lt">
                    <div class="loc-cont" data-tooltip="locations-<?php echo $type;?>-lt">
                        <i class="icon location-flag lt"></i>
                        <span class="title"><?php echo $titles[$type]['lt']['title']; ?> <span class="country"><?php _e('Lithuania');?></span></span>
                    </div>
                    <div class="loc-pop-up" data-tooltip-content="locations-<?php echo $type;?>-lt">
                        <div class="triangle"></div>
                        <div class="head">
                            <h3 class="pop-up-title"><?php _e('Šiauliai')?>, <?php _e('Lithuania')?></h3>
                            <span class="pop-up-descr"><?php _e('Comprehensive real-time monitoring and efficient allocation ensures stability and good performance.')?></span>
                        </div>
                        <div class="content">
                            <?php if(isset($titles[$type]['lt']['panels']['cpanel']) && $titles[$type]['lt']['panels']['cpanel'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('cPanel')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                            <?php if(isset($titles[$type]['lt']['panels']['dadmin']) && $titles[$type]['lt']['panels']['dadmin'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('DirectAdmin')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="loc-pointer"></div>
                </div>
            <?php } ?>
            <?php if(isset($titles[$type]['za'])){ ?>
                <div class="location loc-za">
                    <div class="loc-cont" data-tooltip="locations-<?php echo $type;?>-za">
                        <i class="icon location-flag za"></i>
                        <span class="title"><?php echo $titles[$type]['za']['title']; ?> <span class="country"><?php _e('South Africa');?></span></span>
                    </div>
                    <div class="loc-pop-up" data-tooltip-content="locations-<?php echo $type;?>-za">
                        <div class="triangle"></div>
                        <div class="head">
                            <h3 class="pop-up-title"><?php _e('Johannesburg')?>, <?php _e('South Africa')?></h3>
                            <span class="pop-up-descr"><?php _e('A reliable server environment in Johannesburg guarantees stability and lower latency for online projects, targeted to audiences in the whole African region.')?></span>
                        </div>
                        <div class="content">
                            <?php if(isset($titles[$type]['za']['panels']['cpanel']) && $titles[$type]['za']['panels']['cpanel'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('cPanel')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                            <?php if(isset($titles[$type]['za']['panels']['dadmin']) && $titles[$type]['za']['panels']['dadmin'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('DirectAdmin')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="loc-pointer"></div>
                </div>
            <?php } ?>
            <?php if(isset($titles[$type]['sg'])){ ?>
                <div class="location loc-sg">
                    <div class="loc-cont" data-tooltip="locations-<?php echo $type;?>-sg">
                        <i class="icon location-flag sg"></i>
                        <span class="title"><?php echo $titles[$type]['sg']['title']; ?> <span class="country"><?php _e('Singapore');?></span></span>
                    </div>
                    <div class="loc-pop-up" data-tooltip-content="locations-<?php echo $type;?>-sg">
                        <div class="triangle"></div>
                        <div class="head">
                            <h3 class="pop-up-title"><?php _e('Singapore')?>, <?php _e('Singapore')?></h3>
                            <span class="pop-up-descr"><?php _e('A premium data center in Singapore provides top security measures, higher performance and a better reach to Asia-Pacific markets.')?></span>
                        </div>
                        <div class="content">
                            <?php if(isset($titles[$type]['sg']['panels']['cpanel']) && $titles[$type]['sg']['panels']['cpanel'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('cPanel')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                            <?php if(isset($titles[$type]['sg']['panels']['dadmin']) && $titles[$type]['sg']['panels']['dadmin'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('DirectAdmin')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="loc-pointer"></div>
                </div>
            <?php } ?>
            <?php if(isset($titles[$type]['de'])){ ?>
                <div class="location loc-de">
                    <div class="loc-cont" data-tooltip="locations-<?php echo $type;?>-de">
                        <i class="icon location-flag de"></i>
                        <span class="title"><?php echo $titles[$type]['de']['title']; ?> <span class="country"><?php _e('Germany');?></span></span>
                    </div>
                    <div class="loc-pop-up" data-tooltip-content="locations-<?php echo $type;?>-de">
                        <div class="triangle"></div>
                        <div class="head">
                            <h3 class="pop-up-title"><?php _e('Frankfurt')?>, <?php _e('Germany')?></h3>
                            <span class="pop-up-descr"><?php _e('Being one of the most secure and reliable data centers in the world, our server location in Frankfurt sustains uptime and boosts performance.')?></span>
                        </div>
                        <div class="content">
                            <?php if(isset($titles[$type]['de']['panels']['cpanel']) && $titles[$type]['de']['panels']['cpanel'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('cPanel')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                            <?php if(isset($titles[$type]['de']['panels']['dadmin']) && $titles[$type]['de']['panels']['dadmin'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('DirectAdmin')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="loc-pointer"></div>
                </div>
            <?php } ?>
            <?php if(isset($titles[$type]['br'])){ ?>
                <div class="location loc-br">
                    <div class="loc-cont" data-tooltip="locations-<?php echo $type;?>-br">
                        <i class="icon location-flag br"></i>
                        <span class="title"><?php echo $titles[$type]['br']['title']; ?> <span class="country"><?php _e('Brazil');?></span></span>
                    </div>
                    <div class="loc-pop-up" data-tooltip-content="locations-<?php echo $type;?>-br">
                        <div class="triangle"></div>
                        <div class="head">
                            <h3 class="pop-up-title"><?php _e('São Paulo')?>, <?php _e('Brazil')?></h3>
                            <span class="pop-up-descr"><?php _e('With a wide range of security equipment, the facility in São Paulo upholds a secure and reliable environment and preserves server performance.')?></span>
                        </div>
                        <div class="content">
                            <?php if(isset($titles[$type]['br']['panels']['cpanel']) && $titles[$type]['br']['panels']['cpanel'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('cPanel')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                            <?php if(isset($titles[$type]['br']['panels']['dadmin']) && $titles[$type]['br']['panels']['dadmin'] === true):?>
                                <div class="info-row">
                                    <span class="left"><?php _e('DirectAdmin')?></span>
                                    <span class="right"><?php echo _x_lc('Available', 'singular musculine')?></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="loc-pointer"></div>
                </div>
            <?php } ?>
        </div>


    </div> <!-- end of .row -->
