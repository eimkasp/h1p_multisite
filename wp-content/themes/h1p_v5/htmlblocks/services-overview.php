<?php

    global $whmcs;
    global $config;

?>
<div class="services-overview-box" data-active-service="web-hosting">
    <div class="handles">
        <div class="handle active" data-service-handle="web-hosting" onclick="changeService( $(this).attr('data-service-handle') )">WEB HOSTING</div>
        <div class="handle" data-service-handle="vps-hosting" onclick="changeService( $(this).attr('data-service-handle') )">VPS HOSTING</div>
        <div class="handle" data-service-handle="reseller-hosting" onclick="changeService( $(this).attr('data-service-handle') )">RESELLER HOSTING</div>
        <div class="handle" data-service-handle="dedicated-servers" onclick="changeService( $(this).attr('data-service-handle') )">DEDICATED SERVERS</div>
    </div>
    <div class="dynamic-content">
        <div class="content active" data-service-content="web-hosting">
            <div class="title">
                Europe web hosting
            </div>
            <div class="sub-title">
                Web hosting solution crafted for small and medium size business.
            </div>
            <div class="features-block">
                <strong>Main features include:</strong>
                <ul>
                    <li>DirectAdmin or cPanel control panels (free)</li>
                    <li>1 Gbit network uplink</li>
                    <li>Application auto-installer</li>
                    <li>Unlimited email accounts</li>
                    <li>Free domain with Business and Business Pro plans (6 months and above billing cycles)</li>
                    <li>Backup management</li>
                </ul>
            </div>
            <div class="button_wrap">
                <a href="<?php echo $config['links']['web-hosting'] ?>" class="button">Learn more</a>
            </div>
        </div>
        <div class="content" data-service-content="vps-hosting">
            <div class="title">
                VPS Europe
            </div>
            <div class="sub-title">
                With high server performance and network speed, especially in Europe, our VPS covers any type and size online project.
            </div>
            <div class="features-block">
                <strong>Main features include:</strong>
                <ul>
                    <li>Scalable server resources which can be customized during the checkout and at our client area.</li>
                    <li>DNS management</li>
                    <li>Operating systems – Ubuntu, Fedora, Debian, CentOS, Suse.</li>
                    <li>DDoS protection</li>
                    <li>Full root access</li>
                </ul>
            </div>
            <div class="button_wrap">
                <a href="<?php echo $config['links']['website'] . '/germany-vps-hosting/' ?>" class="button">Learn more</a>
            </div>
        </div>
        <div class="content" data-service-content="reseller-hosting">
            <div class="title">
                Reseller hosting in Europe
            </div>
            <div class="sub-title">
                Carefully crafted reseller hosting solutions for creating online business with exceptional stability.
            </div>
            <div class="features-block">
                <strong>Main features include:</strong>
                <ul>
                    <li>Unlimited domains, sub-domains and databases.</li>
                    <li>Available control panels – cPanel or DirectAdmin</li>
                    <li>CloudLinux operating system.</li>
                    <li>Free WHMCS license (with all plans except Starter).</li>
                    <li>Scalable resources – upgrade or downgrade your plan according to your needs at our client area.</li>
                </ul>
            </div>
            <div class="button_wrap">
                <a href="<?php echo $config['links']['reseller-hosting'] ?>" class="button">Learn more</a>
            </div>
        </div>
        <div class="content" data-service-content="dedicated-servers">
            <div class="title">
                Europe dedicated servers
            </div>
            <div class="sub-title">
                Thoroughly designed for reliability and power, our dedicated servers put you in a fully remote server.
            </div>
            <div class="features-block">
                <strong>Main features include:</strong>
                <ul>
                    <li>Available CPUs – Celeron, Core i3, Atom, Core i5, Core i7, Xeon</li>
                    <li>Available operating systems – CentOS, Debian, Ubuntu, openSUSE, Windows server 2008, Windows server 2012, Windows 7</li>
                    <li>Up to 10 Gbps bandwidth</li>
                    <li>Unrestricted access and control</li>
                    <li>Available control panels – DirectAdmin (free), cPanel ($36 per month)</li>
                    <li>Setup in 24 hours</li>
                </ul>
            </div>
            <div class="button_wrap">
                <a href="<?php echo $config['links']['dedicated-servers'] ?>" class="button">Learn more</a>
            </div>
        </div>
    </div>
    <div class="navigation">
        <div class="nav nav-prev disabled" onclick="prev_service();">
            <i class="fa fa-angle-left"></i>
        </div>
        <div class="nav nav-next" onclick="next_service();">
            <i class="fa fa-angle-right"></i>
        </div>
    </div>
</div>
<script>
    function changeService( new_service ){
        $('[data-active-service].services-overview-box').attr( 'data-active-service', new_service );

        $('.services-overview-box [data-service-content]').removeClass( 'active' );
        $('.services-overview-box [data-service-content="'+ new_service +'"]').addClass( 'active' );

        $('[data-service-handle]').removeClass( 'active' );
        $('[data-service-handle="'+ new_service +'"]').addClass( 'active' );

        //update navigation buttoons
        var next_service_handle = $('.services-overview-box [data-service-handle].active').next();
        var prev_service_handle = $('.services-overview-box [data-service-handle].active').prev();

        $('.services-overview-box .nav').removeClass('disabled');
        if( next_service_handle.text().length < 1 ){
            $('.services-overview-box .nav-next').addClass('disabled');
        }
        if( prev_service_handle.text().length < 1 ){
            $('.services-overview-box .nav-prev').addClass('disabled');
        }
    }
    function prev_service(){
        var prev_service_handle = $('.services-overview-box [data-service-handle].active').prev();
        if( prev_service_handle.text().length < 1 ){
            return;
        }
        else{
            changeService( prev_service_handle.attr('data-service-handle') );
        }
    }
    function next_service(){
        var next_service_handle = $('.services-overview-box [data-service-handle].active').next();
        if( next_service_handle.text().length < 1 ){
            return;
        }
        else{
            changeService( next_service_handle.attr('data-service-handle') );
        }
    }
</script>