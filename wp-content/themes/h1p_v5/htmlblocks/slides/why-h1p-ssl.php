<section class="block slides">

    <div class="container">

        <div class="slider" data-tabs="why-h1p-ssl">

            <div class="slide why-ssl active" data-tab-id="slide-1">

                <div class="slide-block txt">

                    <h4 class="title"><?php _e('Why choose an SSL Certificate at Host1Plus?')?></h4>

                    <div class="content">
                        <p><?php _e('Show customers that you care about their security!')?></p>
                        <p><?php _e('Host1Plus SSL certificates are a cost-effective solution to secure your business. Multiple SSL certificate options may be adjusted to meet different expectations – from individual needs to advanced online businesses.')?></p>
                    </div>

                </div>

                <div class="slide-block image">
                    <div class="image"></div>
                </div>

            </div>

            <div class="slide why-ssl" data-tab-id="slide-2">

                <div class="slide-block txt">

                    <h4 class="title"><?php _e('What is an SSL Certificate?')?></h4>

                    <div class="content">
                        <p><?php _e('SSL certificates are small data files that digitally bind a cryptographic key to your data. When installed on a web server, it activates the padlock and the https protocol, and enables a secure encrypted link between a web server and a browser.')?></p>
                        <p><?php _e('SSL Certificates are not simply a tool – it’s common sense. Keep your online business safe and maintain the confidence of your clients.')?></p>
                    </div>

                </div>

                <div class="slide-block image">
                    <div class="image"></div>
                </div>

            </div>

        </div>

        <div class="slider-navigation">
            <a href="javascript:" class="link previous" data-tab-action="why-h1p-ssl.previous"><i class="icon slider prev"></i></a>
            <a href="javascript:" class="link next" data-tab-action="why-h1p-ssl.next"><i class="icon slider next"></i></a>
        </div>

        <ul class="slider-pagination">
            <li><a href="javascript:" data-tab-link="why-h1p-ssl.slide-1" class="active"></a></li>
            <li><a href="javascript:" data-tab-link="why-h1p-ssl.slide-2"></a></li>
        </ul>

    </div>

</section>
