<div class="hosting-features landing-line">
    <div class="container">
        <h2 class="title">
            <?php _e('VPS Hosting Features') ?>
        </h2>
        <div class="features-list">
            <div class="feature-box">
                <div class="fico">
                    <i class="ico refresh"></i>
                </div>
                <div class="ftitle"><?php _e('Integrated DNS Management') ?></div>
                <div class="fcontent">
                    <?php _e('Manage your server without struggle! Now you can easily create your domain zones and edit records with no extra support.') ?>
                </div>
            </div>
            <div class="feature-box">
                <div class="fico">
                    <i class="ico protection"></i>
                </div>
                <div class="ftitle"><?php _e('Full DDoS Protection') ?></div>
                <div class="fcontent">
                    <?php _e('Enjoy safe online environment and avoid interruptions with fully DDoS protected VPS services.') ?>
                </div>
            </div>
            <div class="feature-box">
                <div class="fico">
                    <i class="ico scalable"></i>
                </div>
                <div class="ftitle"><?php _e('Scalable Resources') ?></div>
                <div class="fcontent">
                    <?php _e('Grab more resources to support your growing needs! Customize the resources through your Client Area according to your preferences and stay on a budget!') ?>
                </div>
            </div>
            <div class="feature-box">
                <div class="fico">
                    <i class="ico ssd"></i>
                </div>
                <div class="ftitle"><?php _e('SSD Caching') ?></div>
                <div class="fcontent">
                    <?php _e('When caching and pre-loading your commonly used data into the SSD drive, SSD caching speeds up the access to your data and ensures rapid loading and boot times.') ?>
                </div>
            </div>
            <div class="feature-box">
                <div class="fico">
                    <i class="ico intel"></i>
                </div>
                <div class="ftitle"><?php _e('Intel Xeon Processors') ?></div>
                <div class="fcontent">
                    <?php _e('Enhance your server performance with enterprise Xeon processors. Enjoy improved service quality with server-grade CPUs, higher core counts and less vulnerability.') ?>
                </div>
            </div>
            <div class="feature-box">
                <div class="fico">
                    <i class="ico linux"></i>
                </div>
                <div class="ftitle"><?php _e('Diversity of Linux OS Distros') ?></div>
                <div class="fcontent">
                    <?php _e('Run multiple OS instances (Ubuntu, Fedora, Debian, CentOS, Suse) with unbeatable performance.') ?>
                </div>
            </div>

        </div>
    </div>
</div>