<div id="ww-locations" class="ww-locations landing-line" data-active-location="chicago">
    <div class="container">
        <div class="title white">
            <?php _e('Worldwide Server Locations');?>
        </div>
        <div class="subtitle">
            <?php _e('Multiple premium data centers ensure high connectivity and better reach.');?>
        </div>

        <div class="locations-list-wrapper">
            <div class="locations-list">
                <div class="location" data-location="chicago" data-location-bg-handle>
                    <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/US.png" alt="" class="flag"/>
                    <div class="city-name"><?php _e('Chicago');?></div>
                    <div class="country-name"><?php _e('USA');?></div>
                </div>
                <div class="location" data-location="los-angeles" data-location-bg-handle>
                    <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/US.png" alt="" class="flag"/>
                    <div class="city-name"><?php _e('Los Angeles');?></div>
                    <div class="country-name"><?php _e('USA');?></div>
                </div>
                <div class="location" data-location="san-paulo" data-location-bg-handle>
                    <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/BR.png" alt="" class="flag"/>
                    <div class="city-name"><?php _e('San Paulo');?></div>
                    <div class="country-name"><?php _e('Brasil');?></div>
                </div>
                <div class="location" data-location="frankfurt" data-location-bg-handle>
                    <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/DE.png" alt="" class="flag"/>
                    <div class="city-name"><?php _e('Frankfurt');?></div>
                    <div class="country-name"><?php _e('Germany');?></div>
                </div>
                <div class="location" data-location="johannesburg" data-location-bg-handle>
                    <img src="<?php echo get_template_directory_uri() ?>/img/flags-iso/shiny/48/US.png" alt="" class="flag"/>
                    <div class="city-name"><?php _e('Johannesburg');?></div>
                    <div class="country-name"><?php _e('South Africa');?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    <!--//--><![CDATA[//><!-- Preload images

    $(document).ready(function(){
        if (document.images) {
            img1 = new Image();
            img2 = new Image();
            img3 = new Image();
            img4 = new Image();
            img5 = new Image();

            img1.src = "<?php echo get_template_directory_uri() ?>/img/locations/chicago-bg.jpg";
            img2.src = "<?php echo get_template_directory_uri() ?>/img/locations/los-angeles-bg.jpg";
            img3.src = "<?php echo get_template_directory_uri() ?>/img/locations/san-paulo-bg.jpg";
            img4.src = "<?php echo get_template_directory_uri() ?>/img/locations/frankfurt-bg.jpg";
            img5.src = "<?php echo get_template_directory_uri() ?>/img/locations/johannesburg-bg.jpg";
        }
    });

    //--><!]]>
</script>
<script>
$(document).ready(function(){
    $('[data-location-bg-handle]').click(function(){
        var new_loc = $(this).attr('data-location');
        $('#ww-locations').attr('data-active-location', new_loc);
    });
});
</script>