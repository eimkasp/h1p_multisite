
<?php

$reviews = get_reviews();

if($reviews && sizeof($reviews > 0)) :

?>

<div class="reviews-slider" data-cs>
    <div class="container">
        <h2 class="title"><?php _e('What do our customers say?');?></h2>

        <div class="reviews-slider-wrapper" data-cs-wrapper>

            <?php

                $rs_index = 1;
                foreach( $reviews as $key => $value ):
                    if( $rs_index > 5 ) break;
            ?>

            <div class="reviews-slide active" style="left:<?php echo ( ( $rs_index - 1 ) * 800 ) ?>px;" data-cs-slide="<?php echo $rs_index++; ?>">
                <span class="review-author-img">
                <?php
                    if ($value['source'] == "facebook") {
                        echo '<img src="' . $value['photo_url'] . '&width=200&height=200" />';
                    } else if ($value['source'] == "twitter") {
                        echo '<span class="img-icon"><i class="fa fa-twitter"></i></span>';
                    }
                ?>
                </span>
                <span class="review-author"><?php echo $value['name']; ?></span>
                <span class="review-author-link"><a href="<?php echo isset( $value['url'] ) && !empty( $value['url'] ) ? $value['url'] : '' ?>" target="_blank"><i class="fa fa-<?php echo $value['source']; ?>"></i> <?php echo ucfirst($value['source']); ?>  @<?php echo $value['name']; ?></a></span>
                <span class="review">
                    <?php echo $value['review_text']; ?>
                </span>
            </div>

            <?php endforeach; ?>

        </div>

        <div class="slide-selection v2" data-cs-paging></div>

        <div class="slider-navigation v2">
            <a data-cs-prev class="link previous disabled" data-cs-prev>
                <i class="fa fa-angle-left prev-handle nav-handle"></i>
            </a>
            <a data-cs-next class="link next" data-cs-next>
                <i class="fa fa-angle-right next-handle nav-handle"></i>
            </a>
        </div>
    </div>
</div>

<?php
    endif;
?>