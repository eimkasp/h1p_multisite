<?php

/**
 * @package WordPress
 * @subpackage h1p_v4
 */
/*
Template Name: Domain Search
*/



$domainPrices = $whmcs->getDomainsPrices();
$active_tab = isset($_GET['search']) ? $_GET['search'] : 'search';

get_header();

?>

<div id="domains-search" class="page domains-search">

    <section id="domains-search-container" class="domains-header">

        <div class="container">
            <div class="title"><?php _e('Find your perfect name!')?></div>
            <div class="navigation-block">
                <li class="menu">
                    <a href="javascript:" class="current">
                        <span><?php _e('Domain Search')?></span>
                        <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="submenu">
                        <a href="javascript:" class="item<?php if($active_tab == 'search'):?> active<?php endif;?>" data-tab-link="domainsearch.search"><?php _e('Domain Search')?></a>
                        <a href="javascript:" class="item<?php if($active_tab == 'transfer'):?> active<?php endif;?>" data-tab-link="domainsearch.bulktransfer"><?php _e('Domain Transfer')?></a>
                    </ul>
                </li>
                <div class="domainsearch-searchtabs" data-tabs="domainsearch">

                    <div id="domains-bulksearch-form" class="domainsearch-tab<?php if($active_tab == 'search'):?> active<?php endif;?>" ng-controller="bulksearchCtrl" data-tab-id="search">
                        <div class="inputs bulk">
                            <div class="input domains">
                                <div class="domains-block">
                                    <div class="form-field parsley-fixed-bottom">
                                        <textarea domains-textarea ng-model="slds" model-element="domainSearchInput" type="text" name="domains" class="domains-input" multiline-placeholder="<?php _e('Enter up to 20 domains. \nEach domain name must be written on a separate line \nE.g.: \ndomainname1 \ndomainname2')?>"  parsley-validate data-parsley-sldslist></textarea>
                                        <i class="icon search"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="input tlds">
                                <div class="tlds-select">
                                    <div class="tlds-scroll">
                                        <div class="tlds-block">
                                            <label class="input-checkbox green">
                                                <input ng-model="tldstoggleall" ng-change="toggleAllTlds()" type="checkbox" id="domains-bulksearch-tldstoggleall"/>
                                                <span class="checkbox">
                                                    <i class="fa fa-check"></i>
                                                    <span><?php _e('Select all TLDs')?></span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="tlds-block">
                                            <?php foreach($domainPrices as $index => $tld):?>
                                                <label class="input-checkbox green">
                                                    <input ng-model="tlds['<?php echo $tld['tld'];?>'].selected" type="checkbox" name="tld" value="<?php echo $tld['tld'];?>"/>
                                                    <span class="checkbox">
                                                        <i class="fa fa-check"></i>
                                                        <span>.<?php echo $tld['tld'];?></span>
                                                    </span>
                                                </label>
                                                <?php if($index == 6):?>
                                            </div>
                                            <div class="tlds-block">
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="input button-input">
                                <button id="domain-bulksearch-submit" type="submit" class="button highlight domain-button" ng-click="submitSearch()"><?php _e('Search')?></button>
                            </div>
                        </div>
                    </div>

                    <div id="domains-bulktransfer-form" class="domainsearch-tab<?php if($active_tab == 'transfer'):?> active<?php endif;?>" ng-controller="bulktransferCtrl" data-tab-id="bulktransfer">
                        <div class="inputs bulk">
                            <div class="input domains">
                                <div class="domains-block">
                                    <div class="form-field parsley-fixed-bottom">
                                        <textarea domains-textarea ng-model="slds" model-element="domainSearchInput" type="text" name="domains" class="domains-input" multiline-placeholder="<?php _e('Enter up to 20 domains. \nEach domain name must be written on a separate line \nE.g.: \ndomainname1 \ndomainname2')?>"  parsley-validate data-parsley-sldslist></textarea>
                                        <i class="icon search"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="input tlds">
                                <div class="tlds-select">
                                    <div class="tlds-scroll">
                                        <div class="tlds-block">
                                            <label class="input-checkbox green">
                                                <input ng-model="tldstoggleall" ng-change="toggleAllTlds()" type="checkbox" id="domains-bulksearch-tldstoggleall"/>
                                                <span class="checkbox">
                                                    <i class="fa fa-check"></i>
                                                    <span><?php _e('Select all TLDs')?></span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="tlds-block">
                                            <?php foreach($domainPrices as $index => $tld):?>
                                                <?php if( !array_key_exists('transfer', $tld['prices'] ) ) continue; ?>
                                                <label class="input-checkbox green">
                                                    <input ng-model="tlds['<?php echo $tld['tld'];?>'].selected" type="checkbox" name="tld" value="<?php echo $tld['tld'];?>"/>
                                                    <span class="checkbox">
                                                        <i class="fa fa-check"></i>
                                                        <span>.<?php echo $tld['tld'];?></span>
                                                    </span>
                                                </label>
                                                <?php if($index == 6):?>
                                            </div>
                                            <div class="tlds-block">
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="input button-input">
                                <button id="domain-bulksearch-submit" type="submit" class="button highlight domain-button" ng-click="submitSearch()"><?php _e('Search')?></button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>

    <section class="content domains">
        <div class="hosting">
            <div class="row">
                <div class="container" ng-controller="resultsCtrl">
                    <div class="page-path">
                        <span class="text-item"><?php _e('Home')?></span>
                        <i class="fa fa-angle-right text-item"></i>
                        <span class="text-item active"><?php _e('Domain Search')?></span>
                    </div>
                    <section class="block results" id="domainssearch-results" ng-if="searchResultsVisible">
                        <h2 class="block-title"><?php _e('Search results')?></h2>
                        <div class="results-blocks">
                            <div class="results-col">

                                <div class="info-block" ng-if="primary" ng-class="{loaded:primary.loaded, available:primary.available, taken:!primary.available, selected:primary.selected}">
                                    <div class="title"><i class="fa"></i><span>www.{{primary.sld}}.{{primary.tld}}</span></div>
                                    <div class="sub-title loading" ng-if="!primary.loaded"><i class="icon domainsearch loading"></i><?php _e('Loading')?></div>
                                    <div class="sub-title available" ng-if="primary.loaded && primary.available"><?php printf(__('Congratulations! %s is available!'), 'www.{{primary.sld}}.{{primary.tld}}')?></div>
                                    <div class="sub-title taken" ng-if="primary.loaded && !primary.available"><?php printf(__('%s is not available!'), 'www.{{primary.sld}}.{{primary.tld}}')?></div>
                                    <div class="domain-options" ng-if="primary.loaded && primary.available">
                                        <div class="dropdown">
                                            <select name="period" ng-model="primary.period">
                                                <option ng-repeat="(index, price) in primary.prices" value="{{index}}" ng-selected="price.period==primary.period">{{price.period}} <?php _e('year')?> {{price.price}}</option>
                                            </select>
                                        </div><div class="selection">
                                            <button type="button" class="button white" ng-if="primary.available && !primary.selected" ng-click="primary.selected=true"><?php _e('Select')?></button>
                                            <div class="selected" ng-if="primary.selected">
                                                <i class="fa"></i>
                                                <span><?php _e('Selected')?></span>
                                            </div>
                                            <div class="remove-domain" ng-if="primary.selected">
                                                <a href="javascript:" class="action remove" ng-click="primary.selected=false"><?php _e('Remove')?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="domainsearch-results-container" class="results">

                                    <div class="domain-row" ng-repeat="domain in domains | filter:{visible:true, primary:false} | limitTo: pager.limit" ng-class="{active: domain.selected, primary: domain.primary}" ng-init="loadDomain(domain.sld, domain.tld)">
                                        <div class="outer-col first">
                                            <div class="domain-col col-1">
                                                <div class="domain-title">www.{{domain.sld}}.{{domain.tld}}</div>
                                                <div class="info loading" ng-if="!domain.loaded">
                                                    <i class="fa"></i>
                                                    <span><?php _e('Loading')?></span>
                                                </div>
                                                <div class="info available" ng-if="domain.loaded && domain.available">
                                                    <i class="fa"></i>
                                                    <span><?php _e('Available')?></span>
                                                </div>
                                                <div class="info  not-available" ng-if="domain.loaded && !domain.available">
                                                    <i class="fa"></i>
                                                    <span><?php _e('Not Available')?></span>
                                                </div>
                                            </div>
                                            <div class="domain-col col-2" ng-if="domain.loaded && domain.available">
                                                <div class="dropdown">
                                                    <select name="period" ng-model="domain.period">
                                                        <option ng-repeat="(index, price) in domain.prices" value="{{index}}" ng-selected="price.period==domain.period">{{price.period}} <?php _e('year')?> {{price.price}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="outer-col second" ng-if="domain.loaded && domain.available">
                                            <div class="domain-col col-3">
                                                <div class="price" ng-bind="domain.prices[domain.period].price"></div>
                                            </div>
                                            <div class="domain-col col-4">
                                                <button type="button" class="button white select" ng-click="domain.selected=true"><?php _e('Select')?></button>
                                                <div class="selected">
                                                    <i class="fa"></i>
                                                    <span><?php _e('Selected')?></span>
                                                </div>
                                                <div class="remove-domain">
                                                    <a href="javascript:" class="action remove" ng-click="domain.selected=false"><?php _e('Remove')?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="results-actions">
                                    <a href="javascript:" class="button primary loadmore" ng-click="loadMore()" ng-if="pager.limit<pager.total"><?php _e('Load More')?></a>
                                    <div class="pager-stats"><?php printf(__('Showing %s of %s'), '<span class="showing" ng-bind="pager.limit">0</span>', '<span class="total" ng-bind="pager.total">0</span>')?></div>
                                    <a href="javascript:" class="button highlight order" ng-class="{disabled:!(domains|filter:{selected:true}:true).length}" ng-if="pager.total>0" ng-click="submit()"><?php _e('Order now')?></a>
                                </div>

                            </div>
                            <div class="filter-col">

                                <div id="domainsearch-results-filters"  class="filter-block">
                                    <div class="title"><?php _e('Filter')?></div>
                                    <div class="filter-choice-block price">
                                        <div class="input-title"><?php _e('Price')?></div>
                                        <p><?php _e('Enter a maximum price')?></p>
                                        <div class="price-input">
                                            <div class="input-block">
                                                <input type="number" name="price" ng-model="filters.priceinput" on-enter-key="filters.price=filters.priceinput" data-validate="number"/>
                                            </div>
                                            <button type="button" class="button highlight filterprice" ng-click="filters.price=filters.priceinput"><?php _e('Apply')?></button>
                                        </div>
                                    </div>
                                    <div class="filter-choice-block">
                                        <div class="input-title"><?php _e('TLDs')?></div>
                                        <div class="scroll">
                                            <?php foreach($domainPrices as $index => $tld):?>
                                                <label class="input-checkbox green">
                                                    <input ng-model="search.tlds['<?php echo $tld['tld'];?>'].selected" type="checkbox" name="tld" value="<?php echo $tld['tld'];?>"/>
                                                    <span class="checkbox">
                                                        <i class="fa fa-check"></i>
                                                        <span>.<?php echo $tld['tld'];?></span>
                                                    </span>
                                                </label>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>
                    <div class="page-title-block">
                        <h1 class="page-title"><?php _e('Get a FREE domain!')?></h1>
                        <span class="title-text"><?php _e('Sign up with us and get a free domain name along with your hosting services! Please note that free domain registration is available for particular TLDs only.')?></span>
                    </div>
                    <?php include_block("htmlblocks/pricing-tables/web-hosting.php", array('destination' => 'domain-search')); ?>
                </div>
            </div>
        </div>
    </section>

    <section class="block domain-ext">
        <div class="container">
            <div class="row">

                <h2 class="block-title"><?php _e('Choose a TLD')?></h2>

                <div class="ext-block">
                    <div class="ext-prices">
                        <?php foreach($domainPrices as $domain):?>
                            <?php if(in_array($domain['tld'], array('com', 'net', 'org', 'biz', 'co'))):?>
                            <div>
                                <div class="ext">
                                    <div><i class="icon exts <?php echo $domain['tld']?>"></i></div>
                                    <div class="prices">
                                        <div class="price"><span><?php echo $domain['prices']['register'][1]['price'];?></span> <?php _e('/ year')?></div>
                                        <div class="price old"><span><?php echo $whmcs->formatPrice((float)($domain['prices']['register'][1]['main'] . '.' . $domain['prices']['register'][1]['sub']) + 2);?></span> <?php _e('/ year')?></div>
                                    </div>
                                </div>
                            </div>
                            <?php endif;?>
                        <?php endforeach;?>
                    </div>
                    <div class="ext-table">
                        <div class="tbl-col">
                            <div class="ext-row title">
                                <div class="info-row"><?php _e('TLD')?></div>
                                <div class="info-row"><?php _e('Min. Period')?></div>
                                <div class="info-row"><?php echo _x_lc('Register', 'noun')?></div>
                                <div class="info-row"><?php echo _x_lc('Transfer', 'noun')?></div>
                                <div class="info-row"><?php echo _x_lc('Renew', 'noun')?></div>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="ext-row title">
                                <div class="info-row"><?php _e('TLD')?></div>
                                <div class="info-row"><?php _e('Min. Period')?></div>
                                <div class="info-row"><?php echo _x_lc('Register', 'noun')?></div>
                                <div class="info-row"><?php echo _x_lc('Transfer', 'noun')?></div>
                                <div class="info-row"><?php echo _x_lc('Renew', 'noun')?></div>
                            </div>
                        </div>

                        <?php foreach($domainPrices as $index => $tld):?>

                            <?php

                            $registerPrices = isset($tld['prices']['register']) ? $tld['prices']['register'] : array();
                            $transferPrices = isset($tld['prices']['transfer']) ? $tld['prices']['transfer'] : array();
                            $renewPrices = isset($tld['prices']['renew']) ? $tld['prices']['renew'] : array();

                            reset($registerPrices);
                            reset($transferPrices);
                            reset($renewPrices);

                            $registerPrice = isset($tld['prices']['register'][key($registerPrices)]['price']) ? $tld['prices']['register'][key($registerPrices)]['price'] : '-';
                            $transferPrice = isset($tld['prices']['transfer'][key($transferPrices)]['price']) ? $tld['prices']['transfer'][key($transferPrices)]['price'] : '-';
                            $renewPrice = isset($tld['prices']['renew'][key($renewPrices)]['price']) ? $tld['prices']['renew'][key($renewPrices)]['price'] : '-';

                            ?>

                            <div class="tbl-col">
                                <div class="ext-row extension">
                                    <div class="info-row">.<?php echo $tld['tld'];?></div>
                                    <div class="info-row"><?php echo key($registerPrices)?></div>
                                    <div class="info-row"><?php echo $registerPrice;?></div>
                                    <div class="info-row"><?php echo $transferPrice;?></div>
                                    <div class="info-row"><?php echo $renewPrice;?></div>
                                </div>
                            </div>

                        <?php endforeach;?>

                        <?php if(count($domainPrices)%2 == 1):?>

                            <div class="tbl-col">
                                <div class="ext-row extension empty">
                                    <div class="info-row">&nbsp;</div>
                                    <div class="info-row">&nbsp;</div>
                                    <div class="info-row">&nbsp;</div>
                                    <div class="info-row">&nbsp;</div>
                                    <div class="info-row">&nbsp;</div>
                                </div>
                            </div>

                        <?php endif;?>

                    </div>
                </div>
                <a href="javascript:void(0)" class="button highlight expand-collapse-exts expand"><?php _e('Expand')?></a>
                <a href="javascript:void(0)" class="button highlight expand-collapse-exts collapse"><?php _e('Collapse')?></a>

            </div>
        </div>
    </section>


    <?php include("htmlblocks/domain_how_to.php"); ?>

</div>

<script type="text/javascript">window.WhmcsTlds = <?php echo json_encode($domainPrices);?>;</script>

<?php

wp_enqueue_script('socketio', get_template_directory_uri() . '/js/vendor/socket.io-1.2.0.js', ['jquery'], false, true);
wp_enqueue_script('angularjs', get_template_directory_uri() . '/js/vendor/angular.js', ['jquery'], false, true);
wp_enqueue_script('angularjs.debounce', get_template_directory_uri() . '/js/vendor/angular-debounce.js', ['jquery'], false, true);
wp_enqueue_script('punycode', get_template_directory_uri() . '/js/vendor/punycode.js', ['jquery'], false, true);
wp_enqueue_script('h1p.domains', get_template_directory_uri() . '/js/domains.js', ['jquery', 'socketio', 'angularjs', 'angularjs.debounce'], false, true);

get_footer();

?>
