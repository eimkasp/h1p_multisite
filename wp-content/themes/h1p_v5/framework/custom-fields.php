<?php
/**
 * ACF Local JSON saving and updating
 * Reference: https://www.advancedcustomfields.com/resources/local-json/
 */

add_filter('acf/settings/save_json', 'my_acf_json_save_point');

function my_acf_json_save_point( $path ) {

// update path
$path = get_stylesheet_directory() . '/framework/custom-fields';


// return
return $path;

}

add_filter('acf/settings/load_json', 'my_acf_json_load_point');

function my_acf_json_load_point( $paths ) {

    // remove original path (optional)
    unset($paths[0]);


    // append path
    $paths[] = get_stylesheet_directory() . '/framework/custom-fields';


    // return
    return $paths;

}


/* ACF options page */
if (function_exists('acf_add_options_page')) {
	$parent = acf_add_options_page(array(
		'page_title' => 'Site options',
		'menu_title' => 'Site options',
	));
}