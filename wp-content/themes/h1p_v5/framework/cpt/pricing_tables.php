<?php

if ( !class_exists( 'H1P_Pricing_Tables' ) ) {

    class H1P_Pricing_Tables {

        function __construct() {
            add_action( 'init', array( $this, 'h1p_register_pricing_cpt' ), 1 );
        }

        function h1p_register_pricing_cpt() {

            // Register custom post type
            $labels = array(
                'name'               => __( 'Pricing tables', 'ktu' ),
                'singular_name'      => __( 'Table', 'ktu' ),
                'add_new'            => __( 'Add New', 'ktu' ),
                'add_new_item'       => __( 'Add new table', 'ktu' ),
                'edit_item'          => __( 'Edit table', 'ktu' ),
                'new_item'           => __( 'New table', 'ktu' ),
                'all_items'          => __( 'All tables', 'ktu' ),
                'view_item'          => __( 'View tables', 'ktu' ),
                'search_items'       => __( 'Search', 'ktu' ),
                'not_found'          => __( 'Not found', 'ktu' ),
                'not_found_in_trash' => __( 'Not found in trashh', 'ktu' ),
                'parent_item_colon'  => __( '', 'ktu' ),
                'menu_name'          => __( 'Pricing tables', 'ktu' ),
            );

            $args = array(
                'labels'             => $labels,
                'public'             => true,
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                'rewrite'            => array( 'slug' => 'pricing-tables' ),
                'capability_type'    => 'post',
                'has_archive'        => true,
                'hierarchical'       => false,
                'menu_position'      => 6,
                'supports'           => array( 'title', 'revisions' )
            );

            register_post_type( 'pricing-tables', $args );


            register_taxonomy(
                'pricing-category', 'pricing-tables', array(
                    'hierarchical' => true,
                    'label' => __('Pricing categories', 'ktu'),
                    'singular_label' => __('Table category', 'ktu'),
                    'rewrite' => true,
                    'query_var' => true,
                    'public' => true,
                    'rewrite' => array('slug' => 'pricing-cat', 'with_front' => true)
                )
            );

        }
    }

    new H1P_Pricing_Tables();
}
