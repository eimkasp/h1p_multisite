<?php

if ( !class_exists( 'H1P_Locations' ) ) {

    class H1P_Locations {

        function __construct() {
            add_action( 'init', array( $this, 'h1p_register_locations_cpt' ), 1 );
        }

        function h1p_register_locations_cpt() {

            // Register custom post type
            $labels = array(
                'name'               => __( 'Server Locations', 'h1p' ),
                'singular_name'      => __( 'Location', 'h1p' ),
                'add_new'            => __( 'Add New', 'h1p' ),
                'add_new_item'       => __( 'Add new location', 'h1p' ),
                'edit_item'          => __( 'Edit location', 'h1p' ),
                'new_item'           => __( 'New location', 'h1p' ),
                'all_items'          => __( 'All location', 'h1p' ),
                'view_item'          => __( 'View location', 'h1p' ),
                'search_items'       => __( 'Search', 'h1p' ),
                'not_found'          => __( 'Not found', 'h1p' ),
                'not_found_in_trash' => __( 'Not found in trash', 'h1p' ),
                'parent_item_colon'  => __( '', 'h1p' ),
                'menu_name'          => __( 'Server Locations', 'h1p' ),
            );

            $args = array(
                'labels'             => $labels,
                'public'             => true,
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                'rewrite'            => array( 'slug' => 'locations' ),
                'capability_type'    => 'post',
                'has_archive'        => true,
                'hierarchical'       => false,
                'menu_position'      => 6,
                'supports'           => array( 'title', 'revisions' )
            );

            register_post_type( 'locations', $args );


            register_taxonomy(
                'locations-category', 'locations', array(
                    'hierarchical' => true,
                    'label' => __('Locations categories', 'h1p'),
                    'singular_label' => __('Location category', 'h1p§'),
                    'rewrite' => true,
                    'query_var' => true,
                    'public' => true,
                    'rewrite' => array('slug' => 'location-cat', 'with_front' => true)
                )
            );

        }
    }

    new H1P_Locations();
}
