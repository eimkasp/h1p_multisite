<?php

if ( !class_exists( 'H1P_Reviews' ) ) {

    class H1P_Reviews {

        function __construct() {
            add_action( 'init', array( $this, 'h1p_register_reviews_cpt' ), 1 );
        }

        function h1p_register_reviews_cpt() {

            // Register custom post type
            $labels = array(
                'name'               => __( 'Reviews', 'h1p' ),
                'singular_name'      => __( 'Review', 'h1p' ),
                'add_new'            => __( 'Add New', 'h1p' ),
                'add_new_item'       => __( 'Add new review', 'h1p' ),
                'edit_item'          => __( 'Edit review', 'h1p' ),
                'new_item'           => __( 'New review', 'h1p' ),
                'all_items'          => __( 'All reviews', 'h1p' ),
                'view_item'          => __( 'View reviews', 'h1p' ),
                'search_items'       => __( 'Search', 'h1p' ),
                'not_found'          => __( 'Not found', 'h1p' ),
                'not_found_in_trash' => __( 'Not found in trash', 'h1p' ),
                'parent_item_colon'  => __( '', 'h1p' ),
                'menu_name'          => __( 'Reviews', 'h1p' ),
            );

            $args = array(
                'labels'             => $labels,
                'public'             => true,
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                'rewrite'            => array( 'slug' => 'reviews' ),
                'capability_type'    => 'post',
                'has_archive'        => true,
                'hierarchical'       => false,
                'menu_position'      => 6,
                'supports'           => array( 'title', 'revisions' )
            );

            register_post_type( 'reviews', $args );


            register_taxonomy(
                'reviews-category', 'reviews', array(
                    'hierarchical' => true,
                    'label' => __('Reviews categories', 'h1p'),
                    'singular_label' => __('Review category', 'h1p§'),
                    'rewrite' => true,
                    'query_var' => true,
                    'public' => true,
                    'rewrite' => array('slug' => 'review-cat', 'with_front' => true)
                )
            );

        }
    }

    new H1P_Reviews();
}
