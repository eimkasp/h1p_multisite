<?php

include(H1P_THEME_DIR . "/components/vc_extensions.php");

/**
 * Removing unused composer elements
 * */
if (function_exists('vc_remove_element')) {
//    vc_remove_element("vc_raw_js"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_line_chart"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_acf"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
//    vc_remove_element("media_grid"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
//    vc_remove_element("vc_icon"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_flickr"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
//    vc_remove_element("vc_text_separator"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_progress_bar"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_facebook"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_pie"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_googleplus"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_round_chart"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_toggle"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_imagegallery"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
//    vc_remove_element("vc_media_grid"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
//    vc_remove_element("vc_separator"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_widget_sidebar"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_video"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_message"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_pinterest"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_images_carousel"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_tta_pageable"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_cta"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_gmaps"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_tweetmeme"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
//    vc_remove_element("vc_single_image"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_custom_heading"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_posts_slider"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_empty_space"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_masonry_grid"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_masonry_media_grid"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_wp_recentcomments"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_wp_rss"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_wp_archives"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_wp_text"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_wp_custommenu"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_wp_tagcloud"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_wp_pages"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_wp_calendar"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_wp_search"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_wp_meta"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_wp_posts"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
    vc_remove_element("vc_wp_categories"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
//    vc_remove_element("vc_gallery"); //Note: "vc_message" was used in the vc_map() function call as a base parameter for "Message box" element
}