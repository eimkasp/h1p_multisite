<?php

wp_enqueue_script('jquery.easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', ['jquery'], '1.3', true);
wp_enqueue_script('jquery.jcarousel', 'https://cdnjs.cloudflare.com/ajax/libs/jcarousel/0.3.4/jquery.jcarousel.min.js', ['jquery'], false, true);
wp_enqueue_script('jquery.jcarousel-swipe', get_template_directory_uri() . '/js/vendor/jquery.jcarousel-swipe.min.js', ['jquery'], false, true);
wp_enqueue_script('flippingtext', get_template_directory_uri() . '/js/vendor/flippingtext.js', [], false, true);
wp_enqueue_script('particles', 'https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js', ['jquery'], '2.0.0', true);
