<?php

/**
 * @package WordPress
 * @subpackage h1p_v5
 */
/*
Template Name: Speed test technical page
*/

global $config;

/*Image resources url*/
$images = $site_current_url . "/wp-content/themes/h1p_v5/";

function do_speedtest_survey ()
{

    global $config;
    global $images;

    //Required Fields
    $API_KEY = '330e5f214f0f14bb01dfe8ffbae7d2db2df6b0334c2099b250';
    $API_SECRET = 'A9pn0wf3VceVM';
    $survey = '2869138'; //Survey to pull from

    $post_action = ( array_key_exists('action', $_POST) ? $_POST['action'] : null);

    if( isset( $post_action ) || $post_action == 'submit' ) {

        //Restful API Call URL base
        $submit_url_base = "https://restapi.surveygizmo.com/v4/survey/{$survey}/surveyresponse?api_token={$API_KEY}&api_token_secret={$API_SECRET}&_method=PUT";

        /*
        params:
        https://restapi.surveygizmo.com/v4/survey/2869138/?api_token=330e5f214f0f14bb01dfe8ffbae7d2db2df6b0334c2099b250&api_token_secret=A9pn0wf3VceVM

        pvz:
        $survey_info_url = "https://restapi.surveygizmo.com/v4/survey/{$survey}/surveyresponse?api_token={$API_KEY}&api_token_secret={$API_SECRET}&_method=PUT&data[6][value]={$location}&data[7][10003]={$clientid}&data[3][10004]={$refererurl}";
        */


        // survey data
        $location = urlencode($_POST['location']);
        $client_ip = urlencode($_POST['client_ip']);
        $client_location = urlencode($_POST['client_location']);

        $submit_url_params = "&data[6][value]={$location}";
        $submit_url_params .= "&data[8][value]={$client_ip}";
        $submit_url_params .= "&data[10][value]={$client_location}";

        $post_data_10006 = (array_key_exists('10006', $_POST) ? $_POST['10006'] : null);
        $q7_10006 = $post_data_10006;
        if (isset($q7_10006))
        {
            $q7_10006 = urlencode($q7_10006);
            $submit_url_params .= "&data[7][10006]={$q7_10006}";
        }

        $post_data_10007 = (array_key_exists('10007', $_POST) ? $_POST['10007'] : null);
        $q7_10007 = $post_data_10007;
        if (isset($q7_10007))
        {
            $q7_10007 = urlencode($q7_10007);
            $submit_url_params .= "&data[7][10007]={$q7_10007}";
        }

        $post_data_10001 = (array_key_exists('10001', $_POST) ? $_POST['10001'] : null);
        $q5_10001 = $post_data_10001;
        if (isset($q5_10001))
        {
            $q5_10001 = urlencode($q5_10001);
            $submit_url_params .= "&data[5][10001]={$q5_10001}";
        }

        $post_data_10002 = (array_key_exists('10002', $_POST) ? $_POST['10002'] : null);        
        $q5_10002 = $post_data_10002;
        if (isset($q5_10002))
        {
            $q5_10002 = urlencode($q5_10002);
            $submit_url_params .= "&data[5][10002]={$q5_10002}";
        }

        $post_data_10003 = (array_key_exists('10003', $_POST) ? $_POST['10003'] : null);        
        $q5_10003 = $post_data_10003;
        if (isset($q5_10003))
        {
            $q5_10003 = urlencode($q5_10003);
            $submit_url_params .= "&data[5][10003]={$q5_10003}";
        }

        $submit_url = $submit_url_base . $submit_url_params;

        //echo '<pre>'.$submit_url.'</pre>';

        //Curl call
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $submit_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        $error = curl_errno($ch);
        $debug = curl_getinfo($ch);
        $output = json_decode($output);

        if( $output->result_ok == true ){
            $response = [
                        'success' => true,
                        'message' => _('Thank You!'),
                        'raw' => $output
                    ];
        } else {
            $response = [
                        'success' => false,
                        'message' => 'ERROR',
                        'raw' => $output
                    ];
        }

        echo json_encode( $response );

/*        echo '<pre>';
        var_dump($error);
        echo '</pre>';
        echo '<pre>';
        var_dump($debug);
        echo '</pre>';*/

        exit;
        // =======================================================================
        // if is a form data submission via ajax call further code is not executed

    }



    // ==================================
    // following code is for initial view

    $test_location = filter_input(INPUT_GET, 'location', FILTER_SANITIZE_STRING);

    //var_dump($location);

    $location = array(
        'frankfurt' => array(
            'name' => __('Frankfurt, Germany'),
            '100mb_link' => 'http://185.137.13.237/100mb',
            // '100mb_link' => '#100mb',
            '1gb_link' => 'http://185.137.13.237/1000mb',
            // '1gb_link' => '#1gb',
            'lat' => 50.1211277,
            'long' => 8.4964843
        ),
        'johannesburg' => array(
            'name' => __('Johannesburg, South Africa'),
            '100mb_link' => 'http://154.127.59.49/100mb',
            // '100mb_link' => '#100mb',
            '1gb_link' => 'http://154.127.59.49/1000mb',
            // '1gb_link' => '#1gb',
            'lat' => -26.1715046,
            'long' => 27.9699852
        ),
        'chicago' => array(
            'name' => __('Chicago, USA'),
            '100mb_link' => 'http://181.214.61.207/100mb',
            // '100mb_link' => '#100mb',
            '1gb_link' => 'http://181.214.61.207/1000mb',
            // '1gb_link' => '#1gb',
            'lat' => 41.833648,
            'long' => -87.8722358
        ),
        'losangeles' => array(
            'name' => __('Los Angeles, USA'),
            '100mb_link' => 'http://191.101.236.61/100mb',
            // '100mb_link' => '#100mb',
            '1gb_link' => 'http://191.101.236.61/1000mb',
            // '1gb_link' => '#1gb',
            'lat' => 34.0204195,
            'long' => -118.5518134
        ),
        'saopaulo' => array(
            'name' => __('São Paulo, Brazil'),
            '100mb_link' => 'http://191.96.4.248/100mb',
            // '100mb_link' => '#100mb',
            '1gb_link' => 'http://191.96.4.248/1000mb',
            // '1gb_link' => '#1gb',
            'lat' => -23.6823494,
            'long' => -46.7353807
        )
    );

    if (isset($test_location))
    {

        $client_ip = $_SERVER['REMOTE_ADDR'];

    ?>

        <div id="speedtest-popup" class="popup-container white-popup-block">
            <div class="location-speedtest">
                <img class="center" src="<?php echo $images ?>img/locations/speedtest.svg" alt="Speed test" width="50">
                <span><?php _e('Speed test'); ?></span>
            </div>
            <p class="location-title"><?php echo $location[$test_location]['name']; ?></p>
            <p><?php _e('Download the test file to check approximate download speed from our servers to your computer.'); ?></p>
            <p id="file-links" class="p-v-20">
                <a href="<?php echo $location[$test_location]['100mb_link']; ?>" id="file-100mb" class="test-link" data-file-size-id="10006" data-file-size-name="100 MB"><i class="fa fa-download"></i> 100 MB</a>
                <a href="<?php echo $location[$test_location]['1gb_link']; ?>" id="file-1gb" class="test-link" data-file-size-id="10007" data-file-size-name="1 GB"><i class="fa fa-download"></i> 1 GB</a>
            </p>
            <div id="test-info" class="hidden">
                <p class="center p-v-10"><strong><?php _e('EVALUATE YOUR SPEED TEST RESULTS'); ?></strong></p>
                <p id="file-size"></p>
                <p id="geo-location"></p>
                <p id="client-ip"><?php echo $client_ip; ?></p>
                <p id="geo-distance" data-server-latitude="<?php echo $location[$test_location]['lat']; ?>" data-server-longitude="<?php echo $location[$test_location]['long']; ?>"></p>
            </div>
            <div id="rating">
                <form id="speedtest-feedback" action="<?php echo $config['links']['speedtest']?>">
                    <input type="hidden" name="action" value="submit">
                    <input type="hidden" name="location" value="<?php echo $test_location ?>">
                    <input type="hidden" name="client_ip" value="<?php echo $client_ip ?>">
                    <input type="hidden" name="client_location">

                    <span class="hidden">
                        <input id="7_10006" type="radio" name="10006" value="100 MB" data-question-num="7" data-question-other="10006">
                        <label for="7_10006"><?php _e('100 MB'); ?></label>
                        <input id="7_10007" type="radio" name="10007" value="1 GB" data-question-num="7" data-question-other="10007">
                        <label for="7_10007"><?php _e('1 GB'); ?></label>

                        <input id="5_10003" type="radio" name="10003" value="Slow" data-question-num="5" data-question-other="10003">
                        <label for="5_10003"><?php _e('Slow'); ?></label>
                        <input id="5_10002" type="radio" name="10002" value="Average" data-question-num="5" data-question-other="10002">
                        <label for="5_10002"><?php _e('Average'); ?></label>
                        <input id="5_10001" type="radio" name="10001" value="Fast" data-question-num="5" data-question-other="10001">
                        <label for="5_10001"><?php _e('Fast'); ?></label>
                    </span>
                </form>
            </div>
        </div>

    <?php
    } else {
    ?>

        <div id="speedtest-popup" class="popup-container white-popup-block">
            <p>Sorry, the location for test is not specified.</p>
        </div>

    <?php
    }



} /*end of do_speedtest_survey()*/

do_speedtest_survey();
