<?php
/**
 *
 * @package WordPress
 * @subpackage h1p_v4
 *
 *
 * Template Name: Reseller sign in Popup
 *
**/


global $config;

if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {

    header('Location: '.$config['links']['website'].'#login');
    die();
}

?>
<div class="popup-container login">
    <div class="popup-header">
        <div class="logo">
            <img alt="HOST1PLUS" src="<?php echo get_template_directory_uri(); ?>/img/logo.svg"/>
        </div>
        <div class="description">
            <h3 class="block-title-small m-t-20"><?php _e('Interested in our Reseller Program?')?></h3>
            <p><?php _e('Log in with your Host1Plus account credentials. Your Personal Manager will contact you after successful application.')?></p>
        </div>
    </div>
    <div class="popup-content">
        <div class="block login">
            <span class="block-title"><?php _e('Log In')?></span>
            <form id="reseller_sign_in_form" action="" method="POST">
                <div class="form-field">
                    <input id="login-username" type="text" name="username" placeholder="<?php _e('Username')?>"/>
                </div>
                <div class="form-field">
                    <input id="login-password" type="password" name="password" placeholder="<?php _e('Password')?>"/>
                </div>
                <a class="forgot-pass-link" href="<?php echo $config['whmcs_links']['passwordreset']?>?back=1"><?php _e('Forgot your password?')?></a>
                <div class="ajax-message message" data-ajax-message-wrapper style="display:none;">
                  <i class="fa fa-info-circle" aria-hidden="true"></i>
                  <span data-ajax-message-text></span>
                </div>
                <div class="form-field submit">
                    <button id="reseller_sign_in_form_button" class="button primary" type="submit"><?php _e('Log In & Apply')?></button>
                </div>
            </form>
        </div>
    </div>
</div>
