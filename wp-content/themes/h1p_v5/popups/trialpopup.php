<?php
/**
 *
 * @package WordPress
 * @subpackage h1p_v4
 *
 *
 * Template Name: Trial Popup
 *
**/
?>
<div class="popup-container trial">
    <div class="popup-header">
        <div class="title"><?php printf(__('Discover HOST1PLUS services for %sFREE%s!'), '<span class="orange">', '</span>')?></div>
        <span class="title-text"><?php _e('Select your preferred service and enjoy free hosting for 14 days*. Try our premium features provided for standard hosting services. No obligations and no strings attached!')?></span>
    </div>
    <div class="popup-content">
        <div class="block trial">
            <div class="plans-pricing-tables labeled-third labeled-fourth">
                <div class="plan-col plan-col-1 web">
                    <div class="plan">
                        <div class="info-text"><?php _e('WEB Hosting')?></div>
                        <div class="plan-block">
                            <span class="plan-title"><?php _e('Personal')?></span>
                            <span class="plan-descr"><?php _e('Great for simple personal websites and blogs.')?></span>
                            <span class="price"><span>$0.00</span><span class="sub full">/ 14 <?php _e('days')?></span><span class="sub sort">/ 14 <?php _e('days')?></span></span>
                            <div class="drop-link-table">
                                <div class="cell-link">
                                    <a href="javascript:void(0)" onclick="TrialPopup.toogleplans('web');" class="button primary web-h-bt"><?php _e('Order Now')?></a>
                                </div>
                            </div>
                            <div class="features">
                                <div class="feature-col">
                                    <div class="feature first left">
                                        <span class="cell left simple"><?php _e('Bandwidth')?></span>
                                        <span class="cell right bold">50GB</span>
                                    </div>
                                </div>
                                <div class="feature-col">
                                    <div class="feature first right">
                                        <span class="cell left"><?php _e('Disk Space')?></span>
                                        <span class="cell right bold">1GB</span>
                                    </div>
                                </div>
                                <div class="feature-col">
                                    <div class="feature left">
                                        <span class="cell left"><?php _e('Domain Name')?></span>
                                        <span class="cell right bold">$12.65 USD</span>
                                    </div>
                                </div>
                                <div class="feature-col">
                                    <div class="feature right">
                                        <span class="cell left"><?php _e('Network Uptime')?></span>
                                        <span class="cell right bold green">99%</span>
                                    </div>
                                </div>
                                <div class="feature-col">
                                    <div class="feature last left">
                                        <span class="cell left"><?php _e('Email Accounts')?></span>
                                        <span class="cell right bold green"><?php _e('Unlimited')?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="plan-col plan-col-2 vps">
                    <div class="plan">
                        <div class="info-text orange"><?php _e('VPS Hosting')?></div>
                        <div class="plan-block">
                            <span class="plan-title"><?php _e('Silver')?></span>
                            <span class="plan-descr"><?php _e('Excellent for apps, personal websites and test projects.')?></span>
                            <span class="price"><span>$0.00</span><span class="sub full">/ 14 <?php _e('days')?></span><span class="sub sort">/ 14 <?php _e('days')?></span></span>
                            <div class="drop-link-table">
                                <div class="cell-link">
                                    <a href="javascript:void(0)" onclick="TrialPopup.toogleplans('vps');" class="button primary vps-h-bt"><?php _e('Order Now')?></a>
                                </div>
                            </div>
                            <div class="features">
                                <div class="feature-col">
                                    <div class="feature first left">
                                        <span class="cell left simple"><?php _e('Bandwidth')?></span>
                                        <span class="cell right bold">50GB</span>
                                    </div>
                                </div>
                                <div class="feature-col">
                                    <div class="feature first right">
                                        <span class="cell left"><?php _e('Disk Space')?></span>
                                        <span class="cell right bold">1GB</span>
                                    </div>
                                </div>
                                <div class="feature-col">
                                    <div class="feature left">
                                        <span class="cell left"><?php _e('Domain Name')?></span>
                                        <span class="cell right bold">$12.65 USD</span>
                                    </div>
                                </div>
                                <div class="feature-col">
                                    <div class="feature right">
                                        <span class="cell left"><?php _e('Network Uptime')?></span>
                                        <span class="cell right bold green">99%</span>
                                    </div>
                                </div>
                                <div class="feature-col">
                                    <div class="feature last left">
                                        <span class="cell left"><?php _e('Email Accounts')?></span>
                                        <span class="cell right bold green"><?php _e('Unlimited')?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="plan-col plan-col-3 cloud">
                    <div class="plan">
                        <div class="info-text orange"><?php _e('Cloud Hosting')?></div>
                        <div class="plan-block">
                            <span class="plan-title"><?php _e('Gold')?></span>
                            <span class="plan-descr"><?php _e('Great for media streamers, archive sites and more.')?></span>
                            <span class="price"><span>$0.00</span><span class="sub full">/ 14 <?php _e('days')?></span><span class="sub sort">/ 14 <?php _e('days')?></span></span>
                            <div class="drop-link-table">
                                <div class="cell-link">
                                    <a href="javascript:void(0)" onclick="TrialPopup.toogleplans('cloud');" class="button primary cloud-h-bt"><?php _e('Order Now')?></a>
                                </div>
                            </div>
                            <div class="features">
                                <div class="feature-col">
                                    <div class="feature first left">
                                        <span class="cell left simple"><?php _e('Bandwidth')?></span>
                                        <span class="cell right bold">50GB</span>
                                    </div>
                                </div>
                                <div class="feature-col">
                                    <div class="feature first right">
                                        <span class="cell left"><?php _e('Disk Space')?></span>
                                        <span class="cell right bold">1GB</span>
                                    </div>
                                </div>
                                <div class="feature-col">
                                    <div class="feature left">
                                        <span class="cell left"><?php _e('Domain Name')?></span>
                                        <span class="cell right bold">$12.65 USD</span>
                                    </div>
                                </div>
                                <div class="feature-col">
                                    <div class="feature right">
                                        <span class="cell left"><?php _e('Network Uptime')?></span>
                                        <span class="cell right bold green">99%</span>
                                    </div>
                                </div>
                                <div class="feature-col">
                                    <div class="feature last left">
                                        <span class="cell left"><?php _e('Email Accounts')?></span>
                                        <span class="cell right bold green"><?php _e('Unlimited')?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- tos --->
                <div class="plan-col plan-col-4 tac" style="display:none;">
                    <div class="plan">
                        <div class="info-text"><?php _e('Terms and Conditons')?></div>
                        <div class="plan-block">
                            <span class="plan-descr">
                                1) DEFINITIONS
                                “Goods” means any goods and/or services provided by the Company as ordered by the Client
                                “ Company” means Sample Answers Ltd. incorporating Prime Prospects.
                                “ Client” means the person, firm or company placing an order with the Company.
                                3) FORMATION OF CONTRACT
                                All Goods sold by the Company are sold subject to the Company’s standard terms and conditions (as detailed below) which form part of the Client’s contract with the Company. Terms and conditions on the Client’s order form or other similar document shall not be binding on the Company.
                                4) QUOTATIONS
                                The prices, quantities and delivery time stated in any quotation are not binding on the Company. They are commercial estimates only which the Company will make reasonable efforts to achieve.
                                5) ORDERS
                                5.1 Orders will be deemed to have been placed when an email confirmation has been received from a responsible executive of the client company.
                                5.2 For email invitations, online fulfilment projects and direct marketing files (as provided by Prime Prospects) it is a requirement of the Company, that the email piece, online form or mail piece shall be approved by the company before an order can be confirmed and any data despatched.
                            </span>

                            <div class="drop-link-table">
                                <div class="cell-link">
                                    <label>
                                        <input type="checkbox" name="acceptTac" id="acceptTac" onchange="TrialPopup.acceptTac()" /> <?php _e('Accept our Terms and Conditions')?>
                                    </label>
                                    <div>
                                        <span id="addWhenAccepted"></span>
                                        <a id="cancelPlan" href="javascript:void(0)" onclick="TrialPopup.cancelTac()" class="button highlight cancelPlan"><?php _e('Cancel')?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="note">
                <span><i class="fa fa-exclamation-triangle"></i></span>
                <span><p><?php _e('Registration is not available for the residents of the following countries: Albania, Bangladesh, Algeria, Egypt, Indonesia, Iran, Morocco, Monaco, Nigeria, Pakistan, Romania, Somalia, Syria, Vietnam. These limitations are set because of high fraud rate and are irrevocable unless stated otherwise.')?></p></span>
            </div>

        </div>
    </div>


</div>


