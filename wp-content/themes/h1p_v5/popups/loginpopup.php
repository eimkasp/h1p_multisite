<?php
/**
 *
 * @package WordPress
 * @subpackage h1p_v4
 *
 *
 * Template Name: Login Popup
 *
**/


global $config;

if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {

    header('Location: '.$config['links']['website'].'#login');
    die();
}

$lang_redirection_val = '';
if(get_locale() == 'en_US') {
    $lang_redirection_val = 'english';
    $lang_redirection_link = '?language=english';
}
if(get_locale() == 'pt_BR'){
    $lang_redirection_val = 'portuguese-br';
    $lang_redirection_link = '?language=portuguese-br';
}

?>
<div class="popup-container login">
    <div class="popup-header">
        <div class="logo">
            <img alt="HOST1PLUS" src="<?php echo get_template_directory_uri(); ?>/img/logo.svg"/>
        </div>
        <div class="description">
            <p><?php _e('Interested in our services?')?></p>
            <p><?php _e('To continue, log in to your account or enter your details and join us now.')?></p>
        </div>
    </div>
    <div class="popup-content">
        <div class="block login">
            <span class="block-title"><?php _e('Log In')?></span>
            <form action="<?php echo $config['whmcs_links']['login_post']?>" method="POST">
                <input type="hidden" name="language" value="<?php echo $lang_redirection_val ?>">
                <div class="form-field">
                    <input id="login-username" type="text" name="username" placeholder="<?php _e('Username')?>"/>
                </div>
                <div class="form-field">
                    <input type="password" name="password" placeholder="<?php _e('Password')?>"/>
                </div>
                <a class="forgot-pass-link" href="<?php echo $config['whmcs_links']['passwordreset'] . $lang_redirection_link ?>&back=1"><?php _e('Forgot your password?')?></a>
                <div class="form-field submit">
                    <button class="button primary" type="submit"><?php _e('Log In & Continue')?></button>
                </div>
            </form>
        </div>
        <form class="block register" action="<?php echo $config['whmcs_links']['register']?>" method="POST">
            <input type="hidden" name="language" value="<?php echo $lang_redirection_val ?>">
            <span class="block-title"><?php _e('New Customer')?></span>
            <span class="description"><?php _e('Don’t have an account yet?');?> <br/><?php _e('Get started today!')?></span>
            <button class="button highlight register-link" type="submit"><?php _e('Register')?></button>
        </form>
    </div>
</div>
